<?php

return [
	'name' => 'API',
    'icon' => 'fa-cloud',
    'version' => '1.0',
    'title' => 'Shine API',
    'folder' => 'API',
    'description' => 'API',
    'developer' => 'Ateneo ShineLabs',
    'copy' => '2016',
    'url' => 'www.shine.ph'
];

    
