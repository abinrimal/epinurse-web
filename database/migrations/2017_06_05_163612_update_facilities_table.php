<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    
        if (Schema::hasColumn('facilities', 'wskey')!=TRUE) {
            Schema::table('facilities', function (Blueprint $table) { 
                $table->string('wskey', 100)->nullable();
                $table->string('ekey', 100)->nullable(); 
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
