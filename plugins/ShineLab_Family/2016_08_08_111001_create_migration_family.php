<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMigrationFamily extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('patient_familyinfo')!=TRUE) {
            Schema::create('patient_familyinfo', function (Blueprint $table) {
                $table->increments('id');
                $table->string('patient_familyinfo_id', 60);
                $table->string('patient_id', 60);

                $table->string('father_firstname', 60)->nullable();
                $table->string('father_middlename', 60)->nullable();
                $table->string('father_lastname', 60)->nullable();
                $table->string('suffix', 20)->nullable();
                $table->tinyInteger('father_alive')->nullable();
                $table->string('mother_firstname', 60)->nullable();
                $table->string('mother_middlename', 60)->nullable();
                $table->string('mother_lastname', 60)->nullable();
                $table->tinyInteger('mother_alive')->nullable();
                $table->integer('ctr_householdmembers_lt10yrs')->nullable();
                $table->integer('ctr_householdmembers_gt10yrs')->nullable();
                $table->string('family_folder_name', 60)->nullable();

                $table->softDeletes();
                $table->timestamps();
                $table->unique('patient_familyinfo_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patient_familyinfo');
    }
}
