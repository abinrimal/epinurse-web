
{!! Form::open(array('url'=>'schoolassessment/save')) !!}


{!! Form::hidden('school_id',isset($school->school_id) ? $school->school_id : NULL) !!}
<div class="box-body">
		<ul class="nav nav-tabs schoolassessment">
		  <li class="active"><a data-toggle="tab" href="#basicinfo">Basic Info</a></li>
		  <li ><a data-toggle="tab" href="#ecd">ECD</a></li>
		  <li ><a data-toggle="tab" href="#eca">Extracurricular Activities</a></li>
		  <li ><a data-toggle="tab" href="#firstaid">First Aid</a></li>
		  <li ><a data-toggle="tab" href="#water">Drinking Water</a></li>
		  <li ><a data-toggle="tab" href="#nutrition">Food and Nutrition</a></li>
		  <li ><a data-toggle="tab" href="#toilet">Toilet Facilities</a></li>
		  <li ><a data-toggle="tab" href="#infrastructure">Infrastructure</a></li>
		  <li ><a data-toggle="tab" href="#hazard">Hazard Risk Assessment</a></li>
		</ul>
		<div class="tab-content">
			<div id="basicinfo" class="tab-pane fade in active">
				<fieldset>
					<legend> Basic Information </legend>
					<div class="col-sm-12">
						<label class="col-sm-2 control-label"> School </label>
						<div class="col-sm-4">
							{!! Form::text('school_name',isset($school->school_name) ? $school->school_name : NULL,['class'=>'form-control','placeholder'=>'School Name']) !!}
						</div>
					</div>
					<div class="col-sm-12">
						<label class="col-sm-2 control-label"> Contact Person </label>
						<div class="col-sm-4">
							{!! Form::text('contact_person',isset($school->contact_person) ? $school->contact_person : NULL,['class'=>'form-control','placeholder'=>'Name of contact person']) !!}
						</div>
						<label class="col-sm-3 control-label"> Position of Contact Person </label>
						<div class="col-sm-3">
							{!! Form::text('contact_person_position',isset($school->contact_person_position) ? $school->contact_person_position : NULL,['class'=>'form-control','placeholder'=>'Position of contact person']) !!}
						</div>
					</div>
					<div class="col-sm-12">
						<div class="icheck">
							<label class="col-sm-4 control-label">Activities performed by the school nurse</label>
							<div class="col-sm-8">
								<label class="checkbox">
									{!! Form::checkbox('activities_student_health_assessment',1, isset($school->activities_student_health_assessment) && $school->activities_student_health_assessment == 1 ? true : false) !!} Health assessment of the students 	
								</label>
								<label class="checkbox">
									{!! Form::checkbox('activities_staff_members_health_assessment',1, isset($school->activities_staff_members_health_assessment) && $school->activities_staff_members_health_assessment == 1 ? true : false) !!} Health assessment of the staff members	
								</label>
								<label class="checkbox">
									{!! Form::checkbox('activities_sanitation_class',1, isset($school->activities_sanitation_class) && $school->activities_sanitation_class == 1 ? true : false) !!} Regular class to promote health and sanitation 	
								</label>
								<label class="checkbox">
									{!! Form::checkbox('activities_diet_class',1, isset($school->activities_diet_class) && $school->activities_diet_class == 1 ? true : false) !!} Healthy diet and nutrition classes
								</label>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<label class="col-sm-4 control-label"> Distance of School from Health Facility </label>
				  		<div class="col-sm-4">
				  			<div class="input-group">
				  				{!! Form::number('school_distance',isset($school->school_distance) ? $school->school_distance : NULL, ['class'=>'form-control']) !!}
				  				<span class="input-group-addon"> KM </span>
				  			</div>
				  		</div>
					</div>
				</fieldset>
			</div>
			<div id="ecd" class="tab-pane fade in">
				<fieldset>
					<legend> Early Child Development </legend>
					<div class="icheck">
						<div class="col-sm-12">
							<label class="col-sm-3 control-label"> ECD program in the school</label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('ecd_program',0, (isset($school->ecd_program) AND $school->ecd_program == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('ecd_program',1, (isset($school->ecd_program) AND $school->ecd_program == 1) ? true : false) !!} Yes
								</label>
							</div>
							<label class="col-sm-3 control-label"> Presence of ECD qualified teacher </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('ecd_teacher',0, (isset($school->ecd_teacher) AND $school->ecd_teacher == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('ecd_teacher',1, (isset($school->ecd_teacher) AND $school->ecd_teacher == 1) ? true : false) !!} Yes
								</label>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="school-subheader">
								Please tick yes if the facilities are suitable for small children
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-3 control-label"> Classroom facilities </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('ecd_classroom',0, (isset($school->ecd_classroom) AND $school->ecd_classroom == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('ecd_classroom',1, (isset($school->ecd_classroom) AND $school->ecd_classroom == 1) ? true : false) !!} Yes
								</label>
							</div>
							<label class="col-sm-3 control-label"> Toilet and handwashing </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('ecd_toilet',0, (isset($school->ecd_toilet) AND $school->ecd_toilet == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('ecd_toilet',1, (isset($school->ecd_toilet) AND $school->ecd_toilet == 1) ? true : false) !!} Yes
								</label>
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-3 control-label"> Seating arrangements </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('ecd_seat_arrangement',0, (isset($school->ecd_seat_arrangement) AND $school->ecd_seat_arrangement == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('ecd_seat_arrangement',1, (isset($school->ecd_seat_arrangement) AND $school->ecd_seat_arrangement == 1) ? true : false) !!} Yes
								</label>
							</div>
							<label class="col-sm-3 control-label"> Collision avoidance measures </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('ecd_collision_avoidance',0, (isset($school->ecd_collision_avoidance) AND $school->ecd_collision_avoidance == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('ecd_collision_avoidance',1, (isset($school->ecd_collision_avoidance) AND $school->ecd_collision_avoidance == 1) ? true : false) !!} Yes
								</label>
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-3 control-label"> Appropriate play materials </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('ecd_appr_play_material',0, (isset($school->ecd_appr_play_material) AND $school->ecd_appr_play_material == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('ecd_appr_play_material',1, (isset($school->ecd_appr_play_material) AND $school->ecd_appr_play_material == 1) ? true : false) !!} Yes
								</label>
							</div>
							<label class="col-sm-3 control-label"> Non-slip floors </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('ecd_non_slip_floors',0, (isset($school->ecd_non_slip_floors) AND $school->ecd_non_slip_floors == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('ecd_non_slip_floors',1, (isset($school->ecd_non_slip_floors) AND $school->ecd_non_slip_floors == 1) ? true : false) !!} Yes
								</label>
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-3 control-label"> Colors and illustrations </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('ecd_colors',0, (isset($school->ecd_colors) AND $school->ecd_colors == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('ecd_colors',1, (isset($school->ecd_colors) AND $school->ecd_colors == 1) ? true : false) !!} Yes
								</label>
							</div>
							<label class="col-sm-3 control-label"> Lighting facilities </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('ecd_lighting',0, (isset($school->ecd_lighting) AND $school->ecd_lighting == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('ecd_lighting',1, (isset($school->ecd_lighting) AND $school->ecd_lighting == 1) ? true : false) !!} Yes
								</label>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<div id="eca" class="tab-pane fade in">
				<fieldset>
					<legend> Extra Curricular Activities </legend>
					<div class="icheck">
						<div class="col-sm-12">
							<label class="col-sm-4 control-label"> Presence of a playground </label>
							<div class="col-sm-2">
								<label class="radio-inline">
									{!! Form::radio('eca_playground',0, (isset($school->eca_playground) AND $school->eca_playground == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('eca_playground',1, (isset($school->eca_playground) AND $school->eca_playground == 1) ? true : false) !!} Yes
								</label>
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-4 control-label"> Spacing at the sports and extra-curricular facilities </label>
							<div class="col-sm-4">
								<label class="radio-inline">
									{!! Form::radio('eca_spacing',1, (isset($school->eca_spacing) AND $school->eca_spacing == 1) ? true : false) !!} Adequate
								</label>
								<label class="radio-inline">
									{!! Form::radio('eca_spacing',0, (isset($school->eca_spacing) AND $school->eca_spacing == 0) ? true : false) !!} Inadequate
								</label>
							</div>
						</div>
						<div class="col-sm-12 eca-activities">
							<div class="school-subheader"> Extra-curricular activities available at school </div>
							<?php
								$ecas = array('EN_ICONS_ECA-01.png','EN_ICONS_ECA-02.png','EN_ICONS_ECA-03.png','EN_ICONS_ECA-04.png','EN_ICONS_ECA-05.png','EN_ICONS_ECA-06.png','EN_ICONS_ECA-07.png','EN_ICONS_ECA-08.png','EN_ICONS_ECA-09.png');
							?>
							<label class="col-sm-4 checkbox">
								{!! Form::checkbox('eca_dancing',1, isset($school->eca_dancing) && $school->eca_dancing == 1 ? true : false) !!} 
								<img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_ECA-01.png') }}" class="epinurse-icon"> 
							</label>
							<label class="col-sm-4 checkbox">
								{!! Form::checkbox('eca_singing',1, isset($school->eca_singing) && $school->eca_singing == 1 ? true : false) !!} 
								<img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_ECA-02.png') }}" class="epinurse-icon"> 
							</label>
							<label class="col-sm-4 checkbox">
								{!! Form::checkbox('eca_running',1, isset($school->eca_running) && $school->eca_running == 1 ? true : false) !!} 
								<img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_ECA-03.png') }}" class="epinurse-icon"> 
							</label>
							<label class="col-sm-4 checkbox">
								{!! Form::checkbox('eca_football',1, isset($school->eca_football) && $school->eca_football == 1 ? true : false) !!} 
								<img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_ECA-04.png') }}" class="epinurse-icon"> 
							</label>
							<label class="col-sm-4 checkbox">
								{!! Form::checkbox('eca_volleyball',1, isset($school->eca_volleyball) && $school->eca_volleyball == 1 ? true : false) !!} 
								<img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_ECA-05.png') }}" class="epinurse-icon"> 
							</label>
							<label class="col-sm-4 checkbox">
								{!! Form::checkbox('eca_basketball',1, isset($school->eca_basketball) && $school->eca_basketball == 1 ? true : false) !!} 
								<img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_ECA-06.png') }}" class="epinurse-icon"> 
							</label>
							<label class="col-sm-4 checkbox">
								{!! Form::checkbox('eca_pingpong',1, isset($school->eca_pingpong) && $school->eca_pingpong == 1 ? true : false) !!} 
								<img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_ECA-07.png') }}" class="epinurse-icon"> 
							</label>
							<label class="col-sm-4 checkbox">
								{!! Form::checkbox('eca_badminton',1, isset($school->eca_badminton) && $school->eca_badminton == 1 ? true : false) !!} 
								<img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_ECA-08.png') }}" class="epinurse-icon"> 
							</label>
							<label class="col-sm-4 checkbox">
								{!! Form::checkbox('eca_martialarts',1, isset($school->eca_martialarts) && $school->eca_martialarts == 1 ? true : false) !!} 
								<img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_ECA-09.png') }}" class="epinurse-icon"> 
							</label>
						</div>
						<div class="col-sm-offset-2 col-sm-8 eca-oth">
							<label class="col-sm-2 checkbox">
								{!! Form::checkbox('eca_oth',1, isset($school->eca_oth) && $school->eca_oth == 1 ? true : false) !!} Others
							</label>
							<label class="col-sm-2">Specify others: </label>
							<div class="col-sm-8">
								{!! Form::text('eca_are_available_oth_spec',isset($school->eca_are_available_oth_spec) ? $school->eca_are_available_oth_spec : NULL,['class'=>'form-control','placeholder'=>'Specify others']) !!}	
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<div id="firstaid" class="tab-pane fade in">
				<fieldset>
					<legend> First Aid Box </legend>
					<div class="icheck">
						<div class="col-sm-12">
							<label class="col-sm-9 control-label"> Is there a first aid box present in the school </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('first_aid_box',0, (isset($school->first_aid_box) AND $school->first_aid_box == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('first_aid_box',1, (isset($school->first_aid_box) AND $school->first_aid_box == 1) ? true : false) !!} Yes
								</label>
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-9 control-label"> Is the first aid box adequately stocked? </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('first_aid_adequacy',0, (isset($school->first_aid_adequacy) AND $school->first_aid_adequacy == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('first_aid_adequacy',1, (isset($school->first_aid_adequacy) AND $school->first_aid_adequacy == 1) ? true : false) !!} Yes
								</label>
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-9 control-label"> Number of students that can be covered for minor injuries (inflicted in school) using available first aid kits</label>
							<div class="col-sm-3">
								{!! Form::number('first_aid_students',isset($school->first_aid_students) ? $school->first_aid_students : NULL,['class'=>'form-control']) !!}	
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<div id="water" class="tab-pane fade in">
				<fieldset>
					<legend> Drinking Water </legend>
					<div class="icheck">
						<div class="col-sm-12">
							<label class="col-sm-3 control-label"> Type of drinking water supplied </label>
							<div class="col-sm-9">
								<?php $drinks = array('Purified','Bottled','Untreated','None') ?>
								@foreach($drinks as $key => $drink)
									<label class="radio-inline">
										{!! Form::radio('water_type',$key, (isset($school->water_type) AND $school->water_type == $key) ? true : false) !!} {{$drink}}
									</label>
								@endforeach
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-3 control-label"> Availability of drinking water outlets </label>
							<div class="col-sm-9">
								<?php $water_avail = array('High','Moderate','Low') ?>
								@foreach($water_avail as $key => $avail)
									<label class="radio-inline">
										{!! Form::radio('water_availability',$key, (isset($school->water_availability) AND $school->water_availability == $key) ? true : false) !!} {{$avail}}

									</label>
								@endforeach
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<div id="nutrition" class="tab-pane fade in">
				<div class="icheck">
					<fieldset>
						<legend> School Nutrition </legend>
						<div class="col-sm-12">
							<label class="col-sm-4 control-label"> Does the school provide food to the students? </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('nutrition_provided_food',0, (isset($school->nutrition_provided_food) AND $school->nutrition_provided_food == 0) ? true : false, ['class'=>'nutrition_provided_food_radio']) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('nutrition_provided_food',1, (isset($school->nutrition_provided_food) AND $school->nutrition_provided_food == 1) ? true : false, ['class'=>'nutrition_provided_food_radio']) !!} Yes
								</label>
							</div>
						</div>
						<div class="col-sm-12 <?php if(isset($school->nutrition_provided_food) AND $school->nutrition_provided_food == 1) {echo '';} else {echo 'hidden';} ?>" id="meal-spec">
							<label class="col-sm-4 control-label"> If yes, specify types of meal provided </label>
							<div class="col-sm-8">
								<?php
									$checker = isset($school->nutrition_type_meal) ? explode('|',$school->nutrition_type_meal) : array(0,0,0,0);
									$nutrition_type_meal_options = array('Breakfast','Lunch','Snacks','Dinner');
								?>
								@foreach($nutrition_type_meal_options as $key => $option)
									<label class="checkbox-inline">
										{!! Form::checkbox('nutrition_type_meal[]',$key, $checker[$key] == 1 ? true : false) !!} {{ $option }}	
									</label>
								@endforeach
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend> Quality of the food provided </legend>
						<div class="col-sm-12">
							<label class="col-sm-7 control-label"> Is the food provided hygienic and safe? </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('food_hygiene',0, (isset($school->food_hygiene) AND $school->food_hygiene == 0) ? true : false, ['class'=>'food_hygiene_radio']) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('food_hygiene',1, (isset($school->food_hygiene) AND $school->food_hygiene == 1) ? true : false, ['class'=>'food_hygiene_radio']) !!} Yes
								</label>
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-7 control-label"> Is the meal served a balanced diet? </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('food_balanced',0, (isset($school->food_balanced) AND $school->food_balanced == 0) ? true : false, ['class'=>'food_balanced_radio']) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('food_balanced',1, (isset($school->food_balanced) AND $school->food_balanced == 1) ? true : false, ['class'=>'food_balanced_radio']) !!} Yes
								</label>
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-7 control-label"> If food not provided at school, select type of food that can be brought from home </label>
							<div class="col-sm-4">
								<label class="radio-inline">
									{!! Form::radio('food_from_home',0, (isset($school->food_from_home) AND $school->food_from_home == 0) ? true : false, ['class'=>'food_from_home_radio']) !!} Homemade
								</label>
								<label class="radio-inline">
									{!! Form::radio('food_from_home',1, (isset($school->food_from_home) AND $school->food_from_home == 1) ? true : false, ['class'=>'food_from_home_radio']) !!} Processed
								</label>
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-7 control-label"> Is junk food allowed in school premises? </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('food_junk',0, (isset($school->food_junk) AND $school->food_junk == 0) ? true : false, ['class'=>'food_junk_radio']) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('food_junk',1, (isset($school->food_junk) AND $school->food_junk == 1) ? true : false, ['class'=>'food_junk_radio']) !!} Yes
								</label>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
			<div id="toilet" class="tab-pane fade in">
				<fieldset>
					<legend> Toilet Facilities </legend>
						<div class="icheck">
							<div class="col-sm-12">
								<label class="col-sm-3 control-label"> Facilities available </label>
							</div>
							<div class="col-sm-offset-1 col-sm-10">
								<label class="col-sm-1"><img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_Toilet Facilities-01.png') }}" class="epinurse-icon"></label>
								<div class="col-sm-3">
									<label class="radio-inline">
										{!! Form::radio('tf_one',0, (isset($school->tf_one) AND $school->tf_one == 0) ? true : false, ['class'=>'tf_one_radio']) !!} No
									</label>
									<label class="radio-inline">
										{!! Form::radio('tf_one',1, (isset($school->tf_one) AND $school->tf_one == 1) ? true : false, ['class'=>'tf_one_radio']) !!} Yes
									</label>
								</div>
								<label class="col-sm-1"><img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_Toilet Facilities-02.png') }}" class="epinurse-icon"></label>
								<div class="col-sm-3">
									<label class="radio-inline">
										{!! Form::radio('tf_two',0, (isset($school->tf_two) AND $school->tf_two == 0) ? true : false, ['class'=>'tf_two_radio']) !!} No
									</label>
									<label class="radio-inline">
										{!! Form::radio('tf_two',1, (isset($school->tf_two) AND $school->tf_two == 1) ? true : false, ['class'=>'tf_two_radio']) !!} Yes
									</label>
								</div>
								<label class="col-sm-1"><img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_Toilet Facilities-03.png') }}" class="epinurse-icon"></label>
								<div class="col-sm-3">
									<label class="radio-inline">
										{!! Form::radio('tf_three',0, (isset($school->tf_three) AND $school->tf_three == 0) ? true : false, ['class'=>'tf_three_radio']) !!} No
									</label>
									<label class="radio-inline">
										{!! Form::radio('tf_three',1, (isset($school->tf_three) AND $school->tf_three == 1) ? true : false, ['class'=>'tf_three_radio']) !!} Yes
									</label>
								</div>
							</div>
							<div class="col-sm-12">
								<label class="col-sm-3 control-label"> Separation of Toilets </label>
							</div>
							<div class="col-sm-offset-1 col-sm-10">
								<label class="col-sm-1"><img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_Toilet Facilities-04.png') }}" class="epinurse-icon"></label>
								<div class="col-sm-3">
									<label class="radio-inline">
										{!! Form::radio('tf_four',0, (isset($school->tf_four) AND $school->tf_four == 0) ? true : false, ['class'=>'tf_four_radio']) !!} No
									</label>
									<label class="radio-inline">
										{!! Form::radio('tf_four',1, (isset($school->tf_four) AND $school->tf_four == 1) ? true : false, ['class'=>'tf_four_radio']) !!} Yes
									</label>
								</div>
								<label class="col-sm-1"><img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_Toilet Facilities-05.png') }}" class="epinurse-icon"></label>
								<div class="col-sm-3">
									<label class="radio-inline">
										{!! Form::radio('tf_five',0, (isset($school->tf_five) AND $school->tf_five == 0) ? true : false, ['class'=>'tf_five_radio']) !!} No
									</label>
									<label class="radio-inline">
										{!! Form::radio('tf_five',1, (isset($school->tf_five) AND $school->tf_five == 1) ? true : false, ['class'=>'tf_five_radio']) !!} Yes
									</label>
								</div>
								<label class="col-sm-1"><img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_Toilet Facilities-06.png') }}" class="epinurse-icon"></label>
								<div class="col-sm-3">
									<label class="radio-inline">
										{!! Form::radio('tf_six',0, (isset($school->tf_six) AND $school->tf_six == 0) ? true : false, ['class'=>'tf_six_radio']) !!} No
									</label>
									<label class="radio-inline">
										{!! Form::radio('tf_six',1, (isset($school->tf_six) AND $school->tf_six == 1) ? true : false, ['class'=>'tf_six_radio']) !!} Yes
									</label>
								</div>
							</div>
							<div class="col-sm-12">
								<label class="col-sm-3 control-label"> Adequacy </label>
							</div>
							<div class="col-sm-offset-1 col-sm-10">
								<label class="col-sm-1"><img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICONS_Toilet Facilities-07.png') }}" class="epinurse-icon"></label>
								<div class="col-sm-6">
									<label class="radio-inline">
										{!! Form::radio('tf_seven',0, (isset($school->tf_seven) AND $school->tf_seven == 0) ? true : false, ['class'=>'tf_seven_radio']) !!} Adequate
									</label>
									<label class="radio-inline">
										{!! Form::radio('tf_seven',1, (isset($school->tf_seven) AND $school->tf_seven == 1) ? true : false, ['class'=>'tf_seven_radio']) !!} Inadequate
									</label>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-6 control-label"> Rate on a scale of 1 to 5 the cleanliness of the toilets at school </label>
						</div>
						<div class="col-sm-offset-1 col-sm-10">
							<div class="form-inline">
								<label>DIRTY</label>
								<!-- <input type="text" name="toilet_clean_range" id="toilet_clean_range" class="ionrange" value="{{ $school->toilet_clean_range or NULL }}"> -->
								<div class="btn-group toggler" data-toggle="buttons" style="padding: 10px 10px 10px 10px">
									@for($i=1;$i<=5;$i++)
									  <label class="btn btn-default <?php if(isset($school->toilet_clean_range) AND $school->toilet_clean_range == $i) {echo "active";} ?>">
									  	{!! Form::radio('toilet_clean_range',$i,(isset($school->toilet_clean_range) AND $school->toilet_clean_range == $i) ? true : false) !!} {{ $i }}
									  </label>
								  	@endfor
								</div>
								<label>CLEAN</label>
							</div>
						</div>

				</fieldset>
			</div>
			<div id="infrastructure" class="tab-pane fade in">
				<fieldset>
					<legend> Infrastructure </legend>
					<div class="icheck">
						<div class="col-sm-12">
							<label class="col-sm-3 control-label"> Where is the school located? </label>
						</div>
						<div class="form-inline">
							<div class="col-sm-offset-1 col-sm-11">
								<div class="col-sm-12">
									<?php $school_locations = array('EN_ICONS_School Location-01.png','EN_ICONS_School Location-02.png','EN_ICONS_School Location-03.png','EN_ICONS_School Location-04.png'); ?>
									@foreach($school_locations as $key => $loc)
										<label class="radio-inline">
											{!! Form::radio('school_location',$key, (isset($school->school_location) AND $school->school_location == $key) ? true : false, ['class'=>'school_location_radio']) !!} <img src="{{ asset('public/uploads/customizations/epinurse/icons/'.$loc) }}" class="epinurse-icon">
										</label>
									@endforeach
									<label class="radio-inline">
										{!! Form::radio('school_location','4', (isset($school->school_location) AND $school->school_location == '4') ? true : false, ['class'=>'school_location_radio']) !!} Other (specify)
										{!! Form::text('school_location_oth',isset($school->school_location_oth) ? $school->school_location_oth : NULL,['class'=>'form-control']) !!}
									</label>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-3 control-label"> Building Type </label>
						</div>
						<div class="col-sm-offset-1 col-sm-11">
							<div class="col-sm-12">
								<?php $building_types = array('Reinforced concrete','Masonry and stone or red bricks','Clay and sun-dried bricks'); ?>
								@foreach($building_types as $key => $value)
									<label class="radio-inline">
										{!! Form::radio('building_type',$key, (isset($school->building_type) AND $school->building_type == $key) ? true : false, ['class'=>'building_type_radio']) !!} {{ $value }}
									</label>
								@endforeach
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-3 control-label"> Age of Building </label>
							<div class="col-sm-3">
								{!! Form::number('age_building',isset($school->age_building) ? $school->age_building : NULL,['class'=>'form-control']) !!}
							</div>
							<label class="col-sm-3 control-label"> Building Height (in store) </label>
							<div class="col-sm-3">
								{!! Form::text('height_building',isset($school->height_building) ? $school->height_building : NULL,['class'=>'form-control']) !!}
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-3 control-label"> Water Damage? </label>
						</div>
						<div class="col-sm-offset-1 col-sm-11">
							<div class="col-sm-12">
								<?php $water_damages = array('Rainwater leaks from roof inside the building','Interior dampness or odor'); ?>
								@foreach($water_damages as $key => $value)
									<label class="radio-inline">
										{!! Form::radio('water_damage',$key, (isset($school->water_damage) AND $school->water_damage == $key) ? true : false, ['class'=>'water_damage_radio']) !!} {{ $value }}
									</label>
								@endforeach
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-3 control-label"> Fire Fighting Instrument </label>
						</div>
						<div class="col-sm-offset-1 col-sm-11">
							<div class="col-sm-12">
								<label class="radio-inline">
									{!! Form::radio('fire_fighting_instrument',0, (isset($school->fire_fighting_instrument) AND $school->fire_fighting_instrument == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('fire_fighting_instrument',1, (isset($school->fire_fighting_instrument) AND $school->fire_fighting_instrument == 1) ? true : false) !!} Yes
								</label>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<div id="hazard" class="tab-pane fade in">
				<fieldset>
					<legend> Hazard Risk Assessment Matrix </legend>
					<?php
						// Earthquake,Landslide, Wildlife attack, Fire, Pest and disases, Flood, Windstorm, Drought, Hailstorm, Soil erosion, Epidemics
						$hazards = array(
							'earthquake' => 'Earthquake',
							'landslide' => 'Landslide',
							'wildlife_attack' => 'Wildlife Attack',
							'fire' => 'Fire',
							'pest_diseases' => 'Pest and Diseases',
							'flood' => 'Flood',
							'windstorm' => 'Windstorm',
							'drought' => 'Drought',
							'hailstorm' => 'Hailstorm',
							'soil_erosion' => 'Soil Erosion',
							'epidemic' => 'Epidemics'
						);

						$measures = array(
							'hazards' => 'Hazards (likelihood)',
							'vulnerabilities' => 'Vulnerabilities (Impact Security)',
							// 'risk_score_one' => 'Risk Score (Description)',
							// 'risk_score_two' => 'Risk Score (Priority Level Description)' 
						);				
					?>

					@foreach($hazards as $key => $value)
						<div class='eca-activities'>
							<div class="col-sm-12">
								<div class="school-subheader"><strong> {{ $value }} </strong></div>
							</div>
							<div class="col-sm-12">				
								@foreach($measures as $measure_key => $measure_value)
								<div class="col-sm-4">
									<div class="col-sm-12">
										<div class="btn-group toggler" data-toggle="buttons">
											<?php $max = 5; ?>
											@if($measure_key =='vulnerabilities')
												<?php $max = 4; ?>
											@endif
											@for($i=1;$i<=$max;$i++)
											  <?php $field_name = $key.'_'.$measure_key; ?>
											  <label class="btn btn-default <?php if(isset($school->$field_name) AND $school->$field_name == $i) {echo "active";} ?>">
											  	{!! Form::radio($field_name,$i,isset($school->$field_name) && $school->$field_name == $i ? true : false) !!} {{$i}}
											  </label>
										  	@endfor									  	
										</div>
									</div>
									{{ $measure_value }}
								</div>
								@endforeach
								 <?php 
								 	$hazards_field = $key.'_hazards'; $vulnerabilities_field = $key.'_vulnerabilities';				 
								 ?>
								<div class="col-sm-4">
									<label class="control-label">Risk Score:</label><label>@if(($school->$hazards_field >= 0 AND $school->$hazards_field <=5) AND ($school->$vulnerabilities_field >= 0 AND $school->$vulnerabilities_field <=4)) {{ $school->$hazards_field*$school->$vulnerabilities_field }}@endif</label>
								</div>
							</div>	
						</div>
					@endforeach	
					<div class="icheck">
						<div class="col-sm-12">
							<label class="col-sm-4 control-label"> Has the school performed Disaster Drill </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('disaster_drill',0, (isset($school->disaster_drill) AND $school->disaster_drill == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('disaster_drill',1, (isset($school->disaster_drill) AND $school->disaster_drill == 1) ? true : false) !!} Yes
								</label>
							</div>
							<label class="col-sm-2 control-label"> How often? </label>
							<div class="col-sm-3">
								{!! Form::text('dd_how_often',isset($school->dd_how_often) ? $school->dd_how_often : NULL,['class'=>'form-control','placeholder'=>'Indicate how often']) !!}					
							</div>
						</div>	
						<div class="col-sm-12">
							<label class="col-sm-4 control-label"> Any measures for disaster preparedness? </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('disaster_preparedness',0, (isset($school->disaster_preparedness) AND $school->disaster_preparedness == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('disaster_preparedness',1, (isset($school->disaster_preparedness) AND $school->disaster_preparedness == 1) ? true : false) !!} Yes
								</label>
							</div>
							<label class="col-sm-2 control-label"> Specify: </label>
							<div class="col-sm-3">
								{!! Form::text('dp_specify',isset($school->dp_specify) ? $school->dp_specify : NULL,['class'=>'form-control','placeholder'=>'Specify measures']) !!}					
							</div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-4 control-label"> Is school earthquake resistant? </label>
							<div class="col-sm-3">
								<label class="radio-inline">
									{!! Form::radio('earthquake_resistant',0, (isset($school->earthquake_resistant) AND $school->earthquake_resistant == 0) ? true : false) !!} No
								</label>
								<label class="radio-inline">
									{!! Form::radio('earthquake_resistant',1, (isset($school->earthquake_resistant) AND $school->earthquake_resistant == 1) ? true : false) !!} Yes
								</label>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
		</div>

</div>

<div class="mainbuttons col-md-12 textcenter">
    <button type="submit" class="btn btn-success">Save School Assessment</button>
    <a type="button" class="btn btn-default" href='{{ url("/schoolassessment") }}'>Close</a>
</div>

{!! Form::close() !!}