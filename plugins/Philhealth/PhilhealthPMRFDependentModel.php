<?php

namespace Plugins\Philhealth;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class PhilhealthPMRFDependentModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'philhealth_pmrf_dependents';
    protected static $table_name = 'philhealth_pmrf_dependents';
    protected $primaryKey = 'id';
    
}
