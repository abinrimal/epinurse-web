@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<?php if(!isset($population)) { $population = 0; } ?>
<table class="table table-striped table-bordered table-report table-responsive">
    <thead>
        <tr><th colspan="5">DEMOGRAPHIC PROFILE</th></tr>
        <tr><th>Indicators</th><th colspan="3">Number</th><th>Ratio to Population</th></tr>
        <tr><th></th><th>Male</th><th>Female</th><th>Total</th><th></th></tr>
    </thead>
    <tbody>
        <tr>
            <td>Barangay</td>
            <td colspan="2" bgcolor="#A9A9A9"></td>
            <td>{{ $data['TOT_BGY'] or 0 }}</td>
            <td>1 : {{ getPercentage($population,$data['TOT_BGY'],1,0) }}    </td>
        </tr>
        <tr>
            <td>No. of BHS</td>
            <td colspan="2" bgcolor="#A9A9A9"></td>
            <td>{{ $data['TOT_BHS'] or 0 }}</td>
            <td>1 : {{ getPercentage($population,$data['TOT_BHS'],1,0) }}    </td>
        </tr>
        <tr>
            <td>No. of Health Centers </td>
            <td colspan="2" bgcolor="#A9A9A9"></td>
            <td>{{ $data['TOT_HC'] or 0 }}</td>
            <td>1 : {{ getPercentage($population,$data['TOT_HC'],1,0) }}    </td>
        </tr>
        <tr>
            <td>No. of Households</td>
            <td colspan="2" bgcolor="#A9A9A9"></td>
            <td>{{ $data['TOT_HH'] or 0 }}</td>
            <td>1 : {{ getPercentage($population,$data['TOT_HH'],1,0) }}    </td>
        </tr>
        <tr>
            <td>Physicians/Doctors</td>
            <td>{{ $data['MD_M'] or 0 }}</td>
            <td>{{ $data['MD_F'] or 0 }}</td>
            <td>{{ $data['MD_M'] + $data['MD_F'] }}</td>
            <td>1 : {{ getPercentage($population, ($data['MD_M'] + $data['MD_F']),1,0) }}    </td>
        </tr>
        <tr>
            <td>Dentist</td>
            <td>{{ $data['DENT_M'] or 0 }}</td>
            <td>{{ $data['DENT_F'] or 0 }}</td>
            <td>{{ $data['DENT_M'] + $data['DENT_F'] }}</td>
            <td>1 : {{ getPercentage($population, ($data['DENT_M'] + $data['DENT_F']),1,0) }}    </td>
        </tr>
        <tr>
            <td>Nurses</td>
            <td>{{ $data['PHN_M'] or 0 }}</td>
            <td>{{ $data['PHN_F'] or 0 }}</td>
            <td>{{ $data['PHN_M'] + $data['PHN_F'] }}</td>
            <td>1 : {{ getPercentage($population,($data['PHN_M'] +  $data['PHN_F']),1,0) }}    </td>
        </tr>
        <tr>
            <td>Midwives</td>
            <td>{{ $data['MIDW_M'] or 0 }}</td>
            <td>{{ $data['MIDW_F'] or 0 }}</td>
            <td>{{ $data['MIDW_M'] +  $data['MIDW_F'] }}</td>
            <td>1 : {{ getPercentage($population,($data['MIDW_M'] +  $data['MIDW_F']),1,0) }}    </td>
        </tr>
        <tr>
            <td>Medical Technologists</td>
            <td>{{ $data['MEDT_M'] or 0 }}</td>
            <td>{{ $data['MEDT_F'] or 0 }}</td>
            <td>{{ $data['MEDT_M'] +  $data['MEDT_F'] }}</td>
            <td>1 : {{ getPercentage($population,($data['MEDT_M'] +  $data['MEDT_F']),1,0) }}    </td>
        </tr>
        <tr>
            <td>Sanitary Engineers</td>
            <td>{{ $data['SE_M'] or 0 }}</td>
            <td>{{ $data['SE_F'] or 0 }}</td>
            <td>{{ $data['SE_M'] + $data['SE_F'] }}</td>
            <td>1 : {{ getPercentage($population,($data['SE_M'] + $data['SE_F']),1,0) }}    </td>
        </tr>
        <tr>
            <td>Sanitary Inspectors</td>
            <td>{{ $data['SI_M'] or 0 }}</td>
            <td>{{ $data['SI_F'] or 0 }}</td>
            <td>{{ $data['SI_M'] + $data['SI_F'] }}</td>
            <td>1 : {{ getPercentage($population,($data['SI_M'] + $data['SI_F']),1,0) }}    </td>
        </tr>
        <tr>
            <td>Nutritionist</td>
            <td>{{ $data['NUTR_M'] or 0 }}</td>
            <td>{{ $data['NUTR_F'] or 0 }}</td>
            <td>{{ $data['NUTR_M'] + $data['NUTR_F'] }}</td>
            <td>1 : {{ getPercentage($population,($data['NUTR_M'] + $data['NUTR_F']),1,0) }}    </td>
        </tr>
        <tr>
            <td>Active Barangay Health Workers</td>
            <td>{{ $data['BHW_M'] or 0 }}</td>
            <td>{{ $data['BHW_F'] or 0 }}</td>
            <td>{{ $data['BHW_M'] + $data['BHW_F'] }}</td>
            <td>1 : {{ getPercentage($population,($data['BHW_M'] + $data['BHW_F']),1,0) }}    </td>
        </tr>
    </tbody>
</table>
@yield('modal-footer')   
