<?php
namespace ShineOS\Core\Reports\Entities;

use Shine\Libraries\FacilityHelper;
use ShineOS\Core\Patients\Entities\Patients;
use Plugins\MaternalCare\MaternalCareModel;
use App\Libraries\CSSColors;
use DB, Input, DateTime, Session;
use Illuminate\Database\Eloquent\Model;

class M1 extends Model {

    /**
     *  REUSABLE SCOPE HERE
     */

    public static function scopeDeliveries($tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.`created_at`');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'h.`barangay`');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count' FROM `maternalcare_delivery` a
                JOIN maternalcare b ON b.`maternalcase_id` = a.`maternalcase_id`
                JOIN healthcare_services c ON c.`healthcareservice_id` = b.`healthcareservice_id`
                JOIN facility_patient_user d ON d.`facilitypatientuser_id` = c.`facilitypatientuser_id`
                JOIN facility_user e ON e.`facilityuser_id` = d.`facilityuser_id`
                JOIN facilities f ON f.`facility_id` = e.`facility_id`
                JOIN patients g ON g.`patient_id` = d.`patient_id`
                JOIN patient_contact h ON h.`patient_id` = g.`patient_id`
                WHERE e.`facility_id` = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.`created_at`) = '$year'
                GROUP BY e.`facility_id`";
//dd($sql);
        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeIronSup($tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT g.facility_id, COUNT(*) as 'count' FROM `maternalcare_supplements` a
                JOIN maternalcare_postpartum b ON b.postpartum_id = a.postpartum_id
                JOIN maternalcare d ON d.maternalcase_id = b.maternalcase_id
                JOIN healthcare_services e ON e.healthcareservice_id = d.healthcareservice_id
                JOIN facility_patient_user f ON f.facilitypatientuser_id = e.facilitypatientuser_id
                JOIN facility_user g ON g.facilityuser_id = f.facilityuser_id
                JOIN facilities h ON h.facility_id = g.facility_id
                JOIN patients i ON i.patient_id = f.patient_id
                JOIN patient_contact j ON j.patient_id = i.patient_id
                WHERE g.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND `supplement_type` = 2
                AND TIMESTAMPDIFF(YEAR,i.birthdate,a.created_at) > 10 AND TIMESTAMPDIFF(YEAR,i.birthdate,a.created_at) < 49
                GROUP BY g.facility_id";

        $month = DB::select( DB::raw( $sql ));
        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }


    public static function scopeBFeeding($tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'h.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count' FROM `maternalcare_delivery` a
                JOIN maternalcare_postpartum x ON x.maternalcase_id = a.maternalcase_id
                JOIN maternalcare b ON b.`maternalcase_id` = a.`maternalcase_id`
                JOIN healthcare_services c ON c.healthcareservice_id = b.healthcareservice_id
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN patients g ON g.patient_id = d.patient_id
                JOIN patient_contact h ON h.patient_id = g.patient_id
                WHERE e.facility_id = '$facility->facility_id'
                AND HOUR(x.created_at) - HOUR(a.`created_at`) < 1
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));
        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopePPVitA($tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count' FROM `maternalcare_supplements` a
                JOIN maternalcare_postpartum b ON b.postpartum_id = a.postpartum_id
                JOIN maternalcare d ON d.maternalcase_id = b.maternalcase_id
                JOIN healthcare_services e ON e.healthcareservice_id = d.healthcareservice_id
                JOIN facility_patient_user f ON f.facilitypatientuser_id = e.facilitypatientuser_id
                JOIN facility_user g ON g.facilityuser_id = f.facilityuser_id
                JOIN facilities h ON h.facility_id = g.facility_id
                JOIN patients i ON i.patient_id = f.patient_id
                JOIN patient_contact j ON j.patient_id = i.patient_id
                WHERE g.facility_id = '$facility->facility_id'
                AND `supplement_type` = 1
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                GROUP BY g.facility_id";

        $month = DB::select( DB::raw( $sql ));
        $t = 0;
        if($month) {
            $t = $month[0]->count;
        }

        return $t;
    }

    public static function scopePPIron($tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count' FROM `maternalcare_supplements` a
                JOIN maternalcare_postpartum b ON b.postpartum_id = a.postpartum_id
                JOIN maternalcare d ON d.maternalcase_id = b.maternalcase_id
                JOIN healthcare_services e ON e.healthcareservice_id = d.healthcareservice_id
                JOIN facility_patient_user f ON f.facilitypatientuser_id = e.facilitypatientuser_id
                JOIN facility_user g ON g.facilityuser_id = f.facilityuser_id
                JOIN facilities h ON h.facility_id = g.facility_id
                JOIN patients i ON i.patient_id = f.patient_id
                JOIN patient_contact j ON i.patient_id = j.patient_id
                WHERE g.facility_id = '$facility->facility_id'
                AND `supplement_type` = 2
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                GROUP BY g.facility_id";

        $month = DB::select( DB::raw( $sql ));
        $t = 0;
        if($month) {
            $t = $month[0]->count;
        }

        return $t;
    }

    public static function scopePP2V($tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT g.facility_id, a.maternalcase_id, count(*) as 'count' FROM `maternalcare_visits` a
                JOIN maternalcare_postpartum b ON b.postpartum_id = a.postpartum_id
                JOIN maternalcare d ON d.maternalcase_id = b.maternalcase_id
                JOIN healthcare_services e ON e.healthcareservice_id = d.healthcareservice_id
                JOIN facility_patient_user f ON f.facilitypatientuser_id = e.facilitypatientuser_id
                JOIN facility_user g ON g.facilityuser_id = f.facilityuser_id
                JOIN facilities h ON h.facility_id = g.facility_id
                JOIN patients i ON i.patient_id = f.patient_id
                JOIN patient_contact j ON j.patient_id = i.patient_id
                WHERE a.`postpartum_id` IS NOT NULL
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                GROUP BY g.facility_id, a.maternalcase_id
                HAVING COUNT(*) >= 2";

        $month = DB::select( DB::raw( $sql ));
        $t = 0;
        if($month) {
            foreach ($month as $key => $value) {
                $t += 1;
            }
        }

        return $t;
    }

    public static function scopePreIron($tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count' FROM `maternalcare_supplements` a
                JOIN maternalcare_prenatal b ON b.prenatal_id = a.prenatal_id
                JOIN maternalcare d ON d.maternalcase_id = b.maternalcase_id
                JOIN healthcare_services e ON e.healthcareservice_id = d.healthcareservice_id
                JOIN facility_patient_user f ON f.facilitypatientuser_id = e.facilitypatientuser_id
                JOIN facility_user g ON g.facilityuser_id = f.facilityuser_id
                JOIN facilities h ON h.facility_id = g.facility_id
                JOIN patients i ON i.patient_id = f.patient_id
                JOIN patient_contact j ON j.patient_id = i.patient_id
                WHERE g.facility_id = '$facility->facility_id'
                AND `supplement_type` = 2
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                GROUP BY g.facility_id";

        $month = DB::select( DB::raw( $sql ));
        $t = 0;
        if($month) {
            $t = $month[0]->count;
        }

        return $t;
    }

    public static function scopeTT2($tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'c.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT (
                    SELECT g.facility_id
                    FROM healthcare_services e
                    JOIN facility_patient_user f ON f.facilitypatientuser_id = e.facilitypatientuser_id
                    JOIN facility_user g ON g.facilityuser_id = f.facilityuser_id
                    JOIN facilities h ON h.facility_id = g.facility_id
                    WHERE e.healthcareservice_id = a.healthcareservice_id AND h.facility_id = '$facility->facility_id') as 'facid',
                    a.`subservice_id`
                FROM `patient_immunization` a
                JOIN patients b ON a.patient_id = b.patient_id
                JOIN patient_contact c ON b.patient_id = c.patient_id
                WHERE a.`immun_type` > 1
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                GROUP BY 'facid', a.`subservice_id`";

        $month = DB::select( DB::raw( $sql ));
        $t = 0;
        if($month) {
            $t = count($month);
        }

        return $t;
    }

    public static function scopeTT2x($tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'c.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT
                    (SELECT g.facility_id FROM healthcare_services e
                        JOIN facility_patient_user f ON f.facilitypatientuser_id = e.facilitypatientuser_id
                        JOIN facility_user g ON g.facilityuser_id = f.facilityuser_id
                        JOIN facilities h ON h.facility_id = g.facility_id WHERE e.healthcareservice_id = a.healthcareservice_id AND h.facility_id = '$facility->facility_id') as 'facid',
                    a.`subservice_id`,
                    count(*) as 'count'
                FROM `patient_immunization` a
                JOIN patients b ON a.patient_id = b.patient_id
                JOIN patient_contact c ON b.patient_id = c.patient_id
                WHERE YEAR(a.created_at) = $year
                " . $monthfilter . "
                " . $brgyfilter . "
                GROUP BY 'facid', a.`subservice_id`
                HAVING 1 < count(*)";

        $month = DB::select( DB::raw( $sql ));
        $t = 0;
        if($month) {
            $t = count($month);
        }

        return $t;
    }

    public static function scopePre4Visit($tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'g.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT
                    (SELECT g.facility_id FROM healthcare_services e
                        JOIN facility_patient_user f ON f.facilitypatientuser_id = e.facilitypatientuser_id
                        JOIN facility_user g ON g.facilityuser_id = f.facilityuser_id
                        JOIN facilities h ON h.facility_id = g.facility_id WHERE e.healthcareservice_id = b.healthcareservice_id AND h.facility_id = '$facility->facility_id') as 'facid',
                        c.prenatal_id, count(*)
                FROM `maternalcare_visits` a
                JOIN maternalcare b ON b.maternalcase_id = a.maternalcase_id
                JOIN maternalcare_prenatal c ON c.maternalcase_id = b.maternalcase_id
                JOIN healthcare_services d ON b.healthcareservice_id = d.healthcareservice_id
                JOIN facility_patient_user e ON e.facilitypatientuser_id = d.facilitypatientuser_id
                JOIN patients f ON f.patient_id = e.patient_id
                JOIN patient_contact g ON g.patient_id = f.patient_id
                WHERE YEAR(a.created_at) = $year
                " . $monthfilter . "
                " . $brgyfilter . "
                GROUP BY c.prenatal_id
                HAVING 3 < count(*)";

        $month = DB::select( DB::raw( $sql ));
        $t = 0;
        if($month) {
            $t = count($month);
        }

        return $t;
    }

    public static function scopeCCare($type, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'f.barangay');

        switch($type) {
            case 'BCG': $col = 'bcg_actual_date'; break;
            case 'DPT1': $col = 'dpt1_actual_date'; break;
            case 'DPT2': $col = 'dpt2_actual_date'; break;
            case 'DPT3': $col = 'dpt3_actual_date'; break;
            case 'HPB1_win24': $col = 'hepa_b1_win24_actual_date'; break;
            case 'HPB1_aft24': $col = 'hepa_b1_aft24_actual_date'; break;
            case 'HPB2': $col = 'hepa_b2_actual_date'; break;
            case 'HPB3': $col = 'hepa_b3_actual_date'; break;
            case 'MEAS': $col = 'measles_actual_date'; break;
            case 'OPV1': $col = 'opv1_actual_date'; break;
            case 'OPV2': $col = 'opv2_actual_date'; break;
            case 'OPV3': $col = 'opv3_actual_date'; break;
            case 'PENTA1': $col = 'penta1_actual_date'; break;
            case 'PENTA2': $col = 'penta2_actual_date'; break;
            case 'PENTA3': $col = 'penta3_actual_date'; break;
            case 'ROTA1': $col = 'rota1_actual_date'; break;
            case 'ROTA2': $col = 'rota2_actual_date'; break;
            case 'ROTA3': $col = 'rota3_actual_date'; break;
            case 'PCV1': $col = 'pcv1_actual_date'; break;
            case 'PCV2': $col = 'pcv2_actual_date'; break;
            case 'PCV3': $col = 'pcv3_actual_date'; break;
            case 'MCV1': $col = 'mcv1_actual_date'; break;
            case 'MCV2': $col = 'mcv2_actual_date'; break;
        }

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) AS 'counter' FROM pediatrics_service a
                JOIN healthcare_services b ON b.healthcareservice_id = a.healthcareservice_id
                JOIN facility_patient_user c ON c.facilitypatientuser_id = b.facilitypatientuser_id
                JOIN facility_user d ON d.facilityuser_id = c.facilityuser_id
                JOIN patients e ON c.patient_id = e.patient_id
                JOIN patient_contact f ON c.patient_id = f.patient_id
                WHERE a.$col IS NOT NULL
                AND e.gender = '$sex'
                AND YEAR(a.created_at) = $year
                AND d.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter;

        $month = DB::select( DB::raw( $sql ));
        $t = 0;
        if($month) {
            $t = $month[0]->counter;
        }

        return $t;
    }

    public static function scopeFullImmune($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) AS 'counter' FROM `pediatrics_service` a
                JOIN healthcare_services b ON b.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user f ON f.facilitypatientuser_id = b.facilitypatientuser_id
                JOIN facility_user g ON g.facilityuser_id = f.facilityuser_id
                JOIN facilities h ON h.facility_id = g.facility_id
                JOIN patients i ON i.patient_id = f.patient_id
                JOIN patient_contact j ON j.patient_id = i.patient_id
                WHERE a.bcg_actual_date IS NOT NULL
                AND a.opv1_actual_date IS NOT NULL
                AND a.opv2_actual_date IS NOT NULL
                AND a.opv3_actual_date IS NOT NULL
                AND a.pcv1_actual_date IS NOT NULL
                AND a.pcv2_actual_date IS NOT NULL
                AND a.pcv3_actual_date IS NOT NULL
                AND a.measles_actual_date IS NOT NULL
                AND TIMESTAMPDIFF(YEAR,i.birthdate,a.created_at) < 1
                AND i.gender = '$sex'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND h.facility_id = '$facility->facility_id'";

        $month = DB::select( DB::raw( $sql ));
        $t = 0;
        if($month) {
            $t = $month[0]->counter;
        }

        return $t;
    }

    public static function scopeCompleteImmune($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) 'counter' FROM `pediatrics_service` a
                JOIN healthcare_services b ON b.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user f ON f.facilitypatientuser_id = b.facilitypatientuser_id
                JOIN facility_user g ON g.facilityuser_id = f.facilityuser_id
                JOIN facilities h ON h.facility_id = g.facility_id
                JOIN patients i ON i.patient_id = f.patient_id
                JOIN patient_contact j ON j.patient_id = i.patient_id
                WHERE a.bcg_actual_date IS NOT NULL
                AND a.opv1_actual_date IS NOT NULL
                AND a.opv2_actual_date IS NOT NULL
                AND a.opv3_actual_date IS NOT NULL
                AND a.pcv1_actual_date IS NOT NULL
                AND a.pcv2_actual_date IS NOT NULL
                AND a.pcv3_actual_date IS NOT NULL
                AND a.measles_actual_date IS NOT NULL
                AND TIMESTAMPDIFF(YEAR,i.birthdate,a.created_at) >= 1
                AND TIMESTAMPDIFF(YEAR,i.birthdate,a.created_at) < 2
                AND i.gender = '$sex'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND h.facility_id = '$facility->facility_id'
                GROUP BY g.facility_id";

        $month = DB::select( DB::raw( $sql ));
        $t = 0;
        if($month) {
            $t = $month[0]->counter;
        }

        return $t;
    }

    public static function scopeLiveBirth($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count' FROM `maternalcare_delivery` a
                JOIN maternalcare b ON b.`maternalcase_id` = a.`maternalcase_id`
                JOIN healthcare_services c ON c.healthcareservice_id = b.healthcareservice_id
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN patients g ON g.patient_id = d.patient_id
                JOIN patient_contact j ON j.patient_id = g.patient_id
                WHERE e.facility_id = '$facility->facility_id'
                AND a.`termination_outcome` = 'LB'
                AND g.gender = '$sex'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeChildProtect($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count' FROM maternalcare_delivery a
                JOIN patient_immunization b ON b.subservice_id = a.maternalcase_id
                JOIN healthcare_services c ON c.healthcareservice_id = b.healthcareservice_id
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN patients g ON g.patient_id = d.patient_id
                JOIN patient_contact j ON j.patient_id = g.patient_id
                WHERE e.facility_id = '$facility->facility_id'
                AND b.immunization_code = 'tetanus'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND g.gender = '$sex'
                GROUP BY a.`maternalcase_id`
                HAVING 2 < count(*)";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeSixMonthSeen($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count' FROM `pediatrics_service` a
                JOIN `healthcare_services` c ON c.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.`patient_id` = d.`patient_id`
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE TIMESTAMPDIFF(MONTH, b.birthdate, a.updated_at) = 6
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeBreastFeed($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `pediatrics_service` a
                JOIN `healthcare_services` c ON c.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN patients g ON g.patient_id = d.patient_id
                JOIN patient_contact j ON j.patient_id = d.patient_id
                WHERE a.`is_breastfed_first_month` = 1
                AND a.`is_breastfed_second_month` = 1
                AND a.`is_breastfed_third_month` = 1
                AND a.`is_breastfed_fourth_month` = 1
                AND a.`is_breastfed_fifth_month` = 1
                AND a.`is_breastfed_sixth_month` = 1
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND g.gender = '$sex'";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }


    public static function scopeCompFood($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');
        $sql = "SELECT count(*) as 'count'
                FROM `pediatrics_service` a
                JOIN `patients` b ON b.`patient_id` = a.`patient_id`
                JOIN `healthcare_services` c ON c.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE a.`complimentary_food_sixth` IS NOT NULL
                AND a.`complimentary_food_seventh` IS NOT NULL
                AND a.`complimentary_food_eight` IS NOT NULL
                AND TIMESTAMPDIFF(MONTH, b.birthdate, a.`complimentary_food_eight`) <= 8
                AND TIMESTAMPDIFF(MONTH, b.birthdate, a.`complimentary_food_eight`) >= 6
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeNBornRef($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `pediatrics_service` a
                JOIN `patients` b ON b.`patient_id` = a.`patient_id`
                JOIN `healthcare_services` c ON c.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE a.`newborn_screening_referral_date` IS NOT NULL
                AND TIMESTAMPDIFF(MONTH, b.birthdate, a.`newborn_screening_referral_date`) < 12
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeNBornDone($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `pediatrics_service` a
                JOIN `patients` b ON b.`patient_id` = a.`patient_id`
                JOIN `healthcare_services` c ON c.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE a.`newborn_screening_actual_date` IS NOT NULL
                AND TIMESTAMPDIFF(MONTH, b.birthdate, a.`newborn_screening_actual_date`) < 12
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeVitAFirst($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `pediatrics_service` a
                JOIN `patients` b ON b.`patient_id` = a.`patient_id`
                JOIN `healthcare_services` c ON c.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE a.`vit_a_supp_first_date` IS NOT NULL
                AND a.`vit_a_first_age` <= 11
                AND a.`vit_a_first_age` >= 6
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeVitASecond($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `pediatrics_service` a
                JOIN `patients` b ON b.`patient_id` = a.`patient_id`
                JOIN `healthcare_services` c ON c.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE a.`vit_a_supp_first_date` IS NOT NULL
                AND a.`vit_a_supp_second_date` IS NOT NULL
                AND a.`vit_a_second_age` <= 59
                AND a.`vit_a_second_age` >= 12
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeIronA($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `pediatrics_service` a
                JOIN `patients` b ON b.`patient_id` = a.`patient_id`
                JOIN `healthcare_services` c ON c.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE a.`iron_supp_start_date` IS NOT NULL
                AND a.`vit_a_first_age` <= 11
                AND a.`vit_a_first_age` >= 6
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeIronB($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `pediatrics_service` a
                JOIN `patients` b ON b.`patient_id` = a.`patient_id`
                JOIN `healthcare_services` c ON c.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE a.`iron_supp_start_date` IS NOT NULL
                AND a.`vit_a_second_age` <= 59
                AND a.`vit_a_second_age` >= 12
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeMNPA($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `pediatrics_service` a
                JOIN `patients` b ON b.`patient_id` = a.`patient_id`
                JOIN `healthcare_services` c ON c.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE a.`mnp_6_11` IS NOT NULL
                AND TIMESTAMPDIFF(MONTH, b.birthdate, a.`mnp_6_11`) <= 11
                AND TIMESTAMPDIFF(MONTH, b.birthdate, a.`mnp_6_11`) >= 6
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeMNPB($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `pediatrics_service` a
                JOIN `patients` b ON b.`patient_id` = a.`patient_id`
                JOIN `healthcare_services` c ON c.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE a.`mnp_12_23` IS NOT NULL
                AND TIMESTAMPDIFF(MONTH, b.birthdate, a.`mnp_12_23`) <= 23
                AND TIMESTAMPDIFF(MONTH, b.birthdate, a.`mnp_12_23`) >= 12
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeDeWorm($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `medicalorder` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `medicalorder_prescription` g ON g.medicalorder_id = c.medicalorder_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.medicalorder_type = 'MO_MED_PRESCRIPTION'
                AND (g.generic_name LIKE '%Pyrantel%'
                OR g.generic_name LIKE '%Praziquantel%'
                OR g.generic_name LIKE '%Albendazole%')
                AND TIMESTAMPDIFF(MONTH, b.birthdate, a.`created_at`) <= 59
                AND TIMESTAMPDIFF(MONTH, b.birthdate, a.`created_at`) >= 12
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(a.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeSickSeen($aa, $ab, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN `vital_physical` h ON h.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE (c.diagnosislist_id LIKE '%Severe Pneumonia%'
                OR c.diagnosislist_id LIKE '%Persistent Diarrhea%'
                OR c.diagnosislist_id LIKE '%Measles%'
                OR h.weight <= 2.5)
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) <= $ab
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) >= $aa
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeSickVitA($aa, $ab, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN `vital_physical` h ON h.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `pediatrics_service` g ON a.healthcareservice_id = g.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE (c.diagnosislist_id LIKE '%Severe Pneumonia%'
                OR c.diagnosislist_id LIKE '%Persistent Diarrhea%'
                OR c.diagnosislist_id LIKE '%Measles%'
                OR h.weight <= 2.5)
                AND g.`vit_a_supp_first_date` IS NOT NULL
                AND g.`vit_a_supp_second_date` IS NOT NULL
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) <= $ab
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) >= $aa
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeLowWt($aa, $ab, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT e.facility_id, count(*) as 'count'
                FROM vital_physical a
                JOIN `healthcare_services` c ON c.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE a.weight <= 2.5
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) <= $ab
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) >= $aa
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeLowWtIron($aa, $ab, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT e.facility_id, count(*) as 'count'
                FROM vital_physical a
                JOIN `healthcare_services` c ON c.`healthcareservice_id` = a.`healthcareservice_id`
                LEFT JOIN `pediatrics_service` g ON g.`healthcareservice_id` = a.`healthcareservice_id`
                JOIN facility_patient_user d ON d.facilitypatientuser_id = c.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE a.weight <= 2.5
                AND MONTH(g.iron_supp_start_date) = MONTH(c.`created_at`)
                AND YEAR(g.iron_supp_start_date) = YEAR(c.`created_at`)
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) <= $ab
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) >= $aa
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeAnemia($aa, $ab, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.diagnosislist_id LIKE '%Anemia%'
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) <= $ab
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) >= $aa
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeAnemiaWithIron($aa, $ab, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $monthfilter_2 = self::getMonthStr($tmp, $month, 'g.iron_supp_end_date');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                LEFT JOIN `pediatrics_service` g ON a.healthcareservice_id = g.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.diagnosislist_id LIKE '%Anemia%'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND g.iron_supp_end_date IS NOT NULL
                " . $monthfilter_2 . "
                AND YEAR(g.iron_supp_end_date) = $year
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) <= $ab
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) >= $aa
                AND e.facility_id = '$facility->facility_id'
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeDiarrhea($aa, $ab, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.diagnosislist_id LIKE '%Diarrhea%'
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) <= $ab
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) >= $aa
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeDiarrheaWtORS($aa, $ab, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $monthfilter_2 = self::getMonthStr($tmp, $month, 'h.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `medicalorder` g ON g.healthcareservice_id = a.healthcareservice_id
                JOIN `medicalorder_prescription` h ON h.medicalorder_id = g.medicalorder_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE g.medicalorder_type = 'MO_MED_PRESCRIPTION'
                AND c.diagnosislist_id LIKE '%Diarrhea%'
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) <= $ab
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) >= $aa
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                " . $monthfilter_2 . "
                AND YEAR(h.created_at) = $year
                AND h.generic_name LIKE '%Oral Rehydration Salts%'
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeDiarrheaWtORSZinc($aa, $ab, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $monthfilter_2 = self::getMonthStr($tmp, $month, 'h.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `medicalorder` g ON g.healthcareservice_id = a.healthcareservice_id
                JOIN `medicalorder_prescription` h ON h.medicalorder_id = g.medicalorder_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE g.medicalorder_type = 'MO_MED_PRESCRIPTION'
                AND c.diagnosislist_id LIKE '%Diarrhea%'
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) <= $ab
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) >= $aa
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                " . $monthfilter_2 . "
                AND YEAR(h.created_at) = $year
                AND h.generic_name LIKE '%Oral Rehydration Salts%'
                AND h.generic_name LIKE '%Zinc%'
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }


    public static function scopePneumonia($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.diagnosislist_id LIKE '%Pneumonia%'
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) <= 59
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) >= 0
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopePneumoniaTreat($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `medicalorder` g ON g.healthcareservice_id = a.healthcareservice_id
                JOIN `medicalorder_prescription` h ON h.medicalorder_id = g.medicalorder_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE g.medicalorder_type = 'MO_MED_PRESCRIPTION'
                AND c.diagnosislist_id LIKE '%Pneumonia%'
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) <= 59
                AND TIMESTAMPDIFF(MONTH, b.birthdate, c.`created_at`) >= 0
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeMalariaLessFive($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.diagnosislist_id LIKE '%Malaria%'
                AND TIMESTAMPDIFF(YEAR, b.birthdate, c.`created_at`) < 5
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeMalariaGreaterFive($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.diagnosislist_id LIKE '%Malaria%'
                AND TIMESTAMPDIFF(YEAR, b.birthdate, c.`created_at`) >= 5
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeMalariaPregnant($tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $monthfilter_2 = self::getMonthStr($tmp, $month, 'h.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `maternalcare` g ON a.healthcareservice_id = g.healthcareservice_id
                JOIN `maternalcare_visits` h ON g.maternalcase_id = h.maternalcase_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.diagnosislist_id LIKE '%Malaria%'
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                " . $monthfilter_2 . "
                AND YEAR(h.created_at) = $year
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeMalariaBySpecies($species, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.diagnosislist_id LIKE '%Malaria%'
                AND c.diagnosislist_id LIKE '%$species%'
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        DB::enableQueryLog();

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeMalariaDeaths($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $monthfilter_2 = self::getMonthStr($tmp, $month, 'g.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `disposition` g ON a.healthcareservice_id = g.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.diagnosislist_id LIKE '%Malaria%'
                AND g.discharge_condition LIKE '%DIED%'
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                " . $monthfilter_2 . "
                AND YEAR(g.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY b.patient_id,e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeTB($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `tuberculosis_record` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeTBDSSM($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $monthfilter_2 = self::getMonthStr($tmp, $month, 'g.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `tuberculosis_record` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `tuberculosis_dssm_record` g ON g.tb_record_number = c.tb_record_number
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                " . $monthfilter_2 . "
                AND YEAR(g.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY b.patient_id, e.facility_id";

        $month = DB::select( DB::raw( $sql ));
        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeTBDSSMPos($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `tuberculosis_record` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `tuberculosis_dssm_record` g ON g.tb_record_number = c.tb_record_number
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE g.result LIKE '%positive%'
                AND c.bacteriology_registration_group IS NULL
                AND g.category_of_treatment IS NOT NULL
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeTBDSSMPosRegistered($type, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `tuberculosis_record` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `tuberculosis_dssm_record` g ON g.tb_record_number = c.tb_record_number
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE g.result LIKE '%positive%'";

        if($type == 'ALL') {
            $sql .= " AND c.bacteriology_registration_group IS NOT NULL";
        }
        elseif($type == 'RAD') {
            $sql .= " AND c.bacteriology_registration_group IN ('TALF','PTOU')";
        }
        else {
            $sql .= " AND c.bacteriology_registration_group = '" . $type . "'";
        }

        $sql .= " AND g.category_of_treatment IS NOT NULL
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeTBDSSMPosCuredByType($type, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.updated_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `tuberculosis_record` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `tuberculosis_dssm_record` g ON g.tb_record_number = c.tb_record_number
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE g.result LIKE '%positive%'";

        if($type == 'ALL')
        {
            $sql .= "AND c.bacteriology_registration_group IS NOT NULL";
        }
        elseif($type == 'RAD')
        {
            $sql .= "AND c.bacteriology_registration_group IN ('TALF','PTOU')";
        }
        elseif($type == 'ALLRETREATMENT')
        {
            $sql .= "AND c.bacteriology_registration_group IN ('R', 'TAF', 'TALF','PTOU')";
        }
        else{
            $sql .= "AND c.bacteriology_registration_group ='" . $type . "'";
        }

        $sql .= " AND c.treatment_outcome_result = 'C'
                AND g.category_of_treatment IS NOT NULL
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.updated_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeSchistosomiasisExamined($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $monthfilter = self::getMonthStr($tmp, $month, 'i.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `medicalroder` g ON a.healthcareservice_id = g.healthcareservice_id
                JOIN `medicalorder_laboratoryexam` h ON g.medicalorder_id = h.medicalorder_id
                JOIN `laboratory_result` i ON h.medicalorderlaboratoryexam_id = i.medicalorderlaboratoryexam_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.diagnosislist_id LIKE '%Schistosomiasis%'
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                " . $monthfilter_2 . "
                AND YEAR(i.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }


    public static function scopeSchistosomiasisByIntensity($intensity, $sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.diagnosislist_id LIKE '%$intensity%'
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeSchistosomiasisWtTreatment($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $monthfilter_2 = self::getMonthStr($tmp, $month, 'h.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `medicalorder` g ON g.healthcareservice_id = a.healthcareservice_id
                JOIN `medicalorder_prescription` h ON h.medicalorder_id = g.medicalorder_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE g.medicalorder_type = 'MO_MED_PRESCRIPTION'
                AND c.diagnosislist_id LIKE '%Schistosomiasis%'
                AND h.generic_name LIKE '%Praziquantel%'
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                " . $monthfilter_2 . "
                AND YEAR(h.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeSchistosomiasisComplicated($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'c.created_at');
        $monthfilter_2 = self::getMonthStr($tmp, $month, 'g.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `disposition` g ON a.healthcareservice_id = g.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.diagnosislist_id LIKE '%Schistosomiasis%'
                AND g.disposition LIKE '%REFER%'
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                " . $monthfilter_2 . "
                AND YEAR(g.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function scopeSchistosomiasisComplicatedReferred($sex, $tmp, $month, $year, $brgycode = NULL)
    {
        $monthfilter = self::getMonthStr($tmp, $month, 'a.created_at');
        $monthfilter_2 = self::getMonthStr($tmp, $month, 'g.created_at');
        $brgyfilter = self::getByBrgyCode($tmp, $brgycode, 'j.barangay');

        $facility = Session::get('facility_details');

        $sql = "SELECT count(*) as 'count'
                FROM `healthcare_services` a
                JOIN facility_patient_user d ON d.facilitypatientuser_id = a.facilitypatientuser_id
                JOIN facility_user e ON e.facilityuser_id = d.facilityuser_id
                JOIN facilities f ON f.facility_id = e.facility_id
                JOIN `patients` b ON b.patient_id = d.patient_id
                JOIN `diagnosis` c ON c.healthcareservice_id = a.healthcareservice_id
                JOIN `disposition` g ON a.healthcareservice_id = g.healthcareservice_id
                JOIN patient_contact j ON j.patient_id = b.patient_id
                WHERE c.diagnosislist_id LIKE '%Schistosomiasis%'
                AND g.disposition LIKE '%REFER%'
                AND e.facility_id = '$facility->facility_id'
                " . $monthfilter . "
                " . $brgyfilter . "
                AND YEAR(c.created_at) = $year
                " . $monthfilter_2 . "
                AND YEAR(g.created_at) = $year
                AND b.gender = '$sex'
                GROUP BY e.facility_id";

        $month = DB::select( DB::raw( $sql ));

        if($month) {
            return $month[0]->count;
        } else {
            return 0;
        }
    }

    public static function getMonthStr($tmp, $month, $variable, $brgycode = NULL, $brgyvariable = NULL)
    {
        $monthfilter = '';
        switch ($tmp) {
            case 'm':
                $monthfilter .= "AND MONTH(" . $variable . ") = " . $month;

                break;
            case 'q':
                switch ($month) {
                    case 3: $monthfilter .= "AND MONTH(" . $variable . ") BETWEEN 00 AND 03"; break;
                    case 6: $monthfilter .= "AND MONTH(" . $variable . ") BETWEEN 04 AND 06"; break;
                    case 9: $monthfilter .= "AND MONTH(" . $variable . ") BETWEEN 07 AND 09"; break;
                    case 12: $monthfilter .= "AND MONTH(" . $variable . ") BETWEEN 10 AND 12"; break; }
                    break;
            case 'a': break; }
        return $monthfilter;
    }

    public static function getByBrgyCode($tmp, $brgycode = NULL, $brgyvariable = NULL)
    {
        $brgyfilter = '';
        if($tmp == 'm')
        {
            if($brgycode != NULL AND $brgyvariable != NULL)
            {
                $brgyfilter .= "AND " . $brgyvariable . " = '" . $brgycode . "'";
            }
        }
        return $brgyfilter;
    }

}
