<?php
$phone = NULL;
$phone_ext = NULL;
$mobile = NULL;
$email = NULL;
$street_address = NULL;
$house_no = NULL;
$ward_no = NULL;
$region = NULL;
$province = NULL;
$city = NULL;
$barangay = NULL;
$district = NULL;
$zip = NULL;
$country = NULL;
$emergency_name = NULL;
$emergency_relationship = NULL;
$emergency_phone = NULL;
$emergency_mobile = NULL;
$emergency_address = NULL;
$email_editable = "";

if($facility) {
    $district = $facility->facilityContact->district;
    $ward_no = $facility->facilityContact->ward_no;
    $region = $facility->facilityContact->region;
    $province = $facility->facilityContact->province;
    $city = $facility->facilityContact->city;
    $barangay = $facility->facilityContact->barangay;
}

if(isset($patient)) {
    if($patient->patientContact){
        $phone = $patient->patientContact->phone;
        $phone_ext = $patient->patientContact->phone_ext;
        $mobile = $patient->patientContact->mobile;
        $email = $patient->patientContact->email;
        $password = $patient->password;
        $house_no = $patient->patientContact->house_no;
        $street_address = $patient->patientContact->street_address;
        $ward_no = $patient->patientContact->ward_no;
        $region = $patient->patientContact->region;
        $province = $patient->patientContact->province;
        $city = $patient->patientContact->city;
        $barangay = $patient->patientContact->barangay;
        $district = $patient->patientContact->district;
        $zip = ($patient->patientContact->zip > 0) ? $patient->patientContact->zip : NULL;
        $country = $patient->patientContact->country;
    }
    if($patient->patientEmergencyInfo){
        $emergency_name = $patient->patientEmergencyInfo->emergency_name;
        $emergency_relationship = $patient->patientEmergencyInfo->emergency_relationship;
        $emergency_phone = $patient->patientEmergencyInfo->emergency_phone;
        $emergency_mobile = $patient->patientEmergencyInfo->emergency_mobile;
        $emergency_address = $patient->patientEmergencyInfo->emergency_address;
    }
}

?>

<div role="tabpanel" class="tab-pane step" id="location">
    <fieldset {{ $hidden }}>
        <legend>Contact Data</legend>
        <div class="">
            <label class="col-sm-2 control-label">Phone number</label>
            <div class="col-sm-4 form-group">
                {!! Form::text('phone', $phone, array('class' => 'form-control phonePH masked', 'name'=>'inputPatientPhone', 'data-mask'=>'')) !!}
            </div>
            <label class="col-sm-2 control-label">Phone extension</label>
            <div class="col-sm-4 form-group">
                {!! Form::text('phone_ext', $phone_ext, array('class' => 'form-control digits', 'name'=>'inputPatientPhoneExtension')) !!}
            </div>
            <label class="col-sm-2 control-label">Mobile number</label>
            <div class="col-sm-4 form-group">
                {!! Form::text('mobile', $mobile, array('class' => 'form-control mobilePH masked', 'name'=>'inputPatientMobile', 'data-mask'=>'')) !!}
            </div>
            <label class="col-sm-2 control-label">Email address</label>
            <div class="col-sm-4 form-group">
                @if(isset($patient))
                    @if($password!=NULL AND $email!=NULL)
                        <?php $email_editable = "disabled"; ?>
                    @endif
                @endif
                {!! Form::text('email', $email, array('class' => 'form-control email', 'name'=>'inputPatientEmail', $email_editable )) !!}
            </div>
        </div>
    </fieldset>
    <fieldset {{ $hidden }}>
        <legend>Address Information</legend>
        <div class="form-group col-md-6">
            <div class="row">
                <label class="col-sm-4 control-label">House Number</label>
                <div class="col-sm-8">
                    {!! Form::text('house_no', $house_no, array('class' => 'form-control alpha')) !!}
                </div>
            </div>
        </div>
        <div class="form-group col-md-6">
            <div class="row">
                <label class="col-sm-4 control-label">Street Name</label>
                <div class="col-sm-8">
                    {!! Form::text('street_address', $street_address, array('class' => 'form-control alpha', 'name' => 'inputPatientAddress')) !!}
                </div>
            </div>
        </div>
        <div class="form-group col-md-6">
            <div class="row">
                <label class="col-sm-4 control-label">Ward No</label>
                <div class="col-sm-8">
                    {!! Form::text('ward_no', $ward_no, array('class' => 'form-control required')) !!}
                </div>
            </div>
        </div>
        <div class="form-group col-md-6">
            <div class="row">
                <label class="col-sm-4 control-label"> Municipality/City </label>
                <div class="col-sm-8">
                    {!! Form::text('city', $city, array('class' => 'form-control alpha required')) !!}
                </div>
            </div>
        </div>
        <div class="form-group col-md-6 hidden">
            <div class="row">
                <label class="col-sm-4 control-label">Barangay</label>
                <div class="col-sm-8">
                    {!! Form::text('brgy', $barangay, array('class' => 'form-control alpha')) !!}
                </div>
            </div>
        </div>  
        <div class="form-group col-md-6">
            <div class="row">
                <label class="col-sm-4 control-label">District</label>
                <div class="col-sm-8">
                    {!! Form::text('district', $district, array('class' => 'form-control alpha required')) !!}
                </div>
            </div>
        </div>        
        <div class="form-group col-md-6">
            <div class="row">
                <label class="col-sm-4 control-label">Province</label>
                <div class="col-sm-8">
                    {!! Form::text('province', $province, array('class' => 'form-control alpha required')) !!}
                </div>
            </div>
        </div>
        <div class="form-group col-md-12">
            <div class="row">
                <label class="col-sm-2 control-label">ZIP</label>
                <div class="col-sm-4">
                    {!! Form::number('zip', $zip, array('class' => 'form-control', 'name'=>'inputPatientZip', 'min'=>'0')) !!}
                </div>
                <label class="col-sm-2 control-label">Country</label>
                <div class="col-sm-4">
                    {!! Form::select('inputPatientCountry', $nationality, $country, ['class' => 'form-control alpha']) !!}
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset {{ $hidden }}>
        <legend>In Case of Emergency</legend>
        <div class="row">
            <div class="form-group col-md-6">
                <label class="col-sm-4 control-label">Name</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Name of Contact" name="emergency_name" value="{{ $emergency_name }}">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="col-sm-4 control-label">Relationship</label>
                <div class="col-sm-8">
                    <select class="form-control" name="emergency_relationship">
                        <option value="">-- Select --</option>
                        <option <?php if($emergency_relationship == 'Father') echo "selected"; ?>>Father</option>
                        <option <?php if($emergency_relationship == 'Mother') echo "selected"; ?>>Mother</option>
                        <option <?php if($emergency_relationship == 'Sibling') echo "selected"; ?>>Sibling</option>
                        <option <?php if($emergency_relationship == 'Spouse') echo "selected"; ?>>Spouse</option>
                        <option <?php if($emergency_relationship == 'Others') echo "selected"; ?>>Others</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6">
                <label class="col-sm-4 control-label">Phone</label>
                <div class="col-sm-8">
                    {!! Form::text('emergency_phone', $emergency_phone, array('class' => 'form-control phonePH masked', 'name'=>'emergency_phone', 'data-mask'=>'')) !!}
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="col-sm-4 control-label">Mobile</label>
                <div class="col-sm-8">
                    {!! Form::text('emergency_mobile', $emergency_mobile, array('class' => 'form-control mobilePH masked', 'name'=>'emergency_mobile', 'data-mask'=>'')) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6">
                <label class="col-sm-4 control-label">Address</label>
                <div class="col-sm-8">
                    <textarea class="form-control alphanumeric" rows="5" name="emergency_address">{{ $emergency_address }}</textarea>
                </div>
            </div>
        </div>
    </fieldset>
</div><!-- /.tab-pane -->
