<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLovIcd10diagnosisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('lov_icd10_diagnosis')!=TRUE) { 
            Schema::create('lov_icd10_diagnosis', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code', 60);
                $table->text('diagnosis_name');
                $table->softDeletes();
                $table->timestamps();

                $table->unique('id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lov_icd10_diagnosis');
    }
}
