<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FhsisM1fpUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('fhsis_m1fp')==TRUE) {
            if (Schema::hasColumn('fhsis_m1fp', 'barangaycode')!=TRUE) {
                Schema::table('fhsis_m1fp', function (Blueprint $table) {
                  $table->string('barangaycode',60)->after('facility_id')->nullable();
                });
            }
        }   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
