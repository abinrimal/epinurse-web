@extends('users::layouts.masterprofile')

@section('profile-content')
       @include('users::partials.user_nav')

        <div class="col-md-9">
          <!-- Custom Tabs -->
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">User Logs</h3>
                  <div class="box-body table-responsive overflowx-hidden">
                    <table class="table table-hover datatable">
                        <thead>
                          <tr>
                            <th>Login Time</th>
                            <th>Logout Time</th>
                            <th>Device</th>
                            <th>User</th>
                          </tr>
                        </thead>
                        <tbody>
                           @foreach ($userLogs as $key=>$val)
                              <tr>
                                <td>{{ $val->login_datetime }}</td>
                                <td>{{ $val->logout_datetime }}</td>
                                <td>{{ $val->device }}</td>
                                <td>{{ $val->users->first_name }} {{ $val->users->middle_name }} {{ $val->users->last_name }} </td>
                              </tr>
                          @endforeach
                        </tbody>
                    </table>
                  </div><!-- /.box-body -->
                </div>
        </div>
@stop
