<?php namespace ShineOS\Controllers;

use Shine\Libraries\UserHelper;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use ShineOS\Core\Reports\Entities\M2;
use ShineOS\Core\Reports\Entities\M1FP;
use ShineOS\Core\Facilities\Entities\Facilities;
use \Cache;
use \Event;
use \Session;
use \Redirect;
use \URL;

use Module;

class WarehouseController extends Controller {

    public function __construct($app = null) {

    }

    public function index() {

    }

    /**
     * Update the M2 FHSIS Report rerun update script
     */
    public static function updateM2()
    {

        $default_limit = ini_get('max_execution_time');
        set_time_limit(300);

        //$facility = Session::get('facility_details');
        $facility = new Facilities;
        $fac = $facility::get()->pluck('facility_id');

        foreach($fac as $f) {
            //run datawarehouse chores
            //fhsis_m2
            $month = date('n');
            $year = date('Y');

            //process today's fhsis M2
            $diagnoses = DB::select( DB::raw(
            "SELECT TIMESTAMPDIFF(YEAR,b.birthdate,a.encounter_datetime) as 'age',
                b.gender,
                a.diagnosislist_id,
                a.code,
                MONTH(a.encounter_datetime) as 'diagnosisMonth',
                YEAR(a.encounter_datetime) as 'diagnosisYear',
                YEAR(a.datetime_death) as 'deathYear',
                a.facility_id,
                a.barangay,
                count(*) as 'count'
            FROM diagnosis_view a
            JOIN patients b ON b.patient_id = a.patient_id
            WHERE a.facility_id = '".$f."'
            AND a.barangay != ''
            AND a.diagnosislist_id != ''
            GROUP BY a.diagnosislist_id, a.code, age, b.gender, a.facility_id, a.barangay
            ORDER BY a.encounter_datetime ASC") );

            if($diagnoses) {
                foreach($diagnoses as $diagnosis) {
                    //check if this month is present
                    $rec = M2::where('diagnosislist_id', $diagnosis->diagnosislist_id)
                        ->where('age', $diagnosis->age)
                        ->where('gender', $diagnosis->gender)
                        ->where('diagnosisMonth', $diagnosis->diagnosisMonth)
                        ->where('diagnosisYear', $diagnosis->diagnosisYear)
                        ->where('facility_id', $diagnosis->facility_id)
                        ->where('brgycode', $diagnosis->barangay)
                        ->where('updated_at', '<', date('Y-m-d H:i:s'))
                        ->first();

                    if($rec) {
                        $rec->count = $diagnosis->count;
                        $rec->save();
                    } else {
                        $m2 = new M2();
                        $m2->age = $diagnosis->age;
                        $m2->gender = $diagnosis->gender;
                        $m2->diagnosislist_id = $diagnosis->diagnosislist_id;
                        $m2->icd10_code = $diagnosis->code;
                        $m2->diagnosisMonth = $diagnosis->diagnosisMonth;
                        $m2->diagnosisYear = $diagnosis->diagnosisYear;
                        $m2->deathYear = $diagnosis->deathYear;
                        $m2->facility_id = $diagnosis->facility_id;
                        $m2->brgycode = $diagnosis->barangay;
                        $m2->count = $diagnosis->count;
                        $m2->save();
                    }
                }
            }
        }

        set_time_limit($default_limit);

        return 'done';
    }

    /**
     * Update the M2 FHSIS Report daily script
     * Must run cron at 11pm so that today's date is used.
     */
    public static function getDailyM2()
    {

        $default_limit = ini_get('max_execution_time');
        set_time_limit(300);

        //$facility = Session::get('facility_details');
        $facility = new Facilities;
        $fac = $facility::get()->pluck('facility_id');

        foreach($fac as $f) {
            //run datawarehouse chores
            //fhsis_m2
            $month = date('n');
            $year = date('Y');

            //process today's fhsis M2
            $diagnoses = DB::select( DB::raw(
            "SELECT TIMESTAMPDIFF(YEAR,b.birthdate,a.encounter_datetime) as 'age',
                b.gender,
                a.diagnosislist_id,
                a.code,
                MONTH(a.encounter_datetime) as 'diagnosisMonth',
                YEAR(a.encounter_datetime) as 'diagnosisYear',
                YEAR(a.datetime_death) as 'deathYear',
                a.facility_id,
                a.barangay,
                count(*) as 'count'
            FROM diagnosis_view a
            JOIN patients b ON b.patient_id = a.patient_id
            WHERE a.facility_id = '".$f."'
            AND a.barangay != ''
            AND a.diagnosislist_id != ''
            AND DATE(a.encounter_datetime) = '".date('Y-m-d')."'
            GROUP BY a.diagnosislist_id, a.code, age, b.gender, a.facility_id, a.barangay
            ORDER BY a.encounter_datetime ASC") );

            if($diagnoses) {
                foreach($diagnoses as $diagnosis) {
                    //check if this month is present
                    $rec = M2::where('diagnosislist_id', $diagnosis->diagnosislist_id)
                        ->where('age', $diagnosis->age)
                        ->where('gender', $diagnosis->gender)
                        ->where('diagnosisMonth', $diagnosis->diagnosisMonth)
                        ->where('diagnosisYear', $diagnosis->diagnosisYear)
                        ->where('facility_id', $diagnosis->facility_id)
                        ->where('brgycode', $diagnosis->barangay)
                        ->where('updated_at', '<', date('Y-m-d H:i:s'))
                        ->first();

                    if($rec) {
                        $rec->count = $rec->count + $diagnosis->count;
                        $rec->save();
                    } else {
                        $m2 = new M2();
                        $m2->age = $diagnosis->age;
                        $m2->gender = $diagnosis->gender;
                        $m2->diagnosislist_id = $diagnosis->diagnosislist_id;
                        $m2->icd10_code = $diagnosis->code;
                        $m2->diagnosisMonth = $diagnosis->diagnosisMonth;
                        $m2->diagnosisYear = $diagnosis->diagnosisYear;
                        $m2->deathYear = $diagnosis->deathYear;
                        $m2->facility_id = $diagnosis->facility_id;
                        $m2->brgycode = $diagnosis->barangay;
                        $m2->count = $diagnosis->count;
                        $m2->save();
                    }
                }
            }
        }

        set_time_limit($default_limit);

        return 'done';
    }

    //function to get previous months value
    public static function getPrevCount($type, $facilityid, $brgycode ,$method, $dropout=False,$month, $year) {
        if($dropout==False):
            $prevCU = M1FP::select($type)->where('facility_id', $facilityid)->where('barangaycode',$brgycode)->where('current_method', $method)->where('FPmonth', $month)->where('FPyear', $year)->first();
        else:
            $prevCU = M1FP::select($type)->where('facility_id', $facilityid)->where('barangaycode',$brgycode)->where('previous_method', $method)->where('FPmonth', $month)->where('FPyear', $year)->first();
        endif;

        if($prevCU) {
            if($prevCU->$type > 0) {
                return $prevCU->$type;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    //process Family Planning FHSIS monthly
    //and store into Warehouse
    public static function getM1_FP()
    {

        $default_limit = ini_get('max_execution_time');
        set_time_limit(300);

        //$facility = Session::get('facility_details');
        $years = array(2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017);
        $months = array(1,2,3,4,5,6,7,8,9,10,11,12);

        foreach($years as $year) {
            foreach($months as $month) {
            //$month = date('n');
            //$year = date('Y');
                echo "Processing ".$month." ".$year."<br>";
                ob_flush(); flush();
                usleep(1125000);
                //process only current year
                $sql = "SELECT
                        ( SELECT h.facility_id FROM healthcare_services d
                            LEFT JOIN facility_patient_user b ON b.facilitypatientuser_id = d.facilitypatientuser_id
                            LEFT JOIN facility_user g ON g.facilityuser_id = b.facilityuser_id
                            LEFT JOIN facilities h ON h.facility_id = g.facility_id
                            WHERE d.`healthcareservice_id` = a.`healthcareservice_id` ) as 'facilityid',
                        ( SELECT i.barangay FROM healthcare_services d
                            LEFT JOIN facility_patient_user b ON b.facilitypatientuser_id = d.facilitypatientuser_id
                            LEFT JOIN patient_contact i ON b.patient_id = i.patient_id
                            WHERE d.`healthcareservice_id` = a.`healthcareservice_id` ) as 'barangaycode',
                        a.`current_method`,
                        a.`previous_method`,
                        SUM(a.`client_type`='CU') as 'CU',
                        SUM(a.`client_type`='NA') as 'NA_end',
                        SUM(a.`client_sub_type`='CC') as 'CC',
                        SUM(a.`client_sub_type`='CM') as 'CM',
                        SUM(a.`client_sub_type`='RS') as 'RS',
                        (SUM(a.`client_sub_type`='CC')+SUM(a.`client_sub_type`='CM')+SUM(a.`client_sub_type`='RS')) as 'OA',
                        SUM(a.`dropout_date` IS NOT NULL) as 'Dropout',
                        MONTH(a.`created_at`) as 'FPmonth',
                        YEAR(a.`created_at`) as 'FPyear', count(*) as 'FP_count',
                        a.created_at
                    FROM `familyplanning_service` a
                    WHERE YEAR(a.created_at) = $year
                    AND MONTH(a.created_at) = $month
                    GROUP BY a.current_method, a.previous_method, facilityid, barangaycode
                    ORDER BY FPyear ASC, FPmonth ASC";

                $cur_fp = DB::select( DB::raw( $sql ));
                if($cur_fp) {

                    foreach($cur_fp as $fp) {

                        if($fp->facilityid != NULL AND ($fp->current_method != NULL OR $fp->previous_method != NULL)) {

                            if($fp->FPmonth == 1) {
                                $fpmonth = 12;
                                $fpyear = $fp->FPyear - 1;
                            } else {
                                $fpmonth = $fp->FPmonth-1;
                                $fpyear = $fp->FPyear;
                            }

                            $brgy = $fp->barangaycode;
                            if($fp->barangaycode == NULL) {
                                $brgy = DB::table('facility_contact')->where('facility_id', $fp->facilityid)->pluck('barangay');
                            }

                            // Count CU and NA of previous months of current method
                            $CUpreviousMonth = self::getPrevCount('CU_end', $fp->facilityid, $brgy, $fp->current_method, False, $fpmonth, $fpyear);
                            $NApreviousMonth = self::getPrevCount('NA_end', $fp->facilityid, $brgy, $fp->current_method, False, $fpmonth, $fpyear);

                            // Count dropouts of previous method
                            $DropoutcurrentMonth = self::getPrevCount('Dropout', $fp->facilityid, $brgy, $fp->current_method, True, $fp->FPmonth, $fp->FPyear);

                            //compute ending users using FHSIS formula
                            $CUend = $CUpreviousMonth + $NApreviousMonth + $fp->OA - $DropoutcurrentMonth;
                            if($CUend < 0) {
                                $CUend = 0;
                            }

                            //delete previous record for this month, year, facility and method
                            $deletePrevM1FP = M1FP::where('facility_id',$fp->facilityid)
                                                    ->where('barangaycode',$brgy)
                                                    ->where('current_method',$fp->current_method)
                                                    ->where('previous_method',$fp->previous_method)
                                                    ->where('FPmonth',$fp->FPmonth)
                                                    ->where('FPyear',$fp->FPyear)
                                                    ->delete();

                            //store new computed data for this record
                            $fp_data = new M1FP();
                            $fp_data->facility_id = (is_null($fp->facilityid)) ? 0: $fp->facilityid;
                            $fp_data->barangaycode = (is_null($brgy)) ? 0: $brgy;
                            $fp_data->current_method = (is_null($fp->current_method)) ? 0: $fp->current_method;
                            $fp_data->previous_method = (is_null($fp->previous_method)) ? 0: $fp->previous_method;
                            $fp_data->CU_begin = (is_null($CUpreviousMonth)) ? 0: $CUpreviousMonth;
                            $fp_data->NA_begin = (is_null($NApreviousMonth)) ? 0: $NApreviousMonth;
                            $fp_data->CC = (is_null($fp->CC)) ? 0: $fp->CC;
                            $fp_data->CM = (is_null($fp->CM)) ? 0: $fp->CM;
                            $fp_data->RS = (is_null($fp->RS)) ? 0: $fp->RS;
                            $fp_data->OA = (is_null($fp->OA)) ? 0: $fp->OA;
                            $fp_data->Dropout = (is_null($fp->Dropout)) ? 0: $fp->Dropout;
                            $fp_data->CU_end = (is_null($CUend)) ? 0: $CUend;
                            $fp_data->NA_end = (is_null($fp->NA_end)) ? 0: $fp->NA_end;
                            $fp_data->FPmonth = (is_null($fp->FPmonth)) ? 0: $fp->FPmonth; 
                            $fp_data->FPyear = (is_null($fp->FPyear)) ? 0: $fp->FPyear;
                            $fp_data->save();
                        }
                    }
                }
            }
        }

        set_time_limit($default_limit);

        return 'done';
    }

}
