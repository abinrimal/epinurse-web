<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpinurseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('epinurse_personal')==FALSE)
        {
            Schema::create('epinurse_personal', function (Blueprint $table) {
                $table->increments('id');
                $table->string('epinursepersonal_id', 32);
                $table->string('patient_id', 32);
                $table->string('uuid', 100)->nullable();
                $table->string('owner_uuid', 100)->nullable();

                $table->integer('marital_status')->nullable();
                $table->string('ethnicity',100)->nullable();
                $table->string('religion',100)->nullable();
                $table->integer('educational_status')->nullable();
                $table->integer('occupation')->nullable();

                $table->integer('income_agriculture')->nullable();
                $table->integer('income_business')->nullable();
                $table->integer('income_service')->nullable();
                $table->integer('income_labor')->nullable();
                $table->integer('income_remittance')->nullable();                
                $table->integer('income_others')->nullable();                

                $table->string('income_specify',100)->nullable();
                $table->integer('food_sufficiency')->nullable();
                $table->integer('type_of_family')->nullable();
                $table->string('family_id',100)->nullable();
                $table->string('fathers_name',100)->nullable();
                $table->string('mothers_name',100)->nullable();
                $table->string('local_guardians_name',100)->nullable();
                $table->string('contact_of_parent_guardian',100)->nullable();

                $table->integer('number_male')->nullable();
                $table->integer('number_female')->nullable();
                $table->integer('number_others')->nullable();
                $table->integer('number_male_under5')->nullable();
                $table->integer('number_femalender5')->nullable();
                $table->integer('number_male_above60')->nullable();
                $table->integer('number_female_above60')->nullable();
                $table->integer('number_disabled')->nullable();
                $table->integer('number_pregnant')->nullable();
                $table->integer('number_lactating_mothers')->nullable();

                $table->softDeletes();
                $table->timestamps();
                $table->unique('patient_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('epinurse_personal');
    }
}
