<?php
namespace ShineOS\Core\Sync\Http\Controllers;

use Illuminate\Routing\Controller;
use Shine\Libraries\IdGenerator;
use ShineOS\Core\Sync\Entities\Sync;
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderLabExam; //model

use Config, View, Response, Validator, Input, Mail, Session, Redirect,
    Hash,
    Auth,
    DB,
    Datetime,
    Request,
    File;

class SyncController extends Controller {

    protected $curr_db;
    protected $master_array = array();

    public function __construct() {
        DB::disableQueryLog();
        $this->curr_datetime = new DateTime('NOW');
        $this->curr_db = Config::get('database.connections.'.Config::get('database.default').'.database');
        $this->facility_details = Session::get('facility_details');
    }

    public function index() {
        $this->install_log("Ready");

        $localtables = DB::select("SELECT DISTINCT TABLE_NAME FROM Information_Schema.Columns WHERE TABLE_SCHEMA = '".$this->curr_db."' AND TABLE_NAME NOT IN ('sync','tracker','migrations','sessions','roles','facilities') AND TABLE_NAME NOT LIKE 'temp_%' AND TABLE_NAME NOT LIKE 'lov_%' AND TABLE_NAME NOT LIKE '%_view' AND TABLE_NAME NOT LIKE 'fhsis_%' AND TABLE_NAME NOT LIKE '%api_%' AND TABLE_NAME NOT LIKE 'wh_%' AND TABLE_NAME NOT LIKE 'phie_%'");

        $data['localtables'] = count($localtables);

        $data['facid'] = $this->facility_details->facility_id;
        return view('sync::index', $data);
    }

    //get all local data then conenct to API
    public function sendtoCloud() {
        $this->install_log("Starting sync process. Please wait...");

        $facid = $this->facility_details->facility_id;
        $all = $this->getAllLocalData();

        $cnttable = count($all);
        $this->install_log("Syncing ".$cnttable.". tables.");
        flush();
        sleep(1);

        $sync = NULL;
        $syncstat = NULL;
        if($cnttable > 0) {
            foreach($all as $key=>$data) {
                $this->install_log("Syncing ".$key.".");
                $table[$key] = $data;

                $sync = json_decode( $this->sendRecords('manageRecords', $table, $facid ) );

                if($sync and $sync->status == 'success') {
                    if($syncstat == NULL) $syncstat = 'SUCCESS';
                }
                if($sync and $sync->status == 'Nothing to sync') {
                    if($syncstat == NULL) $syncstat = 'NOTHING';
                }
                $table = NULL;
            }

            if($syncstat) {
                if($syncstat == 'SUCCESS') {
                    $this->install_log("Logging sync date.");
                    $a = $this->insertLastSync($this->facility_details->facility_id, "toCloud");
                    $status = 'success';
                } elseif($syncstat == 'NOTHING') {
                    $status = 'Nothing to sync';
                } else {
                    $status = 'fail';
                }

            } else {
                $status = 'Nothing to sync';
            }

            if($status == 'success') {
                $this->install_log("done");
            } elseif($status == 'Nothing to sync') {
                $this->install_log("nothing");
            } else {
                $this->install_log("fail");
            }
        } else {
            $this->install_log("nothing");
            $status = 'Nothing to sync';
        }

        return $status;
    }

    //export all data to json file
    public function downloadLocalData() {
        //get all data from last download date
        $all = $this->getAllLocalData('disk');
        // dd($all);
        //download or export
        $all = json_encode($all);
        $ac = self::encrypt_decrypt('encrypt', $all);
        $datetime = $this->curr_datetime->format('YmdHis');

        $file= public_path().'/public/'.$this->facility_details->facility_id.'_'.$datetime.'.txt';
        File::put($file, $ac);

        //let us log date of download
        $result = $this->insert_last_sync($this->facility_details->facility_id, 'disk');

        return response()->download($file)->deleteFileAfterSend(true);
    }

    //backup all data to json file
    public function backupData() {
        //get all data from last beginning of time
        $all = $this->getAllLocalData('backup');
dd($all);
        //download or export
        $all = json_encode($all);
        $ac = self::encrypt_decrypt('encrypt', $all);
        $datetime = $this->curr_datetime->format('YmdHis');

        $file= public_path().'/public/'.$this->facility_details->facility_id.'_'.$datetime.'.txt';
        File::put($file, $ac);

        return response()->download($file)->deleteFileAfterSend(true);
    }

    /*** Get all local data, currently no connection to cloud */
    private function getAllLocalData($type=NULL) {
        $records = array();
            //let us collect all tables
            $all_tables = DB::select("SELECT DISTINCT TABLE_NAME FROM Information_Schema.Columns WHERE TABLE_SCHEMA = '".$this->curr_db."' AND TABLE_NAME NOT IN ('sync','tracker','migrations','sessions','roles','facilities') AND TABLE_NAME NOT LIKE 'temp_%' AND TABLE_NAME NOT LIKE 'lov_%' AND TABLE_NAME NOT LIKE '%_view' AND TABLE_NAME NOT LIKE 'fhsis_%' AND TABLE_NAME NOT LIKE '%api_%' AND TABLE_NAME NOT LIKE 'wh_%' AND TABLE_NAME NOT LIKE 'phie_%'" );

            //let us get the last sync date
            //if this is download to disk
            //get last download made
            if($type) {
                $syncdate = json_encode($this->getLastSync('disk'));
            //else get last online sync date
            } else {
                $syncdate = $this->getLastSync('toCloud');
            }
    
            if($syncdate AND $syncdate != '{}') {
                $sd = json_decode($syncdate);
                if($sd) {
                    $sdate = $sd->created_at;
                } else {
                    $sdate = "1970-01-01 00:00:00";
                }
            } else {
                $sdate = "1970-01-01 00:00:00";
            }

            if($all_tables) {
                foreach($all_tables as $table) {
                    $tablename = $table->TABLE_NAME;
                    DB::table($table->TABLE_NAME)->where('updated_at','>',$sdate)->chunk(50, function ($data) use(&$records, $table){
                            foreach ($data as $v_data) {
                                $records[$table->TABLE_NAME][] = $v_data;
                            }
                        });
                }
            }
            //echo "<pre>"; print_r($records); die;

        return $records;
    }

    //download data from the CLOUD and insert to CE - with internet connection
    public function downloadFromCloud() {
        $this->install_log("Starting sync process. Please wait...");
        flush();
        sleep(1);
        $result = NULL;
        $syncgood = NULL;

        //compare tables from CE to CLOUD
        $tables = DB::select("SELECT DISTINCT TABLE_NAME FROM Information_Schema.Columns WHERE TABLE_SCHEMA = '".$this->curr_db."' AND TABLE_NAME NOT IN ('sync','tracker','migrations','sessions','roles','facilities') AND TABLE_NAME NOT LIKE 'temp_%' AND TABLE_NAME NOT LIKE 'lov_%' AND TABLE_NAME NOT LIKE '%_view' AND TABLE_NAME NOT LIKE 'fhsis_%' AND TABLE_NAME NOT LIKE '%api_%' AND TABLE_NAME NOT LIKE 'wh_%' AND TABLE_NAME NOT LIKE 'phie_%'");
        $facid = $this->facility_details->facility_id;

        $cnttable = count($tables);
        $this->install_log("Syncing ".$cnttable." tables.");
        flush();
        sleep(1);

        //Send CE tables to cloud for comparison
        if($tables) {
            foreach($tables as $table) {
                $this->install_log("Syncing ".$table->TABLE_NAME.".");
                $sync = json_decode($this->sendRecords('cloudRecords', $table->TABLE_NAME, $facid));

                //if there are tables from cloud, insert them into CE
                if($sync != 'Nothing to sync') {
                    $result = $this->insert_to_CEDB($sync);
                    if($result) {
                        if($syncgood == NULL) $syncgood = 'Ok';
                    }
                } else {
                    if($syncgood == NULL) $syncgood = 'Nothing';
                }
                $sync = NULL;
            }
        }

        if($syncgood == 'Ok') {
            //let us say success if successful
            $this->install_log("Logging sync date.");
            $a = $this->insertLastSync($this->facility_details->facility_id, "fromCloud");
            $status = 'success';
        } elseif($syncgood = 'Nothing') {
            $status = 'nothing';
        } else {
            //let us say fail if not
            $status = 'fail';
        }

        if($status == 'success') {
            $this->install_log("done");
        } elseif($status == 'nothing') {
            $this->install_log("nothing");
        } else {
            $this->install_log("fail");
        }

        return $status;
    }


    /*****************
    ** CLOUD / LIVE EMR
    ******************/

    //import data from exported data
    public function importData() {
        $importjson = Input::file('importjson');
        if($importjson) {
            $synced = FALSE;
            $file = $importjson->openFile();
            $fordecrypt = $file->fread($file->getSize());
            //decrypt code
            $decrypted_txt = $this->encrypt_decrypt('decrypt', $fordecrypt);
            $content = json_decode($decrypted_txt);

            if(isset($content) AND $this->facility_details->facility_id) {
                $facid = $this->facility_details->facility_id;
                unset($content->facility_id);
                foreach($content as $key=>$data) {
                    
                    $table[$key] = $data;
                    $sync = $this->manageRecords( $table, $facid );

                    if($sync) {
                        $synced = TRUE;
                    }
                }

                if($synced) {
                    self::insert_last_sync($facid, 'toCloud');
                    Session::flash('alert-class', 'alert-success alert-dismissible');
                    $message = "Upload and sync successful.";
                } else {
                    Session::flash('alert-class', 'alert-danger alert-dismissible');
                    $message = "Sync error. Please try again.";
                }
            } else {
                Session::flash('alert-class', 'alert-danger alert-dismissible');
                $message = "Sync error. You upload an invalid or corrupted file.";
            }
        } else {
            Session::flash('alert-class', 'alert-danger alert-dismissible');
            $message = "Sync error.";
        }


        return Redirect::to('sync')->with('message', $message);

    }


    /**********************************************************************************************************
    ** Sends records to SHINE API
    ************************************************************************************************************/

    public function sendRecords($param, $records, $facid) {
        $data = array(
          "Records" => $records,
          "Facid" => $facid
        );
        $url = "http://".getenv('CLOUDIP')."/api/sync/".$param;

        $str_data = json_encode($data);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$str_data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "ShineKey:3670407151512120101064700",
            "ShineSecret:3670407151512120101064700")
        );
        $result = curl_exec($ch);
        curl_close($ch);

        // echo "<pre> sendRecords"; print_r($result); echo "</pre>";
        return $result;
    }

    /**
     * Get last sync date
     * @return [type] [description]
     */
    public static function getLastSync($toFrom=NULL) {
        $facility_id = Session::get('facility_details');
        $data = array(
          "facility_id" => $facility_id->facility_id,
          "toFrom" => $toFrom
        );
        
        if($toFrom == 'disk') {
            $updated_at = Sync::getSyncDateTime($facility_id->facility_id, 'disk');
            return $updated_at;
        } else {
            $url = "http://".getenv('CLOUDIP')."/api/sync/getSyncDateTime";

            $str_data = json_encode($data);

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$str_data);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json",
                "ShineKey:3670407151512120101064700",
                "ShineSecret:3670407151512120101064700")
            );
            $result = curl_exec($ch);
            curl_close($ch);
        }

        return $result;
    }

    public function insertLastSync($facilityid, $toFrom) {
        $data = array(
          "facility_id" => $facilityid,
          "toFrom" => $toFrom
        );

        $url = "http://".getenv('CLOUDIP')."/api/sync/insertLastSync";

        $str_data = json_encode($data);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$str_data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "ShineKey:3670407151512120101064700",
            "ShineSecret:3670407151512120101064700")
        );

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    /**********************************************************************************************************
    ** API SIDE
    ************************************************************************************************************/

    /**
     * Manage insertion of data
     * either direct from CE or import data
     */
    public static function manageRecords($records, $facid) {
        $_this = new self;
        $id = $facid;
        //check facility_id if exists
        $count = Facilities::where('facility_id',$id)->count();

        if($count) {

            foreach($records as $key=>$rec) {
                $ins_if_new = $_this->insert_to_DB($key, $rec);
            }

            // CLOUD TO INSERT
            if($ins_if_new){
                //self::insert_last_sync($id, 'toCloud');
                return TRUE;
            }
        }
        return FALSE;
    }

    public static function insert_last_sync($facility_id, $toFrom = NULL) {
        $syncid = IdGenerator::generateId();
        $query = new Sync;
        $query->sync_id = $syncid;
        $query->facility_id = $facility_id;
        $query->toFrom = $toFrom;
        $query->save();

        return $query;
    }


    /**********************************************************************************************************
    ** CLOUD SIDE
    ************************************************************************************************************/

    // DOWNLOAD DATA TO CE - with internet connections
    public static function cloudRecords($tables, $facid) {
        $_this = new self;
        $_this->curr_db = Config::get('database.connections.'.Config::get('database.default').'.database');
        $whereInTables = '';
        $facility_id = (!empty($facid)) ? $facid : NULL;

        if($tables) {
            //get all tables
            $tables_2 = DB::select("SELECT DISTINCT TABLE_NAME FROM Information_Schema.Columns WHERE TABLE_SCHEMA = '".$_this->curr_db."' AND TABLE_NAME NOT IN ('sync','tracker','migrations','sessions','roles','facilities') AND TABLE_NAME NOT LIKE 'temp_%' AND TABLE_NAME NOT LIKE 'lov_%' AND TABLE_NAME NOT LIKE '%_view' AND TABLE_NAME NOT LIKE 'fhsis_%' AND TABLE_NAME NOT LIKE '%api_%' AND TABLE_NAME NOT LIKE 'wh_%' AND TABLE_NAME NOT LIKE 'phie_%'");

            //create a compare array
            foreach ($tables_2 as $key_2 => $value_2) {
                $array_2[$key_2] = $value_2->TABLE_NAME;
            }

            //check if table is in compare array
            if(in_array($tables, $array_2)) {
                $whereInTables = $tables;
            }

        }
        $_this->master_array = array();
        $updated_at = Sync::getSyncDateTime($facility_id, 'fromCloud');
        if($updated_at!=NULL OR $updated_at) {
            $updatedat = $updated_at->updated_at;
        } else {
            $updatedat = "0000-00-00 00:00:00";
        }

        $facilities = DB::select("SELECT DISTINCT TABLE_NAME FROM Information_Schema.Columns WHERE TABLE_SCHEMA = '".$_this->curr_db."' AND COLUMN_NAME = 'facility_id' AND TABLE_NAME = '".$whereInTables."'");
        if(count($facilities) > 0 AND $facility_id) {
            foreach($facilities as $table_facility_id) {
                $table_data = DB::table($table_facility_id->TABLE_NAME)->where('facility_id', $facility_id)->where('updated_at', '>', $updatedat)->get();
                if($table_data) {
                    $_this->master_array[$table_facility_id->TABLE_NAME] = $table_data;
                }
            }
        }

        $facilityuserid = DB::table('facility_user')->where('facility_id', $facility_id)->lists('facilityuser_id');

        //step2 : select all table with facilityuser_id column
        $facility_user = DB::select("SELECT DISTINCT TABLE_NAME FROM Information_Schema.Columns WHERE TABLE_SCHEMA = '".$_this->curr_db."' AND COLUMN_NAME = 'facilityuser_id' AND TABLE_NAME = '".$whereInTables."'");

        if(isset($facility_user) AND isset($facilityuserid)) {
            foreach($facility_user as $table_facilityuser_id) {
                $table_data = DB::table($table_facilityuser_id->TABLE_NAME)->whereIn('facilityuser_id', $facilityuserid)->where('updated_at', '>', $updatedat)->get();
                if($table_data) {
                    $_this->master_array[$table_facilityuser_id->TABLE_NAME] = $table_data;
                }
            }
        }

        // get all
        if(isset($facilityuserid)) {
            $fpuid_by_fuid = DB::table('facility_patient_user')->whereIn('facilityuser_id', $facilityuserid)->lists('facilitypatientuser_id');
            $pid_by_fuid = DB::table('facility_patient_user')->whereIn('facilityuser_id', $facilityuserid)->lists('patient_id');
        }

        if($whereInTables == 'healthcare_services') {
            $table_data = DB::table('healthcare_services')->whereIn('facilitypatientuser_id', $fpuid_by_fuid)->where('updated_at', '>', $updatedat)->get();
            if($table_data) {
                $_this->master_array['healthcare_services'] = $table_data;
            }
        }
        if($whereInTables == 'patients' AND isset($pid_by_fuid)) {
            $table_data = DB::table('patients')->whereIn('patient_id', $pid_by_fuid)->where('updated_at', '>', $updatedat)->get();
            if($table_data) {
                $_this->master_array['patients'] = $table_data;
            }
        }

        $user_id = DB::table('facility_user')->where('facility_id', $facility_id)->lists('user_id');
        $users = DB::select("SELECT DISTINCT TABLE_NAME FROM Information_Schema.Columns WHERE TABLE_SCHEMA = '".$_this->curr_db."' AND COLUMN_NAME = 'user_id' AND TABLE_NAME = '".$whereInTables."'");
        if(isset($users) AND isset($user_id)) {
            foreach($users as $table_user_id) {
                $table_data = DB::table($table_user_id->TABLE_NAME)->whereIn('user_id', $user_id)->where('updated_at', '>', $updatedat)->get();
                if($table_data) {
                    $_this->master_array[$table_user_id->TABLE_NAME] = $table_data;
                }
            }
        }

        $except_from_patients = $patients = NULL;
        if(is_array($_this->master_array)){
            $except_from_patients = implode("','", array_keys($_this->master_array));
        }
        $patients = DB::select("SELECT DISTINCT TABLE_NAME FROM Information_Schema.Columns WHERE TABLE_SCHEMA = '".$_this->curr_db."' AND COLUMN_NAME = 'patient_id' AND TABLE_NAME NOT IN ('".$except_from_patients."') AND TABLE_NAME = '".$whereInTables."'");

        if($patients AND isset($pid_by_fuid)) {
            foreach($patients as $table_patient_id) {
                $table_data = DB::table($table_patient_id->TABLE_NAME)->whereIn('patient_id', $pid_by_fuid)->where('updated_at', '>', $updatedat)->get();
                if($table_data) {
                    $_this->master_array[$table_patient_id->TABLE_NAME] = $table_data;
                }
            }
        }

        //get all healthcareservices of the facilitypatientuser_id
        $healthcareservice_id = NULL;
        $healthcareservice_id = DB::select("SELECT DISTINCT TABLE_NAME FROM Information_Schema.Columns WHERE TABLE_SCHEMA = '".$_this->curr_db."' AND COLUMN_NAME = 'healthcareservice_id' AND TABLE_NAME = '".$whereInTables."'");
        if( isset($fpuid_by_fuid) ) {
            $healthcareservices = DB::table('healthcare_services')->whereIn('facilitypatientuser_id', $fpuid_by_fuid)->lists('healthcareservice_id');
        }

        if($healthcareservice_id AND isset($healthcareservices) AND is_array($_this->master_array)) {
            foreach($healthcareservice_id as $table_healthcareservice_id) {
                $table_data = DB::table($table_healthcareservice_id->TABLE_NAME)->whereIn('healthcareservice_id', $healthcareservices)->where('updated_at', '>', $updatedat)->get();
                if($table_data) {
                    $_this->master_array[$table_healthcareservice_id->TABLE_NAME] = $table_data;
                }

                $constraint_type[$table_healthcareservice_id->TABLE_NAME] = DB::select("SELECT DISTINCT COLUMN_NAME from information_schema.KEY_COLUMN_USAGE where TABLE_SCHEMA = '".$_this->curr_db."' AND CONSTRAINT_NAME!='PRIMARY' AND TABLE_NAME = '".$table_healthcareservice_id->TABLE_NAME."' AND TABLE_NAME = '".$whereInTables."'");
                $except = implode("','", array_keys($_this->master_array));
                if(count($constraint_type[$table_healthcareservice_id->TABLE_NAME])!=NULL) {
                    foreach ($constraint_type[$table_healthcareservice_id->TABLE_NAME] as $ct_tbl_key => $ct_tbl_value) {
                        $constraint_type[$table_healthcareservice_id->TABLE_NAME]['COLUMN_NAME_VALUES'] = DB::table($table_healthcareservice_id->TABLE_NAME)->whereIn('healthcareservice_id', $healthcareservices)->lists($ct_tbl_value->COLUMN_NAME);
                        $constraint_type[$table_healthcareservice_id->TABLE_NAME]['COLUMN_CONSTRAINT_LIST'] = DB::select("SELECT DISTINCT TABLE_NAME FROM Information_Schema.Columns WHERE TABLE_SCHEMA = '".$_this->curr_db."' AND COLUMN_NAME = '".$ct_tbl_value->COLUMN_NAME."' AND TABLE_NAME NOT LIKE 'temp_%' AND TABLE_NAME NOT LIKE 'lov_%' AND TABLE_NAME NOT LIKE '%_view' AND TABLE_NAME NOT IN ('".$except."') AND TABLE_NAME != 'tracker'");
                    }

                    foreach ($constraint_type as $key => $value) {
                        if(count($value)) {
                            if(count($value['COLUMN_CONSTRAINT_LIST'])!=NULL) {
                                foreach ($value['COLUMN_CONSTRAINT_LIST'] as $const_key => $const_value) {
                                    $table_data = DB::table($const_value->TABLE_NAME)->whereIn($value[0]->COLUMN_NAME, $value['COLUMN_NAME_VALUES'])->where('updated_at', '>', $updatedat)->get();
                                    if($table_data) {
                                        $_this->master_array[$const_value->TABLE_NAME] = $table_data;
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        // echo "<pre> master_array "; echo " ".$tables." "; print_r($_this->master_array); echo "</pre>";
        return $_this->master_array;
    }

    // Insert to DB - compare, update and insert
    public static function insert_to_DB($key,$record) {
        if(is_array($record)==TRUE) {
            $arr_obj = $record;
        } else {
            $arr_obj = $record->data;
        }

        $_this = new self;
        $result = FALSE;
        $labsID = 0;

        foreach ($arr_obj as $key_1 => $value_2) {
            if(is_array($value_2)==TRUE) {
                $toarray = $value_2;
                $toarray_2 = $value_2;
            } else {
                $toarray = get_object_vars($value_2);
                $toarray_2 = get_object_vars($value_2);
            }

            unset($toarray['id']);
            $constraint[$key] = DB::select("SELECT DISTINCT COLUMN_NAME from information_schema.KEY_COLUMN_USAGE where TABLE_SCHEMA = '".$_this->curr_db."' AND CONSTRAINT_NAME!='PRIMARY' AND TABLE_NAME = '".$key."'");

            if(!empty($constraint[$key])) {
                /*//if this is lab exams, let us force delete all records
                if($constraint[$key][0]->COLUMN_NAME == "medicalorderlaboratoryexam_id") {
                    if($labsID != $toarray['medicalorder_id']) {
                        DB::table($key)->where('medicalorder_id', $toarray['medicalorder_id'])->delete();
                        $labsID = $toarray['medicalorder_id'];
                    }
                }*/

                //check if this record already exists
                $checker_count = DB::table($key)->where($constraint[$key][0]->COLUMN_NAME, $toarray[$constraint[$key][0]->COLUMN_NAME])->count();

                if($checker_count == 1) { //update
                    unset($toarray_2['id']);
                    unset($toarray_2[$constraint[$key][0]->COLUMN_NAME]);

                    $result[$key][$key_1] = DB::table($key)->where($constraint[$key][0]->COLUMN_NAME, $toarray[$constraint[$key][0]->COLUMN_NAME])->update($toarray_2);
                } else { //insert

                    $result[$key][$key_1] = DB::table($key)->insert($toarray);

                }
            }
        }
        return $result;
    }

    public static function insert_to_CEDB($record) {
        $arr_obj = $result = NULL;
        if(is_array($record)==TRUE) {
            $arr_obj = $record;
        } else {
            if(isset($record->data)) {
                $arr_obj = $record->data;
            }
        }
        if($arr_obj){
            $_this = new self;
            $result = FALSE;
            $labsID = 0;
            foreach ($arr_obj as $key => $value) {
                if(count($value)) {
                    foreach ($value as $key_1 => $value_2) {
                        if(is_array($record)==TRUE) {
                            $toarray = $value_2;
                            $toarray_2 = $value_2;
                        } else {
                            $toarray = get_object_vars($value_2);
                            $toarray_2 = get_object_vars($value_2);
                        }
                        unset($toarray['id']);
                        $constraint[$key] = DB::select("SELECT DISTINCT COLUMN_NAME from information_schema.KEY_COLUMN_USAGE where TABLE_SCHEMA = '".$_this->curr_db."' AND CONSTRAINT_NAME!='PRIMARY' AND TABLE_NAME = '".$key."'");

                        if(!empty($constraint[$key])) {
                            /*//if this is lab exams, let us force delete all records
                            if($constraint[$key][0]->COLUMN_NAME == "medicalorderlaboratoryexam_id") {
                                if($labsID != $toarray['medicalorder_id']) {
                                    DB::table($key)->where('medicalorder_id', $toarray['medicalorder_id'])->delete();
                                    $labsID = $toarray['medicalorder_id'];
                                }
                            }*/

                            $checker_count = DB::table($key)->where($constraint[$key][0]->COLUMN_NAME, $toarray[$constraint[$key][0]->COLUMN_NAME])->count();
                                if($checker_count == 1) { //update
                                    unset($toarray_2['id']);
                                    unset($toarray_2[$constraint[$key][0]->COLUMN_NAME]);

                                    $result[$key][$key_1] = DB::table($key)->where($constraint[$key][0]->COLUMN_NAME, $toarray[$constraint[$key][0]->COLUMN_NAME])->update($toarray_2);

                                } else { //insert
                                    $result[$key][$key_1] = DB::table($key)->insert($toarray);

                                }
                        }
                    }
                }
            }
        }
        return $result;
    }

    public static function encrypt_decrypt($action, $string)
    {
        $output = false;

        $encrypt_method = getenv('ENCRYPT_METHOD');
        $secret_key = getenv('SECRET_KEY');
        $secret_iv = getenv('SECRET_IV');

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if( $action == 'decrypt' ){
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    public function install_log($text)
    {
        $log_file = userfiles_path() .  '/logs/sync_'.$this->facility_details->facility_id.'_log.txt';
        if (!is_file($log_file)) {
            @touch($log_file);

        }
        if (is_file($log_file)) {
            $json = array('date' => date('H:i:s'), 'msg' => $text);

            if ($text == 'Ready' or $text == 'done') {
                @file_put_contents($log_file, $text . "\n");
            } else {
                @file_put_contents($log_file, $text . "\n", FILE_APPEND);

            }
        }
    }
}
