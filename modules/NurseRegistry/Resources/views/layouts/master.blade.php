@extends('layout.master')

@section('heads')

@stop

@section('page-header')
  <section class="content-header">
    <!-- edit by RJBS -->
    <h1>
      <i class="fa fa-user-circle"></i>
      Nurse Registry
    </h1>
    <!-- end edit RJBS -->
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> <a href="{{ url('/schoolassessment') }}"> Nurse Registry </a></li>
    </ol>
  </section>
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
	    @if (Session::has('flash_message'))
	        <div class="alert {{Session::get('flash_type') }}">{{ Session::get('flash_message') }}</div>
	    @endif
   	</div>
	   	
		@yield('page-content')
	</div>
@stop

@section('plugin_jsscripts')
  <script type="text/javascript">
    $(document).ready( function(){
      $('#add-quali-btn').click( function(){
        var prot = $("tr.quali-row:first").clone(true);
            prot.find('input').val('');     
            prot.insertAfter("tr.quali-row:last");
      });
      $('.delete-quali-btn').click( function(){
        var master = $(this).closest('tr');
            master.remove();
      });

      $('#add-training-btn').click( function(){
        var prot = $("tr.training-row:first").clone(true);
            prot.find('input').val('');     
            prot.insertAfter("tr.training-row:last");
      });
      $('.delete-training-btn').click( function(){
        var master = $(this).closest('tr');
            master.remove();
      });
    });
  </script>
@stop