@section('heads')
{!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.css') !!}
{!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.skinFlat.css') !!}
{!! HTML::style('public/dist/css/flaticon.css') !!}
<style>
    @page {
      size: Letter;
      margin-left: .25in;
      margin-right: .25in;
    }

    .dataTables_wrapper > .row:first-child, .dataTables_wrapper > .row:last-child {
        display:none;
    }
    .box-title {
        font-size: 26px !important;
    }
    .table label, label {
        float:left;
        width:150px;
    }
    table.table-align-top td {
        vertical-align: top !important;
    }
    table.table-align-top td.align-bottom {
        vertical-align: bottom !important;
    }
    .table label.fullwidth {
        display:block;
        width: 100%;
    }
    .overflowx-hidden {
        padding-bottom: 20px !important;
    }
    .profile-img {
        width:90px !important;
        float: left;
        margin: 0 20px 20px 0;
    }
    p {
        margin: 0 0 5px !important;
    }
    p.text-muted {
        margin: 0 0 5px 150px !important;
    }
</style>
@stop

@extends('layout.masterprint')
@section('page-header')
    <div class="row">
      <div class="col-md-12">
          <table class="table table-align-top no-border">
                <tr>
                    <td width="55%">
                        <?php
                            $logo = $facility->facility_logo;
                        ?>
                        @if ( $logo != '' )
                            <img src="{{ url( 'public/uploads/profile_picture/'.$logo ) }}" class="profile-img img-circle" />
                        @else
                            <img src="{{ asset( 'public/dist/img/no_logo.png' ) }}" class="profile-img img-circle" />
                        @endif

                        <p class="small"><b>{{ $facility->facility_name }}</b><br />
                            {{ isset($facility->facilityContact->building_name) ? $facility->facilityContact->building_name.", " : "" }}
                            {{ isset($facility->facilityContact->house_no) ? $facility->facilityContact->house_no." " : "" }}
                            {{ isset($facility->facilityContact->street_name) ? $facility->facilityContact->street_name.", " : "" }}
                            <br />
                            {{ isset($facility->facilityContact->barangay) ? getBrgyName($facility->facilityContact->barangay) : "" }}
                            {{ isset($facility->facilityContact->municipality) ? ", ".getCityName($facility->facilityContact->municipality) : "" }}
                            <br />
                            @if(isset($facility->facilityContact->province))
                                {{ getProvinceName($facility->facilityContact->province).", " }}
                            @endif
                            @if(isset($facility->facilityContact->zip))
                                {{ $facility->facilityContact->zip }}
                            @endif
                            @if(isset($facility->facilityContact->region))
                                <br />{{ getRegionName($facility->facilityContact->region) }}
                            @endif
                            @if(isset($facility->facilityContact->phone))
                                <br />{{ $facility->facilityContact->phone }}
                            @endif
                            @if(isset($facility->facilityContact->mobile))
                                <br />{{ $facility->facilityContact->mobile }}
                            @endif
                            @if(isset($facility->facilityContact->email_address))
                                <br />{{ $facility->facilityContact->email_address }}
                            @endif
                    </td>
                    <td class="align-bottom">
                        <h2 class="no-margin">Medical Record</h2>
                    </td>
              </tr>
          </table>
      </div>
    </div>
@stop
@section('content')
    <div class="row">
      <div class="col-md-12">
          <div class="box box-primary">
            <div  class="box-body no-border table-responsive overflowx-hidden no-more-tables">
              <h1>{{ $patient->first_name }} {{ $patient->moddile_name}} {{ $patient->last_name}}</h1>

              <table class="table table-align-top no-border">
                <tr>
                    <td width="55%">
                        <label>Sex</label>
                          <p class="text-muted">
                            {{ getGender($patient->gender) }}
                          </p>
                          <label>Birth Date</label>
                          <p class="text-muted">{{ dateFormat($patient->birthdate,"M. d, Y") }}</p>

                          <label>Age</label>
                          <p class="text-muted">{{ getAge($patient->birthdate) }} years old</p>

                        @if($patient->patientContact->email)
                          <label>Email Address</label>
                          <p class="text-muted">{{ $patient->patientContact->email }}</p>
                        @endif
                        @if($patient->patientContact->mobile)
                          <label>Mobile No.</label>
                          <p class="text-muted">{{ $patient->patientContact->mobile }}</p>
                        @endif
                        @if($patient->patientContact->street_address)
                          <label>Address</label>
                          <p class="text-muted">{{ $patient->patientContact->street_address }}</p>
                        @endif
                        @if($patient->patientContact->barangay)
                          <label>Barangay</label>
                          <p class="text-muted">{{ getBrgyName($patient->patientContact->barangay) }}</p>
                        @endif
                          <label>City/Province</label>
                          <p class="text-muted">{{ getCityName($patient->patientContact->city) }} @if($patient->patientContact->province), {{ getProvinceName($patient->patientContact->province) }}@endif</p>

                          <label>Region</label>
                          <p class="text-muted">{{ getRegionName($patient->patientContact->region) }}</p>

                          <label>ZIP Code</label>
                          <p class="text-muted">{{ $patient->patientContact->zip ? $patient->patientContact->zip : '&nbsp;'  }}</p>

                          <label>Country</label>
                          <p class="text-muted">{{ $patient->patientContact->country or "n/a" }}</p>
                    </td>
                    <td width="45%">
                          <label>Status</label>
                          <p class="text-muted red">
                            @if (!empty($patient->deleted_at))
                              Inactive
                            @else
                              Active
                            @endif
                          </p>
                          <?php if(isset($plugs)) {
                              foreach($plugs as $k=>$p) {
                                  if($p['plugin'] == 'Philhealth' AND $p['pdata']) { ?>
                                          <label>PhilHealth No. </label>
                                          <p class="text-muted">
                                            {{ getPHICMemberType($plugs[$k]['pdata']->member_type) }} : {{ $plugs[$k]['pdata']->philhealth_id }}
                                            <br />{{ returnPhilhealthMemberCategory($plugs[$k]['pdata']->philhealth_category) }}
                                            @if($plugs[$k]['pdata']->indigenous_group == 1)
                                              <br />Indigineous <i class="fa fa-check red"></i>
                                            @endif
                                          </p>
                                  <?php }
                                  break;
                              }
                            } ?>
                          <label>Recorded on </label>
                          <p class="text-muted">
                            {{ date('M. d, Y', strtotime($patient->created_at)) }}
                          </p>
                          <label>Recorded by </label>
                          <p class="text-muted">
                            {{ $creator->first_name." ".$creator->last_name }}
                          </p>
                          <p></p>
                          @if($patient->patientEmergencyInfo AND $patient->patientEmergencyInfo->emergency_name != '')
                          <label class="fullwidth">Emergency Contact</label>
                          <p>
                            {{ $patient->patientEmergencyInfo->emergency_name }}<br />
                            {{ $patient->patientEmergencyInfo->emergency_relationship }}<br />
                            {{ $patient->patientEmergencyInfo->phone ? $patient->patientEmergencyInfo->phone."<br>" : NULL }}
                              {{ $patient->patientEmergencyInfo->mobile ? $patient->patientEmergencyInfo->mobile."<br>" : NULL }}
                              {{ $patient->patientEmergencyInfo->emergency_address }}
                          </p>
                          @endif
                    </td>

                </tr>
              </table>
            </div><!-- /.box-body -->
          </div>
          </div>
    </div>

    <div class="row">

      <div class="col-md-12">

      @if(!$patient->patientAllergies->isEmpty())
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-bug"></i> Allergies</h3>
            </div>
            <div class="box-body">
                @foreach($patient->patientAllergies as $allergy)
                <p>{{ $allergy->allergy_id }} : {{ $allergy->allergy_severity }} : {{ Shine\Libraries\Utils\Lovs::getValueOfFieldBy('allergy_reaction', 'allergyreaction_name', 'allergyreaction_id', $allergy->allergy_reaction_id) }}</p>
                @endforeach
            </div>
          </div>
      @endif

      @if($patient->patientAlert != null)
          <?php $alertt = 0; ?>
        @foreach($patient->patientAlert as $alert)
            @if($alert->alert_type != 'ALLER')
                <?php $alertt = 1; break; ?>
            @endif
        @endforeach
        @if($alertt == 1)
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-bell"></i> Alerts</h3>
            </div>
            <div class="box-body">
                @foreach($patient->patientAlert as $alert)
                    @if($alert->alert_type != 'ALLER')
                        @if($alert->alert_type == 'DISAB' AND $patient->patientDisabilities)
                        <p><i class="fa fa-check"></i> Has Disabilities:
                            <ul>
                        @foreach($patient->patientDisabilities as $disability)
                        <li>{{ Shine\Libraries\Utils\Lovs::getValueOfFieldBy('disabilities', 'disability_name', 'disability_id', $disability->disability_id) }}</li>
                        @endforeach
                            </ul>
                        </p>
                        @else
                        <p><i class="fa fa-check"></i> {{ getAlertName($alert->alert_type) }}</p>
                        @endif
                    @endif
                @endforeach
            </div>
          </div>
        @endif
      @endif

      @if($patient->patientDeathInfo != null)
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-certificate"></i> Death Info</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-12">
          <label>Immediate Cause of Death: <small>(Disease, injury or complication that led directly to death)</small></label><p>{{ $patient->patientDeathInfo->Immediate_Cause_of_Death }}</p>
          <label>Antecedent Cause of Death: <small>(Conditions giving rise to the immediate cause of death)</small></label><p>{{ $patient->patientDeathInfo->Antecedent_Cause_of_Death }}</p>
          <label>Underlying Cause of Death: <small>(Disease or injury that initiated the train of events leading directly to death)</small></label><p>{{ $patient->patientDeathInfo->Underlying_Cause_of_Death }}</p>
          </div>
          <div class="col-md-4">
          <label>Place:</label>
          <p>
              @if($patient->patientDeathInfo->PlaceDeath == 'FB')
                Facility-Based
              @else
                Non-Institutional
              @endif
          </p>
          </div>
          <div class="col-md-4">
          <label>Date:</label><p>{{ date('m/d/Y', strtotime($patient->patientDeathInfo->datetime_death)) }}</p>
          </div>
          <div class="col-md-12">
          <label>Notes:</label><pre>{{ $patient->patientDeathInfo->Remarks }}</pre>
          </div>
        </div>
      </div>
      @endif

      @if(count($patients_monitoring))
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-heartbeat"></i> Patient Monitoring</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
          </div><!-- /.box-header -->
          <div class="box-body no-border table-responsive no-padding overflowx-hidden no-more-tables">
              <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Blood Pressure</th>
                      <th>Blood Pressure Assessment</th>
                      <th>Created At</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach ($patients_monitoring as $mkey => $mvalue)
                      <tr>
                        <td data-table="Blood Pressure"> {{ $mvalue->bloodpressure_systolic }} / {{ $mvalue->bloodpressure_diastolic }}</td>
                        <td data-table="BP Assessment"> {{ $mvalue->bloodpressure_assessment_name }} </td>
                        <td data-table="Date">{{ date('M. d, Y', strtotime($mvalue->created_at)) }}</td>
                      </tr>
                    @endforeach
                </tbody>
              </table>
          </div><!-- /.box-body -->
        </div>
      @endif

      @if($patient->patientMedicalHistory)
        <?php $medhistory = FALSE; ?>
        @foreach($patient->patientMedicalHistory as $history)
            @if($history->disease_status != NULL OR $history->disease_status != "")
                <?php $medhistory = TRUE; ?>
            @endif
        @endforeach
        @if($medhistory == TRUE)
      <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-history"></i> Medical History</h3>
            </div>
            <div class="box-body">
                @foreach($patient->patientMedicalHistory as $history)
                    @if($history->disease_status != NULL OR $history->disease_status != "")
                    <p><label>{{ Shine\Libraries\Utils\Lovs::getValueOfFieldBy('diseases', 'disease_name', 'disease_id', $history->disease_id) }}</label> : {{ $history->disease_status }}</p>
                    @endif
                @endforeach
            </div>
      </div>
        @endif
      @endif

      @if($hc_history)
      <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-calendar-check-o"></i> Consultation History</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body no-border table-responsive no-padding overflowx-hidden">
                  <table class="table table-striped table-heading table-align-top">
                    <tbody><tr>
                      <th>Consultation Date</th>
                      <th>Clinical Service</th>
                      <th>Diagnosis</th>
                      <th>Type</th>
                      <th>Orders </th>
                      <th>Status</th>
                      <th>Physician</th>
                    </tr>
                    <?php foreach($hc_history as $history) { ?>
                    <tr class='row-clicker'>
                      <td>{{ date('m/d/y', $history['dater']) }}</td>
                      <td>{{ getHealthcareServiceName($history['type']) }}</td>
                      <td>{{ $history['diagnosis_type'] }}</td>
                      <td>{{ getConsultTypeName($history['consultype']) }}</td>
                      <td align="center">
                        <?php
                          $ord = NULL;
                          if($history['orders']) {
                              foreach($history['orders'] as $order) {
                                  if($ord != $order->medicalorder_type) {
                                      switch($order->medicalorder_type){
                                          case "MO_MED_PRESCRIPTION": echo "<span class='fa fa-pencil'></span> "; break;
                                          case "MO_LAB_TEST": echo "<span class='fa fa-flask'></span> "; break;
                                      }
                                      $ord = $order->medicalorder_type;
                                  }
                              }
                          }
                        ?>
                      </td>
                      <td align="center">
                          <?php
                            if($history['disposition'] AND $history['disposition']->disposition):
                                echo "<span class='fa fa-lock'></span>";
                            else:
                                echo "<span class='fa fa-unlock'></span>";
                            endif;
                          ?>
                      </td>

                        <?php if($history['seen']):
                            $seenby = $history['seen']->first_name." ".$history['seen']->last_name;
                        else:
                            $seenby = 'Not seen by doctor';
                        endif; ?>
                      <td>{{ $seenby }}</td>
                    </tr>
                    <?php } ?>
                  </tbody></table>
                </div><!-- /.box-body -->
          </div>
      @endif

      @if(count($MedicalOrderLabExam))
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-flask"></i> Patient Lab History</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body no-border table-responsive no-padding overflowx-hidden">
                    <table class="table table-striped table-align-top">
                        <thead>
                          <tr>
                            <th>Laboratory</th>
                            <th>Date Requested</th>
                            <th>Status</th>
                            <th>Summary results</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($MedicalOrderLabExam as $lab_key => $lab_value)
                            <tr>
                              <td>
                                  @if( isset($lov_laboratories[$lab_value->laboratory_test_type]) )
                                  {{ $lov_laboratories[$lab_value->laboratory_test_type] }}
                                  @else
                                  {{ $lab_value->laboratory_test_type }}
                                  @endif
                              </td>
                              <td>
                                 {{ date('M d, Y', strtotime($lab_value->created_at)) }}
                              </td>
                              <td>
                                @if($lab_value->LaboratoryResult!=NULL)
                                  <?php $disabled_upload = TRUE; ?>
                                  <span class="">Results Uploaded:<br />{{ date('M d, Y h:i A', strtotime($lab_value->LaboratoryResult->created_at)) }}
                                      </span>
                                @else
                                  <?php $disabled_upload = FALSE; ?>
                                  <span class="">No results yet
                                      </span>
                                @endif
                              </td>
                              <td>
                                @if($lab_value->laboratory_test_type =='BR')
                                  <?php $Modal_labType = "lab_completebloodcount"; ?>
                                @elseif($lab_value->laboratory_test_type =='UR')
                                  <?php $Modal_labType = "lab_urinalysis"; ?>
                                @elseif($lab_value->laboratory_test_type =='FE')
                                  <?php $Modal_labType = "lab_fecalysis"; ?>
                                @else
                                  <?php $Modal_labType = "labModal"; ?>
                                @endif


                              </td>
                            </tr>
                          @endforeach
                      </tbody>
                    </table>
                </div><!-- /.box-body -->
          </div>
      @endif

      @if(count($MedicalOrderPrescription))
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-shield"></i> Patient Prescription History</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body no-border table-responsive no-padding overflowx-hidden">
                <table class="table">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Prescription Date</th>
                        <th>Generic</th>
                        <th>Dosage</th>
                        <th>Intake</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php $c = 0; ?>
                      @foreach($MedicalOrderPrescription as $prescription)
                        <tr>
                          <td>
                              <?php $c++; ?> {{ $c }}
                          </td>
                          <td>
                             {{ date('M d, Y', strtotime($prescription->created_at)) }}
                          </td>
                          <td>
                             {{ $prescription->generic_name }}
                          </td>
                          <td>
                             {{ $prescription->dose_quantity }}
                          </td>
                          <td>
                            <?php $Duration_Intake = explode(" ", $prescription->duration_of_intake); ?>
                            <?php
                                $di = isset($Duration_Intake[0]) ? $Duration_Intake[0] : NULL;
                                $dio = isset($Duration_Intake[1]) ? $Duration_Intake[1] : NULL;
                                $din = isset($Duration_Intake[1]) ? getIntakeName($Duration_Intake[1]) : NULL;
                            ?>
                              @if($prescription->duration_of_intake != ' C')
                                {{ $di." ".$din }}
                                @else
                                {{ $di." ".$din }}
                                @endif
                             {{ " - ".getRegimenName($prescription->dosage_regimen) }}
                          </td>
                        </tr>

                      @endforeach
                  </tbody>
                </table>
            </div><!-- /.box-body -->
          </div>
      @endif
      </div>
    </div>

    <div class="row hidden-print">
        <div class="col-md-12">
            <button class="btn btn-success" onclick="location.href='{{ url('patients/'.$patient->patient_id) }}'">Close print</button>
        </div>
    </div>
@stop
