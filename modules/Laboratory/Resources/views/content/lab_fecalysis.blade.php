<?php
  $data['FE_Color'] = $data['FE_Consistency'] = $data['FE_Mucus'] = $data['FE_Occult_Blood'] = $data['FE_Bile'] = $data['FE_Others'] = $data['FE_REMARKS'] = $data['FE_Ascaris_lumbricoides'] = $data['FE_Tricuris_tricura'] = $data['FE_Hookworm'] = $data['FE_Amoeba_1'] = $data['FE_Amoeba_2'] = $data['FE_Amoeba_3'] = $data['FE_RBC'] = $data['FE_Pus_Cells'] = $data['FE_Yeast_like_Cells'] = $data['FE_Fat_globules'] = $lab_filename = $disabled = NULL;
  $lab_id = $medlab_id = NULL;

  if(isset($lab['FE']) AND $lab['FE']) {
    $disabled = 'disabled';
    $json_lab_data = json_decode($lab['FE']->lab_data);
    $lab_filename = $lab['FE']->filename;
    $lab_id = $lab['FE']->laboratoryresult_id;
    if($json_lab_data) {
        foreach ($json_lab_data as $key => $value) {
          $data[$key] = $value;
        }
    }
  }
if(isset($lab['FE'])) {
    $medlab_id = $lab['FE']->medicalorderlaboratoryexam_id;
  }
  if(isset($medicalorderlaboratoryexam_id)) {
    $medlab_id = $medicalorderlaboratoryexam_id;
  }

?>
<input type="hidden" name="medicalorderlaboratoryexam_id" id="medicalorderlaboratoryexam_id" value="{{ $medlab_id }}"/>

@if(isset($lab['FE']->laboratoryresult_id))
<input type="hidden" name="laboratoryresult_id" id="laboratoryresult_id" value="{{ $lab['FE']->laboratoryresult_id }}"/>
@endif

<div class="row">
  <div class="box no-border">
      <div class="col-sm-12">
        <div class="box-header with-border">
          <h3 class="box-title control-label">Fecalysis</h3>
        </div>
      </div>
      <div class="col-sm-12">
          <div class="col-sm-12">
              <h4>Add Attachments</h4>
              <p>Upload an attachment or fill out the form below.</p>
              <input type="file" name="lab_file_upload" id="lab_file_upload">
          </div>
          <div class="clearfix"></div>
          <hr />
      </div>
      <!-- <div class="box-body"> -->
      <div class="col-md-6">
            <div class="col-sm-12">
              <label class="col-sm-12">Macroscopic</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Color</label>
              <div class="col-sm-8">
                {!!Form::text('FE_Color', $data['FE_Color'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label"></label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Consistency</label>
              <div class="col-sm-8">
                {!!Form::text('FE_Consistency', $data['FE_Consistency'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label"></label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Mucus</label>
              <div class="col-sm-8">
                {!!Form::text('FE_Mucus', $data['FE_Mucus'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label"></label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-12">Chemical</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Occult Blood</label>
              <div class="col-sm-8">
                {!!Form::text('FE_Occult_Blood', $data['FE_Occult_Blood'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label"></label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Bile</label>
              <div class="col-sm-8">
                {!!Form::text('FE_Bile', $data['FE_Bile'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label"></label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Others</label>
              <div class="col-sm-8">
                {!!Form::text('FE_Others', $data['FE_Others'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label"></label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-12">REMARKS</label>
            </div>
            <div class="col-sm-12">
              <div class="col-sm-12">
                {!!Form::textarea('FE_REMARKS', $data['FE_REMARKS'], ['class' => 'form-control', $disabled]); !!}
              </div>
            </div>
          </div>
      <div class="col-md-6">
            <div class="col-sm-12">
              <label class="col-sm-12">Macroscopic</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-12">Parasites:</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Ascaris lumbricoides</label>
              <div class="col-sm-8">
                {!!Form::text('FE_Ascaris_lumbricoides', $data['FE_Ascaris_lumbricoides'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label">/Lpf</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Tricuris tricura</label>
              <div class="col-sm-8">
                {!!Form::text('FE_Tricuris_tricura', $data['FE_Tricuris_tricura'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label">/Lpf</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Hookworm</label>
              <div class="col-sm-8">
                {!!Form::text('FE_Hookworm', $data['FE_Hookworm'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label">/Lpf</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-12 control-label">Amoeba</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label"></label>
              <div class="col-sm-8">
                {!!Form::text('FE_Amoeba_1', $data['FE_Amoeba_1'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label">/Hpf</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label"></label>
              <div class="col-sm-8">
                {!!Form::text('FE_Amoeba_2', $data['FE_Amoeba_2'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label">/Hpf</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label"></label>
              <div class="col-sm-8">
                {!!Form::text('FE_Amoeba_3', $data['FE_Amoeba_3'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label">/Hpf</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-12">Cellular Elements:</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">RBC</label>
              <div class="col-sm-8">
                {!!Form::text('FE_RBC', $data['FE_RBC'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label"></label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Pus Cells</label>
              <div class="col-sm-8">
                {!!Form::text('FE_Pus_Cells', $data['FE_Pus_Cells'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label"></label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Yeast like Cells</label>
              <div class="col-sm-8">
                {!!Form::text('FE_Yeast_like_Cells', $data['FE_Yeast_like_Cells'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label"></label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Fat globules</label>
              <div class="col-sm-8">
                {!!Form::text('FE_Fat_globules', $data['FE_Fat_globules'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label"></label>
            </div>
          </div>
  </div>
</div>

@if($lab_filename!=NULL)
<div class="row">
  <div class="box no-border">
    <div class="col-sm-12">
      <div class="box-header with-border">
        <h3 class="box-title control-label"> Attachments</h3>
      </div>
    </div>
    <div class="col-sm-12" style="overflow:hidden;margin-top:20px;">
      <div id="image-gallery" class="col-md-12 cf">
          <?php
              $filenames = explode(';', $lab_filename);
              foreach($filenames as $filename) {
              $ext = substr($filename, -3);
              if($ext == 'png' OR $ext == 'jpg' OR $ext == 'gif') {
            ?>
                <p><a href="#" data-target="{{ url('laboratory/delete_attachment/'.$lab_id.'/'.$filename) }}" data-mod="{{ url('laboratory/modal/FE/'.$lab['FE']->medicalorderlaboratoryexam_id) }}" class="kbd kbd-sm bg-red deletebutton"><i class="fa fa-times"></i></a> {{ $filename }}</p>
                <p><img src="{{ url(uploads_url().'/lab_results/'.$filename) }}"  data-high-res-src="{{ url(uploads_url().'/lab_results/'.$filename) }}" alt="" class="pannable-image col-sm-12"></p>
                <?php } else { ?>
                <p><a href="#" data-target="{{ url('laboratory/delete_attachment/'.$lab_id.'/'.$filename) }}" data-mod="{{ url('laboratory/modal/FE/'.$lab['FE']->medicalorderlaboratoryexam_id) }}" class="kbd kbd-sm bg-red deletebutton"><i class="fa fa-times"></i></a> <a href="{{ url(uploads_url().'/lab_results/'.$filename) }}" target="_blank">{{ $filename }}</a></p>
                <?php } ?>
            <?php } ?>
      </div>
    </div>
  </div>
</div>
@endif
