<?php

return [
    'name' => 'Laboratory',
    'icon' => 'fa-flask',
    'roles' => '["Admin","Doctor Admin","Doctor","Nurse","Midwife","Encoder"]',
    'version' => '1.0',
    'title' => 'Shine Laboratory Module',
    'folder' => 'Laboratory',
    'table' => 'laboratory_result',
    'description' => 'Laboratory',
    'module_user' => 'Rural Health Unit',
    'developer' => 'Ateneo ShineLabs',
    'copy' => '2016',
    'url' => 'www.shine.ph'
];
