<?php

use Plugins\CommunityNursing\CommunityNursingModel;
use Modules\Healthcareservices\Entities\Healthcareservices;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\HealthcareRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\User;
use Shine\Plugin;

class CommunityNursingController extends Controller
{
    protected $moduleName = 'Healthcareservices';
    protected $modulePath = 'healthcareservices';

    public function __construct(HealthcareRepository $healthcareRepository) {
        $this->healthcareRepository = $healthcareRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        //no index
    }

    public function save($data)
    {
        $cn_id = $data['cn_id'];
        $hservice_id = $data['hservice_id'];

        $communitynursing = CommunityNursingModel::submit($data,$hservice_id);
    }
}
