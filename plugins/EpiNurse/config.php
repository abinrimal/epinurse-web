<?php

$plugin_module = 'patients';            //plugin parent module
$plugin_title = 'EpiNurse';            //plugin title
$plugin_id = 'EpiNurse';              //plugin ID
$plugin_location = 'tab';           //UI location where plugin will be accessible
$plugin_primaryKey = 'patient_id';      //primary_key used to find data
$plugin_table = 'epinurse_personal';           //plugintable default; table_name custom table
$plugin_description = 'Additional personal information for EpiNurse';
$plugin_version = '1.0';
$plugin_developer = 'mediXserve';
$plugin_url = 'http://www.medixserve.com';
$plugin_copy = "2018";
$plugin_folder = 'EpiNurse';

?>
