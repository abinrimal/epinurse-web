<?php namespace ShineOS\Core\API\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use ShineOS\Core\Reminders\Http\Controllers\RemindersController as RemindersController;
use ShineOS\Core\Reminders\Entities\Reminders;
use ShineOS\Core\Reminders\Entities\ReminderMessage;
use ShineOS\Core\Facilities\Entities\FacilityUser as facilityUser;
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser as facilityPatientUser;
use ShineOS\Core\Users\Entities\Users;
use ShineOS\Core\Users\Entities\UserContact;
use ShineOS\Core\Patients\Entities\Patients;
use Shine\Libraries\IdGenerator;
use Shine\Libraries\ChikkaSMS;
use Shine\Libraries\EmailHelper;
use Request, DateTime, Validator;

class APIRemindersController extends Controller {
    public function __construct() {
        /**
         * Reminder Types, as Email's subject
         * @var array
         */
        $this->rtype = [
            "1" => "PRESC",
            "2" => "FOLLO",
            "3" => "EXAMR"
        ];
        $this->subject = [
            "1" => "Prescription Schedule",
            "2" => "Follow-up Consultation Appointment",
            "3" => "Lab Exam Results"
        ];
    }

    public function index() {
        return response(json_encode(array('message'=>'No data')), 200);
    }

    public function reminders_get() {
        $data = Request::all();
        if(array_key_exists('facility_id',$data)) {
            $RemindersData = new RemindersController;
            $RemindersData = $RemindersData->listings(array(1,2,3), $data['facility_id']);
            // dd($RemindersData);

            return new JsonResponse((array('status'=>'success','data'=>json_decode($RemindersData['join']->toJson()))), 200);
        } else {
            return new JsonResponse(array('status'=>'error', 'message'=>'Facility id required'), 400);
        }
    }

    public function broadcast_get() {
        $data = Request::all();
        if(array_key_exists('facility_id',$data)) {
            $BroadcastData = new RemindersController;
            $BroadcastData = $BroadcastData->listings(array(4), $data['facility_id']);
            // dd($BroadcastData);
            return new JsonResponse((array('status'=>'success','data'=>json_decode($BroadcastData['join']->toJson()))), 200);
        } else {
            return new JsonResponse(array('status'=>'error', 'message'=>'Facility id required'), 400);
        }
    }

    public function createReminder_get() {
        $data = Request::all();
        if(array_key_exists('patient_id',$data)) {
            $data['Patients'] = Patients::with('patientContact')->where('patient_id', $data['patient_id'])->first();
            if($data['Patients']) {
                if($data['Patients']->patientContact['mobile'] != NULL || $data['Patients']->patientContact['email'] != NULL) {
                    return new JsonResponse((array('status'=>'success','data'=>$data['Patients'])), 200);
                } else {
                    return new JsonResponse(array('status'=>'error', 'message'=>'Error: No contacts found, unable to create a reminder.'), 400);
                }
            } else {
                return new JsonResponse(array('status'=>'error', 'message'=>'Error: Patient not found.'), 400);
            }
        } else {
            return new JsonResponse(array('status'=>'error', 'message'=>'Patient id required'), 400);
        }
    }

    public function insertReminder_post() {
        $data = Request::all();

        $validator = Validator::make($data, [
            'patient_id'			=> 'required',
            'user_id'				=> 'required',
            'facility_id'			=> 'required',
            'reminder_type'     	=> 'required|integer|max:10',
            'message'           	=> 'required|min:5|max:450',
            'appointment_datetime' 	=> 'required|date_format:Y-m-d H:i:s',
            'email'             	=> 'required_without_all:mobile|email',
            'mobile'    			=> 'required_without_all:email|max:20|min:10',
            'daysbeforesending'		=> 'required',
        ]);

        if ($validator->fails()==false) {
            $newId = IdGenerator::generateId();
            $userId = $data['user_id'];
            $facilityId = $data['facility_id'];
            $facilityUserId = facilityUser::where(array('user_id'=>$userId,'facility_id'=>$facilityId))->pluck('facilityuser_id');

            if($facilityUserId) {
                /** Reminders */
                $patientId = $data['patient_id'];
                $type = $data['reminder_type'];

                /** Reminder Messages */
                $datetime = new DateTime($data['appointment_datetime']);
                $datetime = $datetime->format('Y-m-d H:i:s');
                $send_days = $data['daysbeforesending'];
                $mobile = array_key_exists('mobile',$data) ? $data['mobile'] : NULL;
                $email = array_key_exists('email',$data) ? $data['email'] : NULL;
                $message = $data['message'];

                $ReminderMessage = new ReminderMessage;
                $ReminderMessage->remindermessage_id = $newId;
                $ReminderMessage->reminder_message = $message;
                $ReminderMessage->daysbeforesending = $send_days;
                $ReminderMessage->reminder_subject = $this->subject[$type];
                $ReminderMessage->appointment_datetime = $datetime;
                $ReminderMessage->status = '1';
                $ReminderMessage->sent_status = 'SET';
                $ReminderMessage->reminder_type = $type;
                $ReminderMessage->remindermessage_type = $this->rtype[$type];
                $remindermessage_id = $ReminderMessage->remindermessage_id;
                $ReminderMessagesave = $ReminderMessage->save();

                $Reminders = new Reminders;
                $Reminders->reminder_id = $newId;
                $Reminders->facilityuser_id = $facilityUserId;
                $Reminders->patient_id =$patientId;
                $Reminders->user_id = '';

                $Reminders->remindermessage_id = $remindermessage_id;
                $Reminderssave = $Reminders->save();


                if($ReminderMessagesave && $Reminderssave) {

                    /** Email Sending */
                    $patientsDtls = Patients::where('patient_id', $patientId)->first();
                    $patientName =  $patientsDtls['first_name'].' '.$patientsDtls['middle_name'].' '.$patientsDtls['last_name'];
                    if($email) {
                        $sendEmail = $this->sendToEmail($patientName, $email, $this->subject[$type], $message, $datetime, $userId, $facilityId);
                    }
                    /** INSERT SMS CHIKKA */
                    if($mobile) {
                        // $ChikkaSMS = new ChikkaSMS;
                        // $sendText = $ChikkaSMS->sendText($newId, $mobile, $message);
                    }
                    $status = 'success';
                    $flash_type = 200;
                    $flash_message = 'Well done! You have successfully added new reminder.';
                } else {
                    $status = 'error';
                    $flash_type = 400;
                    $flash_message = 'Failed to add';
                }
            } else {
                $status = 'error';
                $flash_type = 400;
                $flash_message = 'Invalid facility or user id.';
            }
            return new JsonResponse(array('status'=>$status, 'message'=> $flash_message), $flash_type);
        } else {
             return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
        }
    }

    public function insertBroadcast_post() {
        $data = Request::all();

        $validator = Validator::make($data, [
            'patient_id'			=> 'required',
            'user_id'				=> 'required',
            'facility_id'			=> 'required',
            'reminder_type'			=> 'required',
            'message'           	=> 'required|min:5|max:450',
            'subject' 				=> 'required',
        ]);

        if ($validator->fails()==false) {
            $newId = IdGenerator::generateId();
            $userId = $data['user_id'];
            $facilityId = $data['facility_id'];
            $facilityUserId = facilityUser::where(array('user_id'=>$userId,'facility_id'=>$facilityId))->pluck('facilityuser_id');
            if($facilityUserId) {
                $patientId = $data['patient_id'];
                $reminder_type = $data['reminder_type'];
                $message = $data['message'];
                $subject = $data['subject'];

                if($reminder_type == 'BROADCAST_PATIENTS') {
                    $List = facilityPatientUser::where('facilityuser_id',$facilityUserId)->select('patient_id as id')->get();
                } else {
                    $List = facilityUser::where('facility_id', $facilityId)->select('user_id as id')->get();
                }
                // echo "<pre>"; print_r($List->count()); echo "</pre>";

                if($List->count() > 0) {
                    $ReminderMessage = new ReminderMessage;
                    $ReminderMessage->remindermessage_id = $newId;
                    $ReminderMessage->reminder_message = $message;
                    $ReminderMessage->reminder_subject = $subject;
                    $ReminderMessage->reminder_type = '4'; //broadcast
                    $ReminderMessage->remindermessage_type = $reminder_type;
                    $ReminderMessage->status = 1;
                    $ReminderMessage->sent_status = 'SENT';

                    $remindermessage_id = $ReminderMessage->remindermessage_id;
                    $Reminderssave = $ReminderMessage->save();
                    // echo "<pre>"; print_r($ReminderMessage); echo "</pre>";

                    foreach ($List as $key => $value) {
                        $Reminders = new Reminders();
                        $Reminders->reminder_id = $newId.$key;
                        $Reminders->facilityuser_id = $facilityUserId;

                        /** Broadcast to Patients or to User */
                        if($reminder_type == 'BROADCAST_PATIENTS') {
                            /** Patients List
                             * Insert to Patient Id field */
                            $Reminders->patient_id = $value['id'];
                            $Reminders->user_id = '';
                            $emailDetails = Patients::where('patients.patient_id',$value['id'])
                                                    ->leftjoin('patient_contact', 'patients.patient_id','=','patient_contact.patient_id')
                                                    ->select('patients.first_name', 'patients.last_name', 'patient_contact.email', 'patient_contact.mobile')
                                                    ->first();
                            $numberDetails = $emailDetails->mobile;
                        } else {
                            /** Users List
                             * Insert to User Id field */
                            $Reminders->patient_id = '';
                            $Reminders->user_id = $value['id'];
                            $emailDetails = Users::where('user_id',$value['id'])->first();
                            $num = UserContact::where('user_id',$value['id'])->first();
                            $numberDetails = $num->mobile;
                        }


                        $Reminders->remindermessage_id = $remindermessage_id;
                        $ReminderMessagesave = $Reminders->save();

                        /** Patients or Users with Email */
                        if($ReminderMessagesave && $Reminderssave) {
                            $sendEmail = NULL;
                            $sendText = NULL;

                            $fullName = $emailDetails->first_name.' '.$emailDetails->middle_name.' '.$emailDetails->last_name;
                            if($numberDetails!=NULL) {
                                // $ChikkaSMS = new ChikkaSMS;
                                // $sendText = $ChikkaSMS->sendText($newId, $numberDetails, $message);
                            }
                            if($emailDetails->email) {
                                $sendEmail = $this->sendToEmail($fullName, $emailDetails->email, $subject, $message, NULL, $userId, $facilityId);
                            }

              //               if($sendEmail || $sendText) {
              //                   $updateArr = ['sent_status'=>'sent'];
              //                   $updateStatus = new RemindersController;
                                // $updateStatus = $updateStatus->updateStatus($remindermessage_id, $updateArr);
              //               }
                        }
                    }

                    if($ReminderMessagesave && $Reminderssave) {
                        $status1 = 'success';
                        $flash_type = 200;
                        $flash_message = 'Well done! You successfully added new broadcast.';
                    } else {
                        $status1 = 'error';
                        $flash_type = 400;
                        $flash_message = 'Failed to add';
                    }
                } else {
                    $status1 = 'error';
                    $flash_type = 400;
                    $flash_message = 'Failed to add. No record found.';
                }
            } else {
                $status1 = 'error';
                $flash_type = 400;
                $flash_message = 'Invalid facility or user id.';
            }

            return new JsonResponse(array('status'=>$status1, 'message'=> $flash_message), $flash_type);
        } else {
             return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
        }


    }


    public function sendToEmail($recipientName, $recipientEmail, $subject, $message, $appointment_datetime=NULL, $user_id, $facilityId) {
        $FromUser = Users::getRecordById($user_id);
        $FromFacility = Facilities::where('facility_id',$facilityId)->first();
        $data = array(
                'toUser_name' => $recipientName,
                'toUser_email' => $recipientEmail,
                'subj' => $subject,
                'msg' => $message,
                'appointment_datetime' => $appointment_datetime,
                'fromUser_name' => $FromUser->first_name.' '.$FromUser->middle_name.' '.$FromUser->last_name,
                'fromFacility' => $FromFacility->facility_name
                );
        $response = EmailHelper::SendReminderMessage($data);
        return $response;
    }


}
