
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
        rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        {!! HTML::style('public/dist/plugins/iCheck/square/_all.css') !!}
        <style>
            html, body {
                font-size: 13px;
                height: 100%;
            }
            body, .container {
                width: 100%;
                max-width: 700px;
                margin:auto;
            }
            h1 {
                font-size: 27px;
                margin:5px 0;
            }
            hr {
                margin:8px 0;
            }
            
            .letterhead {
                margin-bottom: 0px;
                padding-bottom: 10px;
            }
            #logos {
                
            }
            #logos img {
                margin:15px 10px 0;
                height:  35px;
            }
            <?php if(strtolower($provider->ownership_type) != "government") { ?>
            #logos h1 {
                position: relative;
                  top: 50%;
                  transform: translateY(-50%);
            }
            <?php } ?>
            #onepage {
                background: url("{{ asset('public/dist/img/rx_symbol_gry.png') }}") center 200px no-repeat;
                min-height:660px;
                overflow: hidden;
                page-break-before: always;
            }
            #footlogo img {
                margin:5px 30px 0;
                height: 25px;
            }
            #section-header {
                border-bottom: 2px solid #CCC;
                margin-bottom: 10px;
                padding-bottom: 10px;
            }
            #section-body {
                min-height: 23%;
            }
            #section-footer {
                bottom:0;
            }
            #section-footer img {
                margin:0 9px;
            }
            kbd {
                font-size:9px;
                vertical-align:top;
            }
            .bname {
                font-size: 14px;
            }
            .section td, .section td p {
                font-size: 12px;
            }
            .checkbox + .checkbox, .radio + .radio {
                margin-bottom: 10px;
                margin-top: 10px;
            }
            .checkbox, .radio {
                margin-bottom: 10px;
                margin-top: 10px;
            }
            .img-circle {
                margin: 0;
                padding: 0;
            }
            @media print {
                #logos img {
                    margin:10px 5px 0;
                    height: 20px;
                }   
                #section-footer {
                    position: fixed;
                    bottom:0;
                }
            }
        </style>
    </head>

    <body>
        <?php if($consultation->seen_by == NULL OR $consultation->seen_by =="") { ?>
            <div class="container" style="margin-top:100px;">
            <div class="jumbotron col-md-12">
                <h2>Oops! Not allowed.</h2>
                <p class="lead">This consultation has not been seen/checked by a physician. Please check with the doctor before printing this prescription.</p>
                <p><a class="btn btn-primary btn-lg" href="javascript:window.close();" role="button">Close</a></p>
            </div>
            </div>
        <?php } else { ?>
            <div class="page">
                <div class="letterhead clearfix">
                    <?php if(isset($user->prescription_header) AND $user->prescription_header!=NULL) { 
                        echo $user->prescription_header;
                    } else { ?>
                    <div class="row">
                        <?php if(strtolower($provider->ownership_type) == "government") { ?>
                        <div class="col-md-12" id="logos">
                        <p class="small text-center"><img src="{{ asset('public/dist/img/doh.png') }}" /> <img src="{{ asset('public/dist/img/UHCNew.png') }}" /> <img src="{{ asset('public/dist/img/tsekap.jpg') }}" /> <img src="{{ asset('public/dist/img/philhealth.png') }}" /></p>
                        </div>
                        <?php } ?>

                        <h1 class="text-left col-md-12">
                        <?php
                            $userPhoto = $provider->facility_logo;
                        ?>
                        @if ( $userPhoto != '' )
                            <img src="{{ url( 'public/uploads/profile_picture/'.$userPhoto ) }}" class="img-circle" height="75" />
                        @endif
                        <?php echo $provider->facility_name; ?>
                        </h1>
                        
                    </div>
                    <?php } ?>
                </div>
                <div class="section" id="section-header">
                    <div class="container">
                        <div class="row">
                            <table width="100%">

                                <tr>
                                    <td colspan="3"><strong>Patient Information</strong></td>
                                    <td colspan="2" align="right"><?php echo date("F d, Y"); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="4">Name: <?php echo $patient->first_name." ".$patient->last_name; ?></td>
                                    <td rowspan="3" align="right">
                                        <?php if(isset($user->qrcode) AND $user->qrcode == "1") { ?>
                                        {!! QrCode::margin(2)->size(150)->generate($patqrcode); !!}
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">Address: <?php echo getBrgyName($patient->patientContact->barangay); ?>, <?php echo getCityName($patient->patientContact->city); ?></td>
                                </tr>
                                <tr>
                                    <td width="25%" valign="top">Contact:<br /><?php echo $patient->patientContact->mobile; ?></td>
                                    <td width="25%" valign="top">
                                            PhilHealth#:<br /><?php if(isset($phic->philhealth_id)) echo $phic->philhealth_id; ?><br>
                                            <?php if(isset($phic->philhealth_id)) echo "Member"; ?>
                                            </td>
                                    <td width="25%" valign="top">Sex:<br /><?php echo $patient->gender; ?></td>
                                    <td width="25%" valign="top">Birth:<br /><?php echo date("F d, Y", strtotime($patient->birthdate)); ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="section" id="section-body">
                    <div class="container">
                        <div class="row">
                            <h4>Laboratory Examination Request</h4>
                            <table width="100%">

                                <div class="form-group">
                                    <div class="icheck row" style="margin-bottom:15px;">
                                        @foreach($lovlaboratories as $llabs)
                                                  <?php $selectd = ""; ?>
                                                  @foreach ($labs as $lab_key => $lab_value)
                                                    @if($lab_value->laboratory_test_type == $llabs->laboratorycode)
                                                        <div class="col-md-4 checkbox" style="width:250px;float:left;">
                                                            <label>
                                                                <?php $selectd = "checked='checked'"; ?>
                                                                <i class="fa fa-check-square fa-lg"></i> {{ $llabs->laboratorydescription }} @if($lab_value->laboratory_test_type_others AND $lab_value->laboratory_test_type == 'OT'): {{ $lab_value->laboratory_test_type_others }}@endif
                                                            </label>
                                                        </div>
                                                    @endif
                                                  @endforeach

                                        @endforeach
                                    </div>
                                    <div class="form-group">
                                        <h5>Instructions</h5>
                                        <div class="col-md-12">
                                            {!! $instructions !!}
                                        </div>
                                    </div>
                                </div>

                        </table>
                    </div>
                </div>
                <div class="sectionn" id="section-footer">
                    <div class="container">
                        <div class="row">
                            <table width="100%">
                                <tr>
                                    <td valign="top">
                                        <?php if($consultation->seen_by) {
                                                //try facilityuser_id
                                                $doctor = findUserByUserFacID($consultation->seen_by);
                                                if(!$doctor) {
                                                    //try user_id
                                                    $doctor = findUserByUserID($consultation->seen_by);
                                                }
                                                if($doctor) { ?>
                                            <p><strong><?php echo $doctor->first_name; ?> <?php echo $doctor->last_name; ?>, M.D.<?php if(isset($doctor->professional_titles)) echo ", ".$doctor->professional_titles; ?></strong>
                                                @if(isset($doctor->professional_license_number))<br>License#: <?php echo $doctor->professional_license_number; ?>@endif
                                                @if(isset($doctor->s2))<br>S2#: <?php echo $doctor->s2; ?>@endif
                                                @if(isset($doctor->ptr))<br>PTR#: <?php echo $doctor->ptr; ?>@endif
                                                @if($provider->phic_accr_id)<br>PHIC Accr#: <?php echo $provider->phic_accr_id; ?>@endif
                                            </p>
                                        <?php } } ?>
                                    </td>
                                    <td width="30%" valign="top">
                                        <p><strong><?php echo $provider->facility_name; ?></strong>
                                            <br>{{ isset($provider->facility_contact->street_name) ? $provider->facility_contact->street_name : "" }}
                                            {{ isset($provider->facility_contact->barangay) ? ", ".getBrgyName($provider->facility_contact->barangay) : "" }}{{ isset($provider->facility_contact->municipality) ? ", ".getCityName($provider->facility_contact->municipality) : "" }}
                                            <br><?php if(isset($provider->facility_contact->province)) echo getProvinceName($provider->facility_contact->province).", "; ?><?php if(isset($provider->facility_contact->zip)) echo $provider->facility_contact->zip.", "; ?><?php if(isset($provider->facility_contact->region)) echo getRegionName($provider->facility_contact->region); ?>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="col-md-12" id="footlogo">
                        <p class="small text-center">Powered by <img src="{{ asset('public/dist/img/shine-logo-big.png') }}" /></p>
                    </div>
                </div>
            </div>
        <?php } ?>
    </body>
</html>
{!! HTML::script('public/dist/js/bootbox.min.js') !!}
{!! HTML::script('public/dist/plugins/iCheck/icheck.js') !!}
<script type="text/javascript">
    $('.icheck input').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
      });

    bootbox.alert({
        message: "In order to print this prescription properly, we suggest that you turn off the Headings and Footers settings on the print dialog box.",
        title: 'Prescription Printing'
    });
</script>
