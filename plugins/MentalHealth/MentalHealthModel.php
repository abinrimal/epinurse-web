<?php
namespace Plugins\MentalHealth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class MentalHealthModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mentalhealth_service';
    protected static $table_name = 'mentalhealth_service';
    protected $primaryKey = 'mentalHealth_id';
    protected $touches = array('Healthcareservices');

    public function Healthcareservices() {
        return $this->belongsTo('ShineOS\Core\Healthcareservices\Entities\Healthcareservices', 'healthcareservice_id', 'healthcareservice_id');
    }
}
