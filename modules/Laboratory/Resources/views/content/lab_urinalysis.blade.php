<?php
  $data['UR_Color'] = $data['UR_Transparency'] = $data['UR_Reaction'] = $data['UR_Specific_Gravity'] = $data['UR_Protein'] = $data['UR_Sugar'] = $data['UR_Bile_Pigments'] = $data['UR_Acetone'] = $data['UR_Diabetic_Acid'] = $data['UR_REMARKS'] = $data['UR_RBC'] = $data['UR_Pus_Cells'] = $data['UR_Cast_1'] = $data['UR_Cast_2'] = $data['UR_Cast_3'] = $data['UR_Uric_Acid'] = $data['UR_Calcium_Oxalate'] = $data['UR_amorphous_urates'] = $data['UR_Triple_Phosphate'] = $data['UR_Squamous_epith_cells'] = $data['UR_Renal_Cells'] = $data['UR_Bacteria'] = $data['UR_Others'] = $lab_filename = $disabled = NULL;
  $lab_id = $medlab_id = NULL;

  if(isset($lab['UR']) AND $lab['UR']) {
    //$disabled = 'disabled';
    $json_lab_data = json_decode($lab['UR']->lab_data);
    $lab_filename = $lab['UR']->filename;
    $lab_id = $lab['UR']->laboratoryresult_id;
    foreach ($json_lab_data as $key => $value) {
      $data[$key] = $value;
    }
  }
if(isset($lab['UR'])) {
    $medlab_id = $lab['UR']->medicalorderlaboratoryexam_id;
  }
  if(isset($medicalorderlaboratoryexam_id)) {
    $medlab_id = $medicalorderlaboratoryexam_id;
  }

?>
<input type="hidden" name="medicalorderlaboratoryexam_id" id="medicalorderlaboratoryexam_id" value="{{ $medlab_id }}"/>
@if(isset($lab['UR']->laboratoryresult_id))
<input type="hidden" name="laboratoryresult_id" id="laboratoryresult_id" value="{{ $lab['UR']->laboratoryresult_id }}"/>
@endif
<div class="row">
  <div class="box no-border">
      <div class="col-sm-12">
        <div class="box-header with-border">
          <h3 class="box-title control-label">Urinalysis</h3>
        </div>
      </div>
      <div class="col-sm-12">
      <div class="col-sm-12">
          <h4>Add Attachments</h4>
          <p>Upload an attachment or fill out the form below.</p>
          <input type="file" name="lab_file_upload" id="lab_file_upload">
      </div>
      <div class="clearfix"></div>
    <hr />
    </div>
      <div class="col-md-6">
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Color</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Color', $data['UR_Color'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Transparency</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Transparency', $data['UR_Transparency'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Reaction</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Reaction', $data['UR_Reaction'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label">(ph:)</label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Specific Gravity</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Specific_Gravity', $data['UR_Specific_Gravity'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Protein</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Protein', $data['UR_Protein'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Sugar</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Sugar', $data['UR_Sugar'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-12">Other test:</label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Bile Pigments</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Bile_Pigments', $data['UR_Bile_Pigments'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Acetone</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Acetone', $data['UR_Acetone'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Diabetic Acid</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Diabetic_Acid', $data['UR_Diabetic_Acid'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-12">REMARKS</label>
        </div>
        <div class="col-sm-12">
          <div class="col-sm-12">
            {!!Form::textarea('UR_REMARKS', $data['UR_REMARKS'], ['class' => 'form-control', $disabled]); !!}
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">RBC</label>
          <div class="col-sm-8">
            {!!Form::text('UR_RBC', $data['UR_RBC'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label">/Hpf</label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Pus Cells</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Pus_Cells', $data['UR_Pus_Cells'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label">/Hpf</label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Cast</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Cast_1', $data['UR_Cast_1'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label">/Lpf</label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label"></label>
          <div class="col-sm-8">
            {!!Form::text('UR_Cast_2', $data['UR_Cast_2'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label">/Lpf</label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label"></label>
          <div class="col-sm-8">
            {!!Form::text('UR_Cast_3', $data['UR_Cast_3'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label">/Lpf</label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-12 control-label">Crystals</label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Uric Acid</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Uric_Acid', $data['UR_Uric_Acid'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Calcium Oxalate</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Calcium_Oxalate', $data['UR_Calcium_Oxalate'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Amorphous urates</label>
          <div class="col-sm-8">
            {!!Form::text('UR_amorphous_urates', $data['UR_amorphous_urates'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Triple Phosphate</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Triple_Phosphate', $data['UR_Triple_Phosphate'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Squamous epith cells</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Squamous_epith_cells', $data['UR_Squamous_epith_cells'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Renal Cells</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Renal_Cells', $data['UR_Renal_Cells'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Bacteria</label>
          <div class="col-sm-8">
            {!!Form::text('UR_Bacteria', $data['UR_Bacteria'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
        <div class="col-sm-12">
          <label class="col-sm-4 control-label">Others</label>
          <div class="col-sm-8">
            {!!Form::textarea('UR_Others', $data['UR_Others'], ['class' => 'form-control', $disabled]); !!}
          </div>
          <label class="control-label in-control-label"></label>
        </div>
      </div>
  </div>
</div>
@if($lab_filename!=NULL)
<div class="row">
  <div class="box no-border">
    <div class="col-sm-12">
      <div class="box-header with-border">
        <h3 class="box-title control-label"> Attachments</h3>
      </div>
    </div>
    <div class="col-sm-12" style="overflow:hidden;margin-top:20px;">
      <div id="image-gallery" class="col-md-12 cf">
          <?php
              $filenames = explode(';', $lab_filename);
              foreach($filenames as $filename) {
              $ext = substr($filename, -3);
              if($ext == 'png' OR $ext == 'jpg' OR $ext == 'gif') {
            ?>
                <p><a href="#" data-target="{{ url('laboratory/delete_attachment/'.$lab_id.'/'.$filename) }}" data-mod="{{ url('laboratory/modal/UR/'.$lab['UR']->medicalorderlaboratoryexam_id) }}" class="kbd kbd-sm bg-red deletebutton"><i class="fa fa-times"></i></a> {{ $filename }}</p>
                <p><img src="{{ url(uploads_url().'/lab_results/'.$filename) }}"  data-high-res-src="{{ url(uploads_url().'/lab_results/'.$filename) }}" alt="" class="pannable-image col-sm-12"></p>
                <?php } else { ?>
                <p><a href="#" data-target="{{ url('laboratory/delete_attachment/'.$lab_id.'/'.$filename) }}" data-mod="{{ url('laboratory/modal/UR/'.$lab['UR']->medicalorderlaboratoryexam_id) }}" class="kbd kbd-sm bg-red deletebutton"><i class="fa fa-times"></i></a> <a href="{{ url(uploads_url().'/lab_results/'.$filename) }}" target="_blank">{{ $filename }}</a></p>
                <?php } ?>
            <?php } ?>
      </div>
    </div>
  </div>
</div>
@endif
