<?php
namespace ShineOS\Core\Facilities\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 
use DB;

class FacilityCatchmentArea extends Model {
    use SoftDeletes; 
    protected $table = 'facility_catchment_area';
    protected static $table_name = 'facility_catchment_area'; 
    protected $primaryKey = 'ca_id'; 

    public function facility()
    {
        return $this->belongsTo('ShineOS\Core\Facilities\Entities\Facilities','facility_id');
    }
}
