@section('heads')
{!! HTML::style('public/dist/css/prettify.css') !!}
{!! HTML::style('public/dist/plugins/datepicker/datepicker3.css') !!}
{!! HTML::style('public/dist/plugins/timepicker/bootstrap-timepicker.min.css') !!}
{!! HTML::style('public/dist/plugins/camera/styles.css') !!}
@stop

@extends('patients::layouts.inner')
@section('header-content') Patient's Profile @stop
@section('patient-title') Editing Profile of {{ $patient->first_name }} {{ $patient->middle_name }} {{ $patient->last_name }} @stop

@section('patient-content')
    <style type="text/css" media="screen">
      .help-block {
         margin-top: 11px;
      }
      #screen video {
          margin-left: -62px;
      }
      .modal-header .close {
          margin-top: 0;
          padding: 4px 6px;
      }
    </style>
    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>{{ Session::get('message') }}</strong>
        </div>
    @endif

    <div class="nav-tabs-custom">
      @include('patients::partials.tab_nav')
      {!! Form::model($patient, array('url' => 'patients/'.$patient->patient_id.'/update/','class'=>'form-horizontal', 'enctype'=>"multipart/form-data")) !!}

        <div id="tabContent" class="tab-content">
          @include('patients::pages.forms.basic_info')
          @include('patients::pages.forms.location')
          @include('patients::pages.forms.allergies_alerts')
          @include('patients::pages.forms.history')

          @if($plugs)
              @foreach($plugs as $plug)
                @if($plug['plugin_location'] == 'tab')
                    <?php View::addNamespace('pluginform', plugins_path().$plug['plugin']); ?>
                    @include('pluginform::'.strtolower($plug['plugin']), array('patient' => $patient, strtolower($plug['plugin']) => $plug['pdata']))
                @endif
              @endforeach
          @endif

          @if($plugs)
              @foreach($plugs as $plug)
                @if($plug['plugin_location'] == 'newtab' AND !$plug['pdata'])
                    <?php View::addNamespace('pluginform', plugins_path().$plug['plugin']); ?>
                    @include('pluginform::'.strtolower($plug['plugin']))
                @endif
              @endforeach
          @endif

          @include('patients::pages.forms.notification_settings')
          @include('patients::pages.forms.photos')

        </div><!-- /.tab-content -->

        <div class="mainbuttons col-md-12 textcenter">
            <button type="button" class="btn btn-primary" onclick="location.href='{{ url('/records') }}'">Cancel</button>
            <button type="submit" value="submit" class="btn btn-success">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('scripts')
  {!! HTML::script('public/dist/plugins/camera/webcam.js') !!}
  <div class="modal fade" id="deathModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog ">
        <div class="modal-content">
  @include('patients::pages.forms.modal_death')
        </div>
    </div>
    <!-- end of modal -->
  </div>

    <div class="modal fade" id="camerabox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog ">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Take Photo</h4>
          </div>
          <div class="modal-body" id="modal-body">
                <div id="camera">
                    <div id="screen"></div>
                    <div id="buttons">
                        <div class="buttonPane">
                            <a id="shootButton" href="#" class="blueButton" onClick="preview_snapshot()">Shoot!</a>
                        </div>
                        <div class="buttonPane" style="display:none;">
                            <a id="cancelButton" href="#" class="blueButton" onclick="cancel_preview()">Repeat</a> <a id="uploadButton" href="#" class="greenButton" onclick="save_photo()">Accept</a>
                        </div>
                    </div>
                </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="consentBox" tabindex="-1" role="dialog" aria-labelledby="consentBox" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <button type="button" class="close printConsent"><span aria-hidden="true"><i class="fa fa-print fa-fw"></i></span><span class="sr-only">Print</span></button>
            <h4 class="modal-title" id="consentBox">Patient Consent</h4>
          </div>
          <div class="modal-body" id="printableBox">
                <h2><img src=""><?php
                            $logo = $facility->facility_logo;
                        ?>
                        @if ( $logo != '' )
                            <img src="{{ url( 'public/uploads/profile_picture/'.$logo ) }}" class="img-circle" />
                        @else
                            <img src="{{ asset( 'public/dist/img/no_logo.png' ) }}" class="img-circle" />
                        @endif
                        {{ $facility->facility_name }}
                </h2>

                <h3 class="text-center">Patient Informed Consent for Participation to PHIE</h2>
                <p>&nbsp;</p>
                <p>I, <strong>{{ title_case($patient->first_name." ".$patient->last_name) }}</strong> hereby give my <strong>consent</strong> to <b>{{ $facility->facility_name }}</b> and/or <b>{{ $user->first_name." ".$user->last_name }}</b> in collecting, storing and sending or transmitting my personal and health data to the Philippine Health Information Exchange – Lite (PHIE Lite) for the purpose of PhilHealth’s Primary Care Benefit (PCB) Package and/or the Department of Health’s (DOH’s) National Health Data Reporting Requirements.</p>
                <p>I was made to understand that:</p>
                <ol>
                  <li>All information collected will be kept confidential and it will not be published or shared.</li>
                  <li>All information gathered, stored and transmitted to PhilHealth or DOH will be done thru SHINEOS+, an authorized and recognized EMR provider.</li>
                  <li>Appropriate safety measures have been put in place to protect the privacy and security of my well­being, health information, and other rights under laws governing data privacy and security, and related issuances of the Republic of the Philippines.</li>
                  <li>This consent is valid in PHIE Lite and other DOH National Health Data Reporting Requirements until it is revoked by myself or my duly authorized representative.</li>
                  <li>My participation is voluntary and I can cancel my consent at any time without giving reasons and without any disadvantage effects on my medical treatment and/or services.</li>
                </ol>
                <p>I certify that I have been made to understand my rights in a language and manner understandable to me by a representative of the facility/health care provider and that the health data or information is true and complete to the best of my knowledge.</p>
                <p>Signed this date of {{ date('F') }}, {{ date('Y') }} at {{ date('h:m A') }}.</p><p>&nbsp;</p>
                <p>___________________________________</p>
                <p>Name of Patient/Representative <small>(Signature over printed name)</small></p><p>&nbsp;</p>
                <p>___________________________________</p>
                <p>Representative of Health Facility <small>(Signature over printed name)</small></p><p>&nbsp;</p>
                <p>Contact Number: ______________</p>
          </div>
          <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary printConsent">Print</button>
            </div>
        </div>
      </div>
    </div>

    <script>
      var shutter = new Audio();
      shutter.autoplay = false;
      shutter.src = navigator.userAgent.match(/Firefox/) ? '{{ url("public/dist/plugins/camera") }}/shutter.ogg' : '{{ url("public/dist/plugins/camera") }}/shutter.mp3';

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('span[name="_token"]').attr('content')
          }
      });

      var patientId = "{{ $patient->patient_id }}";

      //Function to the css rule
      function checkSize(){
        if ($(".content-wrapper").width() <= 500 ){
              $('#step_visualization').addClass('hidden');
              $('#tabContent').addClass('no-tab-content');
              $('#tabContent').removeClass('tab-content');
          } else {
              $('#step_visualization').removeClass('hidden');
              $('#tabContent').removeClass('no-tab-content');
              $('#tabContent').addClass('tab-content');
          }
      }

      function togglePane(){
          var visible = $('#camera .buttonPane:visible:first');
          var hidden = $('#camera .buttonPane:hidden:first');

          visible.fadeOut('fast',function(){
              hidden.show();
          });
      }

      function preview_snapshot() {
          shutter.play();
          // freeze camera so user can preview pic
          Webcam.freeze();

          togglePane();
      }

      function cancel_preview() {
          // cancel preview freeze and return to live camera feed
          Webcam.unfreeze();

          togglePane();
      }

      function save_photo() {
          // actually snap photo (from preview freeze) and display it
          Webcam.snap( function(data_uri) {
              // display results in page
              $('#pat_img').css('background-image', 'url(' + data_uri + ')');
              var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
              $('#rawimg').attr('value',raw_image_data);
          });
          togglePane();
          $('#camerabox').modal('hide')
      }

      $(document).ready(function () {
          checkSize();

          $(".masked").inputmask();
          $(".email").inputmask("email");

          show_maiden();

          //webcam.set_api_url('{{ url() }}/patients/uploadCameraPhoto/{{ $patient->patient_id }}');

          $('button.printConsent').on('click', function() {
            $('#printableBox').printThis({
              importStyle: true
            });
          });

          $("#camerabox").on("show.bs.modal", function(e) {
              //display fileupload buttonPane
              $('#fileupload').hide();
              Webcam.set({
                  // live preview size
                  width: 494,
                  height: 370,

                // device capture size
                dest_width: 494,
                dest_height: 370,

                  // device capture size
                  crop_width: 370,
                  crop_height: 370,

                  // format and quality
                  image_format: 'jpeg',
                  jpeg_quality: 90
                });

              Webcam.attach( '#screen' );
              togglePane();
          });

          $(':file').on('fileselect', function(event, numFiles, label) {
              var input = $('#choosenfile'),
                  log = numFiles > 1 ? numFiles + ' files selected' : label;

              if( input.length ) {
                  input.html(log);
              } else {
                  if( log ) alert(log);
              }

          });

      });

      /*$(document).on('show.bs.tab', '.nav-tabs-responsive [data-toggle="tab"]', function(e) {
        var $target = $(e.target);
        var $tabs = $target.closest('.nav-tabs-responsive');
        var $current = $target.closest('li');
        var $parent = $current.closest('li.dropdown');
            $current = $parent.length > 0 ? $parent : $current;
        var $next = $current.next();
        var $prev = $current.prev();
        var updateDropdownMenu = function($el, position){
        $el
            .find('.dropdown-menu')
            .removeClass('pull-xs-left pull-xs-center pull-xs-right')
            .addClass( 'pull-xs-' + position );
        };

        $tabs.find('>li').removeClass('next prev');
        $prev.addClass('prev');
        $next.addClass('next');
        
        updateDropdownMenu( $prev, 'left' );
        updateDropdownMenu( $current, 'center' );
        updateDropdownMenu( $next, 'right' );
      });*/
    </script>
  {!! HTML::script('public/dist/js/pages/patients/patients.js') !!}
@stop
