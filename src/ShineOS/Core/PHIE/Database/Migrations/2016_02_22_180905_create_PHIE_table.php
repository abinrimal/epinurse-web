<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePHIETable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phie_sync', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('sync_id',60);
            $table->string('facility_id',60);
            $table->string('user_id',60);
            
            $table->string('param_type',60);
            $table->string('param_type_id',60);
            $table->string('response_code',60);
            $table->longText('response');

            $table->softDeletes();
            $table->timestamps();
            $table->unique('sync_id');

            $table->foreign('facility_id')
                  ->references('facility_id')
                  ->on('facilities')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('phie_sync');
    }

}
