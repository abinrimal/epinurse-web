<?php
namespace Widgets\analytics\ReportableCases;

use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Shine\Repositories\Eloquent\UserRepository as UserRepository;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\FacilityRepositoryInterface;
use Shine\Libraries\HealthcareservicesHelper;
use Shine\Libraries\FacilityHelper;
use Arrilot\Widgets\AbstractWidget;
use View;

use Plugins\SchoolAccident\SchoolAccidentModel;

//DO NOT FORGET TO UNCOMMENT LOOP WHEN DONE WITH REPOSITORIES
class ReportableCases extends AbstractWidget
{
    private $UserRepository;
    private $healthcareRepository;

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function placeholder()
    {
        $loading = '<div class="box box-primary"><!--Consultations-->
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-cog fa-spin fa-fw"></i> Loading widget...</h3>
                </div>
            </div>';


        return $loading;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(UserRepository $UserRepository, HealthcareRepository $healthcareRepository)
    {
        $start = date('Y-m-01');
        $end = date('Y-m-d', strtotime('last day of this month'));

        $sve = '';
        $ili = '';
        $hfmd = '';

        // Get PE count
        $pe = SchoolAccidentModel::getPERelatedCases($start, $end);
        $pe_total = $pe['total'];

        // Get Cat Related
        $cat = SchoolAccidentModel::getCatRelatedCases($start, $end);
        $cat_count = $cat['total'];

        // Get Count Dengue
        $dengue = getCountOfSpecificDiagnosis($start, $end, 'Dengue');

        // Put into one variable
        $values['pe_total'] = $pe_total;
        $values['dengue'] = $dengue;
        $values['cat'] = $cat_count;
        $values['sve'] = $sve;
        $values['ili'] = $ili;
        $values['hfmd'] = $hfmd;

        View::addNamespace('reportable_cases', 'widgets/analytics/ReportableCases/');
        return view("reportable_cases::index", [
            'config' => $this->config,
            'values' => $values,
        ]);
    }
}
