<div class="box box-primary hidden-xs"><!--Consultations-->
    <div class="box-header">
        <i class="fa fa-area-chart"></i>
        <h3 class="box-title">Analytics Overview</h3>
        <div class="box-tools pull-right">
            <a class="btn bg-teal btn-sm" href="{{ url('reports') }}"><i class="fa fa-pie-chart"></i> View Reports</a>
        </div>
    </div>
    <div class="box-body border-radius-none">
        <?php if($mon) {
            $color[0] = '#FF2E2E';
            $color[1] = '#FFC45F';
            $color[2] = '#00CF18';
            $color[3] = '#39CCCC';
        ?>
            <h4 class="black">Top 4 Diagnosis <span class="small">last 6 months</span></h4>
            <div class="clearfix">
                @foreach($mon as $k=>$m)
                <?php if(isset($m)) { ?>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 text-center" @if($k < 3) style="border-right: 1px solid #f4f4f4" @endif>
                    <input type="text" class="knob" data-max="<?php echo $total; ?>" data-readonly="true" value="<?php echo $m->bilang; ?>" data-width="85" data-height="85" data-fgColor="{{ $color[$k] }}"/>
                    <div class="knob-label"><?php echo $m->diagnosislist_id ? str_limit($m->diagnosislist_id, $limit = 50, $end = '...') : "Not given"; ?></div>
                </div><!-- ./col -->
                <?php } ?>
                @endforeach
            </div><!-- /.row -->
        <?php } ?>

        <?php if($services) { ?>
            <h4>Top 4 Services <span class="small">last 6 months</span></h4>
            <canvas id="servicesChart" height="200"></canvas>
        <?php } else { ?>
            <h4>Not Records Yet</h4>
        <?php } ?>
    </div><!-- /.box-body -->


    <div class="box-footer no-border">

    </div><!-- /.box-footer -->

</div><!--/. end consultations-->



{!! HTML::script('public/dist/plugins/knob/jquery.knob.js') !!}
{!! HTML::script('public/dist/plugins/chartjs/Chart.2.min.js') !!}

<script>
    <?php
    if($services) { ?>
      // Get context with jQuery - using jQuery's .get() method.
      var servicesChartCanvas = $("#servicesChart").get(0).getContext("2d");

      var servicesChartData = {
        labels: [
            <?php
                foreach($ranges as $range) {
                    $dd = date("M/y", strtotime($range));
                    echo "'".$dd."', ";
                }
            ?>
        ],
        datasets: [
            <?php $colors = array('255,0,0','255,172,0','195,255,0','0,255,255','206,220,0','0,129,198','0,159,298','0,229,100','110,19,198','200,129,198','255,0,0','255,172,0','195,255,0','0,255,255','206,220,0','0,129,198','0,159,298','0,229,100','110,19,198','200,129,198'); ?>
            <?php $bil = 1; ?>
            <?php foreach($cs_stats as $cs=>$range) { ?>
            {
                label: "{{ $cs }}",
                data: [
                    <?php foreach($ranges as $rangee) {
                            if(isset($range[$rangee])) {
                                echo $range[$rangee].", ";
                            } else {
                                echo "0, ";
                            }
                    } ?>
                ],
                backgroundColor: [
                        <?php foreach($ranges as $rangee) { ?>
                            "rgba(<?php echo $colors[$bil-1]; ?>,1)",
                        <?php } ?>
                ],
                borderColor: [
                        <?php foreach($ranges as $rangee) { ?>
                            "rgba(<?php echo $colors[$bil-1]; ?>,1)",
                        <?php } ?>
                ],
                borderWidth: 0
            },
            <?php $bil++; ?>
            <?php } ?>
        ]
      };

      var servicesChartOptions = {};

    $(function () {
      //Create the line chart
      var servicesChart = new Chart(servicesChartCanvas, {
          type: "bar",
          data: servicesChartData,
          options: servicesChartOptions
      });
    });

    <?php } ?>

    <?php if($mon) { ?>
    $(".knob").knob({
            draw: function() {

            // "tron" case
            if (this.$.data('skin') == 'tron') {

                var a = this.angle(this.cv)  // Angle
                        , sa = this.startAngle          // Previous start angle
                        , sat = this.startAngle         // Start angle
                        , ea                            // Previous end angle
                        , eat = sat + a                 // End angle
                        , r = true;

                this.g.lineWidth = this.lineWidth;

                this.o.cursor
                        && (sat = eat - 0.3)
                        && (eat = eat + 0.3);

                if (this.o.displayPrevious) {
                    ea = this.startAngle + this.angle(this.value);
                    this.o.cursor
                            && (sa = ea - 0.3)
                            && (ea = ea + 0.3);
                    this.g.beginPath();
                    this.g.strokeStyle = this.previousColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                    this.g.stroke();
                }

                this.g.beginPath();
                this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                this.g.stroke();

                this.g.lineWidth = 2;
                this.g.beginPath();
                this.g.strokeStyle = this.o.fgColor;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                this.g.stroke();

                return false;
            }
        }
    });
    <?php } ?>
</script>

