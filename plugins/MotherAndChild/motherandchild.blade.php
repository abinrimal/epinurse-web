<?php

    $hservice_id = $allData['healthcareserviceid'];
    $mac_id = $data['motherandchild_id'];

$stay_menarche = array(
	'' => 'Please select...',
	0 => 'Own house',
	1 => 'Relative\'s/Family friend\'s house',
	2 => 'Cowshed',
	3 => 'Others'
);

$material_change_frequency = array(
	'' => 'Please select...',
	0 => 'Usually after 4-6 hours',
	1 => 'After every urination and defecation',
	2 => 'After it gets wet',
	3 => 'Once daily',
	4 => 'Others'
);

$sanitarydisposal = array(
	'' => 'Please select...',
	0 => 'Burn',
	1 => 'Burry',
	2 => 'Dispose openly',
	3 => 'Garbage collector',
	4 => 'Others'
);

$antenatalservices = array(
	'' => 'Please select...',
	0 => 'ANC checkup (vital signs, physical examination, abdominal examination)',
	1 => 'Albendazole',
	2 => 'TD immunization',
	3 => 'Iron tablets daily after 3 months',
	4 => 'Counselling on nutrition, hygiene, family planning, rest, exercise, sexual relationship, birth preparedness, etc...',
	5 => 'PMTCT'
);


$healthseeking_behavior = array(
	'' => 'Please select...',
	0 => 'Government health facilities',
	1 => 'Private health facilities',
	2 => 'Traditional birth attendant',
	3 => 'Traditional healers',
	4 => 'Others'
);

$delivery_place = array(
	'' => 'Please select...',
	0 => 'Governmental Health Facility',
	1 => 'Non-governmental Health Facility',
	2 => 'Home',
	3 => 'Others'
);


$delivery_complication_spec = array(
	'' => 'Please select...',
	0 => 'No skilled birth attendant',
	1 => 'Necessary drugs are not available',
	2 => 'No necessary medical supplies',
	3 => 'No means for secondary hospital'
);

$postnatal_services = array(
	'' => 'Please select...',
	0 => 'Physical examination',
	1 => 'Counseling on breastfeeding',
	2 => 'Family planning counseling and services',
	3 => 'Investigations',
	4 => 'Iron tablets',
	5 => 'Others'
);


$breastfeed_times = array(
	'' => 'Please select...',
	0 => '8-10 times',
	1 => '4-5 times',
	2 => '2-3 times',
	3 => 'Once a day',
);

$fp_info_src = array(
	'' => 'Please select...',
	1 => 'Radio',
	2 => 'Television',
	3 => 'Newspaper',
	4 => 'Family/friends/neighbors',
	5 => 'Health personnels',
	6 => 'FCHV',
	7 => 'Health facilities',
);

$breastfeed_start = array(
	'' => 'Please select...',
	0 => 'Immediate after birth',
	1 => 'One hour after birth',
	2 => '2 hours after birth',
	3 => 'One day after birth'
);

$reason_stopbf = array(
	'' => 'Please select...',
	0 => 'Baby had trouble in sucking or latching on',
	1 => 'Starting of complementary feeding',
	2 => 'Returning to work',
	3 => 'Cessation of breast milk',
	4 => 'Next conception',
	5 => 'Others'
);

$bf_times = array(
	'' => 'Please select...',
	0 => '10-12 times',
	1 => '4-5 times',
	2 => '2-3 times',
	3 => 'Once a day',
);

$no_food = array(
	'' => 'Please select...',
	0 => 'Sufficient breast milk production',
	1 => 'Not knowing the exact age of starting complementary feeding',
	2 => 'Lack of food',
	3 => 'Others',
);

?>

<style type="text/css">
	img.epinurse-icon {
		height: 50px;
	}
</style>

{!! Form::hidden('motherandchild[macservice_id]',$mac_id) !!}
{!! Form::hidden('motherandchild[hservice_id]',$hservice_id) !!}

<fieldset>
	<legend> Mother </legend>


	<ul class="nav nav-tabs">
	  <li class="active"><a data-toggle="tab" href="#menstruation">Menstruation</a></li>
	  <li><a data-toggle="tab" href="#pregnancy">Pregnancy</a></li>
	  <li><a data-toggle="tab" href="#childbirth">Childbirth</a></li>
	  <li><a data-toggle="tab" href="#postnatal">Post Natal</a></li>
	  <li><a data-toggle="tab" href="#familyplanning">Family Planning</a></li>
	</ul>

	<div class="tab-content">
	  <div id="menstruation" class="tab-pane fade in active">
	  	<div class="page-header">
	  		 Menstruation
	  	</div>
	  	<div class="icheck">
	  		<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Age of Menarche </label>
		  		<div class="col-sm-3">
		  			{!! Form::number('motherandchild[age_menarche]',isset($data['age_menarche']) ? $data['age_menarche'] : NULL, ['class'=>'form-control']) !!}
		  		</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Where did you stay during your menarche? </label>
		  		<div class="col-sm-3">
		  			{!! Form::select('motherandchild[stay_menarche]', $stay_menarche, isset($data['stay_menarche']) ? $data['stay_menarche'] : NULL, ['class'=>'form-control','id'=>'stay_menarche_select']) !!}
		  		</div>
		  		<div id="stay_menarche_oth_div" class="<?php if (isset($data['stay_menarche']) && $data['stay_menarche'] == 3) {echo '';} else {echo 'hidden';} ?>">
			  		<label class="col-sm-2 control-label"> Specify others </label>
			  		<div class="col-sm-3">
			  			{!! Form::text('motherandchild[stay_menarche_oth]',isset($data['stay_menarche_oth']) ? $data['stay_menarche_oth'] : NULL, ['class'=>'form-control']) !!}
			  		</div>
			  	</div>
		  	</div>
		  	<div class="col-sm-12">
 				<label class="col-sm-4 control-label">Sanitary materials used during menstruation?</label>
 				<div class="col-sm-8">
	 				<label class="checkbox">
	 					{!! Form::checkbox('motherandchild[sanitary_materials_used_pads]',1,(isset($data['sanitary_materials_used_pads']) && $data['sanitary_materials_used_pads'] == 1) ? true : false) !!} <img src="{{ asset('public/uploads/customizations/epinurse/icons/Sanitary-01.png') }}" class="epinurse-icon">
	 				</label>
	 				<label class="checkbox">
	 					{!! Form::checkbox('motherandchild[sanitary_materials_used_tampon]',1,(isset($data['sanitary_materials_used_tampon']) && $data['sanitary_materials_used_tampon'] == 1) ? true : false) !!} <img src="{{ asset('public/uploads/customizations/epinurse/icons/Sanitary-02.png') }}" class="epinurse-icon">
	 				</label>
	 				<label class="checkbox">
	 					{!! Form::checkbox('motherandchild[sanitary_materials_used_clothes]',1,(isset($data['sanitary_materials_used_clothes']) && $data['sanitary_materials_used_clothes'] == 1) ? true : false) !!} Cloths
	 				</label>
	 				<label class="checkbox">
	 					{!! Form::checkbox('motherandchild[sanitary_materials_used_hp]',1,(isset($data['sanitary_materials_used_hp']) && $data['sanitary_materials_used_hp'] == 1) ? true : false) !!} Home pads
	 				</label>
	 				<label class="checkbox">
	 					{!! Form::checkbox('motherandchild[sanitary_materials_used_mhp]',1,(isset($data['sanitary_materials_used_mhp']) && $data['sanitary_materials_used_mhp'] == 1) ? true : false) !!} Modified home pads
	 				</label>
	 				<label class="checkbox">
	 					{!! Form::checkbox('motherandchild[sanitary_materials_used_none]',1,(isset($data['sanitary_materials_used_none']) && $data['sanitary_materials_used_none'] == 1) ? true : false) !!} None
	 				</label>
	 			</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> How often do you change the sanitary materials used? </label>
		  		<div class="col-sm-3">
		  			{!! Form::select('motherandchild[material_change_frequency]', $material_change_frequency, isset($data['material_change_frequency']) ? $data['material_change_frequency'] : NULL, ['class'=>'form-control','id'=>'material_change_frequency_select']) !!}
		  		</div>
		  		<div id="material_change_frequency_oth_div" class="<?php if (isset($data['material_change_frequency']) && $data['material_change_frequency'] == 4) {echo '';} else {echo 'hidden';} ?>">
			  		<label class="col-sm-2 control-label"> Specify others </label>
			  		<div class="col-sm-3">
			  			{!! Form::text('motherandchild[material_change_frequency_oth]',isset($data['material_change_frequency_oth']) ? $data['material_change_frequency_oth'] : NULL, ['class'=>'form-control']) !!}
			  		</div>
			  	</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Reason for not changing timely </label>
		  		<div class="col-sm-3">
		  			{!! Form::text('motherandchild[timeliness_reason]',isset($data['timeliness_reason']) ? $data['timeliness_reason'] : NULL, ['class'=>'form-control']) !!}
		  		</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> How do you dispose the sanitary materials? </label>
		  		<div class="col-sm-3">
		  			{!! Form::select('motherandchild[sanitarydisposal]', $sanitarydisposal, isset($data['sanitarydisposal']) ? $data['sanitarydisposal'] : NULL, ['class'=>'form-control','id'=>'sanitarydisposal_select']) !!}
		  		</div>
		  		<div id="sanitarydisposal_oth_div" class="<?php if (isset($data['sanitarydisposal']) && $data['sanitarydisposal'] == 4) {echo '';} else {echo 'hidden';} ?>">
			  		<label class="col-sm-2 control-label"> Specify others </label>
			  		<div class="col-sm-3">
			  			{!! Form::text('motherandchild[sanitarydisposal_oth]',isset($data['sanitarydisposal_oth']) ? $data['sanitarydisposal_oth'] : NULL, ['class'=>'form-control']) !!}
			  		</div>
			  	</div>
		  	</div>
	  	</div>
	  </div>
	  <div id="pregnancy" class="tab-pane fade">
	  	<div class="page-header">
	  		 Pregnancy
	  	</div>
	  	<div class="icheck">
 			<div class="radio col-sm-12">
 				<label class="col-sm-2 control-label"><img src="{{ asset('public/uploads/customizations/epinurse/icons/EN_ICON_Pregnant-02.png') }}" class="epinurse-icon"> Is pregnant? </label>
 				<div class="col-sm-3">
	 				<label class="radio-inline">
	 					{!! Form::radio('motherandchild[is_pregnant]',0,(isset($data['is_pregnant']) && $data['is_pregnant'] == 0) ? true : false,['id'=>'is_pregnant_no', 'class'=>'is_pregnant_radio']) !!} No 
	 				</label>
	 				<label class="radio-inline">
	 					{!! Form::radio('motherandchild[is_pregnant]',1,(isset($data['is_pregnant']) && $data['is_pregnant'] == 1) ? true : false,['id'=>'is_pregnant_yes', 'class'=>'is_pregnant_radio']) !!} Yes
	 				</label>
	 			</div>
		  		<label class="col-sm-3 control-label"> Age of First Pregnancy </label>
		  		<div class="col-sm-4">
		  			{!! Form::number('motherandchild[age_first_pregnancy]',isset($data['age_first_pregnancy']) ? $data['age_first_pregnancy'] : NULL, ['class'=>'form-control']) !!}
		  		</div>
 			</div>
<!--  			<div class="col-sm-12">
 				<label> Past Obstetric History </label>
 			</div>
 			<div class="col-sm-12">
 				<label class="col-sm-2 control-label"> Gravida </label>
		  		<div class="col-sm-4">
		  			<div class="input-group">
		  				{!! Form::number('motherandchild[past_gravida]',isset($data['past_gravida']) ? $data['past_gravida'] : NULL, ['class'=>'form-control']) !!}
		  				<span class="input-group-addon"> times </span>
		  			</div>
		  		</div>
		  		<label class="col-sm-2 control-label"> Para </label>
		  		<div class="col-sm-4">
		  			<div class="input-group">
		  				{!! Form::number('motherandchild[past_para]',isset($data['past_para']) ? $data['past_para'] : NULL, ['class'=>'form-control']) !!}
		  				<span class="input-group-addon"> times </span>
		  			</div>
		  		</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-2 control-label"> Abortion </label>
		  		<div class="col-sm-4">
		  			<div class="input-group">
		  				{!! Form::number('motherandchild[past_abortion]',isset($data['past_abortion']) ? $data['past_abortion'] : NULL, ['class'=>'form-control']) !!}
		  				<span class="input-group-addon"> times </span>
		  			</div>
		  		</div>
		  		<label class="col-sm-2 control-label"> Living </label>
		  		<div class="col-sm-4">
		  			<div class="input-group">
		  				{!! Form::number('motherandchild[past_living]',isset($data['past_living']) ? $data['past_living'] : NULL, ['class'=>'form-control']) !!}
		  				<span class="input-group-addon"> times </span>
		  			</div>
		  		</div>
 			</div> -->
  			<div class="col-sm-12">
 				<label> Obstetric History </label>
 			</div>
 			<div class="col-sm-12">
 				<label class="col-sm-2 control-label"> Gravida </label>
		  		<div class="col-sm-4">
		  			<div class="input-group">
		  				{!! Form::number('motherandchild[cur_gravida]',isset($data['cur_gravida']) ? $data['cur_gravida'] : NULL, ['class'=>'form-control']) !!}
		  				<span class="input-group-addon"> times </span>
		  			</div>
		  		</div>
		  		<label class="col-sm-2 control-label"> Para </label>
		  		<div class="col-sm-4">
		  			<div class="input-group">
		  				{!! Form::number('motherandchild[cur_para]',isset($data['cur_para']) ? $data['cur_para'] : NULL, ['class'=>'form-control']) !!}
		  				<span class="input-group-addon"> times </span>
		  			</div>
		  		</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-2 control-label"> Abortion </label>
		  		<div class="col-sm-4">
		  			<div class="input-group">
		  				{!! Form::number('motherandchild[cur_abortion]',isset($data['cur_abortion']) ? $data['cur_abortion'] : NULL, ['class'=>'form-control']) !!}
		  				<span class="input-group-addon"> times </span>
		  			</div>
		  		</div>
		  		<label class="col-sm-2 control-label"> Living </label>
		  		<div class="col-sm-4">
		  			<div class="input-group">
		  				{!! Form::number('motherandchild[cur_living]',isset($data['cur_living']) ? $data['cur_living'] : NULL, ['class'=>'form-control']) !!}
		  				<span class="input-group-addon"> times </span>
		  			</div>
		  		</div>
 			</div>
 			<div class="col-sm-12">
 				<label class="col-sm-2 control-label"> Period of Gestation </label>
		  		<div class="col-sm-4">
					<div class="input-group">
		                {!! Form::number('motherandchild[period_gestation]',isset($data['period_gestation']) ? $data['period_gestation'] : NULL, ['class'=>'form-control']) !!}
		                <span class="input-group-addon">weeks</span>
		            </div>
		  		</div>
 			</div>
 			<div class="col-sm-12">
 				<label class="col-sm-2 control-label"> Number of visits to the antenatal clinic </label>
		  		<div class="col-sm-4">
					<div class="input-group">
		                {!! Form::number('motherandchild[times_antenatal]',isset($data['times_antenatal']) ? $data['times_antenatal'] : NULL, ['class'=>'form-control']) !!}
		                <span class="input-group-addon">times</span>
		            </div>
		  		</div>
 			</div>
 			<div class="col-sm-12">
 				<label class="col-sm-2 control-label"> Services received at the antenatal clinic </label>
				<div class="col-sm-10">
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[antenatal_services_anc_check_up]',1, (isset($data['antenatal_services_anc_check_up']) && $data['antenatal_services_anc_check_up'] == 1) ? true : false) !!} ANC checkup (vital signs, physical examination, abdominal examination)
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[antenatal_service_albendazole]',1, (isset($data['antenatal_service_albendazole']) && $data['antenatal_service_albendazole'] == 1) ? true : false) !!} Albendazole
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[antenatal_service_td_immunization]',1, (isset($data['antenatal_service_td_immunization']) && $data['antenatal_service_td_immunization'] == 1) ? true : false) !!} TD immunization
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[antenatal_service_irontabs]',1, (isset($data['antenatal_service_irontabs']) && $data['antenatal_service_irontabs'] == 1) ? true : false) !!} Counselling on nutrition, hygiene, family planning, rest, exercise, sexual relationship, birth preparedness, etc...
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[antenatal_service_pmtct]',1, (isset($data['antenatal_service_pmtct']) && $data['antenatal_service_pmtct'] == 1) ? true : false) !!} PMTCT
					</label>
				    <label class="checkbox">
				    	<div class="form-inline">
				    	{!! Form::checkbox('motherandchild[antenatal_service_others]',1, (isset($data['antenatal_service_others']) && $data['antenatal_service_others'] == 1) ? true : false) !!} Others
				        {!! Form::text('motherandchild[antenatal_service_specify]',isset($data['antenatal_service_specify']) ? $data['antenatal_service_specify'] : NULL, ['class'=>'form-control','id'=>'antenatal_service_specify']) !!} 
				       	</div>
				    </label>
				</div>
 			</div>
 			<div class="col-sm-12">
 				<label class="col-sm-2 control-label"> Health seeking behavior preferred for maternal health services </label>
		  		<div class="col-sm-4">
		  			{!! Form::select('motherandchild[healthseeking_behavior]', $healthseeking_behavior, isset($data['healthseeking_behavior']) ? $data['healthseeking_behavior'] : NULL, ['class'=>'form-control','id'=>'healthseeking_behavior_select']) !!}
		  		</div>
		  		<div id="healthseeking_behavior_oth_div" class="<?php if (isset($data['healthseeking_behavior']) && $data['healthseeking_behavior'] == 4) {echo '';} else {echo 'hidden';} ?>">
			  		<label class="col-sm-2 control-label"> Specify others </label>
			  		<div class="col-sm-4">
			  			{!! Form::text('motherandchild[healthseeking_behavior_oth]',isset($data['healthseeking_behavior_oth']) ? $data['healthseeking_behavior_oth'] : NULL, ['class'=>'form-control']) !!}
			  		</div>
			  	</div>
 			</div>
 			<div class="page-header">
 				Medicines
 			</div>
 			<div class="col-sm-12">
 				<?php 
 					$med_saved = isset($data['medicines']) ? json_decode($data['medicines'],TRUE) : array();

 					$meds_names = array(); 
 					$meds_prescribed = array(); 
 					$meds_stocks = array();
 					if(count($med_saved) > 0)
 					{
	 					foreach ($med_saved as $value) 
	 					{
	 						$meds_names[] = $value['medicine'];
	 						$meds_prescribed[] = $value['prescribed'];
	 						$meds_stocks[] = $value['stock'];
	 					}
 					}

 					$meds_display = array();
 					$meds_display['name'] = $meds_names;
 					$meds_display['prescribed'] = $meds_prescribed;
 					$meds_display['medicine_stock'] = $meds_stocks;

 				?>
				<div class="table-responsive">
				  <table class="table">
				    <thead>
				    	<tr>
				    		<th><label class="control-label">Medicine</label></th>
				    		<th><label class="control-label">Prescribed/Unprescribed</label></th>
				    		<th><label class="control-label">Stock</label></th>
				    		<th></th>
				    	</tr>
				    </thead>
				    <tbody>
			    		@if(count($meds_display['name']) == 0)
				    		<tr class="medicine-row">
				    			<td>{!! Form::text('motherandchild[medicines][name][]',NULL,['class'=>'form-control']) !!}</td>
				    			<td>{!! Form::select('motherandchild[medicines][prescribed][]', [''=>'',0=>'Prescribed',1=>'Unprescribed'],NULL,['class'=>'form-control']) !!}</td>
				    			<td>{!! Form::number('motherandchild[medicines][medicine_stock][]',NULL,['class'=>'form-control']) !!}</td>
				    			<td><button type="button" class="btn btn-sm btn-warning delete-medicine-btn"><i class="fa fa-close"></i> Remove </button></td>
				    		</tr>			    			
			    		@else
			    			@for($i = 0; $i < count($meds_display['name']); $i++)
					    		<tr class="medicine-row">
					    			<td>{!! Form::text('motherandchild[medicines][name][]',isset($meds_display['name']) ? $meds_display['name'][$i] : NULL, ['class'=>'form-control']) !!}</td>
					    			<td>{!! Form::select('motherandchild[medicines][prescribed][]',[''=>'',0=>'Prescribed',1=>'Unprescribed'],isset($meds_display['prescribed']) ? $meds_display['prescribed'][$i] : NULL, ['class'=>'form-control']) !!}</td>
					    			<td>{!! Form::number('motherandchild[medicines][medicine_stock][]',isset($meds_display['medicine_stock']) ? $meds_display['medicine_stock'][$i] : NULL, ['class'=>'form-control']) !!}</td>
					    			<td><button type="button" class="btn btn-sm btn-warning delete-medicine-btn"><i class="fa fa-close"></i> Remove </button></td>
					    		</tr>
					    	@endfor
			    		@endif
				    </tbody>
				  </table>
				</div>
				<div class="col-sm-12">
					<div class="pull-right">
						<button type="button" class="btn btn-success" id="add-medicine-btn" ><i class="fa fa-plus"></i> Add Medicine</button>
					</div>
				</div>
 			</div>
		</div>
	  </div>
	  <div id="childbirth" class="tab-pane fade in">
	  	<div class="icheck">
	  		<div class="page-header">
	  			Childbirth
	  		</div>
	  		<div class="col-sm-12">
				<label class="col-sm-2 control-label"> Age of youngest baby </label>
		  		<div class="col-sm-6">
		  			<div class="input-group">
		  				{!! Form::number('motherandchild[age_young_months]',isset($data['age_young_months']) ? $data['age_young_months'] : NULL, ['class'=>'form-control']) !!}
		  				<span class="input-group-addon"> Months </span>
		  			</div>
		  		</div>
	  		</div>
 			<div class="radio col-sm-12">
 				<label class="col-sm-2 control-label"> Type of Delivery? </label>
 				<div class="col-sm-10">
	 				<label class="radio-inline">
	 					{!! Form::radio('motherandchild[type_of_delivery]',0,(isset($data['type_of_delivery']) && $data['type_of_delivery'] == 0) ? true : false,['id'=>'type_of_delivery_no', 'class'=>'type_of_delivery_radio']) !!} Normal 
	 				</label>
	 				<label class="radio-inline">
	 					{!! Form::radio('motherandchild[type_of_delivery]',1,(isset($data['type_of_delivery']) && $data['type_of_delivery'] == 1) ? true : false,['id'=>'type_of_delivery_yes', 'class'=>'type_of_delivery_radio']) !!} CS
	 				</label>
	 				<label class="radio-inline">
	 					{!! Form::radio('motherandchild[type_of_delivery]',1,(isset($data['type_of_delivery']) && $data['type_of_delivery'] == 1) ? true : false,['id'=>'type_of_delivery_yes', 'class'=>'type_of_delivery_radio']) !!} Assissted Delivery
	 				</label>
	 				<label class="radio-inline">
	 					{!! Form::radio('motherandchild[type_of_delivery]',1,(isset($data['type_of_delivery']) && $data['type_of_delivery'] == 1) ? true : false,['id'=>'type_of_delivery_yes', 'class'=>'type_of_delivery_radio']) !!} Vacuum Delivery
	 				</label>
	 			</div>
 			</div>
	  		<div class="col-sm-12">
				<label class="col-sm-2 control-label"> Place of delivery for youngest baby </label>
		  		<div class="col-sm-4">
		  			{!! Form::select('motherandchild[delivery_place]', $delivery_place, isset($data['delivery_place']) ? $data['delivery_place'] : NULL, ['class'=>'form-control','id'=>'delivery_place_select']) !!}
		  		</div>
		  		<div id="delivery_place_oth_div" class="<?php if (isset($data['delivery_place']) && $data['delivery_place'] == 3) {echo '';} else {echo 'hidden';} ?>">
			  		<label class="col-sm-2 control-label"> Specify others </label>
			  		<div class="col-sm-4">
			  			{!! Form::text('motherandchild[delivery_place_oth]',isset($data['delivery_place_oth']) ? $data['delivery_place_oth'] : NULL, ['class'=>'form-control']) !!}
			  		</div>
			  	</div>
	  		</div>
		  	<div class="radio col-sm-12">
				<label class="col-sm-2 control-label"> Complications during delivery </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('motherandchild[delivery_complication]',0,(isset($data['delivery_complication']) && $data['delivery_complication'] == 0) ? true : false,['id'=>'delivery_complication_no', 'class'=>'delivery_complication_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('motherandchild[delivery_complication]',1,(isset($data['delivery_complication']) && $data['delivery_complication'] == 1) ? true : false,['id'=>'delivery_complication_yes', 'class'=>'delivery_complication_radio']) !!} Yes
					</label>
				</div>
				<div class="<?php if(!isset($data['delivery_complication']) || (isset($data['delivery_complication']) && $data['delivery_complication'] != 1)) { echo 'hidden'; } ?>" id="delivery_complication_spec">
		 			<label class="col-sm-2 control-label">Specify: </label>
		 			<div class="col-sm-3">
		 				{!! Form::text('motherandchild[delivery_complication_spec]',(isset($data['delivery_complication_spec'])) ? $data['delivery_complication_spec'] : NULL, ['class'=>'form-control','id'=>'delivery_complication_spec']) !!}
		 			</div>
	 			</div>
	  		</div>
	  		<div class="col-sm-12">
				<label class="col-sm-2 control-label"> Minimum age gap between kids </label>
		  		<div class="col-sm-4">
	  				{!! Form::number('motherandchild[age_gap_kids]',isset($data['age_gap_kids']) ? $data['age_gap_kids'] : NULL, ['class'=>'form-control']) !!}
		  		</div>
	  		</div>
  		</div>
	  </div>
	  <div id="postnatal" class="tab-pane fade in">
	  	<div class="icheck">
		  	<div class="page-header">
		  		Post Natal
		  	</div>
 			<div class="col-sm-12">
 				<label class="col-sm-4 control-label"> Medical services received during clinic visit after delivery </label>
				<div class="col-sm-8">
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[post_natal_pe]',1, (isset($data['post_natal_pe']) && $data['post_natal_pe'] == 1) ? true : false) !!} Physical examination
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[post_natal_counsel_b_f]',1, (isset($data['post_natal_counsel_b_f']) && $data['post_natal_counsel_b_f'] == 1) ? true : false) !!} Counseling on breastfeeding
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[post_natal_counsel_f_p]',1, (isset($data['post_natal_counsel_f_p']) && $data['post_natal_counsel_f_p'] == 1) ? true : false) !!} Family planning counseling and services
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[post_natal_investigations]',1, (isset($data['post_natal_investigations']) && $data['post_natal_investigations'] == 1) ? true : false) !!} Investigations
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[post_natal_iron_tablets]',1, (isset($data['post_natal_iron_tablets']) && $data['post_natal_iron_tablets'] == 1) ? true : false) !!} Iron tablets
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[post_natal_vitamin_a]',1, (isset($data['post_natal_vitamin_a']) && $data['post_natal_vitamin_a'] == 1) ? true : false) !!} Vitamin A
					</label>
				    <label class="checkbox">
				    	<div class="form-inline">
				    	{!! Form::checkbox('motherandchild[post_natal_oth]',1, (isset($data['post_natal_oth']) && $data['post_natal_oth'] == 1) ? true : false) !!} Others
				        {!! Form::text('motherandchild[post_natal_oth_spec]',isset($data['post_natal_oth_spec']) ? $data['post_natal_oth_spec'] : NULL, ['class'=>'form-control','id'=>'post_natal_oth_spec']) !!} 
				       	</div>
				    </label>
				</div>
 			</div>
		  	<div class="col-sm-12">
				<label class="col-sm-4 control-label">PNC followup attended</label>
				<div class="col-sm-8">
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[pnc_followup_none]',1, (isset($data['pnc_followup_none']) && $data['pnc_followup_none'] == 1 ) ? true : false) !!} None
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[pnc_followup_within24hr]',1, (isset($data['pnc_followup_within24hr']) && $data['pnc_followup_within24hr'] == 1 ) ? true : false) !!} Within 24 hours of delivery
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[pnc_followup_at3rd_day]',1, (isset($data['pnc_followup_at3rd_day']) && $data['pnc_followup_at3rd_day'] == 1 ) ? true : false) !!} In 3rd day after delivery
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[pnc_followup_at7th_day]',1, (isset($data['pnc_followup_at7th_day']) && $data['pnc_followup_at7th_day'] == 1 ) ? true : false) !!} At 7th day after delivery
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[pnc_followup_at28th_day]',1, (isset($data['pnc_followup_at28th_day']) && $data['pnc_followup_at28th_day'] == 1 ) ? true : false) !!} At 28th day after delivery
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[pnc_followup_at45th_day]',1, (isset($data['pnc_followup_at45th_day']) && $data['pnc_followup_at45th_day'] == 1 ) ? true : false) !!} At 45th day after delivery
					</label>
					<label class="checkbox">
						<div class="form-inline">
						{!! Form::checkbox('motherandchild[pnc_followup_others]',1, (isset($data['pnc_followup_others']) && $data['pnc_followup_others'] == 1 ) ? true : false) !!} Other
						{!! Form::text('motherandchild[pnc_followup_specify]',isset($data['pnc_followup_specify']) ? $data['pnc_followup_specify'] : NULL, ['class'=>'form-control','id'=>'pnc_followup_specify']) !!} 
						</div>
					</label>
				</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Food available after delivery</label>
 				<div class="col-sm-3">
	 				<label class="radio-inline">
	 					{!! Form::radio('motherandchild[food_after_delivery]',0,(isset($data['food_after_delivery']) && $data['food_after_delivery'] == 0) ? true : false,['id'=>'food_after_delivery_no', 'class'=>'food_after_delivery_radio']) !!} No 
	 				</label>
	 				<label class="radio-inline">
	 					{!! Form::radio('motherandchild[food_after_delivery]',1,(isset($data['food_after_delivery']) && $data['food_after_delivery'] == 1) ? true : false,['id'=>'food_after_delivery_yes', 'class'=>'food_after_delivery_radio']) !!} Yes
	 				</label>
	 			</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-12 control-label"> EpiNurse assessment of food </label>
		  		<label class="col-sm-4 control-label"> Quality </label>
		  		<div class="col-sm-8">
		  			{!! Form::text('motherandchild[food_after_delivery_quality]',isset($data['food_after_delivery_quality']) ? $data['food_after_delivery_quality'] : NULL, ['class'=>'form-control']) !!}
		  		</div>
		  		<label class="col-sm-4 control-label"> Frequency </label>
		  		<div class="col-sm-8">
		  			{!! Form::text('motherandchild[food_after_delivery_frequency]',isset($data['food_after_delivery_frequency']) ? $data['food_after_delivery_frequency'] : NULL, ['class'=>'form-control']) !!}
		  		</div>
		  		<label class="col-sm-4 control-label"> Distribution </label>
		  		<div class="col-sm-8">
		  			{!! Form::text('motherandchild[food_after_delivery_distribution]',isset($data['food_after_delivery_distribution']) ? $data['food_after_delivery_distribution'] : NULL, ['class'=>'form-control']) !!}
		  		</div>
		  	</div>
 			<div class="radio col-sm-12">
 				<label class="col-sm-4 control-label"> Pre Lactating feeding </label>
 				<div class="col-sm-3">
	 				<label class="radio-inline">
	 					{!! Form::radio('motherandchild[pre_lactating]',0,(isset($data['pre_lactating']) && $data['pre_lactating'] == 0) ? true : false,['id'=>'pre_lactating_no', 'class'=>'pre_lactating_radio']) !!} No 
	 				</label>
	 				<label class="radio-inline">
	 					{!! Form::radio('motherandchild[pre_lactating]',1,(isset($data['pre_lactating']) && $data['pre_lactating'] == 1) ? true : false,['id'=>'pre_lactating_yes', 'class'=>'pre_lactating_radio']) !!} Yes
	 				</label>
	 			</div>
 			</div>
 			<div class="radio col-sm-12">
 				<label class="col-sm-4 control-label"> Dietary restrictions </label>
 				<div class="col-sm-3">
	 				<label class="radio-inline">
	 					{!! Form::radio('motherandchild[dietary_restriction]',0,(isset($data['dietary_restriction']) && $data['dietary_restriction'] == 0) ? true : false,['id'=>'dietary_restriction_no', 'class'=>'dietary_restriction_radio']) !!} No 
	 				</label>
	 				<label class="radio-inline">
	 					{!! Form::radio('motherandchild[dietary_restriction]',1,(isset($data['dietary_restriction']) && $data['dietary_restriction'] == 1) ? true : false,['id'=>'dietary_restriction_yes', 'class'=>'dietary_restriction_radio']) !!} Yes
	 				</label>
	 			</div>
				<div class="<?php if(!isset($data['dietary_restriction']) || (isset($data['dietary_restriction']) && $data['dietary_restriction'] != 1)) { echo 'hidden'; } ?>" id="dietary_restriction_spec">
		 			<label class="col-sm-3 control-label">Specify: </label>
		 			<div class="col-sm-3">
		 				{!! Form::text('motherandchild[dietary_restriction_spec]',(isset($data['dietary_restriction_spec'])) ? $data['dietary_restriction_spec'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
 			</div>
		</div>
	  </div>
	  <div id="familyplanning" class="tab-pane fade in">
	  	<div class="page-header">
	  		 Family Planning
	  	</div>
	  	<div class="icheck">
	  		<div class="radio col-sm-12">
				<label class="col-sm-6 control-label"> Did the mother receive any kind of counseling regarding family planning? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('motherandchild[fp_counseling]',0,(isset($data['fp_counseling']) && $data['fp_counseling'] == 0) ? true : false,['id'=>'fp_counseling_no', 'class'=>'fp_counseling_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('motherandchild[fp_counseling]',1,(isset($data['fp_counseling']) && $data['fp_counseling'] == 1) ? true : false,['id'=>'fp_counseling_yes', 'class'=>'fp_counseling_radio']) !!} Yes
					</label>
				</div>
		  	</div>
	  		<div class="radio col-sm-12">
				<label class="col-sm-6 control-label"> Does the mother use any kind of family planning methods? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('motherandchild[fp_use]',0,(isset($data['fp_use']) && $data['fp_use'] == 0) ? true : false,['id'=>'fp_use_no', 'class'=>'fp_use_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('motherandchild[fp_use]',1,(isset($data['fp_use']) && $data['fp_use'] == 1) ? true : false,['id'=>'fp_use_yes', 'class'=>'fp_use_radio']) !!} Yes
					</label>
				</div>
		  	</div>
			<div class="col-sm-12">
				<label class="col-sm-4 control-label">Type of family planning methods used</label>
				<div class="col-sm-8">
					<label> a. Permanent </label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[permanent_vasectomy]',1, (isset($data['permanent_vasectomy']) && $data['permanent_vasectomy'] == 1) ? true : false) !!} Vasectomy
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[permanent_minilap]',1, (isset($data['permanent_minilap']) && $data['permanent_minilap'] == 1) ? true : false) !!} Minilap hours of delivery
					</label>
					<label> b. Temporary </label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[temporary_natural]',1, (isset($data['temporary_natural']) && $data['temporary_natural'] == 1) ? true : false) !!} Natural method
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[temporary_condom]',1, (isset($data['temporary_condom']) && $data['temporary_condom'] == 1) ? true : false) !!} Condom
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[temporary_depo]',1, (isset($data['temporary_depo']) && $data['temporary_depo'] == 1) ? true : false) !!} Depo (Sangini sui or tin mahine sui)
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[temporary_pills]',1, (isset($data['temporary_pills']) && $data['temporary_pills'] == 1) ? true : false) !!} Pills (Nilikon chakki)
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[temporary_iucd]',1, (isset($data['temporary_iucd']) && $data['temporary_iucd'] == 1) ? true : false) !!} IUCD (Copper T)/PPIUCD
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[temporary_implant]',1, (isset($data['temporary_implant']) && $data['temporary_implant'] == 1) ? true : false) !!} Implant (Norplant)
					</label>
				</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Best source of maternity information </label>
		  		<div class="col-sm-3">
		  			{!! Form::select('motherandchild[fp_info_src]', $fp_info_src, isset($data['fp_info_src']) ? $data['fp_info_src'] : NULL, ['class'=>'form-control','id'=>'fp_info_src_select']) !!}
		  		</div>
		  	</div>
	  	</div>
	  </div>
	</div>
</fieldset>

<fieldset>
	<legend>Child</legend>

	<ul class="nav nav-tabs">
	  <li class="active"><a data-toggle="tab" href="#basicinformation">Basic Information</a></li>
	  <li><a data-toggle="tab" href="#child-breastfeeding">Breast feeding</a></li>
	  <li><a data-toggle="tab" href="#accident">Accident and Injury</a></li>
	  <li><a data-toggle="tab" href="#immunization">Immunization</a></li>
	  <li><a data-toggle="tab" href="#advices">Advices</a></li>
	</ul>

	<div class="tab-content">
	  <div id="basicinformation" class="tab-pane fade in active">
	  	<div class="page-header">
	  		Basic Information
	  	</div>
	  	<div class="col-sm-12">
	  		<label class="col-sm-2 control-label"> Name </label>
	  		<div class="col-sm-4">
	  			{!! Form::text('motherandchild[child_name]',isset($data['child_name']) ? $data['child_name'] : NULL, ['class'=>'form-control']) !!}
	  		</div>
	  		<label class="col-sm-2 control-label"> Unique ID of the child </label>
	  		<div class="col-sm-4">
	  			{!! Form::text('motherandchild[child_id]',isset($data['child_id']) ? $data['child_id'] : NULL, ['class'=>'form-control']) !!}
	  		</div>
	  	</div>
	  	<div class="col-sm-12">
	  		<label class="col-sm-2 control-label"> Date of birth (BS) </label>
	  		<div class="col-sm-4">
	  			{!! Form::text('motherandchild[child_birthdate_bs]',isset($data['child_birthdate_bs']) ? $data['child_birthdate_bs'] : NULL, ['class'=>'form-control','placeholder'=>'MM/DD/YYYY']) !!}
	  		</div>
	  		<label class="col-sm-2 control-label"> Date of birth (AD) </label>
	  		<div class="col-sm-4">
	  			{!! Form::text('motherandchild[child_birthdate_ad]',isset($data['child_birthdate_ad']) ? $data['child_birthdate_ad'] : NULL, ['class'=>'form-control','placeholder'=>'MM/DD/YYYY']) !!}
	  		</div>
	  	</div>
	  	<div class="col-sm-12">
			<label class="col-sm-2 control-label">Sex</label>
	        <div class="col-sm-4">
	            <div class="btn-group toggler" data-toggle="buttons">
	              <label class="btn btn-default @if(isset($data['child_sex']) AND $data['child_sex']==0) active @endif">
	                <i class="fa fa-check"></i> {!! Form::radio('motherandchild[child_sex]', 0, (isset($data['child_sex']) && $data['child_sex'] == 'M') ? true : false, array('class' => 'form-control')) !!} Male
	              </label>
	              <label class="btn btn-default @if(isset($data['child_sex']) AND $data['child_sex']==1) active @endif">
	                <i class="fa fa-check"></i> {!! Form::radio('motherandchild[child_sex]', 1, (isset($data['child_sex']) && $data['child_sex'] == 'F') ? true : false, array('class' => 'form-control')) !!} Female
	              </label>
	            </div>
	        </div>
	  	</div>
	  	<div class="col-sm-8">
	  		<div class="col-sm-12"> Weight </div>
	  		<label class="col-sm-2 control-label"> At birth </label>
	  		<div class="col-sm-4">
	  			<div class="input-group">
	  				{!! Form::text('motherandchild[child_weight_birth]',isset($data['child_weight_birth']) ? $data['child_weight_birth'] : NULL, ['class'=>'form-control']) !!}
	  				<span class="input-group-addon"> kg </span>
	  			</div>
	  		</div>
	  		<label class="col-sm-2 control-label"> Current </label>
	  		<div class="col-sm-4">
	  			<div class="input-group">
	  				{!! Form::text('motherandchild[child_weight_current]',isset($data['child_weight_current']) ? $data['child_weight_current'] : NULL, ['class'=>'form-control']) !!}
	  				<span class="input-group-addon"> kg </span>
	  			</div>
	  		</div>
	  	</div>
	  	<div class="col-sm-4">
	  		<div class="col-sm-12"> Height </div>
	  		<div class="col-sm-12">
	  			<div class="input-group">
	  				{!! Form::text('motherandchild[child_height]',isset($data['child_height']) ? $data['child_height'] : NULL, ['class'=>'form-control']) !!}
	  				<span class="input-group-addon"> cm </span>
	  			</div>
	  		</div>
	  	</div>
	  	<div class="col-sm-12">
	  		<label class="col-sm-4 control-label"> Mid Upper Arm Circumference </label>
	  		<div class="col-sm-4">
	  			<div class="input-group">
	  				{!! Form::text('motherandchild[mid_arm_circumference]',isset($data['mid_arm_circumference']) ? $data['mid_arm_circumference'] : NULL, ['class'=>'form-control']) !!}
	  				<span class="input-group-addon"> cm </span>
	  			</div>
	  		</div>
	  	</div>
		<div class="radio col-sm-12">
			<div class="icheck">
				<label class="col-sm-4 control-label"> Does the child have any congenital anomaly? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('motherandchild[congenital_anomaly]',0,(isset($data['congenital_anomaly']) && $data['congenital_anomaly'] == 0) ? true : false,['id'=>'congenital_anomaly_no', 'class'=>'congenital_anomaly_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('motherandchild[congenital_anomaly]',1,(isset($data['congenital_anomaly']) && $data['congenital_anomaly'] == 1) ? true : false,['id'=>'congenital_anomaly_yes', 'class'=>'congenital_anomaly_radio']) !!} Yes
					</label>
				</div>
				<div class="<?php if(!isset($data['congenital_anomaly']) || (isset($data['congenital_anomaly']) && $data['congenital_anomaly'] != 1)) { echo 'hidden'; } ?>" id="congenital_anomaly_spec">
		 			<label class="col-sm-2 control-label">Specify reason: </label>
		 			<div class="col-sm-4">
		 				{!! Form::text('motherandchild[congenital_anomaly_spec]',(isset($data['congenital_anomaly_spec'])) ? $data['congenital_anomaly_spec'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
 			</div>
  		</div>
		<div class="col-sm-12">
			<label class="col-sm-4 control-label">Health Condition</label>
	        <div class="col-sm-4">
	            <div class="btn-group toggler" data-toggle="buttons">
	              <label class="btn btn-default @if(isset($data['health_condition']) AND $data['health_condition']==0) active @endif">
	                <i class="fa fa-check"></i> {!! Form::radio('motherandchild[health_condition]', 0, (isset($data['health_condition']) && $data['health_condition'] == 0) ? true : false, array('class' => 'form-control')) !!} Healthy
	              </label>
	              <label class="btn btn-default @if(isset($data['health_condition']) AND $data['health_condition']==1) active @endif">
	                <i class="fa fa-check"></i> {!! Form::radio('motherandchild[health_condition]', 1, (isset($data['health_condition']) && $data['health_condition'] == 1) ? true : false, array('class' => 'form-control')) !!} Abnormal
	              </label>
	            </div>
	        </div>
	  	</div>
  		<div class="radio col-sm-12">
  			<div class="icheck">
				<label class="col-sm-5 control-label"> Was your child provided the Vitamin K injection at birth? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('motherandchild[vitamin_k]',0,(isset($data['vitamin_k']) && $data['vitamin_k'] == 0) ? true : false,['id'=>'vitamin_k_no', 'class'=>'vitamin_k_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('motherandchild[vitamin_k]',1,(isset($data['vitamin_k']) && $data['vitamin_k'] == 1) ? true : false,['id'=>'vitamin_k_yes', 'class'=>'vitamin_k_radio']) !!} Yes
					</label>
				</div>
			</div>
  		</div>
	  </div>
	  <div id="child-breastfeeding" class="tab-pane fade in">
	  	<div class="icheck">
		  	<div class="page-header">
		  		Breast feeding
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> When did the mother start breast feeding the baby after delivery?</label>
		  		<div class="col-sm-3">
		  			{!! Form::select('motherandchild[breastfeed_start]', $breastfeed_start, isset($data['breastfeed_start']) ? $data['breastfeed_start'] : NULL, ['class'=>'form-control','id'=>'breastfeed_start_select']) !!}
		  		</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> How often does the mother breastfeed her child in a day?</label>
		  		<div class="col-sm-3">
		  			{!! Form::select('motherandchild[bf_times]', $bf_times, isset($data['bf_times']) ? $data['bf_times'] : NULL, ['class'=>'form-control','id'=>'bf_times_select']) !!}
		  		</div>
		  	</div>
			<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Does/Did the mother practice exclusive breastfeeding? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('motherandchild[exclusive_breastfeeding]',1,(isset($data['exclusive_breastfeeding']) && $data['exclusive_breastfeeding'] == 1) ? true : false,['id'=>'exclusive_breastfeeding_yes', 'class'=>'exclusive_breastfeeding_radio']) !!} Yes
					</label>
					<label class="radio-inline">
						{!! Form::radio('motherandchild[exclusive_breastfeeding]',0,(isset($data['exclusive_breastfeeding']) && $data['exclusive_breastfeeding'] == 0) ? true : false,['id'=>'exclusive_breastfeeding_no', 'class'=>'exclusive_breastfeeding_radio']) !!} No
					</label>
				</div>
				<div class="<?php if(!isset($data['exclusive_breastfeeding']) || (isset($data['exclusive_breastfeeding']) && $data['exclusive_breastfeeding'] != 1)) { echo 'hidden'; } ?>" id="exclusive_breastfeeding_spec">
		 			<label class="col-sm-3 control-label">Specify supplementary food items: </label>
		 			<div class="col-sm-3">
		 				{!! Form::text('motherandchild[exclusive_breastfeeding_spec]',(isset($data['exclusive_breastfeeding_spec'])) ? $data['exclusive_breastfeeding_spec'] : NULL, ['class'=>'form-control','id'=>'exclusive_breastfeeding_spec']) !!}
		 			</div>
	 			</div>
	  		</div>
			<div class="col-sm-12">
				<label class="col-sm-4 control-label"> Age when mother stopped to breastfeed baby </label>
		  		<div class="col-sm-3">
		  			<div class="input-group">
		  				{!! Form::text('motherandchild[age_stop_bf_months]',isset($data['age_stop_bf_months']) ? $data['age_stop_bf_months'] : NULL, ['class'=>'form-control']) !!}
		  				<span class="input-group-addon"> Months </span>
		  			</div>
		  		</div>
	  		</div>
			<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Reason for discontinuation of breastfeeding </label>
		  		<div class="col-sm-3">
		  			{!! Form::select('motherandchild[reason_stopbf]', $reason_stopbf, isset($data['reason_stopbf']) ? $data['reason_stopbf'] : NULL, ['class'=>'form-control','id'=>'reason_stopbf_select']) !!}
		  		</div>
		  		<div id="reason_stopbf_oth_div" class="<?php if (isset($data['reason_stopbf']) && $data['reason_stopbf'] == 5) {echo '';} else {echo 'hidden';} ?>">
			  		<label class="col-sm-2 control-label"> Specify others </label>
			  		<div class="col-sm-3">
			  			{!! Form::text('motherandchild[reason_stopbf_oth]',isset($data['reason_stopbf_oth']) ? $data['reason_stopbf_oth'] : NULL, ['class'=>'form-control']) !!}
			  		</div>
			  	</div>
		  	</div>
		  	<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Was the mother aware that breastfeeding can prevent the child from illness? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('motherandchild[aware_bf]',0,(isset($data['aware_bf']) && $data['aware_bf'] == 0) ? true : false,['id'=>'aware_bf_no', 'class'=>'aware_bf_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('motherandchild[aware_bf]',1,(isset($data['aware_bf']) && $data['aware_bf'] == 1) ? true : false,['id'=>'aware_bf_yes', 'class'=>'aware_bf_radio']) !!} Yes
					</label>
				</div>
	  		</div>
	  		<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Has the mother started the solid/semi-solid feeding to the child? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('motherandchild[solid_feeding]',0,(isset($data['solid_feeding']) && $data['solid_feeding'] == 0) ? true : false,['id'=>'solid_feeding_no', 'class'=>'solid_feeding_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('motherandchild[solid_feeding]',1,(isset($data['solid_feeding']) && $data['solid_feeding'] == 1) ? true : false,['id'=>'solid_feeding_yes', 'class'=>'solid_feeding_radio']) !!} Yes
					</label>
				</div>
	  		</div>
	  		<div class="col-sm-12">
				<label class="col-sm-4 control-label"> Which food does the mother give as complementary? Supplementary? </label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[food_supplement_rice_pudding]', 1, (isset($data['food_supplement_rice_pudding']) && $data['food_supplement_rice_pudding'] == 1) ? true : false) !!} Rice pudding
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[food_supplement_jaulo]', 1, (isset($data['food_supplement_jaulo']) && $data['food_supplement_jaulo'] == 1) ? true : false) !!} Jaulo
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[food_supplement_lito]', 1, (isset($data['food_supplement_lito']) && $data['food_supplement_lito'] == 1) ? true : false) !!} Lito
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[food_supplement_cerelac]', 1, (isset($data['food_supplement_cerelac']) && $data['food_supplement_cerelac'] == 1) ? true : false) !!} Cerelac
					</label>
				    <label class="checkbox" for="food_supplement_other">
				    	<div class="form-inline">
				        	{!! Form::checkbox('motherandchild[food_supplement_oth]', 1, (isset($data['food_supplement_oth']) && $data['food_supplement_oth'] == 1) ? true : false) !!} Other
				        	{!! Form::text('motherandchild[food_supplement_oth_spec]',isset($data['food_supplement_oth_spec']) ? $data['food_supplement_oth_spec'] : NULL, ['class'=>'form-control','id'=>'food_supplement_oth_spec']) !!} 
				        </div>
				    </label>
				</div>
	  		</div>
	  		<div class="col-sm-12">
	  			<label class="col-sm-4 control-label"> If none, state reason for not starting complementary/supplementary food to the child </label>
	  			<div class="col-sm-3">
		  			{!! Form::select('motherandchild[no_food]', $no_food, isset($data['no_food']) ? $data['no_food'] : NULL, ['class'=>'form-control','id'=>'no_food_select']) !!}
		  		</div>
		  		<div id="no_food_oth_div" class="<?php if (isset($data['no_food']) && $data['no_food'] == 3) {echo '';} else {echo 'hidden';} ?>">
			  		<label class="col-sm-2 control-label"> Specify others </label>
			  		<div class="col-sm-3">
			  			{!! Form::text('motherandchild[no_food_oth]',isset($data['no_food_oth']) ? $data['no_food_oth'] : NULL, ['class'=>'form-control']) !!}
			  		</div>
			  	</div>
	  		</div>
		  		<div class="radio col-sm-12">
					<label class="col-sm-4 control-label"> Did the mother wash/clean her hands before breastfeeding? </label>
					<div class="col-sm-3">
						<label class="radio-inline">
							{!! Form::radio('motherandchild[clean_hands_breastfeed]',0,(isset($data['clean_hands_breastfeed']) && $data['clean_hands_breastfeed'] == 0) ? true : false,['id'=>'clean_hands_breastfeed_no', 'class'=>'clean_hands_breastfeed_radio']) !!} No
						</label>
						<label class="radio-inline">
							{!! Form::radio('motherandchild[clean_hands_breastfeed]',1,(isset($data['clean_hands_breastfeed']) && $data['clean_hands_breastfeed'] == 1) ? true : false,['id'=>'clean_hands_breastfeed_yes', 'class'=>'clean_hands_breastfeed_radio']) !!} Yes
						</label>
					</div>
		  		</div>
		  		<div class="radio col-sm-12">
					<label class="col-sm-4 control-label"> Do they have a handwashing facility? </label>
					<div class="col-sm-3">
						<label class="radio-inline">
							{!! Form::radio('motherandchild[handwashing_facility_breastfeed]',0,(isset($data['handwashing_facility_breastfeed']) && $data['handwashing_facility_breastfeed'] == 0) ? true : false,['id'=>'handwashing_facility_breastfeed_no', 'class'=>'handwashing_facility_breastfeed_radio']) !!} No
						</label>
						<label class="radio-inline">
							{!! Form::radio('motherandchild[handwashing_facility_breastfeed]',1,(isset($data['handwashing_facility_breastfeed']) && $data['handwashing_facility_breastfeed'] == 1) ? true : false,['id'=>'handwashing_facility_breastfeed_yes', 'class'=>'handwashing_facility_breastfeed_radio']) !!} Yes
						</label>
					</div>
		  		</div>
			<div class="col-sm-6">
		  		<label class="col-sm-12 control-label"> Details of food items served to the baby </label>
		  		<div class="col-sm-12">
		  			{!! Form::textarea('motherandchild[child_food]',isset($data['child_food']) ? $data['child_food'] : NULL, ['class'=>'form-control','rows'=>5]) !!}
		  		</div>
		  	</div>
		  	<div class="col-sm-6">
		  		<label class="col-sm-12 control-label"> EpiNurse assessment of food </label>
		  		<label class="col-sm-4 control-label"> Quality </label>
		  		<div class="col-sm-8">
		  			{!! Form::text('motherandchild[child_food_quality]',isset($data['child_food_quality']) ? $data['child_food_quality'] : NULL, ['class'=>'form-control']) !!}
		  		</div>
		  		<label class="col-sm-4 control-label"> Frequency </label>
		  		<div class="col-sm-8">
		  			{!! Form::text('motherandchild[child_food_frequency]',isset($data['child_food_frequency']) ? $data['child_food_frequency'] : NULL, ['class'=>'form-control']) !!}
		  		</div>
		  		<label class="col-sm-4 control-label"> Distribution </label>
		  		<div class="col-sm-8">
		  			{!! Form::text('motherandchild[child_food_distribution]',isset($data['child_food_distribution']) ? $data['child_food_distribution'] : NULL, ['class'=>'form-control']) !!}
		  		</div>
		  	</div>
	  	</div>
	  </div>
	  <div id="accident" class="tab-pane fade in">
	  	<div class="icheck">
		  	<div class="page-header">
		  		Accident and Injury
		  	</div>
			<div class="col-sm-12">
				<label class="col-sm-4 control-label"> The types of injuries faced by the child </label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[injury_type_falls]', 1, (isset($data['injury_type_falls']) && $data['injury_type_falls'] == 1) ? true : false) !!} Falls
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[injury_type_drowming]', 1, (isset($data['injury_type_drowming']) && $data['injury_type_drowming'] == 1) ? true : false) !!} Drowning
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[injury_type_burnsscald]', 1, (isset($data['injury_type_burnsscald']) && $data['injury_type_burnsscald'] == 1) ? true : false) !!} Burns/Scald
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[injury_type_poisoning]', 1, (isset($data['injury_type_poisoning']) && $data['injury_type_poisoning'] == 1) ? true : false) !!} Poisoning
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[injury_type_suffocatingchokingaspiration]', 1, (isset($data['injury_type_suffocatingchokingaspiration']) && $data['injury_type_suffocatingchokingaspiration'] == 1) ? true : false) !!} Suffocation, choking, and aspiration
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[injury_type_cut]', 1, (isset($data['injury_type_cut']) && $data['injury_type_cut'] == 1) ? true : false) !!} Cut injury
					</label>
				    <label class="checkbox" for="injury_type_oth_spec">
				    	<div class="form-inline">
				       		{!! Form::checkbox('motherandchild[injury_type_falls]', 1, (isset($data['injury_type_falls']) && $data['injury_type_falls'] == 1) ? true : false, ['id'=>'injury_types_other']) !!} Other
				        	{!! Form::text('motherandchild[injury_types_oth_spec]',isset($data['injury_types_oth_spec']) ? $data['injury_types_oth_spec'] : NULL, ['class'=>'form-control','id'=>'injury_types_oth_spec']) !!} 
				    	</div>
				    </label>
				</div>
	  		</div>
	  		<div class="col-sm-12">
				<label class="col-sm-4 control-label"> Causes of above mentioned injuries </label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[injury_cause_unsafe_home]',1, (isset($data['injury_cause_unsafe_home']) && $data['injury_cause_unsafe_home'] == 1) ? true : false) !!} Unsafe home environment
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[injury_cause_no_supervision]',1, (isset($data['injury_cause_no_supervision']) && $data['injury_cause_no_supervision'] == 1) ? true : false) !!} Lack of supervision of the under five children
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[injury_cause_busy_mother]',1, (isset($data['injury_cause_busy_mother']) && $data['injury_cause_busy_mother'] == 1) ? true : false) !!} Busy mother who works outside the home
					</label>
					<label class="checkbox">
						{!! Form::checkbox('motherandchild[injury_cause_slippery_floor]',1, (isset($data['injury_cause_slippery_floor']) && $data['injury_cause_slippery_floor'] == 1) ? true : false) !!} Slippery floor
					</label>
				    <label class="checkbox" for="injury_cause_oth_spec">
				        {!! Form::checkbox('motherandchild[injury_cause_oth]',1, (isset($data['injury_cause_oth']) && $data['injury_cause_oth'] == 1) ? true : false, ['id'=>'causes_checker_other']) !!} Other
				        {!! Form::text('motherandchild[injury_cause_oth_spec]',isset($data['injury_cause_oth_spec']) ? $data['injury_cause_oth_spec'] : NULL, ['class'=>'form-control','id'=>'injury_cause_oth_spec']) !!} 
				    </label>
				</div>
	  		</div>
  		</div>
	  </div>
	  <div id="immunization" class="tab-pane fade in">
	  	<div class="icheck">
		  	<div class="page-header">
		  		Immunization
		  	</div>
			<div class="table-responsive col-sm-12">
				<table class="table table-bordered">
				  <thead>
				    <tr>
				      <th scope="col">SN</th>
				      <th scope="col">Vaccines</th>
				      <th scope="col">Age of Administration</th>
				      <th scope="col">Remarks</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				    	<td>1</td>
				    	<td>BCG</td>
				    	<td>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_bcg_at_birth]',1, (isset($data['age_bcg_at_birth']) && $data['age_bcg_at_birth'] == 1) ? true :false) !!} At Birth
				    		</label>
				    	</td>
				    	<td>{!! Form::textarea('motherandchild[immu_remarks_bcg]', isset($data['immu_remarks_bcg']) ? $data['immu_remarks_bcg'] : NULL,['class'=>'form-control','rows'=>3]) !!}</td>
				    </tr>
				    <tr>
				    	<td>2</td>
				    	<td>DPT/HepB/HiB</td>
				    	<td>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_dpt_hepb_hib6_weeks]',1, (isset($data['age_dpt_hepb_hib6_weeks']) && $data['age_dpt_hepb_hib6_weeks'] == 1) ? true :false) !!} 6 weeks
				    		</label>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_dpt_hepb_hib10_weeks]',1, (isset($data['age_dpt_hepb_hib10_weeks']) && $data['age_dpt_hepb_hib10_weeks'] == 1) ? true :false) !!} 10 weeks
				    		</label>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_dpt_hepb_hib14_weeks]',1, (isset($data['age_dpt_hepb_hib14_weeks']) && $data['age_dpt_hepb_hib14_weeks'] == 1) ? true :false) !!} 14 weeks
				    		</label>
				    	</td>
				    	<td>{!! Form::textarea('motherandchild[immu_remarks_dpt_hepb_hib]', isset($data['immu_remarks_dpt_hepb_hib']) ? $data['immu_remarks_dpt_hepb_hib'] : NULL,['class'=>'form-control','rows'=>3]) !!}</td>
				    </tr>
				    <tr>
				    	<td>3</td>
				    	<td>OPV (Oral Polio Vaccine)</td>
				    	<td>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_opv6_weeks]',1, (isset($data['age_opv6_weeks']) && $data['age_opv6_weeks'] == 1) ? true :false) !!} 6 weeks
				    		</label>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_opv10_weeks]',1, (isset($data['age_opv10_weeks']) && $data['age_opv10_weeks'] == 1) ? true :false) !!} 10 weeks
				    		</label>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_opv14_weeks]',1, (isset($data['age_opv14_weeks']) && $data['age_opv14_weeks'] == 1) ? true :false) !!} 14 weeks
				    		</label>
				    	</td>
				    	<td>{!! Form::textarea('motherandchild[immu_remarks_opv]', isset($data['immu_remarks_opv']) ? $data['immu_remarks_opv'] : NULL,['class'=>'form-control','rows'=>3]) !!}</td>
				    </tr>
				    <tr>
				    	<td>4</td>
				    	<td>PCV (Pneumococcal Conjugate Vaccine)</td>
				    	<td>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_pcv6_weeks]',1, (isset($data['age_pcv6_weeks']) && $data['age_pcv6_weeks'] == 1) ? true :false) !!} 6 weeks
				    		</label>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_pcv10_weeks]',1, (isset($data['age_pcv10_weeks']) && $data['age_pcv10_weeks'] == 1) ? true :false) !!} 10 weeks
				    		</label>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_pcv9_months]',1, (isset($data['age_pcv9_months']) && $data['age_pcv9_months'] == 1) ? true :false) !!} 9 months
				    		</label>
				    	</td>
				    	<td>{!! Form::textarea('motherandchild[immu_remarks_pcv]', isset($data['immu_remarks_pcv']) ? $data['immu_remarks_pcv'] : NULL,['class'=>'form-control','rows'=>3]) !!}</td>
				    </tr>
				    <tr>
				    	<td>5</td>
				    	<td>IPV (Injectable Polio Vaccine)</td>
				    	<td>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_ipv14_weeks]',1, (isset($data['age_ipv14_weeks']) && $data['age_ipv14_weeks'] == 1) ? true :false) !!}14 weeks
				    		</label>
				    	</td>
				    	<td>{!! Form::textarea('motherandchild[immu_remarks_ipv]', isset($data['immu_remarks_ipv']) ? $data['immu_remarks_ipv'] : NULL,['class'=>'form-control','rows'=>3]) !!}</td>
				    </tr>
				    <tr>
				    	<td>6</td>
				    	<td>Measles and Rubella</td>
				    	<td>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_measles_rubella9_months]',1, (isset($data['age_measles_rubella9_months']) && $data['age_measles_rubella9_months'] == 1) ? true :false) !!} 9 months
				    		</label>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_measles_rubella15_months]',1, (isset($data['age_measles_rubella15_months']) && $data['age_measles_rubella15_months'] == 1) ? true :false) !!} 15 months
				    		</label>
				    	</td>
				    	<td>{!! Form::textarea('motherandchild[immu_remarks_measles_rubella]', isset($data['immu_remarks_measles_rubella']) ? $data['immu_remarks_measles_rubella'] : NULL,['class'=>'form-control','rows'=>3]) !!}</td>
				    </tr>
				    <tr>
				    	<td>7</td>
				    	<td>Japanese Encephalitis</td>
				    	<td>
				    		<label class="checkbox">
				    			{!! Form::checkbox('motherandchild[age_japanese_encephalitis12_months]',1, (isset($data['age_japanese_encephalitis12_months']) && $data['age_japanese_encephalitis12_months'] == 1) ? true :false) !!} 12 months
				    		</label>
				    	</td>
				    	<td>{!! Form::textarea('motherandchild[immu_remarks_japanese_encephalitis]', isset($data['immu_remarks_japanese_encephalitis']) ? $data['immu_remarks_japanese_encephalitis'] : NULL,['class'=>'form-control','rows'=>3]) !!}</td>
				    </tr>
				  </tbody>
				</table>
			</div>
		 </div>
	  </div>
	  <div id="advices" class="tab-pane fade in">
		<div class="page-header">
			Advices
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Breast feeding </label>
			<div class="col-sm-4"> 
				{!! Form::textarea('motherandchild[advise_bf]',(isset($data['advise_bf'])) ? $data['advise_bf'] : NULL,['class'=>'form-control','rows'=>3]) !!}
			</div>
			<label class="col-sm-2 control-label"> Complementary feeding </label>
			<div class="col-sm-4"> 
				{!! Form::textarea('motherandchild[advise_cf]',(isset($data['advise_cf'])) ? $data['advise_cf'] : NULL,['class'=>'form-control','rows'=>3]) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Dental hygiene </label>
			<div class="col-sm-4"> 
				{!! Form::textarea('motherandchild[advise_dh]',(isset($data['advise_dh'])) ? $data['advise_dh'] : NULL,['class'=>'form-control','rows'=>3]) !!}
			</div>
			<label class="col-sm-2 control-label"> Accident prevention </label>
			<div class="col-sm-4"> 
				{!! Form::textarea('motherandchild[advise_ap]',(isset($data['advise_ap'])) ? $data['advise_ap'] : NULL,['class'=>'form-control','rows'=>3]) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Toilet training </label>
			<div class="col-sm-4"> 
				{!! Form::textarea('motherandchild[advise_tt]',(isset($data['advise_tt'])) ? $data['advise_tt'] : NULL,['class'=>'form-control','rows'=>3]) !!}
			</div>
		</div>
		<label> Milestone development </label>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Physical </label>
			<div class="col-sm-4"> 
				{!! Form::textarea('motherandchild[advise_physical]',(isset($data['advise_physical'])) ? $data['advise_physical'] : NULL,['class'=>'form-control','rows'=>3]) !!}
			</div>
			<label class="col-sm-2 control-label"> Motor </label>
			<div class="col-sm-4"> 
				{!! Form::textarea('motherandchild[advise_motor]',(isset($data['advise_motor'])) ? $data['advise_motor'] : NULL,['class'=>'form-control','rows'=>3]) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Verbal </label>
			<div class="col-sm-4"> 
				{!! Form::textarea('motherandchild[advise_verbal]',(isset($data['advise_verbal'])) ? $data['advise_verbal'] : NULL,['class'=>'form-control','rows'=>3]) !!}
			</div>
			<label class="col-sm-2 control-label"> Intellectual </label>
			<div class="col-sm-4"> 
				{!! Form::textarea('motherandchild[advise_intellectual]',(isset($data['advise_intellectual'])) ? $data['advise_intellectual'] : NULL,['class'=>'form-control','rows'=>3]) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Social </label>
			<div class="col-sm-4"> 
				{!! Form::textarea('motherandchild[advise_social]',(isset($data['advise_social'])) ? $data['advise_social'] : NULL,['class'=>'form-control','rows'=>3]) !!}
			</div>
			<label class="col-sm-2 control-label"> Emotional </label>
			<div class="col-sm-4"> 
				{!! Form::textarea('motherandchild[advise_emotional]',(isset($data['advise_emotional'])) ? $data['advise_emotional'] : NULL,['class'=>'form-control','rows'=>3]) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Spiritual/religious </label>
			<div class="col-sm-4"> 
				{!! Form::textarea('motherandchild[advise_spiritual]',(isset($data['advise_spiritual'])) ? $data['advise_spiritual'] : NULL,['class'=>'form-control','rows'=>3]) !!}
			</div>
		</div>
	  </div>
	</div>
</fieldset>

@section('plugin_jsscripts')
	<script type="text/javascript">
		$(document).ready( function(){
			$('#add-medicine-btn').click( function(){
				var prot = $("tr.medicine-row:first").clone(true);
		        prot.find('input').val('');
		        prot.find(".select-gender option:first").attr('selected','selected');
		        console.log(prot);      
		        prot.insertAfter("tr.medicine-row:last");
			});
			$('.delete-medicine-btn').click( function(){
				var master = $(this).closest('tr');
        		master.remove();
			});

			$('.delivery_complication_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#delivery_complication_spec').removeClass('hidden');
				}
				else{
					$('#delivery_complication_spec').addClass('hidden');
				};
			});

			$('.nutritious_food_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#nutritious_food_spec').removeClass('hidden');
				}
				else{
					$('#nutritious_food_spec').addClass('hidden');
				};
			});

			$('.dietary_restriction_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#dietary_restriction_spec').removeClass('hidden');
				}
				else{
					$('#dietary_restriction_spec').addClass('hidden');
				};
			});

			$('.is_breastfed_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#is_breastfed_yes_div').removeClass('hidden');
				}
				else{
					$('#is_breastfed_yes_div').addClass('hidden');
				};
			});

			$('.congenital_anomaly_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#congenital_anomaly_spec').removeClass('hidden');
				}
				else{
					$('#congenital_anomaly_spec').addClass('hidden');
				};
			});

			$('.exclusive_breastfeeding_radio').on('ifClicked', function(event){ 
				if(this.value == 0){
					$('#exclusive_breastfeeding_spec').removeClass('hidden');
				}
				else{
					$('#exclusive_breastfeeding_spec').addClass('hidden');
				};
			});


			$("#stay_menarche_select" ).change(function() {
				if(this.value == 3){
					$('#stay_menarche_oth_div').removeClass('hidden');
				}
				else {
					$('#stay_menarche_oth_div').addClass('hidden');
				}
			});

			$("#material_change_frequency_select").change(function() {
				if(this.value == 4){
					$('#material_change_frequency_oth_div').removeClass('hidden');
				}
				else {
					$('#material_change_frequency_oth_div').addClass('hidden');
				}
			});

			$("#sanitarydisposal_select").change(function() {
				if(this.value == 4){
					$('#sanitarydisposal_oth_div').removeClass('hidden');
				}
				else {
					$('#sanitarydisposal_oth_div').addClass('hidden');
				}
			});

			$("#reason_stopbf_select").change(function() {
				if(this.value == 5){
					$('#reason_stopbf_oth_div').removeClass('hidden');
				}
				else {
					$('#reason_stopbf_oth_div').addClass('hidden');
				}
			});

			$("#healthseeking_behavior_select").change(function() {
				if(this.value == 4){
					$('#healthseeking_behavior_oth_div').removeClass('hidden');
				}
				else {
					$('#healthseeking_behavior_oth_div').addClass('hidden');
				}
			});

			$("#delivery_place_select").change(function() {
				if(this.value == 3){
					$('#delivery_place_oth_div').removeClass('hidden');
				}
				else {
					$('#delivery_place_oth_div').addClass('hidden');
				}
			});

			$("#no_food_select").change(function() {
				if(this.value == 3){
					$('#no_food_oth_div').removeClass('hidden');
				}
				else {
					$('#no_food_oth_div').addClass('hidden');
				}
			});

		});
	</script>
@stop