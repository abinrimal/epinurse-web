var Philhealth = {};

Philhealth.cloneDiv = function ()
{

    $('#addBeneficiary-button').on('click',function (){
        Helper.cloneElement('.cloneDep', $('.parentDivDep'));
    });

    $('.parentDivGuard').on('click', '#addGuardian-button', function (){
        Helper.cloneElement('.cloneGuard', $('.parentDivGuard'));
    });
}

Philhealth.removeDiv = function ()
{

    $('.parentDivDep').on('click', '#removeBeneficiary-button', function () {
        $(this).closest('.cloneDep').remove();
    });

    $('.parentDivGuard').on('click', '#removeGuardian-button', function () {
        $(this).closest('.cloneGuard').remove();
    });

}

Philhealth.changeMember = function ()
{
    // $("#Member_Category").on('change', function(){
    //     var membership = $(this).val();
    //     if(membership == '04') {
    //         $('#ipm_type_form').addClass('hidden'); $('#lifetime_type_form').addClass('hidden'); $('#other_type_form').removeClass('hidden'); $('#ofw_type_form').addClass('hidden');
    //     }
    //     else if(membership == '08' || membership == '09') {
    //         $('#ipm_type_form').addClass('hidden'); $('#lifetime_type_form').addClass('hidden'); $('#other_type_form').removeClass('hidden'); $('#ofw_type_form').addClass('hidden');
    //     }
    //     else if(membership == '13' || membership == '07') {
    //         $('#ipm_type_form').removeClass('hidden'); $('#lifetime_type_form').addClass('hidden'); $('#other_type_form').addClass('hidden'); $('#ofw_type_form').addClass('hidden');
    //     }
    //     else if(membership == '22' || membership == '23') {
    //         $('#ipm_type_form').addClass('hidden'); $('#lifetime_type_form').removeClass('hidden'); $('#other_type_form').addClass('hidden'); $('#ofw_type_form').addClass('hidden');
    //     }
    //     else if(membership == '10' || membership == '11') {
    //         $('#ipm_type_form').addClass('hidden'); $('#lifetime_type_form').addClass('hidden'); $('#other_type_form').addClass('hidden'); $('#ofw_type_form').removeClass('hidden');
    //     }
    //     else if(membership == '01' || membership == '02' || membership == '03') {
    //         $('#ipm_type_form').addClass('hidden'); $('#lifetime_type_form').addClass('hidden'); $('#other_type_form').removeClass('hidden'); $('#ofw_type_form').addClass('hidden');
    //     }
    //     else {
    //         $('#ipm_type_form').addClass('hidden'); $('#lifetime_type_form').addClass('hidden'); $('#other_type_form').addClass('hidden'); $('#ofw_type_form').addClass('hidden');
    //     }
    // });
    

    $("#Member_Category").on('change', function(){
        var membership = $(this).val();
        if(membership == '12') { //InfEco_InfSector
          $('#SponsoredOthers').addClass('hidden');
          $('#NGA').addClass('hidden');
          $('#LGU').addClass('hidden');
          $('#InfEco_OrgGroup').addClass('hidden');
          $('#InfEco_InfSector').removeClass('hidden');
          $('#self_earning').addClass('hidden');
          $('#lifetime_type_form').addClass('hidden');
        } else if(membership == '22' || membership == '23') { //lifetime_type_form
          $('#SponsoredOthers').addClass('hidden');
          $('#NGA').addClass('hidden');
          $('#LGU').addClass('hidden');
          $('#InfEco_OrgGroup').addClass('hidden');
          $('#InfEco_InfSector').addClass('hidden');
          $('#self_earning').addClass('hidden');
          $('#lifetime_type_form').removeClass('hidden');
        } else if(membership == '13') { //self_earning
          $('#SponsoredOthers').addClass('hidden');
          $('#NGA').addClass('hidden');
          $('#LGU').addClass('hidden');
          $('#InfEco_OrgGroup').addClass('hidden');
          $('#InfEco_InfSector').addClass('hidden');
          $('#self_earning').removeClass('hidden');
          $('#lifetime_type_form').addClass('hidden');
        } else if(membership == '17') { //self_earning
          $('#SponsoredOthers').addClass('hidden');
          $('#NGA').addClass('hidden');
          $('#LGU').addClass('hidden');
          $('#InfEco_OrgGroup').removeClass('hidden');
          $('#InfEco_InfSector').addClass('hidden');
          $('#self_earning').addClass('hidden');
          $('#lifetime_type_form').addClass('hidden');
        } else if(membership == '19') { //LGU
          $('#LGU').removeClass('hidden');
          $('#NGA').addClass('hidden');
          $('#SponsoredOthers').addClass('hidden');
          $('#InfEco_OrgGroup').addClass('hidden');
          $('#InfEco_InfSector').addClass('hidden');
          $('#self_earning').addClass('hidden');
          $('#lifetime_type_form').addClass('hidden');
        } else if(membership == '20') { //NGA
          $('#NGA').removeClass('hidden');
          $('#LGU').addClass('hidden');
          $('#SponsoredOthers').addClass('hidden');
          $('#InfEco_OrgGroup').addClass('hidden');
          $('#InfEco_InfSector').addClass('hidden');
          $('#self_earning').addClass('hidden');
          $('#lifetime_type_form').addClass('hidden');
        } else if(membership == '21') { //SponsoredOthers
          $('#SponsoredOthers').removeClass('hidden');
          $('#NGA').addClass('hidden');
          $('#LGU').addClass('hidden');
          $('#InfEco_OrgGroup').addClass('hidden');
          $('#InfEco_InfSector').addClass('hidden');
          $('#self_earning').addClass('hidden');
          $('#lifetime_type_form').addClass('hidden');
        } else { //none
          $('#SponsoredOthers').addClass('hidden');
          $('#NGA').addClass('hidden');
          $('#LGU').addClass('hidden');
          $('#InfEco_OrgGroup').addClass('hidden');
          $('#InfEco_InfSector').addClass('hidden');
          $('#self_earning').addClass('hidden');
          $('#lifetime_type_form').addClass('hidden');
        }
    });   
}

$(function ()
{
   $('#Pat_PHIC_Membership_Type').on('change', function(){
       if(this.value=='DD') {
           $('#benefactorform').removeClass('hidden');
           $('#relationship').addClass('required');
           $('#benefactorform input[type=text]').addClass('required');
       } else {
           $('#benefactorform').addClass('hidden');
           $('#relationship').removeClass('required');
           $('#benefactorform input[type=text]').removeClass('required');
       }
   })

   Philhealth.cloneDiv();
   Philhealth.removeDiv();
   Philhealth.changeMember();
});
