<?php

use ShineOS\Core\Patients\Entities\Patients;
use Plugins\ShineLab_Family\FamilyModel;
use Shine\Repositories\Eloquent\UserRepository as UserRepository;
use Shine\Repositories\Contracts\UserRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\User;
use Shine\Plugin;

class FamilyController extends Controller
{

    protected $moduleName = 'Patients';
    protected $modulePath = 'patients';

    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
        $this->middleware('auth');
        View::addNamespace('patients', 'src/ShineOS/Core/Patients/Resources/Views');
    }

    public function index()
    {
        //no index
    }

    public function save()
    {
        $id = Input::get('patient_id');
        $family = new FamilyModel();
        $family->patient_familyinfo_id = IdGenerator::generateId();
        $family->patient_id = Input::get('patient_id');
        $family->father_firstname = Input::get('father_firstname');
        $family->father_middlename = Input::get('father_middlename');
        $family->father_lastname = Input::get('father_lastname');
        $family->family_folder_name = Input::get('family_folder_name');
        $family->suffix = Input::get('suffix');
        $family->father_alive = Input::get('father_alive');
        $family->mother_firstname = Input::get('mother_firstname');
        $family->mother_middlename = Input::get('mother_middlename');
        $family->mother_lastname = Input::get('mother_lastname');
        $family->mother_alive = Input::get('mother_alive');
        $family->ctr_householdmembers_lt10yrs = Input::get('ctr_householdmembers_lt10yrs');
        $family->ctr_householdmembers_gt10yrs = Input::get('ctr_householdmembers_gt10yrs');

        $family->save();

        Session::flash('alert-class', 'alert-success');
        $message = "Family Info has been added to the patient record.";

        header('Location: '.site_url().'plugin/call/ShineLab_Family/Family/view/'.$id);
        exit;
    }

    public static function update()
    {
        $id = Input::get('patient_id');

        $checks = FamilyModel::where('patient_id', $id)->first();
        $family = array(
            'father_firstname' => Input::get('father_firstname'),
            'father_middlename' => Input::get('father_middlename'),
            'father_lastname' => Input::get('father_lastname'),
            'family_folder_name' => Input::get('family_folder_name'),
            'suffix' => Input::get('suffix'),
            'father_alive' => Input::get('father_alive'),
            'mother_firstname' => Input::get('mother_firstname'),
            'mother_middlename' => Input::get('mother_middlename'),
            'mother_lastname' => Input::get('mother_lastname'),
            'mother_alive' => Input::get('mother_alive'),
            'ctr_householdmembers_lt10yrs' => Input::get('ctr_householdmembers_lt10yrs'),
            'ctr_householdmembers_gt10yrs' => Input::get('ctr_householdmembers_gt10yrs')
        );

        $affectedRows = FamilyModel::where('patient_id', $id)
            ->update($family);

        Session::flash('alert-class', 'alert-success');
        $message = "Family Info has been updated.";

        if($affectedRows > 0) {
            header('Location: '.site_url().'plugin/call/ShineLab_Family/Family/view/'.$id);
            exit;
        }
    }

    public function view($id)
    {
        $patient = Patients::where('patient_id','=', $id)->first();
        $plugdata = FamilyModel::where('patient_id',$id)->first();

        View::addNamespace('pluginform', plugins_path().'ShineLab_Family');
        echo View::make('pluginform::master', array('patient'=>$patient, 'plugdata'=>$plugdata))->render();

    }
}
