<?php

$occupation = array(
    '' => '',
    0 => 'House maker',
    1 => 'Agriculture',
    2 => 'Business',
    3 => 'Services',
    4 => 'Labor'
);

$food_sufficiency = array(
    '' => '',
    0 => 'Sufficient for less than 3 months',
    1 => 'Sufficient for 3-5 months',
    2 => 'Sufficient for 6-11 months',
    3 => 'Sufficient for 12 months and with surplus'
);

?>

<div class="tab-pane step" id="epinurse">
    @if($patient)
    {!! Form::hidden('epinurse[patient_id]', $patient->patient_id) !!}
    @else
    {!! Form::hidden('epinurse[patient_id]', NULL) !!}
    @endif
    <fieldset>
        <legend>Socio-Economic Information</legend>
        <div class="col-sm-12">
            <label class="col-sm-2 control-label"> Ethnicity </label>
            <div class="col-sm-4">
                {!! Form::text('epinurse[ethnicity]',isset($epinurse->ethnicity) ? $epinurse->ethnicity : NULL, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="col-sm-12">
            <?php $marital_status = [''=>'',0=>'Unmarried',1=>'Married',2=>'Divorced',3=>'Widowed/Widower',4=>'Broken/Separated']; ?>
            <label class="col-sm-2 control-label"> Marital Status </label>
            <div class="col-sm-4">
                {!! Form::select('epinurse[marital_status]',$marital_status,isset($epinurse->marital_status) ? $epinurse->marital_status : NULL, ['class'=>'form-control']) !!}
            </div>
            <?php $educational_status = [''=>'',0=>'Illiterate',1=>'Literate',2=>'Primary level (1-5)',3=>'Secondary level (6-10)',4=>'Higher secondary (11-12)',5=>'Bachelor and above']; ?>
            <label class="col-sm-2 control-label"> Educational status </label>
            <div class="col-sm-4">
                {!! Form::select('epinurse[educational_status]',$educational_status,isset($epinurse->educational_status) ? $epinurse->educational_status : NULL, ['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="col-sm-12">
            <label class="col-sm-2 control-label"> Religion </label>
            <div class="col-sm-4">
                {!! Form::text('epinurse[religion]',isset($epinurse->religion) ? $epinurse->religion : NULL, ['class'=>'form-control']) !!}
            </div>
            <label class="col-sm-2 control-label"> Respondents occupation </label>
            <div class="col-sm-4">
                {!! Form::select('epinurse[occupation]', $occupation, isset($epinurse->occupation) ? $epinurse->occupation : NULL, ['class'=>'form-control','id'=>'occupation_select']) !!}
            </div>
        </div>
        <div class="col-sm-12">
            <label class="col-sm-2 control-label"> Main source of family income </label>
            <div class="col-sm-10">
                <div class="form-inline">
                    <div class="icheck">
                        <label class="checkbox">
                            {!! Form::checkbox('epinurse[income_agriculture]',1,(isset($epinurse->income_agriculture) && $epinurse->income_agriculture == 1) ? true : false) !!} Agriculture
                        </label>
                        <label class="checkbox">
                            {!! Form::checkbox('epinurse[income_business]',1,(isset($epinurse->income_business) && $epinurse->income_business == 1) ? true : false) !!} Business
                        </label>
                        <label class="checkbox">
                            {!! Form::checkbox('epinurse[income_service]',1,(isset($epinurse->income_service) && $epinurse->income_service == 1) ? true : false) !!} Service
                        </label>
                        <label class="checkbox">
                            {!! Form::checkbox('epinurse[income_labor]',1,(isset($epinurse->income_labor) && $epinurse->income_labor == 1) ? true : false) !!} Labor
                        </label>
                        <label class="checkbox">
                            {!! Form::checkbox('epinurse[income_remittance]',1,(isset($epinurse->income_remittance) && $epinurse->income_remittance == 1) ? true : false) !!} Remittance
                        </label>
                        <label class="checkbox">
                            {!! Form::checkbox('epinurse[income_others]',1,(isset($epinurse->income_others) && $epinurse->income_others == 1) ? true : false) !!} Others
                            {!! Form::text('epinurse[income_specify]',isset($epinurse->income_specify) ? $epinurse->income_specify : NULL, ['class'=>'form-control','placeholder'=>'Specify others']) !!}
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <label class="col-sm-2 control-label"> Food sufficiency based on income </label>
            <div class="col-sm-4">
                {!! Form::select('epinurse[food_sufficiency]', $food_sufficiency, isset($epinurse->food_sufficiency) ? $epinurse->food_sufficiency : NULL, ['class'=>'form-control','id'=>'food_sufficiency_select']) !!}
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Household Information</legend>
        <div class="col-sm-12">
            <label class="col-sm-2 control-label"> Family ID </label>
            <div class="col-sm-4">
                {!! Form::text('epinurse[family_id]',isset($epinurse->family_id) ? $epinurse->family_id : NULL, ['class'=>'form-control']) !!}
            </div>
            <?php $type_of_family = [''=>'',0=>'Nuclear',1=>'Joint',2=>'Extended']; ?>
            <label class="col-sm-2 control-label"> Type of Family </label>
            <div class="col-sm-4">
                {!! Form::select('epinurse[type_of_family]',$type_of_family,isset($epinurse->type_of_family) ? $epinurse->type_of_family : NULL, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="col-sm-12">
            <label class="col-sm-2 control-label"> Father's Name </label>
            <div class="col-sm-4">
                {!! Form::text('epinurse[fathers_name]',isset($epinurse->fathers_name) ? $epinurse->fathers_name : NULL, ['class'=>'form-control']) !!}
            </div>
            <label class="col-sm-2 control-label"> Mother's Name </label>
            <div class="col-sm-4">
                {!! Form::text('epinurse[mothers_name]',isset($epinurse->mothers_name) ? $epinurse->mothers_name : NULL, ['class'=>'form-control']) !!}
            </div>
        </div>
         <div class="col-sm-12">
            <label class="col-sm-2 control-label"> Guardians's Name </label>
            <div class="col-sm-4">
                {!! Form::text('epinurse[local_guardians_name]',isset($epinurse->local_guardians_name) ? $epinurse->local_guardians_name : NULL, ['class'=>'form-control']) !!}
            </div>
            <label class="col-sm-2 control-label"> Guardian Contact No. </label>
            <div class="col-sm-4">
                {!! Form::text('epinurse[contact_of_parent_guardian]',isset($epinurse->contact_of_parent_guardian) ? $epinurse->contact_of_parent_guardian : NULL, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="col-sm-12">
            <label class="col-sm-3 control-label"> Family details in numbers </label>
        </div>
        <div class="col-sm-offset-1 col-sm-10">
            <table class="table table-bordered" style="padding-top: 10px">
                <thead>
                  <tr>
                    <th style="text-align: center;">Male</th>
                    <th style="text-align: center;">Female</th>
                    <th style="text-align: center;">Others</th>
                    <th style="text-align: center;">Under 5 (Male)</th>
                    <th style="text-align: center;">Under 5 (Female)</th>
                    <th style="text-align: center;">Above 60 (Male)</th>
                    <th style="text-align: center;">Above 60 (Female)</th>
                    <th style="text-align: center;">Disabled</th>
                    <th style="text-align: center;">Pregnant</th>
                    <th style="text-align: center;">Lactating Mothers</th>
                </thead>
                <tbody>
                    <tr>
                        <td>{!! Form::text('epinurse[number_male]',isset($epinurse->number_male) ? $epinurse->number_male : NULL, ['class'=>'form-control']) !!}</td>
                        <td>{!! Form::text('epinurse[number_female]',isset($epinurse->number_female) ? $epinurse->number_female : NULL, ['class'=>'form-control']) !!}</td>
                        <td>{!! Form::text('epinurse[number_others]',isset($epinurse->number_others) ? $epinurse->number_others : NULL, ['class'=>'form-control']) !!}</td>
                        <td>{!! Form::text('epinurse[number_male_under5]',isset($epinurse->number_male_under5) ? $epinurse->number_male_under5 : NULL, ['class'=>'form-control']) !!}</td>
                        <td>{!! Form::text('epinurse[number_female_under5]',isset($epinurse->number_female_under5) ? $epinurse->number_female_under5 : NULL, ['class'=>'form-control']) !!}</td>
                        <td>{!! Form::text('epinurse[number_male_above60]',isset($epinurse->number_male_above60) ? $epinurse->number_male_above60 : NULL, ['class'=>'form-control']) !!}</td>
                        <td>{!! Form::text('epinurse[number_female_above60]',isset($epinurse->number_female_above60) ? $epinurse->number_female_above60 : NULL, ['class'=>'form-control']) !!}</td>
                        <td>{!! Form::text('epinurse[number_disabled]',isset($epinurse->number_disabled) ? $epinurse->number_disabled : NULL, ['class'=>'form-control']) !!}</td>
                        <td>{!! Form::text('epinurse[number_pregnant]',isset($epinurse->number_pregnant) ? $epinurse->number_pregnant : NULL, ['class'=>'form-control']) !!}</td>
                        <td>{!! Form::text('epinurse[number_lactating_mothers]',isset($epinurse->number_lactating_mothers) ? $epinurse->number_lactating_mothers : NULL, ['class'=>'form-control']) !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </fieldset>
</div>