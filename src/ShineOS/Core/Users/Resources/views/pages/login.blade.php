<?php
    /** Customizing the login page via customizations
     * 
     * {
     *  "appTitle:"Municipal Health Information System",
     *  "companyName:"Amellar Solutions",
     *  "byLine:"",
     *  "appTopLogo:"amellar/amellar-top-bar.jpg",
     *  "appLogo:"amellar/amellar.png",
     *  "appBlurb:"A  gateway to promote technology base management to reach out and record the medical services rendered by rural health units.  From profiling down to statistical reports to help in the analysis, for the improvement of welfare and health programs.",
     *  "loginstyle:{
     *      "introbox:{
     *          "width:"col-md-7",
     *          "background:"amellar/amellar-bk.jpg",
     *          "opacity:1
     *      },
     *      "logmebox:{
     *          "width:"col-md-5",
     *          "background:"#005E7D",
     *          "opacity:1
     *      }
     *  }
     * }
     */
?>
@extends('users::layouts.masterlogin')
@section('title') EpiNurse | Login @stop

@section('heads')
    <style>
        #loginstyle {
            display:block;
        }
        body {
            color: #333;
            background: #F3E0B3;
        }
        .background_wrapper {
            background-size: cover;
            left: 0;
            overflow: hidden;
            position: absolute;
            top: 0;
            width: 100%;
            z-index: 2;
        }
        .overlay {
            background-color: #F3E0B3;
            left: 0;
            top: 0;
            position: absolute;
            width: 100%;
            z-index: 3;
            opacity: .55;
        }
        .overlay2 {
            background: url( {{ asset('public/uploads/customizations/epinurse/saas-bak.jpg') }} ) center center / cover no-repeat;
            background-color: #F3E0B3;
            left: 0;
            top: 0;
            position: absolute;
            height: 100%;
            z-index: 3;
            opacity: .3;
        }
        h2.smartlitblue {
            font-size: 2em;
        }
        .modal-content {
            border-radius: 12px;
            overflow:hidden;
        }
        .modal-body {
            padding:0 !important;
        }
        .form-box {
            margin: 25% 17% 0px !important;
        }
        .intro-box {
            margin-top:10% !important;
            color: #52361b;
        }

        #welcomeH1 {
            color: #ED5328 !important;
        }

        #subTitleH2 {
            color: #ED5328 !important;
        }

        #pLead {
            color: #8B4513 !important;
        }

        #loginstyle .form-box .header2 {
            background:#FFFFFF url({{ asset('public/uploads/customizations/epinurse/epinurse.png') }}) no-repeat center center;
            background-size: auto 65px !important;
            border-radius: 4px 4px 0px 0px;
            box-shadow: 0px -3px 0px rgba(0, 0, 0, 0.2) inset;
            padding: 50px 10px;
            text-align: center;
            font-size: 26px;
            font-weight: 300;
            color: #FFF;
        }

    </style>
@stop

@section('content')
    <div class="background_wrapper hidden-xs"></div>
    <div class="overlay hidden-xs"></div>
    <div class="overlay2 hidden-xs col-md-6"></div>

    <div id="loginstyle" class="row">
        <div class="intro-box col-md-6">
            <div class="col-md-10 col-md-offset-1">
            <h1 id="welcomeH1">EpiNurse</h1>
            <h2 class="smartlitblue" id="subtitleH2"> Epinurse + Nursing for Disaster Risk Reduction </h2>
            <p class="lead" id="pLead">Our aim is to develop a disaster risk reduction method to ensure health security in disaster prone communities. Collaborating with local Nepali nurses, named as EpiNurses (Epidemiology+Nurse), who conduct a participatory monitoring by using ICT toolkit, we work toward protection and promotion of health and safety in shelters and communities.</p>
            <h3 id="buttonGroup">
                <a href="https://www.epinurse.org" class="btn btn-danger text-center" target="_blank">Learn more</a>
            </h3>
            </div>
        </div>
        <div class="logme-box col-md-6">
            <div class="col-md-8 col-md-offset-2 form-box" id="login-box">
                <div class="header2"></div>
                {!! Form::open(array( 'url'=>'login/verify', 'id'=>'loginForm', 'name'=>'loginForm' )) !!}
                <div class="body">
                    <h4>Account Login</h4>
                    @if (Session::has('message'))
                        <div class="alert alert-dismissible alert-success">
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif

                    @if (Session::has('warning'))
                        <div class="alert alert-dismissible alert-warning">
                            <p>{{ Session::get('warning') }}</p>
                        </div>
                    @endif

                    <p>Please login with your email/username and password below.</p>

                    <div class="form-group">
                        <input type="text" name="identity" id="identity" class="form-control" placeholder="Username or Email" />
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password" />
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="remember_me" id="remember_me" value="1" /> Remember me
                    </div>
                </div>
                <div class="footer">
                    <p><button type="submit" class="btn btn-warning btn-block">Sign me in</button></p>
                    <p><a href="{{ url('forgotpassword') }}">I forgot my password</a></p>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

        <div class="loginfooter">
            <span class="col-md-1"></span>
            <span class="col-md-5 text-center" id="submenu">
                Powered by<a href='http://www.medixserve.com' target='new'>MediXserve</a> <span>&copy;2014 - <?php echo date("Y"); ?></span>
            </span>
        </div>
    </div>

    <div class="modal fade" id="activateModal" tabindex="-1" role="dialog" aria-labelledby="activateModal">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        // var customizations = $.cookie('customizations');
    </script>
@stop
