<?php
namespace ShineOS\Core\Dashboard\Widgets\TotalCountEpiNurse;

use Arrilot\Widgets\AbstractWidget;
use Shine\Repositories\Eloquent\UserRepository as BaseRepository;
use Shine\Repositories\Contracts\UserRepositoryInterface;
use Shine\Libraries\FacilityHelper;
use Shine\Libraries\UserHelper;
use View, Config, Session;

/**
 * Widget for the total number of records per module
 */
class TotalCountBox extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */

    protected $config = [];

    public function placeholder()
    {
        $loading = '<div class="box box-primary"><!--Consultations-->
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-cog fa-spin fa-fw"></i> Loading Stats widget...</h3>
                </div>
            </div>';


        return $loading;
    }

    /**
     * The repository object.
     *
     * @var object
     */
    private $baseRepository;

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(BaseRepository $baseRepository)
    {

        $this->baseRepository = $baseRepository;

        $id = UserHelper::getUserInfo();
        $facility = Session::get('facility_details');

        //$facilityInfo = FacilityHelper::facilityInfo(); // get user id
        //$userFacilities = FacilityHelper::getFacilities($id);

        //Number of patients
        $patient_count = countAllPatientsByFacility();

        //Number of inbound referrals
        $inbound_count = countByService($facility->facility_id,'HealthAssessment');

        //Number of outbound referrals
        $outbound_count = countByService($facility->facility_id,'MotherAndChild');

        //Number of referrals
        $referral_count = countByService($facility->facility_id,'CommunityNursing');

        $dashboard_count = array('patient' => $patient_count,'inbound'=>$inbound_count,'outbound'=>$outbound_count,'referral'=>$referral_count);

        View::addNamespace('total_count_box', 'src/ShineOS/Core/Dashboard/Widgets/TotalCountEpiNurse');
        return view("total_count_box::index", [
            'config' => $this->config,
            'dashboard_count' => $dashboard_count,
        ]);
    }
}
