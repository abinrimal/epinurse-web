<?php namespace ShineOS\Core\API\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Patients\Entities\Patients;
use Illuminate\Http\JsonResponse;
use Shine\Libraries\IdGenerator;
use Request,Validator,DB;

class APIAnalyticsController extends Controller {
	
    public function __construct() {}

	public function index() {
		return response(json_encode(array('message'=>'No data')), 200);
	}



	public function analyticsdata_get(){
		$data = Request::all();
		$validator = Validator::make($data, [
			'facility_id' => 'required'
        ]);

		if ($validator->fails()==false) { 		
			$analytics = [];
			$analytics = $this->analytics($data['facility_id']); 
			return new JsonResponse(array('status'=>'success', 'data'=> $analytics), 200);
		} else {
			 return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
	}

	public function analytics($facility_id){

		$data['latest_patients'] = DB::select('SELECT patients.patient_id, patients.last_name, patients.first_name, patients.middle_name, patients.photo_url, patients.created_at FROM patients JOIN facility_patient_user ON facility_patient_user.patient_id = patients.patient_id JOIN facility_user ON facility_patient_user.facilityuser_id = facility_user.facilityuser_id WHERE facility_user.facility_id = '.$facility_id.' AND patients.deleted_at IS NULL ORDER BY patients.created_at DESC LIMIT 8');

        $data['count_by_gender'] = Patients::select('gender', DB::raw('count(*) as total'))
            // ->whereBetween('created_at', [$from, $to])
            ->where('deleted_at', NULL)
            ->groupby('gender')
            ->whereHas('facilityUser', function($query) use ($facility_id) {
                    $query->where('facility_id', '=', $facility_id);
            })
            ->get()->toArray();

        //count patients by age range
        $data['count_by_age'] = DB::select('SELECT
             CASE
                WHEN age < 10 THEN "10 and below"
                WHEN age BETWEEN 10 and 24 THEN "10 to 24"
                WHEN age BETWEEN 25 and 39 THEN "25 to 39"
                WHEN age BETWEEN 40 and 59 THEN "40 to 59"
                WHEN age BETWEEN 60 and 79 THEN "60 to 79"
                WHEN age >= 80 THEN "80 and above"
                WHEN age IS NULL THEN "Not Filled In (NULL)"
            END as age_range,
            COUNT(*) AS count
              FROM (SELECT TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) AS age FROM patients
              JOIN facility_patient_user ON facility_patient_user.patient_id = patients.patient_id
              JOIN facility_user ON facility_patient_user.facilityuser_id = facility_user.facilityuser_id
              WHERE facility_user.facility_id = '.$facility_id.' AND patients.deleted_at IS NULL) as derived
              GROUP BY age_range');

        $data['count_by_services_rendered'] = DB::select('SELECT healthcareservicetype_id, count(*) as total FROM healthcare_services JOIN facility_patient_user ON facility_patient_user.facilitypatientuser_id = healthcare_services.facilitypatientuser_id JOIN facility_user ON facility_patient_user.facilityuser_id = facility_user.facilityuser_id WHERE facility_user.facility_id = '.$facility_id.' AND healthcare_services.deleted_at IS NULL GROUP BY healthcareservicetype_id ORDER BY count(*) DESC');

		return $data;
	}
	

}