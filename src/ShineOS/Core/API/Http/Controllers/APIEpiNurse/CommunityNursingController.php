<?php namespace ShineOS\Core\API\Http\Controllers\APIEpiNurse;

use Illuminate\Routing\Controller;
use ShineOS\Core\API\Http\Controllers\APIEpiNurseController;

use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\PatientContacts;
use ShineOS\Core\Patients\Entities\PatientDeathInfo;
use ShineOS\Core\Patients\Entities\PatientEmergencyInfo;

use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser;

use Plugins\CommunityNursing\CommunityNursingModel;

use ShineOS\Core\Users\Entities\Users;
use Illuminate\Http\JsonResponse;
use Shine\Libraries\IdGenerator;


use Request, Auth, Validator;

class CommunityNursingController extends APIEpiNurseController {

	public function insertCommunityNursing($request_data) {
		$data = $request_data['data'];
		$data['facility_id'] = $facility_id = $request_data['facility_id'];
		$data['user_id'] = $user_id = $request_data['user_id'];
		$data['patient_id'] = $patient_id = $request_data['patient_id'];

		$validator = Validator::make($data, [
			"facility_id" => "required|string",
			"user_id" => "required|string",
			"patient_id" => "required|string",
			"uuid" => "required",
			"ownerUuid" => "required",
			"disaster_recently" => "required",
			"type_of_shelter" => "required",
			"adequacy_of_space" => "required",
			"damaged" => "required",
			"source_of_light_electricity" => "required|integer",
			"source_of_light_oil_lamp" => "required|integer",
			"source_of_light_solar" => "required|integer",
			"source_of_light_others" => "required|integer",
			"adequate_ventilation" => "required",
			"kitchen" => "required",
			"when_wash_hands_after_toileting" => "required",
			"when_wash_hands_before_cooking" => "required",
			"when_wash_hands_before_eating" => "required",
			"when_wash_hands_before_feeding_a_child" => "required",
			"bathing_habit" => "required",
			"water_supply_source" => "required",
			"adequacy_water_supply" => "required",
			"quality_drinking_water" => "required",
			"water_purification_boiling" => "required",
			"water_purification_filtration" => "required",
			"water_purification_using_chemical" => "required",
			"water_purification_sodis" => "required",
			"water_purification_others" => "required",
			"water_purification_no_purification" => "required",
			"distance_from_water" => "required",
			"toilet_available" => "required",
			"adequate_toilet_ratio" => "required",
			"toilet_type" => "required",
			"distance_toliet_from_shelter" => "required",
			"toilet_distance" => "required",
			"toilet_distance_unit" => "required",
			"waste_distance" => "required",
			"waste_distance_unit" => "required",
			"disposal_method_composting" => "required",
			"disposal_method_burning" => "required",
			"disposal_method_dumping" => "required",
			"disposal_method_others" => "required",
			"waste_collector_available" => "required",
			"has_dead_animal_disposal" => "required",
			"has_drainage" => "required",
			"surroundings_clean" => "required",
			"problems_menstruation_hygiene" => "required",
			"menstruation_hygiene_problems_no_water" => "required",
			"menstruation_hygiene_problems_no_privacy" => "required",
			"menstruation_hygiene_problems_no_pads" => "required",
			"menstruation_hygiene_problems_no_disposal" => "required",
			"menstruation_hygiene_problems_others" => "required",
			"visible_hazards_slippery_none" => "required",
			"visible_hazards_slippery_floor" => "required",
			"visible_hazards_no_light" => "required",
			"visible_hazards_bad_electical" => "required",
			"visible_hazards_rough_surface" => "required",
			"visible_hazards_sloppy_land" => "required",
			"visible_hazards_dumping_area" => "required",
			"visible_hazards_polluted" => "required",
			"visible_hazards_no_ventilation" => "required",
			"visible_hazards_noisy" => "required",
			"visible_hazards_other"	 => "required",
			"area_prone_to_none" => "required",
			"snake_bite" => "required",
			"animal_bite" => "required",
			"insect_bite" => "required",
			"others" => "required",
			"no_vectors" => "required",
			"mossquito_prevention_nets" => "required",
			"mossquito_prevention_liquid" => "required",
			"mossquito_prevention_drugs" => "required",
			"mossquito_prevention_others" => "required",
			"pest_control" => "required",
			"food_stock_available" => "required",
			"cooking_fuel_used_gas" => "required",
			"cooking_fuel_used_firewood" => "required",
			"cooking_fuel_used_kerosene" => "required",
			"cooking_fuel_used_others" => "required",
			"kind_of_food_available_cooked" => "required",
			"kind_of_food_available_junk" => "required",
			"kind_of_food_available_others" => "required",
			"food_hygienic" => "required",
			"food_storage_appropriate" => "required",
			"family_planning_method" => "required",
			"know_of_services" => "required",
			"know_of_safety_security" => "required",
			"know_of_ecp" => "required",
			"where_ecp_available_mobile_clinic" = "required",
			"where_ecp_available_health_facility" = "required",
			"where_ecp_available_others" = "required",
			"unusual_disease_outbreak" => "required",
			"health_care_on_site" => "required",
			"pfa_present" => "required",
			"time_to_nearest_health_facility" => "required",
			"type_of_nearest_facility_health_post" => "required",
			"type_of_nearest_facility_primary_health_center" => "required",
			"type_of_nearest_facility_district_hospital" => "required",
			"type_of_nearest_facility_zonal_hospital" => "required",
			"type_of_nearest_facility_sub_regional_hospital" => "required",
			"type_of_nearest_facility_tertiary_hospital" => "required",
			"type_of_nearest_facility_private_hospital" => "required",
			"type_of_nearest_facility_others" => "required",
			"family_illness_none" => "required",
			"family_illness_diabetes_mellitus" => "required",
			"family_illness_hypertension" => "required",
			"family_illness_copd" => "required",
			"family_illness_cancer" => "required",
			"family_illness_mental_health_problems" => "required",
			"family_illness_others" => "required",
			"health_hazards_none" => "required",
			"health_hazards_smoking" => "required",
			"health_hazards_alcoholism" => "required",
			"health_hazards_drug_abuse" => "required",
			"health_hazards_others" => "required",
			"abuse_no" => "required",
			"abuse_sexual_abuse" => "required",
			"abuse_physical_abuse" => "required",
			"abuse_psychological_abuse" => "required",
			"abuse_others" => "required",
        ]);

	    if($validator->fails() == false)
	    {
	    	$facilityUser = $this->getFacilityUser($user_id,$facility_id);
	    	if(!$facilityUser) {
	    		return new JsonResponse(array('status'=>'error', 'message'=>'Facility or User not found!'), 400);
	    	}

	    	$facilityPatientUser = FacilityPatientUser::where('facilityuser_id',$facilityUser->facilityuser_id)->where('patient_id',$patient_id)->first();
	    	if(!$facilityPatientUser) {
	    		return new JsonResponse(array('status'=>'error', 'message'=>'Patient not found!'), 400);
	    	}

	    	$message = 'Community nursing assessment has been updated!';
	    	$communitynursing = CommunityNursingModel::where('uuid',$data['uuid'])->first();
	    	if(!$communitynursing)
	    	{
	    		$message = 'New community nursing assessment has been added!';
	    		// CREATE NEW
		    	$hs_id = IdGenerator::generateId();

		    	// Save new healthcare
		    	$healthcare = $this->insertNewHealthcareService($data,$hs_id,'CommunityNursing');

		    	// Save new community Nursing
				$communitynursing = new CommunityNursingModel;	    	
	            $communitynursing->healthcareservice_id = $hs_id;
		        $communitynursing->uuid = isset($data['uuid']) ? $data['uuid'] : NULL;
		        $communitynursing->owner_uuid = isset($data['ownerUuid']) ? $data['ownerUuid'] : NULL;
	    	}


	        $communitynursing->disaster_recently = isset($data['disaster_recently']) ? $data['disaster_recently'] : NULL;
	        $communitynursing->disaster_type = isset($data['type_of_disaster']) ? $data['type_of_disaster'] : NULL;
	        $communitynursing->disaster_duration = isset($data['duration_after_disaster']) ? $data['duration_after_disaster'] : NULL;

			$communitynursing->number_human_loss = isset($data['number_human_loss']) ? $data['number_human_loss'] : NULL;
			$communitynursing->number_death = isset($data['number_death']) ? $data['number_death'] : NULL;
			$communitynursing->number_missing_people = isset($data['number_missing_people']) ? $data['number_missing_people'] : NULL;
			$communitynursing->number_injured_people = isset($data['number_injured_people']) ? $data['number_injured_people'] : NULL;
			$communitynursing->number_livestock_loss = isset($data['number_livestock_loss']) ? $data['number_livestock_loss'] : NULL;

			$communitynursing->when_wash_hands_after_toileting = isset($data['when_wash_hands_after_toileting']) ? $data['when_wash_hands_after_toileting'] : NULL;
			$communitynursing->when_wash_hands_before_cooking = isset($data['when_wash_hands_before_cooking']) ? $data['when_wash_hands_before_cooking'] : NULL;
			$communitynursing->when_wash_hands_before_eating = isset($data['when_wash_hands_before_eating']) ? $data['when_wash_hands_before_eating'] : NULL;
			$communitynursing->when_wash_hands_before_feeding_a_child = isset($data['when_wash_hands_before_feeding_a_child']) ? $data['when_wash_hands_before_feeding_a_child'] : NULL;
			$communitynursing->when_wash_hands_others = isset($data['when_wash_hands_others']) ? $data['when_wash_hands_others'] : NULL;
			$communitynursing->when_wash_hands_specify = isset($data['when_wash_hands_specify']) ? $data['when_wash_hands_specify'] : NULL;

	        $communitynursing->bathing_habit = isset($data['bathing_habit']) && $data['bathing_habit'] != '' ? $data['bathing_habit'] : NULL;

	        $communitynursing->src_water_supply = isset($data['water_supply_source']) && $data['water_supply_source'] != '' ? $data['water_supply_source'] : NULL;
	        $communitynursing->src_water_supply_spec = isset($data['water_supply_source_specify']) ? $data['water_supply_source_specify'] : NULL;
	        $communitynursing->ade_water_supply = isset($data['adequacy_water_supply']) ? $data['adequacy_water_supply'] : NULL;
	        $communitynursing->quality_water = isset($data['quality_drinking_water']) ? $data['quality_drinking_water'] : NULL;
	        $communitynursing->waterpurif_boiling = isset($data['water_purification_boiling']) ? $data['water_purification_boiling'] : NULL;
	        $communitynursing->waterpurif_filtration = isset($data['water_purification_filtration']) ? $data['water_purification_filtration'] : NULL;
	        $communitynursing->waterpurif_chemical = isset($data['water_purification_using_chemical']) ? $data['water_purification_using_chemical'] : NULL;
	        $communitynursing->waterpurif_sodis = isset($data['water_purification_sodis']) ? $data['water_purification_sodis'] : NULL;
	        $communitynursing->waterpurif_none = isset($data['water_purification_none']) ? $data['water_purification_none'] : NULL;
	        $communitynursing->waterpurif_oth = isset($data['water_purification_others']) ? $data['water_purification_others'] : NULL;
	        $communitynursing->waterpurif_oth_spec = isset($data['water_purification_specify']) ? $data['water_purification_specify'] : NULL;

	        $communitynursing->distance_water_supply = isset($data['distance_from_water']) && $data['distance_from_water'] != '' ? $data['distance_from_water'] : NULL;
	        $communitynursing->toilet_available = isset($data['toilet_available']) ? $data['toilet_available'] : NULL;
	        $communitynursing->num_toilet = isset($data['adequate_toilet_ratio']) ? $data['adequate_toilet_ratio'] : NULL;
	        $communitynursing->type_toilet = isset($data['toilet_type']) && $data['toilet_type'] != '' ? $data['toilet_type'] : NULL;
	        $communitynursing->type_toilet_specify = isset($data['toilet_type_specify']) ? $data['toilet_type_specify'] : NULL;
	        $communitynursing->toilet_distance = isset($data['distance_toliet_from_shelter']) && $data['distance_toliet_from_shelter'] != '' ? $data['distance_toliet_from_shelter'] : NULL;
	        
	        $communitynursing->toilet_fr_water = isset($data['toilet_distance'])  && $data['toilet_distance'] != '' ? $data['toilet_distance'] : NULL;
	        $communitynursing->toilet_fr_water_unit = isset($data['toilet_distance_unit']) && $data['toilet_distance_unit'] != '' ? $data['toilet_distance_unit'] : NULL;
	        $communitynursing->toilet_fr_waste = isset($data['waste_distance']) && $data['waste_distance'] != '' ? $data['waste_distance'] : NULL;
	        $communitynursing->toilet_fr_waste_unit = isset($data['waste_distance_unit']) && $data['waste_distance_unit'] != '' ? $data['waste_distance_unit'] : NULL;

			$communitynursing->disposal_method_composting = isset($data['disposal_method_composting']) ? $data['disposal_method_composting'] : NULL;
			$communitynursing->disposal_method_burning = isset($data['disposal_method_burning']) ? $data['disposal_method_burning'] : NULL;
			$communitynursing->disposal_method_dumping = isset($data['disposal_method_dumping']) ? $data['disposal_method_dumping'] : NULL;
			$communitynursing->disposal_method_others = isset($data['disposal_method_others']) ? $data['disposal_method_others'] : NULL;
			$communitynursing->disposal_method_specify = isset($data['disposal_method_specify']) ? $data['disposal_method_specify'] : NULL;	        

	        $communitynursing->waste_collectors = isset($data['waste_collector_available']) ? $data['waste_collector_available'] : NULL;
	        $communitynursing->timely_collection_waste = isset($data['timely_collection']) ? $data['timely_collection'] : NULL;
	        $communitynursing->dead_animal_disposal = isset($data['has_dead_animal_disposal']) ? $data['has_dead_animal_disposal'] : NULL;
	        $communitynursing->drainage_facility = isset($data['has_drainage']) ? $data['has_drainage'] : NULL;
	        $communitynursing->surr_area_clean = isset($data['surroundings_clean']) ? $data['surroundings_clean'] : NULL;
	        
	        $communitynursing->prob_hygiene_mens = isset($data['problems_menstruation_hygiene']) ? $data['problems_menstruation_hygiene'] : NULL;
	        $communitynursing->prob_hygiene_reas_watersupply = isset($data['menstruation_hygiene_problems_no_water']) ? $data['menstruation_hygiene_problems_no_water'] : NULL;
	        $communitynursing->prob_hygiene_reas_privacy = isset($data['menstruation_hygiene_problems_no_privacy']) ? $data['menstruation_hygiene_problems_no_privacy'] : NULL;
	        $communitynursing->prob_hygiene_reas_pads = isset($data['menstruation_hygiene_problems_no_pads']) ? $data['menstruation_hygiene_problems_no_pads'] : NULL;
	        $communitynursing->prob_hygiene_reas_padsdisposal = isset($data['menstruation_hygiene_problems_no_disposal']) ? $data['menstruation_hygiene_problems_no_disposal'] : NULL;
	        $communitynursing->prob_hygiene_reas_oth = isset($data['menstruation_hygiene_problems_others']) ? $data['menstruation_hygiene_problems_others'] : NULL;
	        $communitynursing->prob_hygiene_reas_oth_spec = isset($data['menstruation_hygiene_problems_specify']) ? $data['menstruation_hygiene_problems_specify'] : NULL;
	        
	        $communitynursing->visible_hazards_none = isset($data['visible_hazards_slippery_none']) ? $data['visible_hazards_slippery_none'] : NULL;
	        $communitynursing->visible_hazards_floor = isset($data['visible_hazards_slippery_floor']) ? $data['visible_hazards_slippery_floor'] : NULL;
	        $communitynursing->visible_hazards_light = isset($data['visible_hazards_no_light']) ? $data['visible_hazards_no_light'] : NULL;
	        $communitynursing->visible_hazards_electricity = isset($data['visible_hazards_bad_electical']) ? $data['visible_hazards_bad_electical'] : NULL;
	        $communitynursing->visible_hazards_surface = isset($data['visible_hazards_rough_surface']) ? $data['visible_hazards_rough_surface'] : NULL;
	        $communitynursing->visible_hazards_land = isset($data['visible_hazards_sloppy_land']) ? $data['visible_hazards_sloppy_land'] : NULL;
	        $communitynursing->visible_hazards_dumparea = isset($data['visible_hazards_dumping_area']) ? $data['visible_hazards_dumping_area'] : NULL;
	        $communitynursing->visible_hazards_pollution = isset($data['visible_hazards_polluted']) ? $data['visible_hazards_polluted'] : NULL;
	        $communitynursing->visible_hazards_ventilation = isset($data['visible_hazards_no_ventilation']) ? $data['visible_hazards_no_ventilation'] : NULL;
	        $communitynursing->visible_hazards_noise = isset($data['visible_hazards_noisy']) ? $data['visible_hazards_noisy'] : NULL;
	        $communitynursing->visible_hazards_oth = isset($data['visible_hazards_other']) ? $data['visible_hazards_other'] : NULL;
	        $communitynursing->visible_hazards_oth_spec = isset($data['visible_hazards_specify']) ? $data['visible_hazards_specify'] : NULL;
	        
	        $communitynursing->area_prone_to_none = isset($data['area_prone_to_none']) ? $data['area_prone_to_none'] : NULL;
	        $communitynursing->bite_prone_snake = isset($data['snake_bite']) ? $data['snake_bite'] : NULL;
	        $communitynursing->bite_prone_animal = isset($data['animal_bite']) ? $data['animal_bite'] : NULL;
	        $communitynursing->bite_prone_animal_spec = isset($data['animal_bite_specify']) ? $data['animal_bite_specify'] : NULL;
	        $communitynursing->bite_prone_insect = isset($data['insect_bite']) ? $data['insect_bite'] : NULL;
	        $communitynursing->bite_prone_insect_spec = isset($data['insect_bite_specify']) ? $data['insect_bite_specify'] : NULL;
	        $communitynursing->bite_prone_oth = isset($data['others']) ? $data['others'] : NULL;
	        $communitynursing->bite_prone_oth_spec = isset($data['others_specify']) ? $data['others_specify'] : NULL;
	        
	        $communitynursing->free_vectors = isset($data['no_vectors']) ? $data['no_vectors'] : NULL;
	        
	        $communitynursing->mosqprev_measure_net = isset($data['mossquito_prevention_nets']) ? $data['mossquito_prevention_nets'] : NULL;
	        $communitynursing->mosqprev_measure_liquid = isset($data['mossquito_prevention_liquid']) ? $data['mossquito_prevention_liquid'] : NULL;
	        $communitynursing->mosqprev_measure_drug = isset($data['mossquito_prevention_drugs']) ? $data['mossquito_prevention_drugs'] : NULL;
	        $communitynursing->mosqprev_measure_oth = isset($data['mossquito_prevention_others']) ? $data['mossquito_prevention_others'] : NULL;
	        $communitynursing->mosqprev_measure_oth_spec = isset($data['mossquito_prevention_specify']) ? $data['mossquito_prevention_specify'] : NULL;
	        
	        $communitynursing->periodic_pest_control = isset($data['pest_control']) ? $data['pest_control'] : NULL;

	        $communitynursing->housing = isset($data['type_of_shelter']) && $data['type_of_shelter'] != '' ? $data['type_of_shelter'] : NULL;
	        $communitynursing->housing_oth = isset($data['type_of_shelter_specify']) ? $data['type_of_shelter_specify'] : NULL;
	        $communitynursing->space_adequacy = isset($data['adequacy_of_space']) ? $data['adequacy_of_space'] : NULL;
	        $communitynursing->house_damage = isset($data['damaged']) ? $data['damaged'] : NULL;
	        $communitynursing->source_of_light_electricity = isset($data['source_of_light_electricity']) ? $data['source_of_light_electricity'] : NULL;
	        $communitynursing->source_of_light_oil_lamp = isset($data['source_of_light_oil_lamp']) ? $data['source_of_light_oil_lamp'] : NULL;
	        $communitynursing->source_of_light_solar = isset($data['source_of_light_solar']) ? $data['source_of_light_solar'] : NULL;
	        $communitynursing->source_of_light_others = isset($data['source_of_light_others']) ? $data['source_of_light_others'] : NULL;
	        $communitynursing->source_of_light_specify = isset($data['source_of_light_specify']) ? $data['source_of_light_specify'] : NULL;
	        $communitynursing->ventilation_adequacy = isset($data['adequate_ventilation']) ? $data['adequate_ventilation'] : NULL;
	        $communitynursing->kitchen = isset($data['kitchen']) ? $data['kitchen'] : NULL;

	        $communitynursing->foodstock_avail = isset($data['food_stock_available']) ? $data['food_stock_available'] : NULL;

			$communitynursing->cooking_fuel_used_gas = isset($data['cooking_fuel_used_gas']) ? $data['cooking_fuel_used_gas'] : NULL;
			$communitynursing->cooking_fuel_used_firewood = isset($data['cooking_fuel_used_firewood']) ? $data['cooking_fuel_used_firewood'] : NULL;
			$communitynursing->cooking_fuel_used_kerosene = isset($data['cooking_fuel_used_kerosene']) ? $data['cooking_fuel_used_kerosene'] : NULL;
			$communitynursing->cooking_fuel_used_others = isset($data['cooking_fuel_used_others']) ? $data['cooking_fuel_used_others'] : NULL;
			$communitynursing->cooking_fuel_used_specify = isset($data['cooking_fuel_used_specify']) ? $data['cooking_fuel_used_specify'] : NULL;
			$communitynursing->kind_of_food_available_cooked = isset($data['kind_of_food_available_cooked']) ? $data['kind_of_food_available_cooked'] : NULL;
			$communitynursing->kind_of_food_available_junk = isset($data['kind_of_food_available_junk']) ? $data['kind_of_food_available_junk'] : NULL;
			$communitynursing->kind_of_food_available_others = isset($data['kind_of_food_available_others']) ? $data['kind_of_food_available_others'] : NULL;
			$communitynursing->kind_of_food_available_specify = isset($data['kind_of_food_available_specify']) ? $data['kind_of_food_available_specify'] : NULL;

	        $communitynursing->safe_food = isset($data['food_hygienic']) ? $data['food_hygienic'] : NULL;
	        $communitynursing->food_storage = isset($data['food_storage_appropriate']) ? $data['food_storage_appropriate'] : NULL;

	        $communitynursing->disease_outbreak = isset($data['unusual_disease_outbreak']) ? $data['unusual_disease_outbreak'] : NULL;
	        $communitynursing->disease_outbreak_spec = isset($data['unusual_disease_outbreak_specify']) ? $data['unusual_disease_outbreak_specify'] : NULL;
	        $communitynursing->hs_onsite = isset($data['health_care_on_site']) ? $data['health_care_on_site'] : NULL;
	        $communitynursing->pfa = isset($data['pfa_present']) ? $data['pfa_present'] : NULL;
	        $communitynursing->distance_faci = isset($data['time_to_nearest_health_facility']) ? $data['time_to_nearest_health_facility'] : NULL;

			$communitynursing->type_of_nearest_facility_health_post = isset($data['type_of_nearest_facility_health_post']) ? $data['type_of_nearest_facility_health_post'] : NULL;
			$communitynursing->type_of_nearest_facility_primary_health_center = isset($data['type_of_nearest_facility_primary_health_center']) ? $data['type_of_nearest_facility_primary_health_center'] : NULL;
			$communitynursing->type_of_nearest_facility_district_hospital = isset($data['type_of_nearest_facility_district_hospital']) ? $data['type_of_nearest_facility_district_hospital'] : NULL;
			$communitynursing->type_of_nearest_facility_zonal_hospital = isset($data['type_of_nearest_facility_zonal_hospital']) ? $data['type_of_nearest_facility_zonal_hospital'] : NULL;
			$communitynursing->type_of_nearest_facility_sub_regional_hospital = isset($data['type_of_nearest_facility_sub_regional_hospital']) ? $data['type_of_nearest_facility_sub_regional_hospital'] : NULL;
			$communitynursing->type_of_nearest_facility_tertiary_hospital = isset($data['type_of_nearest_facility_tertiary_hospital']) ? $data['type_of_nearest_facility_tertiary_hospital'] : NULL;
			$communitynursing->type_of_nearest_facility_private_hospital = isset($data['type_of_nearest_facility_private_hospital']) ? $data['type_of_nearest_facility_private_hospital'] : NULL;
			$communitynursing->type_of_nearest_facility_others = isset($data['type_of_nearest_facility_others']) ? $data['type_of_nearest_facility_others'] : NULL;
			$communitynursing->type_of_nearest_facility_specify = isset($data['type_of_nearest_facility_specify']) ? $data['type_of_nearest_facility_specify'] : NULL;

	        $communitynursing->family_illness_none = isset($data['family_illness_none']) ? $data['family_illness_none'] : NULL;
	        $communitynursing->family_illness_diabetes = isset($data['family_illness_diabetes_mellitus']) ? $data['family_illness_diabetes_mellitus'] : NULL;
	        $communitynursing->family_illness_hypertension = isset($data['family_illness_hypertension']) ? $data['family_illness_hypertension'] : NULL;
	        $communitynursing->family_illness_copd = isset($data['family_illness_copd']) ? $data['family_illness_copd'] : NULL;
	        $communitynursing->family_illness_cancer = isset($data['family_illness_cancer']) ? $data['family_illness_cancer'] : NULL;
	        $communitynursing->family_illness_mental = isset($data['family_illness_mental_health_problems']) ? $data['family_illness_mental_health_problems'] : NULL;
	        $communitynursing->family_illness_oth = isset($data['family_illness_others']) ? $data['family_illness_others'] : NULL;
	        $communitynursing->family_illness_oth_spec = isset($data['family_illness_specify']) ? $data['family_illness_specify'] : NULL;

	        $communitynursing->fp_use = isset($data['fp_use']) ? $data['fp_use'] : NULL;
	        $communitynursing->fp_method_used = isset($data['family_planning_method']) ? $data['family_planning_method'] : NULL;
	        $communitynursing->fp_temp_awareness = isset($data['know_of_services']) ? $data['know_of_services'] : NULL;
	        $communitynursing->fp_awareness_security = isset($data['know_of_safety_security']) ? $data['know_of_safety_security'] : NULL;
	        $communitynursing->fp_awareness_ecp = isset($data['know_of_ecp']) ? $data['know_of_ecp'] : NULL;

	        $communitynursing->where_ecp_available_mobile_clinic = isset($data['where_ecp_available_mobile_clinic']) ? $data['where_ecp_available_mobile_clinic']: NULL;
	        $communitynursing->where_ecp_available_health_facility = isset($data['where_ecp_available_health_facility']) ? $data['where_ecp_available_health_facility']: NULL;
	        $communitynursing->where_ecp_available_others = isset($data['where_ecp_available_others']) ? $data['where_ecp_available_others']: NULL;
	        $communitynursing->where_ecp_available_specify = isset($data['where_ecp_available_specify']) ? $data['where_ecp_available_specify']: NULL;

	        $communitynursing->family_mental_illness = isset($data['under_family_mental_illness']) ? $data['under_family_mental_illness'] : NULL;
	        $communitynursing->medication = isset($data['under_medication']) ? $data['under_medication'] : NULL;
	        $communitynursing->health_hazards_none = isset($data['health_hazards_none']) ? $data['health_hazards_none'] : NULL;
	        $communitynursing->mental_hazard_smoking = isset($data['health_hazards_smoking']) ? $data['health_hazards_smoking'] : NULL;
	        $communitynursing->mental_hazard_alcoholism = isset($data['health_hazards_alcoholism']) ? $data['health_hazards_alcoholism'] : NULL;
	        $communitynursing->mental_hazard_drug = isset($data['health_hazards_drug_abuse']) ? $data['health_hazards_drug_abuse'] : NULL;
	        $communitynursing->mental_hazard_oth = isset($data['health_hazards_others']) ? $data['health_hazards_others'] : NULL;
	        $communitynursing->mental_hazard_oth_spec = isset($data['health_hazards_specify']) ? $data['health_hazards_specify'] : NULL;

	        $communitynursing->mental_abuse_none = isset($data['health_hazards_drug_abuse']) ? $data['health_hazards_drug_abuse'] : NULL;
	        $communitynursing->mental_abuse_sexual = isset($data['abuse_sexual_abuse']) ? $data['abuse_sexual_abuse'] : NULL;
	        $communitynursing->mental_abuse_physical = isset($data['abuse_physical_abuse']) ? $data['abuse_physical_abuse'] : NULL;
	        $communitynursing->mental_abuse_psych = isset($data['abuse_psychological_abuse']) ? $data['abuse_psychological_abuse'] : NULL;
	        $communitynursing->mental_abuse_oth = isset($data['abuse_others']) ? $data['abuse_others'] : NULL;
	        $communitynursing->mental_abuse_oth_spec = isset($data['abuse_specify']) ? $data['abuse_specify'] : NULL;


	        if($communitynursing->save())
	        {
	    		return new JsonResponse(array('status'=>'success', 'message'=>$message), 200);
	        }
	        else
	        {
	    		return new JsonResponse(array('status'=>'error', 'message'=>'Error in saving community nursing assessment!'), 400);	
	        }

	    }
	    else
	    {
	    	// return $this->json_response('error',$validator->errors()->all(),400);
	    	return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
	    }
	}

}