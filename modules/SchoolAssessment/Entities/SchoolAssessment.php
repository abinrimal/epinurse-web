<?php namespace Modules\SchoolAssessment\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class SchoolAssessment extends Model {

    use SoftDeletes;

    protected $table = 'schools';
    protected static $table_name = 'schools';
    protected $primaryKey = 'school_id';
}