<?php

use Plugins\HealthAssessment\HealthAssessmentModel;
use Modules\Healthcareservices\Entities\Healthcareservices;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\HealthcareRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\User;
use Shine\Plugin;

class HealthAssessmentController extends Controller
{
    protected $moduleName = 'Healthcareservices';
    protected $modulePath = 'healthcareservices';

    public function __construct(HealthcareRepository $healthcareRepository) {
        $this->healthcareRepository = $healthcareRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        //no index
    }

    public function save($data)
    {
        $health_id = $data['shaservice_id'];
        $hservice_id = $data['hservice_id'];
 
        $healthassessment = HealthAssessmentModel::submit($data,$hservice_id);
    }
}
