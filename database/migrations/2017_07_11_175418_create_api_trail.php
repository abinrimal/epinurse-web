<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiTrail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $mode = Config::get('config.mode');

        if($mode=='cloud') {
            if (Schema::hasTable('api_trail')!=TRUE) {
                Schema::create('api_trail', function(Blueprint $table)
                {
                    $table->increments('id');
                    $table->string('apitrail_id',60);
                    
                    $table->string('apiuseraccount_id',60);
                    $table->string('api_key',100)->nullable();
                    $table->string('api_secret',100)->nullable();
                    
                    $table->string('path',100)->nullable();
                    $table->string('method',25)->nullable();
                    $table->text('input')->nullable();                    
                    
                    $table->softDeletes();
                    $table->timestamps();
                    $table->unique('apitrail_id');
                }); 
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
