<?php

use Shine\Libraries\Utils\Lovs;

/*
 * Methods related to Lovs
 *
 * @package ShineOS+
 * @subpackage Lovs
 * @version 3.0
 *
*/

/**
 * Generate array of nations
 * @param  null
 * @return array             
 *          Array of nations
 * @package Lovs
 */
function nations() {
    $nations = array("Choose country"=>"",
    "Afghanistan"=>"AFG",
    "Aland Islands"=>"ALA",
    "Albania"=>"ALB",
    "Algeria"=>"DZA",
    "American Samoa"=>"ASM",
    "Andorra"=>"AND",
    "Angola"=>"AGO",
    "Anguilla"=>"AIA",
    "Antarctica"=>"ATA",
    "Antigua and Barbuda"=>"ATG",
    "Argentina"=>"ARG",
    "Armenia"=>"ARM",
    "Aruba"=>"ABW",
    "Australia"=>"AUS",
    "Austria"=>"AUT",
    "Azerbaijan"=>"AZE",
    "Bahamas"=>"BHS",
    "Bahrain"=>"BHR",
    "Bangladesh"=>"BGD",
    "Barbados"=>"BRB",
    "Belarus"=>"BLR",
    "Belgium"=>"BEL",
    "Belize"=>"BLZ",
    "Benin"=>"BEN",
    "Bermuda"=>"BMU",
    "Bhutan"=>"BTN",
    "Plurinational State of Bolivia"=>"BOL",
    "Bonaire - Sint Eustatius and Saba"=>"BES",
    "Bosnia and Herzegovina"=>"BIH",
    "Botswana"=>"BWA",
    "Bouvet Island"=>"BVT",
    "Brazil"=>"BRA",
    "British Indian Ocean Territory"=>"IOT",
    "Brunei Darussalam"=>"BRN",
    "Bulgaria"=>"BGR",
    "Burkina Faso"=>"BFA",
    "Burundi"=>"BDI",
    "Cambodia"=>"KHM",
    "Cameroon"=>"CMR",
    "Canada"=>"CAN",
    "Cabo Verde"=>"CPV",
    "Cayman Islands"=>"CYM",
    "Central African Republic"=>"CAF",
    "Chad"=>"TCD",
    "Chile"=>"CHL",
    "China"=>"CHN",
    "Christmas Island"=>"CXR",
    "Cocos (Keeling) Islands"=>"CCK",
    "Colombia"=>"COL",
    "Comoros"=>"COM",
    "Congo"=>"COG",
    "Democratic Republic of Congo"=>"COD",
    "Cook Islands"=>"COK",
    "Costa Rica"=>"CRI",
    "Cote d'Ivoire"=>"CIV",
    "Croatia"=>"HRV",
    "Cuba"=>"CUB",
    "Curacao !Cura ao"=>"CUW",
    "Cyprus"=>"CYP",
    "Czech Republic"=>"CZE",
    "Denmark"=>"DNK",
    "Djibouti"=>"DJI",
    "Dominica"=>"DMA",
    "Dominican Republic"=>"DOM",
    "Ecuador"=>"ECU",
    "Egypt"=>"EGY",
    "El Salvador"=>"SLV",
    "Equatorial Guinea"=>"GNQ",
    "Eritrea"=>"ERI",
    "Estonia"=>"EST",
    "Ethiopia"=>"ETH",
    "Falkland Islands (Malvinas)"=>"FLK",
    "Faroe Islands"=>"FRO",
    "Fiji"=>"FJI",
    "Finland"=>"FIN",
    "France"=>"FRA",
    "French Guiana"=>"GUF",
    "French Polynesia"=>"PYF",
    "French Southern Territories"=>"ATF",
    "Gabon"=>"GAB",
    "Gambia"=>"GMB",
    "Georgia"=>"GEO",
    "Germany"=>"DEU",
    "Ghana"=>"GHA",
    "Gibraltar"=>"GIB",
    "Greece"=>"GRC",
    "Greenland"=>"GRL",
    "Grenada"=>"GRD",
    "Guadeloupe"=>"GLP",
    "Guam"=>"GUM",
    "Guatemala"=>"GTM",
    "Guernsey"=>"GGY",
    "Guinea"=>"GIN",
    "Guinea-Bissau"=>"GNB",
    "Guyana"=>"GUY",
    "Haiti"=>"HTI",
    "Heard Island and McDonald Islands"=>"HMD",
    "Holy See (Vatican City State)"=>"VAT",
    "Honduras"=>"HND",
    "Hong Kong"=>"HKG",
    "Hungary"=>"HUN",
    "Iceland"=>"ISL",
    "India"=>"IND",
    "Indonesia"=>"IDN",
    "Islamic Republic of Iran"=>"IRN",
    "Iraq"=>"IRQ",
    "Ireland"=>"IRL",
    "Isle of Man"=>"IMN",
    "Israel"=>"ISR",
    "Italy"=>"ITA",
    "Jamaica"=>"JAM",
    "Japan"=>"JPN",
    "Jersey"=>"JEY",
    "Jordan"=>"JOR",
    "Kazakhstan"=>"KAZ",
    "Kenya"=>"KEN",
    "Kiribati"=>"KIR",
    "Democratic People's Republic of Korea"=>"PRK",
    "Republic of Korea"=>"KOR",
    "Kuwait"=>"KWT",
    "Kyrgyzstan"=>"KGZ",
    "Lao People's Democratic Republic"=>"LAO",
    "Latvia"=>"LVA",
    "Lebanon"=>"LBN",
    "Lesotho"=>"LSO",
    "Liberia"=>"LBR",
    "Libya"=>"LBY",
    "Liechtenstein"=>"LIE",
    "Lithuania"=>"LTU",
    "Luxembourg"=>"LUX",
    "Macao"=>"MAC",
    "Macedonia (the former Yugoslav Republic of)"=>"MKD",
    "Madagascar"=>"MDG",
    "Malawi"=>"MWI",
    "Malaysia"=>"MYS",
    "Maldives"=>"MDV",
    "Mali"=>"MLI",
    "Malta"=>"MLT",
    "Marshall Islands"=>"MHL",
    "Martinique"=>"MTQ",
    "Mauritania"=>"MRT",
    "Mauritius"=>"MUS",
    "Mayotte"=>"MYT",
    "Mexico"=>"MEX",
    "Micronesia"=>"FSM",
    "Moldova, Republic of"=>"MDA",
    "Monaco"=>"MCO",
    "Mongolia"=>"MNG",
    "Montenegro"=>"MNE",
    "Montserrat"=>"MSR",
    "Morocco"=>"MAR",
    "Mozambique"=>"MOZ",
    "Myanmar"=>"MMR",
    "Namibia"=>"NAM",
    "Nauru"=>"NRU",
    "Nepal"=>"NPL",
    "Netherlands"=>"NLD",
    "New Caledonia"=>"NCL",
    "New Zealand"=>"NZL",
    "Nicaragua"=>"NIC",
    "Niger"=>"NER",
    "Nigeria"=>"NGA",
    "Niue"=>"NIU",
    "Norfolk Island"=>"NFK",
    "Northern Mariana Islands"=>"MNP",
    "Norway"=>"NOR",
    "Oman"=>"OMN",
    "Pakistan"=>"PAK",
    "Palau"=>"PLW",
    "Palestine, State of"=>"PSE",
    "Panama"=>"PAN",
    "Papua New Guinea"=>"PNG",
    "Paraguay"=>"PRY",
    "Peru"=>"PER",
    "Philippines"=>"PHL",
    "Pitcairn"=>"PCN",
    "Poland"=>"POL",
    "Portugal"=>"PRT",
    "Puerto Rico"=>"PRI",
    "Qatar"=>"QAT",
    "Reunion !RŽunion"=>"REU",
    "Romania"=>"ROU",
    "Russian Federation"=>"RUS",
    "Rwanda"=>"RWA",
    "Saint Barthelemy"=>"BLM",
    "Saint Helena (Ascension and Tristan da Cunha)"=>"SHN",
    "Saint Kitts and Nevis"=>"KNA",
    "Saint Lucia"=>"LCA",
    "Saint Martin (French part)"=>"MAF",
    "Saint Pierre and Miquelon"=>"SPM",
    "Saint Vincent and the Grenadines"=>"VCT",
    "Samoa"=>"WSM",
    "San Marino"=>"SMR",
    "Sao Tome and Principe"=>"STP",
    "Saudi Arabia"=>"SAU",
    "Senegal"=>"SEN",
    "Serbia"=>"SRB",
    "Seychelles"=>"SYC",
    "Sierra Leone"=>"SLE",
    "Singapore"=>"SGP",
    "Sint Maarten (Dutch part)"=>"SXM",
    "Slovakia"=>"SVK",
    "Slovenia"=>"SVN",
    "Solomon Islands"=>"SLB",
    "Somalia"=>"SOM",
    "South Africa"=>"ZAF",
    "South Georgia and the South Sandwich Islands"=>"SGS",
    "South Sudan"=>"SSD",
    "Spain"=>"ESP",
    "Sri Lanka"=>"LKA",
    "Sudan"=>"SDN",
    "Suriname"=>"SUR",
    "Svalbard and Jan Mayen"=>"SJM",
    "Swaziland"=>"SWZ",
    "Sweden"=>"SWE",
    "Switzerland"=>"CHE",
    "Syrian Arab Republic"=>"SYR",
    "Taiwan - Province of China"=>"TWN",
    "Tajikistan"=>"TJK",
    "Tanzania"=>"TZA",
    "Thailand"=>"THA",
    "Timor-Leste"=>"TLS",
    "Togo"=>"TGO",
    "Tokelau"=>"TKL",
    "Tonga"=>"TON",
    "Trinidad and Tobago"=>"TTO",
    "Tunisia"=>"TUN",
    "Turkey"=>"TUR",
    "Turkmenistan"=>"TKM",
    "Turks and Caicos Islands"=>"TCA",
    "Tuvalu"=>"TUV",
    "Uganda"=>"UGA",
    "Ukraine"=>"UKR",
    "United Arab Emirates"=>"ARE",
    "United Kingdom"=>"GBR",
    "United States"=>"USA",
    "United States Minor Outlying Islands"=>"UMI",
    "Uruguay"=>"URY",
    "Uzbekistan"=>"UZB",
    "Vanuatu"=>"VUT",
    "Venezuela (Bolivarian Republic of)"=>"VEN",
    "Viet Nam"=>"VNM",
    "Virgin Islands - British"=>"VGB",
    "Virgin Islands - U.S."=>"VIR",
    "Wallis and Futuna"=>"WLF",
    "Western Sahara"=>"ESH",
    "Yemen"=>"YEM",
    "Zambia"=>"ZMB",
    "Zimbabwe"=>"ZWE");

    return array_flip($nations);
}

/**
 * Get Laboratory Order Name
 * @param string $code Code of laboratory
 * @return array             
 *          Array details of laboratory
 * @package Lovs
 */
function getLabName($code)
{
    $result = Lovs::getValueOfFieldBy('laboratories', 'laboratorydescription', 'laboratorycode', $code);

    return $result;
}

/**
 * Get Gender Name
 * @param string $code Code of gender
 * @return string
 *          Gender name
 * @package Lovs
 */
function getGender($code)
{
    switch ($code) {
        case "M": return "Male"; break;
        case "F": return "Female"; break;
        default: return "Unknown"; break;
    }
}

/**
 * Generate array Civil Status Codes
 * @param null
 * @return array             
 *          Array codes
 * @package Lovs
 */
function getArrCivilStatus()
{
    return $arrArrCivilStatus = array(NULL=>'Select Civil Status',
    'A'=>'Annuled',
    'C'=>'Co-Habitation',
    'D'=>'Divorced',
    'M'=>'Married',
    'S'=>'Single',
    'U'=>'Unknown',
    'W'=>'Widowed',
    'X'=>'Separated');
}

/**
 * Generate array Blood Type Codes
 * @param null
 * @return array             
 *          Array codes
 * @package Lovs
 */
function getArrBloodType()
{
    return $arrBloodType = array(NULL=>'Select Blood Type',
  'A+'=>'A+',
  'A-'=>'A-',
  'AB+'=>'AB+',
  'AB-'=>'AB-',
  'B+'=>'B+',
  'B-'=>'B-',
  'O+'=>'O+',
  'O-'=>'O-',
  'U'=>'Unknown');
}

/**
 * Generate array Suffix Codes
 * @param null
 * @return array             
 *          Array codes
 * @package Lovs
 */
function getArrRefSuffix()
{
    return $arrRefSuffix = array('NA'=> 'Not Applicable',
    'II'=> 'II',
    'III'=> 'III',
    'IV'=> 'IV',
    'JR'=> 'Jr.',
    'SR'=> 'Sr.',
    'V'=> 'V');
}

/**
 * Generate array Education Levels
 * @param null
 * @return array             
 *          Array codes
 * @package Lovs
 */
function getArrHighestEducation() {
    return $getArrHighestEducation = array(
    NULL=>'Select Highest Education',
    '01'=>'Elementary Education',
    '02'=>'High School Education',
    '03'=>'College',
    '04'=>'Postgraduate Program',
    '05'=>'No Formal Education/No Schooling',
    '06'=>'Not Applicable',
    '07'=>'Vocational',
    '99'=>'Others');
}

/**
 * Generate array Philhealth Codes
 * @param null
 * @return array             
 *          Array codes
 * @package Lovs
 */
function getArrPhilhealthMemberCategory() {
    return $getArrPhilhealthMemberCategory = array(
    NULL=>'Select member category',
    '01'=>'FE - Private - Permanent Regular',
    '02'=>'FE - Private - Casual',
    '03'=>'FE - Private - Contract/Project Based',
    '04'=>'FE - Govt - Permanent Regular',
    '05'=>'FE - Govt - Casual',
    '06'=>'FE - Govt - Contract/Project Based',
    '07'=>'FE - Enterprise Owner',
    '08'=>'FE - Household Help/Kasambahay',
    '09'=>'FE - Family Driver',
    '10'=>'IE - Migrant Worker - Land Based',
    '11'=>'IE - Migrant Worker - Sea Based',
    '12'=>'IE - Informal Sector',
    '13'=>'IE - Self Earning Individual',
    '14'=>'IE - Filipino with Dual Citizenship',
    '15'=>'IE - Naturalized Filipino Citizen',
    '16'=>'IE - Citizen of other countries working/residing/studying in the Philippines',
    '17'=>'IE - Organized Group',
    '18'=>'Indigent - NHTS-PR',
    '19'=>'Sponsored - LGU',
    '20'=>'Sponsored - NGA',
    '21'=>'Sponsored - Others',
    '22'=>'Lifetime Member - Retiree/Pensioner',
    '23'=>'Lifetime Member - With 120 months contribution and has reached retirement age');
}

/**
 * Generate array PhilHealth Membership Categories
 * @param null
 * @return array             
 *          Array codes
 * @package Lovs
 */
function returnPhilhealthMemberCategory($code) {
    switch($code){
        case '01': return 'FE - Private - Permanent Regular'; break;
        case '02': return 'FE - Private - Casual'; break;
        case '03': return 'FE - Private - Contract/Project Based'; break;
        case '04': return 'FE - Govt - Permanent Regular'; break;
        case '05': return 'FE - Govt - Casual'; break;
        case '06': return 'FE - Govt - Contract/Project Based'; break;
        case '07': return 'FE - Enterprise Owner'; break;
        case '08': return 'FE -Household Help/Kasambahay'; break;
        case '09': return 'FE - Family Driver'; break;
        case '10': return 'IE - Migrant Worker - Land Based'; break;
        case '11': return 'IE - Migrant Worker - Sea Based'; break;
        case '12': return 'IE - Informal Sector'; break;
        case '13': return 'IE - Self Earning Individual'; break;
        case '14': return 'IE - Filipino with Dual Citizenship'; break;
        case '15': return 'IE - Naturalized Filipino Citizen'; break;
        case '16': return 'IE - Citizen of other countries working/residing/studying in the Philippines'; break;
        case '17': return 'IE - Organized Group'; break;
        case '18': return 'Indigent - NHTS-PR'; break;
        case '19': return 'Sponsored - LGU'; break;
        case '20': return 'Sponsored - NGA'; break;
        case '21': return 'Sponsored - Others'; break;
        case '22': return 'Lifetime Member - Retiree/Pensioner'; break;
        case '23': return 'Lifetime Member - With 120 months contribution and has reached retirement age'; break;
    }
}

/**
 * Generate array Relationships
 * @param null
 * @return array             
 *          Array codes
 * @package Lovs
 */
function getArrRelationship(){
    return array(NULL=>"Select relationship",
        "S"=>"Son",
        "D"=>"Daughter",
        "F"=>"Father",
        "M"=>"Mother",
        "AD"=>"Adopted-Daughter",
        "AS"=>"Adopted-Son",
        "AF"=>"Adoptive-Father",
        "AM"=>"Adoptive-Mother",
        "SD"=>"Step Daughter",
        "SS"=>"Step Son",
        "SF"=>"Step Father",
        "SM"=>"Step Mother",
        "H"=>"Husband",
        "W"=>"Wife");
}

/**
 * Generate array Family Planning Methods
 * @param null
 * @return array             
 *          Array of methods
 * @package Lovs
 */
function getArrFamilyPlanningMethod() {
    return array("CON"=>"Condom",
                    "FSTR/BTL"=>"Female Sterilization/Bilateral Tubal",
                    "INJ"=>"Depo-medroxy Progestine Acetate",
                    "IUD"=>"Intra-Uterine Device",
                    "MSTR/VASECTOMY"=>"Male Sterilization/Vasectomy",
                    "NFP-BBT"=>"NFP-Basal Body Temperature",
                    "NFP-CM"=>"NFP-Cervical Mucus Method",
                    "NFP-LAM"=>"Lactational Amenorrhea Method",
                    "NFP-SDM"=>"NFP-Standard Days Method",
                    "NFP-STM"=>"NFP-Sympothermal Method",
                    "PILLS"=>"Pills",
                    "IMPLANT"=>"Implant");
}

/**
 * Generate array Months Zero padded
 * @param null
 * @return array             
 *          Array months
 * @package Lovs
 */
function getArrMonth() {
    return array("01"=>"January",
                "02"=>"February",
                "03"=>"March",
                "04"=>"April",
                "05"=>"May",
                "06"=>"June",
                "07"=>"July",
                "08"=>"August",
                "09"=>"September",
                "10"=>"October",
                "11"=>"November",
                "12"=>"December");
}

/**
 * Generate array Months
 * @param null
 * @return array             
 *          Array codes
 * @package Lovs
 */
function arrMonth() {
    return array("1"=>"January",
                "2"=>"February",
                "3"=>"March",
                "4"=>"April",
                "5"=>"May",
                "6"=>"June",
                "7"=>"July",
                "8"=>"August",
                "9"=>"September",
                "10"=>"October",
                "11"=>"November",
                "12"=>"December");
}

/**
 * Generate array Alert Codes
 * @param null
 * @return array             
 *          Array codes
 * @package Lovs
 */
function getAlertName($code) {
    switch($code){
            case 'ALLER': return 'Allergies'; break;
            case 'DISAB': return 'Has Diabilities'; break;
            case 'DRUGS': return 'Is Drug Dependent'; break;
            case 'HANDI': return 'Is Handicap'; break;
            case 'IMPAI': return 'Is Impaired'; break;
            case 'NOTAP': return 'Not Applicabale'; break;
            case 'OTHER': return 'Other'; break;
    }
}

/**
 * Generate array PhilHealth Status
 * @param null
 * @return array             
 *          Array codes
 * @package Lovs
 */
function getPHICMemberType($code) {
    switch($code){
            case 'MM': return 'Member'; break;
            case 'DD': return 'Dependent'; break;
            default: return 'Non-member'; break;
    }
}
