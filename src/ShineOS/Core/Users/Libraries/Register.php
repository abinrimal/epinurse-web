<?php
namespace ShineOS\Core\Users\Libraries;

// User Entities
use ShineOS\Core\Users\Entities\Users;
use ShineOS\Core\Users\Entities\Contact;
use ShineOS\Core\Users\Entities\MDUsers;
use ShineOS\Core\Users\Entities\FacilityUser;

// Facility Entities
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityContact;
use ShineOS\Core\Facilities\Entities\FacilityWorkforce;
use ShineOS\Core\Facilities\Entities\DOHFacilityCode;

//Role
use ShineOS\Core\Users\Entities\RolesAccess;

// others
use ShineOS\Core\Users\Libraries\Salt;
use ShineOS\Core\Users\Libraries\UserActivation;
use Input, Hash;
use Shine\Libraries\IdGenerator;
use Shine\Libraries\Utils\Lovs;
use Illuminate\Support\Facades\Config;

use DB;

class Register {

    public function __construct() {
    }

    /**
     * Inserts facility info
     *
     * @return int
     */
    protected static function addFacility( $user_id, $type, $data ) {
        $mode = self::getMode();

        if($mode == 'ce')
        {
            $faci = $data;
            // sync ids
            $facility_id = $faci->facility_id;
            $facilitycontact_id = $faci->facility_contact->facilitycontact_id;

            // facility info
            $facility = new Facilities();
            $facility->facility_id = $facility_id; // change to hashed id
            // $facility->phic_accr_id = $faci->phic_accr_id;

            //if government, let us take the official information from the lov_doh_facility_codes
            if(strtolower($faci->ownership_type) == 'government') {
                $facility->facility_name = $faci->facility_name;
                $facility->provider_type = $faci->provider_type;
                $facility->ownership_type = $faci->ownership_type;
                $facility->facility_type = $faci->facility_type;
                // $facility->DOH_facility_code = $faci->DOH_facility_code;
                $facility->enabled_plugins = '["MaternalCare","FamilyPlanning","Pediatrics","Tuberculosis","Employment","Family","MedicalHistory","Philhealth","Geodata"]';
            } else {
                $facility->facility_name = $faci->facility_name;
                $facility->provider_type = $faci->provider_type;
                $facility->ownership_type = $faci->ownership_type;
                $facility->facility_type = $faci->facility_type;
                $facility->DOH_facility_code = NULL;
                $facility->enabled_plugins = '["MedicalHistory"]';
            }

            $facility->enabled_modules = '["Calendar","Laboratory"]';
            $facility->save();

            // facility contact
            $facilityContact = new FacilityContact();

            //get lov values for location
            $brgycode = $faci->facility_contact->barangay;
            $citycode = $faci->facility_contact->city;
            $provcode = $faci->facility_contact->province;
            $regioncode = $faci->facility_contact->region;

            $facilityContact->barangay = $brgycode;
            $facilityContact->city = $citycode;
            $facilityContact->province = $provcode;
            $facilityContact->region = $regioncode;
            $facilityContact->zip = $faci->facility_contact->zip;
            $facilityContact->country = "PHL";

            $facilityContact->facilitycontact_id = $facilitycontact_id;
            $facilityContact->facility_id = $facility_id;
            $facilityContact->save();

            return $facility_id;
        }
        else
        {
            // sync ids
            $facility_id = IdGenerator::generateId();
            $facilitycontact_id = IdGenerator::generateId();

            // facility info
            $facility = new Facilities();
            $facility->customizations = '{"appTitle":"EpiNurse","subTitle":"Epinurse + Nursing for Disaster Risk Reduction","companyName":"EpiNurse","byLine":"none","appTopLogo":"epinurse/epinurse.png","appLogo":"epinurse/epinurse.png","appBlurb":"Our aim is to develop a disaster risk reduction method to ensure health security in disaster prone communities. Collaborating with local Nepali nurses, named as EpiNurses (Epidemiology+Nurse), who conduct a participatory monitoring by using ICT toolkit, we work toward protection and promotion of health and safety in shelters and communities.","customcss":"epinurse/custom.css","loginstyle":{"introbox":{"width":"col-md-7","background":"epinurse/saas-bak.jpg","opacity":".3"},"logmebox":{"width":"col-md-5","background":"#F1E0B4","opacity":"1"}}}';

            // facility contact
            $facilityContact = new FacilityContact();

            //if government, let us take the official information from the lov_doh_facility_codes
            if(strtolower($data['ownership_type']) == 'government') {
                $doh = DOHFacilityCode::checkDoh($data['DOH_facility_code']);
                $facility->facility_name = title_case($doh->name);
                $facility->provider_type = strtoupper($data['provider_type']);
                $facility->ownership_type = strtoupper($data['ownership_type']);
                $facility->facility_type = title_case($doh->type);
                $facility->DOH_facility_code = $doh->code;

                $facilityContact->zip = $doh->zip;

                $facility->enabled_plugins = '["Family","MedicalHistory","EpiNurse","HealthAssessment","CommunityNursing","MotherAndChild"]';
            } else {
                $facility->facility_name = $data['facility_name'];
                $facility->provider_type = strtoupper($data['provider_type']);
                $facility->ownership_type = strtoupper($data['ownership_type']);
                $facility->facility_type = title_case($data['facility_type']);
                // $facility->DOH_facility_code = $data['DOH_facility_code'];
                $facility->enabled_plugins = '["Family","MedicalHistory","EpiNurse","HealthAssessment","CommunityNursing","MotherAndChild"]';

                if(array_key_exists('private_provider_ptr', $data)) {
                    $image = $data['private_provider_ptr'];
                    if(isset($image)) {
                        $private_provider_ptr = $data['private_provider_ptr'];
                        if($private_provider_ptr!=NULL || !empty($private_provider_ptr)) {
                            $ptr_image = self::ptr_upload($private_provider_ptr);
                        }
                        $facility->ptr_file_url = $ptr_image;
                    }
                } else {
                    $facility->ptr_file_url = NULL;
                }

                $facilityContact->zip = NULL;

            }
            //process location
            $facilityContact->ward_no = $data['ward_no'];
            $facilityContact->district = $data['district'];
            $facilityContact->city = $data['city'];
            $facilityContact->province = $data['province'];
            // $facilityContact->region = $data['region'];
            $facilityContact->country = "PHL";

            //default is CLOUD
            $facility->shine_type = "cloud";
            if($type == 'ce')
            {
                $facility->shine_type = "ce";
            }

            $facility->enabled_modules = '["SchoolAssessment","NurseRegistry"]';
            $facility->facility_id = $facility_id; //change to hashed id
            $fac = $facility;
            $facility->save();

            $facilityContact->facilitycontact_id = $facilitycontact_id;
            $facilityContact->facility_id = $facility_id;
            $facilityContact->save();

            return $facility_id;
        }
    }

    //ptr of the private facility
    protected static function ptr_upload($data){
        if ($data->isValid()) {
            $destinationPath = upload_base_path().'facility_ptr'; // upload path
            $extension = $data->getClientOriginalExtension();
            $fileName = "ptr_".rand(11111,99999).'_'.date('YmdHis').'.'.$extension;
            $originalName = $data->getClientOriginalName();
            $data->move($destinationPath, $fileName);
            return $fileName;
        }
        return false;
    }

    /**
     * Add admin user
     *
     * @return array
     */
    protected static function addAdminUser($user = NULL) {

        $mode = self::getMode();

        if($mode == 'ce')
        {
            // password and salt
            $password = $user->password;
            $salt = $user->salt;

            // activation code
            $activation_code = $user->activation_code;

            // sync ids
            $user_id = $user->user_id;
            $usercontact_id = $user->contact->usercontact_id;
            if($user->md_users) {
                $usermd_id = $user->md_users->usermd_id;
            }

            // add user
            $users = new Users();
            $users->user_id = $user_id;
            $users->activation_code = $activation_code;
            $users->first_name = $user->first_name;
            $users->last_name = $user->last_name;
            $users->email = $user->email;
            $users->status = 'Active'; //auto-active for Developer Edition
            $users->user_type = 'Admin'; //set to Developer for Developer Edition
            $users->salt = $salt;
            $users->password = $password;
            $users->save();

            // add user contact
            $contact = new Contact();
            $contact->phone = $user->contact->phone;
            $contact->mobile = $user->contact->mobile;
            $contact->barangay = $user->contact->barangay;
            $contact->city = $user->contact->city;
            $contact->province = $user->contact->province;
            $contact->region = $user->contact->region;
            $contact->country = "PHL";
            $contact->house_no = $user->contact->house_no;
            $contact->building_name = $user->contact->building_name;
            $contact->street_name = $user->contact->street_name;
            $contact->village = $user->contact->village;
            $contact->user_id = $user_id;
            $contact->usercontact_id = $usercontact_id;
            $contact->save();

            if($user->md_users) {
                // add user md info
                $md = new MDUsers();
                $md->user_id = $user_id;
                $md->usermd_id = $usermd_id;
                $md->profession = $user->md_users->profession;
                $md->professional_titles = $user->md_users->professional_titles;
                $md->professional_type_id = $user->md_users->professional_type_id;
                $md->professional_license_number = $user->md_users->professional_license_number;
                $md->s2 = $user->md_users->s2;
                $md->ptr = $user->md_users->ptr;
                $md->med_school = $user->md_users->med_school;
                $md->med_school_grad_yr = $user->md_users->med_school_grad_yr;
                $md->residency_trn_inst = $user->md_users->residency_trn_inst;
                $md->residency_grad_yr = $user->md_users->residency_grad_yr;
                $md->save();
            }

            $data = array();
            $data['users'] = $users;
            $data['user_id'] = $user_id;
            return $data;
        }
        else
        {
            // password and salt
            $password = $user['password'];
            $salt = Salt::generateRandomSalt(10);

            // activation code
            $activation_code = UserActivation::generateActivationCode();

            // sync ids
            $user_id = IdGenerator::generateId();
            $usercontact_id = IdGenerator::generateId();
            $usermd_id = IdGenerator::generateId();

            // add user
            $users = new Users();
            $users->user_id = $user_id;
            $users->activation_code = $activation_code;
            $users->first_name = title_case($user['first_name']);
            $users->last_name = title_case($user['last_name']);
            $users->email = $user['email'];
            $users->status = 'Active'; //auto-activate

            if($mode == 'developer'):
                $users->user_type = 'Developer'; //set to Developer for Developer Edition
            else:
                $users->user_type = 'Admin';
            endif;

            $users->salt = $salt;
            $users->password = Hash::make($password.$salt);
            $users->save();

            // add user contact
            $contact = new Contact();
            $contact->phone = $user['phone'];
            $contact->mobile = $user['mobile'];

            $contact->ward_no = $user['ward_no'];
            $contact->district = $user['district'];
            $contact->city = $user['city'];
            $contact->province = $user['province'];
            // $contact->region = $user['region'];
            $contact->country = "PHL";

            $contact->user_id = $user_id;
            $contact->usercontact_id = $usercontact_id;
            $contact->save();

            // add user md info
            $md = new MDUsers();
            $md->user_id = $user_id;
            $md->usermd_id = $usermd_id;
            $md->save();

            $data = array();
            $data['users'] = $users;
            $data['user_id'] = $user_id;
            return $data;
        }
    }

    /**
     * Establish relationship between user and facility
     *
     * @return null
     */
    public static function addFacilityUser ( $user_id, $facility, $data=null ) {

        $mode = self::getMode();
        $role_id = 7;
        if(array_key_exists('user_doctor',  (array)$data)) {
            $role_id = 1;
        }

        if($mode == 'ce')
        {
            $FacilityUser = new FacilityUser();
            $FacilityUser->facilityuser_id = $facility[0]->facilityuser_id;
            $FacilityUser->user_id = $facility[0]->user_id;
            $FacilityUser->facility_id = $facility[0]->facility_id;
            $FacilityUser->role_id = $role_id;
            $FID = $facility[0]->facilityuser_id;
            $FacilityUser->save();

            return $FID;
        }
        else
        {
            // add facility and user relationship
            $FacilityUser = new FacilityUser();
            $FacilityUser->facilityuser_id = IdGenerator::generateId();
            $FacilityUser->user_id = $user_id;
            $FacilityUser->facility_id = $facility;
            $FacilityUser->role_id = $role_id;
            $FID = $FacilityUser->facilityuser_id;
            $FacilityUser->save();

            return $FID;
        }
    }

    /**
     * Assign role to user
     *
     * @return null
     */
    // public static function addRole ( $facilityUserID ) {

    //     $mode = self::getMode();

    //     // add role
    //     $userRole = new RolesAccess();
    //     $userRole->role_id = 1;  //change to 0 for DevEd
    //     $userRole->facilityuser_id = $facilityUserID;
    //     $userRole->save();

    //     return $userRole;
    // }

    /**
    * Temporary save registration for review and activation
    *
    * @return null
    */
    public static function saveRegistration($type = NULL) {

        $mode = self::getMode();

        if($mode != 'training') {
            $image = Input::file('private_provider_ptr');
            if(isset($image)) {
                $private_provider_ptr = Input::file('private_provider_ptr');
                if($private_provider_ptr!=NULL || !empty($private_provider_ptr)) {
                    $ptr_image = self::ptr_upload($private_provider_ptr);
                }
                $_POST['ptr_file_url'] = $ptr_image;
            }

            //save registration for review and approval
            $tempRegistration = json_encode($_POST);
            $tempReg = DB::table('tempreg')->insert(
                    [
                        'regjson' => $tempRegistration,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                );
            $_POST['id'] = DB::getPdo()->lastInsertId();

            //send account email to Admin
            UserActivation::sendRegistrationNotification($_POST);
            //send account email to Registrant
            UserActivation::sendregistrationConfirmation($_POST);
        } else {
            self::initializeRegistration ($type = NULL);
        }
    }

    /**
     * Activate Registration
     *
     * @return null
     */
    public static function initializeRegistration ($type = NULL, $data = NULL) {
        $from_input = false;
        if(!$data) {
            $data = Input::all();
            $from_input = true;
        }

        if(!is_array($data)) {
            $data = (array)$data;
        }
        // dd($data);
        $mode = self::getMode();
        $users = self::addAdminUser($data);
        $facilityID = self::addFacility($users['user_id'], $type, $data); //return facilityID
        if($facilityID AND !$from_input) {
            DB::table('tempreg')->where('id', $data['id'])->delete();
        }
        $facility = Facilities::where('facility_id',$facilityID)->first(); //get new facility data
        $facilityUserID = self::addFacilityUser( $users['user_id'], $facilityID, $data);
        // $userRole = self::addRole($facilityUserID);

        //send email
        //but do not send if Developer Edition
        if($mode != 'developer' AND $mode!='training') {
            //send email to Admin
            UserActivation::sendAdminActivationCode($users['users'], $type, $facility->ownership_type);
        }
    }

    /**
     * Activate Registration CE
     *
     * @return null
     */
    public static function initializeRegistrationce ($data = NULL) {
        $from_input = false;
        if(!$data) {
            $data = Input::all();
            $from_input = true;
        }

        if(!is_array($data)) {
            $data = (array)$data;
        }

        $mode = self::getMode();
        $users = self::addAdminUser($data['user']);
        $facilityID = self::addFacility($users['user_id'], 'ce', $data['facility']); //return facilityID
        $facility = Facilities::where('facility_id',$facilityID)->first(); //get new facility data
        $facilityUserID = self::addFacilityUser( $users['user_id'], $data['user']->facility_user, $data);
        // $userRole = self::addRole($facilityUserID);

        return $facility;
    }

    public static function getActivationCode ($user) {

        //added for Community Edition registration
        //sends activation code
        $userFacilityID = DB::table('facility_user')->where('user_id', $user->user_id)->first();

        $actcode['user'] = Users::where('user_id', $user->user_id)->with('contact')->with('mdUsers')->with('facilityUser')->first();
        $actcode['facility'] = Facilities::where('facility_id',$actcode['user']->facilityUser[0]->facility_id)->with('facilityContact')->first();
        $actcode['userRole'] = FacilityUser::where('facilityuser_id',$userFacilityID->facilityuser_id)->first();

        $plain_txt = json_encode($actcode);

        $ac = self::encrypt_decrypt('encrypt', $plain_txt);

        $acfile = file_put_contents('public/uploads/'.$user->user_id.'.txt', $ac);

        //send email
        UserActivation::sendAdminCEActivationCode($user,$user->user_id);

    }

    private static function print_this( $object = array(), $title = '' ) {
        echo "<hr><h2>{$title}</h2><pre>";
        print_r($object);
        echo "</pre>";
    }

    public static function encrypt_decrypt($action, $string)
    {
        $output = false;

        $encrypt_method = getenv('ENCRYPT_METHOD');
        $secret_key = getenv('SECRET_KEY');
        $secret_iv = getenv('SECRET_IV');

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if( $action == 'decrypt' ){
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    public static function getMode()
    {
        $mode = Config::get('config.mode');
        return $mode;
    }
}
