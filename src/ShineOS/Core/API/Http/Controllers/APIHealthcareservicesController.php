<?php namespace ShineOS\Core\API\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Eloquent\UserRepository as UserRepository;
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser;
use Shine\Libraries\FacilityHelper;
use Shine\Libraries\IdGenerator;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices; 
use ShineOS\Core\Healthcareservices\Entities\GeneralConsultation; 
use ShineOS\Core\Healthcareservices\Entities\VitalsPhysical; 
use ShineOS\Core\Healthcareservices\Entities\Diagnosis; 
use ShineOS\Core\Healthcareservices\Entities\Disposition;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrder;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderLabExam;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderPrescription;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderProcedure;
use ShineOS\Core\Healthcareservices\Entities\Addendum;
use ShineOS\Core\LOV\Entities\LovLaboratories;
use Request, Auth, Validator;

class APIHealthcareservicesController extends Controller {
	
	private $healthcareRepository;
	private $UserRepository;

    public function __construct(UserRepository $UserRepository, HealthcareRepository $healthcareRepository) {
        $this->HealthcareRepository = $healthcareRepository;
        $this->UserRepository = $UserRepository;
    }

	public function index() {
		return response(json_encode(array('message'=>'No data')), 200);
	}
	
	public function visitsListByFacilityId_get(){ 
		$data = Request::all();
		if(array_key_exists('facility_id',$data)) {
			$facilities = Facilities::where('facility_id', $data['facility_id'])->count();
			if($facilities) {
				$visits = Healthcareservices::join('facility_patient_user','healthcare_services.facilitypatientuser_id','=','facility_patient_user.facilitypatientuser_id')
	                ->join('facility_user','facility_patient_user.facilityuser_id','=','facility_user.facilityuser_id')
	                ->join('facilities','facilities.facility_id','=','facility_user.facility_id')
	                ->join('patients','patients.patient_id','=','facility_patient_user.patient_id')
	                ->where('facilities.facility_id', $data['facility_id'])
	                ->where('healthcare_services.healthcareservicetype_id', 'GeneralConsultation')
	                ->where('facility_patient_user.deleted_at', NULL)
	                ->where('patients.deleted_at', NULL)
	                ->with('VitalsPhysical')
	                ->with(array('MedicalOrder'=>function($q){
				        $q->where('medicalorder_type', 'MO_LAB_TEST');
				        $q->with(array('MedicalOrderLabExam'=>function($mo_q){
					        $mo_q->with('LaboratoryResult');
					    }));
				    }))
	                ->orderBy('healthcare_services.encounter_datetime', 'DESC')
	                ->paginate(10); 

				$healthcare = $visits->toJson();
				// return new JsonResponse((json_decode($healthcare)), 200);
		        foreach ($visits as $k => $v) {
		            $v->seen_by = json_decode($this->UserRepository->findUserByFacilityUserID($v->seen_by));
		            $v->healthcare_disposition = json_decode($this->HealthcareRepository->findDispositionByHealthcareserviceid($v->healthcareservice_id));

		            foreach ($v->MedicalOrder as $key => $value) {
		            	$value->MedicalOrderLabExam = MedicalOrderLabExam::where('medicalorder_id',$value->medicalorder_id)->with('LaboratoryResult')->get();
		            	foreach ($value->MedicalOrderLabExam as $labk => $labv) {
		            		$LovLaboratories = LovLaboratories::lists('laboratorydescription','laboratorycode');
		            		$labv->laboratory_test_type = $LovLaboratories[$labv->laboratory_test_type];
		            	}
		            }
		        }
		        
				return new JsonResponse((array('status'=>'success','data'=>json_decode($visits->toJson()))), 200);
			} else {
				return new JsonResponse(array('status'=>'error', 'message'=>'Incorrect facility id'), 400);
			} 
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>'Facility id required'), 400);
		}
	}

	/**
	 * Healthcare Repository
	 */
	public function healthcaretypeByHealthcareserviceid_get() {
		$data = Request::all();
		if(array_key_exists('healthcareservice_id',$data)) { 
			if(array_key_exists('healthcaretype',$data)) {
				if($data['healthcaretype'] == 'vitals'){
					$result = json_decode($this->HealthcareRepository->findVitalsByHealthcareserviceid($data['healthcareservice_id']));
				} elseif ($data['healthcaretype'] == 'diagnosis') {
					$result = json_decode($this->HealthcareRepository->findDiagnosisByHealthcareserviceid($data['healthcareservice_id']));
				} elseif ($data['healthcaretype'] == 'examinations') {
					$result = json_decode($this->HealthcareRepository->findExaminationsByHealthcareserviceid($data['healthcareservice_id']));
				} elseif ($data['healthcaretype'] == 'medicalOrders') {
					$result = json_decode($this->HealthcareRepository->findMedicalOrdersByHealthcareserviceid($data['healthcareservice_id']));
				} elseif ($data['healthcaretype'] == 'disposition') {
					$result = json_decode($this->HealthcareRepository->findDispositionByHealthcareserviceid($data['healthcareservice_id']));
				} elseif ($data['healthcaretype'] == 'generalConsultation') {
					$result = json_decode($this->HealthcareRepository->findGeneralConsultationByHealthcareserviceid($data['healthcareservice_id']));
				} elseif ($data['healthcaretype'] == 'allHealthcareData') {

	                $result = json_decode(Healthcareservices::with(array('GeneralConsultation', 'VitalsPhysical', 'Diagnosis' => function($query) {
			                $query->with('DiagnosisICD10');
			            },
			            'Examination', 'MedicalOrder', 'Disposition', 'Addendum' => function($query) {
			                $query->orderBy('created_at', 'DESC');
			            }))
	                ->join('facility_patient_user','healthcare_services.facilitypatientuser_id','=','facility_patient_user.facilitypatientuser_id')
	                ->join('facility_user','facility_user.facilityuser_id','=','facility_patient_user.facilityuser_id')
	                ->join('patients','patients.patient_id','=','facility_patient_user.patient_id')
			        ->where('healthcareservice_id', $data['healthcareservice_id'])
			        // ->where('healthcare_services.healthcareservicetype_id', 'GeneralConsultation')
			        ->where('facility_patient_user.deleted_at', NULL)
			        ->where('patients.deleted_at', NULL)
			        ->get());
	                if(count($result)) {
	                	$fname = Facilities::where('facility_id',$result[0]->facility_id)->first();
	                	$result[0]->facility_name = $fname->facility_name;
		                foreach ($result[0]->medical_order as $key => $value) {
		                	if($value->medicalorder_type == 'MO_LAB_TEST') {
		                		$value->MedicalOrderLabExam = MedicalOrderLabExam::where('medicalorder_id',$value->medicalorder_id)->with('LaboratoryResult')->get();
				            	foreach ($value->MedicalOrderLabExam as $labk => $labv) {
				            		$LovLaboratories = LovLaboratories::lists('laboratorydescription','laboratorycode');
				            		$labv->laboratory_test_type = $LovLaboratories[$labv->laboratory_test_type];
				            	}
		                	}

		                	if($value->medicalorder_type == 'MO_MED_PRESCRIPTION') {
		                		$value->MedicalOrderPrescription = MedicalOrderPrescription::where('medicalorder_id',$value->medicalorder_id)->get();
		                	}

		                	if($value->medicalorder_type == 'MO_PROCEDURE') {
		                		$value->MedicalOrderProcedure = MedicalOrderProcedure::where('medicalorder_id',$value->medicalorder_id)->get();
		                	}
			            }
			        }

				} else {
					return new JsonResponse(array('status'=>'error', 'message'=>'Invalid healthcare type'), 400);
				}
				
				return new JsonResponse((array('status'=>'success','data'=>$result)), 200);
			} else {
				return new JsonResponse(array('status'=>'error', 'message'=>'Healthcare care type required'), 400);
			}
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>'Healthcare service id required'), 400);
		}
	} 

	public function insertHealthcareData_post() {
		$data = Request::all();
		$validator = Validator::make($data, [
			'facility_id' => 'required',
			'user_id' => 'required',
			'patient_id' => 'required',
			// 'healthcareservicetype_id' => 'required',
			'consultation_type' => 'required_with:disposition|in:ADMIN,CONSU,FOLLO,admin,consu,follo',
			'encounter_type' => 'required_with:disposition|in:I,O,i,o',
			'parent_service_id' => 'required_if:consultation_type,FOLLO,follo',
			'encounter_datetime' => 'required_with:disposition|date|date_format:Y-m-d H:i:s',
			'seen_by' => 'required_with:disposition',
			'complaint' => 'required_with:disposition|min:5', 
			'diagnosislist_id' => 'required_with:disposition|min:5',
			// 'diagnosis_notes' => 'required|min:5',
			// 'diagnosis_type' => 'required|in:ADMDX,CLIDI,FINDX,OTHER,WODIA,WORDX,admdx,clidi,findx,other,wodia,wordx',
			'disposition' => 'in:ADMDX,admdx,HOME,home,REFER,refer,UNKNW,unknw,HAMA,hama',
			'discharge_condition' => 'in:IMPRO,impro,RECOV,recov,UNIMP,unimp,UNKNW,unknw',
			'discharge_datetime' => 'date_format:Y-m-d H:i:s',
			// 'discharge_notes' => 'required|min:5',
			'bloodpressure_systolic' => 'required_with:disposition|integer|min:70|max:200',
			'bloodpressure_diastolic' => 'required_with:disposition|integer|min:40|max:100',
			'temperature' => 'required_with:disposition|integer|min:29|max:40',
			'heart_rate' => 'required_with:disposition|integer',
			'pulse_rate' => 'required_with:disposition|integer',
			'respiratory_rate' => 'required_with:disposition|integer',
			'height' => 'required_with:disposition|integer',
			'weight' => 'required_with:disposition|integer',
			
        ]);
		if ($validator->fails()==false) {

			$facilityuser_id = FacilityHelper::facilityUserId($data['user_id'], $data['facility_id']);
			// dd($facilityuser_id->facilityuser_id);
			if($facilityuser_id) {
				$FacilityPatientUser = FacilityPatientUser::where('patient_id',$data['patient_id'])->where('facilityuser_id',$facilityuser_id->facilityuser_id)->pluck('facilitypatientuser_id');

				if($FacilityPatientUser) {
					$healthcareData = new Healthcareservices;
					$healthcareData->healthcareservice_id = IdGenerator::generateId();

					if($data['consultation_type'] == 'FOLLO' || $data['consultation_type'] == 'follo') {
						$healthcareData->parent_service_id = (array_key_exists('parent_service_id',$data)) ? $data['parent_service_id'] : '';
					}

					$healthcareData->facilitypatientuser_id = $FacilityPatientUser;
					// $healthcareData->healthcareservicetype_id = $data['healthcareservicetype_id'];
					$healthcareData->healthcareservicetype_id = 'GeneralConsultation';
					$healthcareData->consultation_type = (array_key_exists('consultation_type',$data)) ? $data['consultation_type'] : '';
					$healthcareData->encounter_type = (array_key_exists('encounter_type',$data)) ? $data['encounter_type'] : '';
					$healthcareData->encounter_datetime = (array_key_exists('encounter_datetime',$data)) ? $data['encounter_datetime'] : '';
					$healthcareData->seen_by = $facilityuser_id->facilityuser_id;
					$healthcareservice_id = $healthcareData->healthcareservice_id;
					$insert_healthcareData = $healthcareData->save();

					$genCon = new GeneralConsultation;
	                $genCon->generalconsultation_id = IdGenerator::generateId();
	                $genCon->healthcareservice_id = $healthcareservice_id;
	                $genCon->complaint = (array_key_exists('complaint',$data)) ? $data['complaint'] : '';
	                $genCon->medicalcategory_id = 10;
	                $insert_genCon = $genCon->save();

	                $VitalsPhysical = new VitalsPhysical;
	                $VitalsPhysical->vitalphysical_id = IdGenerator::generateId();
	                $VitalsPhysical->healthcareservice_id = $healthcareservice_id;
	                $VitalsPhysical->bloodpressure_systolic = (array_key_exists('bloodpressure_systolic',$data)) ? $data['bloodpressure_systolic'] : '';
	                $VitalsPhysical->bloodpressure_diastolic = (array_key_exists('bloodpressure_diastolic',$data)) ? $data['bloodpressure_diastolic'] : '';
	                $VitalsPhysical->heart_rate = (array_key_exists('heart_rate',$data)) ? $data['heart_rate'] : '';
	                $VitalsPhysical->pulse_rate = (array_key_exists('pulse_rate',$data)) ? $data['pulse_rate'] : '';
	                $VitalsPhysical->respiratory_rate = (array_key_exists('respiratory_rate',$data)) ? $data['respiratory_rate'] : '';
	                $VitalsPhysical->height = (array_key_exists('height',$data)) ? $data['height'] : '';
	                $VitalsPhysical->weight = (array_key_exists('weight',$data)) ? $data['weight'] : '';
	                $VitalsPhysical->temperature = (array_key_exists('temperature',$data)) ? $data['temperature'] : '';
	                $insert_VitalsPhysical = $VitalsPhysical->save();

	                $Diagnosis = new Diagnosis;
	                $Diagnosis->diagnosis_id = IdGenerator::generateId();
	                $Diagnosis->healthcareservice_id = $healthcareservice_id;
	                $Diagnosis->diagnosis_type = 'CLIDI';
	                $Diagnosis->diagnosis_notes = (array_key_exists('diagnosis_notes',$data)) ? $data['diagnosis_notes'] : '';
	                $Diagnosis->diagnosislist_id = (array_key_exists('diagnosislist_id',$data)) ? $data['diagnosislist_id'] : '';
	                $insert_Diagnosis = $Diagnosis->save();

	                $Disposition = new Disposition;
	                $Disposition->disposition_id = IdGenerator::generateId();
	                $Disposition->healthcareservice_id = $healthcareservice_id;
	                $Disposition->disposition = (array_key_exists('disposition',$data)) ? $data['disposition'] : '';
	                $Disposition->discharge_condition = (array_key_exists('discharge_condition',$data)) ? $data['discharge_condition'] : '';
	                $Disposition->discharge_datetime = (array_key_exists('discharge_datetime',$data)) ? $data['discharge_datetime'] : '';
	                $Disposition->discharge_notes = (array_key_exists('discharge_notes',$data)) ? $data['discharge_notes'] : '';

	                $insert_Disposition = $Disposition->save();

					if($insert_healthcareData AND $insert_genCon AND $insert_Diagnosis AND $insert_Disposition AND $insert_VitalsPhysical) {
						return new JsonResponse(array('status'=>'success', 'message'=> 'Successfully added healthcare data.'), 200);
					} else {
						return new JsonResponse(array('status'=>'error', 'message'=> 'Failed to insert healthcare data.'), 400);
					}
				} else {
					return new JsonResponse(array('status'=>'error', 'message'=>'Patient does not exists.'), 400);
				}
			} else {
				return new JsonResponse(array('status'=>'error', 'message'=>'Invalid IDs'), 400);
			}
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
	}
}