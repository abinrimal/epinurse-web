@extends('layout.master')
@section('heads')
    <link type="text/css" rel="stylesheet" media="all" href="<?php print shineos_includes_url(); ?>css/install/install.css"/>
    <style type="text/css">
        #infoText {
            background-color: #00c0ef !important;
            color: #fff !important;
            border-color: #00acd6;
            border-radius: 3px;
            padding-right: 35px;
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            box-sizing: border-box;
            display: block;
        }
        .shineos-ui-progress-percent {
            color:rgba(0,0,0,0,.35);
            font-size:36px;
            font-weight:100;
        }
    </style>
@stop
@section('page-header')
  <section class="content-header">
    <h1>
      <i class="fa fa-refresh"></i>
      SHINE OS+ Data Sync
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Sync</li>
    </ol>
  </section>
@stop

@section('content')
@if(Session::has('message'))
    <div class="alert alert-warning alert-dismissible fade in" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>{{ Session::get('message') }}</strong>
    </div>
@endif
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">

            <div class="box-body">
                @if(config('config.mode') == 'ce')
                <div class="" id="syncgroup">
                    <div class="col-md-12">
                        <img src="{{ asset('public/dist/img/upload-cloud-outline.png') }}" class="col-md-3"/>
                        <h3 class="box-title text-primary">Welcome to Sync</h3>
                        <p>Use this facility to sync your SHINE OS+ Community Edition data with your cloud account and vice versa. This will ensure all your data are backed up, accessible and can be submitted to PHIE.</p>
                    </div>
                    <div class="row">
                        <hr />
                    </div>
                    <div class="col-md-3">
                        <div class="small-box bg-gray panels">
                            <div class="small-box-header">Download to Disk</div>
                            <div class="inner">
                                <h5>Use this if your SHINE OS+ CE is offline and internet is not available. You need to sync from a PC with internet access.</h5>
                            </div>
                            <a href="<?php site_url(); ?>sync/downloadLocalData" class="small-box-footer lead lead2" id="syncdisk-link" style="padding: 15px 0;">
                                <i class="fa fa-save fa-lg"></i> Download Data
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="small-box bg-green panels">
                            <div class="small-box-header">Sync to Cloud</div>
                            <div class="inner">
                                <h5>Use this if you want to send records to your SHINE OS+ cloud and internet is available.<br />&nbsp;</h5>
                            </div>
                            <a href="#" class="small-box-footer lead lead2" id="syncto-link" style="padding: 15px 0;">
                                <i class="fa fa-cloud-upload fa-lg"></i> Sync up
                            </a>

                            <div class="small-box-footer shineos_install_progress" id="syncto" style="display: none">
                                <div class="shineos-ui-progress">
                                    <div class="shineos-ui-progress-info"><?php _e("Syncing Up Local data to Cloud"); ?></div>
                                    <div class="shineos-ui-progress-bar" style="width: 0%;"></div>
                                    <span class="shineos-ui-progress-percent">0%</span>
                                </div>
                                <div id="installinfo"></div>
                                <div style="margin-top:5px;"><span id="timein"></span><span id="timeout"></span></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="small-box bg-yelloworange panels">
                            <div class="small-box-header">Sync from Cloud</div>
                            <div class="inner">
                                <h5>Use this if you want to download records from your SHINE OS+ cloud and internet is available.</h5>
                            </div>
                            <a href="#" class="small-box-footer lead lead2" id="syncfrom-link" style="padding: 15px 0;">
                                <i class="fa fa-cloud-download fa-lg"></i> Sync down
                            </a>

                            <div class="small-box-footer shineos_install_progress" id="syncfrom" style="display: none">
                                <div class="shineos-ui-progress">
                                    <div class="shineos-ui-progress-info"><?php _e("Syncing Down Cloud data to Local"); ?></div>
                                    <div class="shineos-ui-progress-bar" style="width: 0%;"></div>
                                    <span class="shineos-ui-progress-percent">0%</span>
                                </div>
                                <div id="installinfo"></div>
                                <div style="margin-top:5px;"><span id="timein"></span><span id="timeout"></span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="small-box bg-gray panels">
                            <div class="small-box-header">Backup Data</div>
                            <div class="inner">
                                <h5>Use this if you want to backup your SHINEOS+ CE Data. You will able to save it to disk.<br />&nbsp;</h5>
                            </div>
                            <a href="<?php site_url(); ?>sync/backupData" class="small-box-footer lead lead2" id="backup-link" style="padding: 15px 0;">
                                <i class="fa fa-save fa-lg"></i> Backup Data
                            </a>
                        </div>
                    </div>
                </div>
                @else
                <div class="">
                    <div class="col-md-12">
                        <img src="{{ asset('public/dist/img/upload-cloud-outline.png') }}" class="col-md-3"/>
                        <h3 class="box-title">Welcome to Sync</h3>
                        <p>Use this facility to sync your offline Community Edition with your cloud account. This will ensure all your data are backed up, accessible and can be submitted to PHIE.</p>
                        <p>Please ensure you have the downloaded data from your SHINE OS+ Community Edition at hand. Choose your data and click on Import Data.</p>
                    </div>
                    <div class="row">
                        <div class="clearfix"></div>
                        <hr />
                    </div>

                    <div class="shineos_install_progress" style="display: none">
                        <div class="shineos-ui-progress" id="installprogressbar">
                            <div class="shineos-ui-progress-info"><?php _e("Syncing SHINE OS+ CE"); ?></div>
                            <div class="shineos-ui-progress-bar" style="width: 0%;"></div>
                            <span class="shineos-ui-progress-percent">0%</span>
                        </div>
                        <div id="installinfo"></div>
                        <div style="margin-top:15px;"><span id="timein"></span><span id="timeout"></span></div>
                        <br class="clearfix" />
                    </div>

                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                        {!! Form::open(array('url'=>'sync/importData','method'=>'POST', 'files'=>true)) !!}
                            <div class="small-box bg-info panels">
                                <div class="small-box-header">Load Data From CE</div>
                                <div class="inner">
                                    <div class="control-group text-center">
                                        <h5>Choose your downloaded file from your SHINEOS+ CE.</h5>
                                        <div class="controls" style="width:200px; margin:10px auto;">
                                            {!! Form::file('importjson') !!}
                                        </div>
                                        
                                    </div>
                                </div>
                                {!! Form::button('<i class="fa fa-download"></i> Import CE Data', array('type' => 'submit', 'class' => 'small-box-footer lead lead2', 'id'=>'backup-link', 'style'=>'padding: 15px 0;width: 100%;border: 0;')) !!}
                            </div>
                        {!! Form::close() !!}
                        </div>

                        <div class="col-md-5">
                            <div class="small-box bg-gray panels">
                                <div class="small-box-header">Backup Data</div>
                                <div class="inner">
                                    <h5>Use this if you want to backup your SHINEOS+ Data. You will able to save it to disk.<br />Caution: This might take long.</h5>
                                </div>
                                <a href="<?php site_url(); ?>sync/backupData" class="small-box-footer lead lead2" id="backup-link" style="padding: 15px 0;">
                                    <i class="fa fa-save fa-lg"></i> Backup Data
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div><!-- /.box-body -->
            <div class="box-footer">
                &nbsp;
            </div>
          </div>
    </div>
</div>

@stop

@section('scripts')
<script>
function getTime() {
    time = new Date();

    var hh = time.getHours();
    var mm = time.getMinutes();
    var ss = time.getSeconds()

    thistime = hh + ":" + mm + ":" + ss;

    return thistime;
}

$(function(){

    $('#syncto-link').on('click', function(e){
        e.preventDefault();
        $('#syncto-link').fadeOut();

        $.ajax({
            url: '<?php site_url(); ?>sync/toCloud',
            type: "GET",
            beforeSend: function() {
                timein = getTime();
                  $('#syncto #timein').text("Start time: "+timein);
                  data = 'Sync is in progress. Please wait...';
                  installprogress({{ $localtables }}, 'syncto');
            },
            error: function() {
                if(response == 'abort'){
                    xhr.abort();
                    xhr = null;
                }
                installprogressStop('syncto');
            },
            success: function(data) {
                console.log(data);
                progressBar(data);
                installprogress({{ $localtables }}, 'syncto');
            }
        });
    });

    $('#syncfrom-link').on('click', function(e){
        e.preventDefault();
        $('#syncfrom-link').fadeOut();

        $.ajax({
            url: '<?php site_url(); ?>sync/fromCloud',
            type: "GET",
            beforeSend: function() {
                timein = getTime();
                  $('#syncfrom #timein').text("Start time: "+timein);
                  data = 'Sync is in progress. Please wait...';
                  installprogress({{ $localtables }}, 'syncfrom');
            },
            error: function() {
                if(response == 'abort'){
                    xhr.abort();
                    xhr = null;
                }
                installprogressStop('syncfrom');
            },
            success: function(data) {
                console.log(data);
                installprogress({{ $localtables }}, 'syncfrom');
            }
        });
    });
});

installprogressStopped = false;

function installprogressStop(thisbox) {
    var holder = $('#'+thisbox),
        bar = $(".shineos-ui-progress-bar", holder),
        percent = $(".shineos-ui-progress-percent", holder);
    bar.width('0%');
    percent.html('0%');
    percent.fadeOut();
    bar.fadeOut();
    installprogressStopped = true;
}

function installprogress(countr, thisbox, reset)
{
    if (installprogressStopped) {
        installprogressStopped = false;
        return false;
    }

    var holder = $('#'+thisbox),
        bar = $(".shineos-ui-progress-bar", holder),
        percent = $(".shineos-ui-progress-percent", holder),
        reset = typeof reset === 'undefined' ? true : reset;

    if (reset === true) {
        bar.width('0%');
        percent.html('0%');
        holder.fadeIn();
    }

    <?php $log_file_url = userfiles_url().'/logs/sync_log.txt'; ?>
    $.get('<?php print $log_file_url ?>', function (data) {
        var data = data.replace(/\r/g, '');
        var arr = data.split('\n'),
            l = arr.length,
            i = 0,
            lastline = arr[l-2],
            percentage = Math.round( ((l-3) / countr) * 100);
        
        if(percentage > 100) {
            percentage = 100;
        }
        bar[0].style.width = percentage + '%';
        percent.html(percentage + '%');

        if(lastline == 'done') {
            percent.html('100%');
            installprogressStop(thisbox);
            $("#"+thisbox +" #installinfo").html("<h4>SHINE OS+ Sync complete.</h4><p><a href='{{ url('/sync') }}' class='text-white'> Refresh the page </a></p>");
            timeout = getTime();
            $("#"+thisbox +" #timeout").text(" - Finish time: "+timeout);
        } else if(lastline == 'nothing') {
            percent.html('100%');
            installprogressStop(thisbox);
            $("#"+thisbox +" #installinfo").html("<h4>Nothing to sync.</h4><p><a href='{{ url('/sync') }}' class='text-white'> Refresh the page </a></p>");
            timeout = getTime();
            $("#"+thisbox +" #timeout").text(" - Finish time: "+timeout);
        } else if(lastline == 'fail') {
            percent.html('100%');
            installprogressStop(thisbox);
            $("#"+thisbox +" #installinfo").html("<h4'>Sync error.</h4><p><a href='{{ url('/sync') }}' class='text-white'> Please try again </a></p>");
            timeout = getTime();
            $("#"+thisbox +" #timeout").text(" - Finish time: "+timeout);
        } else {
            $("#"+thisbox +" #installinfo").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> "+lastline);
            setTimeout(function(){
                installprogress(countr, thisbox, true);
            }, 1500);
        }

    });
}
</script>
@stop
