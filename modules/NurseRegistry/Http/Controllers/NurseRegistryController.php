<?php namespace Modules\NurseRegistry\Http\Controllers;

use Modules\NurseRegistry\Entities\NurseRegistry;
use Pingpong\Modules\Routing\Controller;

use Shine\Libraries\IdGenerator;
use Webpatser\Uuid\Uuid;

use Input,Session,Redirect;

class NurseRegistryController extends Controller {
	
	public function index()
	{
		$facility = Session::get('facility_details');

		$data['nurses'] = NurseRegistry::where('facility_id',$facility->facility_id)->get();

		return view('nurseregistry::index')->with($data);
	}

	public function new()
	{
		return view('nurseregistry::new');
	}

	public function save()
	{
		$nurse_id = Input::get('nurse_id');

		$nurse = NurseRegistry::where('nurse_id',$nurse_id)->first();

		if(!$nurse)
		{
			$nurse = new NurseRegistry;

			$nurse->nurse_id = $nurse_id = IdGenerator::generateId();

			$facility = Session::get('facility_details');
			$nurse->facility_id = $facility->facility_id;

			$user = Session::get('user_details');
			$nurse->user_id = $user->user_id;
			
			$nurse->uuid = Uuid::generate(4)->string;
		}

		$nurse->nan_number = Input::has('nan_number') ? Input::get('nan_number') : NULL;
		$nurse->first_name = Input::has('first_name') ? Input::get('first_name') : NULL;
		$nurse->middle_name = Input::has('middle_name') ? Input::get('middle_name') : NULL;
		$nurse->last_name = Input::has('last_name') ? Input::get('last_name') : NULL;
		$nurse->sex = Input::has('sex') ? Input::get('sex') : NULL;
		$nurse->citizenship_number = Input::has('citizenship_number') ? Input::get('citizenship_number') : NULL;
		$nurse->date_of_birth_in_ad = Input::has('date_of_birth_in_ad') ? Input::get('date_of_birth_in_ad') : NULL;
		$nurse->date_of_birth_in_bs = Input::has('date_of_birth_in_bs') ? Input::get('date_of_birth_in_bs') : NULL;
		$nurse->age = Input::has('age') ? Input::get('age') : NULL;
		$nurse->temp_house_number = Input::has('temp_house_number') ? Input::get('temp_house_number') : NULL;
		$nurse->temp_ward_number = Input::has('temp_ward_number') ? Input::get('temp_ward_number') : NULL;
		$nurse->temp_municipality = Input::has('temp_municipality') ? Input::get('temp_municipality') : NULL;
		$nurse->temp_district = Input::has('temp_district') ? Input::get('temp_district') : NULL;
		$nurse->temp_country = Input::has('temp_country') ? Input::get('temp_country') : NULL;
		$nurse->temp_zipcode = Input::has('temp_zipcode') ? Input::get('temp_zipcode') : NULL;
		$nurse->permanent_house_number = Input::has('permanent_house_number') ? Input::get('permanent_house_number') : NULL;
		$nurse->permanent_ward_number = Input::has('permanent_ward_number') ? Input::get('permanent_ward_number') : NULL;
		$nurse->permanent_municipality = Input::has('permanent_municipality') ? Input::get('permanent_municipality') : NULL;
		$nurse->permanent_district = Input::has('permanent_district') ? Input::get('permanent_district') : NULL;
		$nurse->permanent_country = Input::has('permanent_country') ? Input::get('permanent_country') : NULL;
		$nurse->permanent_zipcode = Input::has('permanent_zipcode') ? Input::get('permanent_zipcode') : NULL;
		$nurse->professional_status = Input::has('professional_status') ? Input::get('professional_status') : NULL;
		$nurse->designation = Input::has('designation') ? Input::get('designation') : NULL;
		$nurse->office = Input::has('office') ? Input::get('office') : NULL;

        $quali_to_save = array();
        if(Input::has('qualifications'))
        {
        	$qualifications = Input::get('qualifications');
            for($i=0;$i<count($qualifications['degree']);$i++) {
                $quali_to_save[] = ['degree'=>$qualifications['degree'][$i],'university'=>$qualifications['university'][$i],'date'=>$qualifications['date'][$i]];
            }
        }
        $nurse->qualifications = json_encode($quali_to_save);

        $trainings_to_save = array();
        if(Input::has('trainings'))
        {
        	$trainings = Input::get('trainings');
            for($i=0;$i<count($trainings['name']);$i++) {
                $trainings_to_save[] = ['name'=>$trainings['name'][$i],'organized_by'=>$trainings['organized_by'][$i],'date'=>$trainings['date'][$i]];
            }
        }
        $nurse->trainings = json_encode($trainings_to_save);

		$nurse->save();

        Session::flash('flash_type', 'alert-success alert-dismissible');
        Session::flash('flash_message', 'Success! The nurse record has been saved.'); 

		return Redirect::to('nurseregistry/update/'.$nurse_id);
	}

	public function update($id)
	{
		$data['nurse'] = NurseRegistry::where('nurse_id',$id)->first();

		return view('nurseregistry::edit')->with($data);
	}

	public function delete($id)
	{
		$nurse = NurseRegistry::where('nurse_id',$id)->first();
		$nurse->delete();

        Session::flash('flash_type', 'alert-danger alert-dismissible');
        Session::flash('flash_message', 'Success! The nurse record has been deleted.'); 

		return Redirect::to('nurseregistry');
	}
	
}