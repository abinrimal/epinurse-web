<?php

use ShineOS\Core\Patients\Entities\Patients;
use Plugins\Philhealth\PhilhealthModel as Philhealth;
use Plugins\Philhealth\PhilhealthPMRFModel;
use Plugins\Philhealth\PhilhealthPMRFDependentModel;

use Shine\Repositories\Eloquent\UserRepository as UserRepository;
use Shine\Repositories\Contracts\UserRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\Libraries\UserHelper;
use Shine\Libraries\FacilityHelper;
use Shine\User;
use Shine\Plugin;
use Shine\Libraries\Utils;
use ShineOS\Core\PHIE\Http\Middleware\PHIE_Soap_Connect;

class PhilhealthController extends Controller
{

    protected $moduleName = 'Patients';
    protected $modulePath = 'patients';

    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
        $this->middleware('auth');
        $this->ref_mem_cat = getArrPhilhealthMemberCategory();

        $this->userInfo = UserHelper::getUserInfo();
        $this->facility = FacilityHelper::facilityInfo();
        $this->facilityInfo = $this->facility->facility_id;

        $this->wskey = $this->facility->wskey; // Web Service Key
        $this->ekey = $this->facility->ekey;
    }

    public function index()
    {
        //no index
    }

    public function save()
    {
        $phic = Input::get('philhealth');
        $patient_id = Input::get('patient_id');
        $intent = Input::get('intent');

        $philhealth = "";
        if($intent == "save")
        {
            $philhealth = new Philhealth;
            $philhealth->patient_philhealthinfo_id = IdGenerator::generateId();
        }
        else
        {
            $philhealth = Philhealth::where('patient_id','=',$patient_id)->first();
        }
        $philhealth->patient_id = $patient_id;
        $philhealth->philhealth_id = $phic['MEMID_NO'];
        $philhealth->philhealth_category = $phic['MEM_CATEGORY'];
        $philhealth->member_type = $phic['Pat_PHIC_Membership_Type'];
        if(isset($phic['CCT']))
        {
            $philhealth->ccash_transfer = $phic['CCT'];
        }
        $philhealth->benefit_type = $phic['Pat_Benefit_Package'];
        $philhealth->pamilya_pantawid_id = $phic['Pat_Pantawid_Pamilya_Member'];
        if(isset($phic['IndigenousGroup']))
        {
            $philhealth->indigenous_group = $phic['IndigenousGroup'];
        }
        $philhealth->benefactor_member_id = $phic['WorMemid'];
        $philhealth->benefactor_first_name = $phic['WorFName'];
        $philhealth->benefactor_last_name = $phic['WorLName'];
        $philhealth->benefactor_middle_name = $phic['WorMName'];
        $philhealth->benefactor_name_suffix = $phic['WorExtName'];
        $philhealth->benefactor_relation = $phic['relation'];
        $philhealth->For_Enlistment = $phic['For_Enlistment'];

        $philhealth->save();

        header('Location: '.site_url().'plugin/call/Philhealth/Philhealth/view/'.$patient_id);
        exit;
    }

    public function view($id)
    {
        $patient = Patients::with('patientContact')->where('patient_id','=', $id)->first();
        $patient_philhealth = Philhealth::where('patient_id','=',$id)->first();

        View::addNamespace('pluginform', plugins_path().'Philhealth');
        echo View::make('pluginform::master', array('patient' => $patient, 'philhealth' => $patient_philhealth))->render();
    }

    public function viewPMRF($id)
    {
        $nationality = nations();
        $ref_mem_cat = $this->ref_mem_cat;
        $getArrRelationship = getArrRelationship();

        $utilities = new Utils;
        $religion = $utilities->religion();
        $civilStatus = getArrCivilStatus();
        $bloodType = getArrBloodType();
        $patient = Patients::with('patientContact')->where('patient_id','=', $id)->first();
        $patient_philhealth = Philhealth::where('patient_id','=',$id)->first();
        View::addNamespace('pluginform', plugins_path().'Philhealth');
        echo View::make('pluginform::pmrf', array('patient' => $patient, 'philhealth' => $patient_philhealth, 'nationality' => $nationality, 'religion'=>$religion, 'ref_mem_cat'=>$ref_mem_cat, 'civilStatus'=>$civilStatus, 'bloodType'=>$bloodType, 'getArrRelationship'=>$getArrRelationship))->render();
    }

    public function apply()
    {
        // dd(Input::all());
        $input_dep = Input::get('phicDependent');
        $patient_id = Input::get('patient_id');
        if($patient_id) {
            $pmrf = new PhilhealthPMRFModel();
            $pmrf->philhealthpmrf_id = IdGenerator::generateId();
            $pmrf->Pat_Facility_No = $patient_id;
            $pmrf->Encounter_ID = '';
            $pmrf->app_date = date('Y-m-d');
            $pmrf->patient_id = $patient_id;
            $pmrf->MEMID_NO = Input::get('MEMID_NO');
            $pmrf->Purpose = Input::get('Purpose');
            $pmrf->LASTNAME = Input::get('Last_Name');
            $pmrf->FIRSTNAME = Input::get('First_Name');
            $pmrf->MIDDLENAME = Input::get('Middle_Name');
            $pmrf->SUFFIX = Input::get('Suffix');
            $pmrf->Maiden_Last_Name = Input::get('Maiden_Last_Name');
            $pmrf->Maiden_First_Name = Input::get('First_Name');
            $pmrf->Maiden_Midde_Name = Input::get('Maiden_Middle_Name');
            $pmrf->BIRTH_DATE = Input::get('Patient_Birthdate');
            $pmrf->BIRTH_COUNTRY = Input::get('BIRTH_COUNTRY');
            $pmrf->POB_PROV = Input::get('POB_PROV');
            $pmrf->POB_MUNI = Input::get('POB_MUNI');
            $pmrf->POB_ZIPCODE = Input::get('Zip_Code');
            $pmrf->SEX = Input::get('Sex');
            $pmrf->MARITAL_STATUS = Input::get('Civil_Status_Dep');
            $pmrf->NATIONALITY = Input::get('Nationality');
            $pmrf->religion = Input::get('Religion');
            $pmrf->bloodtype = Input::get('blood_type');
            $pmrf->gsis_no = Input::get('gsis_no');
            $pmrf->sss_no = Input::get('sss_no');
            $pmrf->mec_no = Input::get('mec_no');
            $pmrf->pnp_no = Input::get('pnp_no');
            $pmrf->TIN_NO = Input::get('TIN');
            $pmrf->PRES_UNIT = Input::get('Unit_Room_Number');
            $pmrf->PRES_BLDG = Input::get('Building_Name');
            $pmrf->PRES_HOUSE_NO = Input::get('House_Bldg_No');
            $pmrf->PRES_STREET = Input::get('Street');
            $pmrf->PRES_SUBD = Input::get('Subdivision_Village');
            $pmrf->PRES_REG_PSGC = Input::get('Region');
            $pmrf->PRES_PROV_PSGC = Input::get('Province');
            $pmrf->PRES_MUN_PSGC = Input::get('City');
            $pmrf->PRES_BGY = Input::get('Brgy');
            $pmrf->PRES_COUNTRY = Input::get('country');
            $pmrf->PRES_ZIPCODE = Input::get('Zip_Code');
            $pmrf->UNIT = Input::get('Unit_Room_Number');
            $pmrf->BLDG = Input::get('Building_Name');
            $pmrf->address = Input::get('House_Bldg_No').' '.Input::get('Street').' '.Input::get('Subdivision_Village');
            $pmrf->HOUSE_NO = Input::get('House_Bldg_No');
            $pmrf->STREET = Input::get('Street');
            $pmrf->SUBD = Input::get('Subdivision_Village');
            $pmrf->REGION_PSGC = Input::get('Region');
            $pmrf->PROVINCE_PSGC = Input::get('Province');
            $pmrf->MUNICIPALITY_PSGC = Input::get('City');
            $pmrf->BARANGAY = Input::get('Brgy');
            $pmrf->RESI_COUNTRY = Input::get('country');
            $pmrf->ZIP_CODE = Input::get('Zip_Code');
            $pmrf->Pat_Contact_Landline = Input::get('Patient_Phone');
            $pmrf->Pat_Contact_Mobile = Input::get('Patient_Mobile');
            $pmrf->Pat_Contact_EmailAddress = Input::get('Patient_Email');
            $pmrf->Spouse_MemID_No = '';

            $pmrf->MEM_CATEGORY = Input::get('Member_Category');
            $pmrf->PREMIS_MEM_CATEGORY = Input::get('Member_Category');
            $pmrf->PREMIS_mem_type = Input::get('Member_Type');
            $pmrf->InfEco_InfSector_Specify = Input::get('InfEco_InfSector_Specify');
            $pmrf->InfEco_InfSector_Specify_MoIncome = Input::get('InfEco_InfSector_Specify_MoIncome');
            $pmrf->InfEco_InfSector_Specify_NoIncome = Input::get('InfEco_InfSector_Specify_NoIncome');
            $pmrf->InfEco_SelfEarn_Specify = Input::get('InfEco_SelfEarn_Specify');
            $pmrf->InfEco_SelfEarn_MoIncome = Input::get('InfEco_SelfEarn_MoIncome');
            $pmrf->InfEco_OrgGroup_Specify = Input::get('InfEco_OrgGroup_Specify');
            $pmrf->Sponsored_LGU = Input::get('Sponsored_LGU');
            $pmrf->Sponsored_NGA = Input::get('Sponsored_NGA');
            $pmrf->Sponsored_Others_Specify = Input::get('Sponsored_Others_Specify');
            $pmrf->Lifetime_Member_RetirementDate = Input::get('Lifetime_Member_RetirementDate');
            $pmrf->foreign_address = '';
            $pmrf->foreign_tel_no = '';
            $pmrf->country_code = '';
            $pmrf->cov_start = '';
            $pmrf->cov_end = '';
            $pmrf->memdep_no = '';
            $pmrf->status = '';
            $pmrf->pmrf_status = '';
            $pmrf_save = $pmrf->save();

            // dd($pmrf);
            // dd($input_dep['Relationship']);
            $pmrf_dep = [];
            foreach ($input_dep['Relationship'] as $k => $v) {
                $pmrf_dep[$k] = new PhilhealthPMRFDependentModel();
                $pmrf_dep[$k]->philhealthpmrf_dep_id = IdGenerator::generateId();
                $pmrf_dep[$k]->Pat_Facility_No= $patient_id;
                $pmrf_dep[$k]->Encounter_ID = '';
                $pmrf_dep[$k]->MEMID_NO = Input::get('MEMID_NO');
                $pmrf_dep[$k]->Dep_MemID_No = $input_dep['PIN_Dep'][$k];
                $pmrf_dep[$k]->Purpose = Input::get('Purpose');
                $pmrf_dep[$k]->Dep_Lastname= $input_dep['Last_Name'][$k];
                $pmrf_dep[$k]->Dep_Firstname= $input_dep['First_Name'][$k];
                $pmrf_dep[$k]->Dep_Middlename= $input_dep['Middle_Name'][$k];
                $pmrf_dep[$k]->Dep_Suffix= array_key_exists('Suffix', $input_dep) ? $input_dep['Suffix'][$k] : '';
                $pmrf_dep[$k]->maidenname = $input_dep['First_Name'][$k].' '.$input_dep['maidenmiddlename'][$k].' '.$input_dep['maidenlastname'][$k];
                $pmrf_dep[$k]->Dep_Disability = $input_dep['Disability_Dep'][$k];
                $pmrf_dep[$k]->disability= $input_dep['Disability_Dep'][$k];
                $pmrf_dep[$k]->Dep_birthdate = $input_dep['Birth_Date'][$k];
                $pmrf_dep[$k]->Dep_Sex = array_key_exists('Dep_Sex', $input_dep) ? $input_dep['Dep_Sex'][$k] : '';
                $pmrf_dep[$k]->prov_code = $input_dep['province'][$k];
                $pmrf_dep[$k]->citymun_code = $input_dep['city'][$k];
                $pmrf_dep[$k]->relation = $input_dep['Relationship'][$k];
                $pmrf_dep[$k]->MARITAL_STATUS = $input_dep['Civil_Status'][$k];
                $pmrf_dep[$k]->status = $input_dep['status'][$k];
                $pmrf_dep[$k]->emp_stat = $input_dep['emp_stat'][$k];
                $pmrf_dep[$k]->occupation = $input_dep['occupation'][$k];
                $pmrf_dep[$k]->income = $input_dep['income'][$k];
                $pmrf_dep_save[$k] = $pmrf_dep[$k]->save();
            }

            // dd($pmrf_dep);
            // dd(Input::all(), $pmrf, $pmrf_dep);


        }

        header('Location: '.site_url().'patients/view/'.$patient_id);
        exit;
    }

    public function PHIEconnect_get_phicPatientID() {
        // 108 Phil Health Number Successfully Retrieved!
        // 109 Phil Health Number Doesn't Exists!
        /*if( $this->checkVPN() == 'error') {
            echo 'error';
        } else {*/
            $input = Input::all();
            $patientdata = json_decode($input['patient'], true);
            $soap_result = NULL;

            $wsParam = array(
                 'Client_LastName' => !empty($patientdata['last_name']) ? $patientdata['last_name'] : "",
                 'Client_FirstName' => !empty($patientdata['first_name']) ? $patientdata['first_name'] : "",
                 'Client_MiddleName' => !empty($patientdata['middle_name']) ? $patientdata['middle_name'] : "",
                 'Client_SuffixName' => !empty($patientdata['name_suffix']) ? $patientdata['name_suffix'] : "NA",
                 'Client_Sex' => !empty($patientdata['gender']) ? $patientdata['gender'] : "",
                 'Client_DateofBirth' => !empty($patientdata['birthdate']) ? $patientdata['birthdate'] : ""
            );

            $PHIE_Soap_Connect = new PHIE_Soap_Connect;
            $result = $PHIE_Soap_Connect->phie_test_two(NULL,$wsParam,'get_phicPatientID');

            if($result) {
                if($result['response_code'] == '108') {
                    $res = json_decode($result['soap_result']);
                    $soap_result = $res->PHIE;
                } else if($result['response_code'] == '109') {
                    $soap_result = $result['response_desc'];
                } else {
                    $soap_result = $result['response_desc'];
                }
            }

            echo $soap_result;
        //}
    }

    public function checkVPN() {
        //let us check server VPN
        $str = exec("ping -c2 210.4.103.175", $input, $result);
        if ($result == 0){
        } else {
          echo shell_exec('ikec -r philhealth-test -a');
            $result == 0;
        }


        if(!$this->wskey) {
            return 'error';
        }
        if(!$this->ekey) {
            return 'error';
        }
        //if both keys are present - continue
        if($this->wskey AND $this->ekey AND $result = 0) {
            return 'ok';
        }
    }
}
