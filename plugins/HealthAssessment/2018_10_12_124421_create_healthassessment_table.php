<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHealthassessmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('healthassessment_service')!=TRUE) { 
            Schema::create('healthassessment_service', function (Blueprint $table) {
                $table->increments('id');
                $table->string('healthassessment_id', 32);
                $table->string('healthcareservice_id', 32);

                $table->string('uuid', 100)->nullable();
                $table->string('owner_uuid', 100)->nullable();

                $table->string('student_employee', 15)->nullable();

                $table->string('school', 100)->nullable();
                $table->string('class', 100)->nullable();

                $table->integer('vaccine_bcg')->nullable();
                $table->integer('vaccine_dpt')->nullable();
                $table->integer('vaccine_opv')->nullable();
                $table->integer('vaccine_pcv')->nullable();
                $table->integer('vaccine_mr')->nullable();
                $table->integer('vaccine_je')->nullable();
                $table->integer('vaccine_td')->nullable();
                $table->integer('vaccine_oth')->nullable();
                $table->string('vaccine_oth_spec')->nullable();

                $table->integer('id_watery_diarrhea')->nullable();
                $table->integer('id_bloody_diarrhea')->nullable();
                $table->integer('id_flaccidpar')->nullable();
                $table->integer('id_ari')->nullable();
                $table->integer('id_dysentery')->nullable();
                $table->integer('id_ajs')->nullable();
                $table->integer('id_susmeningitis')->nullable();
                $table->integer('id_sustetanus')->nullable();
                $table->integer('id_fever')->nullable();
                $table->integer('id_hemfever')->nullable();

                // For employee health
                $table->integer('mc_injuretrauma')->nullable();
                $table->integer('mc_injuretrauma_spec')->nullable();

                /*--------------------------------------------------------------*/

                // For student Health
                $table->integer('mc_injuretrauma_head')->nullable();
                $table->integer('mc_injuretrauma_spinal')->nullable();
                $table->integer('mc_injuretrauma_torso')->nullable();
                $table->integer('mc_injuretrauma_leftleg')->nullable();
                $table->integer('mc_injuretrauma_rightleg')->nullable();
                $table->integer('mc_injuretrauma_leftarm')->nullable();
                $table->integer('mc_injuretrauma_rightarm')->nullable();

                $table->string('mc_injuretrauma_head_spec', 100)->nullable();
                $table->string('mc_injuretrauma_spinal_spec', 100)->nullable();
                $table->string('mc_injuretrauma_torso_spec', 100)->nullable();
                $table->string('mc_injuretrauma_leftleg_spec', 100)->nullable();
                $table->string('mc_injuretrauma_rightleg_spec', 100)->nullable();
                $table->string('mc_injuretrauma_leftarm_spec', 100)->nullable();
                $table->string('mc_injuretrauma_rightarm_spec', 100)->nullable();

                /*---------------------------------------------------------------*/

                $table->integer('mc_skindisease')->nullable();
                $table->string('mc_skindisease_spec', 100)->nullable();
                $table->integer('mc_allergies')->nullable();
                $table->string('mc_allergies_spec', 100)->nullable();
                $table->integer('mc_respiratory')->nullable();
                $table->string('mc_respiratory_spec', 100)->nullable();
                $table->integer('mc_cardio')->nullable();
                $table->string('mc_cardio_spec', 100)->nullable();
                $table->integer('mc_endocrine')->nullable();
                $table->string('mc_endocrine_spec', 100)->nullable();
                $table->integer('mc_urinary')->nullable();
                $table->string('mc_urinary_spec', 100)->nullable();
                $table->integer('mc_reproductive')->nullable();
                $table->string('mc_reproductive_spec', 100)->nullable();
                $table->integer('mc_commhearing')->nullable();
                $table->string('mc_commhearing_spec', 100)->nullable();
                $table->integer('mc_vision')->nullable();
                $table->string('mc_vision_spec', 100)->nullable();
                $table->integer('mc_oraldental')->nullable();
                $table->string('mc_oraldental_spec', 100)->nullable();
                $table->integer('mc_psych')->nullable();
                $table->string('mc_psych_spec', 100)->nullable();
                $table->integer('mc_physical')->nullable();
                $table->string('mc_physical_spec', 100)->nullable();
                $table->integer('mc_cognitive')->nullable();
                $table->string('mc_cognitive_spec', 100)->nullable();
                $table->integer('mc_bowel')->nullable();
                $table->string('mc_bowel_spec', 100)->nullable();
                $table->integer('mc_bladder')->nullable();
                $table->string('mc_bladder_spec', 100)->nullable();
                $table->string('mc_oth_spec', 100)->nullable();
                $table->integer('medications')->nullable();
                $table->string('medications_spec', 100)->nullable();
                $table->integer('treatment')->nullable();
                $table->string('treatment_spec', 100)->nullable();
                $table->integer('procedure')->nullable();
                $table->string('procedure_spec', 100)->nullable();
                $table->integer('counselling')->nullable();
                $table->string('counselling_spec', 100)->nullable();
                $table->integer('suspensionschool')->nullable();
                $table->string('reasonsuspension',100)->nullable();
                $table->integer('antisocial')->nullable();
                $table->integer('delinquency')->nullable();
                $table->integer('violence')->nullable();
                $table->integer('smoking')->nullable();
                $table->integer('alcohol')->nullable();
                $table->integer('substanceabuse')->nullable();
                $table->integer('suicidalthoughts')->nullable();
                $table->integer('suicidalattempts')->nullable();
                
                $table->string('breakfast_place',100)->nullable();
                $table->string('breakfast_time',100)->nullable();
                $table->string('breakfast_type_food',100)->nullable();
                $table->string('lunch_place',100)->nullable();
                $table->string('lunch_time',100)->nullable();
                $table->string('lunch_type_food',100)->nullable();
                $table->string('snacks_place',100)->nullable();
                $table->string('snacks_time',100)->nullable();
                $table->string('snacks_type_food',100)->nullable();
                $table->string('dinner_place',100)->nullable();
                $table->string('dinner_time',100)->nullable();
                $table->string('dinner_type_food',100)->nullable();

                $table->integer('malnutrition')->nullable();
                $table->string('malnutrition_spec', 100)->nullable();
                $table->string('idle_hours', 100)->nullable();
                $table->string('active_hours', 100)->nullable();
                $table->integer('age_menarche')->nullable();
                $table->string('material_menstruation',100)->nullable();
                $table->integer('family_involvement')->nullable();
                $table->string('family_involvement_reason', 100)->nullable();
                $table->integer('community_involvement')->nullable();
                $table->string('community_involvement_reason', 100)->nullable();
                $table->integer('drr_knowledge')->nullable();

                $table->text('child_complaint')->nullable();

                $table->softDeletes();
                $table->timestamps();
                $table->unique('healthcareservice_id');
            });
        }        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('healthassessment_service');        //
    }
}
