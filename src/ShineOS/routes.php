<?php

/*Routes for system & warehouse controllers that do not need a logged in session*/
Route::group(['namespace' => '\ShineOS\Controllers'], function () {

    Route::any('default/', 'DefaultController@index');
    Route::any('default/track', 'DefaultController@track');
    Route::any('default/makeHash/{text}', 'DefaultController@makeHash');
    Route::any('install/', 'InstallController@index');

    Route::any('maintenance', 'DefaultController@maintenance');
    Route::any('geojson', 'DefaultController@geojson');
});

Route::group(['namespace' => '\ShineOS\Controllers'], function()
{
    Route::any('message/sendtoday', ['uses' => 'CronController@cronReminders']);
    Route::any('message/testemail', ['uses' => 'DefaultController@testemail']);
    Route::any('message/sms', ['uses' => 'DefaultController@testsms']);
    Route::any('update', ['uses' => 'DefaultController@update']);
});

Route::group(['namespace' => '\ShineOS\Controllers'], function()
{
    Route::any('cron/updateM2', ['uses' => 'WarehouseController@updateM2']);
    Route::any('cron/getDailyM2', ['uses' => 'WarehouseController@getDailyM2']);
    Route::any('cron/getM1_FP', ['uses' => 'WarehouseController@getM1_FP']);
    Route::any('cron/cronReminders', ['uses' => 'CronController@cronReminders']);
});

Route::group(['prefix' => 'plugin', 'namespace' => 'ShineOS\Controllers', 'middleware' => 'auth.access:records'], function()
{
    Route::any('/{plugin_name}/{all}', 'PluginController@index');
    Route::any('/call/{parent}/{plugin_name}/{all}/{ID}', 'PluginController@call');
    Route::any('/saveBlob/{parent}/{plugin_name}/{ID}', 'PluginController@saveBlob');
});
