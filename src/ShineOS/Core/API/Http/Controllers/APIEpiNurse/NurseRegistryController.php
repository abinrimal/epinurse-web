<?php namespace ShineOS\Core\API\Http\Controllers\APIEpiNurse;

use Illuminate\Routing\Controller;
use ShineOS\Core\API\Http\Controllers\APIEpiNurseController;

use Modules\NurseRegistry\Entities\NurseRegistry;

use ShineOS\Core\Users\Entities\Users;
use Illuminate\Http\JsonResponse;
use Shine\Libraries\IdGenerator;


use Request, Auth, Validator;

class NurseRegistryController extends APIEpiNurseController {

	public function insertNurse($request_data) {
		$data = $request_data['data'];
		$data['facility_id'] = $facility_id = $request_data['facility_id'];
		$data['user_id'] = $user_id = $request_data['user_id'];

		$validator = Validator::make($data, [
			"facility_id" => "required|string",
			"user_id" => "required|string",
			"uuid" => "required",
			"nan_number" => "required",
			"first_name" => "required",
			"middle_name" => "required",
			"last_name" => "required",
			"sex" => "required",
			"citizenship_number" => "required",
			"date_of_birth_in_ad" => "required",
			"date_of_birth_in_bs" => "required",
			"age" => "required",
			"temp_house_number" => "required",
			"temp_ward_number" => "required",
			"temp_municipality" => "required",
			"temp_district" => "required",
			"temp_country" => "required",
			"temp_zipcode" => "required",
			"permanent_house_number" => "required",
			"permanent_ward_number" => "required",
			"permanent_municipality" => "required",
			"permanent_district" => "required",
			"permanent_country" => "required",
			"permanent_zipcode" => "required",
			"professional_status" => "required",
			"designation" => "required",
			"office" => "required"
        ]);

	    if($validator->fails() == false)
	    {
	    	$facilityUser = $this->getFacilityUser($data['user_id'],$data['facility_id']);
	    	if(!$facilityUser) {
	    		return new JsonResponse(array('status'=>'error', 'message'=>'Facility not found!'), 400);
	    	}

	    	// Check if existing
	    	$nurse = NurseRegistry::where('uuid',$data['uuid'])->first();

	    	$message = "Nurse has been successfully updated!";
	    	if(!$nurse)
	    	{
	    		$message = "New nurse has been successfully added!";

		    	$nurse = new NurseRegistry;
	    		$nurse->nurse_id = IdGenerator::generateId();
				$nurse->facility_id = $data['facility_id'];
				$nurse->user_id = $data['user_id'];
				$nurse->uuid = isset($data['uuid']) ? $data['uuid'] : NULL;
	    	}

	    	// Proceed to saving
			$nurse->nan_number = isset($data['nan_number']) ? $data['nan_number'] : NULL;
			$nurse->first_name = isset($data['first_name']) ? $data['first_name'] : NULL;
			$nurse->middle_name = isset($data['middle_name']) ? $data['middle_name'] : NULL;
			$nurse->last_name = isset($data['last_name']) ? $data['last_name'] : NULL;
			$nurse->sex = isset($data['sex']) ? $data['sex'] : NULL;
			$nurse->citizenship_number = isset($data['citizenship_number']) ? $data['citizenship_number'] : NULL;
			$nurse->date_of_birth_in_ad = isset($data['date_of_birth_in_ad']) ? $data['date_of_birth_in_ad'] : NULL;
			$nurse->date_of_birth_in_bs = isset($data['date_of_birth_in_bs']) ? $data['date_of_birth_in_bs'] : NULL;
			$nurse->age = isset($data['age']) ? $data['age'] : NULL;
			$nurse->temp_house_number = isset($data['temp_house_number']) ? $data['temp_house_number'] : NULL;
			$nurse->temp_ward_number = isset($data['temp_ward_number']) ? $data['temp_ward_number'] : NULL;
			$nurse->temp_municipality = isset($data['temp_municipality']) ? $data['temp_municipality'] : NULL;
			$nurse->temp_district = isset($data['temp_district']) ? $data['temp_district'] : NULL;
			$nurse->temp_country = isset($data['temp_country']) ? $data['temp_country'] : NULL;
			$nurse->temp_zipcode = isset($data['temp_zipcode']) ? $data['temp_zipcode'] : NULL;
			$nurse->permanent_house_number = isset($data['permanent_house_number']) ? $data['permanent_house_number'] : NULL;
			$nurse->permanent_ward_number = isset($data['permanent_ward_number']) ? $data['permanent_ward_number'] : NULL;
			$nurse->permanent_municipality = isset($data['permanent_municipality']) ? $data['permanent_municipality'] : NULL;
			$nurse->permanent_district = isset($data['permanent_district']) ? $data['permanent_district'] : NULL;
			$nurse->permanent_country = isset($data['permanent_country']) ? $data['permanent_country'] : NULL;
			$nurse->permanent_zipcode = isset($data['permanent_zipcode']) ? $data['permanent_zipcode'] : NULL;
			$nurse->professional_status = isset($data['professional_status']) ? $data['professional_status'] : NULL;
			$nurse->designation = isset($data['designation']) ? $data['designation'] : NULL;
			$nurse->office = isset($data['office']) ? $data['office'] : NULL;

			$nurse->qualifications = isset($data['qualifications']) ? json_encode($data['qualifications']) : NULL;
			$nurse->trainings = isset($data['trainings']) ? json_encode($data['trainings']) : NULL;

			if($nurse->save())
			{
				return new JsonResponse(array('status'=>'success', 'message'=>$message), 200);
			}
			else
			{
				return new JsonResponse(array('status'=>'error', 'message'=>'Erron in saving in nurse registry!'), 400);
			}

	    }
	    else
	    {
	    	// return $this->json_response('error',$validator->errors()->all(),400);
	    	return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
	    }
	}

}