<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableIndexUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string', 'integer', 'datetime');

        Schema::table('patients', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('patients');
            if (! $doctrineTable->hasIndex('patients_patient_id_index')) {
                $table->index(['patient_id']);
            }
        });

        Schema::table('patient_contact', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('patient_contact');
            if (! $doctrineTable->hasIndex('patient_contact_patient_id_index')) {
                $table->index(['patient_id']);
            }
        });

        Schema::table('patient_alert', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('patient_alert');
            if (! $doctrineTable->hasIndex('patient_alert_patient_id_index')) {
                $table->index(['patient_id']);
            }
        });

        Schema::table('allergy_patient', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('allergy_patient');
            if (! $doctrineTable->hasIndex('allergy_patient_allergy_patient_id_index')) {
                $table->index(['allergy_patient_id']);
            } 
        });

        Schema::table('patient_emergencyinfo', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('patient_emergencyinfo');
            if (! $doctrineTable->hasIndex('patient_emergencyinfo_patient_id_index')) {
                $table->index(['patient_id']);
            }
        });

        Schema::table('patient_philhealthinfo', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('patient_philhealthinfo');
            if (! $doctrineTable->hasIndex('patient_philhealthinfo_patient_id_index')) {
                $table->index(['patient_id']);
            }
        });

        Schema::table('patient_death_info', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('patient_death_info');
            if (! $doctrineTable->hasIndex('patient_death_info_patient_id_index')) {
                $table->index(['patient_id']);
            }
        });

        Schema::table('patient_immunization', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('patient_immunization');
            if (! $doctrineTable->hasIndex('patient_immunization_patient_id_index')) {
                $table->index(['patient_id']);
            }
        });

        Schema::table('facility_patient_user', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('facility_patient_user');
            if (! $doctrineTable->hasIndex('facility_patient_user_patient_id_index')) {
                $table->index(['patient_id']);
            }
        });

        Schema::table('patient_medicalhistory', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('patient_medicalhistory');
            if (! $doctrineTable->hasIndex('patient_medicalhistory_patient_id_index')) {
                $table->index(['patient_id']);
            }
        });

        Schema::table('patient_familyinfo', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('patient_familyinfo');
            if (! $doctrineTable->hasIndex('patient_familyinfo_patient_id_index')) {
                $table->index(['patient_id']);
            }
        });

        Schema::table('facility_patient_user', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('facility_patient_user');
            if (! $doctrineTable->hasIndex('facility_patient_user_facilityuser_id_index')) {
                $table->index(['facilityuser_id']);
            } 
        });

        Schema::table('healthcare_services', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('healthcare_services');
            if (! $doctrineTable->hasIndex('healthcare_services_healthcareservice_id_index')) {
                $table->index(['healthcareservice_id']);
            }  
        });

        Schema::table('healthcare_services', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('healthcare_services');
            if (! $doctrineTable->hasIndex('healthcare_services_parent_service_id_index')) {
                $table->index(['parent_service_id']);
            } 
        });

        Schema::table('healthcare_services', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('healthcare_services');
            if (! $doctrineTable->hasIndex('healthcare_services_parent_service_id_index')) {
                $table->index(['seen_by']);
            } 
        });

        Schema::table('disposition', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('disposition');
            if (! $doctrineTable->hasIndex('disposition_healthcareservice_id_index')) {
                $table->index(['healthcareservice_id']);
            }
        });

        Schema::table('disposition', function ($table) { 
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('disposition');
            if (! $doctrineTable->hasIndex('disposition_disposition_id_index')) {
                $table->index(['disposition_id']);
            }
        });

        Schema::table('healthcare_addendum', function ($table) { 
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('healthcare_addendum');
            if (! $doctrineTable->hasIndex('healthcare_addendum_healthcareservice_id_index')) {
                $table->index(['healthcareservice_id']);
            }
        });

        Schema::table('healthcare_addendum', function ($table) { 
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('healthcare_addendum');
            if (! $doctrineTable->hasIndex('healthcare_addendum_addendum_id_index')) {
                $table->index(['addendum_id']);
            }
        });

        Schema::table('general_consultation', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('general_consultation');
            if (! $doctrineTable->hasIndex('general_consultation_healthcareservice_id_index')) {
                $table->index(['healthcareservice_id']);
            }
        });

        Schema::table('general_consultation', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('general_consultation');
            if (! $doctrineTable->hasIndex('general_consultation_generalconsultation_id_index')) {
                $table->index(['generalconsultation_id']);
            } 
        });

        Schema::table('vital_physical', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('vital_physical');
            if (! $doctrineTable->hasIndex('vital_physical_healthcareservice_id_index')) {
                $table->index(['healthcareservice_id']);
            }
        });

        Schema::table('vital_physical', function ($table) { 
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('vital_physical');
            if (! $doctrineTable->hasIndex('vital_physical_vitalphysical_id_index')) {
                $table->index(['vitalphysical_id']);
            } 
        });

        Schema::table('examination', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('examination');
            if (! $doctrineTable->hasIndex('examination_healthcareservice_id_index')) {
                $table->index(['healthcareservice_id']);
            }
        });

        Schema::table('examination', function ($table) { 
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('examination');
            if (! $doctrineTable->hasIndex('examination_examination_id_index')) {
                $table->index(['examination_id']);
            }
        });

        Schema::table('diagnosis', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('diagnosis');
            if (! $doctrineTable->hasIndex('diagnosis_healthcareservice_id_index')) {
                $table->index(['healthcareservice_id']);
            }
        });

        Schema::table('diagnosis', function ($table) { 
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('diagnosis');
            if (! $doctrineTable->hasIndex('diagnosis_diagnosis_id_index')) {
                $table->index(['diagnosis_id']);
            }
        });

        Schema::table('diagnosis_icd10', function ($table) { 
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('diagnosis_icd10');
            if (! $doctrineTable->hasIndex('diagnosis_icd10_diagnosis_id_index')) {
                $table->index(['diagnosis_id']);
            }
        });

        Schema::table('medicalorder', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('diagnosis');
            if (! $doctrineTable->hasIndex('diagnosis_healthcareservice_id_index')) {
                $table->index(['healthcareservice_id']);
            }
        });

        Schema::table('medicalorder', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('medicalorder');
            if (! $doctrineTable->hasIndex('medicalorder_medicalorder_id_index')) {
                $table->index(['medicalorder_id']);
            }
        });

        Schema::table('medicalorder', function ($table) { 
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('medicalorder');
            if (! $doctrineTable->hasIndex('medicalorder_deleted_at_index')) {
                $table->index(['deleted_at']);
            }
        });

        Schema::table('medicalorder_prescription', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('medicalorder_prescription');
            if (! $doctrineTable->hasIndex('medicalorder_prescription_medicalorder_id_index')) {
                $table->index(['medicalorder_id']);
            }
        });

        Schema::table('medicalorder_prescription', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('medicalorder_prescription');
            if (! $doctrineTable->hasIndex('medicalorder_prescription_medicalorderprescription_id_index')) {
                $table->index(['medicalorderprescription_id']);
            } 
        });

        Schema::table('medicalorder_laboratoryexam', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('medicalorder_laboratoryexam');
            if (! $doctrineTable->hasIndex('medicalorder_laboratoryexam_medicalorder_id_index')) {
                $table->index(['medicalorder_id']);
            }
        });

        Schema::table('medicalorder_procedure', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('medicalorder_procedure');
            if (! $doctrineTable->hasIndex('medicalorder_procedure_medicalorder_id_index')) {
                $table->index(['medicalorder_id']);
            }
        });

        Schema::table('facilities', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('facilities');
            if (! $doctrineTable->hasIndex('facilities_facility_id_index')) {
                $table->index(['facility_id']);
            } 
        });

        Schema::table('facilities', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('facilities');
            if (! $doctrineTable->hasIndex('facilities_DOH_facility_code_index')) {
                $table->index(['DOH_facility_code']);
            }  
        });

        Schema::table('facility_contact', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('facility_contact');
            if (! $doctrineTable->hasIndex('facility_contact_facility_id_index')) {
                $table->index(['facility_id']);
            } 
        });

        Schema::table('facility_contact', function ($table) { 
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('facility_contact');
            if (! $doctrineTable->hasIndex('facility_contact_facilitycontact_id_index')) {
                $table->index(['facilitycontact_id']);
            } 
        });

        Schema::table('facility_workforce', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('facility_workforce');
            if (! $doctrineTable->hasIndex('facility_workforce_facility_id_index')) {
                $table->index(['facility_id']);
            }  
        });

        Schema::table('facility_user', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('facility_user');
            if (! $doctrineTable->hasIndex('facility_user_facilityuser_id_index')) {
                $table->index(['facilityuser_id']);
            } 
        });

        Schema::table('facility_user', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('facility_user');
            if (! $doctrineTable->hasIndex('facility_user_user_id_index')) {
                $table->index(['user_id']);
            } 
        });

        Schema::table('facility_user', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('facility_user');
            if (! $doctrineTable->hasIndex('facility_user_facility_id_index')) {
                $table->index(['facility_id']);
            }  
        });

        Schema::table('users', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('users');
            if (! $doctrineTable->hasIndex('users_user_id_index')) {
                $table->index(['user_id']);
            } 
        });

        Schema::table('users', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('users');
            if (! $doctrineTable->hasIndex('users_email_index')) {
                $table->index(['email']);
            } 
        });

        Schema::table('user_md', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('user_md');
            if (! $doctrineTable->hasIndex('user_md_user_id_index')) {
                $table->index(['user_id']);
            } 
        });

        Schema::table('user_contact', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('user_contact');
            if (! $doctrineTable->hasIndex('user_contact_user_id_index')) {
                $table->index(['user_id']);
            } 
        });

        Schema::table('roles', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('roles');
            if (! $doctrineTable->hasIndex('roles_role_id_index')) {
                $table->index(['role_id']);
            } 
        });

        Schema::table('roles_access', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('roles_access');
            if (! $doctrineTable->hasIndex('roles_access_role_id_index')) {
                $table->index(['role_id']);
            } 
        });

        Schema::table('roles_access', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('roles_access');
            if (! $doctrineTable->hasIndex('roles_access_facilityuser_id_index')) {
                $table->index(['facilityuser_id']);
            }  
        });

        Schema::table('lov_diagnosis', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_diagnosis');
            if (! $doctrineTable->hasIndex('lov_diagnosis_diagnosis_id_index')) {
                $table->index(['diagnosis_id']);
            }
        });

        Schema::table('lov_allergy_reaction', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_allergy_reaction');
            if (! $doctrineTable->hasIndex('lov_allergy_reaction_allergyreaction_id_index')) {
                $table->index(['allergyreaction_id']);
            } 
        });

        Schema::table('lov_diseases', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_diseases');
            if (! $doctrineTable->hasIndex('lov_diseases_disease_id_index')) {
                $table->index(['disease_id']);
            } 
        });

        Schema::table('lov_disabilities', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_disabilities');
            if (! $doctrineTable->hasIndex('lov_disabilities_disability_id_index')) {
                $table->index(['disability_id']);
            } 
        });

        Schema::table('lov_immunizations', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_immunizations');
            if (! $doctrineTable->hasIndex('lov_immunizations_immunization_code_index')) {
                $table->index(['immunization_code']);
            } 
        });

        Schema::table('lov_medicalcategory', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_medicalcategory');
            if (! $doctrineTable->hasIndex('lov_medicalcategory_medicalcategory_id_index')) {
                $table->index(['medicalcategory_id']);
            }
        });

        Schema::table('lov_icd10', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_icd10');
            if (! $doctrineTable->hasIndex('lov_icd10_icd10_code_index')) {
                $table->index(['icd10_code']);
            } 
        });

        Schema::table('lov_icd10', function ($table) { 
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_icd10');
            if (! $doctrineTable->hasIndex('lov_icd10_icd10_category_index')) {
                $table->index(['icd10_category']);
            }
        });

        Schema::table('lov_icd10', function ($table) { 
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_icd10');
            if (! $doctrineTable->hasIndex('lov_icd10_icd10_subcategory_index')) {
                $table->index(['icd10_subcategory']);
            }
        });

        Schema::table('lov_icd10', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_icd10');
            if (! $doctrineTable->hasIndex('lov_icd10_icd10_tricategory_index')) {
                $table->index(['icd10_tricategory']);
            } 
        });

        Schema::table('lov_drugs', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_drugs');
            if (! $doctrineTable->hasIndex('lov_drugs_drugs_id_index')) {
                $table->index(['drugs_id']);
            } 
        });

        Schema::table('lov_barangays', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_barangays');
            if (! $doctrineTable->hasIndex('lov_barangays_barangay_code_index')) {
                $table->index(['barangay_code']);
            }  
        });

        Schema::table('lov_region', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_region');
            if (! $doctrineTable->hasIndex('lov_region_region_code_index')) {
                $table->index(['region_code']);
            }
        });

        Schema::table('lov_province', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_province');
            if (! $doctrineTable->hasIndex('lov_province_province_code_index')) {
                $table->index(['province_code']);
            } 
        });

        Schema::table('lov_citymunicipalities', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_citymunicipalities');
            if (! $doctrineTable->hasIndex('lov_citymunicipalities_city_code_index')) {
                $table->index(['city_code']);
            }  
        });

        Schema::table('lov_medicalprocedures', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_medicalprocedures');
            if (! $doctrineTable->hasIndex('lov_medicalprocedures_procedure_code_index')) {
                $table->index(['procedure_code']);
            } 
        });

        Schema::table('lov_loincs', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_loincs');
            if (! $doctrineTable->hasIndex('lov_loincs_loinc_code_index')) {
                $table->index(['loinc_code']);
            } 
        });

        Schema::table('lov_laboratories', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_laboratories');
            if (! $doctrineTable->hasIndex('lov_laboratories_laboratorycode_index')) {
                $table->index(['laboratorycode']);
            } 
        });

        Schema::table('lov_doh_facility_codes', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_doh_facility_codes');
            if (! $doctrineTable->hasIndex('lov_doh_facility_codes_code_index')) {
                $table->index(['code']);
            } 
        });

        Schema::table('lov_enumerations', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_enumerations');
            if (! $doctrineTable->hasIndex('lov_enumerations_code_index')) {
                $table->index(['code']);
            } 
        });

        Schema::table('lov_modules', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_modules');
            if (! $doctrineTable->hasIndex('lov_modules_module_name_index')) {
                $table->index(['module_name']);
            } 
        });

        Schema::table('lov_forms', function ($table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('lov_forms');
            if (! $doctrineTable->hasIndex('lov_forms_form_name_index')) {
                $table->index(['form_name']);
            } 
        });

        // Schema::table('lov_forms', function ($table) {
        //     $conn = Schema::getConnection();
        //     $dbSchemaManager = $conn->getDoctrineSchemaManager();
        //     $doctrineTable = $dbSchemaManager->listTableDetails('lov_forms');
        //     if (! $doctrineTable->hasIndex('lov_forms_facility_id_index')) {
        //         $table->index(['facility_id']);
        //     } 
        // });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
