<?php

if(isset($patient)) {
    $patient_age = getAge($patient->birthdate);
}

$region = NULL;
$province = NULL;
$city = NULL;
$barangay = NULL;

$intent = "hidden";
if(isset($philhealth)) { $intent = "update"; }
else { $intent = "save"; }

$hidden = "hidden";
$philhealth_member_type = NULL;
if(isset($philhealth->member_type)) {
    $philhealth_member_type = $philhealth->member_type;
    if($philhealth->member_type != 'DD') {
        $hidden = "hidden";
    }
    else{
        $hidden = "";
    }
}
else {
    $hidden = "hidden";
}
foreach($plugs as $plug) {
    if($plug['plugin'] == 'Philhealth') {
        $pfolder = $plug['folder'];
    }
}

$philhealth_philhealth_category = NULL;
$philhealth_benefactor_relation = NULL;
if(isset($philhealth->philhealth_category)) {
    $philhealth_philhealth_category = $philhealth->philhealth_category;
}
if(isset($philhealth->benefactor_relation)) {
    $$philhealth_benefactor_relation = $philhealth->benefactor_relation;
}

?>

<div class="tab-pane step" id="philhealth">
    {!! Form::hidden('intent', $intent) !!}
    @if($patient)
    {!! Form::hidden('patient_data', $patient) !!}
    {!! Form::hidden('patient_id', $patient->patient_id) !!}
    @endif
    <fieldset>
        <div class="icheck" >
            
            <legend>PhilHealth Membership Data</legend>
            <label class="col-md-3 control-label">PhilHealth Member ID</label>
            <div class="col-md-3">
                <input type="text" id="phicpin" class="form-control alphanum phicpin" name="philhealth[MEMID_NO]" placeholder="PHIC ID" value=<?php if(isset($philhealth->philhealth_id)) {echo $philhealth->philhealth_id;} ?> >
            </div>
            @if( (Config::get('config.mode') == 'cloud' OR Config::get('config.mode') == 'training') AND isset($patient) AND (!$philhealth OR $philhealth->philhealth_id == '') )
            <div class="col-md-3">
                <a class="btn btn-primary btn-arrow-left" id="submitgetPhicPatientId">Check for Philhealth PIN</a>
            </div>
            @endif
            <br clear="all">
            <label class="col-md-3 control-label">For Enlistment</label>
            <div class="col-md-3">
                <div class="radio-inline">
                    <label>
                        <input name="philhealth[For_Enlistment]" type="radio" value="1" <?php if(isset($philhealth->For_Enlistment) && $philhealth->For_Enlistment == "1" ) {echo "checked";} ?> > Yes
                    </label>
                </div>
            </div>
            <br clear="all">

            <label class="col-md-3 control-label">PhilHealth Member Type</label>
            <div class="col-md-3">
                {!! Form::select('philhealth[Pat_PHIC_Membership_Type]',
                            array(NULL => 'Select membership type',
                            'MM' => 'Member',
                            'DD' => 'Dependent',
                            'NM' => 'Non Member'),
                            $philhealth_member_type, ['class' => 'form-control', 'id'=>'Pat_PHIC_Membership_Type']) !!}
            </div>

            <label class="col-md-3 control-label">PhilHealth Member Category</label>
            <div class="col-md-3">
                {!! Form::select('philhealth[MEM_CATEGORY]', $ref_mem_cat, $philhealth_philhealth_category, ['class' => 'form-control', 'id'=>'MEM_CATEGORY']) !!}
            </div>

            <label class="col-md-3 control-label">Benefit Package</label>
            <div class="col-md-3">
                <select class="form-control" name="philhealth[Pat_Benefit_Package]" id="Pat_Benefit_Package">
                    <option value="">-- Select a type --</option>
                    <option value="TSeKaP" <?php if(isset($philhealth->benefit_type) && $philhealth->benefit_type == "TSeKaP" ) {echo "selected";} ?> >TSeKap</option>
                </select>
            </div>

            <label class="col-md-3 control-label">Pamilya Pantawid ID</label>
            <div class="col-md-3">
                <input type="text" class="form-control" name="philhealth[Pat_Pantawid_Pamilya_Member]" placeholder="Pantawid Pamilya Member ID" value=<?php if(isset($philhealth->pamilya_pantawid_id)) {echo $philhealth->pamilya_pantawid_id;} ?> >
            </div>

            <label class="col-md-3 control-label">Indigenous Group</label>
            <div class="col-md-3">
                <div class="radio-inline">
                    <label>
                        <input name="philhealth[IndigenousGroup]" type="radio" value="1" <?php if(isset($philhealth->indigenous_group) && $philhealth->indigenous_group == "1" ) {echo "checked";} ?> > Yes
                    </label>
                </div>
                <div class="radio-inline">
                    <label>
                        <input name="philhealth[IndigenousGroup]" type="radio" value="0" <?php if(isset($philhealth->indigenous_group) && $philhealth->indigenous_group == "0" ) {echo "checked";} ?> > No
                    </label>
                </div>
            </div>

            <label class="col-md-3 control-label">Conditional Cash Transfer</label>
            <div class="col-md-3">
                <div class="radio-inline">
                    <label>
                        <input name="philhealth[CCT]" type="radio" value="Y" <?php if(isset($philhealth->ccash_transfer) && $philhealth->ccash_transfer == "Y" ) {echo "checked";} ?> > Yes
                    </label>
                </div>
                <div class="radio-inline">
                    <label>
                        <input name="philhealth[CCT]" type="radio" value="N" <?php if(isset($philhealth->ccash_transfer) && $philhealth->ccash_transfer == "N" ) {echo "checked";} ?> > No
                    </label>
                </div>
            </div>
        </div>
    </fieldset>

    <br clear="all">

    <div class="{{ $hidden }}" id="benefactorform">
        <fieldset>
            <legend>PhilHealth Benefactor Data</legend>
            <label class="col-md-2 control-label">Member ID</label>
            <div class="col-md-4 form-group">
                <input type="text" class="form-control phicpin" name="philhealth[WorMemid]" placeholder="" value=<?php if(isset($philhealth->benefactor_member_id)) {echo $philhealth->benefactor_member_id;}  ?> >
            </div>
            <label class="col-md-2 control-label">Relationship</label>
            <div class="col-md-4 form-group">
                {!! Form::select('philhealth[relation]', $getArrRelationship, $philhealth_benefactor_relation, ['class' => 'form-control', 'id'=>'relationship']) !!}
            </div>
            <br clear="all">

            <label class="col-md-2 control-label">First Name</label>
            <div class="col-md-4 form-group">
                <input type="text" class="form-control firstname" name="philhealth[WorFName]" placeholder="" value=<?php if(isset($philhealth->benefactor_first_name)) {echo $philhealth->benefactor_first_name;} ?> >
            </div>

            <label class="col-md-2 control-label">Last Name</label>
            <div class="col-md-4 form-group">
                <input type="text" class="form-control lastname" name="philhealth[WorLName]" placeholder="" value=<?php if(isset($philhealth->benefactor_last_name)) {echo $philhealth->benefactor_last_name;} ?> >
            </div>

            <label class="col-md-2 control-label">Middle Name</label>
            <div class="col-md-4 form-group">
                <input type="text" class="form-control midname" name="philhealth[WorMName]" placeholder="" value=<?php if(isset($philhealth->benefactor_middle_name)) {echo $philhealth->benefactor_middle_name;} ?> >
            </div>

            <div class="form-group col-md-6">
                <div class="row">
                    <label class="col-sm-4 control-label">Suffix</label>
                    <div class="col-sm-8">
                        <div class="btn-group toggler" data-toggle="buttons">
                          <label class="btn btn-default @if(isset($philhealth->benefactor_name_suffix) && $philhealth->benefactor_name_suffix == 'SR') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="philhealth[WorExtName]" id="" autocomplete="off" value="SR"> Sr.
                          </label>
                          <label class="btn btn-default @if(isset($philhealth->benefactor_name_suffix) && $philhealth->benefactor_name_suffix == 'JR') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="philhealth[WorExtName]" id="" autocomplete="off" value="JR"> Jr.
                          </label>
                          <label class="btn btn-default @if(isset($philhealth->benefactor_name_suffix) && $philhealth->benefactor_name_suffix == 'II') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="philhealth[WorExtName]" id="" autocomplete="off" value="II"> II
                          </label>
                          <label class="btn btn-default @if(isset($philhealth->benefactor_name_suffix) && $philhealth->benefactor_name_suffix == 'III') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="philhealth[WorExtName]" id="" autocomplete="off" value="III"> III
                          </label>
                          <label class="btn btn-default @if(isset($philhealth->benefactor_name_suffix) && $philhealth->benefactor_name_suffix == 'IV') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="philhealth[WorExtName]" id="" autocomplete="off" value="IV"> IV
                          </label>
                          <label class="btn btn-default @if(isset($philhealth->benefactor_name_suffix) && $philhealth->benefactor_name_suffix == 'V') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="philhealth[WorExtName]" id="" autocomplete="off" value="V"> V
                          </label>
                          <label class="btn btn-default">
                            <i class="fa fa-check"></i> <input type="radio" name="philhealth[WorExtName]" id="" autocomplete="off" value="NA"> None
                          </label>
                        </div>
                    </div>
                </div>
            </div>

        </fieldset>
    </div>

    @if($patient)
        <?php
            //let us check if there is already an PMRF application
            $pmrf = DB::table('philhealth_pmrf')->where('patient_id', $patient->patient_id)->first();
        ?>
        @if( (!$philhealth OR ($philhealth AND $philhealth->philhealth_id == "")) AND $pmrf == NULL)
        <!-- Let us not show the PHIC Application form if patient is already a member with a PIN -->
        <fieldset id="applicationLink">
            <legend>PhilHealth Membership Application</legend>
            <label class="col-md-3 control-label">Apply or Update PhilHealth Membership</label>
            <div class="col-md-3">
                <!-- Trigger the modal with a button -->

                <button class="btn btn-danger btn-md" href="{{ url('plugin/call/Philhealth/Philhealth/viewPMRF/'.$patient->patient_id) }}" data-toggle="modal" data-target="#PMRFModal">Fill-up Application PMRF</button>
            </div>
            <div class="col-md-5">
                <p>If patient is not yet a PhilHealth Member or requires an update on the membership, you can fill-up the membership application form and SHINEOS+ will submit it to PHIE for submission to PhilHealth. Click the Fill-up button to show the form now.</p>
            </div>
        </fieldset>
        @elseif( (!$philhealth OR ($philhealth AND $philhealth->philhealth_id == "")) AND $pmrf != NULL)
        <fieldset>
            <legend>PhilHealth Membership Application</legend>
            <div class="alert alert-info">
            <h4>Submitted and pending</h4>
            <p>This patient has a pending PhilHealth application submitted to PHIE. Please try to check the PIN using the Check for PhilHealth PIN above, the application is already processed.</p>
            </div>
        </fieldset>
        @else
        @endif
    @else
        <fieldset>
            <legend>PhilHealth Membership Application</legend>
            <p>If this patient is not yet a PhilHealth member, you can submit a PhilHealth Application Form (PMRF) after saving this record.</p>
        </fieldset>
    @endif
    <br clear="all" />

</div>


@section('plugin_jsscripts')
<div class="modal fade" id="PMRFModal" tabindex="-1" role="dialog" aria-labelledby="PMRFLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>

{!! HTML::script('/plugins/Philhealth/Assets/js/philhealth.js'); !!}

@if(isset($patient))
<script type="text/javascript">
$(function(){
    $('#submitgetPhicPatientId').on('click', function(e){

        $('#submitgetPhicPatientId').html("<i class='fa fa-spinner fa-pulse fa-fw'></i> Checking for Philhealth PIN");
        e.preventDefault();

        $.ajax({
            url: "{{ url('plugin/call/Philhealth/Philhealth/PHIEconnect_get_phicPatientID/'.$patient->patient_id) }}",
            type: "POST",
            data: {
                '_token' : $('input[name=_token]').val(),
                patient : $('input[name=patient_data]').val(),
            },
            error: function(data) {
                console.log(data);
            },
            success: function(data) {
                console.log(data);
                /*if(data.Client_PhilHealth_No){
                    var obj =  JSON.parse(data);
                    alert('Patient is already a PhilHealth Member with PHICPIN: '+ obj['Client_PhilHealth_No']);
                    $('#phicpin').val(obj);
                    $('#submitgetPhicPatientId').remove();
                } else if(data == "Phil Health Number Doesn't Exists!") {
                    alert('Patient is not yet a PhilHealth Member.');
                    $('#submitgetPhicPatientId').html("Check for Philhealth PIN");
                } else {
                    alert('Service Error: '+ data);
                    $('#submitgetPhicPatientId').html("Check for Philhealth PIN");
                }*/
                if(data == "Phil Health Number Doesn't Exists!") {
                    alert('Patient is not yet a PhilHealth Member.');
                    $('#submitgetPhicPatientId').html("Check for Philhealth PIN");
                } else if(data.Client_PhilHealth_No) {
                    alert('Patient is already a PhilHealth Member with PHICPIN: '+ data.Client_PhilHealth_No);
                    $('#phicpin').val(data.Client_PhilHealth_No);
                    $('#submitgetPhicPatientId').remove();
                } else {
                    alert('Service Error: '+ data);
                    $('#submitgetPhicPatientId').html("Check for Philhealth PIN");
                }
            }
        });
    });

    $("#myInfoModal").on("show.bs.modal", function(e) {
        $(this).find(".modal-content").html("");
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
});

</script>

{!! HTML::script('/plugins/MedMomPatientHistory/medmompatienthistory.js'); !!}

@endif
@stop
