<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class lov_modules extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('lov_modules')->insert(array(
          array('id' => '1','module_name' => 'dashboard','menu_title' => NULL,'icon' => 'fa-dashboard','menu_show' => '1','menu_order' => '0','status' => '1','deleted_at' => '2016-04-04 02:28:48','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '2','module_name' => 'facilities','menu_title' => NULL,'icon' => 'fa-hospital-o','menu_show' => '1','menu_order' => '10002','status' => '1','deleted_at' => '2016-10-18 09:33:48','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '3','module_name' => 'healthcareservices','menu_title' => NULL,'icon' => '','menu_show' => '0','menu_order' => '0','status' => '1','deleted_at' => '2016-04-04 02:28:48','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '4','module_name' => 'patients','menu_title' => NULL,'icon' => '','menu_show' => '0','menu_order' => '0','status' => '1','deleted_at' => '2016-04-04 02:28:48','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '5','module_name' => 'records','menu_title' => NULL,'icon' => 'fa-archive','menu_show' => '1','menu_order' => '1','status' => '1','deleted_at' => '2016-04-04 02:28:48','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '6','module_name' => 'referrals','menu_title' => NULL,'icon' => 'fa-paper-plane-o','menu_show' => '1','menu_order' => '2','status' => '1','deleted_at' => '2016-04-04 02:28:48','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '7','module_name' => 'reminders','menu_title' => NULL,'icon' => 'fa-calendar-check-o','menu_show' => '1','menu_order' => '3','status' => '1','deleted_at' => '2016-04-04 02:28:48','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '8','module_name' => 'reports','menu_title' => NULL,'icon' => 'fa-clipboard','menu_show' => '1','menu_order' => '4','status' => '1','deleted_at' => '2016-04-04 02:28:48','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '9','module_name' => 'roles','menu_title' => NULL,'icon' => 'fa-user-md','menu_show' => '0','menu_order' => '0','status' => '0','deleted_at' => '2016-04-04 02:28:48','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '10','module_name' => 'users','menu_title' => NULL,'icon' => 'fa-group','menu_show' => '1','menu_order' => '10001','status' => '1','deleted_at' => '2016-10-18 09:33:51','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '11','module_name' => 'sync','menu_title' => NULL,'icon' => 'fa-gears','menu_show' => '1','menu_order' => '10004','status' => '1','deleted_at' => '2016-10-18 09:33:56','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '12','module_name' => 'calendar','menu_title' => NULL,'icon' => 'fa-calendar','menu_show' => '1','menu_order' => '11','status' => '1','deleted_at' => '2016-04-04 02:28:48','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '13','module_name' => 'extensions','menu_title' => NULL,'icon' => 'fa-plug','menu_show' => '1','menu_order' => '10003','status' => '1','deleted_at' => '2016-10-18 09:33:58','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '14','module_name' => 'updates','menu_title' => NULL,'icon' => 'fa-cloud-download','menu_show' => '1','menu_order' => '10005','status' => '1','deleted_at' => '2016-10-18 09:34:01','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00'),
          array('id' => '15','module_name' => 'phie','menu_title' => 'PHIE LITE','icon' => 'fa-database','menu_show' => '1','menu_order' => '10000','status' => '1','deleted_at' => '2016-10-18 10:39:21','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00')
        ));

        Model::reguard();
    }
}

