<?php namespace Modules\Nurseregistry\Entities;
   
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class NurseRegistry extends Model {

    use SoftDeletes;

    protected $table = 'nurse_registry';
    protected static $table_name = 'nurse_registry';
    protected $primaryKey = 'nurse_id';

}