<?php

namespace Plugins\Philhealth;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Shine\Libraries\IdGenerator;
use DB;
use Input;

class PhilhealthModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'patient_philhealthinfo';
    protected static $table_name = 'patient_philhealthinfo';
    protected $primaryKey = 'patient_id';
    protected $touches = array('patients');

    public function patients()
    {
        return $this->belongsTo('ShineOS\Core\Patients\Entities\Patients');
    }

    public static function saveplugin( $data, $id=NULL )
    {
        $phic = $data;
        if($id) {
            $patient_id = $id;
        } else {
            $patient_id = Input::get('patient_id');
        }
        $intent = Input::get('intent');

        if($intent == "save")
        {
            $philhealth = new PhilhealthModel;
            $philhealth->patient_philhealthinfo_id = IdGenerator::generateId();
        }
        else
        {
            $philhealth = self::where('patient_id','=',$patient_id)->first();
        }
        $philhealth->patient_id = $patient_id;
        $philhealth->philhealth_id = $phic['MEMID_NO'];
        $philhealth->philhealth_category = $phic['MEM_CATEGORY'];
        $philhealth->member_type = $phic['Pat_PHIC_Membership_Type'];
        if(isset($phic['CCT']))
        {
            $philhealth->ccash_transfer = $phic['CCT'];
        }
        $philhealth->benefit_type = $phic['Pat_Benefit_Package'];
        $philhealth->pamilya_pantawid_id = $phic['Pat_Pantawid_Pamilya_Member'];
        if(isset($phic['IndigenousGroup']))
        {
            $philhealth->indigenous_group = $phic['IndigenousGroup'];
        }
        $philhealth->benefactor_member_id = $phic['WorMemid'];
        $philhealth->benefactor_first_name = $phic['WorFName'];
        $philhealth->benefactor_last_name = $phic['WorLName'];
        $philhealth->benefactor_middle_name = $phic['WorMName'];
        if(isset($phic['WorExtName'])) {
            $philhealth->benefactor_name_suffix = $phic['WorExtName'];
        } else {
            $philhealth->benefactor_name_suffix = NULL;
        }
        $philhealth->benefactor_relation = $phic['relation'];

        if(isset($phic['For_Enlistment'])) {
            $philhealth->For_Enlistment = $phic['For_Enlistment'];
        } else {
            $philhealth->For_Enlistment = '0';
        }

        $res = $philhealth->save();

        //header('Location: '.site_url().'plugin/call/Philhealth/Philhealth/view/'.$patient_id);
        //exit;
    }
}
