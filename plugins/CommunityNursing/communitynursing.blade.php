<?php

$hservice_id = $allData['healthcareserviceid'];
$cn_id = $data['communitynursing_id'];

$fp_awareness_where_ecp = array(
	0 => 'Mobile clinic',
	1 => 'Health facility',
	2 => 'Others'
);

// $handwash = array(
// 	0 => 'After toileting',
// 	1 => 'Before cooking',
// 	2 => 'Before eating',
// 	3 => 'Before feeding a child',
// 	4 => 'Others'
// );

$src_water_supply = array(
	0 => 'Tap water',
	1 => 'Tubewell/hand pump',
	2 => 'Stream water',
	3 => 'Ground water',
	4 => 'Others'
);

$distance_water_supply = array(
	0 => 'Within home/shelter',
	1 => '< 30 minutes',
	2 => '> 30 minutes'
);

$type_toilet = array(
	0 => 'Bore hole',
	1 => 'Dugwell',
	2 => 'Water seal',
	3 => 'Others',
);

$toilet_distance = array(
	0 => 'Within home/shelter',
	1 => 'Nearby shelter < 5 minutes',
	2 => 'Far from shelter > 5 minutes'
);

$waste_disposal = array(
	0 => 'Composting',
	1 => 'Burning',
	2 => 'Dumping',
	3 => 'Other'
);

$housing = array(
	0 => 'Tent',
	1 => 'Shifted house',
	2 => 'Government buildings',
	3 => 'Own house',
	4 => 'Others',
);

// $light_source = array(
// 	0 => 'Electricity',
// 	1 => 'Oil lamp',
// 	2 => 'Solar',
// 	3 => 'Others'
// );

// $cooking_fuel = array(
// 	0 => 'Gas',
// 	1 => 'Fire wood',
// 	2 => 'Kerosene',
// 	3 => 'Others'
// );

// $food_supply_type = array(
// 	0 => 'Cooked',
// 	1 => 'Junk',
// 	2 => 'Others'
// );

$near_faci_type = array(
	0 => 'Health post',
	1 => 'Primary health center',
	2 => 'District hospital',
	3 => 'Zonal hospital',
	4 => 'Sub regional hospital',
	5 => 'Tertiary hospital',
	6 => 'Private hospital'
);

?>

<style type="text/css">
	img.epinurse-icon {
		height: 50px;
	}
</style>

{!! Form::hidden('communitynursing[cn_id]',$cn_id) !!}
{!! Form::hidden('communitynursing[hservice_id]',$hservice_id) !!}

<fieldset>
	<legend>Community Nursing</legend>

	<ul class="nav nav-tabs">
	  <li class="active"><a data-toggle="tab" href="#disaster">Disaster Information</a></li>
	  <li><a data-toggle="tab" href="#housing">Housing/Shelter</a></li>
	  <li><a data-toggle="tab" href="#hygiene">Personal Hygiene and Environmental Sanitation</a></li>
	  <li><a data-toggle="tab" href="#health">Health Condition</a></li>
	  <li><a data-toggle="tab" href="#familyplanning">Family Planning</a></li>
	  <li><a data-toggle="tab" href="#mental">Mental Illness</a></li>
	</ul>

	<div class="tab-content">
	  <div id="disaster" class="tab-pane fade in active">
	  	<div class="page-header">
	  		 Disaster Information
	  	</div>
	  	<div class="radio col-sm-12">
			<label class="col-sm-4 control-label"> Any disaster occurred recently? </label>
			<div class="col-sm-3">
				<div class="icheck">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[disaster_recently]',0,(isset($data['disaster_recently']) && $data['disaster_recently'] == 0) ? true : false,['id'=>'disaster_recently_no', 'class'=>'disaster_recently_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[disaster_recently]',1,(isset($data['disaster_recently']) && $data['disaster_recently'] == 1) ? true : false,['id'=>'disaster_recently_yes', 'class'=>'disaster_recently_radio']) !!} Yes
					</label>
				</div>
			</div>
	  	</div>
	  	<div id="yes_disaster_recently" class="<?php if (isset($data['disaster_recently']) && $data['disaster_recently'] == 1) {echo '';} else {echo 'hidden';} ?>">
	 		<div class="col-sm-12">
	 			<div class="col-sm-12">
		 			<label class="col-sm-4 control-label"> Type of disaster (if any) </label>
		 			<div class="col-sm-3">
		 				{!! Form::text('communitynursing[disaster_type]',(isset($data['disaster_type'])) ? $data['disaster_type'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
	 		</div>
	 		<div class="col-sm-12">
	 			<div class="col-sm-12">
		 			<label class="col-sm-4 control-label"> Duration of disaster </label>
		 			<div class="col-sm-3">
		 				{!! Form::text('communitynursing[disaster_duration]',(isset($data['disaster_duration'])) ? $data['disaster_duration'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
	 		</div>
	 		<div class="col-sm-12">
	 			<div class="col-sm-12">
		 			<label class="col-sm-4 control-label"> Number of human loss </label>
		 			<div class="col-sm-3">
		 				{!! Form::number('communitynursing[number_human_loss]',(isset($data['number_human_loss'])) ? $data['number_human_loss'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
	 			<div class="col-sm-12">
		 			<label class="col-sm-4 control-label"> Number of deaths </label>
		 			<div class="col-sm-3">
		 				{!! Form::number('communitynursing[number_death]',(isset($data['number_death'])) ? $data['number_death'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
	 			<div class="col-sm-12">
		 			<label class="col-sm-4 control-label"> Number of missing people </label>
		 			<div class="col-sm-3">
		 				{!! Form::number('communitynursing[number_missing_people]',(isset($data['number_missing_people'])) ? $data['number_missing_people'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
	 			<div class="col-sm-12">
		 			<label class="col-sm-4 control-label"> Number of injured people </label>
		 			<div class="col-sm-3">
		 				{!! Form::number('communitynursing[number_injured_people]',(isset($data['number_injured_people'])) ? $data['number_injured_people'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
	 			<div class="col-sm-12">
		 			<label class="col-sm-4 control-label"> Number of livestock loss </label>
		 			<div class="col-sm-3">
		 				{!! Form::number('communitynursing[number_livestock_loss]',(isset($data['number_livestock_loss'])) ? $data['number_livestock_loss'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
	 		</div>
 		</div>
	  </div>
	  <div id="hygiene" class="tab-pane fade in">
	  	<div class="page-header">
	  		 Personal Hygiene and Environmental Sanitation
	  	</div>
	  	<div class="icheck">
			<div class="col-sm-12">
				<label class="col-sm-4 control-label">Handwash</label>
				<div class="col-sm-8">
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[when_wash_hands_after_toileting]',1,(isset($data['when_wash_hands_after_toileting']) && $data['when_wash_hands_after_toileting'] == 1) ? true : false) !!} After toileting
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[when_wash_hands_before_cooking]',1,(isset($data['when_wash_hands_before_cooking']) && $data['when_wash_hands_before_cooking'] == 1) ? true : false) !!} Before cooking
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[when_wash_hands_before_eating]',1,(isset($data['when_wash_hands_before_eating']) && $data['when_wash_hands_before_eating'] == 1) ? true : false) !!} Before eating
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[when_wash_hands_before_feeding_a_child]',1,(isset($data['when_wash_hands_before_feeding_a_child']) && $data['when_wash_hands_before_feeding_a_child'] == 1) ? true : false) !!} Before feeding a child
					</label>
					<label class="checkbox" for="when_wash_hands_other">
						<div class="form-inline">
				        {!! Form::checkbox('communitynursing[when_wash_hands_others]',1,(isset($data['when_wash_hands_others']) && $data['when_wash_hands_others'] == 1) ? true : false) !!} Others
				        {!! Form::text('communitynursing[when_wash_hands_specify]',isset($data['when_wash_hands_specify']) ? $data['when_wash_hands_specify'] : NULL, ['class'=>'form-control','id'=>'when_wash_hands_specify']) !!} 
			    		</div>
				    </label>
				</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Bathing habit (how many times in a week)</label>
		  		<div class="col-sm-3">
		  			<div class="input-group">
		  				{!! Form::number('communitynursing[bathing_habit]',isset($data['bathing_habit']) ? $data['bathing_habit'] : NULL, ['class'=>'form-control']) !!}
		  				<span class="input-group-addon"> times </span>
		  			</div>
		  		</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Source of water supply</label>
		  		<div class="col-sm-3">
		  			{!! Form::select('communitynursing[src_water_supply]', $src_water_supply, isset($data['src_water_supply']) ? $data['src_water_supply'] : NULL, ['class'=>'form-control','id'=>'src_water_supply_select','placeholder'=>'Please select...']) !!}
		  		</div>
		  		<div id="src_water_supply_oth_div" class="<?php if (isset($data['src_water_supply']) && $data['src_water_supply'] == 4) {echo '';} else {echo 'hidden';} ?>">
			  		<label class="col-sm-2 control-label"> Specify others </label>
			  		<div class="col-sm-3">
			  			{!! Form::text('communitynursing[src_water_supply_spec]',isset($data['src_water_supply_spec']) ? $data['src_water_supply_spec'] : NULL, ['class'=>'form-control']) !!}
			  		</div>
			  	</div>
		  	</div>
		  	<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Adequacy of water supply </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[ade_water_supply]',0,(isset($data['ade_water_supply']) && $data['ade_water_supply'] == 0) ? true : false,['id'=>'ade_water_supply_no', 'class'=>'ade_water_supply_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[ade_water_supply]',1,(isset($data['ade_water_supply']) && $data['ade_water_supply'] == 1) ? true : false,['id'=>'ade_water_supply_yes', 'class'=>'ade_water_supply_radio']) !!} Yes
					</label>
				</div>
		  	</div>
		  	<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Quality of water assured </label>
				<div class="col-sm-4">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[quality_water]',0,(isset($data['quality_water']) && $data['quality_water'] == 0) ? true : false,['id'=>'quality_water_no', 'class'=>'quality_water_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[quality_water]',1,(isset($data['quality_water']) && $data['quality_water'] == 1) ? true : false,['id'=>'quality_water_yes', 'class'=>'quality_water_radio']) !!} Yes
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[quality_water]',1,(isset($data['quality_water']) && $data['quality_water'] == 2) ? true : false,['id'=>'quality_water_unknown', 'class'=>'quality_water_radio']) !!} Unknown
					</label>
				</div>
		  	</div>
			<div class="col-sm-12">
				<label class="col-sm-4 control-label"> Methods of water purification? Supplementary? </label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[waterpurif_boiling]',1, (isset($data['waterpurif_boiling']) && $data['waterpurif_boiling'] == 1) ? true : false) !!} Boiling
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[waterpurif_filtration]',1, (isset($data['waterpurif_filtration']) && $data['waterpurif_filtration'] == 1) ? true : false) !!} Filtration
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[waterpurif_chemical]',1, (isset($data['waterpurif_chemical']) && $data['waterpurif_chemical'] == 1) ? true : false) !!} Using chemical
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[waterpurif_sodis]',1, (isset($data['waterpurif_sodis']) && $data['waterpurif_sodis'] == 1) ? true : false) !!} Sodis
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[waterpurif_none]',1, (isset($data['waterpurif_none']) && $data['waterpurif_none'] == 1) ? true : false) !!} None
					</label>
				    <label class="checkbox" for="water_purification_other">
				        {!! Form::checkbox('communitynursing[waterpurif_oth]',1, (isset($data['waterpurif_oth']) && $data['waterpurif_oth'] == 1) ? true : false, ['id'=>'water_purification_other']) !!} Other
				        {!! Form::text('communitynursing[waterpurif_oth_spec]',isset($data['waterpurif_oth_spec']) ? $data['waterpurif_oth_spec'] : NULL, ['class'=>'form-control','id'=>'waterpurif_oth_spec']) !!} 
				    </label>
				</div>
	  		</div>
			<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Distance of water supply from shelter? </label>
		  		<div class="col-sm-3">
		  			{!! Form::select('communitynursing[distance_water_supply]', $distance_water_supply, isset($data['distance_water_supply']) ? $data['distance_water_supply'] : NULL, ['class'=>'form-control','id'=>'distance_water_supply_select','placeholder'=>'Please select...']) !!}
		  		</div>
		  	</div>
			<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Toilet available? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[toilet_available]',0,(isset($data['toilet_available']) && $data['toilet_available'] == 0) ? true : false,['id'=>'toilet_available_no', 'class'=>'toilet_available_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[toilet_available]',1,(isset($data['toilet_available']) && $data['toilet_available'] == 1) ? true : false,['id'=>'toilet_available_yes', 'class'=>'toilet_available_radio']) !!} Yes
					</label>
				</div>
				<div class="<?php if(!isset($data['toilet_available']) || (isset($data['toilet_available']) && $data['toilet_available'] != 1)) { echo 'hidden'; } ?>" id="num_toilet_spec">
					<label class="col-sm-4 control-label"> Is the number of toilet adequate to the ratio of population? </label>
					<div class="col-sm-2">
						<label class="radio-inline">
							{!! Form::radio('communitynursing[num_toilet]',0,(isset($data['num_toilet']) && $data['num_toilet'] == 0) ? true : false,['id'=>'num_toilet_no', 'class'=>'num_toilet_radio']) !!} No
						</label>
						<label class="radio-inline">
							{!! Form::radio('communitynursing[num_toilet]',1,(isset($data['num_toilet']) && $data['num_toilet'] == 1) ? true : false,['id'=>'num_toilet_yes', 'class'=>'num_toilet_radio']) !!} Yes
						</label>
					</div>
	 			</div>
	  		</div>
			<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Type of toilet available? </label>
		  		<div class="col-sm-3">
		  			{!! Form::select('communitynursing[type_toilet]', $type_toilet, isset($data['type_toilet']) ? $data['type_toilet'] : NULL, ['class'=>'form-control','id'=>'type_toilet_select','placeholder'=>'Please select...']) !!}
		  		</div>
		  		<div id="type_toilet_oth_div" class="<?php if (isset($data['type_toilet']) && $data['type_toilet'] == 3) {echo '';} else {echo 'hidden';} ?>">
			  		<label class="col-sm-2 control-label"> Specify others </label>
			  		<div class="col-sm-3">
			  			{!! Form::text('communitynursing[type_toilet_specify]',isset($data['type_toilet_specify']) ? $data['type_toilet_specify'] : NULL, ['class'=>'form-control']) !!}
			  		</div>
			  	</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Distance of toilet from the toilet? </label>
		  		<div class="col-sm-3">
		  			{!! Form::select('communitynursing[toilet_distance]', $toilet_distance, isset($data['toilet_distance']) ? $data['toilet_distance'] : NULL, ['class'=>'form-control','id'=>'toilet_distance_select','placeholder'=>'Please select...']) !!}
		  		</div>
		  	</div>
			<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Mention the distance of water source from </label>
		  	</div>
		  	<div class="col-sm-offset-1 col-sm-11">
		  		<label class="col-sm-3 control-label"> Toilet </label>
		  		<div class="col-sm-4">
		  			{!! Form::number('communitynursing[toilet_fr_water]',isset($data['toilet_fr_water']) ? $data['toilet_fr_water'] : NULL, ['class'=>'form-control']) !!}
		  		</div>
		  		<div class="col-sm-2">
  					{!! Form::select('communitynursing[toilet_fr_water_unit]',[0=>'feet',1=>'meter'],isset($data['toilet_fr_water_unit']) ? $data['toilet_fr_water_unit'] : NULL, ['class'=>'form-control','placeholder'=>'Please select...']) !!}
		  		</div>
		  	</div> 
		  	<div class="col-sm-offset-1 col-sm-11">
		  		<label class="col-sm-3 control-label"> Waste disposal site </label>
		  		<div class="col-sm-4">
		  			{!! Form::number('communitynursing[toilet_fr_waste]',isset($data['toilet_fr_waste']) ? $data['toilet_fr_waste'] : NULL, ['class'=>'form-control']) !!}
		  		</div>
		  		<div class="col-sm-2">
  					{!! Form::select('communitynursing[toilet_fr_waste_unit]',[0=>'feet',1=>'meter'],isset($data['toilet_fr_waste_unit']) ? $data['toilet_fr_waste_unit'] : NULL, ['class'=>'form-control','placeholder'=>'Please select...']) !!}
		  		</div>
		  	</div>  	
			<div class="col-sm-12">
				<label class="col-sm-4 control-label"> Methods of waste disposal </label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[disposal_method_composting]',1, (isset($data['disposal_method_composting']) && $data['disposal_method_composting'] == 1) ? true : false) !!} Composting
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[disposal_method_burning]',1, (isset($data['disposal_method_burning']) && $data['disposal_method_burning'] == 1) ? true : false) !!} Burning
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[disposal_method_dumping]',1, (isset($data['disposal_method_dumping']) && $data['disposal_method_dumping'] == 1) ? true : false) !!} Dumping
					</label>
				    <label class="checkbox" for="waste_disposal_other">
				    	<div class="form-inline">
				        {!! Form::checkbox('communitynursing[disposal_method_others]',1, (isset($data['disposal_method_others']) && $data['disposal_method_others'] == 1) ? true : false, ['id'=>'waste_disposal_other']) !!} Other
				        {!! Form::text('communitynursing[disposal_method_specify]',isset($data['disposal_method_specify']) ? $data['disposal_method_specify'] : NULL, ['class'=>'form-control','id'=>'disposal_method_specify']) !!} 
				    	</div>
				    </label>
				</div>
	  		</div>
	  		<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Are waste collectors available? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[waste_collectors]',0,(isset($data['waste_collectors']) && $data['waste_collectors'] == 0) ? true : false,['id'=>'waste_collectors_no', 'class'=>'waste_collectors_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[waste_collectors]',1,(isset($data['waste_collectors']) && $data['waste_collectors'] == 1) ? true : false,['id'=>'waste_collectors_yes', 'class'=>'waste_collectors_radio']) !!} Yes
					</label>
				</div>
			  	<div class="<?php if(!isset($data['waste_collectors']) || (isset($data['waste_collectors']) && $data['waste_collectors'] != 1)) { echo 'hidden'; } ?>" id="waste_collectors_timely">
					<label class="col-sm-4 control-label"> Timely collection of waste? </label>
					<div class="col-sm-2">
						<label class="radio-inline">
							{!! Form::radio('communitynursing[timely_collection_waste]',0,(isset($data['timely_collection_waste']) && $data['timely_collection_waste'] == 0) ? true : false,['id'=>'timely_collection_waste_no', 'class'=>'timely_collection_waste_radio']) !!} No
						</label>
						<label class="radio-inline">
							{!! Form::radio('communitynursing[timely_collection_waste]',1,(isset($data['timely_collection_waste']) && $data['timely_collection_waste'] == 1) ? true : false,['id'=>'timely_collection_waste_yes', 'class'=>'timely_collection_waste_radio']) !!} Yes
						</label>
					</div>
			  	</div>
		  	</div>
		  	<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Are dead animal bodies disposed properly? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[dead_animal_disposal]',0,(isset($data['dead_animal_disposal']) && $data['dead_animal_disposal'] == 0) ? true : false,['id'=>'dead_animal_disposal_no', 'class'=>'dead_animal_disposal_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[dead_animal_disposal]',1,(isset($data['dead_animal_disposal']) && $data['dead_animal_disposal'] == 1) ? true : false,['id'=>'dead_animal_disposal_yes', 'class'=>'dead_animal_disposal_radio']) !!} Yes
					</label>
				</div>
		  	</div>
		  	<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Drainage facility? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[drainage_facility]',0,(isset($data['drainage_facility']) && $data['drainage_facility'] == 0) ? true : false,['id'=>'drainage_facility_no', 'class'=>'drainage_facility_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[drainage_facility]',1,(isset($data['drainage_facility']) && $data['drainage_facility'] == 1) ? true : false,['id'=>'drainage_facility_yes', 'class'=>'drainage_facility_radio']) !!} Yes
					</label>
				</div>
		  	</div>
		  	<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Is the surrounding area clean? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[surr_area_clean]',0,(isset($data['surr_area_clean']) && $data['surr_area_clean'] == 0) ? true : false,['id'=>'surr_area_clean_no', 'class'=>'surr_area_clean_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[surr_area_clean]',1,(isset($data['surr_area_clean']) && $data['surr_area_clean'] == 1) ? true : false,['id'=>'surr_area_clean_yes', 'class'=>'surr_area_clean_radio']) !!} Yes
					</label>
				</div>
		  	</div>
			<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Is there any problem for women in maintaining personal hygiene during menstruation? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[prob_hygiene_mens]',0,(isset($data['prob_hygiene_mens']) && $data['prob_hygiene_mens'] == 0) ? true : false,['id'=>'prob_hygiene_mens_radio_no', 'class'=>'prob_hygiene_mens_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[prob_hygiene_mens]',1,(isset($data['prob_hygiene_mens']) && $data['prob_hygiene_mens'] == 1) ? true : false,['id'=>'prob_hygiene_mens_radio_yes', 'class'=>'prob_hygiene_mens_radio']) !!} Yes
					</label>
				</div>
			</div>
			<div class="<?php if(!isset($data['prob_hygiene_mens']) || (isset($data['prob_hygiene_mens']) && $data['prob_hygiene_mens'] != 1)) { echo 'hidden'; } ?>" id="prob_hygiene_reason_spec">
				<div class="col-sm-offset-1 col-sm-11">
					<label class="col-sm-4 control-label">If yes, what are the reasons?</label>
					<div class="col-sm-6">
						<label class="checkbox">
							{!! Form::checkbox('communitynursing[prob_hygience_reas_watersupply]',1, isset($data['prob_hygiene_reas_watersupply']) && $data['prob_hygiene_reas_watersupply'] == 1 ? true : false) !!} Lack of water supply
						</label>
						<label class="checkbox">
							{!! Form::checkbox('communitynursing[prob_hygience_reas_privacy]',1, isset($data['prob_hygiene_reas_privacy']) && $data['prob_hygiene_reas_privacy'] == 1 ? true : false) !!} Lack of privacy (separate and safe washroom)
						</label>
						<label class="checkbox">
							{!! Form::checkbox('communitynursing[prob_hygience_reas_pads]',1, isset($data['prob_hygiene_reas_pads']) && $data['prob_hygiene_reas_pads'] == 1 ? true : false) !!} Inadequate pads
						</label>
						<label class="checkbox">
							{!! Form::checkbox('communitynursing[prob_hygience_reas_padsdisposal]',1, isset($data['prob_hygiene_reas_padsdisposal']) && $data['prob_hygiene_reas_padsdisposal'] == 1 ? true : false) !!} Problems with disposing the pad
						</label>
					    <label class="checkbox">
					        {!! Form::checkbox('communitynursing[prob_hygience_reas_oth]',4, isset($data['prob_hygiene_reas_oth']) && $data['prob_hygiene_reas_oth'] == 1 ? true : false, ['id'=>'prob_hygiene_reason_other']) !!} Other
					        {!! Form::text('communitynursing[prob_hygiene_reas_oth_spec]',isset($data['prob_hygiene_reas_oth_spec']) ? $data['prob_hygiene_reas_oth_spec'] : NULL, ['class'=>'form-control','id'=>'prob_hygiene_reas_oth_spec']) !!} 
					    </label>
					</div>
			  	</div>
	  		</div>
			<div class="col-sm-12">
				<label class="col-sm-4 control-label">Any visible hazards?</label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[visible_hazards_none]',1, (isset($data['visible_hazards_none']) && $data['visible_hazards_none'] == 1) ? true : false) !!} None
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[visible_hazards_floor]',1, (isset($data['visible_hazards_floor']) && $data['visible_hazards_floor'] == 1) ? true : false) !!} Slippery floor
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[visible_hazards_light]',1, (isset($data['visible_hazards_light']) && $data['visible_hazards_light'] == 1) ? true : false) !!} Inadequate light
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[visible_hazards_electricity]',1, (isset($data['visible_hazards_electricity']) && $data['visible_hazards_electricity'] == 1) ? true : false) !!} Hooking of electricity
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[visible_hazards_surface]',1, (isset($data['visible_hazards_surface']) && $data['visible_hazards_surface'] == 1) ? true : false) !!} Rough surface
					</label>
				    <label class="checkbox">
						{!! Form::checkbox('communitynursing[visible_hazards_land]',1, (isset($data['visible_hazards_land']) && $data['visible_hazards_land'] == 1) ? true : false) !!} Sloppy land
					</label>
				    <label class="checkbox">
						{!! Form::checkbox('communitynursing[visible_hazards_dumparea]',1, (isset($data['visible_hazards_dumparea']) && $data['visible_hazards_dumparea'] == 1) ? true : false) !!} Dumping area
					</label>
				    <label class="checkbox">
						{!! Form::checkbox('communitynursing[visible_hazards_pollution]',1, (isset($data['visible_hazards_pollution']) && $data['visible_hazards_pollution'] == 1) ? true : false) !!} Extremely dusty known air polluted area
					</label>
				    <label class="checkbox">
						{!! Form::checkbox('communitynursing[visible_hazards_ventilation]',1, (isset($data['visible_hazards_ventilation']) && $data['visible_hazards_ventilation'] == 1) ? true : false) !!} Inadequate ventilation
					</label>
				    <label class="checkbox">
						{!! Form::checkbox('communitynursing[visible_hazards_noise]',1, (isset($data['visible_hazards_noise']) && $data['visible_hazards_noise'] == 1) ? true : false) !!} Constantly noisy place
					</label>
				    <label class="checkbox" for="visible_hazards_other">
				        {!! Form::checkbox('communitynursing[visible_hazards_oth]',1, (isset($data['visible_hazards_oth']) && $data['visible_hazards_oth'] == 1) ? true : false, ['id'=>'visible_hazards_other']) !!} Other
				        {!! Form::text('communitynursing[visible_hazards_oth_spec]',isset($data['visible_hazards_oth_spec']) ? $data['visible_hazards_oth_spec'] : NULL, ['class'=>'form-control','id'=>'visible_hazards_oth_spec']) !!} 
				    </label>
				</div>
		  	</div>
			<div class="col-sm-12">
				<label class="col-sm-4 control-label"> Is the area/place bites prone? </label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[area_prone_to_none]',1, (isset($data['area_prone_to_none']) && $data['area_prone_to_none'] == 1) ? true : false) !!} None
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[bite_prone_snake]',1, (isset($data['bite_prone_snake']) && $data['bite_prone_snake'] == 1) ? true : false) !!} Snake bite
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[bite_prone_animal]',1, (isset($data['bite_prone_animal']) && $data['bite_prone_animal'] == 1) ? true : false) !!} Animal bite
						{!! Form::text('communitynursing[bite_prone_animal_spec]',isset($data['bite_prone_animal_spec']) ? $data['bite_prone_animal_spec'] : NULL, ['class'=>'form-control','id'=>'bite_prone_animal_spec', 'placeholder'=>'Specify animal']) !!} 
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[bite_prone_insect]',1, (isset($data['bite_prone_insect']) && $data['bite_prone_insect'] == 1) ? true : false) !!} Insect bite
						{!! Form::text('communitynursing[bite_prone_insect_spec]',isset($data['bite_prone_insect_spec']) ? $data['bite_prone_insect_spec'] : NULL, ['class'=>'form-control','id'=>'bite_prone_insect_spec','placeholder'=>'Specify insect']) !!} 
					</label>
				    <label class="checkbox">
				        {!! Form::checkbox('communitynursing[bite_prone_oth]',1, (isset($data['bite_prone_oth']) && $data['bite_prone_oth'] == 1) ? true : false, ['id'=>'bite_prone_oth']) !!} Others
				        {!! Form::text('communitynursing[bite_prone_oth_spec]',isset($data['bite_prone_oth_spec']) ? $data['bite_prone_oth_spec'] : NULL, ['class'=>'form-control','id'=>'bite_prone_oth_spec','placeholder'=>'Specify other']) !!} 
				    </label>
				</div>
	  		</div>
			<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Free of vectors (flies/insects/mouse) </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[free_vectors]',0,(isset($data['free_vectors']) && $data['free_vectors'] == 0) ? true : false,['id'=>'free_vectors_no', 'class'=>'free_vectors_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[free_vectors]',1,(isset($data['free_vectors']) && $data['free_vectors'] == 1) ? true : false,['id'=>'free_vectors_yes', 'class'=>'free_vectors_radio']) !!} Yes
					</label>
				</div>
		  	</div>
		  	<div class="col-sm-12">
				<label class="col-sm-4 control-label"> Use of mosquito preventive measure? </label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[mosqprev_measure_net',1, (isset($data['mosqprev_measure_net']) && $data['mosqprev_measure_net']) == 1 ? true : false) !!} Mosquito nets
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[mosqprev_measure_liquid',1, (isset($data['mosqprev_measure_liquid']) && $data['mosqprev_measure_liquid']) == 1 ? true : false) !!} Repelling liquid
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[mosqprev_measure_drug',1, (isset($data['mosqprev_measure_drug']) && $data['mosqprev_measure_drug']) == 1 ? true : false) !!} Repelling drugs
					</label>
				    <label class="checkbox" for="mosquito_preventive_measure_other">
				        {!! Form::checkbox('communitynursing[mosqprev_measure_oth',1, (isset($data['mosqprev_measure_oth']) && $data['mosqprev_measure_oth']) == 1 ? true : false, ['id'=>'mosquito_preventive_measure_other']) !!} Others
				        {!! Form::text('communitynursing[mosq_prev_oth_spec]',isset($data['mosq_prev_oth_spec']) ? $data['mosq_prev_oth_spec'] : NULL, ['class'=>'form-control','id'=>'mosq_prev_oth_spec','placeholder'=>'Specify other']) !!} 
				    </label>
				</div>
	  		</div>
	  		<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Periodic pest control as per needed </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[periodic_pest_control]',0,(isset($data['periodic_pest_control']) && $data['periodic_pest_control'] == 0) ? true : false,['id'=>'periodic_pest_control_no', 'class'=>'periodic_pest_control_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[periodic_pest_control]',1,(isset($data['periodic_pest_control']) && $data['periodic_pest_control'] == 1) ? true : false,['id'=>'periodic_pest_control_yes', 'class'=>'periodic_pest_control_radio']) !!} Yes
					</label>
				</div>
		  	</div>
		</div>
	  </div>
	  <div id="housing" class="tab-pane fade in">
	  	<div class="page-header">
	  		 Housing/Shelter
	  	</div>
	  	<div class="icheck">
			<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Type of Housing/Shelter </label>
		  		<div class="col-sm-3">
		  			{!! Form::select('communitynursing[housing]', $housing, isset($data['housing']) ? $data['housing'] : NULL, ['class'=>'form-control','id'=>'housing_select','placeholder'=>'Please select...']) !!}
		  		</div>
		  		<div id="housing_oth_div" class="<?php if (isset($data['housing']) && $data['housing'] == 4) {echo '';} else {echo 'hidden';} ?>">
			  		<label class="col-sm-2 control-label"> Specify others </label>
			  		<div class="col-sm-3">
			  			{!! Form::text('communitynursing[housing_oth]',isset($data['housing_oth']) ? $data['housing_oth'] : NULL, ['class'=>'form-control']) !!}
			  		</div>
			  	</div>
		  	</div>
		  	<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Adequacy of space? </label>
				<div class="col-sm-3">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[space_adequacy]',0,(isset($data['space_adequacy']) && $data['space_adequacy'] == 0) ? true : false,['id'=>'space_adequacy_adequate', 'class'=>'space_adequacy_radio']) !!} Adequate
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[space_adequacy]',1,(isset($data['space_adequacy']) && $data['space_adequacy'] == 1) ? true : false,['id'=>'space_adequacy_crowded', 'class'=>'space_adequacy_radio']) !!} Crowded
					</label>
				</div>
		  	</div>
		  	<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Infrastructural damage </label>
				<div class="col-sm-4">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[house_damage]',0,(isset($data['house_damage']) && $data['house_damage'] == 0) ? true : false,['id'=>'house_damage_no', 'class'=>'house_damage_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[house_damage]',1,(isset($data['house_damage']) && $data['house_damage'] == 1) ? true : false,['id'=>'house_damage_complete', 'class'=>'house_damage_radio']) !!} Complete
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[house_damage]',2,(isset($data['house_damage']) && $data['house_damage'] == 2) ? true : false,['id'=>'house_damage_partial', 'class'=>'house_damage_radio']) !!} Partial
					</label>
				</div>
		  	</div>
			<div class="col-sm-12">
				<label class="col-sm-4 control-label">Source of light</label>
				<div class="col-sm-8">
					<div class="form-inline">
						<label class="checkbox">
							{!! Form::checkbox('communitynursing[source_of_light_electricity]',1,(isset($data['source_of_light_electricity']) && $data['source_of_light_electricity'] == 1) ? true : false) !!} Electricity
						</label>
						<label class="checkbox">
							{!! Form::checkbox('communitynursing[source_of_light_oil_lamp]',1,(isset($data['source_of_light_oil_lamp']) && $data['source_of_light_oil_lamp'] == 1) ? true : false) !!} Oil lamp
						</label>
						<label class="checkbox">
							{!! Form::checkbox('communitynursing[source_of_light_solar]',1,(isset($data['source_of_light_solar']) && $data['source_of_light_solar'] == 1) ? true : false) !!} Solar
						</label>
						<label class="checkbox" for="source_of_light_other">
					        {!! Form::checkbox('communitynursing[source_of_light_others]',1,(isset($data['source_of_light_others']) && $data['source_of_light_others'] == 1) ? true : false) !!} Others
					        {!! Form::text('communitynursing[source_of_light_specify]',isset($data['source_of_light_specify']) ? $data['source_of_light_specify'] : NULL, ['class'=>'form-control','id'=>'source_of_light_specify']) !!} 
					    </label>
				    </div>
				</div>
		  	</div>
		  	<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Adequate ventilation? </label>
				<div class="col-sm-8">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[ventilation_adequacy]',0,(isset($data['ventilation_adequacy']) && $data['ventilation_adequacy'] == 0) ? true : false,['id'=>'ventilation_adequacy_no', 'class'=>'ventilation_adequacy_radio']) !!} No window
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[ventilation_adequacy]',1,(isset($data['ventilation_adequacy']) && $data['ventilation_adequacy'] == 1) ? true : false,['id'=>'ventilation_adequacy_one', 'class'=>'ventilation_adequacy_radio']) !!} Only one window
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[ventilation_adequacy]',2,(isset($data['ventilation_adequacy']) && $data['ventilation_adequacy'] == 2) ? true : false,['id'=>'ventilation_adequacy_two', 'class'=>'ventilation_adequacy_radio']) !!} Two or more windows
					</label>
				</div>
		  	</div>
	  		<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Kitchen </label>
				<div class="col-sm-4">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[kitchen]',0,(isset($data['kitchen']) && $data['kitchen'] == 0) ? true : false,['id'=>'kitchen_no', 'class'=>'kitchen_radio']) !!} Separated
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[kitchen]',1,(isset($data['kitchen']) && $data['kitchen'] == 1) ? true : false,['id'=>'kitchen_yes', 'class'=>'kitchen_radio']) !!} Attached to room
					</label>
				</div>
	  		</div>
			<div class="page-header">
		  		 Nutrition
		  	</div>
		  	<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Food stock available? </label>
				<div class="col-sm-8">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[foodstock_avail]',0,(isset($data['foodstock_avail']) && $data['foodstock_avail'] == 0) ? true : false,['id'=>'foodstock_avail_no', 'class'=>'foodstock_avail_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[foodstock_avail]',1,(isset($data['foodstock_avail']) && $data['foodstock_avail'] == 1) ? true : false,['id'=>'foodstock_avail_one', 'class'=>'foodstock_avail_radio']) !!} Adequate
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[foodstock_avail]',2,(isset($data['foodstock_avail']) && $data['foodstock_avail'] == 2) ? true : false,['id'=>'foodstock_avail_two', 'class'=>'foodstock_avail_radio']) !!} Inadequate
					</label>
				</div>
		  	</div>
			<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Fuel used during cooking </label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[cooking_fuel_used_gas]',1, (isset($data['cooking_fuel_used_gas']) && $data['cooking_fuel_used_gas'] == 1) ? true : false) !!} Gas
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[cooking_fuel_used_firewood]',1, (isset($data['cooking_fuel_used_firewood']) && $data['cooking_fuel_used_firewood'] == 1) ? true : false) !!} Firewood
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[cooking_fuel_used_kerosene]',1, (isset($data['cooking_fuel_used_kerosene']) && $data['cooking_fuel_used_kerosene'] == 1) ? true : false) !!} Kerosene
					</label>
				    <label class="checkbox">
				    	<div class="form-inline">
				        {!! Form::checkbox('communitynursing[cooking_fuel_used_others]',1, (isset($data['cooking_fuel_used_others']) && $data['cooking_fuel_used_others'] == 1) ? true : false, ['id'=>'waste_disposal_other']) !!} Other
				        {!! Form::text('communitynursing[cooking_fuel_used_specify]',isset($data['cooking_fuel_used_specify']) ? $data['cooking_fuel_used_specify'] : NULL, ['class'=>'form-control','id'=>'cooking_fuel_used_specify']) !!} 
				    	</div>
				    </label>
				</div>
		  	</div>
		  	<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> What type of food is supplied? Available? </label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[kind_of_food_available_cooked]',1, (isset($data['kind_of_food_available_cooked']) && $data['kind_of_food_available_cooked'] == 1) ? true : false) !!} Cooked
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[kind_of_food_available_junk]',1, (isset($data['kind_of_food_available_junk']) && $data['kind_of_food_available_junk'] == 1) ? true : false) !!} Junk
					</label>
				    <label class="checkbox">
				    	<div class="form-inline">
				        {!! Form::checkbox('communitynursing[kind_of_food_available_others]',1, (isset($data['kind_of_food_available_others']) && $data['kind_of_food_available_others'] == 1) ? true : false, ['id'=>'waste_disposal_other']) !!} Other
				        {!! Form::text('communitynursing[kind_of_food_available_specify]',isset($data['kind_of_food_available_specify']) ? $data['kind_of_food_available_specify'] : NULL, ['class'=>'form-control','id'=>'kind_of_food_available_specify']) !!} 
				    	</div>
				    </label>
				</div>
		  	</div>
			<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Are supplied food safe and hygienic? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[safe_food]',0,(isset($data['safe_food']) && $data['safe_food'] == 0) ? true : false,['id'=>'safe_food_no', 'class'=>'safe_food_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[safe_food]',1,(isset($data['safe_food']) && $data['safe_food'] == 1) ? true : false,['id'=>'safe_food_yes', 'class'=>'safe_food_radio']) !!} Yes
					</label>
				</div>
		  	</div>
		  	<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Is food storage appropriate? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[food_storage]',0,(isset($data['food_storage']) && $data['food_storage'] == 0) ? true : false,['id'=>'food_storage_no', 'class'=>'food_storage_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[food_storage]',1,(isset($data['food_storage']) && $data['food_storage'] == 1) ? true : false,['id'=>'food_storage_yes', 'class'=>'food_storage_radio']) !!} Yes
					</label>
				</div>
		  	</div>
		 </div>
	  </div>
	  <div id="health" class="tab-pane fade in">
	  	<div class="page-header">
	  		 Health Condition
	  	</div>
	  	<div class="icheck">
			<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Unusual disease/event outbreak </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[disease_outbreak]',0,(isset($data['disease_outbreak']) && $data['disease_outbreak'] == 0) ? true : false,['id'=>'disease_outbreak_no', 'class'=>'disease_outbreak_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[disease_outbreak]',1,(isset($data['disease_outbreak']) && $data['disease_outbreak'] == 1) ? true : false,['id'=>'disease_outbreak_yes', 'class'=>'disease_outbreak_radio']) !!} Yes
					</label>
				</div>
				<div class="<?php if(!isset($data['disease_outbreak']) || (isset($data['disease_outbreak']) && $data['disease_outbreak'] != 1)) { echo 'hidden'; } ?>" id="disease_outbreak_yes_div">
					<label class="col-sm-2 control-label"> If yes </label>
					<div class="col-sm-4">
						{!! Form::text('communitynursing[disease_outbreak_spec]',isset($data['disease_outbreak_spec']) ? $data['disease_outbreak_spec'] : NULL, ['class'=>'form-control']) !!}	
					</div>
	 			</div>
	  		</div>
	  		<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Health care service on site? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[hs_onsite]',0,(isset($data['hs_onsite']) && $data['hs_onsite'] == 0) ? true : false,['id'=>'hs_onsite_no', 'class'=>'hs_onsite_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[hs_onsite]',1,(isset($data['hs_onsite']) && $data['hs_onsite'] == 1) ? true : false,['id'=>'hs_onsite_yes', 'class'=>'hs_onsite_radio']) !!} Yes
					</label>
				</div>
	  		</div>
	  		<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Psychological support/counselling available? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[pfa]',0,(isset($data['pfa']) && $data['pfa'] == 0) ? true : false,['id'=>'pfa_no', 'class'=>'pfa_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[pfa]',1,(isset($data['pfa']) && $data['pfa'] == 1) ? true : false,['id'=>'pfa_yes', 'class'=>'pfa_radio']) !!} Yes
					</label>
				</div>
	  		</div>
			<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Distance of nearest health facility from home/shelter? </label>
				<div class="col-sm-4">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[distance_faci]',0,(isset($data['distance_faci']) && $data['distance_faci'] == 0) ? true : false,['id'=>'distance_faci_zero', 'class'=>'distance_faci_radio']) !!} Less than 1 hour
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[distance_faci]',1,(isset($data['distance_faci']) && $data['distance_faci'] == 1) ? true : false,['id'=>'distance_faci_one', 'class'=>'distance_faci_radio']) !!} More than 1 hour
					</label>
				</div>
	  		</div>
			<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> Type of nearest health facility </label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[type_of_nearest_facility_health_post]',1, (isset($data['type_of_nearest_facility_health_post']) && $data['type_of_nearest_facility_health_post'] == 1) ? true : false) !!} Health post
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[type_of_nearest_facility_primary_health_center]',1, (isset($data['type_of_nearest_facility_primary_health_center']) && $data['type_of_nearest_facility_primary_health_center'] == 1) ? true : false) !!} Primary health center
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[type_of_nearest_facility_district_hospital]',1, (isset($data['type_of_nearest_facility_district_hospital']) && $data['type_of_nearest_facility_district_hospital'] == 1) ? true : false) !!} District hospital
					</label>
				    <label class="checkbox">
						{!! Form::checkbox('communitynursing[type_of_nearest_facility_zonal_hospital]',1, (isset($data['type_of_nearest_facility_zonal_hospital']) && $data['type_of_nearest_facility_zonal_hospital'] == 1) ? true : false) !!} Zonal hospital
					</label>
				    <label class="checkbox">
						{!! Form::checkbox('communitynursing[type_of_nearest_facility_sub_regional_hospital]',1, (isset($data['type_of_nearest_facility_sub_regional_hospital']) && $data['type_of_nearest_facility_sub_regional_hospital'] == 1) ? true : false) !!} Regional hospital
					</label>
				    <label class="checkbox">
						{!! Form::checkbox('communitynursing[type_of_nearest_facility_tertiary_hospital]',1, (isset($data['type_of_nearest_facility_tertiary_hospital']) && $data['type_of_nearest_facility_tertiary_hospital'] == 1) ? true : false) !!} Tertiary hospital
					</label>
				    <label class="checkbox">
						{!! Form::checkbox('communitynursing[type_of_nearest_facility_private_hospital]',1, (isset($data['type_of_nearest_facility_private_hospital']) && $data['type_of_nearest_facility_private_hospital'] == 1) ? true : false) !!} Private hospital
					</label>
				    <label class="checkbox">
				    	<div class="form-inline">
				        {!! Form::checkbox('communitynursing[type_of_nearest_facility_others]',1, (isset($data['type_of_nearest_facility_others']) && $data['type_of_nearest_facility_others'] == 1) ? true : false, ['id'=>'type_of_nearest_facility_others']) !!} Other
				        {!! Form::text('communitynursing[type_of_nearest_facility_specify]',isset($data['type_of_nearest_facility_specify']) ? $data['type_of_nearest_facility_specify'] : NULL, ['class'=>'form-control','id'=>'type_of_nearest_facility_specify']) !!} 
				    	</div>
				    </label>
				</div>
		  	</div>
			<div class="col-sm-12">
				<label class="col-sm-4 control-label">Any known case of illness in your family?</label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[family_illness_none]',1,(isset($data['family_illness_none']) && $data['family_illness_none'] == 1) ? true : false) !!} None
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[family_illness_diabetes]',1,(isset($data['family_illness_diabetes']) && $data['family_illness_diabetes'] == 1) ? true : false) !!} Diabetes mellitus
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[family_illness_hypertension]',1,(isset($data['family_illness_hypertension']) && $data['family_illness_hypertension'] == 1) ? true : false) !!} Hypertension
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[family_illness_copd]',1,(isset($data['family_illness_copd']) && $data['family_illness_copd'] == 1) ? true : false) !!} COPD
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[family_illness_cancer]',1,(isset($data['family_illness_cancer']) && $data['family_illness_cancer'] == 1) ? true : false) !!} Cancer
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[family_illness_mental]',1,(isset($data['family_illness_mental']) && $data['family_illness_mental'] == 1) ? true : false) !!} Mental health problems
					</label>
					<label class="checkbox" for="family_illness_other">
				        {!! Form::checkbox('communitynursing[family_illness_oth]',1,(isset($data['family_illness_oth']) && $data['family_illness_oth'] == 1) ? true : false) !!} Other
				        {!! Form::text('communitynursing[family_illness_oth_spec]',isset($data['family_illness_oth_spec']) ? $data['family_illness_oth_spec'] : NULL, ['class'=>'form-control','id'=>'family_illness_oth_spec']) !!} 
				    </label>
				</div>
		  	</div>
	  	</div>
	  </div>
	  <div id="familyplanning" class="tab-pane fade in">
	  	<div class="page-header">
	  		 Family Planning
	  	</div>
	  	<div class="icheck">
	  		<div class="radio col-sm-12">
				<label class="col-sm-6 control-label"> Use of family planning methods </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[fp_use]',0,(isset($data['fp_use']) && $data['fp_use'] == 0) ? true : false,['id'=>'fp_use_no', 'class'=>'fp_use_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[fp_use]',1,(isset($data['fp_use']) && $data['fp_use'] == 1) ? true : false,['id'=>'fp_use_yes', 'class'=>'fp_use_radio']) !!} Yes
					</label>
				</div>
		  	</div>
	  		<div class="radio col-sm-12">
				<label class="col-sm-4 control-label"> Type of family planning methods used </label>
				<div class="col-sm-8">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[fp_method_used]',0,(isset($data['fp_method_used']) && $data['fp_method_used'] == 0) ? true : false,['id'=>'fp_use_no', 'class'=>'fp_use_radio']) !!} Permanent
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[fp_method_used]',1,(isset($data['fp_method_used']) && $data['fp_method_used'] == 1) ? true : false,['id'=>'fp_method_used_yes', 'class'=>'fp_method_used_radio']) !!} Temporary
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[fp_method_used]',2,(isset($data['fp_method_used']) && $data['fp_method_used'] == 2) ? true : false,['id'=>'fp_method_used_yes', 'class'=>'fp_method_used_radio']) !!} Natural
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[fp_method_used]',3,(isset($data['fp_method_used']) && $data['fp_method_used'] == 1) ? true : false,['id'=>'fp_method_used_yes', 'class'=>'fp_method_used_radio']) !!} None
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[fp_method_used]',4,(isset($data['fp_method_used']) && $data['fp_method_used'] == 1) ? true : false,['id'=>'fp_method_used_yes', 'class'=>'fp_method_used_radio']) !!} Not Applicable
					</label>
				</div>
		  	</div>
		  	<div class="radio col-sm-offset-1 col-sm-12">
				<label class="col-sm-6 control-label"> If temporary, is the respondent aware of the availability of service? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[fp_temp_awareness]',0,(isset($data['fp_temp_awareness']) && $data['fp_temp_awareness'] == 0) ? true : false,['id'=>'fp_temp_awareness_no', 'class'=>'fp_temp_awareness_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[fp_temp_awareness]',1,(isset($data['fp_temp_awareness']) && $data['fp_temp_awareness'] == 1) ? true : false,['id'=>'fp_temp_awareness_yes', 'class'=>'fp_temp_awareness_radio']) !!} Yes
					</label>
				</div>
		  	</div>
		  	<div class="radio col-sm-offset-1 col-sm-12">
				<label class="col-sm-6 control-label"> If temporary, is the respondent aware on safety and security? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[fp_awareness_security]',0,(isset($data['fp_awareness_security']) && $data['fp_awareness_security'] == 0) ? true : false,['id'=>'fp_awareness_security_no', 'class'=>'fp_awareness_security_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[fp_awareness_security]',1,(isset($data['fp_awareness_security']) && $data['fp_awareness_security'] == 1) ? true : false,['id'=>'fp_awareness_security_yes', 'class'=>'fp_awareness_security_radio']) !!} Yes
					</label>
				</div>
		  	</div>
		  	<div class="radio col-sm-12">
				<label class="col-sm-6 control-label"> Awareness on Emergency Contraceptive Pills (ECP)? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[fp_awareness_ecp]',0,(isset($data['fp_awareness_ecp']) && $data['fp_awareness_ecp'] == 0) ? true : false,['id'=>'fp_awareness_ecp_no', 'class'=>'fp_awareness_ecp_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[fp_awareness_ecp]',1,(isset($data['fp_awareness_ecp']) && $data['fp_awareness_ecp'] == 1) ? true : false,['id'=>'fp_awareness_ecp_yes', 'class'=>'fp_awareness_ecp_radio']) !!} Yes
					</label>
				</div>
		  	</div>
			<div class="col-sm-12">
		  		<label class="col-sm-4 control-label"> If yes, where is ECP available? </label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[where_ecp_available_mobile_clinic]',1, (isset($data['where_ecp_available_mobile_clinic']) && $data['where_ecp_available_mobile_clinic'] == 1) ? true : false) !!} Mobile clinic
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[where_ecp_available_health_facility]',1, (isset($data['where_ecp_available_health_facility']) && $data['where_ecp_available_health_facility'] == 1) ? true : false) !!} Health facility
					</label>
				    <label class="checkbox">
				    	<div class="form-inline">
				        {!! Form::checkbox('communitynursing[where_ecp_available_others]',1, (isset($data['where_ecp_available_others']) && $data['where_ecp_available_others'] == 1) ? true : false, ['id'=>'where_ecp_available_others']) !!} Other
				        {!! Form::text('communitynursing[where_ecp_available_specify]',isset($data['where_ecp_available_specify']) ? $data['where_ecp_available_specify'] : NULL, ['class'=>'form-control','id'=>'where_ecp_available_specify']) !!} 
				    	</div>
				    </label>
				</div>
		  	</div>	  	
		 </div>
	  </div>
	  <div id="mental" class="tab-pane fade in">
	  	<div class="page-header">
	  		 Mental Illness
	  	</div>
	  	<div class="icheck">
	  		<div class="radio col-sm-12">
				<label class="col-sm-6 control-label"> Does any one in your family have mental illness? </label>
				<div class="col-sm-2">
					<label class="radio-inline">
						{!! Form::radio('communitynursing[family_mental_illness]',0,(isset($data['family_mental_illness']) && $data['family_mental_illness'] == 0) ? true : false,['id'=>'family_mental_illness_no', 'class'=>'family_mental_illness_radio']) !!} No
					</label>
					<label class="radio-inline">
						{!! Form::radio('communitynursing[family_mental_illness]',1,(isset($data['family_mental_illness']) && $data['family_mental_illness'] == 1) ? true : false,['id'=>'family_mental_illness_yes', 'class'=>'family_mental_illness_radio']) !!} Yes
					</label>
				</div>
			</div>
			<div id="medication_div" class="<?php if (isset($data['family_mental_illness']) && $data['family_mental_illness'] == 1) {echo '';} else {echo 'hidden';} ?>">
		  		<div class="radio col-sm-12">
					<label class="col-sm-6 control-label"> Are they under medication? </label>
					<div class="col-sm-2">
						<label class="radio-inline">
							{!! Form::radio('communitynursing[medication]',0,(isset($data['medication']) && $data['medication'] == 0) ? true : false,['id'=>'medication_no', 'class'=>'medication_radio']) !!} No
						</label>
						<label class="radio-inline">
							{!! Form::radio('communitynursing[medication]',1,(isset($data['medication']) && $data['medication'] == 1) ? true : false,['id'=>'medication_yes', 'class'=>'medication_radio']) !!} Yes
						</label>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<label class="col-sm-6 control-label">Potential health hazards?</label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[mental_hazard_none]',1,(isset($data['health_hazards_none']) && $data['health_hazards_none'] == 1) ? true : false) !!} None
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[mental_hazard_smoking]',1,(isset($data['mental_hazard_smoking']) && $data['mental_hazard_smoking'] == 1) ? true : false) !!} Smoking
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[mental_hazard_alcoholism]',1,(isset($data['mental_hazard_alcoholism']) && $data['mental_hazard_alcoholism'] == 1) ? true : false) !!} Alcoholism
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[mental_hazard_drug]',1,(isset($data['mental_hazard_drug']) && $data['mental_hazard_drug'] == 1) ? true : false) !!} Drug abuse
					</label>
					<label class="checkbox" for="mental_hazard_other">
				        {!! Form::checkbox('communitynursing[mental_hazard_oth]',1,(isset($data['mental_hazard_oth']) && $data['mental_hazard_oth'] == 1) ? true : false) !!} Other
				        {!! Form::text('communitynursing[mental_hazard_oth_spec]',isset($data['mental_hazard_oth_spec']) ? $data['mental_hazard_oth_spec'] : NULL, ['class'=>'form-control','id'=>'mental_hazard_oth_spec']) !!} 
				    </label>
				</div>
		  	</div>
		  	<div class="col-sm-12">
				<label class="col-sm-6 control-label">Abuse faced by any family member?</label>
				<div class="col-sm-4">
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[mental_abuse_none]',1,(isset($data['mental_abuse_none']) && $data['mental_abuse_none'] == 1) ? true : false) !!} None
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[mental_abuse_sexual]',1,(isset($data['mental_abuse_sexual']) && $data['mental_abuse_sexual'] == 1) ? true : false) !!} Sexual abuse
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[mental_abuse_physical]',1,(isset($data['mental_abuse_physical']) && $data['mental_abuse_physical'] == 1) ? true : false) !!} Physical abuse
					</label>
					<label class="checkbox">
						{!! Form::checkbox('communitynursing[mental_abuse_psych]',1,(isset($data['mental_abuse_psych']) && $data['mental_abuse_psych'] == 1) ? true : false) !!} Psychological abuse
					</label>
					<label class="checkbox" for="mental_abuse_other">
				        {!! Form::checkbox('communitynursing[mental_abuse_oth]',1,(isset($data['mental_abuse_oth']) && $data['mental_abuse_oth'] == 1) ? true : false) !!} Other
				        {!! Form::text('communitynursing[mental_abuse_oth_spec]',isset($data['mental_abuse_oth_spec']) ? $data['mental_abuse_oth_spec'] : NULL, ['class'=>'form-control','id'=>'mental_abuse_oth_spec']) !!} 
				    </label>
				</div>
		  	</div>
	  	</div>
	  </div>
	</div>
</fieldset>

@section('plugin_jsscripts')
	<script type="text/javascript">
		$(document).ready( function(){
			$('.disaster_recently_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#yes_disaster_recently').removeClass('hidden');
				}
				else{
					$('#yes_disaster_recently').addClass('hidden');
				};
			});

			$('.toilet_available_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#num_toilet_spec').removeClass('hidden');
				}
				else{
					$('#num_toilet_spec').addClass('hidden');
				};
			});

			$('.waste_collectors_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#waste_collectors_timely').removeClass('hidden');
				}
				else{
					$('#waste_collectors_timely').addClass('hidden');
				};
			});

			$('.prob_hygiene_mens_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#prob_hygiene_reason_spec').removeClass('hidden');
				}
				else{
					$('#prob_hygiene_reason_spec').addClass('hidden');
				};
			});

			$('.disease_outbreak_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#disease_outbreak_yes_div').removeClass('hidden');
				}
				else{
					$('#disease_outbreak_yes_div').addClass('hidden');
				};
			});

			$('.family_mental_illness_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#medication_div').removeClass('hidden');
				}
				else{
					$('#medication_div').addClass('hidden');
				};
			});


			$("#fp_awareness_where_ecp_select").change(function() {
				if(this.value == 2){
					$('#fp_awareness_where_ecp_oth_div').removeClass('hidden');
				}
				else {
					$('#fp_awareness_where_ecp_oth_div').addClass('hidden');
				}
			});

			$("#housing_select").change(function() {
				if(this.value == 4){
					$('#housing_oth_div').removeClass('hidden');
				}
				else {
					$('#housing_oth_div').addClass('hidden');
				}
			});


			// $("#handwash_select").change(function() {
			// 	if(this.value == 4){
			// 		$('#handwash_oth_div').removeClass('hidden');
			// 	}
			// 	else {
			// 		$('#handwash_oth_div').addClass('hidden');
			// 	}
			// });

			$("#type_toilet_select").change(function() {
				if(this.value == 3){
					$('#type_toilet_oth_div').removeClass('hidden');
				}
				else {
					$('#type_toilet_oth_div').addClass('hidden');
				}
			});



			$("#src_water_supply_select").change(function() {
				if(this.value == 4){
					$('#src_water_supply_oth_div').removeClass('hidden');
				}
				else {
					$('#src_water_supply_oth_div').addClass('hidden');
				}
			});

			$("#waste_disposal_select").change(function() {
				if(this.value == 3){
					$('#waste_disposal_oth_div').removeClass('hidden');
				}
				else {
					$('#waste_disposal_oth_div').addClass('hidden');
				}
			});

			$("#light_source_select").change(function() {
				if(this.value == 3){
					$('#light_source_oth_div').removeClass('hidden');
				}
				else {
					$('#light_source_oth_div').addClass('hidden');
				}
			});


		});
	</script>
@stop