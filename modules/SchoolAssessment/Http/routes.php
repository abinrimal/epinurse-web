<?php

Route::group(['prefix' => 'schoolassessment', 'namespace' => 'Modules\SchoolAssessment\Http\Controllers', 'middleware' => 'auth.access:schoolassessment'], function()
{
	Route::get('/', 'SchoolAssessmentController@index');
	Route::get('/new', 'SchoolAssessmentController@new');
	Route::post('/save', 'SchoolAssessmentController@save');
	Route::get('/update/{id}', 'SchoolAssessmentController@update');
	Route::get('/delete/{id}', 'SchoolAssessmentController@delete');
});