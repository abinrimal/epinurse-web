<?php

use Plugins\MotherAndChild\MotherAndChildModel;
use Modules\Healthcareservices\Entities\Healthcareservices;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\HealthcareRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\User;
use Shine\Plugin;
use Webpatser\Uuid\Uuid;

class MotherAndChildController extends Controller
{
    protected $moduleName = 'Healthcareservices';
    protected $modulePath = 'healthcareservices';

    public function __construct(HealthcareRepository $healthcareRepository) {
        $this->healthcareRepository = $healthcareRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        //no index
    }

    public function save($data)
    {
        $mac_id = $data['macservice_id'];
        $hservice_id = $data['hservice_id'];

        $motherandchild = MotherAndChildModel::where('healthcareservice_id',$hservice_id)->first();

        if(!$motherandchild) {
            $motherandchild =  new MotherAndChildModel;
            $motherandchild->motherandchild_id = IdGenerator::generateId();
            $motherandchild->healthcareservice_id = $hservice_id;
            $motherandchild->uuid = Uuid::generate(4)->string;
            $motherandchild->owner_uuid = isset($data['owner_uuid']) ? $data['owner_uuid'] : NULL;
        }


        $motherandchild->age_menarche = isset($data['age_menarche']) && $data['age_menarche'] != '' ? $data['age_menarche'] : NULL;
        $motherandchild->stay_menarche = isset($data['stay_menarche']) && $data['stay_menarche'] != '' ? $data['stay_menarche'] : NULL;
        $motherandchild->stay_menarche_oth = isset($data['stay_menarche_oth']) ? $data['stay_menarche_oth'] : NULL;

        $motherandchild->sanitary_materials_used_pads = isset($data['sanitary_materials_used_pads']) ? $data['sanitary_materials_used_pads'] : NULL;
        $motherandchild->sanitary_materials_used_tampon = isset($data['sanitary_materials_used_tampon']) ? $data['sanitary_materials_used_tampon'] : NULL;
        $motherandchild->sanitary_materials_used_clothes = isset($data['sanitary_materials_used_clothes']) ? $data['sanitary_materials_used_clothes'] : NULL;
        $motherandchild->sanitary_materials_used_hp = isset($data['sanitary_materials_used_hp']) ? $data['sanitary_materials_used_hp'] : NULL;
        $motherandchild->sanitary_materials_used_mhp = isset($data['sanitary_materials_used_mhp']) ? $data['sanitary_materials_used_mhp'] : NULL;
        $motherandchild->sanitary_materials_used_none = isset($data['sanitary_materials_used_none']) ? $data['sanitary_materials_used_none'] : NULL;

        $motherandchild->material_change_frequency = isset($data['material_change_frequency']) && $data['material_change_frequency'] != '' ? $data['material_change_frequency'] : NULL;
        $motherandchild->material_change_frequency_oth = isset($data['material_change_frequency_oth']) ? $data['material_change_frequency_oth'] : NULL;
        $motherandchild->timeliness_reason = isset($data['timeliness_reason']) ? $data['timeliness_reason'] : NULL;
        $motherandchild->sanitarydisposal = isset($data['sanitarydisposal']) && $data['sanitarydisposal'] != '' ? $data['sanitarydisposal'] : NULL;
        $motherandchild->sanitarydisposal_oth = isset($data['sanitarydisposal_oth']) ? $data['sanitarydisposal_oth'] : NULL;
        $motherandchild->is_pregnant = isset($data['is_pregnant']) ? $data['is_pregnant'] : NULL;
        $motherandchild->age_first_pregnancy = isset($data['age_first_pregnancy']) && $data['age_first_pregnancy'] != '' ? $data['age_first_pregnancy'] : NULL;
        // $motherandchild->past_gravida = isset($data['past_gravida']) && $data['past_gravida'] != '' ? $data['past_gravida'] : NULL;
        // $motherandchild->past_para = isset($data['past_para']) && $data['past_para'] != '' ? $data['past_para'] : NULL;
        // $motherandchild->past_abortion = isset($data['past_abortion']) && $data['past_abortion'] != '' ? $data['past_abortion'] : NULL;
        // $motherandchild->past_living = isset($data['past_living']) && $data['past_living'] != '' ? $data['past_living'] : NULL;
        $motherandchild->cur_gravida = isset($data['cur_gravida']) && $data['cur_gravida'] != '' ? $data['cur_gravida'] : NULL;
        $motherandchild->cur_para = isset($data['cur_para']) && $data['cur_para'] != '' ? $data['cur_para'] : NULL;
        $motherandchild->cur_abortion = isset($data['cur_abortion']) && $data['cur_abortion'] != '' ? $data['cur_abortion'] : NULL;
        $motherandchild->cur_living = isset($data['cur_living']) && $data['cur_living'] != '' ? $data['cur_living'] : NULL;
        $motherandchild->period_gestation = isset($data['period_gestation']) && $data['period_gestation'] != '' ? $data['period_gestation'] : NULL;
        $motherandchild->times_antenatal = isset($data['times_antenatal']) && $data['times_antenatal'] != '' ? $data['times_antenatal'] : NULL;
        $motherandchild->antenatal_services_anc_check_up = isset($data['antenatal_services_anc_check_up']) ? $data['antenatal_services_anc_check_up'] : NULL;
        $motherandchild->antenatal_service_albendazole = isset($data['antenatal_service_albendazole']) ? $data['antenatal_service_albendazole'] : NULL;
        $motherandchild->antenatal_service_td_immunization = isset($data['antenatal_service_td_immunization']) ? $data['antenatal_service_td_immunization'] : NULL;
        $motherandchild->antenatal_service_irontabs = isset($data['antenatal_service_irontabs']) ? $data['antenatal_service_irontabs'] : NULL;
        $motherandchild->antenatal_service_pmtct = isset($data['antenatal_service_pmtct']) ? $data['antenatal_service_pmtct'] : NULL;
        $motherandchild->antenatal_service_others = isset($data['antenatal_service_others']) ? $data['antenatal_service_others'] : NULL;
        $motherandchild->antenatal_service_specify = isset($data['antenatal_service_specify']) ? $data['antenatal_service_specify'] : NULL;
        $motherandchild->healthseeking_behavior = isset($data['healthseeking_behavior']) && $data['healthseeking_behavior'] != '' ? $data['healthseeking_behavior'] : NULL;
        $motherandchild->healthseeking_behavior_oth_spec = isset($data['healthseeking_behavior_oth_spec']) ? $data['healthseeking_behavior_oth_spec'] : NULL;
        
        $med_to_save = array();
        if(isset($data['medicines']))
        {
            for($i=0;$i<count($data['medicines']['name']);$i++) {
                $med_to_save[] = ['medicine'=>$data['medicines']['name'][$i],'prescribed'=>$data['medicines']['prescribed'][$i],'stock'=>$data['medicines']['medicine_stock'][$i]];
            }
        }
        $motherandchild->medicines = json_encode($med_to_save);

        $motherandchild->age_young_months = isset($data['age_young_months']) && $data['age_young_months'] != '' ? $data['age_young_months'] : NULL;
        $motherandchild->type_of_delivery = isset($data['type_of_delivery']) && $data['type_of_delivery'] != '' ? $data['type_of_delivery'] : NULL;
        $motherandchild->age_gap_kids = isset($data['age_gap_kids']) && $data['age_gap_kids'] != '' ? $data['age_gap_kids'] : NULL;
        $motherandchild->delivery_place = isset($data['delivery_place']) && $data['delivery_place'] != '' ? $data['delivery_place'] : NULL;
        $motherandchild->delivery_place_oth_spec = isset($data['delivery_place_oth_spec']) ? $data['delivery_place_oth_spec'] : NULL;
        $motherandchild->delivery_complication = isset($data['delivery_complication']) ? $data['delivery_complication'] : NULL;
        $motherandchild->delivery_complication_spec = isset($data['delivery_complication_spec']) ? $data['delivery_complication_spec'] : NULL;
        $motherandchild->post_natal_pe = isset($data['post_natal_pe']) ? $data['post_natal_pe'] : NULL;
        $motherandchild->post_natal_counsel_b_f = isset($data['post_natal_counsel_b_f']) ? $data['post_natal_counsel_b_f'] : NULL;
        $motherandchild->post_natal_counsel_f_p = isset($data['post_natal_counsel_f_p']) ? $data['post_natal_counsel_f_p'] : NULL;
        $motherandchild->post_natal_investigations = isset($data['post_natal_investigations']) ? $data['post_natal_investigations'] : NULL;
        $motherandchild->post_natal_iron_tablets = isset($data['post_natal_iron_tablets']) ? $data['post_natal_iron_tablets'] : NULL;
        $motherandchild->post_natal_vitamin_a = isset($data['post_natal_vitamin_a']) ? $data['post_natal_vitamin_a'] : NULL;
        $motherandchild->post_natal_oth = isset($data['post_natal_oth']) ? $data['post_natal_oth'] : NULL;
        $motherandchild->post_natal_oth_spec = isset($data['post_natal_oth_spec']) ? $data['post_natal_oth_spec'] : NULL;
        $motherandchild->pnc_followup_none = isset($data['pnc_followup_none']) ? $data['pnc_followup_none'] : NULL;
        $motherandchild->pnc_followup_within24hr = isset($data['pnc_followup_within24hr']) ? $data['pnc_followup_within24hr'] : NULL;
        $motherandchild->pnc_followup_at3rd_day = isset($data['pnc_followup_at3rd_day']) ? $data['pnc_followup_at3rd_day'] : NULL;
        $motherandchild->pnc_followup_at7th_day = isset($data['pnc_followup_at7th_day']) ? $data['pnc_followup_at7th_day'] : NULL;
        $motherandchild->pnc_followup_at28th_day = isset($data['pnc_followup_at28th_day']) ? $data['pnc_followup_at28th_day'] : NULL;
        $motherandchild->pnc_followup_at45th_day = isset($data['pnc_followup_at45th_day']) ? $data['pnc_followup_at45th_day'] : NULL;
        $motherandchild->pnc_followup_others = isset($data['pnc_followup_others']) ? $data['pnc_followup_others'] : NULL;
        $motherandchild->pnc_followup_specify = isset($data['pnc_followup_specify']) ? $data['pnc_followup_specify'] : NULL;

        $motherandchild->food_after_delivery = isset($data['food_after_delivery']) ? $data['food_after_delivery'] : NULL;
        $motherandchild->food_after_delivery_quality = isset($data['food_after_delivery_quality']) ? $data['food_after_delivery_quality'] : NULL;
        $motherandchild->food_after_delivery_frequency = isset($data['food_after_delivery_frequency']) ? $data['food_after_delivery_frequency'] : NULL;
        $motherandchild->food_after_delivery_distribution = isset($data['food_after_delivery_distribution']) ? $data['food_after_delivery_distribution'] : NULL;
        $motherandchild->nutritious_food = isset($data['nutritious_food']) ? $data['nutritious_food'] : NULL;
        $motherandchild->pre_lactating = isset($data['pre_lactating']) ? $data['pre_lactating'] : NULL;
        $motherandchild->nutritious_food_spec = isset($data['nutritious_food_spec']) ? $data['nutritious_food_spec'] : NULL;
        $motherandchild->dietary_restriction = isset($data['dietary_restriction']) ? $data['dietary_restriction'] : NULL;
        $motherandchild->dietary_restriction_spec = isset($data['dietary_restriction_spec']) ? $data['dietary_restriction_spec'] : NULL;
        $motherandchild->is_breastfed = isset($data['is_breastfed']) ? $data['is_breastfed'] : NULL;
        $motherandchild->is_breastfed_early = isset($data['is_breastfed_early']) ? $data['is_breastfed_early'] : NULL;
        $motherandchild->is_breastfed_exclusive = isset($data['is_breastfed_exclusive']) ? $data['is_breastfed_exclusive'] : NULL;
        $motherandchild->is_breastfed_extended = isset($data['is_breastfed_extended']) ? $data['is_breastfed_extended'] : NULL;
        $motherandchild->breastfeed_times = isset($data['breastfeed_times']) && $data['breastfeed_times'] != '' ? $data['breastfeed_times'] : NULL;
        $motherandchild->clean_hands_breastfeed = isset($data['clean_hands_breastfeed']) ? $data['clean_hands_breastfeed'] : NULL;
        $motherandchild->handwashing_facility_breastfeed = isset($data['handwashing_facility_breastfeed']) ? $data['handwashing_facility_breastfeed'] : NULL;
        $motherandchild->fp_counseling = isset($data['fp_counseling']) ? $data['fp_counseling'] : NULL;
        $motherandchild->fp_use = isset($data['fp_use']) ? $data['fp_use'] : NULL;
        $motherandchild->permanent_vasectomy = isset($data['permanent_vasectomy']) ? $data['permanent_vasectomy'] : NULL;
        $motherandchild->permanent_minilap = isset($data['permanent_minilap']) ? $data['permanent_minilap'] : NULL;
        $motherandchild->temporary_natural = isset($data['temporary_natural']) ? $data['temporary_natural'] : NULL;
        $motherandchild->temporary_condom = isset($data['temporary_condom']) ? $data['temporary_condom'] : NULL;
        $motherandchild->temporary_depo = isset($data['temporary_depo']) ? $data['temporary_depo'] : NULL;
        $motherandchild->temporary_pills = isset($data['temporary_pills']) ? $data['temporary_pills'] : NULL;
        $motherandchild->temporary_iucd = isset($data['temporary_iucd']) ? $data['temporary_iucd'] : NULL;
        $motherandchild->temporary_implant = isset($data['temporary_implant']) ? $data['temporary_implant'] : NULL;
        $motherandchild->fp_info_src = isset($data['fp_info_src']) && $data['fp_info_src'] != '' ? $data['fp_info_src'] : NULL;
        $motherandchild->child_id = isset($data['child_id']) ? $data['child_id'] : NULL;
        $motherandchild->child_name = isset($data['child_name']) ? $data['child_name'] : NULL;
        $motherandchild->child_sex = isset($data['child_sex']) ? $data['child_sex'] : NULL;
        $motherandchild->child_birthdate_ad = isset($data['child_birthdate_ad']) ? $data['child_birthdate_ad'] : NULL;
        $motherandchild->child_birthdate_bs = isset($data['child_birthdate_bs']) ? $data['child_birthdate_bs'] : NULL;
        $motherandchild->child_weight_birth = isset($data['child_weight_birth']) && $data['child_weight_birth'] != '' ? $data['child_weight_birth'] : NULL;
        $motherandchild->child_weight_current = isset($data['child_weight_current']) && $data['child_weight_current'] != '' ? $data['child_weight_current'] : NULL;
        $motherandchild->child_height = isset($data['child_height']) && $data['child_height'] != '' ? $data['child_height'] : NULL;
        $motherandchild->mid_arm_circumference = isset($data['mid_arm_circumference']) && $data['mid_arm_circumference'] != '' ? $data['mid_arm_circumference'] : NULL;
        $motherandchild->congenital_anomaly = isset($data['congenital_anomaly']) ? $data['congenital_anomaly'] : NULL;
        $motherandchild->congenital_anomaly_spec = isset($data['congenital_anomaly_spec']) ? $data['congenital_anomaly_spec'] : NULL;
        $motherandchild->health_condition = isset($data['health_condition']) ? $data['health_condition'] : NULL;
        $motherandchild->vitamin_k = isset($data['vitamin_k']) ? $data['vitamin_k'] : NULL;
        $motherandchild->breastfeed_start = isset($data['breastfeed_start']) && $data['breastfeed_start'] != '' ? $data['breastfeed_start'] : NULL;
        $motherandchild->bf_times = isset($data['bf_times']) && $data['bf_times'] != '' ? $data['bf_times'] : NULL;
        $motherandchild->exclusive_breastfeeding = isset($data['exclusive_breastfeeding']) ? $data['exclusive_breastfeeding'] : NULL;
        $motherandchild->exclusive_breastfeeding_spec = isset($data['exclusive_breastfeeding_spec']) ? $data['exclusive_breastfeeding_spec'] : NULL;
        $motherandchild->age_stop_bf_months = isset($data['age_stop_bf_months']) ? $data['age_stop_bf_months'] : NULL;
        $motherandchild->reason_stopbf = isset($data['reason_stopbf']) ? $data['reason_stopbf'] : NULL;
        $motherandchild->reason_stopbf_spec = isset($data['reason_stopbf_spec']) ? $data['reason_stopbf_spec'] : NULL;
        $motherandchild->aware_bf = isset($data['aware_bf']) ? $data['aware_bf'] : NULL;
        $motherandchild->solid_feeding = isset($data['solid_feeding']) ? $data['solid_feeding'] : NULL;
        $motherandchild->food_supplement_rice_pudding = isset($data['food_supplement_rice_pudding']) ? $data['food_supplement_rice_pudding'] : NULL;
        $motherandchild->food_supplement_jaulo = isset($data['food_supplement_jaulo']) ? $data['food_supplement_jaulo'] : NULL;
        $motherandchild->food_supplement_lito = isset($data['food_supplement_lito']) ? $data['food_supplement_lito'] : NULL;
        $motherandchild->food_supplement_cerelac = isset($data['food_supplement_cerelac']) ? $data['food_supplement_cerelac'] : NULL;
        $motherandchild->food_supplement_oth = isset($data['food_supplement_oth']) ? $data['food_supplement_oth'] : NULL;
        $motherandchild->food_supplement_oth_spec = isset($data['food_supplement_oth_spec']) ? $data['food_supplement_oth_spec'] : NULL;
        $motherandchild->no_food = isset($data['no_food']) && $data['no_food'] != '' ? $data['no_food'] : NULL;
        $motherandchild->no_food_oth = isset($data['no_food_oth']) ? $data['no_food_oth'] : NULL;
        $motherandchild->child_food = isset($data['child_food']) ? $data['child_food'] : NULL;
        $motherandchild->child_food_quality = isset($data['child_food_quality']) ? $data['child_food_quality'] : NULL;
        $motherandchild->child_food_frequency = isset($data['child_food_frequency']) ? $data['child_food_frequency'] : NULL;
        $motherandchild->child_food_distribution = isset($data['child_food_distribution']) ? $data['child_food_distribution'] : NULL;
        $motherandchild->injury_type_falls = isset($data['injury_type_falls']) ? $data['injury_type_falls'] : NULL;
        $motherandchild->injury_type_drowning = isset($data['injury_type_drowning']) ? $data['injury_type_drowning'] : NULL;
        $motherandchild->injury_type_burnsscald = isset($data['injury_type_burnsscald']) ? $data['injury_type_burnsscald'] : NULL;
        $motherandchild->injury_type_poisoning = isset($data['injury_type_poisoning']) ? $data['injury_type_poisoning'] : NULL;
        $motherandchild->injury_type_suffocatingchokingaspiration = isset($data['injury_type_suffocatingchokingaspiration']) ? $data['injury_type_suffocatingchokingaspiration'] : NULL;
        $motherandchild->injury_type_cut = isset($data['injury_type_cut']) ? $data['injury_type_cut'] : NULL;
        $motherandchild->injury_type_oth = isset($data['injury_type_oth']) ? $data['injury_type_oth'] : NULL;
        $motherandchild->injury_types_oth_spec = isset($data['injury_types_oth_spec']) ? $data['injury_types_oth_spec'] : NULL;
        $motherandchild->injury_cause_unsafe_home = isset($data['injury_cause_unsafe_home']) ? $data['injury_cause_unsafe_home'] : NULL;
        $motherandchild->injury_cause_no_supervision = isset($data['injury_cause_no_supervision']) ? $data['injury_cause_no_supervision'] : NULL;
        $motherandchild->injury_cause_busy_mother = isset($data['injury_cause_busy_mother']) ? $data['injury_cause_busy_mother'] : NULL;
        $motherandchild->injury_cause_slippery_floor = isset($data['injury_cause_slippery_floor']) ? $data['injury_cause_slippery_floor'] : NULL;
        $motherandchild->injury_cause_oth = isset($data['injury_cause_oth']) ? $data['injury_cause_oth'] : NULL;
        $motherandchild->injury_cause_oth_spec = isset($data['injury_cause_oth_spec']) ? $data['injury_cause_oth_spec'] : NULL;
        $motherandchild->age_bcg_at_birth = isset($data['age_bcg_at_birth']) ? $data['age_bcg_at_birth'] : NULL;
        $motherandchild->age_dpt_hepb_hib6_weeks = isset($data['age_dpt_hepb_hib6_weeks']) ? $data['age_dpt_hepb_hib6_weeks'] : NULL;
        $motherandchild->age_dpt_hepb_hib10_weeks = isset($data['age_dpt_hepb_hib10_weeks']) ? $data['age_dpt_hepb_hib10_weeks'] : NULL;
        $motherandchild->age_dpt_hepb_hib14_weeks = isset($data['age_dpt_hepb_hib14_weeks']) ? $data['age_dpt_hepb_hib14_weeks'] : NULL;
        $motherandchild->age_opv6_weeks = isset($data['age_opv6_weeks']) ? $data['age_opv6_weeks'] : NULL;
        $motherandchild->age_opv10_weeks = isset($data['age_opv10_weeks']) ? $data['age_opv10_weeks'] : NULL;
        $motherandchild->age_opv14_weeks = isset($data['age_opv14_weeks']) ? $data['age_opv14_weeks'] : NULL;
        $motherandchild->age_pcv6_weeks = isset($data['age_pcv6_weeks']) ? $data['age_pcv6_weeks'] : NULL;
        $motherandchild->age_pcv10_weeks = isset($data['age_pcv10_weeks']) ? $data['age_pcv10_weeks'] : NULL;
        $motherandchild->age_pcv9_months = isset($data['age_pcv9_months']) ? $data['age_pcv9_months'] : NULL;
        $motherandchild->age_ipv14_weeks = isset($data['age_ipv14_weeks']) ? $data['age_ipv14_weeks'] : NULL;
        $motherandchild->age_measles_rubella9_months = isset($data['age_measles_rubella9_months']) ? $data['age_measles_rubella9_months'] : NULL;
        $motherandchild->age_measles_rubella15_months = isset($data['age_measles_rubella15_months']) ? $data['age_measles_rubella15_months'] : NULL;
        $motherandchild->age_japanese_encephalitis12_months = isset($data['age_japanese_encephalitis12_months']) ? $data['age_japanese_encephalitis12_months'] : NULL;
        $motherandchild->immu_remarks_bcg = isset($data['immu_remarks_bcg']) ? $data['immu_remarks_bcg'] : NULL;
        $motherandchild->immu_remarks_dpt_hepb_hib = isset($data['immu_remarks_dpt_hepb_hib']) ? $data['immu_remarks_dpt_hepb_hib'] : NULL;
        $motherandchild->immu_remarks_opv = isset($data['immu_remarks_opv']) ? $data['immu_remarks_opv'] : NULL;
        $motherandchild->immu_remarks_pcv = isset($data['immu_remarks_pcv']) ? $data['immu_remarks_pcv'] : NULL;
        $motherandchild->immu_remarks_ipv = isset($data['immu_remarks_ipv']) ? $data['immu_remarks_ipv'] : NULL;
        $motherandchild->immu_remarks_measles_rubella = isset($data['immu_remarks_measles_rubella']) ? $data['immu_remarks_measles_rubella'] : NULL;
        $motherandchild->immu_remarks_japanese_encephalitis = isset($data['immu_remarks_japanese_encephalitis']) ? $data['immu_remarks_japanese_encephalitis'] : NULL;
        $motherandchild->advise_bf = isset($data['advise_bf']) ? $data['advise_bf'] : NULL;
        $motherandchild->advise_dh = isset($data['advise_dh']) ? $data['advise_dh'] : NULL;
        $motherandchild->advise_tt = isset($data['advise_tt']) ? $data['advise_tt'] : NULL;
        $motherandchild->advise_cf = isset($data['advise_cf']) ? $data['advise_cf'] : NULL;
        $motherandchild->advise_ap = isset($data['advise_ap']) ? $data['advise_ap'] : NULL;
        $motherandchild->advise_physical = isset($data['advise_physical']) ? $data['advise_physical'] : NULL;
        $motherandchild->advise_verbal = isset($data['advise_verbal']) ? $data['advise_verbal'] : NULL;
        $motherandchild->advise_social = isset($data['advise_social']) ? $data['advise_social'] : NULL;
        $motherandchild->advise_spiritual = isset($data['advise_spiritual']) ? $data['advise_spiritual'] : NULL;
        $motherandchild->advise_motor = isset($data['advise_motor']) ? $data['advise_motor'] : NULL;
        $motherandchild->advise_intellectual = isset($data['advise_intellectual']) ? $data['advise_intellectual'] : NULL;
        $motherandchild->advise_emotional = isset($data['advise_emotional']) ? $data['advise_emotional'] : NULL;

        $motherandchild->save();
    }
}
