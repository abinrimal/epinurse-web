
<?php
    $father_firstname = NULL;
    $father_middlename = NULL;
    $father_lastname = NULL;
    $father_suffix = NULL;
    $father_alive = NULL;
    $mother_firstname = NULL;
    $mother_middlename = NULL;
    $mother_lastname = NULL;
    $mother_alive = NULL;
    $ctr_householdmembers_lt10yrs = NULL;
    $ctr_householdmembers_gt10yrs = NULL;
    $family_folder_name = NULL;

    if($plugdata) {
        $family = $plugdata;

        $father_firstname = isset($family->father_firstname) ? $family->father_firstname : NULL;
        $father_middlename = isset($family->father_middlename) ? $family->father_middlename : NULL;
        $father_lastname = isset($family->father_lastname) ? $family->father_lastname : NULL;
        $father_suffix = isset($family->father_suffix) ? $family->father_suffix : NULL;
        $father_alive = isset($family->father_alive) ? $family->father_alive: NULL;
        $mother_firstname = isset($family->mother_firstname) ? $family->mother_firstname : NULL;
        $mother_middlename = isset($family->mother_middlename) ? $family->mother_middlename : NULL;
        $mother_lastname = isset($family->mother_lastname) ? $family->mother_lastname : NULL;
        $mother_alive = isset($family->mother_alive) ? $family->mother_alive : NULL;
        $ctr_householdmembers_lt10yrs = isset($family->ctr_householdmembers_lt10yrs) ? $family->ctr_householdmembers_lt10yrs : NULL;
        $ctr_householdmembers_gt10yrs = isset($family->ctr_householdmembers_gt10yrs) ? $family->ctr_householdmembers_gt10yrs : NULL;
        $family_folder_name = isset($family->family_folder_name) ? $family->family_folder_name : NULL;
    }

    $method = 'save';
    if($plugdata) {
        $method = 'update';
    }
?>
<div class="tab-content">
    @if (Session::has('flash_message'))
            <div class="alert {{Session::get('flash_type') }}">{{ Session::get('flash_message') }}</div>
        @endif
 <div class="tab-pane step active" id="family">
    <fieldset>
         {!! Form::model($patient, array('url' => 'plugin/call/ShineLab_Family/family/'.$method.'/'.$patient->patient_id,'class'=>'form-horizontal')) !!}
        <fieldset>
            <input type="hidden" name="id" value="{{ $plugdata->id or NULL }}" />
            <input type="hidden" name="patient_id" value="{{ $patient->patient_id }}" />
            <legend>Family Information</legend>

                <label class="col-sm-2 control-label">Number of Households Members</label>
                <div class="col-sm-4">
                    <div class="form-group">
                    <label class="sublabel">Less than 10 years old</label>
                    {!! Form::text('ctr_householdmembers_lt10yrs', $ctr_householdmembers_lt10yrs, array('class' => 'form-control autowidth numericonly', 'name'=>'ctr_householdmembers_lt10yrs','placeholder'=>'&lt; 10 years')) !!}
                    </div>
                    <div class="form-group">
                    <label class="sublabel">More than 10 years old</label>
                    {!! Form::text('ctr_householdmembers_gt10yrs', $ctr_householdmembers_gt10yrs, array('class' => 'form-control autowidth numericonly', 'name'=>'ctr_householdmembers_gt10yrs','placeholder'=>'&gt; 10 years')) !!}
                    </div>
                </div>
                <label class="col-sm-2 control-label">Family Folder</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control alphanumeric" name="family_folder_name" placeholder="Enter Last Name of the patient" value="{{ $family_folder_name }}">
                </div>

        </fieldset>
        <fieldset>
            <legend>Parents Information</legend>
            <div class="form-group">
                <div class="col-md-6"><strong>Father Details</strong><hr /></div>
                <div class="col-md-6"><strong>Mother Details</strong><hr /></div>

                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                    <div class="radio-inline">
                        <label>
                          <input type="radio" name="father_alive" id="" value="1" <?php if($father_alive == 1) echo "checked='checked'"; ?>> Alive
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                          <input type="radio" name="father_alive" id="" value="0" <?php if($father_alive == 0) echo "checked='checked'"; ?>> Deceased
                        </label>
                    </div>
                </div>

                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                    <div class="radio-inline">
                        <label>
                          <input type="radio" name="mother_alive" id="" value="1" <?php if($mother_alive == 1) echo "checked='checked'"; ?>> Alive
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                          <input type="radio" name="mother_alive" id="" value="0" <?php if($mother_alive == 0) echo "checked='checked'"; ?>> Deceased
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <label class="col-sm-4 control-label">Firstname</label>
                    <div class="col-sm-8">
                        {!! Form::text('father_firstname', $father_firstname, array('class' => 'form-control', 'name'=>'father_firstname')) !!}
                    </div>

                    <label class="col-sm-4 control-label">Middlename</label>
                    <div class="col-sm-8">
                        {!! Form::text('father_middlename', $father_middlename, array('class' => 'form-control', 'name'=>'father_middlename')) !!}
                    </div>

                    <label class="col-sm-4 control-label">Lastname</label>
                    <div class="col-sm-8">
                        {!! Form::text('father_lastname', $father_lastname, array('class' => 'form-control', 'name'=>'father_lastname')) !!}
                    </div>

                    <label class="col-sm-4 control-label">Suffix</label>
                    <div class="col-sm-8">
                        <div class="btn-group toggler" data-toggle="buttons">
                          <label class="btn btn-default btn-sm @if(isset($father_suffix) AND $father_suffix == 'SR') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="father_suffix" id="" autocomplete="off" value="SR"> Sr.
                          </label>
                          <label class="btn btn-default btn-sm @if(isset($father_suffix) AND $father_suffix == 'JR') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="father_suffix" id="" autocomplete="off" value="JR"> Jr.
                          </label>
                          <label class="btn btn-default btn-sm @if(isset($father_suffix) AND $father_suffix == 'II') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="father_suffix" id="" autocomplete="off" value="II"> II
                          </label>
                          <label class="btn btn-default btn-sm @if(isset($father_suffix) AND $father_suffix == 'III') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="father_suffix" id="" autocomplete="off" value="III"> III
                          </label>
                          <label class="btn btn-default btn-sm @if(isset($father_suffix) AND $father_suffix == 'IV') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="father_suffix" id="" autocomplete="off" value="IV"> IV
                          </label>
                          <label class="btn btn-default btn-sm @if(isset($father_suffix) AND $father_suffix == 'V') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="father_suffix" id="" autocomplete="off" value="V"> V
                          </label>
                          <label class="btn btn-default btn-sm">
                            <i class="fa fa-check"></i> <input type="radio" name="father_suffix" id="" autocomplete="off" value="NA"> None
                          </label>
                        </div>
                    </div>

                </div>
                <div class="col-sm-6">
                        <label class="col-sm-4 control-label">Firstname</label>
                        <div class="col-sm-8">
                            {!! Form::text('mother_firstname', $mother_firstname, array('class' => 'form-control', 'name'=>'mother_firstname')) !!}
                        </div>

                        <label class="col-sm-4 control-label">Middlename</label>
                        <div class="col-sm-8">
                            {!! Form::text('mother_middlename', $mother_middlename, array('class' => 'form-control', 'name'=>'mother_middlename')) !!}
                        </div>

                        <label class="col-sm-4 control-label">Lastname</label>
                        <div class="col-sm-8">
                            {!! Form::text('mother_lastname', $mother_lastname, array('class' => 'form-control', 'name'=>'mother_lastname')) !!}
                        </div>

                </div>
            </div>

        </fieldset>

         <div class="form-group pull-right ">
            <button type="button" class="btn btn-primary" onclick="location.href='{{ url('patients/view/'.$patient->patient_id) }}'">Close</button>
            <button type="submit" value="submit" class="btn btn-success">{{ ucfirst($method) }}</button>
        </div>
        {!! Form::close() !!}
    </fieldset>
    <br clear="all" />
  </div><!-- /.tab-pane -->
</div>
