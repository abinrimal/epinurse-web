<?php namespace ShineOS\Core\PHIE\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\PHIE\Http\Controllers\PHIEController;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Request, Input, DateTime, Session;

class PHIEExaminationController extends Controller {


    public function __construct() {
        $this->vitalsRequired = array('Pat_Facility_No',
                                        'Encounter_ID',
                                        'Diastolic_Measure',
                                        'Systolic_Measure',
                                        'Heart_Rate',
                                        'Respiratory_Rate',
                                        'Height',
                                        'Weight',
                                        'Waist',
                                        'BMI',
                                        'BMI_Category');
    }


    public function healthcarebyFacilityId($facility_id, $type=NULL, $compareToDate, $data_Encounter_ID=NULL) {
        $PHIEController = new PHIEController;
        $healthcare = Healthcareservices::query();
        if(!empty($data_Encounter_ID)) {
            $healthcare = $healthcare->whereIn('healthcare_services.healthcareservice_id', $data_Encounter_ID);
        } else {
            $arr_param_type_id = array();
            $param_type_id = $PHIEController->PHIESync_list($facility_id, NULL, 'EncounterData');
            foreach ($param_type_id as $key => $value) {
                $arr_param_type_id[] = json_decode($value->param_type_id);
            }
            $healthcare = $healthcare->whereIn('healthcare_services.healthcareservice_id', array_unique(array_flatten($arr_param_type_id)));
        }

        $healthcare = $healthcare->join('facility_patient_user','healthcare_services.facilitypatientuser_id','=','facility_patient_user.facilitypatientuser_id')
                ->join('facility_user','facility_patient_user.facilityuser_id','=','facility_user.facilityuser_id')
                ->join('facilities','facilities.facility_id','=','facility_user.facility_id')
                ->where('facilities.facility_id', $facility_id)
                ->where('healthcare_services.updated_at', '>', $compareToDate)
                ->whereYear('healthcare_services.encounter_datetime', '>', 2016)
                ->whereYear('healthcare_services.encounter_datetime', '=', 2017)
                ->whereMonth('healthcare_services.encounter_datetime', '<', 4)
                ->join('patients','patients.patient_id','=','facility_patient_user.patient_id')
                ->where('patients.deleted_at','=',NULL)
                ->where('facility_patient_user.deleted_at','=',NULL)
                ->with($type)->has($type)
                ->orderBy('healthcare_services.encounter_datetime', 'DESC')
                ->get();

        return $healthcare;
    }

/**
 * Physical Examination
 */

    /**
     * Vital Signs
     */
    public function PHIE_PE_VitalSigns($facility_id, $EncounterData) {
        $param_type = 'VitalSignData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_PE_VitalSigns = [];
        $vitalsData = $this->healthcarebyFacilityId($facility_id,'VitalsPhysical', $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if($vitalsData) {
            $c = count($vitalsData); $ct = 0;
            $PHIEController->install_log("Processing ".$param_type.".");

            foreach ($vitalsData as $pkey => $value) {
                if($value->patient_id AND $value->healthcareservice_id) {
                    $BMI_result = NULL;
                    if($value->VitalsPhysical->weight!=NULL AND $value->VitalsPhysical->weight>0 AND $value->VitalsPhysical->height!=NULL AND $value->VitalsPhysical->height>0) {
                        $BMI_result = $value->VitalsPhysical->weight / ($value->VitalsPhysical->height/100*$value->VitalsPhysical->height/100);
                          $BMI_result = round((($BMI_result * 100) / 100),2);
                    }

                    $PHIE_PE_VitalSigns[$pkey]['Pat_Facility_No'] = $value->patient_id;
                    $PHIE_PE_VitalSigns[$pkey]['Encounter_ID'] = $value->healthcareservice_id;
                    $PHIE_PE_VitalSigns[$pkey]['Systolic_Measure'] = !is_null($value->VitalsPhysical->bloodpressure_systolic) ?  $value->VitalsPhysical->bloodpressure_systolic : "";
                    $PHIE_PE_VitalSigns[$pkey]['Diastolic_Measure'] = !is_null($value->VitalsPhysical->bloodpressure_diastolic) ?  $value->VitalsPhysical->bloodpressure_diastolic : "";
                    $PHIE_PE_VitalSigns[$pkey]['Heart_Rate'] = !is_null($value->VitalsPhysical->heart_rate) ?  $value->VitalsPhysical->heart_rate : "";
                    $PHIE_PE_VitalSigns[$pkey]['Respiratory_Rate'] = !is_null($value->VitalsPhysical->respiratory_rate) ?  $value->VitalsPhysical->respiratory_rate : "";
                    $PHIE_PE_VitalSigns[$pkey]['Height'] = !is_null($value->VitalsPhysical->height) ?  $value->VitalsPhysical->height : "";
                    $PHIE_PE_VitalSigns[$pkey]['Weight'] = !is_null($value->VitalsPhysical->weight) ?  $value->VitalsPhysical->weight : "";
                    $PHIE_PE_VitalSigns[$pkey]['Waist'] = !is_null($value->VitalsPhysical->waist) ?  $value->VitalsPhysical->waist : "";
                    $PHIE_PE_VitalSigns[$pkey]['BMI'] = strval($BMI_result);
                    $PHIE_PE_VitalSigns[$pkey]['BMI_Category'] = !is_null($value->VitalsPhysical->BMI_category) ?  $value->VitalsPhysical->BMI_category : "";
                }
            }

            //unset the array if incomplete data
            $out = false;
            if(count($PHIE_PE_VitalSigns)) {
                foreach ($PHIE_PE_VitalSigns as $result_key => $result_value) {
                    foreach ($result_value as $val_key => $val_value) {
                        $exists = in_array($val_key, $this->vitalsRequired);
                        if($exists) {
                            if($val_value == NULL) {
                                $this_errors[] = $val_key;
                                $out = true;
                            }
                        }
                    }
                    if($out == true) {
                        $fullname = returnFullNameOfPatientID($PHIE_PE_VitalSigns[$result_key]['Pat_Facility_No']);
                        $encounter_date = getCompleteHealthRecordByServiceID($PHIE_PE_VitalSigns[$result_key]['Encounter_ID']);
                        $PHIE_Excluded_Encounter[] = [ 'id'=>$result_key, 'Encounter Date'=>$encounter_date[0]->encounter_datetime, 'Patient ID' => $PHIE_PE_VitalSigns[$result_key]['Pat_Facility_No'], 'Healthcare ID' => $PHIE_PE_VitalSigns[$result_key]['Encounter_ID'], 'Pateint Name'=>$fullname, 'Incomplete Data:'=>$this_errors];
                        $out = false;
                        $this_errors = [];
                        unset($PHIE_PE_VitalSigns[$result_key]);
                    }
                }
            }
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_PE_VitalSigns) ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $PHIE_PE_VitalSigns;
            $filer = $PHIE_Excluded_Encounter;
            $result = $PHIEController->sendArray($data,$param_type,NULL,$filer);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

    /**
     * Skin
     */
    public function PHIE_PE_Skin($facility_id, $EncounterData) {
        $param_type = 'SkinData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_PE_Skin = [];
        $ExaminationData = $this->healthcarebyFacilityId($facility_id,'Examination', $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if($ExaminationData) {
            $c = count($ExaminationData); $ct = 0;
            $PHIEController->install_log("Processing ".$param_type.".");

            foreach ($ExaminationData as $pkey => $value) {
                if($value->patient_id AND $value->healthcareservice_id) {
                    foreach ($value->Examination as $key => $pvalue) {
                        $PHIEController = new PHIEController;
                        $PHIE_PE_Skin[$pkey]['Pat_Facility_No'] = $value->patient_id;
                        $PHIE_PE_Skin[$pkey]['Encounter_ID'] = $value->healthcareservice_id;
                        $PHIE_PE_Skin[$pkey]['Pallor'] = !is_null($pvalue->Pallor) ?  $PHIEController->ref_logical2($pvalue->Pallor) : "U";
                        $PHIE_PE_Skin[$pkey]['Rashes'] = !is_null($pvalue->Rashes) ?  $PHIEController->ref_logical2($pvalue->Rashes) : "U";
                        $PHIE_PE_Skin[$pkey]['Jaundice'] = !is_null($pvalue->Jaundice) ?  $PHIEController->ref_logical2($pvalue->Jaundice) : "U";
                        $PHIE_PE_Skin[$pkey]['Good_Skin_Turgor'] = !is_null($pvalue->Good_Skin_Turgor) ?  $PHIEController->ref_logical2($pvalue->Good_Skin_Turgor) : "U";
                        $PHIE_PE_Skin[$pkey]['Others'] = !is_null($pvalue->skin_others) ?  $pvalue->skin_others : NULL;
                    }
                }
            }
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_PE_Skin) ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
                // return $PHIE_PE_Skin;
                $data[$param_type] = $PHIE_PE_Skin;
                $result = $PHIEController->sendArray($data,$param_type);
                return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

    /**
     * Heent
     */

    public function PHIE_PE_HEENT($facility_id, $EncounterData) {
        $param_type = 'HeentData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_PE_HEENT = [];
        $ExaminationData = $this->healthcarebyFacilityId($facility_id,'Examination', $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if($ExaminationData) {
            $c = count($ExaminationData); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($ExaminationData as $pkey => $value) {
                //there should be patient and healthcare
                if($value->patient_id AND $value->healthcareservice_id){
                foreach ($value->Examination as $key => $pvalue) {
                    $PHIEController = new PHIEController;

                    $PHIE_PE_HEENT[$pkey]['Pat_Facility_No'] = $value->patient_id;
                    $PHIE_PE_HEENT[$pkey]['Encounter_ID'] = $value->healthcareservice_id;
                    $PHIE_PE_HEENT[$pkey]['Anicteric_Sclerae'] = !is_null($pvalue->Anicteric_Sclerae) ?  $PHIEController->ref_logical2($pvalue->Anicteric_Sclerae) : "U";
                    $PHIE_PE_HEENT[$pkey]['Pupils'] = !is_null($pvalue->Pupils) ?  $PHIEController->ref_logical2($pvalue->Pupils) : "U";
                    $PHIE_PE_HEENT[$pkey]['Aural_Discharge'] = !is_null($pvalue->Aural_Discharge) ?  $PHIEController->ref_logical2($pvalue->Aural_Discharge) : "U";
                    $PHIE_PE_HEENT[$pkey]['Intact_Tympanic_Membrane'] = !is_null($pvalue->Intact_Tympanic_Membrane) ?  $PHIEController->ref_logical2($pvalue->Intact_Tympanic_Membrane) : "U";
                    $PHIE_PE_HEENT[$pkey]['Alar_Flaring'] = !is_null($pvalue->Alar_Flaring) ?  $PHIEController->ref_logical2($pvalue->Alar_Flaring) : "U";
                    $PHIE_PE_HEENT[$pkey]['Nasal_Discharge'] = !is_null($pvalue->Nasal_Discharge) ?  $PHIEController->ref_logical2($pvalue->Nasal_Discharge) : "U";
                    $PHIE_PE_HEENT[$pkey]['Tonsillopharyngeal_Congestion'] = !is_null($pvalue->Tonsillopharyngeal_Congestion) ?  $PHIEController->ref_logical2($pvalue->Tonsillopharyngeal_Congestion) : "U";
                    $PHIE_PE_HEENT[$pkey]['Hypertrophic_Tonsils'] = !is_null($pvalue->Hypertrophic_Tonsils) ?  $PHIEController->ref_logical2($pvalue->Hypertrophic_Tonsils) : "U";
                    $PHIE_PE_HEENT[$pkey]['Palpable_Mass'] = !is_null($pvalue->Palpable_Mass_B) ?  $PHIEController->ref_logical2($pvalue->Palpable_Mass_B) : "U";
                    $PHIE_PE_HEENT[$pkey]['Exudates'] = !is_null($pvalue->Exudates) ?  $PHIEController->ref_logical2($pvalue->Exudates) : "U";
                    $PHIE_PE_HEENT[$pkey]['Others'] = !is_null($pvalue->heent_others) ?  $pvalue->heent_others : NULL;

                }
                }
            }

            // return $PHIE_PE_HEENT;
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_PE_HEENT) ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $PHIE_PE_HEENT;
            $result = $PHIEController->sendArray($data,$param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

    /**
     * Chest/Lungs
     */
    public function PHIE_PE_ChestLungs($facility_id, $EncounterData) {
        $param_type = 'ChestLungsData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_PE_ChestLungs = [];
        $ExaminationData = $this->healthcarebyFacilityId($facility_id,'Examination', $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if($ExaminationData) {
            $c = count($ExaminationData); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($ExaminationData as $pkey => $value) {
                //there should be patient and healthcare
                if($value->patient_id AND $value->healthcareservice_id){
                    foreach ($value->Examination as $key => $pvalue) {
                        $PHIEController = new PHIEController;

                        $PHIE_PE_ChestLungs[$pkey]['Pat_Facility_No'] = $value->patient_id;
                        $PHIE_PE_ChestLungs[$pkey]['Encounter_ID'] = $value->healthcareservice_id;
                        $PHIE_PE_ChestLungs[$pkey]['Symmetrical_Chest_Expansion'] = !is_null($pvalue->Symmetrical_Chest_Expansion) ?  $PHIEController->ref_logical2($pvalue->Symmetrical_Chest_Expansion) : "U";
                        $PHIE_PE_ChestLungs[$pkey]['Clear_Breathsounds'] = !is_null($pvalue->Clear_Breathsounds) ?  $PHIEController->ref_logical2($pvalue->Clear_Breathsounds) : "U";
                        $PHIE_PE_ChestLungs[$pkey]['Retractions'] = !is_null($pvalue->Retractions) ?  $PHIEController->ref_logical2($pvalue->Retractions) : "U";
                        $PHIE_PE_ChestLungs[$pkey]['Crackles_Rales'] = !is_null($pvalue->Crackles_Rales) ?  $PHIEController->ref_logical2($pvalue->Crackles_Rales) : "U";
                        $PHIE_PE_ChestLungs[$pkey]['Wheezes'] = !is_null($pvalue->Wheezes) ?  $PHIEController->ref_logical2($pvalue->Wheezes) : "U";
                        $PHIE_PE_ChestLungs[$pkey]['Other'] = !is_null($pvalue->chest_others) ?  $pvalue->chest_others : NULL;
                    }
                }
            }

            // return $PHIE_PE_ChestLungs;
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_PE_ChestLungs) ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $PHIE_PE_ChestLungs;
            $result = $PHIEController->sendArray($data,$param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

    /**
     * Heart
     */

    public function PHIE_PE_Heart($facility_id, $EncounterData) {
        $param_type = 'HeartData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_PE_Heart = [];
        $ExaminationData = $this->healthcarebyFacilityId($facility_id,'Examination', $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if($ExaminationData) {
            $c = count($ExaminationData); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($ExaminationData as $pkey => $value) {
                //there should be patient and healthcare
                if($value->patient_id AND $value->healthcareservice_id) {
                    foreach ($value->Examination as $key => $pvalue) {
                        $PHIEController = new PHIEController;

                        $PHIE_PE_Heart[$pkey]['Pat_Facility_No'] = $value->patient_id;
                        $PHIE_PE_Heart[$pkey]['Encounter_ID'] = $value->healthcareservice_id;
                        $PHIE_PE_Heart[$pkey]['Adynamic_Precordium'] = !is_null($pvalue->Adynamic_Precordium) ?  $PHIEController->ref_logical2($pvalue->Adynamic_Precordium) : "U";
                        $PHIE_PE_Heart[$pkey]['Rhythm'] = !is_null($pvalue->Rhythm) ?  $PHIEController->ref_logical2($pvalue->Rhythm) : "U";
                        $PHIE_PE_Heart[$pkey]['Heaves'] = !is_null($pvalue->Heaves) ?  $PHIEController->ref_logical2($pvalue->Heaves) : "U";
                        $PHIE_PE_Heart[$pkey]['Murmurs'] = !is_null($pvalue->Murmurs) ?  $PHIEController->ref_logical2($pvalue->Murmurs) : "U";
                        $PHIE_PE_Heart[$pkey]['Other'] = !is_null($pvalue->chest_others) ?  $pvalue->heart_others : NULL;
                    }
                }
            }

            // return $PHIE_PE_Heart;
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_PE_Heart) ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $PHIE_PE_Heart;
            $result = $PHIEController->sendArray($data,$param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }

    }

    /**
     * Abdomen
     */

    public function PHIE_PE_Abdomen($facility_id, $EncounterData) {
        $param_type = 'AbdomenData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_PE_Abdomen = [];
        $ExaminationData = $this->healthcarebyFacilityId($facility_id,'Examination', $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if($ExaminationData) {
            $c = count($ExaminationData); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($ExaminationData as $pkey => $value) {
                //there should be patient and healthcare
                if($value->patient_id AND $value->healthcareservice_id){
                    foreach ($value->Examination as $key => $pvalue) {
                        $PHIEController = new PHIEController;

                        $PHIE_PE_Abdomen[$pkey]['Pat_Facility_No'] = $value->patient_id;
                        $PHIE_PE_Abdomen[$pkey]['Encounter_ID'] = $value->healthcareservice_id;
                        $PHIE_PE_Abdomen[$pkey]['Flat'] = !is_null($pvalue->Flat) ?  $PHIEController->ref_logical2($pvalue->Flat) : "U";
                        $PHIE_PE_Abdomen[$pkey]['Globular'] = !is_null($pvalue->Globular) ?  $PHIEController->ref_logical2($pvalue->Globular) : "U";
                        $PHIE_PE_Abdomen[$pkey]['Flabby'] = !is_null($pvalue->Flabby) ?  $PHIEController->ref_logical2($pvalue->Flabby) : "U";
                        $PHIE_PE_Abdomen[$pkey]['Muscle_Guarding'] = !is_null($pvalue->Muscle_Guarding) ?  $PHIEController->ref_logical2($pvalue->Muscle_Guarding) : "U";
                        $PHIE_PE_Abdomen[$pkey]['Tenderness'] = !is_null($pvalue->Tenderness) ?  $PHIEController->ref_logical2($pvalue->Tenderness) : "U";
                        $PHIE_PE_Abdomen[$pkey]['Palpable_Mass'] = !is_null($pvalue->Palpable_Mass) ?  $PHIEController->ref_logical2($pvalue->Palpable_Mass) : "U";
                        $PHIE_PE_Abdomen[$pkey]['Other'] = !is_null($pvalue->abdomen_others) ?  $pvalue->abdomen_others : NULL;
                    }
                }
            }

            // return $PHIE_PE_Abdomen;
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_PE_Abdomen) ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $PHIE_PE_Abdomen;
            $result = $PHIEController->sendArray($data,$param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }


    /**
     * Extremities
     */
    public function PHIE_PE_Extremities($facility_id, $EncounterData) {
        $param_type = 'ExtremitiesData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_PE_Extremities = [];
        $ExaminationData = $this->healthcarebyFacilityId($facility_id,'Examination', $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if($ExaminationData) {
            $c = count($ExaminationData); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($ExaminationData as $pkey => $value) {
                //there should be patient and healthcare
                if($value->patient_id AND $value->healthcareservice_id){
                foreach ($value->Examination as $key => $pvalue) {
                    $PHIEController = new PHIEController;

                    $PHIE_PE_Extremities[$pkey]['Pat_Facility_No'] = $value->patient_id;
                    $PHIE_PE_Extremities[$pkey]['Encounter_ID'] = $value->healthcareservice_id;
                    $PHIE_PE_Extremities[$pkey]['Gross_Deformity'] = !is_null($pvalue->Gross_Deformity) ?  $PHIEController->ref_logical2($pvalue->Gross_Deformity) : "U";
                    $PHIE_PE_Extremities[$pkey]['Normal_Gait'] = !is_null($pvalue->Normal_Gait) ?  $PHIEController->ref_logical2($pvalue->Normal_Gait) : "U";
                    $PHIE_PE_Extremities[$pkey]['Full_Equal_Pulses'] = !is_null($pvalue->Full_Equal_Pulses) ?  $PHIEController->ref_logical2($pvalue->Full_Equal_Pulses) : "U";
                    $PHIE_PE_Extremities[$pkey]['Other'] = !is_null($pvalue->extreme_others) ?  $pvalue->extreme_others : NULL;
                }
                }
            }

            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_PE_Extremities) ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $PHIE_PE_Extremities;
            $result = $PHIEController->sendArray($data,$param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }


/**
 * Whopen
 */


}
