
    
    
    
    
    <legend>Mental Status Examination Form</legend>
<fieldset>
    <div class="form-group">
        <label class="col-md-2 control-label">General Description</label>
        <div class="col-md-10">
            <textarea class="form-control noresize" placeholder="General Description" cols="10" rows="5" name="" ></textarea>
        </div>
    </div>
</fieldset>
<fieldset>
    <div class="form-group">
        <label class="col-md-2 control-label">Mood and Affectivity</label>
        <div class="col-md-10">
            <textarea class="form-control noresize" placeholder="Mood and Affectivity" cols="10" rows="5" name="" ></textarea>
        </div>
    </div>
</fieldset>


<fieldset>
    <div class="form-group">
        <label class="col-md-2 control-label">Speech Characteristics</label>
        <div class="col-md-10">
            <textarea class="form-control noresize" placeholder="Speech Characteristics" cols="10" rows="5" name=""></textarea>
        </div>
    </div>
</fieldset>
<div class="icheck">
                <fieldset>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <dl class="col-md-2">
                                <dt> Perception </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="mentalHealth[perception][]" id="" value="hallucinations"
                                            <?php
                                                foreach(explode(",", $data['perception']) as $value)
                                                {
                                                    if($value=="hallucinations")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Hallucinations
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="mentalHealth[perception][]" id="" value="illusions"
                                            <?php
                                                foreach(explode(",", $data['perception']) as $value)
                                                {
                                                    if($value=="illusions")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Illusions
                                        </label>
                                </dd>
                            </dl>
                            <dl class="col-md-3">
                                <dt> Thought Content and Mental Trends </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="mentalHealth[thoughtContentAndMentalTrends][]" id="" value="flight"
                                            <?php
                                                foreach(explode(",", $data['thoughtContentAndMentalTrends']) as $value)
                                                {
                                                    if($value=="flight")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Flight
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="mentalHealth[thoughtContentAndMentalTrends][]" id="" value="loosenessOfAssociations"
                                            <?php
                                                foreach(explode(",", $data['thoughtContentAndMentalTrends']) as $value)
                                                {
                                                    if($value=="loosenessOfAssociations")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Looseness Of Assosciations
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="mentalHealth[thoughtContentAndMentalTrends][]" id="" value="circumstantiality"
                                            <?php
                                                foreach(explode(",", $data['thoughtContentAndMentalTrends']) as $value)
                                                {
                                                    if($value=="circumstantiality")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Circumstantiality
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="mentalHealth[thoughtContentAndMentalTrends][]" id="" value="tangentiality"
                                            <?php
                                                foreach(explode(",", $data['thoughtContentAndMentalTrends']) as $value)
                                                {
                                                    if($value=="tangentiality")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Tangentiality
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="mentalHealth[thoughtContentAndMentalTrends][]" id="" value="delusion"
                                            <?php
                                                foreach(explode(",", $data['thoughtContentAndMentalTrends']) as $value)
                                                {
                                                    if($value=="delusion")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Delusion
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="mentalHealth[thoughtContentAndMentalTrends][]" id="" value="preoccupations"
                                            <?php
                                                foreach(explode(",", $data['thoughtContentAndMentalTrends']) as $value)
                                                {
                                                    if($value=="preoccupations")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Preoccupations
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="mentalHealth[thoughtContentAndMentalTrends][]" id="" value="suicidal"
                                            <?php
                                                foreach(explode(",", $data['thoughtContentAndMentalTrends']) as $value)
                                                {
                                                    if($value=="suicidal")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Suicidal
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="col-md-7">
                                <dt> Sensorium and Cognition </dt>
                                <dd>
                                <div class="col-sm-11 noPaddingMargin">
                                        <select class="form-control" id="" name="mentalHealth[sensoriumAndCognition][]" value ="conciousness">
                                            <option value="" selected="selected">-- Choose Degree of Consciousness --</option>
                                            <optgroup>
                                                <option value="alert">Alert</option>
                                                <option value="fugue">Fugue</option>
                                                <option value="cloudy">Cloudy</option>
                                                <option value="somnolence">Somnolence</option>
                                                <option value="stupor">Stupor</option>
                                                <option value="coma">Coma</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="mentalHealth[sensoriumAndCognition][]" id="" value="orientedTime"
                                            <?php
                                                foreach(explode(",", $data['sensoriumAndCognition']) as $value)
                                                {
                                                    if($value=="orientedTime")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Oriented to Time
                                        </label>
                                    </div>
                                    <div class="checkbox" >
                                        <label>
                                          <input type="checkbox" {{ $read }} name="mentalHealth[sensoriumAndCognition][]" id="" value="confabulation"
                                            <?php
                                                foreach(explode(",", $data['sensoriumAndCognition']) as $value)
                                                {
                                                    if($value=="confabulation")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Confabulation 
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="mentalHealth[sensoriumAndCognition][]" id="" value="orientedPerson"
                                            <?php
                                                foreach(explode(",", $data['sensoriumAndCognition']) as $value)
                                                {
                                                    if($value=="orientedPerson")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Oriented to Person
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="mentalHealth[sensoriumAndCognition][]" id="" value="orientedPlace"
                                            <?php
                                                foreach(explode(",", $data['sensoriumAndCognition']) as $value)
                                                {
                                                    if($value=="orientedPlace")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Oriented to Place 
                                        </label>
                                    </div>
                                    <div>
                                        <label class="col-md-3 control-label noPaddingMargin">Remote Memory:</label>
                                        <div class="col-md-8">
                                            <input name="3" type="text" value="" placeholder ="Intact/Impaired" class="form-control">
                                        </div> 
                                    </div>
                                    <div>
                                        <label class="col-md-3 control-label noPaddingMargin">Recent Past Memory:</label>
                                        <div class="col-md-8">
                                            <input name="3" type="text" value="" placeholder ="Intact/Impaired" class="form-control">
                                        </div> 
                                    </div>
                                    <div>
                                        <label class="col-md-3 control-label noPaddingMargin">Recent Memory:</label>
                                        <div class="col-md-8">
                                            <input name="3" type="text" value="" placeholder ="Intact/Impaired" class="form-control">
                                        </div> 
                                    </div>
                                    
                                    <div style="margin-bottom:10px;">
                                        <label class="col-md-3 control-label noPaddingMargin">Immediate Retention and Recall:</label>
                                        <div class="col-md-8">
                                            <input name="3" type="text" value="" placeholder ="Intact/Impaired" class="form-control">
                                        </div> 
                                    </div>                                   
                                </dd>
                            </dl>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="col-md-12" style = "margin-left:5%;">
                        <div class="col-md-12">
                            <dl class="col-md-5">
                                <dt> Concentration and Attention </dt>
                                <dd>
                                <div>
                                        <label class="col-md-4 control-label noPaddingMargin">Reading and writing:</label>
                                        <div class="col-md-8">
                                            <input name="5" type="text" value="" placeholder ="Intact/Impaired" class="form-control">
                                        </div> 
                                    </div>
                                    <div>
                                        <label class="col-md-4 control-label noPaddingMargin">Visual spatial ability:</label>
                                        <div class="col-md-8">
                                            <input name="6" type="text" value="" placeholder ="Intact/Impaired" class="form-control">
                                        </div> 
                                    </div>
                                    <div>
                                        <label class="col-md-4 control-label noPaddingMargin">Abstract thuoght:</label>
                                        <div class="col-md-8">
                                            <input name="7" type="text" value="" placeholder ="Intact/Impaired" class="form-control">
                                        </div> 
                                    </div>
                                    
                                    <div>
                                        <label class="col-md-4 control-label noPaddingMargin">Serial 7's:</label>
                                        <div class="col-md-8">
                                            <input name="8" type="text" value="" placeholder ="Intact/Impaired" class="form-control">
                                        </div> 
                                    </div>
                                </dd>
                            </dl>
                            <dl class="col-md-7">
                            <dt> Impulsivity </dt>
                                <dd>
                                <div>
                                        <label class="col-md-3 control-label noPaddingMargin">Impulse Control:</label>
                                        <div class="col-md-8">
                                            <input name="9" type="text" value="" placeholder ="" class="form-control">
                                        </div> 
                                    </div>
                                <dt> Judgment and Insight </dt>
                                <dd>
                                    <div class="col-sm-12">
                                        <label class="col-sm-3 control-label nnoPaddingMargin">Judgment:</label>
                                        <div class="radio-inline">
                                            <label>
                                            <input name="mentalHealth[judgmentAndInsight]" type="radio" value="good" <?php if($data['judgmentAndInsight']=='good'){echo "checked";}?>>Good
                                            </label>
                                        </div>
                                        <div class="radio-inline">
                                            <label>
                                            <input name="mentalHealth[judgmentAndInsight]" type="radio" value="poor" <?php if($data['judgmentAndInsight']=='poor'){echo "checked";}?>>Poor
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-11 noPaddingMargin">
                                        <select class="form-control" id="" name="mentalHealth[judgmentAndInsight][]" value ="insight">
                                            <option value="" selected="selected">-- Choose Degree of Insight --</option>
                                            <optgroup>
                                                <option value="denial">Denial</option>
                                                <option value="aware">Aware</option>
                                                <option value="intellectualInsight">Intellectual Insight</option>
                                                <option value="trueEmotionalInsight">True Emotional Insight</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </dd>
                                <dt> Reliability </dt>
                                <dd>
                                    <div class="col-sm-12">
                                        <label class="col-sm-3 control-label nnoPaddingMargin">Reliability:</label>
                                        <div class="radio-inline">
                                            <label>
                                            <input name="mentalHealth[reliability]" type="radio" value="good" <?php if($data['reliability']=='good'){echo "checked";}?>>Good
                                            </label>
                                        </div>
                                        <div class="radio-inline">
                                            <label>
                                            <input name="mentalHealth[reliability]" type="radio" value="fair" <?php if($data['reliability']=='fair'){echo "checked";}?>>Fair
                                            </label>
                                        </div>
                                        <div class="radio-inline">
                                            <label>
                                            <input name="mentalHealth[reliability]" type="radio" value="poor" <?php if($data['reliability']=='fair'){echo "checked";}?>>Poor
                                            </label>
                                        </div>
                                    </div>
                                    
                                </dd>
                            </dl>
                        </div>
                    </div>
                </fieldset>
            </div>
            