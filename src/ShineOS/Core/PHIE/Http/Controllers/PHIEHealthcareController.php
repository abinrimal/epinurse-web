<?php namespace ShineOS\Core\PHIE\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\PHIE\Http\Controllers\PHIEController;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\LOV\Http\Controllers\LOVController;
use Shine\Libraries\FacilityHelper;
use Input, Datetime, DB, Session;

class PHIEHealthcareController extends Controller {

    public function __construct() {
        $this->facility = FacilityHelper::facilityInfo();
        $this->HealthcareRequired = array('Pat_Facility_No',
                    'Pat_PHIC_Membership_Type',
                    'CCT',
                    'IndigenousGroup',
                    'pat_benefit_package',
                    'Encounter_ID',
                    'Encounter_Type',
                    'Encounter_Date',
                    'Encounter_Time',
                    'Age_Years',
                    'Age_Months',
                    'Age_Days',
                    'Chief_Complaint',
                    'Provider_PhilHealth_Acc_No',
                    'Prov_Last_Name',
                    'Prov_First_Name',
                    'Diagnosis',
                    'Disposition',
                    'Type_of_Visit',
                    'Pregnant',
                    'Weight_Loss',
                    'With_Intact_Uterus',
                    'Source_of_Referral',
                    'Reason_For_Referral');
    }

    public function healthcarebyFacilityId($facility_id, $compareToDate, $data_Patient_ID=NULL) {
        //create healthcare query object
        $healthcare = Healthcareservices::query();
        //filter using the list of patients from the Patient Collector
        if(!empty($data_Patient_ID)) {
            
        } else {
            $PHIEController = new PHIEController;
            $arr_param_type_id = array();
            $param_type_id = $PHIEController->PHIESync_list($facility_id, NULL, 'PatientData');
            $dd = $param_type_id->toArray();
            if($dd) {
                foreach($dd as $val) {
                    $d[] = json_decode($val['response']);
                }
                foreach($d as $va) {
                    $de[] = $va->PatientData;
                }
                foreach($de as $v) {
                    foreach($v as $t) {
                        $arr_param_type_id[] = json_decode($t->soap_result);
                    }
                }
                $x = array();
                foreach($arr_param_type_id as $key => $value) {
                    if(isset($value->PHIE->Pat_Facility_No)){
                        $x[] = $value->PHIE->Pat_Facility_No;
                    }
                }
                //$healthcare = $healthcare->whereIn('patients.patient_id', $x);
                $data_Patient_ID = $x;
            } else {
                $healthcare = NULL;
            }
        }

        //filter healthcare using dates
        if($healthcare){
            $healthcare = $healthcare->join('facility_patient_user','healthcare_services.facilitypatientuser_id','=','facility_patient_user.facilitypatientuser_id')
                    ->join('facility_user','facility_patient_user.facilityuser_id','=','facility_user.facilityuser_id')
                    ->join('facilities','facilities.facility_id','=','facility_user.facility_id')
                    ->join('patients','patients.patient_id','=','facility_patient_user.patient_id')
                    ->whereIn('patients.patient_id', $data_Patient_ID)
                    ->with(array('GeneralConsultation', 'VitalsPhysical', 'Diagnosis' => function($query) {
                            $query->with('DiagnosisICD10')->where('diagnosislist_id',"<>","")->whereNotNull('diagnosislist_id')
                            ->where('diagnosislist_id','!=','*');
                        },
                        'Examination', 'MedicalOrder', 'Disposition'))
                    ->where('facilities.facility_id', $facility_id)
                    ->where('healthcare_services.deleted_at','=',NULL)
                    ->where('healthcare_services.updated_at', '>', $compareToDate) //record must be updated equal to or greater than last date of sync
                    ->whereYear('healthcare_services.encounter_datetime', '=', 2017)
                    ->whereMonth('healthcare_services.encounter_datetime', '<=', 3)
                    ->where('patients.deleted_at', NULL)
                    ->orderBy('healthcare_services.encounter_datetime', 'DESC')
                    ->get();

            foreach ($healthcare as $key => $value) {
                $Patients = Patients::query();
                $value->specialaccomodation = $Patients->join('patient_philhealthinfo','patients.patient_id','=','patient_philhealthinfo.patient_id')
                                    ->with('patientDisabilities')
                                    ->first();
            }

            return $healthcare;
        }
    }

    /**
     * Encounter Log
     */
    public function PHIE_encounterLogData($facility_id, $data_Patient_ID) {
        $PHIEController = new PHIEController;

        $param_type = 'EncounterData';
        $PHIEController->install_logg("Please wait...");
        //compare date with param type
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_EncounterLog = [];
        $PHIE_Excluded_Encounter = [];
        $this_errors = [];
        $PHIEController->install_log("Collecting ".$param_type.".");
        $EncounterLogData = $this->healthcarebyFacilityId($facility_id,$compareToDate,$data_Patient_ID);
        

        $facilityInfo = findByFacilityID($facility_id);

        $LOVController = new LOVController();

        if($EncounterLogData) {
            $c = count($EncounterLogData); $ct = 0;
            foreach ($EncounterLogData as $pkey => $value) {
                
                $Pat_Special_Accommodation = NULL;
                $disabilityid = NULL;

                //disabilities
                if(count($value->specialaccomodation)) {
                    foreach ($value->specialaccomodation->patientDisabilities as $d_key => $d_value) {
                        $disabilityid[$d_key] = $d_value->disability_id;
                    }
                    if($disabilityid) {
                        $Pat_Special_Accommodation = implode(", ", $disabilityid);
                    }
                }

                $PHIE_EncounterLog[$pkey]['Pat_Facility_No'] = !is_null($value->patient_id) ?  $value->patient_id : "";
                $PHIE_EncounterLog[$pkey]['Encounter_ID'] = !is_null($value->healthcareservice_id) ?  $value->healthcareservice_id : "";
                $PHIE_EncounterLog[$pkey]['Encounter_Type'] = !is_null($value->encounter_type) ?  $value->encounter_type : "";

                //Philhealth info
                $PHIE_EncounterLog[$pkey]['Pat_PHIC_Membership_Type'] = !is_null($value->specialaccomodation->member_type) ?  $value->specialaccomodation->member_type : 'NM';
                $PHIE_EncounterLog[$pkey]['Pat_PHIC_Membership_Category'] = !is_null($value->specialaccomodation->philhealth_category) ?  $value->specialaccomodation->philhealth_category : "";

                $PHIE_EncounterLog[$pkey]['CCT'] = !is_null($value->specialaccomodation->ccash_transfer) ?  $value->specialaccomodation->ccash_transfer : "U";
                $PHIE_EncounterLog[$pkey]['IndigenousGroup'] = !is_null($value->specialaccomodation->indigenous_group) ?  ($value->specialaccomodation->indigenous_group==1) ? "Y" : "N" : "U";
                $PHIE_EncounterLog[$pkey]['pat_benefit_package'] = !is_null($value->specialaccomodation->benefit_type) ?  $value->specialaccomodation->benefit_type : "TSeKaP"; //no other options from PHIE list/excel

                $datetime = !is_null($value->encounter_datetime) ?  new Datetime($value->encounter_datetime) : "1900-01-01 8:00:00";
                $Encounter_Date = !is_null($datetime) ? $datetime->format('Y-m-d') : "";
                $Encounter_Time = !is_null($datetime) ? $datetime->format('H:i:s') : "";

                $PHIE_EncounterLog[$pkey]['Encounter_Date'] = $Encounter_Date;
                $PHIE_EncounterLog[$pkey]['Encounter_Time'] = $Encounter_Time;

                $PHIE_EncounterLog[$pkey]['Pat_Special_Accommodation'] = !is_null($Pat_Special_Accommodation) ?  $Pat_Special_Accommodation : '';
                //if disabilities contain 'Others' give Others
                if(is_array($disabilityid) AND in_array('10',$disabilityid)) {
                    $PHIE_EncounterLog[$pkey]['Pat_Special_Accommodation_Others'] = $value->specialaccommodation->disability_others;
                } else {
                    $PHIE_EncounterLog[$pkey]['Pat_Special_Accommodation_Others'] = 'NA';
                }

                $datetime1 = !is_null($value->birthdate) ? new DateTime() : "1900-01-01 8:00:00";
                $datetime2 = !is_null($value->birthdate) ? new DateTime($value->birthdate.' '.$value->birthtime) : "1900-01-01 8:00:00";
                $interval = date_diff(new DateTime(), new DateTime($value->birthdate.' '.$value->birthtime));

                $PHIE_EncounterLog[$pkey]['Age_Years'] = $interval->format('%Y');
                $PHIE_EncounterLog[$pkey]['Age_Months'] = $interval->format('%M');
                $PHIE_EncounterLog[$pkey]['Age_Days'] = $interval->format('%d');

                $PHIE_EncounterLog[$pkey]['Chief_Complaint'] = (!$value->GeneralConsultation->isEmpty()) ? $value->GeneralConsultation[0]->complaint : '';
                $PHIE_EncounterLog[$pkey]['Provider_PhilHealth_Acc_No'] = !is_null($value->phic_accr_id) ?  ($value->phic_accr_id != "") ? $value->phic_accr_id : "000000000000" : "000000000000";

                $doctor = NULL;
                $prov_fn = NULL;
                $prov_ln = NULL;
                $prov_mn = "Notgiven";
                $prov_sf = NULL;
                if(!is_null($value->seen_by)) {
                    $doctor = findUserByUserID($value->seen_by);
                    $prov_fn = getUserFirstNameByID($value->seen_by);
                    $prov_ln = getUserLastNameByID($value->seen_by);
                    $prov_mn = getUserMidNameByID($value->seen_by);
                    $prov_sf = getUserSuffixByID($value->seen_by);
                } else {
                    $doctor = getFacilityDoctor($facility_id);
                    $prov_fn = $doctor->first_name;
                    $prov_ln = $doctor->last_name;
                    $prov_mn = $doctor->middle_name;
                    $prov_sf = $doctor->suffix;
                }
                /*$suffixArray = array("II","III","IV","JR","NA","SR","V");

                if(!in_array($prov_sf, $suffixArray)) {
                    $prov_sf = NULL;
                }*/
                $ptr = "0000000000000";
                if($doctor) {
                    $ptr = $doctor->professional_license_number;
                }
                $PHIE_EncounterLog[$pkey]['Provider_PRC_No'] = ($ptr != "") ? $ptr : '0000000000000';

                $PHIE_EncounterLog[$pkey]['Prov_Last_Name'] = $prov_ln;
                $PHIE_EncounterLog[$pkey]['Prov_First_Name'] = $prov_fn;
                $PHIE_EncounterLog[$pkey]['Prov_Middle_Name'] = ($prov_mn != "") ? $prov_mn : "Notgiven";
                $PHIE_EncounterLog[$pkey]['Prov_Suffix_Name'] = $prov_sf;

                $PHIE_EncounterLog[$pkey]['Doctors_Advice'] = '';
                $PHIE_EncounterLog[$pkey]['Diagnosis'] = '';
                $PHIE_EncounterLog[$pkey]['Diagnosis_Type'] = '';
                $PHIE_EncounterLog[$pkey]['Final_Diagnosis_ICD10'] = '';
                $PHIE_EncounterLog[$pkey]['Final_Diagnosis_Date'] = '';

                //if there is no diagnosis data
                //check if this is a TBDOTS Service
                if($value->Diagnosis->isEmpty()) {
                    if($value->healthcareservicetype_id == 'Tuberculosis') {
                        $PHIE_EncounterLog[$pkey]['Doctors_Advice'] = "TBDOTS Service";
                        $PHIE_EncounterLog[$pkey]['Diagnosis'] = "Respiratory tuberculosis";
                        $PHIE_EncounterLog[$pkey]['Diagnosis_Type'] = "CLIDI";
                        
                    }
                    if($value->healthcareservicetype_id == 'FamilyPlanning') {
                        $PHIE_EncounterLog[$pkey]['Doctors_Advice'] = "Family Planning Service";
                        $PHIE_EncounterLog[$pkey]['Diagnosis'] = "Family Planning";
                        $PHIE_EncounterLog[$pkey]['Diagnosis_Type'] = "CLIDI";
                        
                    }
                }

                if(!$value->Diagnosis->isEmpty()) {
                    foreach ($value->Diagnosis as $Dkey => $Dvalue) {
                        if($Dvalue->diagnosislist_id != NULL OR $Dvalue->diagnosislist_id != "") {
                            $PHIE_EncounterLog[$pkey]['Doctors_Advice'] = !is_null($Dvalue->diagnosis_notes) ? $Dvalue->diagnosis_notes : NULL;
                            $PHIE_EncounterLog[$pkey]['Diagnosis'] = $Dvalue->diagnosislist_id;
                            $PHIE_EncounterLog[$pkey]['Diagnosis_Type'] = !is_null($Dvalue->diagnosis_type) ?  $Dvalue->diagnosis_type : '';

                            if($Dvalue->diagnosis_type == 'FINDX') {

                                if(count($Dvalue->DiagnosisICD10) > 0) {
                                    $PHIE_EncounterLog[$pkey]['Final_Diagnosis_ICD10'] = count($Dvalue->DiagnosisICD10) ?  ((($Dvalue->DiagnosisICD10[0]->icd10_code) == '0') ? NULL : $Dvalue->DiagnosisICD10[0]->icd10_code ) : NULL;
                                    $PHIE_EncounterLog[$pkey]['Final_Diagnosis_Date'] = $Encounter_Date;
                                } else {
                                    //let us try icd10 from diagnosis if set
                                    $icd10code = $LOVController->icd10FromDiagnosis($Dvalue->diagnosislist_id);

                                    //no icd10code found force Diganosis to clinical type
                                    if($icd10code == NULL) {
                                        $PHIE_EncounterLog[$pkey]['Diagnosis_Type'] = 'CLIDI';
                                        $PHIE_EncounterLog[$pkey]['Final_Diagnosis_ICD10'] = NULL;
                                        $PHIE_EncounterLog[$pkey]['Final_Diagnosis_Date'] = NULL;
                                    } else {
                                        $PHIE_EncounterLog[$pkey]['Final_Diagnosis_ICD10'] = $icd10code;
                                        $PHIE_EncounterLog[$pkey]['Final_Diagnosis_Date'] = $Encounter_Date;
                                    }
                                }                            
                            }
                        }
                    }
                }

                $disp = 'HOME';
                if(!is_null($value->Disposition)) {
                    if(($value->Disposition->disposition == 'HAMA' OR $value->Disposition->disposition == 'ABS') AND ($this->facility->facility_type != 'Hospital' OR $this->facility->facility_type != 'PRIVATE_HOSPITAL' OR $this->facility->facility_type != 'PROVINCIAL_HOSPITAL' OR $this->facility->facility_type != 'MUNICIPAL_DISTRICT_HOSPITAL')) {
                        $disp = 'HOME';
                    } else {
                        $disp = $value->Disposition->disposition;
                    }
                }

                $PHIE_EncounterLog[$pkey]['Disposition'] = $disp;
                $PHIE_EncounterLog[$pkey]['Discharge_Condition'] = !is_null($value->Disposition) ? $value->Disposition->discharge_condition : '';

                $discharge_datetime = !is_null($value->Disposition) ?  new Datetime($value->Disposition->discharge_datetime) : NULL;
                $Discharge_Date = !is_null($discharge_datetime) ? $discharge_datetime->format('Y-m-d') : "";
                $Discharge_Time = !is_null($discharge_datetime) ? $discharge_datetime->format('H:i:s') : "";

                $PHIE_EncounterLog[$pkey]['Discharge_Date'] = $Discharge_Date;
                $PHIE_EncounterLog[$pkey]['Discharge_Time'] = $Discharge_Time;

                $PHIE_EncounterLog[$pkey]['Type_of_Visit'] = !is_null($value->consultation_type) ?  $value->consultation_type : "";

                $PHIEController = new PHIEController;
                $PHIE_EncounterLog[$pkey]['Pregnant'] = !is_null($value->VitalsPhysical) ?  $PHIEController->ref_logical2($value->VitalsPhysical->pregnant) : "U";
                $PHIE_EncounterLog[$pkey]['Weight_Loss'] = !is_null($value->VitalsPhysical) ?  $PHIEController->ref_logical2($value->VitalsPhysical->weight_loss) : "U";
                $PHIE_EncounterLog[$pkey]['With_Intact_Uterus'] = !is_null($value->VitalsPhysical) ?  $PHIEController->ref_logical2($value->VitalsPhysical->with_intact_uterus) : "U";

                $PHIE_EncounterLog[$pkey]['Source_of_Referral'] = 'UNKNO';
                $PHIE_EncounterLog[$pkey]['SOR_Health_Facility'] = '';
                $PHIE_EncounterLog[$pkey]['SOR_Health_Care_Provider'] = '';
                $PHIE_EncounterLog[$pkey]['SOR_Name'] = '';
                $PHIE_EncounterLog[$pkey]['TR_Non_Health_Facility'] = 'NA';
                $PHIE_EncounterLog[$pkey]['Reason_For_Referral'] = 'OTHER';
                $PHIE_EncounterLog[$pkey]['Reason_For_Referral_Other_Specify'] = 'NA';
/*
                //get BP Assessment
                $PHIE_EncounterLog[$pkey]['BP_Assessment'] = 'NA';
                $LOVController = new LOVController();
               if(!empty($value->VitalsPhysical->bloodpressure_diastolic) AND !empty($value->VitalsPhysical->bloodpressure_systolic) AND !empty($value->VitalsPhysical->BMI_category)) {
                   $PHIE_EncounterLog[$pkey]['BP_Assessment'] = $LOVController->bloodpressure_assessment($value->VitalsPhysical->bloodpressure_systolic, $value->VitalsPhysical->bloodpressure_diastolic);
               }
*/

                //let us default this to Consultation - 01
                //$PHIE_EncounterLog[$pkey]['Service_Code'] = "01";

            }

            //unset the array if incomplete data
            $out = false;
            if(count($PHIE_EncounterLog)) {
                $PHIEController->install_log("Filtering ".$param_type.".");
                foreach ($PHIE_EncounterLog as $result_key => $result_value) {
                    foreach ($result_value as $val_key => $val_value) {
                        $exists = in_array($val_key, $this->HealthcareRequired);
                        if($exists) {
                            if($val_value == NULL) {
                                $this_errors[] = $val_key;
                                $out = true;
                            }
                        }
                    }
                    if($out == true) {
                        $fullname = returnFullNameOfPatientID($PHIE_EncounterLog[$result_key]['Pat_Facility_No']);
                        $PHIE_Excluded_Encounter[] = [ 'id'=>$result_key, 'Encounter Date'=>$PHIE_EncounterLog[$result_key]['Encounter_Date'], 'Patient ID' => $PHIE_EncounterLog[$result_key]['Pat_Facility_No'], 'Healthcare ID' => $PHIE_EncounterLog[$result_key]['Encounter_ID'], 'Pateint Name'=>$fullname, 'Incomplete Data:'=>$this_errors];
                        $out = false;
                        $this_errors = [];
                        unset($PHIE_EncounterLog[$result_key]);
                    }
                }
            }

            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_EncounterLog) ." records.");
            $data[$param_type] = $PHIE_EncounterLog;
            $data_Encounter_ID = array_values(array_column($PHIE_EncounterLog, 'Encounter_ID'));
            $filer = $PHIE_Excluded_Encounter;

            if(Session::get('_global_phie_type') == 'check') {
                $PHIEController->install_logg("Excluded ".$param_type." ".count($filer).".<br />");
                //write logs
                $error_file = userfiles_path() .  'logs/'.date('Y-m-d').'_'.studly_case($facilityInfo->facility_name)."_".$param_type.'_error_log.txt';
                @file_put_contents($error_file, json_encode($filer) . "\n");
                //return result of send
                return array('result'=> NULL,'PHIE_EncounterLog' => $data_Encounter_ID);
            } elseif(Session::get('_global_phie_type') == 'single') {
                return array('result'=> NULL,'PHIE_EncounterLog' => $data_Encounter_ID);
            } else {
                //send to PHIE
                $result = $PHIEController->sendArray($data, $param_type, $data_Encounter_ID, $filer);
                //return result of send
                return array('result'=> $result['result'],'PHIE_EncounterLog' => $result['newData']);
            }
            
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return array('result'=> NULL,'PHIE_EncounterLog' => NULL);
        }
    }
}
