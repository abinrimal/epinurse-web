@extends('nurseregistry::layouts.master')

@section('page-content')
	
	<div class="col-sm-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
			  <li class="active"><a data-toggle="tab" href="#registry">Nurse Registry</a></li>
			  <li class="pull-right"> <a href="nurseregistry/new" class="btn btn-sm btn-primary"> <i class="fa fa-plus"></i> <span> Add New Entry </span></a></li>
			</ul>

			<div class="tab-content">
				<div id="registry" class="tab-pane fade in active">
					<table class="table table-condensed datatable">
						<thead>
						  <tr>
						  	<th class="nosort"></th>
						    <th>Name</th>
						    <th>Designation</th>
						    <th>Office</th>
						    <th class="nosort">Actions</th>
						  </tr>
						</thead>
						<tbody>
							@foreach($nurses as $key => $nurse)
								<tr>
									<td>{{ $nurse->id or NULL }}</td>
									<td>{{ $nurse->first_name or NULL }} {{ $nurse->last_name or NULL }}</td>
									<td>{{ $nurse->designation or NULL }}</td>
									<td>{{ $nurse->office or NULL }}</td>
									<td>
										<div class="btn-group">
											<a href="{{ url('/nurseregistry/update/'.$nurse->nurse_id) }}" class="btn btn-success"><i class="fa fa-pencil"></i> Edit </a>
											<a href="{{ url('/nurseregistry/delete/'.$nurse->nurse_id) }}" class="btn btn-danger" onclick="if( confirm('Are you sure you want to delete this nurse?') ) { return true; } return false;"><i class="fa fa-trash-o"></i> Delete </a>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@stop