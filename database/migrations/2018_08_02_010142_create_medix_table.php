<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medix', function (Blueprint $table) {
            $table->increments('id');

            $table->string('medix_id', 16);
            $table->string('patient_id', 60)->nullable();
            $table->double('tokens');
            $table->double('mdx_coin');

            $table->timestamps();
            $table->unique('medix_id');
            $table->unique('patient_id');
        });       //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('medix');       //
    }
}
