@extends('schoolassessment::layouts.master')

@section('page-content')		
	<div class="col-sm-12">
		<div class="box box-primary">
		  <div class="box-header with-border">
		    <h3 class="box-title">School Record</h3>
		  </div>
		  @include('schoolassessment::pages.schoolform')
		</div>
	</div>
@stop