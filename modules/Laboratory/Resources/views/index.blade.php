@extends('layout.master')

@section('heads')
{!! HTML::style('modules/Laboratory/Assets/laboratory.css') !!}
@stop

@section('page-header')
  <section class="content-header">
    <!-- edit by RJBS -->
    <h1>
      <i class="fa fa-flask"></i>
      Laboratory
    </h1>
    <!-- end edit RJBS -->
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> <a href="{{ url('/laboratory') }}"> Laboratory </a></li>
    </ol>
  </section>
@stop

@section('content')
<div class="row">
    @if (Session::has('flash_message'))
        <div class="alert {{Session::get('flash_type') }}">{{ Session::get('flash_message') }}</div>
    @endif

    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#by_laboratory" data-toggle="tab">Grouped by Laboratory</a></li>
              <li><a href="#by_patients" data-toggle="tab">Grouped by Patients</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="by_laboratory">
                  <div class="box box-solid">
                    <div class="box-body">
                      <div class="box-group" id="accordion">
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        @if(isset($lov_laboratories))
                        @foreach($lov_laboratories as $lov_key => $lov_val)
                            <div class="panel box">
                                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $lov_key; ?>">
                                    <div class="box-header with-border">
                                        <h4 class="box-title col-md-12">
                                        <i class="fa fa-fw fa-angle-double-right"></i> {{ $lov_val }}
                                          @if(isset($MedicalOrderLabExam_count))
                                              @foreach($MedicalOrderLabExam_count as $ctr_key => $ctr_val)
                                                  @if($ctr_val->laboratory_test_type == $lov_key)
                                                      <span class="badge bg-red pull-right">{{ $ctr_val->total }}</span>
                                                  @endif
                                              @endforeach
                                          @endif
                                        </h4>
                                    </div>
                                </a>

                              <div id="<?php echo $lov_key; ?>" class="panel-collapse collapse">
                                <div class="box-body">
                                    <table id="" class="table table-bordered datatable">
                                        <thead>
                                            <tr>
                                              <th>Patient Name</th>
                                              <th>Request Date</th>
                                              <th>Status</th>
                                              <th style="width: 20%;">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($MedicalOrderLabExam))
                                            <?php //dd($MedicalOrderLabExam); ?>
                                            @foreach($MedicalOrderLabExam as $lab_key => $lab_valu)
                                                @foreach ($lab_valu as $lab_ky => $lab_val)
                                                @foreach ($lab_val['lab'] as $lab_k => $lab_v)
                                                     @if($lov_key == $lab_v->laboratory_test_type)
                                                        <tr>
                                                          <td>
                                                              {{ $lab_val['patient']['first_name'] }}
                                                              {{ $lab_val['patient']['middle_name'] }}
                                                              {{ $lab_val['patient']['last_name'] }}
                                                              <br>
                                      <a href="{{ url('healthcareservices/qview/'.$lab_val['patient']['patient_id'].'/'.$lab_val['healthcareservice']['healthcareservice_id']) }}" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myInfoModal">Open Consultation | <i class="fa fa-search"></i></a>
                                                          </td>
                                                          <td>
                                                              <?php echo date('M d, Y h:i A', strtotime($lab_v->orderdate)); ?>
                                                          </td>
                                                          <td>
                                                              @if($lab_v->laboratoryresult_id != NULL)
                                                                  <?php $disabled_upload = 'disabled'; ?>
                                                                  <span class="label label-success">Has Results: <?php echo date('M d, Y h:i A', strtotime($lab_v->labdate)); ?>
                                                                  </span>
                                                              @else
                                                                  <?php $disabled_upload = ''; ?>
                                                                  <span class="label label-warning">No Results</span>
                                                              @endif
                                                          </td>
                                                          <td>
                                                              @if($lab_v->laboratory_test_type =='BR')
                                                                  <?php $Modal_labType_1 = "lab_completebloodcount"; ?>
                                                              @elseif($lab_v->laboratory_test_type =='UR')
                                                                  <?php $Modal_labType_1 = "lab_urinalysis"; ?>
                                                              @elseif($lab_v->laboratory_test_type =='FE')
                                                                  <?php $Modal_labType_1 = "lab_fecalysis"; ?>
                                                              @else
                                                                  <?php $Modal_labType_1 = "labModal"; ?>
                                                              @endif

                                                              @if(!$disabled_upload)
                                                                  <a href="#" class="btn btn-block btn-default btn-sm labClick <?php echo $Modal_labType_1; ?>" data-toggle="modal" data-id="{{ $lab_v->lr_mdle_id }}" data-target="#<?php echo $Modal_labType_1; ?>" > Upload result </a>
                                                              @else
                                                                  <a href="laboratory/modal/<?php echo $lab_v->laboratory_test_type; ?>/{{ $lab_v->lr_mdle_id }}" class="btn btn-block btn-default btn-sm" data-toggle="modal" data-target="#myInfoModal"> View </a>
                                                              @endif
                                                          </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                @endforeach
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                              </div>
                            </div>
                        @endforeach
                        @endif
                      </div>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->

                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="by_patients">
                    <table id="" class="table table-bordered datatable">
                        <thead>
                            <tr>
                              <th>Patient Name</th>
                              <th>Laboratory</th>
                              <th>Request Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(isset($MedicalOrderLabExam))
                            <?php //dd($lov_laboratories, $MedicalOrderLabExam); ?>
                            @foreach($MedicalOrderLabExam as $lab_key_a => $lab_val_a)
                            @foreach($lab_val_a as $lab_key_1 => $lab_val_1)
                                <tr>
                                  <td style="width:25%;vertical-align: top !important;">
                                      {{ $lab_val_1['patient']['first_name'] }}
                                      {{ $lab_val_1['patient']['middle_name'] }}
                                      {{ $lab_val_1['patient']['last_name'] }}
                                      <br>
                                      <a href="{{ url('healthcareservices/qview/'.$lab_val_1['patient']['patient_id'].'/'.$lab_val_1['healthcareservice']['healthcareservice_id']) }}" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myInfoModal">Open Consultation | <i class="fa fa-search"></i></a>
                                  </td>
                                  <td>
                                      <table class="table" style="width:100%;">
                                          @foreach ($lab_val_1['lab'] as $lab_k_1 => $lab_v_1)
                                              @if($lab_v_1->laboratory_test_type != NULL)
                                                  <tr>
                                                      <td style="width:30%;">
                                                          {{ $lov_laboratories[$lab_v_1->laboratory_test_type] }}

                                                      </td>
                                                      <td>
                                                          @if($lab_v_1->labdate != NULL)
                                                              <?php $disabled_upload_1 = TRUE; ?>
                                                              <span class="label label-success">Has Results: <?php echo date('M d, Y h:i A', strtotime($lab_v_1->labdate)); ?>
                                                              </span>
                                                          @else
                                                              <?php $disabled_upload_1 = FALSE; ?>
                                                              <span class="label label-warning">No Results
                                                              </span>
                                                          @endif

                                                      </td>
                                                      <td style="width:28%;">
                                                          @if($lab_v_1->laboratory_test_type =='BR')
                                                              <?php $Modal_labType = "lab_completebloodcount"; ?>
                                                          @elseif($lab_v_1->laboratory_test_type =='UR')
                                                              <?php $Modal_labType = "lab_urinalysis"; ?>
                                                          @elseif($lab_v_1->laboratory_test_type =='FE')
                                                              <?php $Modal_labType = "lab_fecalysis"; ?>
                                                          @else
                                                              <?php $Modal_labType = "labModal"; ?>
                                                          @endif

                                                          @if(!$disabled_upload_1)
                                                              <a href="#" class="btn btn-block btn-default btn-sm labClick" data-toggle="modal" data-id="{{ $lab_v_1->lr_mdle_id }}" data-target="#<?php echo $Modal_labType; ?>" > Upload result </a>
                                                          @else
                                                              <a href="laboratory/modal/<?php echo $lab_v_1->laboratory_test_type; ?>/{{ $lab_v_1->lr_mdle_id }}" class="btn btn-block btn-default btn-sm" data-toggle="modal" data-target="#myInfoModal"> View </a>

                                                          @endif

                                                      </td>
                                                  </tr>
                                              @endif
                                          @endforeach
                                      </table>
                                  </td>
                                  <td style="width:22%;vertical-align:top;">
                                          <h6>
                                              <?php if(isset($lab_v_1->orderdate)) echo date('M d, Y h:i A', strtotime($lab_v_1->orderdate)); ?>
                                          </h6>

                                    </td>
                                </tr>
                            @endforeach
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col -->
</div>

@include('laboratory::modal.modal_lab_completebloodcount')
@include('laboratory::modal.modal_lab_urinalysis')
@include('laboratory::modal.modal_lab_fecalysis')
@include('laboratory::modal.modal_laboratory_result')
@stop

@section('scripts')
    <div class="modal fade" id="myInfoModal" tabindex="-1" role="dialog" aria-labelledby="myInfoModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myInfoModalLabel"> Healthcare Record Preview </h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<script>
$(document).on("click", ".labClick", function () {
    var lab_dataTarget = $(this).data('target');
    var medicalorderlaboratoryexam_id = $(this).data('id');
    $(lab_dataTarget+" #medicalorderlaboratoryexam_id").val( medicalorderlaboratoryexam_id );

});
$("#myInfoModal").on("show.bs.modal", function(e) {
    $(this).find(".modal-content").html("");
    $(this).find(".modal-content").attr("style", "");
    var link = $(e.relatedTarget);
    $(this).find(".modal-content").load(link.attr("href"));
});

$(document).on("click", ".deletebutton", function (e) {
    var link = $(this).data('target');
    var mod = $(this).data('mod');
    $.ajax({
         url: link,
         dataType: 'html',
         success: function(data) {
              $("#myInfoModal").find(".modal-content").load(mod).modal('show');
         }
     });
});
</script>
@stop
