<?php

use ShineOS\Core\LOV\Entities\LovDrugs;

use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrder;
use ShineOS\Core\Healthcareservices\Entities\Diagnosis;

use ShineOS\Core\Patients\Entities\FacilityPatientUser;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\PatientAlert;
use ShineOS\Core\Patients\Entities\PatientAllergies;
use ShineOS\Core\Patients\Entities\PatientContacts;
use ShineOS\Core\Patients\Entities\PatientDisabilities;
use ShineOS\Core\Patients\Entities\PatientDeathInfo;
use ShineOS\Core\Patients\Entities\PatientEmergencyInfo;
use ShineOS\Core\Facilities\Entities\FacilityUser;

use Shine\Libraries\FacilityHelper;


/**
 * Get all health records by facility ID with available options
 * @param  INT $id  Facility ID
 * @param  INT [$order = NULL] Field name to sort from
 * @param  INT [$dir = NULL] Direction of sorting
 * @param  INT [$limit = NULL] Number of records to retrieve
 * @param  INT [$offset = NULL] Starting row to retrieve
 * @return Array Health record array
 * @package Healthcareservices
 */
function getAllHealthcareByFacilityIDwOptions($id, $search = NULL, $order = NULL, $dir = NULL, $limit = NULL, $offset = NULL) {
    $bhs=NULL;
    $sql = "deleted_at IS NULL AND hcdeleted IS NULL";

    if($search) {
        $sql .= ' AND (first_name LIKE "%'.$search.'%" OR last_name LIKE "%'.$search.'%" OR middle_name LIKE "%'.$search.'%" OR healthcareservicetype_id LIKE "%'.$search.'%" OR encounter_type LIKE "%'.$search.'%")';
    }
    if(strpos($order, 'name') > 1) {
        $sql .= ' order by '.$order.' '.$dir;
    } else {
        $sql .= ' order by '.$order.' '.$dir;
    }
    if($limit) {
        $sql .= ' limit '.$limit;
    }
    if($offset) {
        $sql .= ' offset '.$offset;
    }
    //let us check if this is a BHS account
    $user = Session::get('user_details');
    if($user->facilityUser){
        foreach($user->facilityUser as $fuser) {
            if($fuser->facility_id == $id AND $fuser->catchment_area_id != NULL) {
                //this is a BHS user
                $bhs = $fuser->catchment_area_id;
            }
        }
    }
    if(isset($bhs) AND $bhs) { //this is a BHS account
        $fus = FacilityUser::where('catchment_area_id', $bhs)->lists('facilityuser_id')->toArray();
        if($fus) {
            $healthcare = DB::table('healthcare_view')
            ->whereIn('facilityuser_id', $fus)
            ->where('deleted_at','=',NULL)
            ->whereRaw($sql)
            ->get();
        } else {
            $healthcare = NULL;
        }
    } else {
        $healthcare = DB::table('healthcare_view')
        ->where('facility_id', $id)
        ->where('deleted_at','=',NULL)
        ->whereRaw($sql)
        ->get();
    }

    return $healthcare;
}

/**
 * Get all health records by facility ID
 * @param  INT $id  Facility ID
 * @return Array Health record array
 * @package Healthcareservices
 */
function getAllHealthcareByFacilityID($id) {
    $user = Session::get('user_details');
    $bhs = NULL;
    if($user->facilityUser){
        foreach($user->facilityUser as $fuser) {
            if($fuser->facility_id == $id AND $fuser->catchment_area_id != NULL) {
                //this is a BHS user
                $bhs = $fuser->catchment_area_id;
            }
        }
    }
    if($bhs) { //this is a BHS account
        $fus = FacilityUser::where('catchment_area_id', $bhs)->lists('facilityuser_id')->toArray();
        if($fus) {
            $healthcare = DB::table('healthcare_view')
            ->whereIn('facilityuser_id', $fus)
            ->where('deleted_at', NULL)
            ->get();
        } else {
            $healthcare = NULL;
        }
    } else {
        $healthcare = DB::table('healthcare_view')
            ->where('facility_id', $id)
            ->where('deleted_at', NULL)
            ->get();
    }

    return $healthcare;
}

/**
 * Get all health records by month given date range
 * @param  date $start  Starting date Y-m-d
 * @param  date $end  Ending date Y-m-d Nullable
 * @return Array list of Health records array per month
 *
 *      healthcareservice_id      - ID of healthcare record
 *      encounter_datetime        - date of encounter
 *      patient_id                - patient ID
 *      first_name                - patient First Name
 *      last_name                 - patient Last Name
 *      seen_by                   - attending physician
 *      healthcareservicetype_id  - healthcare type ID
 * @package Healthcareservices
 */
function getAllHealthcareByDate($start, $end = NULL) {

    $facilityInfo = FacilityHelper::facilityInfo();
    $user = Session::get('user_details');
    $bhs = NULL;
    if($user->facilityUser){
        foreach($user->facilityUser as $fuser) {
            if($fuser->facility_id == $id AND $fuser->catchment_area_id != NULL) {
                //this is a BHS user
                $bhs = $fuser->catchment_area_id;
            }
        }
    }
    if($end == NULL) {
        //visits today
        $rawsql = 'DATE_FORMAT(`encounter_datetime`, "%Y-%m-%d") = "'.date('Y-m-d').'"';
    } else {
        //visits from date range
        $rawsql = 'DATE_FORMAT(`encounter_datetime`, "%Y-%m-%d") BETWEEN "'.$start.'" AND "'.$end.'"';
    }
    if($bhs) { //this is a BHS account
        $fus = FacilityUser::where('catchment_area_id', $bhs)->lists('facilityuser_id')->toArray();
        if($fus) {
            $visits = DB::table('healthcare_view')
            ->whereIn('facilityuser_id', $fus)
            ->where('deleted_at','=',NULL)
            ->whereRaw($rawsql)
            ->orderBy('encounter_datetime', 'DESC')
            ->get();
        } else {
            $visits = DB::table('healthcare_view')
            ->where('facility_id', $facilityInfo->facility_id)
            ->where('deleted_at','=',NULL)
            ->whereRaw($rawsql)
            ->orderBy('encounter_datetime', 'DESC')
            ->get();
        }
        foreach ($visits as $k => $v) {
            $v->seen_by = findUserByFacilityUserID($v->seen_by);
        }
    } else {
        $visits = NULL;
    }

    return $visits;
}

/**
 * Get all health records by month range
 * @param  date $start  Starting date Y-m-d
 * @param  date $end  Ending date Y-m-d Nullable
 * @return Array list of Health records array per month
 *      healthcareservice_id      - ID of healthcare record
 *      encounter_datetime        - date of encounter
 *      patient_id                - patient ID
 *      first_name                - patient First Name
 *      last_name                 - patient Last Name
 *      seen_by                   - attending physician
 *      healthcareservicetype_id  - healthcare type ID
 * @package Healthcareservices
 */
function getCountHealthcareByMonth($start, $end) {

    $facilityInfo = FacilityHelper::facilityInfo();
    $user = Session::get('user_details');
    $bhs = NULL;
    if($user->facilityUser){
        foreach($user->facilityUser as $fuser) {
            if($fuser->facility_id == $id AND $fuser->catchment_area_id != NULL) {
                //this is a BHS user
                $bhs = $fuser->catchment_area_id;
            }
        }
    }
    //visits from date range
    $daterangeRaw = 'DATE_FORMAT(`encounter_datetime`, "%Y-%m-%d") BETWEEN "'.$start.'" AND "'.$end.'"';

    if($bhs) { //this is a BHS account
        $fus = FacilityUser::where('catchment_area_id', $bhs)->lists('facilityuser_id')->toArray();
        if($fus) {
            $visits = DB::table('healthcare_view')
                ->select(DB::raw('MONTHNAME(`encounter_datetime`) AS monther , YEAR(`encounter_datetime`) AS yearer, DATE_FORMAT(`encounter_datetime`, "%Y-%m-%d") AS encounter_date, COUNT(*) as counter'))
                ->whereIn('facilityuser_id', $fus)
                ->where('deleted_at','=',NULL)
                ->whereRaw($daterangeRaw)
                ->groupBy('monther','yearer')
                ->orderBy('encounter_date', 'ASC')
                ->get();

        } else {
            $visits = NULL;
        }
    } else {
        $visits = DB::table('healthcare_view')
        ->select(DB::raw('MONTHNAME(`encounter_datetime`) AS monther , YEAR(`encounter_datetime`) AS yearer, DATE_FORMAT(`encounter_datetime`, "%Y-%m-%d") AS encounter_date, COUNT(*) as counter'))
        ->where('facility_id', $facilityInfo->facility_id)
        ->where('deleted_at','=',NULL)
        ->whereRaw($daterangeRaw)
        ->groupBy('monther','yearer')
        ->orderBy('encounter_date', 'ASC')
        ->get();
    }
    return $visits;
}

/**
 * Get all health records by diagnosis
 * @param  date $start  Starting date Y-m-d
 * @param  date $end  Ending date Y-m-d Nullable
 * @return Array list of diagnosis and total count
 * @package Healthcareservices
 */
function getCountByDiagnosis($start, $end, $top = 10) {

    $facilityInfo = FacilityHelper::facilityInfo();
    $user = Session::get('user_details');
    $bhs = NULL;
    if($user->facilityUser){
        foreach($user->facilityUser as $fuser) {
            if($fuser->facility_id == $id AND $fuser->catchment_area_id != NULL) {
                //this is a BHS user
                $bhs = $fuser->catchment_area_id;
            }
        }
    }
    if($bhs) { //this is a BHS account
        $fus = FacilityUser::where('catchment_area_id', $bhs)->lists('facilityuser_id')->toArray();
        if($fus) {
            $data['diagnosis'] = DB::table('diagnosis_view')
                ->select('diagnosislist_id', DB::raw('count(*) as bilang'))
                ->whereIn('facilityuser_id', $fus)
                ->where('hccreated', '<=', $end)
                ->where('hccreated', '>=', $start)
                ->groupBy('diagnosislist_id')
                ->orderBy('bilang', 'desc')
                ->take($top)
                ->get();

            $data['totalCount'] = DB::table('diagnosis_view')
                ->whereIn('facilityuser_id', $fus)
                ->where('hccreated', '<=', $end)
                ->where('hccreated', '>=', $start)
                ->count();
        } else {
            $data['diagnosis'] = NULL;
            $data['totalCount'] = NULL;
        }
    } else {
        $data['diagnosis'] = DB::table('diagnosis_view')
            ->select('diagnosislist_id', DB::raw('count(*) as bilang'))
            ->where('facility_id', $facilityInfo->facility_id)
            ->where('hccreated', '<=', $end)
            ->where('hccreated', '>=', $start)
            ->groupBy('diagnosislist_id')
            ->orderBy('bilang', 'desc')
            ->take($top)
            ->get();

        $data['totalCount'] = DB::table('diagnosis_view')
            ->where('facility_id', $facilityInfo->facility_id)
            ->where('hccreated', '<=', $end)
            ->where('hccreated', '>=', $start)
            ->count();
    }

    return $data;
}

/**
 * Get count of specific Diagnosis with date range
 * @param  date $start  Starting date Y-m-d
 * @param  date $end  Ending date Y-m-d Nullable
 * @param  string $diagnosis Diagnosis to count
 * @return int Count of Diagnosis
 * @package Healthcareservices
 */
function getCountOfSpecificDiagnosis($start, $end, $diagnosis) {

    $facilityInfo = FacilityHelper::facilityInfo();
    $user = Session::get('user_details');
    $bhs = NULL;
    if($user->facilityUser){
        foreach($user->facilityUser as $fuser) {
            if($fuser->facility_id == $id AND $fuser->catchment_area_id != NULL) {
                //this is a BHS user
                $bhs = $fuser->catchment_area_id;
            }
        }
    }
    if($bhs) { //this is a BHS account
        $fus = FacilityUser::where('catchment_area_id', $bhs)->lists('facilityuser_id')->toArray();
        if($fus) {
            $count = DB::table('diagnosis_view')
                ->whereIn('facilityuser_id', $fus)
                ->where('hccreated', '<=', $end)
                ->where('hccreated', '>=', $start)
                ->where('diagnosislist_id', 'LIKE', '%'. $diagnosis . '%')
                ->count();
        } else {
            $count = 0;
        }
    } else {
        $count = DB::table('diagnosis_view')
        ->where('facility_id', $facilityInfo->facility_id)
        ->where('hccreated', '<=', $end)
        ->where('hccreated', '>=', $start)
        ->where('diagnosislist_id', 'LIKE', '%'. $diagnosis . '%')
        ->count();
    }

    return $count;
}

/**
 * Count all health records by facility with available options
 * @param  INT $id  Facility ID
 * @return INT Number of Health records
 * @package Healthcareservices
 */
function countAllHealthcareByFacilityID($id, $start=NULL, $end=NULL) {
    $bhs = NULL;
    if($end == NULL) {
        $rawsql = 'DATE_FORMAT(`encounter_datetime`, "%Y-%m-%d") <= "'.date('Y-m-d').'"';
    } else {
        $rawsql = 'DATE_FORMAT(`encounter_datetime`, "%Y-%m-%d") BETWEEN "'.$start.'" AND "'.$end.'"';
    }
    $user = Session::get('user_details');
    if($user->facilityUser){
        foreach($user->facilityUser as $fuser) {
            if($fuser->facility_id == $id AND $fuser->catchment_area_id != NULL) {
                //this is a BHS user
                $bhs = $fuser->catchment_area_id;
            }
        }
    }
    if($bhs) {
        $fus = FacilityUser::where('catchment_area_id', $bhs)->lists('facilityuser_id')->toArray();
        $healthcare = DB::table('healthcare_view')
        ->whereIn('facilityuser_id', $fus)
        ->whereRaw($rawsql)
        ->where('deleted_at', '=', NULL)
        ->count();
    } else {
        $healthcare = DB::table('healthcare_view')
        ->where('facility_id', $id)
        ->whereRaw($rawsql)
        ->where('deleted_at', '=', NULL)
        ->count();
    }

    return $healthcare;
}

/**
 * Get the diagnosis details by Health service ID
 * @param  INT $id  Health service ID
 * @return Object Array Details of diagnosis
 * @package Healthcareservices
 */
function getDiagnosisDetailsByHealthServiceID($id)
{
    $diagnosis = DB::table('diagnosis')
        ->where('diagnosis.healthcareservice_id', $id)
        ->first();
    if($diagnosis) {
        return $diagnosis;
    } else {
        return false;
    }
}

/**
 * Get the name diagnosis by Health service ID
 * @param  INT $id  Health service ID
 * @return String Name of diagnosis
 * @package Healthcareservices
 */
function getDiagnosisByHealthServiceID($id)
{
    $diagnosis = DB::table('diagnosis')
        ->where('diagnosis.healthcareservice_id', $id)
        ->first();
    if($diagnosis) {
        return $diagnosis->diagnosislist_id;
    } else {
        return "No diagnosis given";
    }
}

/**
 * Get medical order by health service ID
 * @param  Int $id  Health service ID
 * @return String Medical Order name or title
 * @package Healthcareservices
 */
function getMedicalOrderByHealthServiceID($id)
{
    $data = DB::table('medicalorder')
        ->where('medicalorder.healthcareservice_id', $id)
        ->first();

    if($data) {
        switch($data->medicalorder_type){
            case "MO_MED_PRESCRIPTION": return "Prescription"; break;
            case "MO_LAB_TEST": return "Laboratory Examination"; break;
            case "MO_PROCEDURE": return "Medical Procedure"; break;
            case "MO_IMMUNIZATION": return "Immunization"; break;
            case "MO_OTHER": return "Other"; break;
        }
    } else {
        return "No medical order given.";
    }
}


/**
 * Get medical orders by health service ID
 * @param  Int $id  Health service ID
 * @return Object Array Medical Orders
 * @package Healthcareservices
 */
function getMedicalOrdersByHealthServiceID($id)
{
    $data = MedicalOrder::with('MedicalOrderLabExam')->with('MedicalOrderPrescription')->with('MedicalOrderProcedure')->where('healthcareservice_id', $id)->get();
    $d = json_encode($data);

    if($data) {
        return json_decode($d);
    } else {
        return false;
    }
}


/**
 * Get product code of drug by product generic name
 * @param  String $name 
 * @return String product code
 * @package Healthcareservices
 */
function getProductCodeByProductName($name)
{
    $data = LovDrugs::where('drug_specification',$name)->first();

    if($data) {
        return $data->product_id;
    } else {
        return false;
    }
}


/**
 * Get the disposition by Health service ID
 * @param  INT $id  Health service ID
 * @return Array of disposition
 * @package Healthcareservices
 */
function getDispositionByHealthServiceID($id)
{
    $disposition = DB::table('disposition')
        ->where('disposition.healthcareservice_id', $id)
        ->whereNotNull('disposition.disposition')
        ->first();
    if($disposition) {
        return $disposition;
    } else {
        return NULL;
    }
}

/**
 * Get the general consultation by Health service ID
 * @param  INT $id  Health service ID
 * @return Array of disposition
 * @package Healthcareservices
 */
function getGenConsultationByHealthServiceID($id)
{
    $genconsult = DB::table('general_consultation')
        ->where('general_consultation.healthcareservice_id', $id)
        ->first();
    if($genconsult) {
        return $genconsult;
    } else {
        return NULL;
    }
}

/**
 * Find Health Record by Service ID
 * @param  INT $serviceID Health service ID
 * @return Array Health Record array
 * @package Healthcareservices
 */
function findHealthRecordByServiceID($serviceID)
{
    $healthrecord = DB::table('healthcare_services')
        ->join('facility_patient_user', 'healthcare_services.facilitypatientuser_id', '=', 'facility_patient_user.facilitypatientuser_id')
        ->where('healthcare_services.healthcareservice_id', $serviceID)
        ->where('healthcare_services.deleted_at', NULL)
        ->where('facility_patient_user.deleted_at', NULL)
        ->first();
        
        return $healthrecord;
}

/**
 * Find Health Record by Parent Record ID
 * @param  INT $parentID Patient ID
 * @return Array Health Record array
 * @package Healthcareservices
 */
function findHealthRecordChild($parentID)
{
    $healthrecord = DB::table('healthcare_services')
        ->join('facility_patient_user', 'healthcare_services.facilitypatientuser_id', '=', 'facility_patient_user.facilitypatientuser_id')
        ->where('healthcare_services.parent_service_id', $parentID)
        ->where('healthcare_services.deleted_at', NULL)
        ->where('facility_patient_user.deleted_at', NULL)
        ->first();

        return $healthrecord;
}

/**
 * Get Complete Health Record by Service ID
 * @param  INT $serviceID Health service ID
 * @return Array Health Record array
 * @package Healthcareservices
 */
function getCompleteHealthRecordByServiceID($serviceID)
{
    $healthrecord = DB::table('healthcare_view')
        ->where('healthcareservice_id', $serviceID)
        ->where('deleted_at','=',NULL)
        ->get();

        return $healthrecord;
}

/**
 * Find Health Record by Patient ID
 * @param  INT $patientID Patient ID
 * @return Array Health Record array
 * @package Healthcareservices
 */
function findHealthRecordByPatientID($patientID)
{
    $healthrecord = Patients::with(['healthcareservices' => function ($query) {
            $query->orderBy('created_at', 'desc');

        }],'patientAlert','patientContact','patientDeathInfo','patientEmergencyInfo')->where('patient_id','=', $patientID)->first();

        return $healthrecord;
}

/**
 * Get Health Service Name
 * @param  varchar $healthcareservicetype_id Healthcareservice Type ID
 * @return string Healthcareservice Type Name
 * @package Healthcareservices
 */
function getHealthcareServiceName($healthcareservicetype_id)
{
    if($healthcareservicetype_id == 'GeneralConsultation') {
        return "General/Family Medicine";
    } else {
        return $healthcareservicetype_id;
    }
}

/**
 * Get Medical Category Name with Category ID
 * @param  varchar $medicalcategory_id Category ID
 * @return string Medical Category Name
 * @package Healthcareservices
 */
function getMedicalCategoryName($medicalcategory_id)
{
    $medcat = DB::table('lov_medicalcategory')
            ->where('medicalcategory_id', '=', $medicalcategory_id)
            ->first();
    if($medcat) {
        echo $medcat->medicalcategory_name;
    } else {
        return NULL;
    }
}

/**
 * Get Consultation Type Name with Type ID
 * @param  varchar $ID Type ID
 * @return string Consultation Type Name
 * @package Healthcareservices
 */
function getConsultTypeName($ID)
{
    switch($ID)
    {
        case "ADMIN": return "New Admisssion"; break;
        case "CONSU": return "New Consultation"; break;
        case "FOLLO": return "Followup"; break;
    }
}

/**
 * Get Regimen Name with ID
 * @param  varchar $ID Regimen ID
 * @return string Regimen Name
 * @package Healthcareservices
 */
function getRegimenName($ID)
{
    switch($ID)
    {
        case 'OD': return 'Once a day'; break;
        case 'BID': return '2 x a day - Every 12 hours'; break;
        case 'TID': return '3 x a day - Every 8 hours'; break;
        case 'QID': return '4 x a day - Every 6 hours'; break;
        case 'QOD': return 'Every other day'; break;
        case 'QHS': return 'Every bedtime'; break;
        case 'PRM': return 'As needed'; break;
        case 'OTH': return 'Others'; break;
    }
}

/**
 * Get Intake Name with ID
 * @param  varchar $ID Intake ID
 * @return string Intake Name
 * @package Healthcareservices
 */
function getIntakeName($ID)
{
    switch($ID)
    {
        case 'D': return 'Days'; break;
        case 'M': return 'Months'; break;
        case 'Q': return 'Quarters'; break;
        case 'W': return 'Weeks'; break;
        case 'Y': return 'Years'; break;
        case 'C': return 'For Maintenance or Continuous'; break;
        case 'O': return 'Others'; break;
    }
}

/**
 * Parse Intake
 * @param string $intake Chosen Intake
 * @return string Intake Name
 * @package Healthcareservices
 */
function parseIntakeName($intake)
{
    $id = explode(" ", $intake);

    if( isset($id[1]) ) {
        switch($id[1])
        {
            case 'D': return $id[0].' Days'; break;
            case 'M': return $id[0].' Months'; break;
            case 'Q': return $id[0].' Quarters'; break;
            case 'W': return $id[0].' Weeks'; break;
            case 'Y': return $id[0].' Years'; break;
        }
    } else {
        switch($id[0])
        {
            case 'C': return 'Maintenance or Continuous'; break;
            case 'O': return 'Others'; break;
        }
    }
}

/**
 * Get Encounter Name with ID
 * @param  varchar $ID Encounter ID
 * @return string Encounter Name
 * @package Healthcareservices
 */
function getEncounterName($ID)
{
    switch($ID)
    {
        case "O": return "Out Patient"; break;
        case "I": return "In Patient"; break;
    }
}

/**
 * Get Diagnosis Type Name with ID
 * @param  varchar $ID Diagnosis ID
 * @return string Diagnosis Type Name
 * @package Healthcareservices
 */
function getDiagnosisTypeName($ID)
{
    switch($ID)
    {
        case 'ADMDX': return 'Admitting diagnosis'; break;
        case 'CLIDI': return 'Clinical diagnosis'; break;
        case 'FINDX': return 'Final Diagnosis'; break;
        case 'OTHER': return 'Other Diagnosis'; break;
        case 'WODIA': return 'Working Diagnosis'; break;
        case 'WORDX': return 'Interim Diagnosis'; break;
    }
}

/**
 * Get Medical Order Type Name with ID
 * @param  varchar $ID Order ID
 * @return string Medical Order Name
 * @package Healthcareservices
 */
function getOrderTypeName($ID)
{
    switch($ID)
    {
        case "MO_LAB_TEST": return "Laboratory Examination"; break;
        case "MO_MED_PRESCRIPTION": return "Prescription"; break;
        case "MO_IMMUNIZATION": return "Immunization"; break;
        case "MO_PROCEDURE": return "Medical Procedure"; break;
        case "MO_OTHERS": return "Other"; break;
    }
}

/**
 * Get Disposition Name with ID
 * @param  varchar $ID Dispostion ID
 * @return string Disposition Type Name
 * @package Healthcareservices
 */
function getDispositionName($ID)
{
    switch($ID)
    {
        case 'ADMDX': return 'Admitted'; break;
        case 'HOME': return 'Sent Home'; break;
        case 'ABS': return 'Absconded'; break;
        case 'HAMA': return 'Home Against Medical Advise'; break;
        case 'REFER': return 'Referred'; break;
    }
}

/**
 * Get Discharge Name with Type ID
 * @param  varchar $ID Discharge ID
 * @return string Discharge Name
 * @package Healthcareservices
 */
function getDischargeName($ID)
{
    switch($ID)
    {
        case 'IMPRO': return 'Improved'; break;
        case 'RECOV': return 'Recovered'; break;
        case 'UNIMP': return 'Unimproved'; break;
        case 'UNKNW': return 'Unknown'; break;
    }
}


function countByService($id,$service) {
    $healthcare = DB::table('healthcare_view')
        ->where('facility_id', $id)
        ->where('healthcareservicetype_id',$service)
        ->where('deleted_at', NULL)
        ->count();

    return $healthcare;
}