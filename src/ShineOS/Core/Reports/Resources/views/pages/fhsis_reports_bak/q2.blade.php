<?php
$population = 0;

if(isset($geodata))
{
    $population = $geodata->population;
}

?>

@extends('reports::layouts.fhsis_master')

@section('heads')
{!! HTML::style('public/dist/plugins/stickytableheader/css/component.css') !!}
<style>
    #diseaseTable tr th {
        width: 65px;
    }
</style>
@stop
@section('reportGroup')FHSIS @stop
@section('reportTitle')FHSIS Q2 @stop
@section('content')
    <!-- Main content -->
    <section class="fhsis">
      <!-- title row -->
      <div class="row report-header">
        <div class="col-xs-6">
            <h4>Morbidity Disease Report Q2</h4>
        </div>


        <table class="table table-striped table-bordered table-report">
            <tbody><tr><th class="thleft">FHSIS Report Month/Year</th><td>
              {!! Form::open(array( 'url'=>'reports/fhsis/q2/', 'id'=>'dateFilter', 'name'=>'dateFilter', 'class'=>'form-horizontal' )) !!}
                  <label class="col-sm-2 control-label">Quarter</label>
                  <div class="col-sm-3">
                      <select name="quarter" class="form-control" id="quarter">
                        <option value="" selected="selected"></option>
                        <option value="03" @if($quarter <= '3') selected='selected'@endif >Q1</option>
                        <option value="06" @if($quarter <= '6' && $quarter > '3') selected='selected'@endif >Q2</option>
                        <option value="09" @if($quarter <= '9' && $quarter > '6') selected='selected'@endif >Q3</option>
                        <option value="12" @if($quarter <= '12' && $quarter > '9') selected='selected'@endif >Q4</option>
                      </select>
                  </div>
                  <label class="col-sm-2 control-label">Year</label>
                  <div class="col-sm-3">
                      <?php $thisyear = date('Y'); ?>
                      <select name="year" class="form-control" id="year">
                          @for( $y=$thisyear-5; $y<=$thisyear; $y++)
                          <option @if($year == $y) selected='selected'@endif >{{ $y }}</option>
                          @endfor
                      </select>
                  </div>
                  <input type="submit" class="btn btn-primary" value="VIEW">
              {!! Form::close() !!}
            </td></tr>
            <tr><th class="thleft">City/Municipality of</th><td>{{ getCityName($facility->facility_contact->city) }}</td></tr>
            <tr><th class="thleft">Province of</th><td>{{ getProvinceName($facility->facility_contact->province) }}</td></tr>
            <tr><th class="thleft">Projected Population of the Year</th><td>{{ $population }}</td></tr>
            </tbody>
        </table><!-- /table details -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          @include('reports::pages.fhsis_reports.disease_table')
        </div><!-- /.col -->
      </div><!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="fhsis-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button class="btn btn-primary pull-right hidden" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
        </div>
      </div>
    </section><!-- /.content -->
@stop

@section('scripts')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js"></script>
    {!! HTML::script('public/dist/plugins/stickytableheader/js/jquery.stickyheader.js') !!}
@stop
