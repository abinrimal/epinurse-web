<!-- Modal form-->
<div class="modal fade bs-example-modal-lg" id="lab_urinalysis" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      {!! Form::open(array('url' => 'laboratory/add', 'enctype'=>'multipart/form-data')) !!}
        <div class="modal-header with-border bg-yellow">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-flask"></i> Laboratory Result </h4>
        </div>
        <div class="modal-body" id="modal-body">
          @include('laboratory::content.lab_urinalysis')
        </div>
        <div class="modal-footer" id="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
<!-- end of modal -->
