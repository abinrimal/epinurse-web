<?php namespace ShineOS\Core\PHIE\Http\Middleware;

use Artisaninweb\SoapWrapper\Facades\SoapWrapper;
use Shine\Libraries\FacilityHelper;
use SoapClient, SoapVar, SoapHeader, aes256_cbc_encrypt, Session;

class PHIE_Soap_Connect {

    public function __construct(){
        $this->wsdl = getenv('WSDL'); //wsdl
        if(Session::get('_global_phie_type')=='batch' OR Session::get('_global_phie_type')=='check' OR Session::get('_global_phie_type')=='single' OR Session::get('_global_phie_type')=='singles' OR Session::get('_global_phie_type')=='singlex') {
            $this->facilityInfo = Session::get('_batch_facility');
            $this->wskey = $this->facilityInfo->wskey; // Web Service Key
            $this->ekey = $this->facilityInfo->ekey; // Encrption Key
        } else {
            $this->facilityInfo = FacilityHelper::facilityInfo();
            $this->wskey = $this->facilityInfo->wskey; // Web Service Key
            $this->ekey = $this->facilityInfo->ekey; // Encrption Key
        }
    }

    public function demo()
    {
        $serviceData = array('wskey'        =>  $this->wskey,
                            'wsdl'          =>  $this->wsdl,
                            'public_key'    =>  $this->public_key
                            );

        $headerBody = array('WSKey'         =>  $this->wskey,
                            'ReturnFormat'  =>  'json'
                            );

        SoapWrapper::add(function ($service) use ($serviceData,$headerBody) {
            $service->name('push_PHIEData')
                    ->certificate($serviceData['public_key'])
                    ->header($serviceData['wsdl'], 'Authentication', $headerBody)
                    ->wsdl('http://uhmistrn.doh.gov.ph/phie/webservice/index.php?wsdl');
        });

        // Using the added service
        SoapWrapper::service('push_PHIEData', function ($service) use ($data) {
            $data['getFunctions'] = $service->getFunctions();
            $data['WS_Check'] = $service->call('WS_Check', $data);
            $data['push_PHIEData'] = $service->call('push_PHIEData', $data);

            return $data;
        });
    }

    public function phielitetest() {
        // WSKey is the authentication key (login id) for PHIE Web Service.
        $wskey = $this->wskey; // Web Service Key
        $ekey = $this->ekey; // Encrption Key;

        // Initialize Soap Client
        $server_wsdl = $this->wsdl;
        // $soap_server = new SoapClient($this->wsdl, ['trace' => true, 'cache_wsdl' => WSDL_CACHE_MEMORY]);

        $streamContext = stream_context_create(array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
             ),
             'http'=>array(
                'user_agent' => 'PHPSoapClient'
             )
        ));
        $option=array(
                'trace'=>1,
                'stream_context' => $streamContext,
                'connection_timeout' => 1800,
                'cache_wsdl' => WSDL_CACHE_MEMORY,
                'keep_alive' => true,
                'exceptions' => 1
            );

        // Declare Soap Header to include in Soap Request
        // AuthenticationKey : Required Parameter for PHIE Soap Header.

        $headerBody = array(
                'WSKey'=>$wskey,
                'ReturnFormat'=>'json' // json or xml
            );
        $headerBody = new SoapVar($headerBody,SOAP_ENC_OBJECT);
        $soap_header = new SoapHeader($server_wsdl,'Authentication',$headerBody);

        /* ---------------- Call Soap Method (Web Service Method) -------------------- */

        $wsMethod = "WS_Check"; // Web Service Method (Function) to Call

        try{
            $soap_server = new SoapClient($this->wsdl,$option);
            // $output_headers - catch soap response header as array.
            // $soap_result - catch soap response body.
            $soap_result = $soap_server->__soapCall($wsMethod, array(), NULL,$soap_header, $output_headers);

            /* -- Get Soap Response Header -- */
            $Response_Code = $output_headers['response_code'];
            $Response_Desc = $output_headers['response_desc'];
            $Response_DateTime = $output_headers['response_datetime'];

            // Decrypt $soap_result
            $soap_result = $this->aes256_cbc_decrypt($ekey,base64_decode($soap_result));
            // Display Result
            return array('soap_result'=>$soap_result, 'response_code'=>$Response_Code, 'response_desc'=>$Response_Desc, 'response_datetime'=>$Response_DateTime);

        } catch (SoapFault $fault) {
            return array('soap_result' => $fault);
        }
    }

    public function phieGetMaster($year) {
        // WSKey is the authentication key (login id) for PHIE Web Service.
        $wskey = $this->wskey; // Web Service Key
        $ekey = $this->ekey; // Encrption Key;

        // Initialize Soap Client
        $server_wsdl = $this->wsdl;

        $streamContext = stream_context_create(array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
             ),
             'http'=>array(
                'user_agent' => 'PHPSoapClient'
             )
        ));
        $option=array(
                'trace'=>1,
                'stream_context' => $streamContext,
                'connection_timeout' => 1800,
                'cache_wsdl' => WSDL_CACHE_MEMORY,
                'keep_alive' => true,
                'exceptions' => 1
            );

        // Declare Soap Header to include in Soap Request
        // AuthenticationKey : Required Parameter for PHIE Soap Header.

        $headerBody = array(
                'WSKey'=>$wskey,
                'ReturnFormat'=>'json' // json or xml
            );
        $headerBody = new SoapVar($headerBody,SOAP_ENC_OBJECT);
        $soap_header = new SoapHeader($server_wsdl,'Authentication',$headerBody);

        /* ---------------- Call Soap Method (Web Service Method) -------------------- */

        $wsMethod = "get_upcmAssignmentList"; // Web Service Method (Function) to Call
        $wsParam = array(
             'Effectivity_Year' => $year
        );

        try{
            $soap_server = new SoapClient($this->wsdl,$option);
            $soap_result = $soap_server->__soapCall($wsMethod, array($wsParam), NULL,$soap_header, $output_headers);

            /* -- Get Soap Response Header -- */
            $Response_Code = $output_headers['response_code'];
            $Response_Desc = $output_headers['response_desc'];
            $Response_DateTime = $output_headers['response_datetime'];

            // Decrypt $soap_result
            $soap_result = $this->aes256_cbc_decrypt($ekey,base64_decode($soap_result));
            // Display Result
            return array('soap_result'=>$soap_result, 'response_code'=>$Response_Code, 'response_desc'=>$Response_Desc, 'response_datetime'=>$Response_DateTime);

        } catch (SoapFault $fault) {
            return array('soap_result' => $fault);
        }
    }

    public function phieGetSAP($quarter, $year) {
        // WSKey is the authentication key (login id) for PHIE Web Service.
        $wskey = $this->wskey; // Web Service Key
        $ekey = $this->ekey; // Encrption Key;

        // Initialize Soap Client
        $server_wsdl = $this->wsdl;

        $streamContext = stream_context_create(array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
             ),
             'http'=>array(
                'user_agent' => 'PHPSoapClient'
             )
        ));
        $option=array(
                'trace'=>1,
                'stream_context' => $streamContext,
                'connection_timeout' => 1800,
                'cache_wsdl' => WSDL_CACHE_MEMORY,
                'keep_alive' => true,
                'exceptions' => 1
            );

        // Declare Soap Header to include in Soap Request
        // AuthenticationKey : Required Parameter for PHIE Soap Header.

        $headerBody = array(
            'WSKey'=>$wskey,
            'ReturnFormat'=>'json' // json or xml
        );
        $headerBody = new SoapVar($headerBody,SOAP_ENC_OBJECT);
        $soap_header = new SoapHeader($server_wsdl,'Authentication',$headerBody);

        /* ---------------- Call Soap Method (Web Service Method) -------------------- */

        $wsMethod = "get_upcmBilling"; // Web Service Method (Function) to Call
        $wsParam = array(
             'Billing_Quarter' => $quarter,
             'Billing_Year' => $year
        );

        try{
            $soap_server = new SoapClient($this->wsdl,$option);
            $soap_result = $soap_server->__soapCall($wsMethod, array($wsParam), NULL,$soap_header, $output_headers);

            /* -- Get Soap Response Header -- */
            $Response_Code = $output_headers['response_code'];
            $Response_Desc = $output_headers['response_desc'];
            $Response_DateTime = $output_headers['response_datetime'];

            // Decrypt $soap_result
            $soap_result = $this->aes256_cbc_decrypt($ekey,base64_decode($soap_result));
            // Display Result
            return array('soap_result'=>$soap_result, 'response_code'=>$Response_Code, 'response_desc'=>$Response_Desc, 'response_datetime'=>$Response_DateTime);

        } catch (SoapFault $fault) {
            return array('soap_result' => $fault);
        }
    }

    public function phie_test_two($dataValue, $data, $method=NULL) {
    //web service client call sample from http://phielitetest.doh.gov.ph/webservice/#WebServiceCallSample
        libxml_disable_entity_loader(false);
        // WSKey is the authentication key (login id) for PHIE Web Service.
        $wskey = $this->wskey; // Web Service Key
        $ekey = $this->ekey; // Encrption Key;

        // Initialize Soap Client
        $server_wsdl = $this->wsdl;

        $streamContext = stream_context_create(array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
             ),
             'http'=>array(
                'user_agent' => 'PHPSoapClient'
             )
        ));
        $option=array(
                'trace'=>1,
                'stream_context' => $streamContext,
                'connection_timeout' => 1800,
                'cache_wsdl' => WSDL_CACHE_MEMORY,
                'keep_alive' => true,
                'exceptions' => 1
            );

        // Declare Soap Header to include in Soap Request
        // AuthenticationKey : Required Parameter for PHIE Soap Header.
        $headerBody = array(
                'WSKey'=>$wskey,
                'ReturnFormat'=>'json' // json or xml
            );
        $headerBody = new SoapVar($headerBody,SOAP_ENC_OBJECT);
        $soap_header = new SoapHeader($server_wsdl,'Authentication',$headerBody);

        /* ---------------- Call Soap Method (Web Service Method) -------------------- */

        if($method) {
            $wsMethod = $method; // Web Service Method (Function) to Call
            $wsParam = $data; // Web Service Parameter
        } else {
            $wsMethod = "push_PHIEData"; // Web Service Method (Function) to Call
            $wsParam = array(
                 'Type' => $dataValue,
                 'Data' => $data
            ); // Web Service Parameter
        }

        // Encrypt $wsParam
        $wsParam = $ec = $this->aes256_cbc_encrypt($ekey,$wsParam);
        // Convert to Base64
        $wsParam = $eb = base64_encode($wsParam);

        $soap_server = new SoapClient($this->wsdl,$option);

        try{
            $soap_result = $soap_server->__soapCall($wsMethod, array($wsParam), NULL,$soap_header, $output_headers);

            /* -- Get Soap Response Header -- */
            $Response_Code = $output_headers['response_code'];
            $Response_Desc = $output_headers['response_desc'];
            $Response_DateTime = $output_headers['response_datetime'];

            // Decrypt $soap_result
            $soap_result = $this->aes256_cbc_decrypt($ekey,base64_decode($soap_result));
            // return Result
/*if($Response_Code == '103') {
    dd($Response_Desc, $dataValue, $data, array($wsParam), $soap_result);
}*/
            return array('soap_result'=>$soap_result, 'sentdata' => $data, 'response_code'=>$Response_Code, 'response_desc'=>$Response_Desc, 'response_datetime'=>$Response_DateTime);

        } catch (\SoapFault $fault) {
            echo $soap_server->__getLastResponse();
        }
    }

    /*
     @param string $key     [ Encryption Key Provided by PHIE ]
     @param string $data    [ String / Array to be Encrypted: Note: Array will be converted to json format to allow encryption]
     @return Encrypted String
    */

    public function aes256_cbc_encrypt($key, $data) {
        $iv = $key;
        if(32 !== strlen($key)) $key = hash('SHA256', $key, true);
        if(16 !== strlen($iv)) $iv = hash('MD5', $iv, true);

        if(is_array($data))
        {
        $data = json_encode($data);
        }

        $padding = 16 - (strlen($data) % 16);
        $data .= str_repeat(chr($padding), $padding);
        return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv);
    }

    /*
    @param string $key     [ Encryption Key Provided by PHIE ]
    @param string $data       [ Encrypted Data to be Decrypted ]
    @return Decrypted String
    */
    function aes256_cbc_decrypt($key, $data)
    {
       $iv = $key;
       if(32 !== strlen($key)) $key = hash('SHA256', $key, true);
       if(16 !== strlen($iv)) $iv = hash('MD5', $iv, true);
       $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv);
       $padding = ord($data[strlen($data) - 1]);
       return substr($data, 0, -$padding);
    }
}
