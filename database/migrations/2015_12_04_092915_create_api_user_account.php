<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Config;

class CreateApiUserAccount extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $mode = Config::get('config.mode');

        if($mode=='cloud') {
            if (Schema::hasTable('api_user_account')!=TRUE) {
                Schema::create('api_user_account', function(Blueprint $table)
                {
                    $table->increments('id');
                    $table->string('apiuseraccount_id',60);
                    $table->string('first_name',100)->nullable();
                    $table->string('middle_name',100)->nullable();
                    $table->string('last_name',100)->nullable();
                    $table->enum('gender', array('F', 'M', 'U'))->nullable();
                    $table->string('api_purpose',250)->nullable();
                    $table->string('email',60)->nullable();            
                    $table->string('password',60)->nullable();

                    $table->string('api_key',100)->nullable();
                    $table->string('api_secret',100)->nullable();
                    
                    $table->softDeletes();
                    $table->timestamps();
                    $table->unique('apiuseraccount_id');
                }); 
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_user_account'); 
    }

}
