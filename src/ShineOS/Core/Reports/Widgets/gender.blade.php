<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Patients by Gender</h3>
    </div>
    @if($total_patients_by_sex)
    <div class="box-body">
        <canvas id="patpie" height="140"></canvas>

        @foreach ($total_patients_by_sex as $sex)
          <div class="progress-group">
            <?php if ($sex->genderu == 'M' )
              {
                  $color = "bg-red";
                  $tag = "Male";
              }
              else if ($sex->genderu == 'F' )
              {
                  $color = "bg-yellow";
                  $tag = "Female";
              } else {
                  $color = "bg-blue";
                  $tag = "Unknown";
                  $sex->genderu = 'U';
              }
                $percent = ($sex->total/$patient_count)*100;
            ?>
            <span class="progress-text"><a href="{{ url('records/sex/'.$sex->genderu) }}" class="btn btn-xs bg-black white">View</a> {{ $tag }}</span>
            <span class="progress-number"><b>{{ $sex->total }}</b></span>
            <div class="progress sm">
              <div class="progress-bar {{ $color }}" style="width: {{ $percent }}%"></div>
            </div>
          </div>
        @endforeach
    </div>
    <div class="box-footer text-center">
      <a href="{{ url('/patients') }}" class="uppercase hidden">View All Patients</a>
    </div>
    @else
    <div class="box-body">
        <h4>No records yet.</h4>
    </div>
    <div class="box-footer text-center">
      <a href="{{ url('/records') }}" class="uppercase hidden">Create a record</a>
    </div>
    @endif
</div>

<!-- Chart /dashboard-page-->
<script type="text/javascript">

        var patpie = $("#patpie").get(0).getContext("2d");

        var patpieData = {
            labels: [
                        <?php foreach ($total_patients_by_sex as $sex) {
                            if ($sex->genderu == 'M' ) {
                                  $ctag = "Male";
                              } elseif ($sex->genderu == 'F' ) {
                                  $ctag = "Female";
                              } else {
                                  $ctag = "Unknown";
                              } ?>

                            "<?php echo $ctag; ?>",
                        <?php } ?>
            ],
            datasets: [
                {
                    data: [
                        <?php foreach ($total_patients_by_sex as $sex) { ?>
                            <?php echo $sex->total; ?>,
                        <?php } ?>
                    ],
                    backgroundColor: [
                        <?php foreach ($total_patients_by_sex as $sex) {
                              if ($sex->genderu == 'M' ) {
                                  $ccolor = "#F7464A";
                              } elseif ($sex->genderu == 'F' ) {
                                  $ccolor = "#FCD200";
                              } else {
                                  $ccolor = "rgb(60, 141, 188)";
                              } ?>

                            "<?php echo $ccolor; ?>",
                        <?php } ?>
                    ],
                    hoverBackgroundColor: [
                        <?php foreach ($total_patients_by_sex as $sex) {
                              if ($sex->genderu == 'M' ) {
                                  $ccolor = "#F7464A";
                              } elseif ($sex->genderu == 'F' ) {
                                  $ccolor = "#FCD200";
                              } else {
                                  $ccolor = "rgb(60, 141, 188)";
                              } ?>

                            "<?php echo $ccolor; ?>",
                        <?php } ?>
                    ]
            }]
        };

        $(function () {
            var genderChart = new Chart(patpie, {
                type: 'doughnut',
                data: patpieData,
                options: {
                    animation:{
                        animateScale:true
                    }
                }
            });
        });

</script>
