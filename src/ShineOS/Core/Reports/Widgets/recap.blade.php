
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Facility Data Report</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-primary btn-sm rangeonly-daterange hidden" data-toggle="tooltip" title="" data-original-title="Date range"><i class="fa fa-calendar"></i></button>
        </div>
      </div><!-- /.box-header -->
      @if($services)
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <p class="text-center">
              <strong>Top Healthcare Services Provided : 12 months</strong>
            </p>
            <div class="chart">
              <!-- Sales Chart Canvas -->
              <canvas id="servicesChart" height="200"></canvas>
            </div><!-- /.chart-responsive -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- ./box-body -->

      <!-- <div class="box-footer">
        <div class="row">
          <div class="col-sm-4 col-xs-6">
            <div class="description-block border-right">
              <h5 class="description-header">{{ $patient_count }}</h5>
              <span class="description-text">Total patients served</span>
            </div><
          </div>
          <div class="col-sm-4 col-xs-6">
            <div class="description-block border-right">
              <h5 class="description-header">{{ count($count_by_services_rendered) }}</h5>
              <span class="description-text">Total services rendered</span>
            </div>
          </div>
          <div class="col-sm-4 col-xs-6">
            <div class="description-block border-right">
              <h5 class="description-header">{{ count($count_by_disease) }}</h5>
              <span class="description-text">Total number of diseases recorded</span>
            </div>
          </div>
        </div>
      </div> -->
      @else
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <h4>No records yet.</h4>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- ./box-body -->
      @endif
</div>

<!-- Chart /dashboard-page-->
<script type="text/javascript">
    <?php
    if($services) { ?>
      // Get context with jQuery - using jQuery's .get() method.
      var servicesChartCanvas = $("#servicesChart").get(0).getContext("2d");

      var servicesChartData = {
        labels: [
            <?php
                foreach($ranges as $range) {
                    $dd = date("M/y", strtotime($range));
                    echo "'".$dd."', ";
                }
            ?>
        ],
        datasets: [
            <?php $colors = array('255,0,0','255,172,0','195,255,0','0,255,255','206,220,0','0,129,198','0,159,298','0,229,100','110,19,198','200,129,198','255,0,0','255,172,0','195,255,0','0,255,255','206,220,0','0,129,198','0,159,298','0,229,100','110,19,198','200,129,198'); ?>
            <?php $bil = 1; ?>
            <?php foreach($cs_stats as $cs=>$range) { ?>
            {
                label: "{{ $cs }}",
                data: [
                  <?php foreach($ranges as $rangee) {
                    if(isset($range[$rangee])) {
                        echo $range[$rangee].", ";
                    } else {
                        echo "0, ";
                    }
                  } ?>
                ],
                backgroundColor: [
                  <?php foreach($ranges as $rangee) { ?>
                      "rgba(<?php echo $colors[$bil-1]; ?>,1)",
                  <?php } ?>
                ],
                borderColor: [
                  <?php foreach($ranges as $rangee) { ?>
                      "rgba(<?php echo $colors[$bil-1]; ?>,1)",
                  <?php } ?>
                ],
                borderWidth: 0
            },
            <?php $bil++; ?>
            <?php } ?>
        ]
      };

      var servicesChartOptions = {};

    $(function () {
      //Create the line chart
      var servicesChart = new Chart(servicesChartCanvas, {
          type: "bar",
          data: servicesChartData,
          options: servicesChartOptions
      });
    });

    <?php } ?>
</script>
