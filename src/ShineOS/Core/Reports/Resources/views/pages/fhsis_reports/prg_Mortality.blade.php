@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<?php if(!isset($population)) { $population = 0; } ?>
<table class=" table-striped table-bordered table-report">
    <thead>
        <tr><th colspan="5">MORTALITY</th></tr>
        <tr><th rowspan="2">Indicators</th><th colspan="3">Number</th><th rowspan="2">Rate</th></tr>
        <tr><th>Male</th><th>Female</th><th>Total</th></tr>
    </thead>

    <tbody>
        <?php $mort_mult = 100000 ?>
        <tr>
            <td>Deaths ♣</td>
            <td>{{ $data['TOTDEATH_M'] }}</td>
            <td>{{ $data['TOTDEATH_F'] }}</td>
            <td>{{ $data['TOTDEATH_M'] + $data['TOTDEATH_F'] }}</td>
            <td>{{ getPercentage( $data['TOTDEATH_M'] + $data['TOTDEATH_F'] , $population, $mort_mult, 0) }} / {{ $mort_mult }}</td>
        </tr>
        <tr>
            <td>Maternal Deaths ☻</td><td bgcolor="#A9A9A9"></td>
            <td>{{ $data['MATDEATH_F'] }}</td>
            <td>{{ $data['MATDEATH_F'] }}</td>
            <td>{{ getPercentage( $data['MATDEATH_F'] , $population, $mort_mult, 0) }} / {{ $mort_mult }}</td>
        </tr>
        <tr>
            <td>Perinatal Deaths ☻</td>
            <td>{{ $data['PRENATDEATH_M'] }}</td>
            <td>{{ $data['PRENATDEATH_F'] }}</td>
            <td>{{ $data['PRENATDEATH_M'] + $data['PRENATDEATH_F'] }}</td>
            <td>{{ getPercentage( $data['PRENATDEATH_M'] + $data['PRENATDEATH_F'] , $population, $mort_mult, 0) }} / {{ $mort_mult }}</td>
        </tr>
        <tr>
            <td>Fetal Deaths ☻</td>
            <td>{{ $data['FD_M'] }}</td>
            <td>{{ $data['FD_F'] }}</td>
            <td>{{ $data['FD_M'] + $data['FD_F'] }}</td>
            <td>{{ getPercentage( $data['FD_M'] + $data['FD_F'], $population, $mort_mult, 0) }} / {{ $mort_mult }}</td>
        </tr>
        <tr>
            <td>Neonatal Deaths ☻</td>
            <td>{{ $data['NEON_M'] }}</td>
            <td>{{ $data['NEON_F'] }}</td>
            <td>{{ $data['NEON_M'] + $data['NEON_F'] }}</td>
            <td>{{ getPercentage( $data['NEON_M'] + $data['NEON_F'], $population, $mort_mult, 0) }} / {{ $mort_mult }}</td>
        </tr>
        <tr>
            <td>Infant Deaths ☻</td>
            <td>{{ $data['INFDEATH_M'] }}</td>
            <td>{{ $data['INFDEATH_F'] }}</td>
            <td>{{ $data['INFDEATH_M'] + $data['INFDEATH_F'] }}</td>
            <td>{{ getPercentage( $data['INFDEATH_M'] + $data['INFDEATH_F'], $population, $mort_mult, 0) }} / {{ $mort_mult }}</td>
        </tr>
        <tr>
            <td>Deaths among children Under 5 yrs old ☻</td>
            <td>{{ $data['DEATHUND5_M'] }}</td>
            <td>{{ $data['DEATHUND5_F'] }}</td>
            <td>{{ $data['DEATHUND5_M'] + $data['DEATHUND5_F'] }}</td>
            <td>{{ getPercentage( $data['DEATHUND5_M'] + $data['DEATHUND5_F'], $population, $mort_mult, 0) }} / {{ $mort_mult }}</td>
        </tr>
        <tr>
            <td>Deaths due to Neonatal Tetanus ☻</td>
            <td>{{ $data['NEOTET_M'] }}</td>
            <td>{{ $data['NEOTET_F'] }}</td>
            <td>{{ $data['NEOTET_M'] + $data['NEOTET_F'] }}</td>
            <td>{{ getPercentage( $data['NEOTET_M'] + $data['NEOTET_F'], $population, $mort_mult, 0) }} / {{ $mort_mult }}</td>
        </tr>
        <tr>
            <td colspan="5"><em class="small"> Denominator: &nbsp; ♣ Population &nbsp; ☻Livebirths </em></td></tr>
    </tbody>
</table>
@yield('modal-footer')