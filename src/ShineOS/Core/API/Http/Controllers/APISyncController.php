<?php namespace ShineOS\Core\API\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\Sync\Entities\Sync;
use Illuminate\Http\JsonResponse;
use ShineOS\Core\Users\Entities\Users;
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Sync\Http\Controllers\SyncController;
use Illuminate\Support\Collection;
use Request, DB, Config;

class APISyncController extends Controller {
    public function __construct() {
        $this->curr_db = Config::get('database.connections.'.Config::get('database.default').'.database');
    }

    public function index() {
        return new JsonResponse(array('message'=>'No data'), 400);
    }

    public function getSyncDateTime_get() {
        $data = Request::all();
        if(array_key_exists('facility_id',$data)) {
            $updated_at = Sync::getSyncDateTime($data['facility_id'], $data['toFrom']);
            return new JsonResponse(($updated_at), 200);
        } else {
            return new JsonResponse(array('error'=>'Facility Id not found'), 400);
        }
    }


    public function manageRecords_post() {
        $data = Request::all();
        if(array_key_exists('Records',$data)) {
            $sync = SyncController::manageRecords($data['Records'], $data['Facid']);

            if($sync) {
                return new JsonResponse(array('status'=>'success', 'data'=>$sync), 200);
            } else {
                return new JsonResponse(array('status'=>'Nothing to sync'), 200);
            }
        } else {
            return new JsonResponse(array('error'=>'Records not found'), 400);
        }

        return new JsonResponse(array('status'=>'error'), 400);
    }

    public function cloudRecords_post() {
        $data = Request::all();
        if(array_key_exists('Records',$data)) {
            $sync = SyncController::cloudRecords($data['Records'], $data['Facid']);
            if($sync) {
                return new JsonResponse(array('status'=>'success', 'data'=>$sync), 200);
            } else {
                return new JsonResponse(array('status'=>'Nothing to sync'), 200);
            }
        } else {
            return new JsonResponse(array('error'=>'Records not found'), 400);
        }

        return new JsonResponse(array('status'=>'error'), 400);
    }

    public function getAllTables_get() {
        $data = Request::all();
        $all_tables = DB::select("SHOW TABLES FROM ".$this->curr_db."");
        return new JsonResponse(($all_tables), 200);
    }

    //create a function that checks if facility id exists

    // public function checkFacilityID_get() {
    // 	$data = Request::all();
    // 	$count = Facilities::where('facility_id',$data['facility_id'])->count();
    // 	return $count;
    // }

    //compare tables from CE to SAAS


    // public function compareTables_CE_SAAS_get() {
    // 	$data = Request::all();
    // 	$result = SyncController::compareTables($data);
    // 	return $result;
    // }

    public function insertLastSync_post() {
        $data = Request::all();
        $result = SyncController::insert_last_sync($data['facility_id'], $data['toFrom']);
        return $result;
    }
}
