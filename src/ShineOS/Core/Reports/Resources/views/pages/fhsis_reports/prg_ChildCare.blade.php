@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<table class="table table-striped table-bordered table-report table-responsive">
    <thead>
    <tr>
        <th>CHILD CARE - Part 1</th>
        <th width="15%">Male</th>
        <th width="15%">Female</th>
        <th width="15%">Total</th>
    </tr>
    </thead>
        <tbody>
        <tr>
            <td>BCG</td>
            <td>{{ $data['IMM_BCG_M'] }}</td>
            <td>{{ $data['IMM_BCG_F'] }}</td>
            <td>{{ $data['IMM_BCG_M'] + $data['IMM_BCG_F'] }}</td>
        </tr>
        <tr>
            <td>Hepa B1 (w/in 24 hrs)</td>
            <td>{{ $data['IMM_HEPAB1WIN24_M'] }}</td>
            <td>{{ $data['IMM_HEPAB1WIN24_F'] }}</td>
            <td>{{ $data['IMM_HEPAB1WIN24_M'] + $data['IMM_HEPAB1WIN24_F']  }}</td>
        </tr>
        <tr>
            <td>Hepa B1 (&gt;24 hrs)</td>
            <td>{{ $data['IMM_HEPAB124_M'] }}</td>
            <td>{{ $data['IMM_HEPAB124_F'] }}</td>
            <td>{{ $data['IMM_HEPAB124_M'] + $data['IMM_HEPAB124_F']  }}</td>
        </tr>
        <tr>
            <td>Hepa B2</td>
            <td>{{ $data['IMM_HEPAB2_M'] }}</td>
            <td>{{ $data['IMM_HEPAB2_F'] }}</td>
            <td>{{ $data['IMM_HEPAB2_M'] + $data['IMM_HEPAB2_F']  }}</td>
        </tr>
        <tr>
            <td>Hepa B3</td>
            <td>{{ $data['IMM_HEPAB3_M'] }}</td>
            <td>{{ $data['IMM_HEPAB3_F'] }}</td>
            <td>{{ $data['IMM_HEPAB3_M'] + $data['IMM_HEPAB3_F'] }}</td>
        </tr>
        <tr>
            <td>Penta 1</td>
            <td>{{ $data['IMM_PENTA1_M'] }}</td>
            <td>{{ $data['IMM_PENTA1_F'] }}</td>
            <td>{{ $data['IMM_PENTA1_M'] + $data['IMM_PENTA1_F'] }}</td>
        </tr>
        <tr>
            <td>Penta 2</td>
            <td>{{ $data['IMM_PENTA2_M'] }}</td>
            <td>{{ $data['IMM_PENTA2_F'] }}</td>
            <td>{{ $data['IMM_PENTA2_M'] + $data['IMM_PENTA2_F'] }}</td>
        </tr>
        <tr>
            <td>Penta 3</td>
            <td>{{ $data['IMM_PENTA3_M'] }}</td>
            <td>{{ $data['IMM_PENTA3_F'] }}</td>
            <td>{{ $data['IMM_PENTA3_M'] + $data['IMM_PENTA3_F'] }}</td>
        </tr>
        <tr>
            <td>OPV 1</td>
            <td>{{ $data['IMM_OPV1_M'] }}</td>
            <td>{{ $data['IMM_OPV1_F'] }}</td>
            <td>{{ $data['IMM_OPV1_M'] + $data['IMM_OPV1_F'] }}</td>
        </tr>
        <tr>
            <td>OPV 2</td>
            <td>{{ $data['IMM_OPV2_M'] }}</td>
            <td>{{ $data['IMM_OPV2_F'] }}</td>
            <td>{{ $data['IMM_OPV2_M'] + $data['IMM_OPV2_F'] }}</td>
        </tr>
        <tr>
            <td>OPV 3</td>
            <td>{{ $data['IMM_OPV3_M'] }}</td>
            <td>{{ $data['IMM_OPV3_F'] }}</td>
            <td>{{ $data['IMM_OPV3_M'] + $data['IMM_OPV3_F'] }}</td>
        </tr>
        <tr>
            <td>MCV1 (AMV)</td>
            <td>{{ $data['IMM_MCV1_M'] }}</td>
            <td>{{ $data['IMM_MCV1_F'] }}</td>
            <td>{{ $data['IMM_MCV1_M'] + $data['IMM_MCV1_F'] }}</td>
        </tr>
        <tr>
            <td>MCV2 (MMR)</td>
            <td>{{ $data['IMM_MCV2_M'] }}</td>
            <td>{{ $data['IMM_MCV2_F'] }}</td>
            <td>{{ $data['IMM_MCV2_M'] + $data['IMM_MCV2_F'] }}</td>
        </tr>
        <tr>
            <td>Rota 1</td>
            <td>{{ $data['IMM_ROTA1_M'] }}</td>
            <td>{{ $data['IMM_ROTA1_F'] }}</td>
            <td>{{ $data['IMM_ROTA1_M'] + $data['IMM_ROTA1_F'] }}</td>
        </tr>
        <tr>
            <td>Rota 2</td>
            <td>{{ $data['IMM_ROTA2_M'] }}</td>
            <td>{{ $data['IMM_ROTA2_F'] }}</td>
            <td>{{ $data['IMM_ROTA2_M'] + $data['IMM_ROTA2_F'] }}</td>
        </tr>
        <tr>
            <td>Rota 3</td>
            <td>{{ $data['IMM_ROTA3_M'] }}</td>
            <td>{{ $data['IMM_ROTA3_F'] }}</td>
            <td>{{ $data['IMM_ROTA3_M'] + $data['IMM_ROTA3_F'] }}</td>
        </tr>
        <tr>
            <td>PCV 1</td>
            <td>{{ $data['IMM_PCV1_M'] }}</td>
            <td>{{ $data['IMM_PCV1_F'] }}</td>
            <td>{{ $data['IMM_PCV1_M'] + $data['IMM_PCV1_F'] }}</td>
        </tr>
        <tr>
            <td>PCV 2</td>
            <td>{{ $data['IMM_PCV2_M'] }}</td>
            <td>{{ $data['IMM_PCV2_F'] }}</td>
            <td>{{ $data['IMM_PCV2_M'] + $data['IMM_PCV2_F'] }}</td>
        </tr>
        <tr>
            <td>PCV 3</td>
            <td>{{ $data['IMM_PCV3_M'] }}</td>
            <td>{{ $data['IMM_PCV3_F'] }}</td>
            <td>{{ $data['IMM_PCV3_M'] + $data['IMM_PCV3_F'] }}</td>
        </tr>
        <tr>
            <td>Fully Immunized Child (0-11 mos)</td>
            <td>{{ $data['FIC_M'] }}</td>
            <td>{{ $data['FIC_F'] }}</td>
            <td>{{ $data['FIC_M'] + $data['FIC_F'] }}</td>
        </tr>
        <tr>
            <td>Completely Immunized Child (12-23 mos)</td>
            <td>{{ $data['CIC_M'] }}</td>
            <td>{{ $data['CIC_F'] }}</td>
            <td>{{ $data['CIC_M'] + $data['CIC_F'] }}</td>
        </tr>
        <tr>
            <td>Total Live births</td>
            <td>{{ $data['LBTOT_M'] }}</td>
            <td>{{ $data['LBTOT_F'] }}</td>
            <td>{{ $data['LBTOT_M'] + $data['LBTOT_F'] }}</td>
        </tr>
        <tr>
            <td>Child Protected at Birth</td>
            <td>{{ $data['CPB_M'] }}</td>
            <td>{{ $data['CPB_F'] }}</td>
            <td>{{ $data['CPB_M'] + $data['CPB_F'] }}</td>
        </tr>
        <tr>
            <td>Infant Age 6 months seen</td>
            <td>{{ $data['INF_AGE_M'] }}</td>
            <td>{{ $data['INF_AGE_F'] }}</td>
            <td>{{ $data['INF_AGE_M'] + $data['INF_AGE_F'] }}</td>
        </tr>
        <tr>
            <td>Infant exclusively breastfed until 6 months</td>
            <td>{{ $data['INF_BREAST_M'] }}</td>
            <td>{{ $data['INF_BREAST_F'] }}</td>
            <td>{{ $data['INF_BREAST_M'] + $data['INF_BREAST_F'] }}</td>
        </tr>
        <tr>
            <td>Infant given complimentary food from 6-8 months</td>
            <td>{{ $data['INF_FOOD_M'] }}</td>
            <td>{{ $data['INF_FOOD_F'] }}</td>
            <td>{{ $data['INF_FOOD_M'] + $data['INF_FOOD_F'] }}</td>
        </tr>
        <tr>
            <td>Infant for newborn screening : referred</td>
            <td>{{ $data['INF_NEWBS_M'] }}</td>
            <td>{{ $data['INF_NEWBS_F'] }}</td>
            <td>{{ $data['INF_NEWBS_M'] + $data['INF_NEWBS_F'] }}</td>
        </tr>
        <tr>
            <td>Infant for newborn screening : done</td>
            <td>{{ $data['INF_NEWBS_M'] }}</td>
            <td>{{ $data['INF_NEWBS_F'] }}</td>
            <td>{{ $data['INF_NEWBS_M'] + $data['INF_NEWBS_F'] }}</td>
        </tr>
        <tr>
            <td>Infant 6-11 months old received Vitamin A</td>
            <td>{{ $data['INF_VITA611_M'] }}</td>
            <td>{{ $data['INF_VITA611_F'] }}</td>
            <td>{{ $data['INF_VITA611_M'] + $data['INF_VITA611_F'] }}</td>
        </tr>
        <tr>
            <td>Children 12-59 months old received Vitamin A</td>
            <td>{{ $data['INF_VITA1259_M'] }}</td>
            <td>{{ $data['INF_VITA1259_F'] }}</td>
            <td>{{ $data['INF_VITA1259_M'] + $data['INF_VITA1259_F'] }}</td>
        </tr>
        <tr>
            <td>Infant 6-11 months old received Iron</td>
            <td>{{ $data['INF611FE_M'] }}</td>
            <td>{{ $data['INF611FE_F'] }}</td>
            <td>{{ $data['INF611FE_M'] + $data['INF611FE_F'] }}</td>
        </tr>
        <tr>
            <td>Infant 6-11 months received MNP</td>
            <td>{{ $data['INF611MNP_M'] }}</td>
            <td>{{ $data['INF611MNP_F'] }}</td>
            <td>{{ $data['INF611MNP_M'] + $data['INF611MNP_F'] }}</td>
        </tr>
        <tr>
            <td>Children 12-23 months received MNP</td>
            <td>{{ $data['INF1223MNP_M'] }}</td>
            <td>{{ $data['INF1223MNP_F'] }}</td>
            <td>{{ $data['INF1223MNP_M'] + $data['INF1223MNP_F'] }}</td>
        </tr>
        <tr>
            <td>Sick Children 6-11 months seen</td>
            <td>{{ $data['SICK_611_M'] }}</td>
            <td>{{ $data['SICK_611_F'] }}</td>
            <td>{{ $data['SICK_611_M'] + $data['SICK_611_F'] }}</td>
        </tr>
        <tr>
            <td>Sick Children 6-11 months received Vitamin A</td>
            <td>{{ $data['SICKVITA_611_M'] }}</td>
            <td>{{ $data['SICKVITA_611_F'] }}</td>
            <td>{{ $data['SICKVITA_611_M'] + $data['SICKVITA_611_F'] }}</td>
        </tr>
        <tr>
            <td>Sick Children 12-59 months seen</td>
            <td>{{ $data['SICK_1259_M'] }}</td>
            <td>{{ $data['SICK_1259_F'] }}</td>
            <td>{{ $data['SICK_1259_M'] + $data['SICK_1259_F'] }}</td>
        </tr>
        <tr>
            <td>Sick Children 12-59 months received Vitamin A</td>
            <td>{{ $data['SICKVITA_1259_M'] }}</td>
            <td>{{ $data['SICKVITA_1259_F'] }}</td>
            <td>{{ $data['SICKVITA_1259_M'] + $data['SICKVITA_1259_F'] }}</td>
        </tr>
        <tr>
            <td>Children 12-59 mos. old given de-worming tablet/syrup</td>
            <td>{{ $data['CHILD_1259DW_M'] }}</td>
            <td>{{ $data['CHILD_1259DW_F'] }}</td>
            <td>{{ $data['CHILD_1259DW_M'] + $data['CHILD_1259DW_F'] }}</td>
        </tr>
        <tr>
            <td>Infant 2-5 mos w/ Low Birth Weight seen</td>
            <td>{{ $data['INF26LBWNS_M'] }}</td>
            <td>{{ $data['INF26LBWNS_F'] }}</td>
            <td>{{ $data['INF26LBWNS_M'] + $data['INF26LBWNS_F'] }}</td>
        </tr>
        <tr>
            <td>Infant 2-5 mos w/ LBW received full dose iron</td>
            <td>{{ $data['INF26FE_M'] }}</td>
            <td>{{ $data['INF26FE_F'] }}</td>
            <td>{{ $data['INF26FE_M'] + $data['INF26FE_F'] }}</td>
        </tr>
        <tr>
            <td>Anemic Children 6-11 months old seen</td>
            <td>{{ $data['ANECHILD611_M'] }}</td>
            <td>{{ $data['ANECHILD611_F'] }}</td>
            <td>{{ $data['ANECHILD611_M'] + $data['ANECHILD611_F'] }}</td>
        </tr>
        <tr>
            <td>Anemic Children 6-11 mos received full dose iron</td>
            <td>{{ $data['ANECHILD611FE_M']  }}</td>
            <td>{{ $data['ANECHILD611FE_F']  }}</td>
            <td>{{ $data['ANECHILD611FE_M'] + $data['ANECHILD611FE_F'] }}</td>
        </tr>
        <tr>
            <td>Anemic Children 12-59 months old seen</td>
            <td>{{ $data['ANECHILD1259_M'] }}</td>
            <td>{{ $data['ANECHILD1259_F'] }}</td>
            <td>{{ $data['ANECHILD1259_M'] + $data['ANECHILD1259_F'] }}</td>
        </tr>
        <tr>
            <td>Anemic Children 12-59 mos received full dose iron</td>
            <td>{{ $data['ANECHILD1259FE_M'] }}</td>
            <td>{{ $data['ANECHILD1259FE_F'] }}</td>
            <td>{{ $data['ANECHILD1259FE_M'] + $data['ANECHILD1259FE_F'] }}</td>
        </tr>
        <tr>
            <td>Diarrhea cases 0-59 months old seen</td>
            <td>{{ $data['DIARRNC_M'] }}</td>
            <td>{{ $data['DIARRNC_F'] }}</td>
            <td>{{ $data['DIARRNC_M'] + $data['DIARRNC_F'] }}</td>
        </tr>
        <tr>
            <td>Diarrhea cases 0-59 mos old received ORS</td>
            <td>{{ $data['DIARRORS_M'] }}</td>
            <td>{{ $data['DIARRORS_F'] }}</td>
            <td>{{ $data['DIARRORS_M'] + $data['DIARRORS_F'] }}</td>
        </tr>
        <tr>
            <td>Diarrhea 0-59 mos received ORS/ORT w/ zinc</td>
            <td>{{ $data['DIARRORSZ_M'] }}</td>
            <td>{{ $data['DIARRORSZ_F'] }}</td>
            <td>{{ $data['DIARRORSZ_M'] + $data['DIARRORSZ_F'] }}</td>
        </tr>
        <tr>
            <td>Pneumonia cases 0-59 months old</td>
            <td>{{ $data['PNEUNC_M'] }}</td>
            <td>{{ $data['PNEUNC_F'] }}</td>
            <td>{{ $data['PNEUNC_M'] + $data['PNEUNC_F'] }}</td>
        </tr>
        <tr>
            <td>Pneumonia cases 0-59 mos. old completed Tx</td>
            <td>{{ $data['PNEUGT_M'] }}</td>
            <td>{{ $data['PNEUGT_F'] }}</td>
            <td>{{ $data['PNEUGT_M'] + $data['PNEUGT_F'] }}</td>
        </tr>
    </tbody>
</table><!-- /.table child care -->

 @yield('modal-footer')   