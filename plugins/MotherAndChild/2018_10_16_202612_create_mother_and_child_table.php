<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotherAndChildTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('motherandchild_service')!=TRUE) { 
            Schema::create('motherandchild_service', function (Blueprint $table) {
                $table->increments('id');
                $table->string('motherandchild_id', 32);
                $table->string('healthcareservice_id', 32);

                $table->string('uuid', 100)->nullable();
                $table->string('owner_uuid', 100)->nullable();
                
                $table->integer('age_menarche')->nullable();
                $table->integer('stay_menarche')->nullable();
                $table->string('stay_menarche_oth',100)->nullable();

                $table->integer('material_menstruation')->nullable();

                $table->integer('sanitary_materials_used_pads')->nullable();
                $table->integer('sanitary_materials_used_tampon')->nullable();
                $table->integer('sanitary_materials_used_clothes')->nullable();
                $table->integer('sanitary_materials_used_hp')->nullable();
                $table->integer('sanitary_materials_used_mhp')->nullable();
                $table->integer('sanitary_materials_used_none')->nullable();

                $table->integer('material_change_frequency')->nullable();

                $table->string('material_change_frequency_oth',100)->nullable();
                $table->string('timeliness_reason',100)->nullable();

                $table->integer('sanitarydisposal')->nullable();
                $table->string('sanitarydisposal_oth',100)->nullable();

                $table->integer('is_pregnant')->nullable();
                $table->integer('age_first_pregnancy')->nullable();
                // $table->integer('past_gravida')->nullable();
                // $table->integer('past_para')->nullable();
                // $table->integer('past_abortion')->nullable();
                // $table->integer('past_living')->nullable();
                $table->integer('cur_gravida')->nullable();
                $table->integer('cur_para')->nullable();
                $table->integer('cur_abortion')->nullable();
                $table->integer('cur_living')->nullable();
                $table->integer('period_gestation')->nullable();
                $table->integer('times_antenatal')->nullable();

                $table->integer('antenatal_services_anc_check_up')->nullable();
                $table->integer('antenatal_service_albendazole')->nullable();
                $table->integer('antenatal_service_td_immunization')->nullable();
                $table->integer('antenatal_service_irontabs')->nullable();
                $table->integer('antenatal_service_pmtct')->nullable();
                $table->integer('antenatal_service_others')->nullable();
                $table->string('antenatal_service_specify',100)->nullable();

                $table->string('healthseeking_behavior',100)->nullable();
                $table->string('healthseeking_behavior_oth_spec',100)->nullable();

                $table->text('medicines')->nullable();

                $table->integer('age_young_months')->nullable();
                $table->integer('type_of_delivery')->nullable();
                $table->integer('age_gap_kids')->nullable();
                $table->integer('delivery_place')->nullable();
                $table->string('delivery_place_oth_spec',100)->nullable();
                $table->integer('delivery_complication')->nullable();
                $table->string('delivery_complication_spec',100)->nullable();

                $table->integer('post_natal_pe')->nullable();
                $table->integer('post_natal_counsel_b_f')->nullable();
                $table->integer('post_natal_counsel_f_p')->nullable();
                $table->integer('post_natal_investigations')->nullable();
                $table->integer('post_natal_iron_tablets')->nullable();
                $table->integer('post_natal_vitamin_a')->nullable();
                $table->integer('post_natal_oth')->nullable();
                $table->string('post_natal_oth_spec',100)->nullable();

                $table->integer('pnc_followup_none')->nullable();
                $table->integer('pnc_followup_within24hr')->nullable();
                $table->integer('pnc_followup_at3rd_day')->nullable();
                $table->integer('pnc_followup_at7th_day')->nullable();
                $table->integer('pnc_followup_at28th_day')->nullable();
                $table->integer('pnc_followup_at45th_day')->nullable();
                $table->integer('pnc_followup_others')->nullable();
                $table->string('pnc_followup_specify',100)->nullable();

                $table->text('food_after_delivery')->nullable();
                $table->text('food_after_delivery_quality')->nullable();
                $table->text('food_after_delivery_frequency')->nullable();
                $table->text('food_after_delivery_distribution')->nullable();

                $table->integer('nutritious_food')->nullable();
                $table->integer('pre_lactating')->nullable();
                $table->integer('nutritious_food_spec')->nullable();

                $table->integer('dietary_restriction')->nullable();
                $table->string('dietary_restriction_spec',100)->nullable();

                $table->integer('fp_counseling')->nullable();
                $table->integer('fp_use')->nullable();

                $table->integer('permanent_vasectomy')->nullable();
                $table->integer('permanent_minilap')->nullable();
                
                $table->integer('temporary_natural')->nullable();
                $table->integer('temporary_condom')->nullable();
                $table->integer('temporary_depo')->nullable();
                $table->integer('temporary_pills')->nullable();
                $table->integer('temporary_iucd')->nullable();
                $table->integer('temporary_implant')->nullable();

                $table->integer('fp_info_src')->nullable();

                $table->string('child_id',100)->nullable();
                $table->string('child_name',100)->nullable();
                $table->integer('child_sex')->nullable();
                $table->string('child_birthdate_ad',100)->nullable();
                $table->string('child_birthdate_bs',100)->nullable();

                $table->double('child_weight_birth')->nullable();
                $table->double('child_weight_current')->nullable();
                $table->double('child_height')->nullable();
                $table->double('mid_arm_circumference')->nullable();

                $table->integer('congenital_anomaly')->nullable();
                $table->string('congenital_anomaly_spec',100)->nullable();
                $table->integer('health_condition')->nullable();
                $table->integer('vitamin_k')->nullable();

                $table->integer('breastfeed_start')->nullable();
                $table->integer('bf_times')->nullable();

                $table->integer('exclusive_breastfeeding')->nullable();
                $table->string('exclusive_breastfeeding_spec',100)->nullable();

                $table->string('age_stop_bf_months')->nullable();
                $table->string('reason_stopbf')->nullable();

                $table->string('reason_stopbf_spec',100)->nullable();

                $table->string('aware_bf',100)->nullable();
                $table->string('solid_feeding',100)->nullable();

                $table->integer('food_supplement_rice_pudding')->nullable();
                $table->integer('food_supplement_jaulo')->nullable();
                $table->integer('food_supplement_lito')->nullable();
                $table->integer('food_supplement_cerelac')->nullable();
                $table->integer('food_supplement_oth')->nullable();

                $table->string('food_supplement_oth_spec',100)->nullable();

                $table->integer('no_food')->nullable();
                $table->string('no_food_oth',100)->nullable();

                $table->integer('clean_hands_breastfeed')->nullable();
                $table->integer('handwashing_facility_breastfeed')->nullable();

                $table->text('child_food')->nullable();
                $table->text('child_food_quality')->nullable();
                $table->text('child_food_frequency')->nullable();
                $table->text('child_food_distribution')->nullable();

                $table->integer('injury_type_falls')->nullable();
                $table->integer('injury_type_drowning')->nullable();
                $table->integer('injury_type_burnsscald')->nullable();
                $table->integer('injury_type_poisoning')->nullable();
                $table->integer('injury_type_suffocatingchokingaspiration')->nullable();
                $table->integer('injury_type_cut')->nullable();
                $table->integer('injury_type_oth')->nullable();

                $table->string('injury_types_oth_spec',100)->nullable();

                $table->integer('injury_cause_unsafe_home')->nullable();
                $table->integer('injury_cause_no_supervision')->nullable();
                $table->integer('injury_cause_busy_mother')->nullable();
                $table->integer('injury_cause_slippery_floor')->nullable();
                $table->integer('injury_cause_oth')->nullable();
                $table->string('injury_cause_oth_spec',100)->nullable();

                $table->integer('age_bcg_at_birth')->nullable();
                $table->integer('age_dpt_hepb_hib6_weeks')->nullable();
                $table->integer('age_dpt_hepb_hib10_weeks')->nullable();
                $table->integer('age_dpt_hepb_hib14_weeks')->nullable();
                $table->integer('age_opv6_weeks')->nullable();
                $table->integer('age_opv10_weeks')->nullable();
                $table->integer('age_opv14_weeks')->nullable();
                $table->integer('age_pcv6_weeks')->nullable();
                $table->integer('age_pcv10_weeks')->nullable();
                $table->integer('age_pcv9_months')->nullable();
                $table->integer('age_ipv14_weeks')->nullable();
                $table->integer('age_measles_rubella9_months')->nullable();
                $table->integer('age_measles_rubella15_months')->nullable();
                $table->integer('age_japanese_encephalitis12_months')->nullable();

                $table->text('immu_remarks_bcg')->nullable();
                $table->text('immu_remarks_dpt_hepb_hib')->nullable();
                $table->text('immu_remarks_opv')->nullable();
                $table->text('immu_remarks_pcv')->nullable();
                $table->text('immu_remarks_ipv')->nullable();
                $table->text('immu_remarks_measles_rubella')->nullable();
                $table->text('immu_remarks_japanese_encephalitis')->nullable();

                $table->text('advise_bf')->nullable();
                $table->text('advise_dh')->nullable();
                $table->text('advise_tt')->nullable();
                $table->text('advise_cf')->nullable();
                $table->text('advise_ap')->nullable();
                $table->text('advise_physical')->nullable();
                $table->text('advise_verbal')->nullable();
                $table->text('advise_social')->nullable();
                $table->text('advise_spiritual')->nullable();
                $table->text('advise_motor')->nullable();
                $table->text('advise_intellectual')->nullable();
                $table->text('advise_emotional')->nullable();
                                
                $table->softDeletes();
                $table->timestamps();
                $table->unique('healthcareservice_id');
            });
        }          //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
