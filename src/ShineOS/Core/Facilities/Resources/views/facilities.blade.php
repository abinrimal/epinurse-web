<?php
// dd($currentFacility);
if(isset($currentFacility->facility_workforce)) {
  $facility_workforce = json_decode($currentFacility->facility_workforce->workforce);
}
?>

@extends('facilities::layouts.masterfacility')

@section('profile-content')
    @include('facilities::fac_nav')
        <div class="col-md-9 padleft0">
           <div class="row">
                <div class="col-md-12">
                    @if (Session::has('message'))
                        <div class="alert alert-dismissible alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif
                    @if (Session::has('warning'))
                        <div class="alert alert-dismissible alert-warning">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <p>{{ Session::get('warning') }}</p>
                        </div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-dismissible alert-error">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <p>{{ Session::get('error') }}</p>
                        </div>
                    @endif

                </div>
            </div> 
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Facility Information</a></li>
              <li><a href="#tab_2" data-toggle="tab">Facility Contact</a></li>
              <li><a href="#tab_3" data-toggle="tab">Specializations</a></li>
              <li><a href="#tab_4" data-toggle="tab">Workforce</a></li>
              @if($currentFacility->facility_type == 'Rural Health Unit' OR $currentFacility->facility_type == 'RURAL_HEALTH_UNIT' OR $currentFacility->facility_type == 'City Health Office' OR $currentFacility->facility_type == 'Main Health Center' OR $currentFacility->facility_type == 'Municipal Health Office')
                <li><a href="#tab_5" data-toggle="tab">Catchment Area</a></li>
              @endif
              @if($plugs)
                  @foreach($plugs  as $key => $plug)
                    @if($plug['plugin_location'] == 'tab')
                    <li><a href="#{{ $plug['plugin'] }}" data-toggle="tab">{{ $plug['title'] }}</a></li>
                    @endif
                  @endforeach
              @endif
            </ul>
            <div class="tab-content">

                <!-- Facility Info -->
              <div class="tab-pane active icheck" id="tab_1">
                <h4>Facility Information</h4>
                <!-- form start -->
               {!! Form::open(array( 'url'=>$modulePath.'/updatefacilityinfo/'.$currentFacility->facility_id, 'id'=>'facilityForm', 'name'=>'facilityForm', 'class'=>'form-horizontal' )) !!}

                  <div class="box-body">
                    <div class="form-group">
                      <label for="facility_name" class="col-sm-3 control-label">Facility Name</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="facility_name" name="facility_name" placeholder="Facility Name" value="{{ $currentFacility->facility_name }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="DOH_facility_code" class="col-sm-3 control-label">DOH Facility Code</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" readonly placeholder="DOH Facility Code" value="{{ $doh }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="phic_accr_id" class="col-sm-3 control-label">PHIC Accr No.</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="phic_accr_id" name="phic_accr_id" placeholder="PHIC Accr No" value="{{ $currentFacility->phic_accr_id }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="ownership_type" class="col-sm-3 control-label">Ownership Type</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" placeholder="Ownership Type" name="ownership_type" value="{{ ucfirst($currentFacility->ownership_type) }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="provider_type" class="col-sm-3 control-label">Provider Type</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" readonly placeholder="Provider Type" name="provider_type" value="{{ ucfirst($currentFacility->provider_type) }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="facility_type" class="col-sm-3 control-label">Facility Type</label>
                      <div class="col-sm-9">
                        <?php $faciltyType = array_merge(array("NULL"=>"Select Facility Type"), $faciltyType->toarray()); ?>

                        {!! Form::select('facility_type', $faciltyType, $currentFacility->facility_type, ['class' => 'form-control', 'id'=>'facility_type']) !!}
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="registration_date" class="col-sm-3 control-label">Registration Date</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="registration_date" name="registration_date" placeholder="Registration Date" readonly value="{{ date('m-d-Y',strtotime($currentFacility->created_at)) }}" />
                      </div>
                      <label for="hospital_license_number" class="col-sm-3 control-label">Hospital License Number</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="hospital_license_number" name="hospital_license_number" placeholder="Hospital License Number" value="{{ $currentFacility->hospital_license_number }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="bmonc_cmonc" class="col-sm-3 control-label">BMONC/CMONC</label>
                      <div class="col-sm-9">
                        <label class="radio-inline"><input type="radio" name="bmonc_cmonc" value="bmonc" @if ($currentFacility->bmonc_cmonc == 'bmonc') checked="checked" @endif />BMONC</label>
                        <label class="radio-inline"><input type="radio" name="bmonc_cmonc" value="cmonc" @if ($currentFacility->bmonc_cmonc == 'cmonc') checked="checked" @endif />CMONC</label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="flag_allow_referral" class="col-sm-3 control-label">Can receive referrals?</label>
                      <div class="col-sm-9">
                        <label class="radio-inline"><input type="radio" name="flag_allow_referral" value="1" @if ($currentFacility->flag_allow_referral == '1') checked="checked" @endif />Yes</label>
                        <label class="radio-inline"><input type="radio" name="flag_allow_referral" value="0" @if ($currentFacility->flag_allow_referral == '0') checked="checked" @endif />No</label>
                      </div>
                    </div>
                    @if($currentFacility->wskey != NULL AND $currentFacility->ekey != NULL)
                    <div class="form-group" disable>
                      <label class="col-sm-3 control-label">PHIE Registered</label>
                      <div class="col-sm-9">
                        <label class="checkbox-inline"><div class="state icheckbox_square-green checked"></div> Yes</label>
                      </div>
                    </div>
                    @endif
                  </div><!-- /.box-body !-->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-success pull-right">Update Info</button>
                  </div><!-- /.box-footer -->
               {!! Form::close() !!}
              </div><!-- /.tab-pane -->

              <div class="tab-pane" id="tab_2">
                <h4>Contact Details</h4>
                <!-- form start -->
                {!! Form::open(array( 'url'=>$modulePath.'/updatefacilitycontact/'.$currentFacility->facility_id, 'id'=>'facilityContactForm', 'name'=>'facilityContactForm', 'class'=>'form-horizontal' )) !!}
                  <div class="box-body">
                    <div class="form-group">
                      <label for="email_address" class="col-sm-2 control-label">Email Address</label>
                      <div class="col-sm-4">
                        <input type="email" class="form-control" id="email_address" name="email_address" placeholder="Email Address" value="{{ $facilityContact->email_address }}" />
                      </div>
                      <label for="email_address" class="col-sm-2 control-label">Mobile No.</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" value="{{ $facilityContact->mobile }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="website" class="col-sm-2 control-label">Website</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="website" name="website" placeholder="Website" value="{{ $facilityContact->website }}" />
                      </div>
                    </div>

                    <div class="form-group">
                      <h4 class="col-sm-12">Address</h4>
                      <hr />
                      <label for="house_no" class="col-sm-2 control-label">House No.</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="house_no" name="house_no" placeholder="House No." value="{{ $facilityContact->house_no }}" />
                      </div>
                      <label for="house_no" class="col-sm-2 control-label">Building</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="building_name" name="building_name" placeholder="Building Name" value="{{ $facilityContact->building_name }}" />
                      </div>

                      <label for="street_name" class="col-sm-2 control-label">Street</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="street_name" name="street_name" placeholder="Street Name" value="{{ $facilityContact->street_name }}" />
                      </div>
                      <label for="street_name" class="col-sm-2 control-label">Village</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="village" name="village" placeholder="Village" value="{{ $facilityContact->village }}" />
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="ward_no" class="col-sm-2 control-label">Ward No.</label>
                      <div class="col-sm-4">
                        {!! Form::text('ward_no', $facilityContact->ward_no, array('class' => 'form-control alpha required','placeholder'=>'Ward No.')) !!}
                      </div>
                      <label for="city" class="col-sm-2 control-label">Municipality/City</label>
                      <div class="col-sm-4">
                        {!! Form::text('city', $facilityContact->city, array('class' => 'form-control alpha required','placeholder'=>'City/Municipality')) !!}
                      </div>
                      <label for="brgy" class="col-sm-2 control-label">District</label>
                      <div class="col-sm-4">
                        {!! Form::text('district', $facilityContact->district, array('class' => 'form-control alpha required','placeholder'=>'District')) !!}
                      </div>
                      <label for="province" class="col-sm-2 control-label">Province</label>
                      <div class="col-sm-4">
                          {!! Form::text('province', $facilityContact->province, array('class' => 'form-control alpha required','placeholder'=>'Province')) !!}
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="zip" class="col-sm-2 control-label">Zip Code</label>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="zip" name="zip" placeholder="ZIP" value="{{ $facilityContact->zip }}" />
                      </div>
                      <label for="country" class="col-sm-2 control-label">Country</label>
                      <div class="col-sm-4">
                        {!! Form::select('country', $country, $facilityContact->country, ['class' => 'form-control', 'id'=>'country']) !!}
                      </div>
                    </div>
                  </div>

                <div class="box-footer">
                    <input type="submit" name="updateContact" id="updateContact" class="btn btn-success pull-right" value="Update Contacts" />
                </div><!-- /.box-footer -->
                {!! Form::close() !!}
              </div><!-- /.tab-pane -->

              <div class="tab-pane" id="tab_3">
                <h4>Specializations</h4>
                <p>Choose as many specializations your facility can provide. Click on the field and choose.</p>
                <!-- form start -->
                <!-- NOTE:: Fix this! -->
                {!! Form::open(array( 'url'=>$modulePath.'/updatespecialization/'.$currentFacility->facility_id, 'id'=>'facilitySpecializationForm', 'name'=>'facilitySpecializationForm', 'class'=>'form-horizontal' )) !!}
                  <div class="box-body">
                    <div class="form-group">
                      <label for="specializations" class="col-sm-3 control-label">Specializations</label>
                      <div class="col-sm-9">
                        <select id="specializations" name="specializations[]" class="form-control select2" multiple="multiple" style="width: 100%;">
                            <option value=""></option>
                            <?php foreach($specialties as $specialty) { ?>
                                <option value="<?php echo $specialty->code; ?>"
                                    <?php
                                        foreach (explode(',', $currentFacility->specializations) as $key => $value)
                                            {
                                                // echo $value;
                                                if($value === $specialty->code) { echo "selected='selected'"; }
                                            }
                                         ?>>

                                        <?php echo $specialty->description; ?>
                                </option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="services" class="col-sm-3 control-label">Services</label>
                      <div class="col-sm-9">
                          <select id="services" class="form-control select2" name="services[]" multiple="multiple" style="width: 100%">
                                <option value=""></option>
                                <?php foreach($services as $service) { ?>
                                    <option value="<?php echo $service->code; ?>" <?php foreach (explode(',', $currentFacility->services) as $key => $value)
                                                {
                                                    // echo $value;
                                                    if($value === $service->code) { echo "selected='selected'"; }
                                                }
                                             ?>>
                                        <?php echo $service->description; ?>
                                    </option>
                                <?php } ?>
                            </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="equipment" class="col-sm-3 control-label">Equipment</label>
                      <div class="col-sm-9">
                          <select id="equipment" class="form-control select2" name="equipment[]" multiple="multiple" style="width: 100%">
                                <option value=""></option>
                                <?php foreach($equipments as $equipment) { ?>
                                    <option value="<?php echo $equipment->code; ?>"<?php foreach (explode(',', $currentFacility->equipment) as $key => $value)
                                                {
                                                    // echo $value;
                                                    if($value === $equipment->code) { echo "selected='selected'"; }
                                                }
                                             ?>>
                                             <?php echo $equipment->description; ?>
                                    </option>
                                <?php } ?>
                            </select>
                      </div>
                    </div>
                  </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-success pull-right">Update Specilizations</button>
                </div><!-- /.box-footer -->
                {!! Form::close() !!}
              </div><!-- /.tab-pane -->

              <div class="tab-pane" id="tab_4">
                <h4>Workforce<small> (or labour force is the labour pool in employment) </small></h4>
                <!-- form start -->
                <!-- NOTE:: Fix this! -->
                {!! Form::open(array( 'url'=>$modulePath.'/updateworkforce/'.$currentFacility->facility_id, 'id'=>'facilityWorkforceForm', 'name'=>'facilityWorkforceForm', 'class'=>'form-horizontal' )) !!}
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-4 control-label"></label>
                      <label class="col-sm-4 control-label">Male</label>
                      <label class="col-sm-4 control-label">Female</label>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Physicians/Doctors</label>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="doctors_male" name="doctors_male" placeholder="# of Male Doctors" value="{{ $facility_workforce->doctors_male or NULL }}" />
                      </div>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="doctors_female" name="doctors_female" placeholder="# of Female Doctors" value="{{ $facility_workforce->doctors_female or NULL }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"> Dentists </label>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="dentists_male" name="dentists_male" placeholder="# of Male Dentists" value="{{ $facility_workforce->dentists_male or NULL }}" />
                      </div>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="dentists_female" name="dentists_female" placeholder="# of Female Dentists" value="{{ $facility_workforce->dentists_female or NULL }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"> Nurses </label>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="nurse_male" name="nurses_male" placeholder="# of Male Nurses" value="{{ $facility_workforce->nurses_male or NULL }}" />
                      </div>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="nurse_female" name="nurses_female" placeholder="# of Female Nurses" value="{{ $facility_workforce->nurses_female or NULL }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"> Midwives </label>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="midwives_male" name="midwives_male" placeholder="# of Male Midwives" value="{{ $facility_workforce->midwives_male or NULL }}" />
                      </div>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="midwives_female" name="midwives_female" placeholder="# of Female Midwives" value="{{ $facility_workforce->midwives_female or NULL }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"> Medical Technologists </label>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="medical_technologists_male" name="medical_technologists_male" placeholder="# of Male Medical Technologists" value="{{ $facility_workforce->medical_technologists_male or NULL }}" />
                      </div>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="medical_technologists_female" name="medical_technologists_female" placeholder="# of Female Medical Technologists" value="{{ $facility_workforce->medical_technologists_female or NULL }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"> Sanitary Engineers </label>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="sanitary_engineers_male" name="sanitary_engineers_male" placeholder="# of Male Sanitary Engineers" value="{{ $facility_workforce->sanitary_engineers_male or NULL }}" />
                      </div>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="sanitary_engineers_female" name="sanitary_engineers_female" placeholder="# of Female Sanitary Engineers" value="{{ $facility_workforce->sanitary_engineers_female or NULL }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"> Sanitary Inspectors </label>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="sanitary_inspectors_male" name="sanitary_inspectors_male" placeholder="# of Male Sanitary Inspectors" value="{{ $facility_workforce->sanitary_inspectors_male or NULL }}" />
                      </div>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="sanitary_inspectors_female" name="sanitary_inspectors_female" placeholder="# of Female Sanitary Inspectors" value="{{ $facility_workforce->sanitary_inspectors_female or NULL }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"> Nutritionists </label>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="nutritionists_male" name="nutritionists_male" placeholder="# of Male Nutritionists" value="{{ $facility_workforce->nutritionists_male or NULL }}" />
                      </div>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="nutritionists_female" name="nutritionists_female" placeholder="# of Female Nutritionists" value="{{ $facility_workforce->nutritionists_female or NULL }}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"> Active Barangay Health Workers </label>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="active_barangay_health_workers_male" name="active_barangay_health_workers_male" placeholder="# of Male Active Barangay Health Workers" value="{{ $facility_workforce->active_barangay_health_workers_male or NULL }}" />
                      </div>
                      <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="active_barangay_health_workers_female" name="active_barangay_health_workers_female" placeholder="# of Female Active Barangay Health Workers" value="{{ $facility_workforce->active_barangay_health_workers_female or NULL }}" />
                      </div>
                    </div>
                  </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-success pull-right">Update Workforce</button>
                </div><!-- /.box-footer -->
                {!! Form::close() !!}
              </div><!-- /.tab-pane -->

              <div class="tab-pane" id="tab_5">
                {!! Form::open(array( 'url'=>$modulePath.'/updatecatchmentarea/'.$currentFacility->facility_id, 'id'=>'facilityCatchmentArea', 'name'=>'facilityCatchmentAreaForm', 'class'=>'form-horizontal' )) !!}
                 <div class="box-body">
                    <h4>Add/Update Catchment Area</h4>
                    <h4><code><small>{{$currentFacility->facility_type}}: BRGY. {{getBrgyName($currentFacility->facility_contact->barangay)}}</small></code></h4>
                    <h4><small><b>(For RHUs, CHOs, MHCs and MHOs:</b> Add/Update your covered barangay health stations.)</small></h4> 
                    @if($brgys)
                      <div class="box-body">
                        <div class="form-group">
                          <label for="ca_barangayname" class="col-sm-3 control-label">Barangay Health Station Name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="ca_barangayname" name="ca_barangayname" placeholder="Facility Name" value="" />
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="ca_barangay" class="col-sm-3 control-label">Barangay</label>
                          <div class="col-sm-9">
                            <select name="ca_barangay" class="form-control" id="ca_barangay">
                                <option value="">Select Barangay</option>
                                @foreach($brgys as $brgy_k => $brgy_v)
                                  @if($brgy_v->barangay_code!=$currentFacility->facility_contact->barangay)
                                    <option value="{{$brgy_v->barangay_code}}">{{ $brgy_v->barangay_name }}</option>
                                  @endif
                                @endforeach
                            </select> 
                          </div>
                        </div>
                        <button type="submit" class="btn btn-success pull-right">Update Catchment Area</button>
                      </div>
                    @else
                    <div class="alert alert-error"> 
                        <p>No Barangay found. Please update your Facility contact address then try to logout and login again. Thank you!</p>
                    </div>  
                    @endif 

                  @if($currentFacility->facility_catchment_area !=NULL)
                  <div class="box-footer">
                    <h4>Catchment Area <small>(These are the covered barangay health stations of your facility.)</small></h4>

                   <table class="table table-hover" id="UseList">
                      <thead>
                        <tr>
                          <th>Barangay Health Station Name</th>
                          <th>Barangay</th>
                          <th class="text-center">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($currentFacility->facility_catchment_area !=NULL)
                        @foreach($currentFacility->facility_catchment_area as $fca_key => $fca_val)
                            <tr>
                              <td>
                                {!! Form::hidden('ca_id', $fca_val->ca_id) !!}
                                {{ $fca_val->bhs_name }}
                              </td>
                              <td>
                                {{ getBrgyName($fca_val->bhs_brgy_code) }}
                              </td>
                              <td>
                                <div class="btn-group">
                                  <a href="{{ url('/facilities/brgy_area/delete', [$fca_val->ca_id])}}" type="button" class="btn btn-danger btn-flat"><i class="fa fa-trash-o"></i> Delete</a>
                                </div>
                              </td>
                            </tr>
                        @endforeach
                      @else
                        <tr>
                          <td colspan='3'>
                            No Data Found
                          </td>
                        </tr>
                      @endif
                      </tbody>
                    </table>
                  </div>
                  @endif
                  </div>
                  
                {!! Form::close() !!}
              </div>

              
              <!-- insert tab plugins here -->

              @if($plugs)
                @foreach($plugs as $k=>$tab)
                  @if($tab['plugin_location'] == 'tab')
                    <div class="tab-pane" id="{{ $tab['plugin'] }}">
                      <?php
                        $brgy_data = array();
                        $brgy_data['brgys'] = $brgys;
                        $brgy_data['geodatabarangay'] = $geodatabarangay;

                        View::addNamespace('pluginform', plugins_path().$tab['folder']);
                        echo View::make('pluginform::'.strtolower($tab['plugin']), array('plugdata'=>$tab['pdata'], 'currentFacility' => $currentFacility, 'brgy_data' => $brgy_data))->render();
                      ?>
                    </div>
                  @endif
                @endforeach
              @endif

            </div><!-- /.tab-content -->
          </div><!-- nav-tabs-custom -->
        </div>
@stop

@section('before_validation_scripts')
{!! HTML::script('public/dist/plugins/chain/jquery.chained.min.js') !!}
{!! HTML::script('public/dist/plugins/chain/jquery.chained.remote.min.js') !!}
{!! HTML::script('public/dist/js/pages/users/userprofile.js') !!}

<script>
@if (Session::has('popup'))
    bootbox.alert({
      title: "Welcome",
      message: "Welcome to SHINE OS+. Please complete your facility and user profile to make the system more reliable and help for you."
    });
@endif

@if($currentFacility->catchment_area!=NULL AND ($currentFacility->facility_type == 'Rural Health Unit' OR $currentFacility->facility_type == 'RURAL_HEALTH_UNIT' OR $currentFacility->facility_type == 'City Health Office' OR $currentFacility->facility_type == 'Main Health Center' OR $currentFacility->facility_type == 'Municipal Health Office')) 
  bootbox.alert({
    title: "Catchment Area Update",
    message: "We are sorry to inform you that you have to update again your catchment area."
  });
@endif

// $(function() {
//     $('.chk_boxes').click(function() {
//         $('.checkbox_catchment').prop('checked', this.checked);
//     });
// });

</script>
@stop
