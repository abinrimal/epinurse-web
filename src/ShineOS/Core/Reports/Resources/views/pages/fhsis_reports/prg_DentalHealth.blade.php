@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<table class="table table-striped table-bordered table-report table-responsive">
    <thead>
        <tr><th colspan="4">DENTAL HEALTH</th></tr>
        <tr><th></th><th>Male</th><th>Female</th><th>Total</th></tr>
    </thead>
    <tbody>
        <tr>
            <td>Orally fit child</td>
            <td> {{ $data['OFC_M'] }} </td>
            <td> {{ $data['OFC_F'] }} </td>
            <td> {{ $data['OFC_M'] + $data['OFC_F'] }} </td>
        </tr>
        <tr>
            <td>Child 12-71 moths old provided with basic oral health care</td>
            <td> {{ $data['CHILD_BOHC_M'] }} </td>
            <td> {{ $data['CHILD_BOHC_F'] }} </td>
            <td> {{ $data['CHILD_BOHC_M'] + $data['CHILD_BOHC_F'] }} </td>
        </tr>
        <tr>
            <td>Adolescent and youth porovided with BOHC</td>
            <td> {{ $data['AY_BOHC_M'] }} </td>
            <td> {{ $data['AY_BOHC_F'] }} </td>
            <td> {{ $data['AY_BOHC_M'] + $data['AY_BOHC_F'] }} </td>
        </tr>
        <tr>
            <td>Pregnant women provided with BOHC</td>
            <td bgcolor="#A9A9A9"> </td>
            <td> {{ $data['PREG_BOHC'] }} </td>
            <td> {{ $data['PREG_BOHC'] }} </td>
        </tr>
        <tr>
            <td>Older persons provided with BOHC</td>
            <td> {{ $data['OLDPER_BOHC_M'] }} </td>
            <td> {{ $data['OLDPER_BOHC_F'] }} </td>
            <td> {{ $data['OLDPER_BOHC_M'] + $data['OLDPER_BOHC_F'] }} </td>
        </tr>
        
    </tbody>
</table>
 @yield('modal-footer')