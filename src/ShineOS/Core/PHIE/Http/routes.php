<?php

Route::group(['prefix' => 'phie',
                'namespace' => 'ShineOS\Core\PHIE\Http\Controllers', 'middleware' => 'auth.access:phie'], function()
{
    Route::get('/', 'PHIEController@index');
    Route::get('/submitDatatoPHIE/{facid}', 'PHIEController@submitDatatoPHIE');
    Route::get('/getMaster/{year}', 'PHIEController@getMaster');
    Route::get('/getSAP/{qyear}', 'PHIEController@getSAP');
    Route::get('/showDetails/{fid}/{date}', 'PHIEController@showDetails');

    // Route::post('/patientsData', 'PHIEController@PHIE_patientsData');
});
