@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<table class="table table-striped table-bordered table-report table-responsive">
    <thead>
    <tr>
    <th colspan="2">POPULATION</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Population Year</td>
        <td>{{ $data['POP_YEAR'] or 0 }}</td>
    </tr>
    <tr>
        <td>Barangay population</td>
        <td>{{$data['POP_BGY'] or 0 }}</td>
    </tr>
    <tr>
        <td>Number of Household</td>
        <td>{{$data['NO_HH'] or 0 }}</td>
    </tr>
    <tr>
        <td>Endemic Population for Filariasis</td>
        <td>{{$data['END_POP_FIL'] or 0 }}</td>
    </tr>
    <tr>
        <td>Endemic Population for Malaria</td>
        <td>{{$data['END_POP_MAL'] or 0 }}</td>
    </tr>
    <tr>
        <td>Endemic Population for Schistosomiasis</td>
        <td>{{$data['END_POP_SCH'] or 0 }}</td>
    </tr>
    </tbody>
</table> @yield('modal-footer')