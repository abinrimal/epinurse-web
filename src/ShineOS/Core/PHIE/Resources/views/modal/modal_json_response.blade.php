<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <h4 class="modal-title" id="myModalLabel"><i class="fa fa-database"></i> PHIE JSON Response </h4>
</div>

<div class="modal-body" id="modal-body">
  <table class="table table-bordered table-striped dataTable">
     @foreach($PHIESync_all as $a => $b)
     <?php $resp = json_decode($b->response); ?>
      @if (is_array($resp) || is_object($resp))
        @foreach($resp as $c => $d)
          <tr>
            <td style="vertical-align: top;!important">
              <h6> {{ $c }} </h6>
            </td>
            <td style="vertical-align: top;!important">
              <table class="table table-bordered table-striped">
                <?php $good = 0; ?>
                @foreach($d as $e => $f)
                  @if($f)
                      @if($f->response_code == '106')
                          <?php
                              $good++;
                              if($f === end($d)) echo "<h6>".$good." Records submitted.</h6>";
                          ?>
                      @elseif($f->response_code == '103' OR $f->response_code == '107')
                        <tr>
                          <td style="vertical-align: top;!important" width="50%">
                            <h6><dl>
                              <dt> Response Code </dt> <dd> {{ $f->response_code }} </dd>
                              <dt> Description </dt>
                              <dd>
                                <span class="label <?php if($f->response_code=='106') { echo "label-success"; } else { echo "label-danger"; }?>">
                                  {{ $f->response_desc }}
                                </span>
                              </dd>
                              <dt> Datetime </dt> <dd> {{ $f->response_datetime }} </dd>
                            </dl></h6>
                          </td>
                          <td width="50%" style="vertical-align: top;!important">
                            
                            <h6><dl>
                              @if($f->response_code == '103')
                                <?php 
                                  $fsoapresult = str_replace('\"','"',$f->soap_result);
                                  $resultjson = json_decode($fsoapresult);
                                ?>
                                @if(is_array($resultjson))
                                  @foreach($resultjson as $k => $l)
                                    $l = collect($l);
                                    @if(isset($l->SubmittedDetails->Pat_Facility_No))<dt> Patient ID </dt> <dd> {{ $l->SubmittedDetails->Pat_Facility_No }} </dd>@endif
                                    @if(isset($l->SubmittedDetails->Pat_First_Name))<dt> Patient Name </dt> <dd> {{ $l->SubmittedDetails->Pat_First_Name." ".$l->SubmittedDetails->Pat_Last_Name }} </dd>@endif
                                    @if(isset($l->Required_Content_Count))<dt> Required_Content_Count </dt> <dd> {{ $l->Required_Content_Count }} </dd>@endif
                                    @if(isset($l->Submitted_Content_Count)) <dt> Submitted_Content_Count </dt> <dd> {{ $l->Submitted_Content_Count }} </dd>@endif
                                    @if(isset($l->Unknown_Content_Count)) <dt> Unknown_Content_Count </dt> <dd> {{ $l->Unknown_Content_Count }}</dd> @endif
                                    @if(isset($l->Missing_Content_Count)) <dt> Missing_Content_Count </dt> <dd> {{ $l->Missing_Content_Count }}</dd> @endif
                                    @if(isset($l->Invalid_Content_Count)) <dt> Invalid_Content_Count </dt> <dd> {{ $l->Invalid_Content_Count }}</dd> @endif
                                    @if(isset($l->UnknownDetails) AND $l->UnknownDetails!=NULL) <dt> Unknown Details </dt>
                                      <dd>
                                        @foreach($l->UnknownDetails as $k_ud => $v_ud)
                                          {{ $k_ud }}: {{ $v_ud }}
                                        @endforeach
                                      </dd> @endif
                                    @if(isset($l->MissingDetails) AND $l->MissingDetails!=NULL) <dt> Missing Details </dt>
                                      <dd>
                                        @foreach($l->MissingDetails as $k_md => $v_md)
                                          {{ $k_md }}: {{ $v_md }}
                                        @endforeach
                                      </dd> @endif
                                    @if(isset($l->InvalidDetails) AND $l->InvalidDetails!=NULL) <dt> Invalid Details </dt>
                                      <dd>
                                        @foreach($l->InvalidDetails as $k_id => $v_id)
                                          {{ $k_id }}: {{ $v_id }} <br />
                                        @endforeach
                                      </dd> @endif
                                  @endforeach
                                @else
                                    @if(isset($resultjson->PHIE->SubmittedDetails->Pat_Facility_No))<dt> Patient ID </dt> <dd> <?php echo $resultjson->PHIE->SubmittedDetails->Pat_Facility_No; ?> 
                                    </dd>
                                    @endif
                                    @if(isset($resultjson->PHIE->SubmittedDetails->Pat_First_Name))<dt> Patient Name </dt> <dd> <?php echo $resultjson->PHIE->SubmittedDetails->Pat_First_Name." ".$resultjson->PHIE->SubmittedDetails->Pat_Last_Name; ?> </dd>
                                    @endif
                                    @if(isset($resultjson->PHIE->SubmittedDetails->Encounter_ID))<dt> Healthcareservice ID </dt> <dd> <?php echo $resultjson->PHIE->SubmittedDetails->Encounter_ID; ?> 
                                    </dd>
                                    <dt> Healthcareservice Date </dt> <dd> <?php echo $resultjson->PHIE->SubmittedDetails->Encounter_Date; ?> 
                                    </dd>
                                    @endif
                                    @if(isset($resultjson->PHIE->Required_Content_Count))<dt> Required_Content_Count </dt> <dd> <?php echo $resultjson->PHIE->Required_Content_Count; ?> </dd>
                                    @endif
                                    @if(isset($resultjson->PHIE->Submitted_Content_Count)) <dt> Submitted_Content_Count </dt> <dd> <?php echo $resultjson->PHIE->Submitted_Content_Count; ?> </dd>
                                    @endif
                                    
                                    @if(isset($resultjson->PHIE->Unknown_Content_Count)) 
                                      @if(isset($resultjson->PHIE->UnknownDetails) AND $resultjson->PHIE->UnknownDetails!=NULL) <dt> Unknown Details </dt>
                                        <dd>
                                          @foreach($resultjson->PHIE->UnknownDetails as $k_ud => $v_ud)
                                            <?php echo $k_ud; ?> : <?php echo $v_ud; ?>
                                          @endforeach
                                        </dd> 
                                      @endif 
                                    @endif
                                    
                                    @if(isset($resultjson->PHIE->Missing_Content_Count)) 
                                      @if(isset($resultjson->PHIE->MissingDetails) AND $resultjson->PHIE->MissingDetails!=NULL) <dt> Missing Details </dt>
                                        <dd>
                                          @foreach($resultjson->PHIE->MissingDetails as $k_md => $v_md)
                                            <?php echo $k_mdl ?> : <?php echo $v_md; ?>
                                          @endforeach
                                        </dd> 
                                      @endif
                                    @endif
                                    
                                    @if(isset($resultjson->PHIE->Invalid_Content_Count)) 
                                      @if(isset($resultjson->PHIE->InvalidDetails) AND $resultjson->PHIE->InvalidDetails!=NULL) <dt> Invalid Details </dt>
                                      <dd>
                                        @foreach($resultjson->PHIE->InvalidDetails as $k_id => $v_id)
                                          <?php echo $k_id; ?> : <?php echo $v_id; ?> <br />
                                        @endforeach
                                      </dd> 
                                      @endif 
                                    @endif
                                @endif

                              @elseif($f->response_code == '107')
                                <?php $fsoapresult = str_replace('\"', '"',$f->soap_result); 
                                $soapdecode = json_decode($fsoapresult);
                                ?>
                                @foreach($soapdecode as $i => $j)
                                <dt> PHIE </dt>
                                <dd> 
                                    @if(is_array($j))
                                      <?php print_r($j); ?>
                                    @else
                                      {{ $j }}
                                    @endif 
                                </dd>
                                @endforeach
                              @else
                                
                              @endif
                            </dl></h6>
                          </td>
                        </tr>
                      @endif
                  @endif
                @endforeach
                @if($b->excludeFile AND $b->excludeFile != '"null"')
                  <tr><td width="50%"></td><td>
                    Excluded files with errors found and not submitted.
                  </td></tr>
                @endif
              </table>
            </td>
          </tr>
        @endforeach
      @endif
     @endforeach
  </table>
</div>
<div class="modal-footer" id="modal-footer">
