
var url = '/healthcareservices/add';
var Healthcare = {};

function firedate(el)
{
    $(el).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
    $(el).inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
}

function getEndDate() {
    var num = $('#intake_input').val();
    var unit = $('#intakeothers').val();

    var u = 'days';
    if(unit == 'D') { u = 'days' };
    if(unit == 'W') { u = 'weeks' };
    if(unit == 'M') { u = 'months' };
    if(unit == 'Q') { u = 'quarters' };
    if(unit == 'Y') { u = 'years' };

    var now = $('.startdate').val();

    if(num && unit != 'O') {
        var result = moment(now, 'MM/DD/YYYY');
        if(u == 'days') {
            num = num - 1;
        }
        var end = result.add(num, u);
        $('.enddate').val( result.format('MM/DD/YYYY') );
    } else {
        $('.enddate').val( now );
    }
}

function fixDose(val)
{

  var qtype = new Array('Ampule','Bottles','Capsules','Caplets','Diskus','Ellipta','Inhaler','Insulin Pens','Lozenges','Nasal Spray','Nebule','Papertab','Pre-filled Syringe','Respimat','Sachet','Suppository','Suspension','Syrup','Tablets','Tube','Turbohaler','Unit dose Vial','Vials');
  $("#qtyUOMSel").html("<option value=''>Choose</option>");
  $(qtype).each(function(i, v){
      $("#qtyUOMSel").append($("<option>", { value: v, html: v }));
  });
  var dtype = new Array('application','capsule','click','drops','grams','puff','mg','ml','mcg','meq','sachet','tablets','teaspoon','tablespoon','units');
  $("#doseUOMSel").html("<option value=''>Choose</option>");
  $(dtype).each(function(i, v){
      $("#doseUOMSel").append($("<option>", { value: v, html: v }));
  });

    if(val.indexOf("Tablet") >= 0 || val.indexOf("TABLET") >= 0 ) {
        var qtype = new Array("Tablets");
        $("#qtyUOMSel").html("");
        $(qtype).each(function(i, v){
            $("#qtyUOMSel").append($("<option>", { value: v, html: v }));
        });
        var dtype = new Array("tablet");
        $("#doseUOMSel").html("");
        $(dtype).each(function(i, v){
            $("#doseUOMSel").append($("<option>", { value: v, html: v }));
        });
    }
    if(val.indexOf("Capsule") >= 0 || val.indexOf("CAPSULE") >= 0) {
        var qtype = new Array("Capsules");
        $("#qtyUOMSel").html("");
        $(qtype).each(function(i, v){
            $("#qtyUOMSel").append($("<option>", { value: v, html: v }));
        });
        var dtype = new Array("capsule");
        $("#doseUOMSel").html("");
        $(dtype).each(function(i, v){
            $("#doseUOMSel").append($("<option>", { value: v, html: v }));
        });
    }
    if(val.indexOf("Ointment") >= 0 || val.indexOf("Cream") >= 0) {
        var qtype = new Array("Bottle","Can","Sachet","Tube");
        $("#qtyUOMSel").html("<option value=''>Choose</option>");
        $(qtype).each(function(i, v){
            $("#qtyUOMSel").append($("<option>", { value: v, html: v }));
        });
        var dtype = new Array("application");
        $("#doseUOMSel").html("");
        $(dtype).each(function(i, v){
            $("#doseUOMSel").append($("<option>", { value: v, html: v }));
        });
    }
    if(val.indexOf("Suppository") >= 0 ) {
        var qtype = new Array("Suppository");
        $("#qtyUOMSel").html("");
        $(qtype).each(function(i, v){
            $("#qtyUOMSel").append($("<option>", { value: v, html: v }));
        });
        var dtype = new Array("units");
        $("#doseUOMSel").html("");
        $(dtype).each(function(i, v){
            $("#doseUOMSel").append($("<option>", { value: v, html: v }));
        });
    }
    if(val.indexOf("Sachet") >= 0 ) {
        var qtype = new Array("Sachet");
        $("#qtyUOMSel").html("");
        $(qtype).each(function(i, v){
            $("#qtyUOMSel").append($("<option>", { value: v, html: v }));
        });
        var dtype = new Array("units");
        $("#doseUOMSel").html("");
        $(dtype).each(function(i, v){
            $("#doseUOMSel").append($("<option>", { value: v, html: v }));
        });
    }
    if(val.indexOf("Suspension") >= 0 || val.indexOf("SUSPENSION") >= 0 || val.indexOf("Syrup") >= 0 || val.indexOf("Nasal") >= 0) {
      var qtype = new Array("Bottles","Nasal Spray","Suspension","Vials");
      $("#qtyUOMSel").html("<option value=''>Choose</option>");
      $(qtype).each(function(i, v){
          $("#qtyUOMSel").append($("<option>", { value: v, html: v }));
      });
      var dtype = new Array("click","drops","grams","mg","ml","mcg","spray","teaspoon","tablespoon");
      $("#doseUOMSel").html("<option value=''>Choose</option>");
      $(dtype).each(function(i, v){
          $("#doseUOMSel").append($("<option>", { value: v, html: v }));
      });
    }
    if(val.indexOf("Inhaler") >= 0) {
      var qtype = new Array("Inhaler","Turbohaler");
      $("#qtyUOMSel").html("<option value=''>Choose</option>");
      $(qtype).each(function(i, v){
          $("#qtyUOMSel").append($("<option>", { value: v, html: v }));
      });
      var dtype = new Array("grams","mg","ml","mcg","meq","units");
      $("#doseUOMSel").html("<option value=''>Choose</option>");
      $(dtype).each(function(i, v){
          $("#doseUOMSel").append($("<option>", { value: v, html: v }));
      });
    }
    if(val.indexOf("Injection") >= 0 || val.indexOf("INJECTION") >= 0) {
      var qtype = new Array("Ampules","Pre-filled Syringe","Tube", "Vials");
      $("#qtyUOMSel").html("<option value=''>Choose</option>");
      $(qtype).each(function(i, v){
          $("#qtyUOMSel").append($("<option>", { value: v, html: v }));
      });
      var dtype = new Array("ml","mcg","meq","units");
      $("#doseUOMSel").html("<option value=''>Choose</option>");
      $(dtype).each(function(i, v){
          $("#doseUOMSel").append($("<option>", { value: v, html: v }));
      });
    }
}

Healthcare.init = function () {
   $('#healthcare_services').on('change', function () {
        if($(this).val() == 'GeneralConsultation') {
            $('#medicalcategory').removeClass("hidden");
        } else {
            $('#medicalcategory').addClass("hidden");
        }
   });
  $('#consuTypeNewAdmit').on('click', function () {
        $('#inPatient').addClass("active");
        $('#inPatient input').attr("checked","checked");
        $('#outPatient').removeClass("active");
        $('#outPatient input').removeAttr("checked");
   });
  $('#consuTypeNewConsu').on('click', function () {
        $('#inPatient').removeClass("active");
        $('#inPatient input').removeAttr("checked");
        $('#outPatient').addClass("active");
        $('#outPatient input').attr("checked","checked");
   });
  $('#consuTypeFollow').on('click', function () {
        $('#inPatient').removeClass("active");
        $('#inPatient input').removeAttr("checked");
        $('#outPatient').addClass("active");
        $('#outPatient input').attr("checked","checked");
   });
  $("#diag_cat").remoteChained({
     parents : "#diag_parent",
     url : baseurl+"lov/api/diagnosis/category",
     loading : "Loading . . ."
  });

  $("#diag_subcat").remoteChained({
      parents : "#diag_cat",
      url : baseurl+"lov/api/diagnosis/subCat",
      loading : "Loading . . ."
  });

  $("#diag_subsubcat").remoteChained({
    parents : "#diag_subcat",
    url : baseurl+"lov/api/diagnosis/subsubCat",
    loading : "Loading . . ."
  });
  $('#OT').on('ifChecked', function(event){
        $("#labOthers").removeClass("hidden");
  });
  $('#OT').on('ifUnchecked', function(event){
        $("#labOthers").addClass("hidden");
  });

   Healthcare.computeBMI();
   if($('.diagnosis_input').length>0) {
       Healthcare.impAndDIag();
   }

   if($('.medicalOrders').length > 0){
       Healthcare.medOrder();
   }

   if($('#diagnosisEntry').length>0) {
       $(".select2").select2({
            placeholder: "You can enter multiple diagnosis",
            ajax: {
                url: baseurl+"lov/api/diagnosis",
                dataType: 'json',
                delay: 250,
                data: function (term) {
                    return {
                        q: term.term
                    };
                },
                processResults: function (data) {
                    var myResults = [];
                    //console.log(data);
                    $.each(data, function (index, item) {
                        myResults.push({
                            'id': index,
                            'text': item
                        });
                    });
                    //console.log(myResults);
                    return {
                        results: myResults
                    };
                },
                cache: true
            },
            tags: true
        });
        $(".select2").prop("disabled", false);
    }

   if(!$('#prescriptionTab.hidden').length) {
       //remove laboratory in selection
       $('#medorders').find('[value="MO_MED_PRESCRIPTION"]').remove();
   }
   if(!$('#procedureTab.hidden').length) {
       //remove others in selection
       $('#medorders').find('[value="MO_PROCEDURE"]').remove();
   }
   if(!$('#labTab.hidden').length) {
       //remove laboratory in selection
       $('#medorders').find('[value="MO_LAB_TEST"]').remove();
   }
   if(!$('#otherTab.hidden').length) {
       //remove others in selection
       $('#medorders').find('[value="MO_OTHERS"]').remove();
   }

}

Healthcare.procedureAutoComplete = function () {
    var c = 0;
    var procforms = $('.procedure_input');
    procforms.each(function() {
        $( ".procedure_input" ).autocomplete({
            source: availableProcedures
        });
        c++;
    })
}

Healthcare.fireProcedureAutoComplete = function(id)
{
    var procforms = $('.procedure_input');
    procforms.each(function() {
        console.log("id "+id);
        $( "#procedure_input"+id).autocomplete({
            source: availableProcedures
        });
    })
}

Healthcare.computeBMI = function () {
    if($('input[name="vitals[height]"]').val() != '' && $('input[name="vitals[weight]"]').val() != '') {
        var weight = $('input[name="vitals[weight]"]').val();
        var height = $('input[name="vitals[height]"]').val();
        var bmi = weight / (height / 100 * height / 100);
        bmi = Math.round(bmi * 100) / 100;
        $('p.bmiResult').text(isNaN(bmi) ? '' : bmi);
        $('input#bmi').val(isNaN(bmi) ? '' : bmi);

        if(bmi < 18.5) {
            $(".weightStat").html("<label class='control-label hidden-xs'>&nbsp;</label><p class='bmi_result control-label'>Patient is Underweight.</p>");
        }
        if(bmi >= 18.5 && bmi < 25) {
            $(".weightStat").html("<label class='control-label hidden-xs'>&nbsp;</label><p class='bmi_result control-label'>Patient is Normal.</p>");
        }
        if(bmi >= 25 && bmi < 30) {
            $(".weightStat").html("<label class='control-label hidden-xs'>&nbsp;</label><p class='bmi_result control-label'>Patient is Overweight.</p>");
        }
        if(bmi >= 30) {
            $(".weightStat").html("<label class='control-label hidden-xs'>&nbsp;</label><p class='bmi_result control-label'>Patient is Obese</p>");
        }
    }

    $('input[name="vitals[height]"], input[name="vitals[weight]"]').on('keyup keydown keypress click change', function () {
        var weight = $('input[name="vitals[weight]"]').val();
        var height = $('input[name="vitals[height]"]').val();
        var bmi = weight / (height / 100 * height / 100);
        bmi = Math.round(bmi * 100) / 100;
        $('p.bmiResult').text(isNaN(bmi) ? '' : bmi);
        //$('input[name=bmi]').val(isNaN(bmi) ? '' : bmi);

        if(bmi < 18.5) {
            $(".weightStat").html("<p class='bmi_result control-label'>Patient is Underweight.</p>");
        }
        if(bmi >= 18.5 && bmi < 25) {
            $(".weightStat").html("<p class='bmi_result control-label'>Patient is Normal.</p>");
        }
        if(bmi >= 25 && bmi < 30) {
            $(".weightStat").html("<p class='bmi_result control-label'>Patient is Overweight.</p>");
        }
        if(bmi >= 30) {
            $(".weightStat").html("<p class='bmi_result control-label'>Patient is Obese</p>");
        }
    });
}

Healthcare.impAndDIag = function () {
  var count = 0;
  //let us count all forms
  count = $('.diagnosis_input').length - 1;
  if (count>0)  $('.deleteRow').removeAttr('disabled');
  else $('.deleteRow').attr('disabled', 'disabled');

  $("label.diagnosisType").on('click', function(e) {
    var mydiagnosis = $( this ).find("input[type=radio]").attr('value');
    if(mydiagnosis == 'FINDX') {
        //let us check if the diagnosis field only contains one item
      var diagcount = $("#diagnosisEntry :selected").length;
      if(diagcount > 1) {
        //show popover alert
        $('#diagnosisEntry').popover('show');
        //prevent button to be clicked
        e.stopPropagation();
        //let us time the popover for 3 secs and hide it
        window.setTimeout(function() {
            $('#diagnosisEntry').popover('hide');
        }, 3000);
      } else {
        $("#FinalDiagnosis").removeClass("hidden");
        $("#diag_subsubcat.form-control").addClass('required');
        $("option[value='FINDX']").addClass("hidden");
        $(".addRow").addClass("hidden");
        //get the list of icd10 using the value of diagnosis
        var diagnose = $('#diagnosisEntry').val();
        var opts = "";
        $.ajax({
            url: baseurl+"healthcareservices/icd",
            data: {
                code: diagnose[0]
            },
                headers: {
                'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'POST',
            success: function(result){
                $("#diag_subsubcat.form-control").html(result);
            }});
      }
    } else {
      $("#FinalDiagnosis").addClass("hidden");
      $("#diag_subsubcat.form-control").removeClass('required');
      $("option[value='FINDX']").removeClass("hidden");
      $(".addRow").removeClass("hidden");
    }
  });

  $('.diagnosis_input').on('blur', function() {
    var svalue = $(".impressionDiagnosis input:checked").attr('value');
 
    if(svalue == 'FINDX') {
      $("#FinalDiagnosis").removeClass("hidden");
      $("#diag_subsubcat.form-control").addClass('required');
      $("option[value='FINDX']").addClass("hidden");
      $(".addRow").addClass("hidden");
      //get the list of icd10 using the value of diagnosis
      var diagnose = $('.diagnosis_input').val();
      var opts = "";
      $.ajax({
          url: baseurl+"healthcareservices/icd",
          data: {
              code: diagnose
          },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
          },
          method: 'POST',
          success: function(result){
            $("#diag_subsubcat.form-control").html(result);
        }});
    } else {
      $("#FinalDiagnosis").addClass("hidden");
      $("#diag_subsubcat.form-control").removeClass('required');
      $("option[value='FINDX']").removeClass("hidden");
      $(".addRow").removeClass("hidden");
    }
  });

}

function prescriptionEdit() {

    $(".prescription_edit").click(function(){

        $('.prescription_group input[name=PrescriptionMedID]').val($(this).parents('.medical_prescription_item').find('input.prescid').val());
        $('.prescription_group #drug_input').val($(this).parents('.medical_prescription_item').find('input.dcode').val());

        fixDose($(this).parents('.medical_prescription_item').find('input.dcode').val());

        $('.prescription_group input[name=Drug_Brand_Name]').val($(this).parents('.medical_prescription_item').find('input.brand').val());

        $('.prescription_group input[name=Dose_Qty]').val( $(this).parents('.medical_prescription_item').find('input.dqty').val() );
        $('.prescription_group select[name=Dose_UOM]').val( $(this).parents('.medical_prescription_item').find('input.dqtyuom').val() );
        $('.prescription_group input[name=Total_Quantity]').val($(this).parents('.medical_prescription_item').find('input.TQ').val());
        $('.prescription_group select[name=Total_Quantity_UOM]').val($(this).parents('.medical_prescription_item').find('input.TQuom').val());
        $('.prescription_group select[name=dosage]').val($(this).parents('.medical_prescription_item').find('input.regimen').val());
        $('.prescription_group input[name=Duration_Intake]').val($(this).parents('.medical_prescription_item').find('input.di').val());
        $('.prescription_group select[name=Duration_Intake_Freq]').val($(this).parents('.medical_prescription_item').find('input.dio').val());

        if($(this).parents('.medical_prescription_item').find('input.regimen').val() == 'OTH'){
            $('#forregimenothers').removeClass('hidden');
            $('.prescription_group input[name=Specify]').val($(this).parents('.medical_prescription_item').find('input.regimenothers').val())
        }
        if($(this).parents('.medical_prescription_item').find('input.regimen').val() == 'PRM'){
            $('#forregimenothers').addClass('hidden');
            $("#forregimenothers input").removeClass('required');
            $("#intakeGroup").addClass("hidden");
            $("#intake_input").removeClass('required');
            $("#intakeothers").removeClass("required");
            $("#forintakeothers").addClass("hidden");
            $("#forintakeothers input").removeClass("required");
            $("#regimen_range").addClass("hidden");
            $("#regimen_range input").removeClass("required");
        }

        if($(this).parents('.medical_prescription_item').find('input.dio').val() == 'O'){
            $('#forintakeothers').removeClass('hidden');
            $('.prescription_group input[name=IntakeOther]').val($(this).parents('.medical_prescription_item').find('input.intakeothers').val())
        }
        if($(this).parents('.medical_prescription_item').find('input.dio').val() != ''){
            rdate = $(this).parents('.medical_prescription_item').find('input.regimen_dates').val();
            mdates = rdate.split(' - ');
            $('#regimen_range').removeClass('hidden');
            $('.prescription_group input.startdate').val( mdates[0] );
            $('.prescription_group input.enddate').val( mdates[1] );
        }
        if($(this).parents('.medical_prescription_item').find('input.dio').val() == 'C'){
            $('.prescription_group input.startdate').val( $(this).parents('.medical_prescription_item').find('input.regimen_dates').val() );
            $('#regimen_range').addClass('hidden');
            $('#intake_input').removeClass('required');
            $('input[name=regimen_start_date]').removeClass('required');
            $('input[name=regimen_end_date]').removeClass('required');
        }
        //remove the listed prescription
        $(this).parents(".medical_prescription_item").remove();

        var newPrescCount = $('.medical_prescription_item.added').length;

        if(newPrescCount == 0) {
            $(".printPrescription").addClass('hidden');
        }
    })
}
function procedureEdit() {
    $(".procedure_edit").click(function(){

        $('.procedure_group input[name=ProcedureMedID]').val($(this).parents('.medical_procedure_item').find('input.procid').val());
        $('.procedure_group #procedure_input').val($(this).parents('.medical_procedure_item').find('input.procorder').val());
        $('.procedure_group #datepicker_future').val($(this).parents('.medical_procedure_item').find('input.procdate').val());
        $('.procedure_group input[name=Procedure_Remarks]').val($(this).parents('.medical_procedure_item').find('input.procinstruct').val());

        //remove the listed prescription
        $(this).parents(".medical_procedure_item").remove();

    })
}
function removePrescriptionTab() {
    $('a.removePrescriptionTab').click(function(){
        $('#medorders').append('<option value="MO_MED_PRESCRIPTION">Give Medical Prescription</option>');
        $('.medicalOrders .nav-tabs li.prescTab').addClass('hidden');
        $('#prescriptionTab').addClass('hidden');

        var pitems = $('.medical_prescription_item.added');
        if( pitems.length > 0) {
            pitems.each(function(){
                var id = $(this).find("input.remid").val();
                var mid = $(this).find("input.medid").val();
                $(this).html('<input type="hidden" name="medicalorders[delete][type][]" value="MO_MED_PRESCRIPTION" /><input type="hidden" name="medicalorders[delete][MO_MED_PRESCRIPTION][medicalorder_id][]" value="'+mid+'" /><input type="hidden" name="medicalorders[delete][MO_MED_PRESCRIPTION][medicalorderprescription_id][]" value="'+id+'" />').addClass('removed').removeClass("added");
            })
        }
        $('#prescriptionTab textarea.instructions').html("");
        $('#prescriptionTab').find('.form-control').val("");

    })
}
function removeProcedureTab() {
    $('a.removeProcedureTab').click(function(){
        $('#medorders').append('<option value="MO_PROCEDURE">Medical Procedure</option>');
        $('.medicalOrders .nav-tabs li.procTab').addClass('hidden');
        $('#procedureTab').addClass('hidden');

        var pitems = $('.medical_procedure_item.added');
        if( pitems.length > 0) {
            pitems.each(function(){
                var id = $(this).find("input.remid").val();
                var mid = $(this).find("input.medid").val();
                $(this).html('<input name="medicalorders[delete][type][]" value="MO_PROCEDURE" type="hidden"><input type="hidden" name="medicalorders[delete][MO_PROCEDURE][medicalorder_id][]" value="'+mid+'" /><input type="hidden" name="medicalorders[delete][MO_PROCEDURE][medicalorderprocedure_id][]" value="'+id+'" />').addClass('removed').removeClass("added");
            })
        }
        $('#procedureTab textarea.instructions').html("");
        $('#procedureTab').find('.form-control').val("");

    })
}
function removeLaboratoryTab() {
    $('a.removeLaboratoryTab').click(function(){
        $('#medorders').append('<option value="MO_LAB_TEST">Laboratory Exam</option>');
        $('.medicalOrders .nav-tabs li.labTab').addClass('hidden');
        $('#labTab').addClass('hidden');

        $('#labTab textarea.instructions').remove();
        $('#labTab').find('.form-control').val("");
        $('#labTab').find('.form-control').removeAttr("checked");
        $('#labTab').find('.form-control').html("");

    })
}
function removeOtherTab() {
    $('a.removeOtherTab').click(function(){
        $('#medorders').append('<option value="MO_OTHER">Specify Other</option>');
        $('.medicalOrders .nav-tabs li.otherTab').addClass('hidden');
        $('#otherTab').addClass('hidden');

        $('#otherTab textarea.instructions').html("");
        $('#otherTab').find('.form-control').val("");
        $('#otherTab').find('.form-control').html("");

    })
}

function lessPresc()
{
    $(".prescription_less").click(function(){
        var id = $(this).parents(".medical_prescription_item").find("input.remid").val();

        $(this).parents(".medical_prescription_item").html('<input type="hidden" name="medicalorders[delete][type][]" value="MO_MED_PRESCRIPTION" /><input type="hidden" name="medicalorders[delete][MO_MED_PRESCRIPTION][medicalorderprescription_id][]" value="'+id+'" />').addClass('removed').removeClass("added");

        var newPrescCount = $('.medical_prescription_item.added').length;
        if(newPrescCount == 0) {
            $(".printPrescription").addClass('hidden');
        }
    })
}

function lessProc()
{
    $(".procedure_less").click(function(){
        var id = $(this).parents(".medical_procedure_item").find("input.remid").val();

        $(this).parents(".medical_procedure_item").html('<input name="medicalorders[delete][type][]" value="MO_PROCEDURE" type="hidden"><input type="hidden" name="medicalorders[delete][MO_PROCEDURE][medicalorderprocedure_id][]" value="'+id+'" />').addClass('removed').removeClass("added");

    })
}

Healthcare.medOrder = function () {

  var count = 1;
  $('select[id=medorders]').on('change', function() {
      var $selected = $(this).val();

      //check if this is a new form
      if($selected == 'MO_MED_PRESCRIPTION') {
          $('#medorders').find('[value="MO_MED_PRESCRIPTION"]').remove();
          $('.medicalOrders .nav-tabs li.prescTab').removeClass('hidden');
          $('#prescriptionTab').removeClass('hidden');
          $('#medorderTabs a[href="#prescriptionTab"]').tab('show');
      }
      if($selected == 'MO_PROCEDURE') {
          $('#medorders').find('[value="MO_PROCEDURE"]').remove();
          $('.medicalOrders .nav-tabs li.procTab').removeClass('hidden');
          $('#procedureTab').removeClass('hidden');
          $('#medorderTabs a[href="#procedureTab"]').tab('show');
      }
      if($selected == 'MO_LAB_TEST') {
          $('#medorders').find('[value="MO_LAB_TEST"]').remove();
          $('.medicalOrders .nav-tabs li.labTab').removeClass('hidden');
          $('#labTab').removeClass('hidden');
          $('#medorderTabs a[href="#labTab"]').tab('show');
      }
      if($selected == 'MO_OTHERS') {
          $('#medorders').find('[value="MO_OTHERS"]').remove();
          $('.medicalOrders .nav-tabs li.otherTab').removeClass('hidden');
          $('#otherTab').removeClass('hidden');
          $('#medorderTabs a[href="#otherTab"]').tab('show');
      }

      $('#medorders').prop('selectedIndex', 0);
  });

  $('#regimenothers').change(function(e) {
      var value = $.trim( this.value );
      if(value=='OTH') {
        $("#forregimenothers").removeClass("hidden");
        $("#forregimenothers input").addClass('required');
        $("#intakeGroup").removeClass("hidden");
        $("#intake_input").addClass('required');
        $("#intakeothers").addClass("required");
        $("#forintakeothers").addClass("hidden");
        $("#forintakeothers input").removeClass("required");
        $("#regimen_range").removeClass("hidden");
        $("#regimen_range input").addClass("required");
      } else if(value=='PRM') {
        $("#forregimenothers").addClass("hidden");
        $("#forregimenothers input").removeClass('required');
        $("#intakeGroup").addClass("hidden");
        $("#intake_input").removeClass('required');
        $("#intakeothers").removeClass("required");
        $("#forintakeothers").addClass("hidden");
        $("#forintakeothers input").removeClass("required");
        $("#regimen_range").addClass("hidden");
        $("#regimen_range input").removeClass("required");
      } else {
        $("#forregimenothers").addClass("hidden");
        $("#forregimenothers input").removeClass('required');
        $("#intakeGroup").removeClass("hidden");
        $("#intake_input").addClass('required');
        $("#intakeothers").addClass("required");
        $("#forintakeothers").addClass("hidden");
        $("#forintakeothers input").removeClass("required");
        $("#regimen_range").removeClass("hidden");
        $("#regimen_range input").addClass("required");
      }
  });

  $('#intakeothers').change(function(e) {
      var value = $.trim( this.value );
      if(value=='O') {
        $("#forintakeothers").removeClass("hidden");
        $("#forintakeothers input").addClass("required");
      } else {
        $("#forintakeothers").addClass("hidden");
        $("#forintakeothers input").removeClass("required");
      }

      if(value!='C') {
        $("#regimen_range").removeClass("hidden");
        $("#intake_input").removeAttr('disabled');
        $("#intake_input").addClass('required');
        $("#regimen_range input").addClass("required");
      } else {
        $("#regimen_range").addClass("hidden");
        $("#regimen_range input").removeClass("required");
        $("#intake_input").attr('disabled','disabled');
        $("#intake_input").removeClass('required');
        $("#intake_input").val('');
      }
  });

  $('#startDate').on('apply.daterangepicker', function(ev, picker) {
      getEndDate();
  });

  $('.rmvbtn').click(function(){
        $(this).parent().parent().parent().parent().remove();
        if (count>1)  $('#rmvbtn').removeAttr('disabled');
        else $('#rmvbtn').attr('disabled', 'disabled');
    });

  $('.prescription_add').click(function() {
      var mayempty = 0;
      var itemCount = $('.medical_prescription_item.added').length;
      var f = "none";
      var chkforms = $('.prescription_group').find('.required');
      var formfield = "medicalorders[insert]";
      chkforms.each( function(e) {
          if( mayempty == 0 && ($(this).val() == "" || $(this).val() == "undefined" )) {
              f = $(this).attr('name');
              mayempty = 1;
          };
      })
      if($('input[name=PrescriptionMedID]').val()) {
          formfield = "medicalorders[update]";
      }

      if( $('input[name=Drug_Brand_Name]').val() ) {
          brand = "<p><strong>( "+$('input[name=Drug_Brand_Name]').val()+" )</strong><br />";
      } else {
          brand = "<p>";
      }

      var duration_of_intake = $('select[name=Duration_Intake_Freq]').val();
      var regimenstartenddate = '';
      var regimenstartenddatestring = '';
      if(duration_of_intake != 'C')
      {
        regimenstartenddate = $('input[name=regimen_start_date]').val() + " - " + $('input[name=regimen_end_date]').val();
        regimenstartenddatestring = ' [ ' + regimenstartenddate + ' ] ';
      } else {
        regimenstartenddate = $('input[name=regimen_start_date]').val();
        regimenstartenddatestring = '';
      }

      var dosage = $('select[name=dosage]').val();
      if($('select[name=dosage]').val() == 'OTH') {
        dosage = $('input[name=Specify]').val();
      }
      if($('select[name=dosage]').val() == 'PRM') {

      }

      var dregimen = $('select[name=dosage] option:selected').html();
      if( $('select[name=dosage]').val() == 'OTH' ) {
        dregimen = $('input[name=Specify]').val();
      }

      var newform = '<div class="medical_prescription_item added"><input name="' + formfield + '[type][]" value="MO_MED_PRESCRIPTION" type="hidden" class="presctype"><input class="remid" name="' + formfield + '[medicalorder_id][]" value="' + $('input[name=prescriptionmedicalorder_id]').val() + '" type="hidden"><input name="' + formfield + '[MO_MED_PRESCRIPTION][medicalorderprescription_id][]" class="prescid" type="hidden" value="' + $('input[name=PrescriptionMedID]').val() + '"><input name="' + formfield + '[MO_MED_PRESCRIPTION][Drug_Code][]" class="dcode" type="hidden" value="' + $('textarea[name=Drug_Code]').val() + '"><input name="' + formfield + '[MO_MED_PRESCRIPTION][Drug_Brand_Name][]" type="hidden" class="brand" value="' + $('input[name=Drug_Brand_Name]').val() + '"><input name="' + formfield + '[MO_MED_PRESCRIPTION][Dose_Qty][]" type="hidden" class="dqty" value="' + $('#dqtyinput').val() + '"><input name="' + formfield + '[MO_MED_PRESCRIPTION][Dose_UOM][]" class="dqtyuom" type="hidden" value="' + $('select[name=Dose_UOM]').val() + '"><input name="' + formfield + '[MO_MED_PRESCRIPTION][Total_Quantity][]" type="hidden" class="TQ" value="' + $('input[name=Total_Quantity]').val() + '"><input name="' + formfield + '[MO_MED_PRESCRIPTION][Total_Quantity_UOM][]" type="hidden" class="TQuom" value="' + $('select[name=Total_Quantity_UOM]').val() + '"><input name="' + formfield + '[MO_MED_PRESCRIPTION][dosage][]" type="hidden" class="regimen" value="' + $('select[name=dosage]').val() + '"><input name="' + formfield + '[MO_MED_PRESCRIPTION][Specify][]" type="hidden" class="regimenothers" value="' + $('input[name=Specify]').val() + '"><input name="' + formfield + '[MO_MED_PRESCRIPTION][Duration_Intake][]" type="hidden" class="di" value="' + $('input[name=Duration_Intake]').val() + '"><input name="' + formfield + '[MO_MED_PRESCRIPTION][Duration_Intake_Freq][]" type="hidden" class="dio" value="' + $('select[name=Duration_Intake_Freq]').val() + '"><input name="' + formfield + '[MO_MED_PRESCRIPTION][regimen_startend_date][]" type="hidden" class="regimen_dates" value="' + regimenstartenddate + '"><input name="' + formfield + '[MO_MED_PRESCRIPTION][Remarks][]" type="hidden" class="remarks" value="' + $('textarea[name=Remarks]').val() + '"><div class="col-md-12 form-group dynamic-row"><label class="col-md-1 control-label">&nbsp;</label><div class="col-md-10 has-feedback bordered-bottom"><h4>' + $('textarea[name=Drug_Code]').val() + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#' + $('input[name=Total_Quantity]').val() + ' ' + $('select[name=Total_Quantity_UOM]').val() + '</h4>' + brand + '<br />' + $('input[name=Dose_Qty]').val() + ' ' + $('select[name=Dose_UOM]').val() + ' - ' + dregimen;

     if( $('select[name=dosage]').val() != 'PRM' ) {
        newform += '<br />' + $('input[name=Duration_Intake]').val() + ' ' + $('select[name=Duration_Intake_Freq] option:selected').html() + regimenstartenddatestring + '<br /><em>' + $('textarea[name=Remarks]').val() + '</em>';
     }

     newform += '</p></div><div class="col-md-1"><span class="btn btn-default btn-sm prescription_less" id="" title="Remove Prescription"><i class="fa fa-times"></i></span><span class="btn btn-default btn-sm prescription_edit" id="" title="Edit Prescription"><i class="fa fa-pencil"></i></span></div></div>';


      if(mayempty == 0) {
          $('#medical_prescription_data').append(newform);
          $('.prescription_group').find('.form-control').val("");
          $('#forregimenothers').addClass('hidden');
      } else {
        bootbox.alert({
          title: "Incomplete Form!",
          message: "Please complete the form and try again. "+f+" is empty."
        });
      }

      prescriptionEdit();
      lessPresc();

  })

  $('.procedure_add').click(function() {
      var mayempty = 0;
      var itemCount = $('.medical_procedure_item.added').length;

      var f = "none";
      var chkforms = $('.procedure_group').find('.required');
      chkforms.each( function(e) {
          if( mayempty == 0 && ($(this).val() == "" || $(this).val() == "undefined" )) {
              f = $(this).attr('name');
              mayempty = 1;
          };
      })
      var formfield = "medicalorders[insert]";
      if($('input[name=ProcedureMedID]').val()) {
          formfield = "medicalorders[update]";
      }

      var newform = '<div class="medical_procedure_item added"><input name="'+formfield+'[type][]" value="MO_PROCEDURE" type="hidden" class="proctype"><input name="'+formfield+'[medicalorder_id][]" value="'+$('input[name=proceduremedicalorder_id]').val()+'" type="hidden" class="medorderid"><input class="remid" name="'+formfield+'[MO_PROCEDURE][medicalorderprocedure_id][]" type="hidden" class="procid" value="'+$('input[name=ProcedureMedID]').val()+'"><input name="'+formfield+'[MO_PROCEDURE][Procedure_Order][]" type="hidden" class="procorder" value="'+$('textarea[name=Procedure_Order]').val()+'"><input name="'+formfield+'[MO_PROCEDURE][Date_of_Procedure][]" type="hidden" class="procdate" value="'+$('input[name=Date_of_Procedure]').val()+'"><input name="'+formfield+'[MO_PROCEDURE][Procedure_Remarks][]" type="hidden" class="procinstruct" value="'+$('textarea[name=Procedure_Remarks]').val()+'"><div class="col-md-12 form-group dynamic-row"><label class="col-md-1 control-label">&nbsp;</label><div class="col-md-10 has-feedback bordered-bottom"><h4>'+$('textarea[name=Procedure_Order]').val()+'</h4><p>'+$('input[name=Date_of_Procedure]').val()+'<br /><em>'+$('textarea[name=Procedure_Remarks]').val()+'</em></p></div><div class="col-md-1"><span class="btn btn-default btn-sm procedure_less" id="" title="Remove Procedure"><i class="fa fa-times"></i></span><span class="btn btn-default btn-sm procedure_edit" id="" title="Edit Procedure"><i class="fa fa-pencil"></i></span></div></div>';

      if(mayempty == 0) {
          $('#medical_procedure_data').append(newform);
          $('.procedure_group').find('.form-control').val("");
      } else {
          bootbox.alert({
              title: "Incomplete Form!",
              message: "Please complete the form and try again. "+f+" is empty."
            });
      }

      procedureEdit();
      lessProc();

  })

  $('#saveMedOrder').click( function(e){

       if( $('#drug_input').val() != '') {
           e.preventDefault();
           var $this = $(this);
           bootbox.confirm({
               message: "There is a new Prescription entry that has not been added to the list.<br />If you choose Continue, it will not be saved.",
               title: "Form Alert",
               buttons: {
                   cancel: {
                       label: "Cancel"
                   },
                   confirm: {
                       label: "Continue"
                   }
               },
               callback: function(result){
                   if (result) {
                      $this.submit();
                   }
               }
           });
       }
      if( $('#procedure_input').val() != '') {
           e.preventDefault();
           var $this = $(this);
           bootbox.confirm({
               message: "There is a new Medical Procedure entry that has not been added to the list.<br />If you choose Continue, it will not be saved.",
               title: "Form Alert",
               buttons: {
                   cancel: {
                       label: "Cancel"
                   },
                   confirm: {
                       label: "Continue"
                   }
               },
               callback: function(result){
                   if (result) {
                      $this.submit();
                   }
               }
           });
       }

   })

    $('.dqtyRadio').click( function() {
        $('#dqtyinput').val("").removeClass('required');
    })
    $('#dqtyinput').focus( function() {
        $('.dqtyRadio').removeClass('active');
        $('.dqtyRadio input').removeAttr('selected');
        $('#dqtyinput').addClass('required');
    })

    if($('.medicalOrders').length > 0){
        $( "#drug_input" ).autocomplete({
             source: availableDrugs
         });
 
        $("#procedure_input").autocomplete({
             source: availableProcedures
         });
    }

    lessPresc();
    lessProc();

    prescriptionEdit();
    procedureEdit();

    removePrescriptionTab();
    removeProcedureTab();
    removeLaboratoryTab();
    removeOtherTab();

}

$(document).ready(function() {
   Healthcare.init();
});
