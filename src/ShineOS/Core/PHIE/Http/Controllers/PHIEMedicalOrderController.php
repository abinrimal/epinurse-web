<?php namespace ShineOS\Core\PHIE\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\LOV\Entities\LovDrugs;
use ShineOS\Core\LOV\Entities\LovMedicalProcedures;
use Request, Input, DateTime, DB, Session;

class PHIEMedicalOrderController extends Controller {


    public function __construct() {
        $this->PHIE_MO_PrescriptionRequired = array('Drug_Code',
                                                    'Dose_Qty',
                                                    'Dose_UOM',
                                                    'Dosage_Regimen',
                                                    'Duration_Intake',
                                                    'Duration_Intake_Freq',
                                                    'Total_Quantity',
                                                    'Total_Quantity_UOM'
                                                    );
    }

    public function healthcaredataByFacilityId($facility_id, $orderType,$compareToDate, $data_Encounter_ID=NULL) {
        $PHIEController = new PHIEController;
        $procedures = Healthcareservices::query();
        if(!empty($data_Encounter_ID)) {
            $procedures = $procedures->whereIn('healthcare_services.healthcareservice_id', $data_Encounter_ID);
        } else {
            $arr_param_type_id = array();
            $param_type_id = $PHIEController->PHIESync_list($facility_id, NULL, 'EncounterData');
            foreach ($param_type_id as $key => $value) {
                $arr_param_type_id[] = json_decode($value->param_type_id);
            }
            $procedures = $procedures->whereIn('healthcare_services.healthcareservice_id', array_unique(array_flatten($arr_param_type_id)));
        }

        $procedures = $procedures->has('MedicalOrder')
                ->with(array('MedicalOrder' => function($query) use ($orderType) {
                        $query->with($orderType);
                        $query->has($orderType);
                    }))
                ->join('facility_patient_user','healthcare_services.facilitypatientuser_id','=','facility_patient_user.facilitypatientuser_id')
                ->join('facility_user','facility_patient_user.facilityuser_id','=','facility_user.facilityuser_id')
                ->join('facilities','facilities.facility_id','=','facility_user.facility_id')
                ->where('facilities.facility_id', $facility_id)
                ->where('healthcare_services.deleted_at','=',NULL)
                ->where('facility_patient_user.deleted_at','=',NULL)
                ->where('healthcare_services.updated_at','>',$compareToDate)
                ->whereYear('healthcare_services.encounter_datetime', '=', 2017)
                ->whereMonth('healthcare_services.encounter_datetime', '>', 3)
                ->whereMonth('healthcare_services.encounter_datetime', '<', 7)
                ->get();

        return $procedures;
    }

    public function PHIE_MO_Examinations($facility_id, $EncounterData) {
        $param_type = 'ExaminationData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_MO_Examinations = [];
        $Examinations = $this->healthcaredataByFacilityId($facility_id, "MedicalOrderLabExam",$compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if($Examinations) {
            $c = count($Examinations); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($Examinations as $key => $value) {
                foreach ($value->MedicalOrder as $key_mo => $value_mo) {
                    foreach ($value_mo->MedicalOrderLabExam as $key_exam => $value_exam) {
                        $value_exam->examresult = DB::table('laboratory_result')->where('medicalorderlaboratoryexam_id', $value_exam->medicalorderlaboratoryexam_id)->first();

                        if($value_exam->examresult!=NULL) {
                            $PHIE_MO_Examinations[$key]['Pat_Facility_No'] = !is_null($value->patient_id) ? $value->patient_id : "";
                            $PHIE_MO_Examinations[$key]['Encounter_ID'] = !is_null($value->healthcareservice_id) ?  $value->healthcareservice_id : "";

                            if(!is_null($value_exam->laboratory_test_type) AND ($value_exam->laboratory_test_type == 'CX' OR $value_exam->laboratory_test_type == 'LP' OR $value_exam->laboratory_test_type == 'FB' OR $value_exam->laboratory_test_type == 'SM') ) {
                                $PHIE_MO_Examinations[$key]['Examination_Code'] = "OT";
                                if($value_exam->laboratory_test_type == 'CX')
                                    $PHIE_MO_Examinations[$key]['Examination_Others'] = "Chest X-Ray";
                                if($value_exam->laboratory_test_type == 'LP')
                                    $PHIE_MO_Examinations[$key]['Examination_Others'] = "Lipid Profile";
                                if($value_exam->laboratory_test_type == 'SM')
                                    $PHIE_MO_Examinations[$key]['Examination_Others'] = "Sputum Microscopy";
                                if($value_exam->laboratory_test_type == 'FB')
                                    $PHIE_MO_Examinations[$key]['Examination_Others'] = "Fasting Blood Sugar";
                            } else {
                                $PHIE_MO_Examinations[$key]['Examination_Code'] = $value_exam->laboratory_test_type;
                                $PHIE_MO_Examinations[$key]['Examination_Others'] = "NA";
                            }

                            $Date_of_Result = "0000-00-00";
                            $Time_of_Result = "00:00:00";
                            if(!is_null($value_exam->examresult)) {
                                $datetime_1 = new Datetime($value_exam->examresult->updated_at);
                                $Date_of_Result = $datetime_1->format('Y-m-d');
                                $Time_of_Result = $datetime_1->format('H:i:s');
                            }

                            $PHIE_MO_Examinations[$key]['Date_of_Result'] = $Date_of_Result;
                            $PHIE_MO_Examinations[$key]['Time_of_Result'] = $Time_of_Result;
                            $PHIE_MO_Examinations[$key]['Examination_Result'] = !is_null($value_exam->examresult) ?  $value_exam->examresult->lab_data : "";
                        }
                    }
                }
            }

            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_MO_Examinations) ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $PHIE_MO_Examinations;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

/**
 * Procedures
 */
    public function PHIE_MO_Procedures($facility_id, $EncounterData) {
        $param_type = 'ProcedureData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_MO_Procedures = [];
        $procedures = $this->healthcaredataByFacilityId($facility_id, "MedicalOrderProcedure",$compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");
        if($procedures) {
            $c = count($procedures); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($procedures as $key => $value) {
                foreach ($value->MedicalOrder as $key_mo => $value_mo) {
                    foreach ($value_mo->MedicalOrderProcedure as $key_proc => $value_proc) {

                        $LovMedicalProcedures = LovMedicalProcedures::where('procedure_description','LIKE',$value_proc->procedure_order)->select('procedure_code')->first();
                        $PHIE_MO_Procedures[$key]['Pat_Facility_No'] = !is_null($value->patient_id) ? $value->patient_id : "";
                        $PHIE_MO_Procedures[$key]['Encounter_ID'] = !is_null($value->healthcareservice_id) ?  $value->healthcareservice_id : "";

                        $PHIE_MO_Procedures[$key]['Procedure_RVS'] = !is_null($LovMedicalProcedures) ?  $LovMedicalProcedures->procedure_code : "99999";
                        if($PHIE_MO_Procedures[$key]['Procedure_RVS'] == '99999') {
                            $PHIE_MO_Procedures[$key]['Procedure_Other_Specify'] = !empty($value_proc->procedure_order) ? $value_proc->procedure_order : "Not Stated";
                        } else {
                            $PHIE_MO_Procedures[$key]['Procedure_Other_Specify'] = "NA";
                        }

                        if(is_null($value_proc->procedure_date)==FALSE) {
                            $datetime = new Datetime($value_proc->procedure_date);
                            $date = $datetime->format('Y-m-d');
                            $time = $datetime->format('H:i:s');
                        } else {
                            $date = "";
                            $time = "";
                        }
                        $PHIE_MO_Procedures[$key]['Date_of_Procedure'] = $date;
                        $PHIE_MO_Procedures[$key]['Time_of_Procedure'] = $time;
                    }
                }
            }

            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_MO_Procedures) ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $PHIE_MO_Procedures;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

/**
 * Drugs and Medicines Prescription
 */
    public function PHIE_MO_Prescription($facility_id, $EncounterData) {
        $param_type = 'DrugMedicinePrescriptionData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_MO_Prescription = [];
        $Prescription = $this->healthcaredataByFacilityId($facility_id, "MedicalOrderPrescription",$compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");
        if($Prescription) {
            $c = count($Prescription); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($Prescription as $key => $value) {
                foreach ($value->MedicalOrder as $key_mo => $value_mo) {
                    foreach ($value_mo->MedicalOrderPrescription as $key_pres => $value_pres) {

                        $PHIE_MO_Prescription[$key]['Pat_Facility_No'] = !is_null($value->patient_id) ? $value->patient_id : "";
                        $PHIE_MO_Prescription[$key]['Encounter_ID'] = !is_null($value->healthcareservice_id) ?  $value->healthcareservice_id : "";

                        $LovDrugs = LovDrugs::where('drug_specification','LIKE','%'.$value_pres->generic_name.'%')->select('product_id')->first();
                        $PHIE_MO_Prescription[$key]['Drug_Code'] = $LovDrugs['product_id'];
                        //$PHIE_MO_Prescription[$key]['Drug_Name'] = !is_null($value_pres->generic_name) ?  $value_pres->generic_name : "";

                        $dose_quantity = !is_null($value_pres->dose_quantity) ?  explode(" ", $value_pres->dose_quantity) : NULL;
                        $PHIE_MO_Prescription[$key]['Dose_Qty'] = !is_null($dose_quantity) ? array_key_exists(0, $dose_quantity) ? $dose_quantity[0] : 0 : 0;
                        $PHIE_MO_Prescription[$key]['Dose_UOM'] = !is_null($dose_quantity) ? array_key_exists(1, $dose_quantity) ? $dose_quantity[1] : "unit" : "unit";

                        $PHIE_MO_Prescription[$key]['Dosage_Regimen'] = !is_null($value_pres->dosage_regimen) ?  $value_pres->dosage_regimen : "Not given";
                        $PHIE_MO_Prescription[$key]['Dosage_Regimen_Others_Speciy'] = !is_null($value_pres->dosage_regimen_others) ?  $value_pres->dosage_regimen_others : "NA";

                        $duration_of_intake = !is_null($value_pres->duration_of_intake) ?  explode(" ", $value_pres->duration_of_intake) : NULL;

                        $PHIE_MO_Prescription[$key]['Duration_Intake'] = !is_null($duration_of_intake) ? array_key_exists(0, $duration_of_intake) ? $duration_of_intake[0] : 0 : 0;

                        //FOR REVIEW OF DOH
                        $PHIE_MO_Prescription[$key]['Duration_Intake_Freq'] = !is_null($duration_of_intake) ? array_key_exists(1, $duration_of_intake) ? "O" : "O" : "O";

                        $PHIE_MO_Prescription[$key]['Duration_Intake_Freq_Others_Specify'] = !is_null($value_pres->Duration_Intake_Freq_Others_Specify) ?  $value_pres->Duration_Intake_Freq_Others_Specify : "NA";

                        $total_quantity = !is_null($value_pres->total_quantity) ?  explode(" ", $value_pres->total_quantity) : "";
                        $PHIE_MO_Prescription[$key]['Total_Quantity'] = !is_null($total_quantity) ? array_key_exists(0, $total_quantity) ? $total_quantity[0] : "" : "";
                        $PHIE_MO_Prescription[$key]['Total_Quantity_UOM'] = !is_null($total_quantity) ? array_key_exists(1, $total_quantity) ? $total_quantity[1] : "" : "";

                        // need to ask DOH about this
                        // $PHIE_MO_Prescription[$key]['Purpose'] = !is_null($value_pres->Purpose) ?  $value_pres->Purpose : "";

                        $PHIE_MO_Prescription[$key]['Remarks'] = !is_null($value_pres->prescription_remarks) ?  $value_pres->prescription_remarks : "NA";

                        //Despensed drugs - not yet implemented
                        // $PHIE_MO_Prescription[$key]['Number_Given'] = !is_null($value_pres->Number_Given) ?  $value_pres->Number_Given : "";
                    }
                }
            }

            $out = false;
            if(count($PHIE_MO_Prescription)) {
                foreach ($PHIE_MO_Prescription as $result_key => $result_value) {
                    foreach ($result_value as $val_key => $val_value) {
                        $exists = in_array($val_key, $this->PHIE_MO_PrescriptionRequired);
                        if($exists) {
                            if($val_value == NULL) {
                                $this_errors[] = $val_key;
                                $out = true;
                            }
                        }
                    }
                    if($out == true) {
                        $fullname = returnFullNameOfPatientID($PHIE_MO_Prescription[$result_key]['Pat_Facility_No']);
                        $encounter_date = getCompleteHealthRecordByServiceID($PHIE_MO_Prescription[$result_key]['Encounter_ID']);
                        $PHIE_Excluded_Encounter[] = [ 'id'=>$result_key, 'Encounter Date'=>$encounter_date[0]->encounter_datetime, 'Patient ID' => $PHIE_MO_Prescription[$result_key]['Pat_Facility_No'], 'Healthcare ID' => $PHIE_MO_Prescription[$result_key]['Encounter_ID'], 'Pateint Name'=>$fullname, 'Incomplete Data:'=>$this_errors];
                        $out = false;
                        $this_errors = [];
                        unset($PHIE_MO_Prescription[$result_key]);
                    }
                }
            }
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_MO_Prescription) ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $PHIE_MO_Prescription;
            $filer = $PHIE_Excluded_Encounter;
            $result = $PHIEController->sendArray($data, $param_type, NULL, $filer);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }
}
