<div class="box box-primary" id="content_overview">
    <div class="box-header ui-sortable-handle">
        <i class="fa fa-eye"></i>
        <h3 class="box-title text-shine-blue">At a glance</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <?php
                if( strlen($dashboard_count['patient']) >= 5 )
                {
                    $class = "class='col-lg-12 col-md-12 col-sm-12 col-xs-12 wide'";
                }
                elseif( strlen($dashboard_count['patient']) == 4 )
                {
                    $class = "class='col-lg-6 col-md-6 col-sm-12 col-xs-12 narrow'";
                }
                else
                {
                    $class = "class='col-lg-6 col-md-6 col-sm-12 col-xs-12'";
                }
            ?>
            <div <?php echo $class; ?>>
                <!-- small box -->
                <div class="overview-box small-box bg-smartred">
                    <a href="{{ url('/records')}}" class="ajax-link">
                        <div class="inner">
                            <div class="icon"><i class="ion ion-ios-people"></i></div>
                            <h3>{{ $dashboard_count['patient'] }}</h3>
                            <p>Basic Information</p>
                        </div>

                    </a>
                </div>
            </div><!-- ./col -->

            @if(Config::get('config.mode') != 'ce')
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <!-- small box -->
                    <div class="overview-box small-box bg-smartolive">
                    <a href="{{ url('/records')}}" class="ajax-link">
                        <div class="inner">
                            <h3>{{ isset($dashboard_count['inbound']) ? $dashboard_count['inbound'] : 0 }}</h3>
                            <p>Health Assessment</p>
                        </div>
                        <div class="icon"><i class="ion ion-ios-download"></i></div>
                        </a>
                    </div>
                </div><!-- ./col -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <!-- small box -->
                    <div class="overview-box small-box bg-smartlitblue">
                    <a href="{{ url('/records')}}" class="ajax-link">
                        <div class="inner">
                            <h3>{{ isset($dashboard_count['outbound']) ? $dashboard_count['outbound'] : 0 }}</h3>
                            <p>Mother and Child</p>
                        </div>
                        <div class="icon"><i class="ion ion-ios-upload"></i></div>
                        </a>
                    </div>
                </div><!-- ./col -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <!-- small box -->
                    <div class="overview-box small-box bg-yellow">
                    <a href="{{ url('/records')}}" class="ajax-link">
                        <div class="inner">
                            <h3>{{ isset($dashboard_count['referral']) ? $dashboard_count['referral'] : 0 }}</h3>
                            <p>Community Nursing</p>
                        </div>
                        <div class="icon"><i class="ion ion-chatbubbles"></i></div>
                        </a>
                    </div>
                </div><!-- ./col -->

            @endif
        </div><!-- /.row -->
    </div>
</div><!--./box-->
