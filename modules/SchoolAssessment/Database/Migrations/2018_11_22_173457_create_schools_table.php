<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('school_id',60);
            $table->string('facility_id',60);

            $table->string('uuid',100)->nullable();

            $table->string('school_name',100)->nullable();
            $table->string('contact_person',100)->nullable();
            $table->string('contact_person_position',100)->nullable();

            $table->integer('activities_student_health_assessment')->nullable();
            $table->integer('activities_staff_members_health_assessment')->nullable();
            $table->integer('activities_sanitation_class')->nullable();
            $table->integer('activities_diet_class')->nullable();

            $table->double('school_distance')->nullable();

            $table->integer('ecd_program')->nullable();
            $table->integer('ecd_teacher')->nullable();
            $table->integer('ecd_classroom')->nullable();
            $table->integer('ecd_toilet')->nullable();
            $table->integer('ecd_seat_arrangement')->nullable();
            $table->integer('ecd_collision_avoidance')->nullable();
            $table->integer('ecd_appr_play_material')->nullable();
            $table->integer('ecd_non_slip_floors')->nullable();
            $table->integer('ecd_colors')->nullable();
            $table->integer('ecd_lighting')->nullable();

            $table->integer('eca_playground')->nullable();
            $table->integer('eca_spacing')->nullable();

            $table->integer('eca_dancing')->nullable();
            $table->integer('eca_singing')->nullable();
            $table->integer('eca_running')->nullable();
            $table->integer('eca_football')->nullable();
            $table->integer('eca_volleyball')->nullable();
            $table->integer('eca_basketball')->nullable();
            $table->integer('eca_pingpong')->nullable();
            $table->integer('eca_badminton')->nullable();
            $table->integer('eca_martialarts')->nullable();
            $table->integer('eca_oth')->nullable();
            $table->string('eca_are_available_oth_spec',100)->nullable();

            $table->integer('first_aid_box')->nullable();
            $table->integer('first_aid_adequacy')->nullable();
            $table->integer('first_aid_students')->nullable();

            $table->integer('water_type')->nullable();
            $table->integer('water_availability')->nullable();

            $table->integer('nutrition_provided_food')->nullable();
            $table->integer('nutrition_type_meal_breakfast')->nullable();
            $table->integer('nutrition_type_meal_lunch')->nullable();
            $table->integer('nutrition_type_meal_snacks')->nullable();
            $table->integer('nutrition_type_meal_dinner')->nullable();

            $table->integer('food_hygiene')->nullable();
            $table->integer('food_balanced')->nullable();
            $table->integer('food_from_home')->nullable();
            $table->integer('food_junk')->nullable();

            $table->integer('toil_has_running_water')->nullable();
            $table->integer('toil_has_cleaning_water')->nullable();
            $table->integer('toil_has_soap')->nullable();
            $table->integer('toil_has_sep_gender')->nullable();
            $table->integer('toil_has_sep_student_staff')->nullable();
            $table->integer('toil_has_trash')->nullable();
            $table->integer('toil_has_adequate_toilets')->nullable();

            $table->integer('toilet_clean_range')->nullable();

            $table->integer('school_location')->nullable();
            $table->string('school_location_oth_spec',100)->nullable();
            $table->integer('building_type')->nullable();
            $table->integer('age_building')->nullable();
            $table->integer('height_building')->nullable();
            $table->integer('water_damage')->nullable();
            $table->integer('fire_fighting_instrument')->nullable();
            $table->integer('disaster_drill')->nullable();
            $table->string('dd_how_often',100)->nullable();
            $table->integer('disaster_preparedness')->nullable();
            $table->string('dp_specify',100)->nullable();
            $table->integer('earthquake_resistant')->nullable();

            // $table->text('hazards')->nullable();

            $table->integer('earthquake_hazards')->nullable();
            $table->integer('earthquake_vulnerabilities')->nullable();
            $table->integer('earthquake_risk_score_one')->nullable();
            $table->integer('earthquake_risk_score_two')->nullable();
            $table->integer('landslide_hazards')->nullable();
            $table->integer('landslide_vulnerabilities')->nullable();
            $table->integer('landslide_risk_score_one')->nullable();
            $table->integer('landslide_risk_score_two')->nullable();
            $table->integer('wildlife_attack_hazards')->nullable();
            $table->integer('wildlife_attack_vulnerabilities')->nullable();
            $table->integer('wildlife_attack_risk_score_one')->nullable();
            $table->integer('wildlife_attack_risk_score_two')->nullable();
            $table->integer('fire_hazards')->nullable();
            $table->integer('fire_vulnerabilities')->nullable();
            $table->integer('fire_risk_score_one')->nullable();
            $table->integer('fire_risk_score_two')->nullable();
            $table->integer('pest_diseases_hazards')->nullable();
            $table->integer('pest_diseases_vulnerabilities')->nullable();
            $table->integer('pest_diseases_risk_score_one')->nullable();
            $table->integer('pest_diseases_risk_score_two')->nullable();
            $table->integer('flood_hazards')->nullable();
            $table->integer('flood_vulnerabilities')->nullable();
            $table->integer('flood_risk_score_one')->nullable();
            $table->integer('flood_risk_score_two')->nullable();
            $table->integer('windstorm_hazards')->nullable();
            $table->integer('windstorm_vulnerabilities')->nullable();
            $table->integer('windstorm_risk_score_one')->nullable();
            $table->integer('windstorm_risk_score_two')->nullable();
            $table->integer('drought_hazards')->nullable();
            $table->integer('drought_vulnerabilities')->nullable();
            $table->integer('drought_risk_score_one')->nullable();
            $table->integer('drought_risk_score_two')->nullable();
            $table->integer('hailstorm_hazards')->nullable();
            $table->integer('hailstorm_vulnerabilities')->nullable();
            $table->integer('hailstorm_risk_score_one')->nullable();
            $table->integer('hailstorm_risk_score_two')->nullable();
            $table->integer('soil_erosion_hazards')->nullable();
            $table->integer('soil_erosion_vulnerabilities')->nullable();
            $table->integer('soil_erosion_risk_score_one')->nullable();
            $table->integer('soil_erosion_risk_score_two')->nullable();
            $table->integer('epidemics_hazards')->nullable();
            $table->integer('epidemics_vulnerabilities')->nullable();
            $table->integer('epidemics_risk_score_one')->nullable();
            $table->integer('epidemics_risk_score_two')->nullable();

            $table->softDeletes();
            $table->timestamps();
            $table->unique('school_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('schools');
    }

}
