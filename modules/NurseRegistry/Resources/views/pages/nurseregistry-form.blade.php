
{!! Form::open(array('url'=>'nurseregistry/save')) !!}


{!! Form::hidden('nurse_id',isset($nurse->nurse_id) ? $nurse->nurse_id : NULL) !!}
<div class="box-body">
	<fieldset>
		<legend> Nurse Information </legend>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Registration No. </label>
			<div class="col-sm-4">
				{!! Form::text('nan_number',isset($nurse->nan_number) ? $nurse->nan_number : NULL,['class'=>'form-control','placeholder'=>'Nurse Registration No.']) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> First Name </label>
			<div class="col-sm-2">
				{!! Form::text('first_name',isset($nurse->first_name) ? $nurse->first_name : NULL,['class'=>'form-control']) !!}
			</div>
			<label class="col-sm-2 control-label"> Middle Name </label>
			<div class="col-sm-2">
				{!! Form::text('middle_name',isset($nurse->middle_name) ? $nurse->middle_name : NULL,['class'=>'form-control']) !!}
			</div>
			<label class="col-sm-2 control-label"> Last Name </label>
			<div class="col-sm-2">
				{!! Form::text('last_name',isset($nurse->last_name) ? $nurse->last_name : NULL,['class'=>'form-control']) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Birthdate (BS) </label>
			<div class="col-sm-4">
				{!! Form::text('date_of_birth_in_bs',isset($nurse->date_of_birth_in_bs) ? $nurse->date_of_birth_in_bs : NULL,['class'=>'form-control','placeholder'=>'MM/DD/YYYY']) !!}
			</div>
			<label class="col-sm-2 control-label"> Birthdate (AD) </label>
			<div class="col-sm-4">
				{!! Form::text('date_of_birth_in_ad',isset($nurse->date_of_birth_in_ad) ? $nurse->date_of_birth_in_ad : NULL,['class'=>'form-control','placeholder'=>'MM/DD/YYYY']) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Age </label>
			<div class="col-sm-4">
				{!! Form::number('age',isset($nurse->age) ? $nurse->age : NULL,['class'=>'form-control']) !!}
			</div>
			<label class="col-sm-2 control-label"> Sex </label>
			<div class="col-sm-4">
				<div class="icheck">
					<label class="radio-inline">
						{!! Form::radio('sex',0, (isset($nurse->sex) AND $nurse->sex == 0) ? true : false) !!} Male
					</label>
					<label class="radio-inline">
						{!! Form::radio('sex',1, (isset($nurse->sex) AND $nurse->sex == 1) ? true : false) !!} Female
					</label>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Citizenship No </label>
			<div class="col-sm-4">
				{!! Form::text('citizenship_number',isset($nurse->citizenship_number) ? $nurse->citizenship_number : NULL,['class'=>'form-control']) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Permanent Address </label>
		</div>
		<div class="col-sm-12">
			<div class="col-sm-4">
				{!! Form::text('temp_house_number',isset($nurse->temp_house_number) ? $nurse->temp_house_number : NULL,['class'=>'form-control','placeholder'=>'House No.']) !!}
			</div>
			<div class="col-sm-4">
				{!! Form::text('temp_ward_number',isset($nurse->temp_ward_number) ? $nurse->temp_ward_number : NULL,['class'=>'form-control','placeholder'=>'Ward No.']) !!}
			</div>
			<div class="col-sm-4">
				{!! Form::text('temp_municipality',isset($nurse->temp_municipality) ? $nurse->temp_municipality : NULL,['class'=>'form-control','placeholder'=>'Municipality']) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<div class="col-sm-4">
				{!! Form::text('temp_district',isset($nurse->temp_district) ? $nurse->temp_district : NULL,['class'=>'form-control','placeholder'=>'District']) !!}
			</div>
			<div class="col-sm-4">
				{!! Form::text('temp_country',isset($nurse->temp_country) ? $nurse->temp_country : NULL,['class'=>'form-control','placeholder'=>'Country']) !!}
			</div>
			<div class="col-sm-4">
				{!! Form::text('temp_zipcode',isset($nurse->temp_zipcode) ? $nurse->temp_zipcode : NULL,['class'=>'form-control','placeholder'=>'Zip Code']) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Temporary Address </label>
		</div>
		<div class="col-sm-12">
			<div class="col-sm-4">
				{!! Form::text('permanent_house_number',isset($nurse->permanent_house_number) ? $nurse->permanent_house_number : NULL,['class'=>'form-control','placeholder'=>'House No.']) !!}
			</div>
			<div class="col-sm-4">
				{!! Form::text('permanent_ward_number',isset($nurse->permanent_ward_number) ? $nurse->permanent_ward_number : NULL,['class'=>'form-control','placeholder'=>'Ward No.']) !!}
			</div>
			<div class="col-sm-4">
				{!! Form::text('permanent_municipality',isset($nurse->permanent_municipality) ? $nurse->permanent_municipality : NULL,['class'=>'form-control','placeholder'=>'Municipality']) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<div class="col-sm-4">
				{!! Form::text('permanent_district',isset($nurse->permanent_district) ? $nurse->permanent_district : NULL,['class'=>'form-control','placeholder'=>'District']) !!}
			</div>
			<div class="col-sm-4">
				{!! Form::text('permanent_country',isset($nurse->permanent_country) ? $nurse->permanent_country : NULL,['class'=>'form-control','placeholder'=>'Country']) !!}
			</div>
			<div class="col-sm-4">
				{!! Form::text('permanent_zipcode',isset($nurse->permanent_zipcode) ? $nurse->permanent_zipcode : NULL,['class'=>'form-control','placeholder'=>'Zip Code']) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Professional Status </label>
			<div class="col-sm-4">
				{!! Form::text('professional_status',isset($nurse->professional_status) ? $nurse->professional_status : NULL,['class'=>'form-control']) !!}
			</div>
		</div>
		<div class="col-sm-12">
			<label class="col-sm-2 control-label"> Designation </label>
			<div class="col-sm-4">
				{!! Form::text('designation',isset($nurse->designation) ? $nurse->designation : NULL,['class'=>'form-control']) !!}
			</div>
			<label class="col-sm-2 control-label"> Office </label>
			<div class="col-sm-4">
				{!! Form::text('office',isset($nurse->office) ? $nurse->office : NULL,['class'=>'form-control']) !!}
			</div>
		</div>
		<legend> Professional Qualification </legend>
 			<div class="col-sm-12">
 				<?php 
 					$qualification_saved = isset($nurse->qualifications) ? json_decode($nurse->qualifications,TRUE) : array();

 					$qualifications_degree = array(); 
 					$qualifications_university = array(); 
 					$qualifications_date = array();
 					if(count($qualification_saved) > 0)
 					{
	 					foreach ($qualification_saved as $value) 
	 					{
	 						$qualifications_degree[] = $value['degree'];
	 						$qualifications_university[] = $value['university'];
	 						$qualifications_date[] = $value['date'];
	 					}
 					}

 					$qualifications_display = array();
 					$qualifications_display['degree'] = $qualifications_degree;
 					$qualifications_display['university'] = $qualifications_university;
 					$qualifications_display['date'] = $qualifications_date;

 				?>
				<div class="table-responsive">
				  <table class="table">
				    <thead>
				    	<tr>
				    		<th><label class="control-label">Degree/Diploma/Certificate/Fellowship</label></th>
				    		<th><label class="control-label">University/Institution</label></th>
				    		<th><label class="control-label">Period of training date </label></th>
				    		<th></th>
				    	</tr>
				    </thead>
				    <tbody>
				    	@if(count($qualifications_display['degree']) == 0)
				    		<tr class="quali-row">
				    			<td>{!! Form::text('qualifications[degree][]',NULL,['class'=>'form-control']) !!}</td>
				    			<td>{!! Form::text('qualifications[university][]',NULL,['class'=>'form-control']) !!}</td>
				    			<td>{!! Form::text('qualifications[date][]',NULL,['class'=>'form-control']) !!}</td>
				    			<td><button type="button" class="btn btn-sm btn-warning delete-quali-btn"><i class="fa fa-close"></i> Remove </button></td>
				    		</tr>
			    		@else
			    			@for($i = 0; $i < count($qualifications_display['degree']); $i++)
					    		<tr class="quali-row">
					    			<td>{!! Form::text('qualifications[degree][]',isset($qualifications_display['degree'][$i]) ? $qualifications_display['degree'][$i] : NULL,['class'=>'form-control']) !!}</td>
					    			<td>{!! Form::text('qualifications[university][]',isset($qualifications_display['university'][$i]) ? $qualifications_display['university'][$i] : NULL,['class'=>'form-control']) !!}</td>
					    			<td>{!! Form::text('qualifications[date][]',isset($qualifications_display['date'][$i]) ? $qualifications_display['date'][$i] : NULL,['class'=>'form-control']) !!}</td>
					    			<td><button type="button" class="btn btn-sm btn-warning delete-quali-btn"><i class="fa fa-close"></i> Remove </button></td>
					    		</tr>
					    	@endfor
			    		@endif			    			
				    </tbody>
				  </table>
				</div>
				<div class="col-sm-12">
					<div class="pull-right">
						<button type="button" class="btn btn-success" id="add-quali-btn" ><i class="fa fa-plus"></i> Add Qualifications</button>
					</div>
				</div>
 			</div>
		<legend> Special Trainings related with Nursing </legend>
 			<div class="col-sm-12">
 				<?php 
 					$training_saved = isset($nurse->trainings) ? json_decode($nurse->trainings,TRUE) : array();

 					$trainings_name = array(); 
 					$trainings_organized_by = array(); 
 					$trainings_date = array();
 					if(count($training_saved) > 0)
 					{
	 					foreach ($training_saved as $value) 
	 					{
	 						$trainings_name[] = $value['name'];
	 						$trainings_organized_by[] = $value['organized_by'];
	 						$trainings_date[] = $value['date'];
	 					}
 					}

 					$trainings_display = array();
 					$trainings_display['name'] = $trainings_name;
 					$trainings_display['organized_by'] = $trainings_organized_by;
 					$trainings_display['date'] = $trainings_date;

 				?>
				<div class="table-responsive">
				  <table class="table">
				    <thead>
				    	<tr>
				    		<th><label class="control-label">Name of the training</label></th>
				    		<th><label class="control-label">Organized by</label></th>
				    		<th><label class="control-label">Period of training date </label></th>
				    		<th></th>
				    	</tr>
				    </thead>
				    <tbody>
				    	@if(count($trainings_display['name']) == 0)
				    		<tr class="training-row">
				    			<td>{!! Form::text('trainings[name][]',NULL,['class'=>'form-control']) !!}</td>
				    			<td>{!! Form::text('trainings[organized_by][]',NULL,['class'=>'form-control']) !!}</td>
				    			<td>{!! Form::text('trainings[date][]',NULL,['class'=>'form-control']) !!}</td>
				    			<td><button type="button" class="btn btn-sm btn-warning delete-training-btn"><i class="fa fa-close"></i> Remove </button></td>
				    		</tr>
			    		@else
			    			@for($i = 0; $i < count($trainings_display['name']); $i++)
					    		<tr class="training-row">
					    			<td>{!! Form::text('trainings[name][]',isset($trainings_display['name'][$i]) ? $trainings_display['name'][$i] : NULL,['class'=>'form-control']) !!}</td>
					    			<td>{!! Form::text('trainings[organized_by][]',isset($trainings_display['organized_by'][$i]) ? $trainings_display['organized_by'][$i] : NULL,['class'=>'form-control']) !!}</td>
					    			<td>{!! Form::text('trainings[date][]',isset($trainings_display['date'][$i]) ? $trainings_display['date'][$i] : NULL,['class'=>'form-control']) !!}</td>
					    			<td><button type="button" class="btn btn-sm btn-warning delete-training-btn"><i class="fa fa-close"></i> Remove </button></td>
					    		</tr>
					    	@endfor
			    		@endif			    			
				    </tbody>
				  </table>
				</div>
				<div class="col-sm-12">
					<div class="pull-right">
						<button type="button" class="btn btn-success" id="add-training-btn" ><i class="fa fa-plus"></i> Add Trainings</button>
					</div>
				</div>
 			</div>
	</fieldset>
</div>

<div class="mainbuttons col-md-12 textcenter">
    <button type="submit" class="btn btn-success">Save Nurse Record</button>
    <a type="button" class="btn btn-default" href='{{ url("/nurseregistry") }}'>Close</a>
</div>

{!! Form::close() !!}