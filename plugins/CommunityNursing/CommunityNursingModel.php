<?php
namespace Plugins\CommunityNursing;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Shine\Libraries\IdGenerator;
use Webpatser\Uuid\Uuid;
use DB;

class CommunityNursingModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'communitynursing_service';
    protected static $table_name = 'communitynursing_service';
    protected $primaryKey = 'communitynursing_id';
    protected $touches = array('Healthcareservices');

    public function Healthcareservices() {
        return $this->belongsTo('ShineOS\Core\Healthcareservices\Entities\Healthcareservices', 'healthcareservice_id', 'healthcareservice_id');
    }

    public static function submit($data,$hid) {

        $communitynursing = self::where('healthcareservice_id',$hid)->first();

        if(!$communitynursing) {
            $communitynursing =  new self;
            $communitynursing->communitynursing_id = IdGenerator::generateId();
            $communitynursing->healthcareservice_id = $hid;
            $communitynursing->uuid = Uuid::generate(4)->string;
            $communitynursing->owner_uuid = isset($data['owner_uuid']) ? $data['owner_uuid'] : NULL;
        }

        $communitynursing->disaster_recently = isset($data['disaster_recently']) ? $data['disaster_recently'] : NULL;
        $communitynursing->disaster_type = isset($data['disaster_type']) ? $data['disaster_type'] : NULL;

        $communitynursing->number_human_loss = isset($data['number_human_loss']) && $data['number_human_loss'] != '' ? $data['number_human_loss'] : NULL;
        $communitynursing->number_death = isset($data['number_death']) && $data['number_death'] != '' ? $data['number_death'] : NULL;
        $communitynursing->number_missing_people = isset($data['number_missing_people']) && $data['number_missing_people'] != '' ? $data['number_missing_people'] : NULL;
        $communitynursing->number_injured_people = isset($data['number_injured_people']) && $data['number_injured_people'] != '' ? $data['number_injured_people'] : NULL;
        $communitynursing->number_livestock_loss = isset($data['number_livestock_loss']) && $data['number_livestock_loss'] != '' ? $data['number_livestock_loss'] : NULL;


        $communitynursing->disaster_duration = isset($data['disaster_duration']) ? $data['disaster_duration'] : NULL;

        $communitynursing->when_wash_hands_after_toileting = isset($data['when_wash_hands_after_toileting']) ? $data['when_wash_hands_after_toileting'] : NULL;
        $communitynursing->when_wash_hands_before_cooking = isset($data['when_wash_hands_before_cooking']) ? $data['when_wash_hands_before_cooking'] : NULL;
        $communitynursing->when_wash_hands_before_eating = isset($data['when_wash_hands_before_eating']) ? $data['when_wash_hands_before_eating'] : NULL;
        $communitynursing->when_wash_hands_before_feeding_a_child = isset($data['when_wash_hands_before_feeding_a_child']) ? $data['when_wash_hands_before_feeding_a_child'] : NULL;
        $communitynursing->when_wash_hands_others = isset($data['when_wash_hands_others']) ? $data['when_wash_hands_others'] : NULL;
        $communitynursing->when_wash_hands_specify = isset($data['when_wash_hands_specify']) ? $data['when_wash_hands_specify'] : NULL;

        $communitynursing->bathing_habit = isset($data['bathing_habit']) && $data['bathing_habit'] != '' ? $data['bathing_habit'] : NULL;
        $communitynursing->src_water_supply = isset($data['src_water_supply']) && $data['src_water_supply'] != '' ? $data['src_water_supply'] : NULL;
        $communitynursing->src_water_supply_spec = isset($data['src_water_supply_spec']) ? $data['src_water_supply_spec'] : NULL;
        $communitynursing->ade_water_supply = isset($data['ade_water_supply']) ? $data['ade_water_supply'] : NULL;
        $communitynursing->quality_water = isset($data['quality_water']) ? $data['quality_water'] : NULL;
        $communitynursing->waterpurif_boiling = isset($data['waterpurif_boiling']) ? $data['waterpurif_boiling'] : NULL;
        $communitynursing->waterpurif_filtration = isset($data['waterpurif_filtration']) ? $data['waterpurif_filtration'] : NULL;
        $communitynursing->waterpurif_chemical = isset($data['waterpurif_chemical']) ? $data['waterpurif_chemical'] : NULL;
        $communitynursing->waterpurif_sodis = isset($data['waterpurif_sodis']) ? $data['waterpurif_sodis'] : NULL;
        $communitynursing->waterpurif_none = isset($data['waterpurif_none']) ? $data['waterpurif_none'] : NULL;
        $communitynursing->waterpurif_oth = isset($data['waterpurif_oth']) ? $data['waterpurif_oth'] : NULL;
        $communitynursing->waterpurif_oth_spec = isset($data['waterpurif_oth_spec']) ? $data['waterpurif_oth_spec'] : NULL;
        $communitynursing->distance_water_supply = isset($data['distance_water_supply']) && $data['distance_water_supply'] != '' ? $data['distance_water_supply'] : NULL;
        $communitynursing->toilet_available = isset($data['toilet_available']) ? $data['toilet_available'] : NULL;
        $communitynursing->num_toilet = isset($data['num_toilet']) ? $data['num_toilet'] : NULL;
        $communitynursing->type_toilet = isset($data['type_toilet']) && $data['type_toilet'] != '' ? $data['type_toilet'] : NULL;
        $communitynursing->type_toilet_specify = isset($data['type_toilet_specify']) ? $data['type_toilet_specify'] : NULL;
        $communitynursing->toilet_distance = isset($data['toilet_distance']) && $data['toilet_distance'] != '' ? $data['toilet_distance'] : NULL;
        $communitynursing->toilet_fr_water = isset($data['toilet_fr_water'])  && $data['toilet_fr_water'] != '' ? $data['toilet_fr_water'] : NULL;
        $communitynursing->toilet_fr_water_unit = isset($data['toilet_fr_water_unit']) && $data['toilet_fr_water_unit'] != '' ? $data['toilet_fr_water_unit'] : NULL;
        $communitynursing->toilet_fr_waste = isset($data['toilet_fr_waste']) && $data['toilet_fr_waste'] != '' ? $data['toilet_fr_waste'] : NULL;
        $communitynursing->toilet_fr_waste_unit = isset($data['toilet_fr_waste_unit']) && $data['toilet_fr_waste_unit'] != '' ? $data['toilet_fr_waste_unit'] : NULL;

        $communitynursing->disposal_method_composting = isset($data['disposal_method_composting']) ? $data['disposal_method_composting'] : NULL;
        $communitynursing->disposal_method_burning = isset($data['disposal_method_burning']) ? $data['disposal_method_burning'] : NULL;
        $communitynursing->disposal_method_dumping = isset($data['disposal_method_dumping']) ? $data['disposal_method_dumping'] : NULL;
        $communitynursing->disposal_method_others = isset($data['disposal_method_others']) ? $data['disposal_method_others'] : NULL;
        $communitynursing->disposal_method_specify = isset($data['disposal_method_specify']) ? $data['disposal_method_specify'] : NULL;

        $communitynursing->waste_collectors = isset($data['waste_collectors']) ? $data['waste_collectors'] : NULL;
        $communitynursing->timely_collection_waste = isset($data['timely_collection_waste']) ? $data['timely_collection_waste'] : NULL;
        $communitynursing->dead_animal_disposal = isset($data['dead_animal_disposal']) ? $data['dead_animal_disposal'] : NULL;
        $communitynursing->drainage_facility = isset($data['drainage_facility']) ? $data['drainage_facility'] : NULL;
        $communitynursing->surr_area_clean = isset($data['surr_area_clean']) ? $data['surr_area_clean'] : NULL;
        $communitynursing->prob_hygiene_mens = isset($data['prob_hygiene_mens']) ? $data['prob_hygiene_mens'] : NULL;
        $communitynursing->prob_hygiene_reas_watersupply = isset($data['prob_hygiene_reas_watersupply']) ? $data['prob_hygiene_reas_watersupply'] : NULL;
        $communitynursing->prob_hygiene_reas_privacy = isset($data['prob_hygiene_reas_privacy']) ? $data['prob_hygiene_reas_privacy'] : NULL;
        $communitynursing->prob_hygiene_reas_pads = isset($data['prob_hygiene_reas_pads']) ? $data['prob_hygiene_reas_pads'] : NULL;
        $communitynursing->prob_hygiene_reas_padsdisposal = isset($data['prob_hygiene_reas_padsdisposal']) ? $data['prob_hygiene_reas_padsdisposal'] : NULL;
        $communitynursing->prob_hygiene_reas_oth = isset($data['prob_hygiene_reas_oth']) ? $data['prob_hygiene_reas_oth'] : NULL;
        $communitynursing->prob_hygiene_reas_oth_spec = isset($data['prob_hygiene_reas_oth_spec']) ? $data['prob_hygiene_reas_oth_spec'] : NULL;
        $communitynursing->visible_hazards_none = isset($data['visible_hazards_none']) ? $data['visible_hazards_none'] : NULL;
        $communitynursing->visible_hazards_floor = isset($data['visible_hazards_floor']) ? $data['visible_hazards_floor'] : NULL;
        $communitynursing->visible_hazards_light = isset($data['visible_hazards_light']) ? $data['visible_hazards_light'] : NULL;
        $communitynursing->visible_hazards_electricity = isset($data['visible_hazards_electricity']) ? $data['visible_hazards_electricity'] : NULL;
        $communitynursing->visible_hazards_surface = isset($data['visible_hazards_surface']) ? $data['visible_hazards_surface'] : NULL;
        $communitynursing->visible_hazards_land = isset($data['visible_hazards_land']) ? $data['visible_hazards_land'] : NULL;
        $communitynursing->visible_hazards_dumparea = isset($data['visible_hazards_dumparea']) ? $data['visible_hazards_dumparea'] : NULL;
        $communitynursing->visible_hazards_pollution = isset($data['visible_hazards_pollution']) ? $data['visible_hazards_pollution'] : NULL;
        $communitynursing->visible_hazards_ventilation = isset($data['visible_hazards_ventilation']) ? $data['visible_hazards_ventilation'] : NULL;
        $communitynursing->visible_hazards_noise = isset($data['visible_hazards_noise']) ? $data['visible_hazards_noise'] : NULL;
        $communitynursing->visible_hazards_noise = isset($data['visible_hazards_noise']) ? $data['visible_hazards_noise'] : NULL;
        $communitynursing->visible_hazards_noise = isset($data['visible_hazards_noise']) ? $data['visible_hazards_noise'] : NULL;
        $communitynursing->visible_hazards_oth = isset($data['visible_hazards_oth']) ? $data['visible_hazards_oth'] : NULL;
        $communitynursing->visible_hazards_oth_spec = isset($data['visible_hazards_oth_spec']) ? $data['visible_hazards_oth_spec'] : NULL;
        $communitynursing->area_prone_to_none = isset($data['area_prone_to_none']) ? $data['area_prone_to_none'] : NULL;
        $communitynursing->bite_prone_snake = isset($data['bite_prone_snake']) ? $data['bite_prone_snake'] : NULL;
        $communitynursing->bite_prone_animal = isset($data['bite_prone_animal']) ? $data['bite_prone_animal'] : NULL;
        $communitynursing->bite_prone_animal_spec = isset($data['bite_prone_animal_spec']) ? $data['bite_prone_animal_spec'] : NULL;
        $communitynursing->bite_prone_insect = isset($data['bite_prone_insect']) ? $data['bite_prone_insect'] : NULL;
        $communitynursing->bite_prone_insect_spec = isset($data['bite_prone_insect_spec']) ? $data['bite_prone_insect_spec'] : NULL;
        $communitynursing->bite_prone_oth = isset($data['bite_prone_oth']) ? $data['bite_prone_oth'] : NULL;
        $communitynursing->bite_prone_oth_spec = isset($data['bite_prone_oth_spec']) ? $data['bite_prone_oth_spec'] : NULL;
        $communitynursing->free_vectors = isset($data['free_vectors']) ? $data['free_vectors'] : NULL;
        $communitynursing->mosqprev_measure_net = isset($data['mosqprev_measure_net']) ? $data['mosqprev_measure_net'] : NULL;
        $communitynursing->mosqprev_measure_liquid = isset($data['mosqprev_measure_liquid']) ? $data['mosqprev_measure_liquid'] : NULL;
        $communitynursing->mosqprev_measure_drug = isset($data['mosqprev_measure_drug']) ? $data['mosqprev_measure_drug'] : NULL;
        $communitynursing->mosqprev_measure_oth = isset($data['mosqprev_measure_oth']) ? $data['mosqprev_measure_oth'] : NULL;
        $communitynursing->mosqprev_measure_oth_spec = isset($data['mosqprev_measure_oth_spec']) ? $data['mosqprev_measure_oth_spec'] : NULL;
        $communitynursing->periodic_pest_control = isset($data['periodic_pest_control']) ? $data['periodic_pest_control'] : NULL;
        $communitynursing->housing = isset($data['housing']) && $data['housing'] != '' ? $data['housing'] : NULL;
        $communitynursing->housing_oth = isset($data['housing_oth']) ? $data['housing_oth'] : NULL;
        $communitynursing->space_adequacy = isset($data['space_adequacy']) ? $data['space_adequacy'] : NULL;
        $communitynursing->house_damage = isset($data['house_damage']) ? $data['house_damage'] : NULL;
        $communitynursing->source_of_light_electricity = isset($data['source_of_light_electricity']) ? $data['source_of_light_electricity'] : NULL;
        $communitynursing->source_of_light_oil_lamp = isset($data['source_of_light_oil_lamp']) ? $data['source_of_light_oil_lamp'] : NULL;
        $communitynursing->source_of_light_solar = isset($data['source_of_light_solar']) ? $data['source_of_light_solar'] : NULL;
        $communitynursing->source_of_light_others = isset($data['source_of_light_others']) ? $data['source_of_light_others'] : NULL;
        $communitynursing->source_of_light_specify = isset($data['source_of_light_specify']) ? $data['source_of_light_specify'] : NULL;
        $communitynursing->ventilation_adequacy = isset($data['ventilation_adequacy']) ? $data['ventilation_adequacy'] : NULL;
        $communitynursing->kitchen = isset($data['kitchen']) ? $data['kitchen'] : NULL;
        $communitynursing->foodstock_avail = isset($data['foodstock_avail']) ? $data['foodstock_avail'] : NULL;

        $communitynursing->cooking_fuel_used_gas = isset($data['cooking_fuel_used_gas']) ? $data['cooking_fuel_used_gas'] : NULL;
        $communitynursing->cooking_fuel_used_firewood = isset($data['cooking_fuel_used_firewood']) ? $data['cooking_fuel_used_firewood'] : NULL;
        $communitynursing->cooking_fuel_used_kerosene = isset($data['cooking_fuel_used_kerosene']) ? $data['cooking_fuel_used_kerosene'] : NULL;
        $communitynursing->cooking_fuel_used_others = isset($data['cooking_fuel_used_others']) ? $data['cooking_fuel_used_others'] : NULL;
        $communitynursing->cooking_fuel_used_specify = isset($data['cooking_fuel_used_specify']) ? $data['cooking_fuel_used_specify'] : NULL;
        $communitynursing->kind_of_food_available_cooked = isset($data['kind_of_food_available_cooked']) ? $data['kind_of_food_available_cooked'] : NULL;
        $communitynursing->kind_of_food_available_junk = isset($data['kind_of_food_available_junk']) ? $data['kind_of_food_available_junk'] : NULL;
        $communitynursing->kind_of_food_available_others = isset($data['kind_of_food_available_others']) ? $data['kind_of_food_available_others'] : NULL;
        $communitynursing->kind_of_food_available_specify = isset($data['kind_of_food_available_specify']) ? $data['kind_of_food_available_specify'] : NULL;

        $communitynursing->safe_food = isset($data['safe_food']) ? $data['safe_food'] : NULL;
        $communitynursing->food_storage = isset($data['food_storage']) ? $data['food_storage'] : NULL;
        $communitynursing->disease_outbreak = isset($data['disease_outbreak']) ? $data['disease_outbreak'] : NULL;
        $communitynursing->disease_outbreak_spec = isset($data['disease_outbreak_spec']) ? $data['disease_outbreak_spec'] : NULL;
        $communitynursing->hs_onsite = isset($data['hs_onsite']) ? $data['hs_onsite'] : NULL;
        $communitynursing->pfa = isset($data['pfa']) ? $data['pfa'] : NULL;
        $communitynursing->distance_faci = isset($data['distance_faci']) ? $data['distance_faci'] : NULL;

        $communitynursing->type_of_nearest_facility_health_post = isset($data['type_of_nearest_facility_health_post']) ? $data['type_of_nearest_facility_health_post'] : NULL;
        $communitynursing->type_of_nearest_facility_primary_health_center = isset($data['type_of_nearest_facility_primary_health_center']) ? $data['type_of_nearest_facility_primary_health_center'] : NULL;
        $communitynursing->type_of_nearest_facility_district_hospital = isset($data['type_of_nearest_facility_district_hospital']) ? $data['type_of_nearest_facility_district_hospital'] : NULL;
        $communitynursing->type_of_nearest_facility_zonal_hospital = isset($data['type_of_nearest_facility_zonal_hospital']) ? $data['type_of_nearest_facility_zonal_hospital'] : NULL;
        $communitynursing->type_of_nearest_facility_sub_regional_hospital = isset($data['type_of_nearest_facility_sub_regional_hospital']) ? $data['type_of_nearest_facility_sub_regional_hospital'] : NULL;
        $communitynursing->type_of_nearest_facility_tertiary_hospital = isset($data['type_of_nearest_facility_tertiary_hospital']) ? $data['type_of_nearest_facility_tertiary_hospital'] : NULL;
        $communitynursing->type_of_nearest_facility_private_hospital = isset($data['type_of_nearest_facility_private_hospital']) ? $data['type_of_nearest_facility_private_hospital'] : NULL;
        $communitynursing->type_of_nearest_facility_others = isset($data['type_of_nearest_facility_others']) ? $data['type_of_nearest_facility_others'] : NULL;
        $communitynursing->type_of_nearest_facility_specify = isset($data['type_of_nearest_facility_specify']) ? $data['type_of_nearest_facility_specify'] : NULL;

        $communitynursing->family_illness_none = isset($data['family_illness_none']) ? $data['family_illness_none'] : NULL;
        $communitynursing->family_illness_diabetes = isset($data['family_illness_diabetes']) ? $data['family_illness_diabetes'] : NULL;
        $communitynursing->family_illness_hypertension = isset($data['family_illness_hypertension']) ? $data['family_illness_hypertension'] : NULL;
        $communitynursing->family_illness_copd = isset($data['family_illness_copd']) ? $data['family_illness_copd'] : NULL;
        $communitynursing->family_illness_cancer = isset($data['family_illness_cancer']) ? $data['family_illness_cancer'] : NULL;
        $communitynursing->family_illness_mental = isset($data['family_illness_mental']) ? $data['family_illness_mental'] : NULL;
        $communitynursing->family_illness_oth = isset($data['family_illness_oth']) ? $data['family_illness_oth'] : NULL;
        $communitynursing->family_illness_oth_spec = isset($data['family_illness_oth_spec']) ? $data['family_illness_oth_spec'] : NULL;
        $communitynursing->fp_use = isset($data['fp_use']) ? $data['fp_use'] : NULL;
        $communitynursing->fp_method_used = isset($data['fp_method_used']) ? $data['fp_method_used'] : NULL;
        $communitynursing->fp_temp_awareness = isset($data['fp_temp_awareness']) ? $data['fp_temp_awareness'] : NULL;
        $communitynursing->fp_awareness_security = isset($data['fp_awareness_security']) ? $data['fp_awareness_security'] : NULL;
        $communitynursing->fp_awareness_ecp = isset($data['fp_awareness_ecp']) ? $data['fp_awareness_ecp'] : NULL;

        $communitynursing->where_ecp_available_mobile_clinic = isset($data['where_ecp_available_mobile_clinic']) ? $data['where_ecp_available_mobile_clinic']: NULL;
        $communitynursing->where_ecp_available_health_facility = isset($data['where_ecp_available_health_facility']) ? $data['where_ecp_available_health_facility']: NULL;
        $communitynursing->where_ecp_available_others = isset($data['where_ecp_available_others']) ? $data['where_ecp_available_others']: NULL;
        $communitynursing->where_ecp_available_specify = isset($data['where_ecp_available_specify']) ? $data['where_ecp_available_specify']: NULL;

        $communitynursing->family_mental_illness = isset($data['family_mental_illness']) ? $data['family_mental_illness'] : NULL;
        $communitynursing->medication = isset($data['medication']) ? $data['medication'] : NULL;

        $communitynursing->health_hazards_none = isset($data['mental_hazard_none']) ? $data['mental_hazard_none'] : NULL;
        $communitynursing->mental_hazard_smoking = isset($data['mental_hazard_smoking']) ? $data['mental_hazard_smoking'] : NULL;
        $communitynursing->mental_hazard_alcoholism = isset($data['mental_hazard_alcoholism']) ? $data['mental_hazard_alcoholism'] : NULL;
        $communitynursing->mental_hazard_drug = isset($data['mental_hazard_drug']) ? $data['mental_hazard_drug'] : NULL;
        $communitynursing->mental_hazard_oth = isset($data['mental_hazard_oth']) ? $data['mental_hazard_oth'] : NULL;
        $communitynursing->mental_hazard_oth_spec = isset($data['mental_hazard_oth_spec']) ? $data['mental_hazard_oth_spec'] : NULL;
        $communitynursing->mental_abuse_none = isset($data['mental_abuse_none']) ? $data['mental_abuse_none'] : NULL;
        $communitynursing->mental_abuse_sexual = isset($data['mental_abuse_sexual']) ? $data['mental_abuse_sexual'] : NULL;
        $communitynursing->mental_abuse_physical = isset($data['mental_abuse_physical']) ? $data['mental_abuse_physical'] : NULL;
        $communitynursing->mental_abuse_psych = isset($data['mental_abuse_psych']) ? $data['mental_abuse_psych'] : NULL;
        $communitynursing->mental_abuse_oth = isset($data['mental_abuse_oth']) ? $data['mental_abuse_oth'] : NULL;
        $communitynursing->mental_abuse_oth_spec = isset($data['mental_abuse_oth_spec']) ? $data['mental_abuse_oth_spec'] : NULL;

        if($communitynursing->save())
        {
            return $communitynursing;
        }
        return false;
    }

}
