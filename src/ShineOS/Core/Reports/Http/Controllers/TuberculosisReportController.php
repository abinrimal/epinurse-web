<?php

namespace ShineOS\Core\Reports\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Session,DateTime,DB;

class TuberculosisReportController extends Controller {

    public function __construct() {
        $this->TuberculosisDssmModel = 'Plugins\Tuberculosis\TuberculosisDssmModel';
        $this->TuberculosismModel = 'Plugins\Tuberculosis\TuberculosisModel';

    }

    public static function TB_query($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        $_this = new self();
        $facility_id = Session::get('facility_details')->facility_id;
        $file_1 = plugins_path() . 'Tuberculosis' . DS . 'TuberculosisModel.php';
        $file_2 = plugins_path() . 'Tuberculosis' . DS . 'TuberculosisDssmModel.php';
        if(file_exists($file_1)) {
            if(file_exists($file_2)) {
                $tb = $_this->TuberculosismModel::withAndWhereHas('Healthcareservices',function($query) use ($facility_id,$barangay) {
                            $query->withAndWhereHas('FacilityPatientUser',function($query) use ($facility_id,$barangay) {
                                $query->withAndWhereHas('patients', function($query) use ($barangay) {
                                    if($barangay!=NULL) {
                                        $query->withAndWhereHas('patientContact',function($query) use ($barangay) {
                                            $query->whereIn('barangay', (array)$barangay);
                                        });
                                    } else {
                                        $query->with('patientContact');
                                    }
                                });

                                $query->withAndWhereHas('facilityUser',function($query) use ($facility_id) {
                                    $query->withAndWhereHas('facilities',function($query) use ($facility_id) {
                                        $query->where('facility_id', $facility_id);
                                    });
                                });
                                
                            });
                        });
                if($year!=NULL){ $tb = $tb->whereYear('created_at','=',$year); }
                else { $year = date('Y'); $tb = $tb->whereYear('created_at','=',$year); }
                $month = implode("','",(array)$month);
                if($month!=NULL){ $tb = $tb->whereRaw(DB::raw("month(`created_at`) IN ('".$month."')")); }

                $tb = $tb->with('dssm')->get()->toArray();
                $data = $_this->query_format($tb);
            }
        }
        return $data;
    }

    public static function query_format($tb){
        $_this = new self();
        $arrTB = array('TBSYMP'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'SMEARPOS'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'NEWSMEAR'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'NWSMEARPOS'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'SMEARET'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'SMEARET_CURED'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'SMEARET_REL'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'SMEARET_TXF'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'SMEARET_RAD'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'SMEARET_OTB'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'TOT_TB'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'SMEARET_CURED_REL'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'SMEARET_CURED_TXF'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'SMEARET_CURED_RAD'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
                        'TB_ALL'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0));

        $arrRetreatment = array('R', 'TAF', 'TALF','PTOU');
        $arrRAD = array('TALF','PTOU');


        if($tb) {
            foreach ($tb as $key => $value) {
                if(count($value['dssm'])!=0) {
                    //TB symptomatics who underwent DSSM
                    $arrTB = $_this->gender_check($tb,$key,$arrTB,'TBSYMP');
                    
                    $dssm_positive = FALSE;
                    foreach ($value['dssm'] as $k => $v) {
                        if(stripos($v['result'], 'positive') !== FALSE){
                            $dssm_positive = TRUE;
                        }
                    }
                    if($dssm_positive) {
                        // Smear Positive discovered and identified
                        $arrTB = $_this->gender_check($tb,$key,$arrTB,'SMEARPOS');

                        if($value['tb_drugs_before'] == 'Y' AND $value['bacteriology_registration_group'] == 'N') {
                            //New Smear (+) cases initiated tx & registered
                            $arrTB = $_this->gender_check($tb,$key,$arrTB,'NEWSMEAR');

                            if($value['treatment_outcome_result'] == 'C') {
                                // New Smear (+) cases cured
                                $arrTB = $_this->gender_check($tb,$key,$arrTB,'NWSMEARPOS');
                            }
                        }

                        if($value['bacteriology_registration_group'] == 'R') {
                            // Smear (+) retreatment cases initiated tx & registered (relapse)
                            $arrTB = $_this->gender_check($tb,$key,$arrTB,'SMEARET_REL');
                            if($value['treatment_outcome_result'] == 'C') {
                                // No of smear (+) retreatment cured (relapse)
                                $arrTB = $_this->gender_check($tb,$key,$arrTB,'SMEARET_CURED_REL');
                            }
                        }

                        if($value['bacteriology_registration_group'] == 'OTH') {
                            // Smear (+) retreatment cases initiated tx & registered (Other type of TB)
                            $arrTB = $_this->gender_check($tb,$key,$arrTB,'SMEARET_OTB');
                        }

                        if($value['bacteriology_registration_group'] != NULL) {
                            // Total No. of TB cases (all forms) initiated treatment
                            $arrTB = $_this->gender_check($tb,$key,$arrTB,'TOT_TB');
                        }

                        if($value['bacteriology_registration_group'] == 'TAF') {
                            // Smear (+) retreatment cases initiated tx & registered (Treatment Failure)
                            $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET_TXF');
                            // No of smear (+) retreatment cured (Treatment Failure)
                            if($value['treatment_outcome_result'] == 'C') {
                                $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET_CURED_TXF');
                            }
                        }


                        if(in_array($value['bacteriology_registration_group'],$arrRetreatment)) {
                            // Smear (+) retreatment cases initiated treatment
                            $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET');
                            // Smear (+) retreatment case got cured
                            if($value['treatment_outcome_result'] == 'C') {
                                $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET_CURED');
                            }
                        }

                        if(in_array($value['bacteriology_registration_group'],$arrRAD)) {
                            // Smear (+) retreatment cases initiated tx & registered ( Return After Default)
                            $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET_RAD');
                            // No of smear (+) retreatment cured (Return After Default)
                            if($value['treatment_outcome_result'] == 'C') {
                                $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET_CURED_RAD');
                            }
                        }
                    }
                    
                }

                $arrTB = $_this->gender_check($tb,$key,$arrTB,'TB_ALL');
            }    
        }
        return $arrTB;
    }

    public function gender_check($tb,$key,$arrTB,$value) {
        if($tb[$key]['healthcareservices']['facility_patient_user']['patients']['gender']=='M') {
            $arrTB[$value]['M'] += 1;
        }
        else if($tb[$key]['healthcareservices']['facility_patient_user']['patients']['gender']=='F') {
            $arrTB[$value]['F'] += 1;
        } else {
            $arrTB[$value]['U'] += 1;
        }
        
        $arrTB[$value]['Total'] += 1;
        return $arrTB;
    }

    
    // public static function TB_query($month=NULL,$year=NULL,$barangay=NULL) {
    //     $_this = new self();
    //     $facility_id = Session::get('facility_details')->facility_id;

    //     $tb = $_this->TuberculosismModel::has('Healthcareservices')
    //             ->with(array('Healthcareservices'=>function($query) use ($facility_id,$barangay) {
    //                 $query->select('healthcareservice_id','facilitypatientuser_id','healthcareservicetype_id','consultation_type','encounter_type','encounter_datetime');
    //                 $query->has('FacilityPatientUser');
    //                 $query->with(array('FacilityPatientUser'=>function($query) use ($facility_id,$barangay) {
    //                     $query->select('facilitypatientuser_id','patient_id','facilityuser_id');
    //                     $query->with(array('patients'=>function($query) {
    //                             $query->select('patient_id','first_name','middle_name','last_name','gender','birthdate');
    //                         }));
    //                     $query->with(array('facilityUser'=>function($query) use ($facility_id,$barangay) {
    //                         $query->select('facilityuser_id','facility_id');
    //                         $query->with(array('facilities'=>function($query) use ($facility_id,$barangay) {
    //                             $query->where('facility_id', $facility_id);
    //                             $query->select('facility_id','facility_name','ownership_type','facility_type','provider_type');
    //                             if($barangay!=NULL) {
    //                                 $query->with(array('facilityContact'=>function($query) use ($facility_id,$barangay) {
    //                                     $query->where('barangay', $barangay);
    //                                     $query->select('facility_id','barangay','city','province','region');
    //                                 }));
    //                             } else {
    //                                 $query->with(array('facilityContact'=>function($query) use ($facility_id) {
    //                                     $query->select('facility_id','barangay','city','province','region');
    //                                 }));
    //                             }
    //                         }));
    //                     }));
                        
    //                 }));
    //             }));
    //     if($year!=NULL){ $tb->whereYear('updated_at','=',$year); }
    //     else { $tb->whereYear('updated_at','>=','2016'); }

    //     if($month!=NULL){ $tb->whereMonth('updated_at','=',$month); }

    //     $tb = $tb->get()->toArray();
    //     foreach ($tb as $key => $value) {
    //         if($value['healthcareservices']['facility_patient_user']['facility_user']['facilities']!=NULL) {
    //             $data[$key] = $value;
    //         }
    //     }
    //     return $data;
    // }
    // public static function TB_DSSM_query($month=NULL,$year=NULL,$barangay=NULL){
    //     $_this = new self();
    //     $facility_id = Session::get('facility_details')->facility_id;

    //     $tb_dssm = $_this->TuberculosisDssmModel::has('tuberculosis')
    //         ->with(array('tuberculosis'=>function($query) use ($facility_id,$barangay) {
    //             $query->has('Healthcareservices');
    //             $query->with(array('Healthcareservices'=>function($query) use ($facility_id,$barangay) {
    //                 $query->select('healthcareservice_id','facilitypatientuser_id','healthcareservicetype_id','consultation_type','encounter_type','encounter_datetime');
    //                 $query->has('FacilityPatientUser');
    //                 $query->with(array('FacilityPatientUser'=>function($query) use ($facility_id,$barangay) {
    //                     $query->select('facilitypatientuser_id','patient_id','facilityuser_id');
    //                     $query->with(array('patients'=>function($query) {
    //                             $query->select('patient_id','first_name','middle_name','last_name','gender','birthdate');
    //                         }));
    //                     $query->with(array('facilityUser'=>function($query) use ($facility_id,$barangay) {
    //                         $query->select('facilityuser_id','facility_id');
    //                         $query->with(array('facilities'=>function($query) use ($facility_id,$barangay) {
    //                             $query->where('facility_id', $facility_id);
    //                             $query->select('facility_id','facility_name','ownership_type','facility_type','provider_type');
    //                             if($barangay!=NULL) {
    //                                 $query->with(array('facilityContact'=>function($query) use ($facility_id,$barangay) {
    //                                     $query->where('barangay', $barangay);
    //                                     $query->select('facility_id','barangay','city','province','region');
    //                                 }));
    //                             } else {
    //                                 $query->with(array('facilityContact'=>function($query) use ($facility_id) {
    //                                     $query->select('facility_id','barangay','city','province','region');
    //                                 }));
    //                             }
    //                         }));
    //                     }));
                        
    //                 }));
    //             }));
    //         }));
        
    //     if($year!=NULL){ $tb_dssm->whereYear('updated_at','=',$year); }
    //     else { $tb_dssm->whereYear('updated_at','>=','2016'); }

    //     if($month!=NULL){ $tb_dssm->whereMonth('updated_at','=',$month); }

    //     $tb_dssm = $tb_dssm->get()->toArray();
    //     $data = $_this->TB_query_format($tb_dssm);
    //     return $data;
    // }

    // public static function TB_query_format($tb_dssm){
    //     $_this = new self();
    //     $arrTB = array('TBSYMP'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
    //                     'SMEARPOS'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
    //                     'NEWSMEAR'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
    //                     'NWSMEARPOS'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
    //                     'SMEARET'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
    //                     'SMEARET_CURED'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
    //                     'SMEARET_REL'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
    //                     'SMEARET_TXF'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
    //                     'SMEARET_RAD'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
    //                     'SMEARET_OTB'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
    //                     'TOT_TB'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
    //                     'SMEARET_CURED_REL'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
    //                     'SMEARET_CURED_TXF'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0),
    //                     'SMEARET_CURED_RAD'=>array('M'=>0,'F'=>0,'U'=>0,'Total'=>0)
    //                     );
    //     $arrRetreatment = array('R', 'TAF', 'TALF','PTOU');
    //     $arrRAD = array('TALF','PTOU');
    //     foreach ($tb_dssm as $key => $value) {
    //         if($value['tuberculosis']['healthcareservices']['facility_patient_user']['facility_user']['facilities']!=NULL) {
    //             //TB symptomatics who underwent DSSM BY GENDER
    //             $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'TBSYMP');
                
    //             if(stripos($value['result'], 'positive') !== FALSE AND $value['category_of_treatment'] != NULL) {
    //                 //Smear Positive discovered and identified
    //                 $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARPOS');
    //                 $bacteriology_registration_group = $value['tuberculosis']['bacteriology_registration_group'];

    //                 if($bacteriology_registration_group == 'N') {
    //                     //New Smear (+) cases initiated tx & registered
    //                     $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'NEWSMEAR');
    //                     // New Smear (+) cases cured
    //                     if($value['tuberculosis']['treatment_outcome_result'] == 'C') {
    //                         $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'NWSMEARPOS');
    //                     }
    //                 } else if($bacteriology_registration_group == 'R') {
    //                     // Smear (+) retreatment cases initiated tx & registered (Relapsed)
    //                     $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET_REL');
    //                     // No of smear (+) retreatment cured (Relapse)
    //                     if($value['tuberculosis']['treatment_outcome_result'] == 'C') {
    //                         $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET_CURED_REL');
    //                     }
    //                 } else if($bacteriology_registration_group == 'TAF') {
    //                     // Smear (+) retreatment cases initiated tx & registered (Treatment Failure)
    //                     $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET_TXF');
    //                     // No of smear (+) retreatment cured (Treatment Failure)
    //                     if($value['tuberculosis']['treatment_outcome_result'] == 'C') {
    //                         $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET_CURED_TXF');
    //                     }
    //                 } else if($bacteriology_registration_group == 'OTH') {
    //                     // Smear (+) retreatment cases initiated tx & registered (Other type of TB)
    //                     $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET_OTB');
    //                 } else if($bacteriology_registration_group != NULL) {
    //                     // Total No. of TB cases (all forms) initiated treatment
    //                     $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'TOT_TB');
    //                 } else {}

    //                 if(in_array($bacteriology_registration_group,$arrRetreatment)) {
    //                     // Smear (+) retreatment cases initiated treatment
    //                     $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET');
    //                     // Smear (+) retreatment case got cured
    //                     if($value['tuberculosis']['treatment_outcome_result'] == 'C') {
    //                         $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET_CURED');
    //                     }
    //                 } 
    //                 if(in_array($bacteriology_registration_group,$arrRAD)) {
    //                     // Smear (+) retreatment cases initiated tx & registered ( Return After Default)
    //                     $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET_RAD');
    //                     // No of smear (+) retreatment cured (Return After Default)
    //                     if($value['tuberculosis']['treatment_outcome_result'] == 'C') {
    //                         $arrTB = $_this->gender_check($tb_dssm,$key,$arrTB,'SMEARET_CURED_RAD');
    //                     }
    //                 }
    //             }
    //         }
    //     }
        
    //     return $arrTB;
    // }
}
