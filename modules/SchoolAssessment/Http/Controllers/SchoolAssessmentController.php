<?php namespace Modules\SchoolAssessment\Http\Controllers;

use Modules\SchoolAssessment\Entities\SchoolAssessment;
use Pingpong\Modules\Routing\Controller;

use Shine\Libraries\IdGenerator;
use Webpatser\Uuid\Uuid;

use Input,Session,Redirect;

class SchoolAssessmentController extends Controller {
	
	public function index()
	{
		$facility = Session::get('facility_details');

		$data['schools'] = SchoolAssessment::where('facility_id',$facility->facility_id)->get();

		return view('schoolassessment::index')->with($data);
	}

	public function new()
	{
		return view('schoolassessment::new');
	}

	public function save()
	{
		$school_id = $_POST['school_id'];

		if(!$school_id)
		{
			$school_id = IdGenerator::generateId();
			$school = new SchoolAssessment;
			$school->school_id = $school_id;

			$facility = Session::get('facility_details');
			$school->facility_id = $facility->facility_id;

			$school->uuid = Uuid::generate(4)->string;
		}
		else
		{
			$school = SchoolAssessment::where('school_id',$school_id)->first();
		}
		$school->school_name = isset($_POST['school_name']) ? $_POST['school_name'] : NULL;
		$school->contact_person = isset($_POST['contact_person']) ? $_POST['contact_person'] : NULL;
		$school->contact_person_position = isset($_POST['contact_person_position']) ? $_POST['contact_person_position'] : NULL;
		$school->activities_student_health_assessment = isset($_POST['activities_student_health_assessment']) ? $_POST['activities_student_health_assessment'] : NULL;
		$school->activities_staff_members_health_assessment = isset($_POST['activities_staff_members_health_assessment']) ? $_POST['activities_staff_members_health_assessment'] : NULL;
		$school->activities_sanitation_class = isset($_POST['activities_sanitation_class']) ? $_POST['activities_sanitation_class'] : NULL;
		$school->activities_diet_class = isset($_POST['activities_diet_class']) ? $_POST['activities_diet_class'] : NULL;
		$school->school_distance = isset($_POST['school_distance']) && $_POST['school_distance'] != '' ? $_POST['school_distance'] : NULL;
		$school->ecd_program = isset($_POST['ecd_program']) ? $_POST['ecd_program'] : NULL;
		$school->ecd_teacher = isset($_POST['ecd_teacher']) ? $_POST['ecd_teacher'] : NULL;
		$school->ecd_classroom = isset($_POST['ecd_classroom']) ? $_POST['ecd_classroom'] : NULL;
		$school->ecd_toilet = isset($_POST['ecd_toilet']) ? $_POST['ecd_toilet'] : NULL;
		$school->ecd_seat_arrangement = isset($_POST['ecd_seat_arrangement']) ? $_POST['ecd_seat_arrangement'] : NULL;
		$school->ecd_collision_avoidance = isset($_POST['ecd_collision_avoidance']) ? $_POST['ecd_collision_avoidance'] : NULL;
		$school->ecd_appr_play_material = isset($_POST['ecd_appr_play_material']) ? $_POST['ecd_appr_play_material'] : NULL;
		$school->ecd_non_slip_floors = isset($_POST['ecd_non_slip_floors']) ? $_POST['ecd_non_slip_floors'] : NULL;
		$school->ecd_colors = isset($_POST['ecd_colors']) ? $_POST['ecd_colors'] : NULL;
		$school->ecd_lighting = isset($_POST['ecd_lighting']) ? $_POST['ecd_lighting'] : NULL;
		$school->eca_playground = isset($_POST['eca_playground']) ? $_POST['eca_playground'] : NULL;
		$school->eca_spacing = isset($_POST['eca_spacing']) ? $_POST['eca_spacing'] : NULL;
		$school->eca_dancing = isset($_POST['eca_dancing']) ? $_POST['eca_dancing'] : NULL;
		$school->eca_singing = isset($_POST['eca_singing']) ? $_POST['eca_singing'] : NULL;
		$school->eca_running = isset($_POST['eca_running']) ? $_POST['eca_running'] : NULL;
		$school->eca_football = isset($_POST['eca_football']) ? $_POST['eca_football'] : NULL;
		$school->eca_volleyball = isset($_POST['eca_volleyball']) ? $_POST['eca_volleyball'] : NULL;
		$school->eca_basketball = isset($_POST['eca_basketball']) ? $_POST['eca_basketball'] : NULL;
		$school->eca_pingpong = isset($_POST['eca_pingpong']) ? $_POST['eca_pingpong'] : NULL;
		$school->eca_badminton = isset($_POST['eca_badminton']) ? $_POST['eca_badminton'] : NULL;
		$school->eca_martialarts = isset($_POST['eca_martialarts']) ? $_POST['eca_martialarts'] : NULL;
		$school->eca_oth = isset($_POST['eca_oth']) ? $_POST['eca_oth'] : NULL;
		$school->eca_are_available_oth_spec = isset($_POST['eca_are_available_oth_spec']) ? $_POST['eca_are_available_oth_spec'] : NULL;
		$school->first_aid_box = isset($_POST['first_aid_box']) ? $_POST['first_aid_box'] : NULL;
		$school->first_aid_adequacy = isset($_POST['first_aid_adequacy']) ? $_POST['first_aid_adequacy'] : NULL;
		$school->first_aid_students = isset($_POST['first_aid_students']) && $_POST['first_aid_students'] != '' ? $_POST['first_aid_students'] : NULL;
		$school->water_type = isset($_POST['water_type']) ? $_POST['water_type'] : NULL;
		$school->water_availability = isset($_POST['water_availability']) ? $_POST['water_availability'] : NULL;
		$school->nutrition_provided_food = isset($_POST['nutrition_provided_food']) ? $_POST['nutrition_provided_food'] : NULL;
		$school->nutrition_type_meal_breakfast = isset($_POST['nutrition_type_meal_breakfast']) ? $_POST['nutrition_type_meal_breakfast'] : NULL;
		$school->nutrition_type_meal_lunch = isset($_POST['nutrition_type_meal_lunch']) ? $_POST['nutrition_type_meal_lunch'] : NULL;
		$school->nutrition_type_meal_snacks = isset($_POST['nutrition_type_meal_snacks']) ? $_POST['nutrition_type_meal_snacks'] : NULL;
		$school->nutrition_type_meal_dinner = isset($_POST['nutrition_type_meal_dinner']) ? $_POST['nutrition_type_meal_dinner'] : NULL;
		$school->food_hygiene = isset($_POST['food_hygiene']) ? $_POST['food_hygiene'] : NULL;
		$school->food_balanced = isset($_POST['food_balanced']) ? $_POST['food_balanced'] : NULL;
		$school->food_from_home = isset($_POST['food_from_home']) ? $_POST['food_from_home'] : NULL;
		$school->food_junk = isset($_POST['food_junk']) ? $_POST['food_junk'] : NULL;
		$school->toil_has_running_water = isset($_POST['toil_has_running_water']) ? $_POST['toil_has_running_water'] : NULL;
		$school->toil_has_cleaning_water = isset($_POST['toil_has_cleaning_water']) ? $_POST['toil_has_cleaning_water'] : NULL;
		$school->toil_has_soap = isset($_POST['toil_has_soap']) ? $_POST['toil_has_soap'] : NULL;
		$school->toil_has_sep_gender = isset($_POST['toil_has_sep_gender']) ? $_POST['toil_has_sep_gender'] : NULL;
		$school->toil_has_sep_student_staff = isset($_POST['toil_has_sep_student_staff']) ? $_POST['toil_has_sep_student_staff'] : NULL;
		$school->toil_has_trash = isset($_POST['toil_has_trash']) ? $_POST['toil_has_trash'] : NULL;
		$school->toil_has_adequate_toilets = isset($_POST['toil_has_adequate_toilets']) ? $_POST['toil_has_adequate_toilets'] : NULL;
		$school->toilet_clean_range = isset($_POST['toilet_clean_range']) ? $_POST['toilet_clean_range'] : NULL;
		$school->school_location = isset($_POST['school_location']) ? $_POST['school_location'] : NULL;
		$school->school_location_oth_spec = isset($_POST['school_location_oth_spec']) ? $_POST['school_location_oth_spec'] : NULL;
		$school->building_type = isset($_POST['building_type']) ? $_POST['building_type'] : NULL;
		$school->age_building = isset($_POST['age_building']) && $_POST['age_building'] != '' ? $_POST['age_building'] : NULL;
		$school->height_building = isset($_POST['height_building']) ? $_POST['height_building'] : NULL;
		$school->water_damage = isset($_POST['water_damage']) ? $_POST['water_damage'] : NULL;
		$school->fire_fighting_instrument = isset($_POST['fire_fighting_instrument']) ? $_POST['fire_fighting_instrument'] : NULL;
		$school->disaster_drill = isset($_POST['disaster_drill']) ? $_POST['disaster_drill'] : NULL;
		$school->dd_how_often = isset($_POST['dd_how_often']) ? $_POST['dd_how_often'] : NULL;
		$school->disaster_preparedness = isset($_POST['disaster_preparedness']) ? $_POST['disaster_preparedness'] : NULL;
		$school->dp_specify = isset($_POST['dp_specify']) ? $_POST['dp_specify'] : NULL;
		$school->earthquake_resistant = isset($_POST['earthquake_resistant']) ? $_POST['earthquake_resistant'] : NULL;
		$school->earthquake_hazards = isset($_POST['earthquake_hazards']) ? $_POST['earthquake_hazards'] : NULL;
		$school->earthquake_vulnerabilities = isset($_POST['earthquake_vulnerabilities']) ? $_POST['earthquake_vulnerabilities'] : NULL;
		$school->earthquake_risk_score_one = isset($_POST['earthquake_risk_score_one']) ? $_POST['earthquake_risk_score_one'] : NULL;
		$school->earthquake_risk_score_two = isset($_POST['earthquake_risk_score_two']) ? $_POST['earthquake_risk_score_two'] : NULL;
		$school->landslide_hazards = isset($_POST['landslide_hazards']) ? $_POST['landslide_hazards'] : NULL;
		$school->landslide_vulnerabilities = isset($_POST['landslide_vulnerabilities']) ? $_POST['landslide_vulnerabilities'] : NULL;
		$school->landslide_risk_score_one = isset($_POST['landslide_risk_score_one']) ? $_POST['landslide_risk_score_one'] : NULL;
		$school->landslide_risk_score_two = isset($_POST['landslide_risk_score_two']) ? $_POST['landslide_risk_score_two'] : NULL;
		$school->wildlife_attack_hazards = isset($_POST['wildlife_attack_hazards']) ? $_POST['wildlife_attack_hazards'] : NULL;
		$school->wildlife_attack_vulnerabilities = isset($_POST['wildlife_attack_vulnerabilities']) ? $_POST['wildlife_attack_vulnerabilities'] : NULL;
		$school->wildlife_attack_risk_score_one = isset($_POST['wildlife_attack_risk_score_one']) ? $_POST['wildlife_attack_risk_score_one'] : NULL;
		$school->wildlife_attack_risk_score_two = isset($_POST['wildlife_attack_risk_score_two']) ? $_POST['wildlife_attack_risk_score_two'] : NULL;
		$school->fire_hazards = isset($_POST['fire_hazards']) ? $_POST['fire_hazards'] : NULL;
		$school->fire_vulnerabilities = isset($_POST['fire_vulnerabilities']) ? $_POST['fire_vulnerabilities'] : NULL;
		$school->fire_risk_score_one = isset($_POST['fire_risk_score_one']) ? $_POST['fire_risk_score_one'] : NULL;
		$school->fire_risk_score_two = isset($_POST['fire_risk_score_two']) ? $_POST['fire_risk_score_two'] : NULL;
		$school->pest_diseases_hazards = isset($_POST['pest_diseases_hazards']) ? $_POST['pest_diseases_hazards'] : NULL;
		$school->pest_diseases_vulnerabilities = isset($_POST['pest_diseases_vulnerabilities']) ? $_POST['pest_diseases_vulnerabilities'] : NULL;
		$school->pest_diseases_risk_score_one = isset($_POST['pest_diseases_risk_score_one']) ? $_POST['pest_diseases_risk_score_one'] : NULL;
		$school->pest_diseases_risk_score_two = isset($_POST['pest_diseases_risk_score_two']) ? $_POST['pest_diseases_risk_score_two'] : NULL;
		$school->flood_hazards = isset($_POST['flood_hazards']) ? $_POST['flood_hazards'] : NULL;
		$school->flood_vulnerabilities = isset($_POST['flood_vulnerabilities']) ? $_POST['flood_vulnerabilities'] : NULL;
		$school->flood_risk_score_one = isset($_POST['flood_risk_score_one']) ? $_POST['flood_risk_score_one'] : NULL;
		$school->flood_risk_score_two = isset($_POST['flood_risk_score_two']) ? $_POST['flood_risk_score_two'] : NULL;
		$school->windstorm_hazards = isset($_POST['windstorm_hazards']) ? $_POST['windstorm_hazards'] : NULL;
		$school->windstorm_vulnerabilities = isset($_POST['windstorm_vulnerabilities']) ? $_POST['windstorm_vulnerabilities'] : NULL;
		$school->windstorm_risk_score_one = isset($_POST['windstorm_risk_score_one']) ? $_POST['windstorm_risk_score_one'] : NULL;
		$school->windstorm_risk_score_two = isset($_POST['windstorm_risk_score_two']) ? $_POST['windstorm_risk_score_two'] : NULL;
		$school->drought_hazards = isset($_POST['drought_hazards']) ? $_POST['drought_hazards'] : NULL;
		$school->drought_vulnerabilities = isset($_POST['drought_vulnerabilities']) ? $_POST['drought_vulnerabilities'] : NULL;
		$school->drought_risk_score_one = isset($_POST['drought_risk_score_one']) ? $_POST['drought_risk_score_one'] : NULL;
		$school->drought_risk_score_two = isset($_POST['drought_risk_score_two']) ? $_POST['drought_risk_score_two'] : NULL;
		$school->hailstorm_hazards = isset($_POST['hailstorm_hazards']) ? $_POST['hailstorm_hazards'] : NULL;
		$school->hailstorm_vulnerabilities = isset($_POST['hailstorm_vulnerabilities']) ? $_POST['hailstorm_vulnerabilities'] : NULL;
		$school->hailstorm_risk_score_one = isset($_POST['hailstorm_risk_score_one']) ? $_POST['hailstorm_risk_score_one'] : NULL;
		$school->hailstorm_risk_score_two = isset($_POST['hailstorm_risk_score_two']) ? $_POST['hailstorm_risk_score_two'] : NULL;
		$school->soil_erosion_hazards = isset($_POST['soil_erosion_hazards']) ? $_POST['soil_erosion_hazards'] : NULL;
		$school->soil_erosion_vulnerabilities = isset($_POST['soil_erosion_vulnerabilities']) ? $_POST['soil_erosion_vulnerabilities'] : NULL;
		$school->soil_erosion_risk_score_one = isset($_POST['soil_erosion_risk_score_one']) ? $_POST['soil_erosion_risk_score_one'] : NULL;
		$school->soil_erosion_risk_score_two = isset($_POST['soil_erosion_risk_score_two']) ? $_POST['soil_erosion_risk_score_two'] : NULL;
		$school->epidemics_hazards = isset($_POST['epidemics_hazards']) ? $_POST['epidemics_hazards'] : NULL;
		$school->epidemics_vulnerabilities = isset($_POST['epidemics_vulnerabilities']) ? $_POST['epidemics_vulnerabilities'] : NULL;
		$school->epidemics_risk_score_one = isset($_POST['epidemics_risk_score_one']) ? $_POST['epidemics_risk_score_one'] : NULL;
		$school->epidemics_risk_score_two = isset($_POST['epidemics_risk_score_two']) ? $_POST['epidemics_risk_score_two'] : NULL;

		$school->save();

        Session::flash('flash_type', 'alert-success alert-dismissible');
        Session::flash('flash_message', 'Success! The school record has been saved.'); 

		return Redirect::to('schoolassessment/update/'.$school_id);
	}

	public function update($id)
	{
		$data['school'] = SchoolAssessment::where('school_id',$id)->first();

		return view('schoolassessment::edit')->with($data);
	}

	public function delete($id)
	{
		$school = SchoolAssessment::where('school_id',$id)->first();
		$school->delete();

        Session::flash('flash_type', 'alert-danger alert-dismissible');
        Session::flash('flash_message', 'Success! The school record has been deleted.'); 

		return Redirect::to('schoolassessment');
	}
	
}