<?php
$civil_status_value = NULL;
$education_value = NULL;

if(isset($patient)) {
    $civil_status_value = $patient->civil_status;
    $education_value = $patient->education;
}
?>
<div class="tab-pane step" id="personal">
    <fieldset {{ $hidden }}>
        <legend>Personal Information</legend>

        <fieldset>
            <div class="form-group">
                <label class="col-sm-2 control-label">Suffix</label>
                <div class="col-sm-4">
                    <div class="btn-group toggler" data-toggle="buttons">
                      <label class="btn btn-default @if(isset($patient) AND $patient->suffix == 'SR') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="SR"> Sr.
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->suffix == 'JR') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="JR"> Jr.
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->suffix == 'II') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="II"> II
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->suffix == 'III') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="III"> III
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->suffix == 'IV') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="IV"> IV
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->suffix == 'V') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="V"> V
                      </label>
                      <label class="btn btn-default">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="NA"> None
                      </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Gender *</label>
                <div class="col-sm-4">
                    <div class="btn-group toggler" data-toggle="buttons">
                      <?php
                        $genderm = false; $genderf = false; $genderu = false; $gender = "";
                        if(isset($patient) AND $patient->gender == "M") $genderm = true;
                        if(isset($patient) AND $patient->gender == "F") $genderf = true;
                        if(isset($patient) AND $patient->gender == "U") $genderu = true;
                        if(isset($patient)) $gender = $patient->gender;
                      ?>
                      <label class="btn btn-default required @if(isset($patient) AND $patient->gender=='M') active @endif">
                        <i class="fa fa-check"></i> {!! Form::radio('', 'M', $genderm, array('class' => 'gender', 'autocomplete'=>'off', 'onchange'=>"$('#sex_field').val(this.value);" )) !!} Male
                      </label>
                      <label class="btn btn-default required @if(isset($patient) AND $patient->gender=='F') active @endif">
                        <i class="fa fa-check"></i> {!! Form::radio('', 'F', $genderf, array('class' => 'gender', 'autocomplete'=>'off', 'onchange'=>"$('#sex_field').val(this.value);")) !!} Female
                      </label>
                      @if($genderu)
                      <label class="btn btn-default required @if(isset($patient) AND $patient->gender=='U') active @endif">
                        <i class="fa fa-check"></i> {!! Form::radio('', 'U', $genderu, array('class' => 'gender', 'autocomplete'=>'off')) !!} Unknown
                      </label>
                      @endif
                    </div>
                    <input type="hidden" name="inputPatientGender" class="required marginb-2" id="sex_field" value="{{ $gender }}" />
                </div>
            </div>
            <div class="maiden">
                <div class="form-group col-md-6">
                    <label class="col-sm-4 control-label">Maiden Last Name </label>
                    <div class="col-sm-8">
                        {!! Form::text('maiden_lastname', null, array('class' => 'form-control alphaonly', 'name'=>'inputMaidenLastName')) !!}
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-sm-4 control-label">Maiden Middle Name </label>
                    <div class="col-sm-8">
                        {!! Form::text('maiden_middlename', null, array('class' => 'form-control alphaonly', 'name'=>'inputMaidenMiddleName')) !!}
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend></legend>
            <div class="form-group col-md-6">
                <label class="col-sm-4 control-label">Place of Birth</label>
                <div class="col-sm-8">
                    {!! Form::text('birthplace', null, array('class' => 'form-control alphaonly', 'name'=>'inputPatientBirthPlace')) !!}
                </div>
            </div>
            <div class="form-group col-md-6">
                <label class="col-sm-4 control-label">Time of Birth</label>
                <div class="col-sm-8 iconed-input bootstrap-timepicker">
                    <i class="fa fa-clock-o inner-icon"></i>
                    {!! Form::text('birthtime', null, array('class' => 'form-control timepicker', 'name'=>'inputPatientBirthTime','id'=>'')) !!}
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend></legend>
            <div class="form-group col-md-6 hidden">
                <label class="col-sm-4 control-label">Religion</label>
                <div class="col-sm-8">
                    <select class="populate placeholder form-control" name="inputPatientReligion" id="inputPatientReligion" onchange="if(this.value == 'OTHER') { $('#religionother').removeClass('hidden'); } else { $('#religionother').addClass('hidden'); }">
                        <option value="">- Select religion -</option>
                        @foreach ($religion as $key=>$val)
                            <option value='{{ $key }}' @if(isset($patient) AND $patient->religion == $key) selected='selected' @endif >{{ $val }}</option>
                        @endforeach
                    </select>
                </div>
                <div id="religionother" class="hidden">
                    <label class="col-sm-4 control-label">Specify Religion</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control alphaonly" name="inputPatientOtherReligion" placeholder="Other Religion" value="{{{ isset($patient->religion_others) ? $patient->religion_others : '' }}}"  />
                    </div>
                </div>
            </div>
            <div class="form-group col-md-6">

                <label class="col-sm-4 control-label" for="education">Highest Education</label>
                <div class="col-sm-8">
                    {!! Form::select('inputPatientEducation', $highest_education, $education_value, ['class' => 'form-control', 'onchange'=>"if(this.value == '99') { $('#educother').removeClass('hidden'); } else { $('#educother').addClass('hidden'); }", 'id'=>'inputPatientEducation']) !!}
                </div>
                <div id="educother" class="hidden">
                    <label class="col-sm-4 control-label">Specify Education</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control alphaonly" name="inputPatientEducationOther" placeholder="Other Education" value="{{{ isset($patient->highesteducation_others) ? $patient->highesteducation_others : '' }}}"  />
                    </div>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label class="col-sm-4 control-label">Nationality</label>
                <div class="col-sm-8">
                    <?php
                        $nat = 'PHL';
                        if(isset($patient) AND $patient->nationality != NULL) {
                            $nat = $patient->nationality;
                        }
                    ?>
                    <!-- <input type="text" class="form-control alpha" name="inputPatientNationality" value="{{{ isset($patient->nationality) ? $patient->nationality : 'PHL' }}}"  /> -->
                    {!! Form::select('inputPatientNationality', $nationality, $nat, ['class' => 'form-control alpha']) !!}
                </div>
            </div>

        </fieldset>
    </fieldset>
</div><!-- /.tab-pane -->


