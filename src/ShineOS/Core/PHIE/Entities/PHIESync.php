<?php namespace ShineOS\Core\PHIE\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PHIESync extends Model {
    use SoftDeletes;  
    protected $fillable = [];
    protected $dates = array('deleted_at','created_at','updated_at');
    protected $table = 'phie_sync'; 
    protected $primaryKey = 'phie_sync';

    public function Facility() {
    	return $this->belongsTo('ShineOS\Core\Facilities\Entities\Facilities', 'facility_id', 'facility_id');
    }
}