<?php

/**
** UPDATE SCRIPT TEMPLATE
** Use this for creating an update script for SHINEOS+ CE
** Use migration scripts for DB additions
** Use DB functions for updating tables
**
** Version 3.4 update
** Last update for year-3 of SHINEOS Research Project
**
** Add Philhealth PMRF tables and Dependent Tables
** Add column 'owner' to Facility Patient User table
** Fix Facility table by adding
**    -- enabled plugins
**    -- enabled modules
**    -- add ownership_type value 'GOVERNMENT'
** Add plugin tables required for RHUs
** Fix User table by changing gender to nullable
** Fix Tuberculosis Intake Table by adding column 'adult_drug_intake_remarks'
** Fix Philhealth_info table by adding 'benefactor_relation' column
** Add new ICD10 Diagnosis Table
**
**/

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Shine\Libraries\FacilityHelper;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\FacilityPatientUser;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Install;
use ShineOS\Utils\Database as DbUtils;

use DB, Schema;

function checkIndex($tblname, $index) {
    Schema::table(
        $tblname,
        function (Blueprint $table) use($tblname, $index) {
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $indexesFound = $sm->listTableIndexes($tblname);

            $indexesToCheck = [
                $tblname."_".$index."_index",
                $index
            ];

            foreach ($indexesToCheck as $currentIndex) {
                if (array_key_exists($currentIndex, $indexesFound)) {
                    return "found";
                }
            }
            return "notfound";
        }
    );
}

function doupdate($type) {

    //let us backup current DB
    

    echo "<h3>Updating SHINEOS+</h3><br><br>";
    ob_flush(); flush();
    usleep(1125000);
    $installer = new Install\DbInstaller();
    ob_flush(); flush();
    usleep(250000);

    echo "Updating Databases...<br>";
    ob_flush(); flush();
    usleep(1125000);
    $facility = DB::table('facilities')->first();
    ob_flush(); flush();
    usleep(250000);

    if($facility) {
        echo "Updating Facility Table...<br>";
        ob_flush(); flush();
        usleep(1125000);

        //let us make sure plugins are updated_option
        if($type == 'government') {
            DB::table('facilities')
                ->where('facility_id', $facility->facility_id)
                ->update(['enabled_plugins' => '["MaternalCare","FamilyPlanning","Pediatrics","Tuberculosis","Employment","FamilyInfo","MedicalHistory","Philhealth","Geodata"]', 'enabled_modules' => '["Laboratory","Calendar"]', 'ownership_type' => 'GOVERNMENT' ]);
        }

        $facility = DB::table('facilities')->first();

        if(!Schema::hasColumn('facilities', 'catchment_area'))
        {
            Schema::table('facilities', function (Blueprint $table) {
                $table->text('catchment_area')->nullable();
            });
        }

        //let us update databases:
        $patientPluginDir = plugins_path()."/";
        $plugins = directoryFiles($patientPluginDir);
        foreach($plugins as $k=>$plugin) {
            if(strpos($plugin, ".")===false) {
                //check if config.php exists
                if(file_exists(plugins_path().$plugin.'/config.php')){
                    include(plugins_path().$plugin.'/config.php');

                    //check if this folder is enabled
                    if(json_decode($facility->enabled_plugins) != NULL) {
                        if(in_array($plugin_id, json_decode($facility->enabled_plugins))){
                            if(!Schema::hasTable($plugin_table)) {
                                $migrate = Artisan::call('migrate', ['--path'=>'plugins/'.$plugin, '--force'=>true]);
                            }

                        }
                    }
                }
            }
        }
        ob_flush(); flush();
        usleep(250000);

        echo "Adding Philhealth PMRF Table...<br>";
        ob_flush(); flush();
        usleep(1125000);
        if(!Schema::hasTable('philhealth_pmrf')) {
            $migrate = Artisan::call('migrate', ['--path'=>'plugins/Philhealth', '--force'=>true]);
        }
        ob_flush(); flush();
        usleep(250000);

        echo "Adding New ICD10 Base Diagnosis Table...<br>";
        ob_flush(); flush();
        usleep(1125000);
        if(!Schema::hasTable('lov_icd10_diagnosis'))
        {
            Schema::create('lov_icd10_diagnosis', function ($table) {
                $table->increments('id');
                $table->string('code', 60);
                $table->text('diagnosis_name');
                $table->softDeletes();
                $table->timestamps();
            });
            $installer->run('lov_icd10_diagnosis');
        }
        ob_flush(); flush();
        usleep(250000);

        echo "Fixing Facility Patient User Table...<br>";
        ob_flush(); flush();
        usleep(1125000);
        if(!Schema::hasColumn('facility_patient_user', 'owner'))
        {
            Schema::table('facility_patient_user', function ($table) {
                $table->string('owner', 1)->nullable()->default(1);
            });
        }
        ob_flush(); flush();
        usleep(250000);

        echo "Fixing Examination Table...<br>";
        ob_flush(); flush();
        usleep(1125000);
        if(!Schema::hasColumn('examination', 'Hypertrophic_Congestion'))
        {
            Schema::table('examination', function ($table) {
                $table->string('Hypertrophic_Congestion', 5)->nullable();
            });
        }
        if(!Schema::hasColumn('examination', 'Retractions'))
        {
            Schema::table('examination', function ($table) {
                $table->string('Retractions', 5)->nullable();
            });
        }
        if(!Schema::hasColumn('examination', 'Gross_Deformity'))
        {
            Schema::table('examination', function ($table) {
                $table->string('Gross_Deformity', 5)->nullable();
            });
        }
        ob_flush(); flush();
        usleep(250000);

        echo "Fixing Philhealth Table...<br>";
        ob_flush(); flush();
        usleep(1125000);
        if(!Schema::hasColumn('patient_philhealthinfo', 'benefactor_relation'))
        {
            Schema::table('patient_philhealthinfo', function ($table) {
                $table->string('benefactor_relation', 35)->nullable();
            });
        }
        if(!Schema::hasColumn('patient_philhealthinfo', 'For_Enlistment'))
        {
            Schema::table('patient_philhealthinfo', function ($table) {
                $table->string('For_Enlistment', 1)->nullable();
            });
        }
        ob_flush(); flush();
        usleep(250000);

        echo "Fixing Tuberculosis Intake Table...<br>";
        ob_flush(); flush();
        usleep(1125000);
        if(!Schema::hasColumn('tuberculosis_drug_intake_record', 'adult_drug_intake_remarks'))
        {
            Schema::table('tuberculosis_drug_intake_record', function ($table) {
                $table->text('adult_drug_intake_remarks')->nullable();
            });
        }
        ob_flush(); flush();
        usleep(250000);

        echo "Fixing Users Table...<br>";
        ob_flush(); flush();
        usleep(1125000);
        if(Schema::hasColumn('users', 'gender'))
        {
            DB::statement('ALTER TABLE `users` CHANGE `gender` `gender` ENUM("F","M","U") NULL DEFAULT "U";');
        }
        $conn = Schema::getConnection();
        $dbSchemaManager = $conn->getDoctrineSchemaManager();
        $doctrineTable = $dbSchemaManager->listTableDetails('user_contact');
        if(!$doctrineTable->hasIndex('user_contact_user_id_unique'))
        {
            Schema::table('user_contact', function ($table) {
                $table->unique('user_id');
            });
        }
        
        $doctrineTable2 = $dbSchemaManager->listTableDetails('roles_access');
        if(!$doctrineTable2->hasIndex('roles_access_facilityuser_id_unique'))
        {
            Schema::table('roles_access', function ($table) {
                $table->unique('facilityuser_id');
            });
        }
        ob_flush(); flush();
        usleep(250000);
        
        echo "Fixing Views Table...<br>";
        ob_flush(); flush();
        usleep(1125000);
        $builder = new DbUtils();
        $shineos_sql = base_path() . DS . 'public' . DS . 'updates' . DS . 'viewupdates.sql';
        $builder->import_sql_file($shineos_sql);

        ob_flush(); flush();
        usleep(250000);


        echo "Adding Indexes...<br>";
        ob_flush(); flush();
        usleep(1125000);

        if(checkIndex('patients','patient_id') == "notfound"){
        Schema::table('patients', function ($table) {
            $table->index(['patient_id']);
        });
        }

        if(checkIndex('patient_contact','patient_id') == "notfound"){
        Schema::table('patient_contact', function ($table) {
            $table->index(['patient_id']);
        });
        }

        if(checkIndex('patient_alert','patient_id') == "notfound"){
        Schema::table('patient_alert', function ($table) {
            $table->index(['patient_id']);
        });
        }

        if(checkIndex('allergy_patient','allergy_patient_id') == "notfound"){
        Schema::table('allergy_patient', function ($table) {
            $table->index(['allergy_patient_id']);
        });
        }

        if(checkIndex('patient_emergencyinfo','patient_id') == "notfound"){
        Schema::table('patient_emergencyinfo', function ($table) {
            $table->index(['patient_id']);
        });
        }

        if(checkIndex('patient_death_info','patient_id') == "notfound"){
        Schema::table('patient_death_info', function ($table) {
            $table->index(['patient_id']);
        });
        }

        if(checkIndex('patient_immunization','patient_id') == "notfound"){
        Schema::table('patient_immunization', function ($table) {
            $table->index(['patient_id']);
        });
        }

        if(checkIndex('facility_patient_user','patient_id') == "notfound"){
        Schema::table('facility_patient_user', function ($table) {
            $table->index(['patient_id']);
        });
        }

        if(checkIndex('patient_medicalhistory','patient_id') == "notfound"){
        Schema::table('patient_medicalhistory', function ($table) {
            $table->index(['patient_id']);
        });
        }

        if(checkIndex('patient_familyinfo','patient_id') == "notfound"){
        Schema::table('patient_familyinfo', function ($table) {
            $table->index(['patient_id']);
        });
        }

        if(checkIndex('facility_patient_user','facilityuser_id') == "notfound"){
        Schema::table('facility_patient_user', function ($table) {
            $table->index(['facilityuser_id']);
        });
        }

        if(checkIndex('healthcare_services','healthcareservice_id') == "notfound"){
        Schema::table('healthcare_services', function ($table) {
            $table->index(['healthcareservice_id']);
        });
        }

        if(checkIndex('healthcare_services','parent_service_id') == "notfound"){
        Schema::table('healthcare_services', function ($table) {
            $table->index(['parent_service_id']);
        });
        }

        if(checkIndex('healthcare_services','seen_by') == "notfound"){
        Schema::table('healthcare_services', function ($table) {
            $table->index(['seen_by']);
        });
        }

        if(checkIndex('disposition','healthcareservice_id') == "notfound"){
        Schema::table('disposition', function ($table) {
            $table->index(['healthcareservice_id']);
        });
        }

        if(checkIndex('disposition','disposition_id') == "notfound"){
        Schema::table('disposition', function ($table) {
            $table->index(['disposition_id']);
        });
        }

        if(checkIndex('healthcare_addendum','healthcareservice_id') == "notfound"){
        Schema::table('healthcare_addendum', function ($table) {
            $table->index(['healthcareservice_id']);
        });
        }

        if(checkIndex('healthcare_addendum','addendum_id') == "notfound"){
        Schema::table('healthcare_addendum', function ($table) {
            $table->index(['addendum_id']);
        });
        }

        if(checkIndex('general_consultation','healthcareservice_id') == "notfound"){
        Schema::table('general_consultation', function ($table) {
            $table->index(['healthcareservice_id']);
        });
        }

        if(checkIndex('general_consultation','generalconsultation_id') == "notfound"){
        Schema::table('general_consultation', function ($table) {
            $table->index(['generalconsultation_id']);
        });
        }

        if(checkIndex('vital_physical','healthcareservice_id') == "notfound"){
        Schema::table('vital_physical', function ($table) {
            $table->index(['healthcareservice_id']);
        });
        }

        if(checkIndex('vital_physical','vitalphysical_id') == "notfound"){
        Schema::table('vital_physical', function ($table) {
            $table->index(['vitalphysical_id']);
        });
        }

        if(checkIndex('examination','healthcareservice_id') == "notfound"){
        Schema::table('examination', function ($table) {
            $table->index(['healthcareservice_id']);
        });
        }

        if(checkIndex('examination','examination_id') == "notfound"){
        Schema::table('examination', function ($table) {
            $table->index(['examination_id']);
        });
        }

        if(checkIndex('diagnosis','healthcareservice_id') == "notfound"){
        Schema::table('diagnosis', function ($table) {
            $table->index(['healthcareservice_id']);
        });
        }

        if(checkIndex('diagnosis','diagnosis_id') == "notfound"){
        Schema::table('diagnosis', function ($table) {
            $table->index(['diagnosis_id']);
        });
        }

        if(checkIndex('diagnosis_icd10','diagnosis_id') == "notfound"){
        Schema::table('diagnosis_icd10', function ($table) {
            $table->index(['diagnosis_id']);
        });
        }

        if(checkIndex('medicalorder','healthcareservice_id') == "notfound"){
        Schema::table('medicalorder', function ($table) {
            $table->index(['healthcareservice_id']);
        });
        }

        if(checkIndex('medicalorder','medicalorder_id') == "notfound"){
        Schema::table('medicalorder', function ($table) {
            $table->index(['medicalorder_id']);
        });
        }

        if(checkIndex('medicalorder','deleted_at') == "notfound"){
        Schema::table('medicalorder', function ($table) {
            $table->index(['deleted_at']);
        });
        }

        if(checkIndex('medicalorder_prescription','medicalorder_id') == "notfound"){
        Schema::table('medicalorder_prescription', function ($table) {
            $table->index(['medicalorder_id']);
        });
        }

        if(checkIndex('medicalorder_prescription','medicalorderprescription_id') == "notfound"){
        Schema::table('medicalorder_prescription', function ($table) {
            $table->index(['medicalorderprescription_id']);
        });
        }

        if(checkIndex('medicalorder_laboratoryexam','medicalorder_id') == "notfound"){
        Schema::table('medicalorder_laboratoryexam', function ($table) {
            $table->index(['medicalorder_id']);
        });
        }

        if(checkIndex('medicalorder_procedure','medicalorder_id') == "notfound"){
        Schema::table('medicalorder_procedure', function ($table) {
            $table->index(['medicalorder_id']);
        });
        }

        if(checkIndex('facilities','facility_id') == "notfound"){
        Schema::table('facilities', function ($table) {
            $table->index(['facility_id']);
        });
        }

        if(checkIndex('facilities','DOH_facility_code') == "notfound"){
        Schema::table('facilities', function ($table) {
            $table->index(['DOH_facility_code']);
        });
        }

        if(checkIndex('facility_contact','facility_id') == "notfound"){
        Schema::table('facility_contact', function ($table) {
            $table->index(['facility_id']);
        });
        }

        if(checkIndex('facility_contact','facilitycontact_id') == "notfound"){
        Schema::table('facility_contact', function ($table) {
            $table->index(['facilitycontact_id']);
        });
        }

        if(checkIndex('facility_workforce','facility_id') == "notfound"){
        Schema::table('facility_workforce', function ($table) {
            $table->index(['facility_id']);
        });
        }

        if(checkIndex('facility_user','facilityuser_id') == "notfound"){
        Schema::table('facility_user', function ($table) {
            $table->index(['facilityuser_id']);
        });
        }

        if(checkIndex('facility_user','user_id') == "notfound"){
        Schema::table('facility_user', function ($table) {
            $table->index(['user_id']);
        });
        }

        if(checkIndex('facility_user','facility_id') == "notfound"){
        Schema::table('facility_user', function ($table) {
            $table->index(['facility_id']);
        });
        }

        if(checkIndex('users','user_id') == "notfound"){
        Schema::table('users', function ($table) {
            $table->index(['user_id']);
        });
        }

        if(checkIndex('users','email') == "notfound"){
        Schema::table('users', function ($table) {
            $table->index(['email']);
        });
        }

        if(checkIndex('user_md','user_id') == "notfound"){
        Schema::table('user_md', function ($table) {
            $table->index(['user_id']);
        });
        }

        if(checkIndex('user_contact','user_id') == "notfound"){
        Schema::table('user_contact', function ($table) {
            $table->index(['user_id']);
        });
        }

        if(checkIndex('roles','role_id') == "notfound"){
        Schema::table('roles', function ($table) {
            $table->index(['role_id']);
        });
        }

        if(checkIndex('roles_access','role_id') == "notfound"){
        Schema::table('roles_access', function ($table) {
            $table->index(['role_id']);
        });
        }

        if(checkIndex('roles_access','facilityuser_id') == "notfound"){
        Schema::table('roles_access', function ($table) {
            $table->index(['facilityuser_id']);
        });
        }

        if(checkIndex('lov_diagnosis','diagnosis_id') == "notfound"){
        Schema::table('lov_diagnosis', function ($table) {
            $table->index(['diagnosis_id']);
        });
        }

        if(checkIndex('lov_allergy_reaction','allergyreaction_id') == "notfound"){
        Schema::table('lov_allergy_reaction', function ($table) {
            $table->index(['allergyreaction_id']);
        });
        }

        if(checkIndex('lov_diseases','disease_id') == "notfound"){
        Schema::table('lov_diseases', function ($table) {
            $table->index(['disease_id']);
        });
        }

        if(checkIndex('lov_disabilities','disability_id') == "notfound"){
        Schema::table('lov_disabilities', function ($table) {
            $table->index(['disability_id']);
        });
        }

        if(checkIndex('lov_immunizations','immunization_code') == "notfound"){
        Schema::table('lov_immunizations', function ($table) {
            $table->index(['immunization_code']);
        });
        }

        if(checkIndex('lov_medicalcategory','medicalcategory_id') == "notfound"){
        Schema::table('lov_medicalcategory', function ($table) {
            $table->index(['medicalcategory_id']);
        });
        }

        if(checkIndex('lov_icd10','icd10_code') == "notfound"){
        Schema::table('lov_icd10', function ($table) {
            $table->index(['icd10_code']);
        });
        }

        if(checkIndex('lov_icd10','icd10_category') == "notfound"){
        Schema::table('lov_icd10', function ($table) {
            $table->index(['icd10_category']);
        });
        }

        if(checkIndex('','') == "notfound"){
        Schema::table('lov_icd10', function ($table) {
            $table->index(['icd10_subcategory']);
        });
        }

        if(checkIndex('lov_icd10','icd10_tricategory') == "notfound"){
        Schema::table('lov_icd10', function ($table) {
            $table->index(['icd10_tricategory']);
        });
        }

        if(checkIndex('lov_drugs','drugs_id') == "notfound"){
        Schema::table('lov_drugs', function ($table) {
            $table->index(['drugs_id']);
        });
        }

        if(checkIndex('lov_barangays','barangay_code') == "notfound"){
        Schema::table('lov_barangays', function ($table) {
            $table->index(['barangay_code']);
        });
        }

        if(checkIndex('lov_region','region_code') == "notfound"){
        Schema::table('lov_region', function ($table) {
            $table->index(['region_code']);
        });
        }

        if(checkIndex('lov_province','province_code') == "notfound"){
        Schema::table('lov_province', function ($table) {
            $table->index(['province_code']);
        });
        }

        if(checkIndex('lov_citymunicipalities','city_code') == "notfound"){
        Schema::table('lov_citymunicipalities', function ($table) {
            $table->index(['city_code']);
        });
        }

        if(checkIndex('lov_medicalprocedures','procedure_code') == "notfound"){
        Schema::table('lov_medicalprocedures', function ($table) {
            $table->index(['procedure_code']);
        });
        }

        if(checkIndex('lov_loincs','loinc_code') == "notfound"){
        Schema::table('lov_loincs', function ($table) {
            $table->index(['loinc_code']);
        });
        }

        if(checkIndex('lov_laboratories','laboratorycode') == "notfound"){
        Schema::table('lov_laboratories', function ($table) {
            $table->index(['laboratorycode']);
        });
        }

        if(checkIndex('lov_doh_facility_codes','code') == "notfound"){
        Schema::table('lov_doh_facility_codes', function ($table) {
            $table->index(['code']);
        });
        }

        if(checkIndex('lov_enumerations','code') == "notfound"){
        Schema::table('lov_enumerations', function ($table) {
            $table->index(['code']);
        });
        }

        if(checkIndex('lov_modules','module_name') == "notfound"){
        Schema::table('lov_modules', function ($table) {
            $table->index(['module_name']);
        });
        }

        if(checkIndex('lov_forms','form_name') == "notfound"){
        Schema::table('lov_forms', function ($table) {
            $table->index(['form_name']);
        });
        }

        if(checkIndex('lov_forms','facility_id') == "notfound"){
        Schema::table('lov_forms', function ($table) {
            $table->index(['facility_id']);
        });
        }

        ob_flush(); flush();
        usleep(250000);

        return "done";

    } else {

        echo "You are trying to update a newly installed SHINEOS+ Community Edition. You do not need to update.";

    }
}
