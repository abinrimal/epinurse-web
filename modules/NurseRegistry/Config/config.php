<?php

return [
    'name' => 'Nurse Registry',
    'icon' => 'fa-user-circle',
    'roles' => '["Doctor Admin","Doctor","Nurse","Midwife","Encoder"]',
    'version' => '1.0',
    'title' => 'Nurse Registry Module',
    'folder' => 'NurseRegistry',
    'table' => 'nurse_registry',
    'description' => 'EpiNurse Nurse Registry Module',
    'developer' => 'mediXserve',
    'copy' => '2018',
    'url' => 'epinurse.org'
];