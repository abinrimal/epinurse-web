<?php
namespace Plugins\HealthAssessment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Shine\Libraries\IdGenerator;
use Webpatser\Uuid\Uuid;
use DB;

class HealthAssessmentModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'healthassessment_service';
    protected static $table_name = 'healthassessment_service';
    protected $primaryKey = 'healthassessment_id';
    protected $touches = array('Healthcareservices');

    public function Healthcareservices() {
        return $this->belongsTo('ShineOS\Core\Healthcareservices\Entities\Healthcareservices', 'healthcareservice_id', 'healthcareservice_id');
    }

    public static function submit($data,$hid)
    {
        $health_assessment = self::where('healthcareservice_id',$hid)->first();

        if(!$health_assessment) {
            $health_assessment =  new self;
            $health_assessment->healthassessment_id = IdGenerator::generateId();
            $health_assessment->healthcareservice_id = $hid;
            $health_assessment->uuid = Uuid::generate(4)->string;
            $health_assessment->owner_uuid = isset($data['ownerUuid']) ? $data['ownerUuid'] : NULL;
        }

        $health_assessment->student_employee = isset($data['student_employee']) ? $data['student_employee'] : NULL;

        $health_assessment->school = isset($data['school']) ? $data['school'] : NULL;
        $health_assessment->class = isset($data['class']) ? $data['class'] : NULL;

        $health_assessment->id_ari = isset($data['id_ari']) ? $data['id_ari'] : NULL ;
        $health_assessment->id_flaccidpar = isset($data['id_flaccidpar']) ? $data['id_flaccidpar'] : NULL;
        $health_assessment->id_watery_diarrhea = isset($data['id_watery_diarrhea']) ? $data['id_watery_diarrhea'] : NULL;
        $health_assessment->id_bloody_diarrhea = isset($data['id_bloody_diarrhea']) ? $data['id_bloody_diarrhea'] : NULL;
        $health_assessment->id_dysentery = isset($data['id_dysentery']) ? $data['id_dysentery'] : NULL;
        $health_assessment->id_ajs = isset($data['id_ajs']) ? $data['id_ajs'] : NULL;
        $health_assessment->id_susmeningitis = isset($data['id_susmeningitis']) ? $data['id_susmeningitis'] : NULL;
        $health_assessment->id_sustetanus = isset($data['id_sustetanus']) ? $data['id_sustetanus'] : NULL;
        $health_assessment->id_hemfever = isset($data['id_hemfever']) ? $data['id_hemfever'] : NULL;
        $health_assessment->id_fever = isset($data['id_fever']) ? $data['id_fever'] : NULL;

        $health_assessment->vaccine_bcg = isset($data['vaccine_bcg']) ? $data['vaccine_bcg'] : NULL;
        $health_assessment->vaccine_dpt = isset($data['vaccine_dpt']) ? $data['vaccine_dpt'] : NULL;
        $health_assessment->vaccine_opv = isset($data['vaccine_opv']) ? $data['vaccine_opv'] : NULL;
        $health_assessment->vaccine_pcv = isset($data['vaccine_pcv']) ? $data['vaccine_pcv'] : NULL;
        $health_assessment->vaccine_mr = isset($data['vaccine_mr']) ? $data['vaccine_mr'] : NULL;
        $health_assessment->vaccine_je = isset($data['vaccine_je']) ? $data['vaccine_je'] : NULL;
        $health_assessment->vaccine_td = isset($data['vaccine_td']) ? $data['vaccine_td'] : NULL;
        $health_assessment->vaccine_oth = isset($data['vaccine_oth']) ? $data['vaccine_oth'] : NULL;
        $health_assessment->vaccine_oth_spec = isset($data['vaccine_oth_spec']) ? $data['vaccine_oth_spec'] : NULL;

        $health_assessment->mc_injuretrauma_head = isset($data['mc_injuretrauma_head']) ? $data['mc_injuretrauma_head'] : NULL;
        $health_assessment->mc_injuretrauma_spinal = isset($data['mc_injuretrauma_spinal']) ? $data['mc_injuretrauma_spinal'] : NULL;
        $health_assessment->mc_injuretrauma_torso = isset($data['mc_injuretrauma_torso']) ? $data['mc_injuretrauma_torso'] : NULL;
        $health_assessment->mc_injuretrauma_leftleg = isset($data['mc_injuretrauma_leftleg']) ? $data['mc_injuretrauma_leftleg'] : NULL;
        $health_assessment->mc_injuretrauma_rightleg = isset($data['mc_injuretrauma_rightleg']) ? $data['mc_injuretrauma_rightleg'] : NULL;
        $health_assessment->mc_injuretrauma_leftarm = isset($data['mc_injuretrauma_leftarm']) ? $data['mc_injuretrauma_leftarm'] : NULL;
        $health_assessment->mc_injuretrauma_rightarm = isset($data['mc_injuretrauma_rightarm']) ? $data['mc_injuretrauma_rightarm'] : NULL;
     
        $health_assessment->mc_injuretrauma_head_spec = isset($data['mc_injuretrauma_head_spec']) ? $data['mc_injuretrauma_head_spec'] : NULL;
        $health_assessment->mc_injuretrauma_spinal_spec = isset($data['mc_injuretrauma_spinal_spec']) ? $data['mc_injuretrauma_spinal_spec'] : NULL;
        $health_assessment->mc_injuretrauma_torso_spec = isset($data['mc_injuretrauma_torso_spec']) ? $data['mc_injuretrauma_torso_spec'] : NULL;
        $health_assessment->mc_injuretrauma_leftleg_spec = isset($data['mc_injuretrauma_leftleg_spec']) ? $data['mc_injuretrauma_leftleg_spec'] : NULL;
        $health_assessment->mc_injuretrauma_rightleg_spec = isset($data['mc_injuretrauma_rightleg_spec']) ? $data['mc_injuretrauma_rightleg_spec'] : NULL;
        $health_assessment->mc_injuretrauma_leftarm_spec = isset($data['mc_injuretrauma_leftarm_spec']) ? $data['mc_injuretrauma_leftarm_spec'] : NULL;
        $health_assessment->mc_injuretrauma_rightarm_spec = isset($data['mc_injuretrauma_rightarm_spec']) ? $data['mc_injuretrauma_rightarm_spec'] : NULL;

        $health_assessment->mc_skindisease = isset($data['mc_skindisease']) ? $data['mc_skindisease'] : NULL;
        $health_assessment->mc_skindisease_spec = isset($data['mc_skindisease_spec']) ? $data['mc_skindisease_spec'] : NULL;
        $health_assessment->mc_allergies = isset($data['mc_allergies']) ? $data['mc_allergies'] : NULL;
        $health_assessment->mc_allergies_spec = isset($data['mc_allergies_spec']) ? $data['mc_allergies_spec'] : NULL ;
        $health_assessment->mc_injuretrauma = isset($data['mc_injuretrauma']) ? $data['mc_injuretrauma'] : NULL;
        $health_assessment->mc_injuretrauma_spec = isset($data['mc_injuretrauma_spec']) ? $data['mc_injuretrauma_spec'] : NULL;
        $health_assessment->mc_respiratory = isset($data['mc_respiratory']) ? $data['mc_respiratory'] : NULL;
        $health_assessment->mc_respiratory_spec = isset($data['mc_respiratory_spec']) ? $data['mc_respiratory_spec'] : NULL;
        $health_assessment->mc_cardio = isset($data['mc_cardio']) ? $data['mc_cardio'] : NULL;
        $health_assessment->mc_cardio_spec = isset($data['mc_cardio_spec']) ? $data['mc_cardio_spec'] : NULL;
        $health_assessment->mc_endocrine = isset($data['mc_endocrine']) ? $data['mc_endocrine'] : NULL;
        $health_assessment->mc_endocrine_spec = isset($data['mc_endocrine_spec']) ? $data['mc_endocrine_spec'] : NULL;
        $health_assessment->mc_urinary = isset($data['mc_urinary']) ? $data['mc_urinary'] : NULL;
        $health_assessment->mc_urinary_spec = isset($data['mc_urinary_spec']) ? $data['mc_urinary_spec'] : NULL ;
        $health_assessment->mc_reproductive = isset($data['mc_reproductive']) ? $data['mc_reproductive'] : NULL;
        $health_assessment->mc_reproductive_spec = isset($data['mc_reproductive_spec']) ? $data['mc_reproductive_spec'] : NULL;
        $health_assessment->mc_commhearing = isset($data['mc_commhearing']) ? $data['mc_commhearing'] : NULL;
        $health_assessment->mc_commhearing_spec = isset($data['mc_commhearing_spec']) ? $data['mc_commhearing_spec'] : NULL;
        $health_assessment->mc_vision = isset($data['mc_vision']) ? $data['mc_vision'] : NULL;
        $health_assessment->mc_vision_spec = isset($data['mc_vision_spec']) ? $data['mc_vision_spec'] : NULL;
        $health_assessment->mc_oraldental = isset($data['mc_oraldental']) ? $data['mc_oraldental'] : NULL;
        $health_assessment->mc_oraldental_spec = isset($data['mc_oraldental_spec']) ? $data['mc_oraldental_spec'] : NULL;
        $health_assessment->mc_psych = isset($data['mc_psych']) ? $data['mc_psych'] : NULL ;
        $health_assessment->mc_psych_spec = isset($data['mc_psych_spec']) ? $data['mc_psych_spec'] : NULL;
        $health_assessment->mc_physical = isset($data['mc_physical']) ? $data['mc_physical'] : NULL;
        $health_assessment->mc_physical_spec = isset($data['mc_physical_spec']) ? $data['mc_physical_spec'] : NULL;
        $health_assessment->mc_cognitive = isset($data['mc_cognitive']) ? $data['mc_cognitive'] : NULL;
        $health_assessment->mc_cognitive_spec = isset($data['mc_cognitive_spec']) ? $data['mc_cognitive_spec'] : NULL;
        $health_assessment->mc_bowel = isset($data['mc_bowel']) ? $data['mc_bowel'] : NULL;
        $health_assessment->mc_bowel_spec = isset($data['mc_bowel_spec']) ? $data['mc_bowel_spec'] : NULL;
        $health_assessment->mc_bladder = isset($data['mc_bladder']) ? $data['mc_bladder'] : NULL;
        $health_assessment->mc_bladder_spec = isset($data['mc_bladder_spec']) ? $data['mc_bladder_spec'] : NULL;
        $health_assessment->mc_oth_spec = isset($data['mc_oth_spec']) ? $data['mc_oth_spec'] : NULL;

        $health_assessment->medications = isset($data['medications']) ? $data['medications'] : NULL;
        $health_assessment->medications_spec = isset($data['medications_spec']) ? $data['medications_spec'] : NULL;

        $health_assessment->treatment = isset($data['treatment']) ? $data['treatment'] : NULL;
        $health_assessment->treatment_spec = isset($data['treatment_spec']) ? $data['treatment_spec'] : NULL;
        $health_assessment->procedure = isset($data['procedure']) ? $data['procedure'] : NULL;
        $health_assessment->procedure_spec = isset($data['procedure_spec']) ? $data['procedure_spec'] : NULL;
        $health_assessment->counselling = isset($data['counselling']) ? $data['counselling'] : NULL;
        $health_assessment->counselling_spec = isset($data['counselling_spec']) ? $data['counselling_spec'] : NULL;

        $health_assessment->suspensionschool = isset($data['suspensionschool']) ? $data['suspensionschool'] : NULL;
        $health_assessment->reasonsuspension = isset($data['reasonsuspension']) ? $data['reasonsuspension'] : NULL;
        $health_assessment->antisocial = isset($data['antisocial']) ? $data['antisocial'] : NULL;
        $health_assessment->delinquency = isset($data['delinquency']) ? $data['delinquency'] : NULL;
        $health_assessment->violence = isset($data['violence']) ? $data['violence'] : NULL;
        $health_assessment->smoking = isset($data['smoking']) ? $data['smoking'] : NULL;
        $health_assessment->alcohol = isset($data['alcohol']) ? $data['alcohol'] : NULL;
        $health_assessment->substanceabuse = isset($data['substanceabuse']) ? $data['substanceabuse'] : NULL;
        $health_assessment->suicidalthoughts = isset($data['suicidalthoughts']) ? $data['suicidalthoughts'] : NULL;
        $health_assessment->suicidalattempts = isset($data['suicidalattempts']) ? $data['suicidalattempts'] : NULL;

        $health_assessment->breakfast_place = isset($data['breakfast_place']) ? $data['breakfast_place'] : NULL;
        $health_assessment->breakfast_time = isset($data['breakfast_time']) ? $data['breakfast_time'] : NULL;
        $health_assessment->breakfast_type_food = isset($data['breakfast_type_food']) ? $data['breakfast_type_food'] : NULL;
        $health_assessment->lunch_place = isset($data['lunch_place']) ? $data['lunch_place'] : NULL;
        $health_assessment->lunch_time = isset($data['lunch_time']) ? $data['lunch_time'] : NULL;
        $health_assessment->lunch_type_food = isset($data['lunch_type_food']) ? $data['lunch_type_food'] : NULL;
        $health_assessment->snacks_place = isset($data['snacks_place']) ? $data['snacks_place'] : NULL;
        $health_assessment->snacks_time = isset($data['snacks_time']) ? $data['snacks_time'] : NULL;
        $health_assessment->snacks_type_food = isset($data['snacks_type_food']) ? $data['snacks_type_food'] : NULL;
        $health_assessment->dinner_place = isset($data['dinner_place']) ? $data['dinner_place'] : NULL;
        $health_assessment->dinner_time = isset($data['dinner_time']) ? $data['dinner_time'] : NULL;
        $health_assessment->dinner_type_food = isset($data['dinner_type_food']) ? $data['dinner_type_food'] : NULL;

        $health_assessment->malnutrition = isset($data['malnutrition']) ? $data['malnutrition'] : NULL;
        $health_assessment->malnutrition_spec = isset($data['malnutrition_spec']) ? $data['malnutrition_spec'] : NULL;

        $health_assessment->idle_hours = isset($data['idle_hours']) ? $data['idle_hours'] : NULL;
        $health_assessment->active_hours = isset($data['active_hours']) ? $data['active_hours'] : NULL;

        $health_assessment->age_menarche = isset($data['age_menarche']) ? $data['age_menarche'] : NULL;
        $health_assessment->material_menstruation = isset($data['material_menstruation']) ? $data['material_menstruation'] : NULL;

        $health_assessment->family_involvement = isset($data['family_involvement']) ? $data['family_involvement'] : NULL;
        $health_assessment->family_involvement_reason = isset($data['family_involvement_reason']) ? $data['family_involvement_reason'] : NULL;
        $health_assessment->community_involvement = isset($data['community_involvement']) ? $data['community_involvement'] : NULL;
        $health_assessment->community_involvement_reason = isset($data['community_involvement_reason']) ? $data['community_involvement_reason'] : NULL;

        $health_assessment->drr_knowledge = isset($data['drr_knowledge']) ? $data['drr_knowledge'] : NULL;

        if($health_assessment->save())
        {
            return $health_assessment;
        }
        return false;
    }
}
