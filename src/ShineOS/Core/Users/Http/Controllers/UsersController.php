<?php
namespace ShineOS\Core\Users\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\Users\Entities\Users;
use ShineOS\Core\Users\Entities\Contact;
use ShineOS\Core\Users\Entities\MDUsers;
use ShineOS\Core\Users\Entities\FacilityUser;
use Shine\Libraries\FacilityId;
use ShineOS\Core\Users\Entities\Roles;
use ShineOS\Core\Users\Entities\RolesAccess;
use Shine\Libraries\Utils\Lovs;
use Shine\Libraries\Utils;
use ShineOS\Core\Users\Libraries\Salt;
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityCatchmentArea;
use Shine\Libraries\UserHelper;
use Shine\Libraries\IdGenerator;
use Shine\Libraries\FacilityHelper;
use ShineOS\Core\Users\Entities\UserLogs;
use View, Response, Validator, Input, Mail, Session, Redirect, Hash, Cache, Auth, DB;

class UsersController extends Controller {

    protected $moduleName = 'Users';
    protected $modulePath = 'users';
    protected $viewPath = 'users::pages.';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $modules =  Utils::getModules();

        # variables to share to all view
        View::share('modules', $modules);
        View::share('moduleName', $this->moduleName);
        View::share('modulePath', $this->modulePath);
    }

    /**
     * Display a user listing.
     *
     * @return Response
     */
    public function index()
    {

        $data = array();
        //$userFacilityID = Cache::get('facility_details');
        $data['userFacilityID'] = Session::get('facility_details');
        $facilityID = $data['userFacilityID']['facility_id'];

        $data['facility_catchment_brgys'] = Session::get('facility_details')->facilityCatchmentArea->lists('bhs_name','ca_id');
        $data['records'] = Users::with(array('facilityUser' => function($query) use($facilityID) {
                $query->where('facility_id', $facilityID);
        }))->with('facilities')->whereHas('facilities', function ($query1) use($facilityID) {
            $query1->where('facilities.facility_id', $facilityID);
        })->paginate(30); 
        $curUser = Session::get('user_details');

        // get data
        $data['currentID'] = $curUser->user_id;
        $data['currentRole'] = $curUser->facilityUser[0]->role_id; 
        return view($this->viewPath.'admin.userslist')->with($data);
    }

    /**
     * Show the form for creating a new user
     *
     * @return Response
     */
    public function adduser()
    {
        $data = array();
        $data['userFacilityID'] = Session::get('facility_details');
        $data['curUser'] = Session::get('user_details');
        $data['facility_user'] = FacilityUser::where('facility_id',Session::get('facility_details')->facility_id)->where('user_id',$data['curUser']->user_id)->first();
        $data['facility_catchment_brgys'] = Session::get('facility_details')->facilityCatchmentArea->lists('bhs_name','ca_id');
        $data['roles'] = Roles::lists('role_name','role_id')->toArray();

        return view($this->viewPath.'admin.adduser')->with($data);
    }

    /**
     * Store a newly created user in user table(s).
     *
     * @return Response
     */
    public function store() {
        $data = array();
        $data['input'] = Input::all();
        $data['roles'] = Roles::lists('role_name','role_id')->toArray();

        $user = Users::addUser();

        // redirect
        if ($user != false):
            Session::flash('message', 'Successfully created a new record!');
        else:
            $userdata = Users::where('email',Input::get('email'))->first();
            // Session::flash('warning', 'There is an existing email address.');
            Session::flash('email_exists', TRUE);
            //create view - add existing email to the Facility

            return view($this->viewPath.'admin.adduser')->with($data);
        endif;

        return Redirect::to($this->modulePath);
    }
    /**
     * Store additional facility to user
     *
     * @return Response
     */
    public function store_facilityuser() {
        Session::flash('email_exists', FALSE);
        $data = array();
        $data = Input::all();
        $data['roles'] = Roles::where('role_name','!=','Admin')->lists('role_name','role_id')->toArray();

        $userFacilityID = Session::get('facility_details');
        $email = $data['email'];
        $check = Users::checkIfExists($email,$userFacilityID['facility_id']);
        if($check) {
            //exists in facility user
            Session::flash('warning', 'User already connected to this Facility.');
            return view($this->viewPath.'admin.adduser')->with($data);
        } else {
            //insert to facility user
            $userdata = Users::where('email',$email)->first();
            $facilityuser = new FacilityUser();
            $facilityuser->facilityuser_id = IdGenerator::generateId();
            $facilityuser->user_id = $userdata->user_id;
            $facilityuser->facility_id = $userFacilityID['facility_id'];
            $facilityuser->role_id = $data['role'];
            $facilityuser->save();

            Session::flash('message', 'User successfully connected to this Facility.');

        }

        return Redirect::to($this->modulePath);
    }

    /**
     * Display profile of user
     *
     * @param  int  $id
     * @return Response
     */
    public function profile( $id ) { 
        $data = array();

        $curUser = UserHelper::getUserInfo();
        $userFacility = Users::with('facilityUser')->where('user_id', $id)->first();
        // get data
        $data['currentID'] = $curUser->user_id;
        $data['userInfo'] = Users::getRecordById($id); 
        $data['userContact'] = Contact::getRecordById($id); 
        $userMd = MDUsers::getRecordById($id);
        $data['userMd'] = $userMd;
        $data['profile_completeness'] = Users::computeProfileCompleteness($id);
        $data['role'] = Roles::all()->toArray();
        $data['userRole'] = getRoleByFacilityUserID($userFacility->facilityUser[0]->facilityuser_id);
        //dd($data['userRole']);
        // lovs
        $regions = Lovs::getLovs('region');
        $data['regions'] = $regions;
        $provinces = Lovs::getLovs('province');
        $data['provinces'] = $provinces;
        $citymunicipalities = Lovs::getLovs('citymunicipalities');
        $data['citymunicipalities'] = $citymunicipalities;
        
        $data['country'] = nations();
        //LIST OF BARANGAYS FROM CATCHMENT AREA
        $data['facility_catchment_brgys'] = Session::get('facility_details')->facilityCatchmentArea->lists('bhs_name','ca_id');
        $data['facility_user'] = FacilityUser::where('facility_id',Session::get('facility_details')->facility_id)->where('user_id',$id)->first();
        $data['facility_id'] = Session::get('facility_details')->facility_id;
        // dd($data);
        return view($this->viewPath.'userprofile')->with($data);
    }


    public function facilities( $id = 0 )
    {
        $data = array();

        // get data
        $userInfo = DB::table('users')->where('user_id', $id)->first();
        $data['userInfo'] = $userInfo;
        $data['profile_completeness'] = Users::computeProfileCompleteness($id);
        $userFacilityID = DB::table('facility_user')->where('user_id', $id)->first();
        $facilityID = $userFacilityID->facility_id;
        $facility = DB::table('facilities')->where('facility_id', $facilityID)->first();
        $data['facility'] = $facility;
        $facilityContact = DB::table('facility_contact')->where('facility_id', $facilityID)->first();
        $data['facilityContact'] = $facilityContact;

        return view($this->viewPath.'userfacilities')->with($data);
    }

    public function auditTrail( $id = 0 )
    {
        // DB::connection()->enableQueryLog();
        $facility_id = FacilityHelper::facilityInfo()->facility_id;


        $userLogs = UserLogs::with('users')
                    ->whereHas('users', function ($q) use($facility_id) {
                        $q->where('facility_id', $facility_id);
                        $q->whereHas('facilityUser', function ($query) use($facility_id)
                        {   $query->where('facility_id', $facility_id); });
                    })
                    ->orderBy('id','DESC')
                    ->get();

        $profile_completeness = Users::computeProfileCompleteness($id);
        $userInfo = Users::getRecordById($id);
        //dd($userLogs);
        // $queries = DB::getQueryLog();
        // dd($queries, $userLogs, $facility_id);
        // dd($userLogs);
        return view($this->viewPath.'userlogs',compact('userLogs','userInfo','profile_completeness'));
    }

    public function access( $id = 0 )
    {
        $data = array();

        // get all user data
        $userInfo = Users::getRecordById($id);
        $data['userInfo'] = $userInfo;
        $userContact = Contact::getRecordById($id);
        $data['userContact'] = $userContact;
        $userMd = MDUsers::getRecordById($id);
        $data['userMd'] = $userMd;
        $data['profile_completeness'] = Users::computeProfileCompleteness($id);
        $data['role'] = Roles::all()->toArray();

        return view($this->viewPath.'userrole')->with($data);
    }

    /**
     * Update User Password
     *
     * @return Response
     */
    public function changeUserPassword ( $id = 0 )
    {

        $currentPassword = Input::get('currentPassword');
        $newPassword = Input::get('newPassword');
        $confirmPassword = Input::get('confirmPassword');

        // get user via user_id
        $user = Users::getRecordById($id);
        $checkCurrentPassword = Hash::make('baltarejos12@gmail.com'.$user->salt);

        if (Hash::check($currentPassword.$user->salt, $user->password)) {
            if ( $newPassword != $confirmPassword ) {
                Session::flash('warning', 'You have entered mismatched passwords.');
                return Redirect::to($this->modulePath.'/'.$id);
            } else {
                $salt = Salt::generateRandomSalt(10);
                $newPassword = Hash::make($newPassword.$salt);

                Users::updateUserPassword($id, $newPassword, $salt);

                Session::flash('message', 'Password successfully updated!');
                return Redirect::to($this->modulePath.'/'.$id);
            }
        } else { // old password did not match
            Session::flash('warning', 'Current password did not match. Please try again.');
            return Redirect::to($this->modulePath.'/'.$id);
        }
    }

    /**
     * Update User Information
     *
     * @return Response
     */
    public function updateInfo( $id = 0 )
    {
        // dd(Input::all());
        // ben: will convert to eloquent and move these DB codes to model
        DB::table('users')
            ->where('user_id', $id)
            ->update(array(
                'last_name' => Input::get('last_name'),
                'first_name' => Input::get('first_name'),
                'middle_name' => Input::get('middle_name'),
                'suffix' => Input::get('suffix_name'),
                'gender' => Input::get('usergender'),
                'birth_date' => date("Y-m-d", strtotime(Input::get('birth_date'))),
                'updated_at' => date('Y-m-d H:i:s')
            ) );

        DB::table('user_contact')
            ->where('user_id', $id)
            ->update(array(
                'phone' => Input::get('phone'),
                'mobile' => Input::get('mobile'),
                'house_no' => Input::get('house_no'),
                'ward_no' => Input::get('ward_no'),
                'district' => Input::get('district'),
                'building_name' => Input::get('building_name'),
                'street_name' => Input::get('street_name'),
                'village' => Input::get('village'),
                'region' => Input::get('region'),
                'province' => Input::get('province'),
                'city' => Input::get('city'),
                'barangay' => Input::get('brgy'),
                'zip' => Input::get('zip'),
                'country' => Input::get('country'),
                'updated_at' => date('Y-m-d H:i:s')
            ) );



        // redirect
        Session::flash('message', 'Successfully updated User Information!');
        return Redirect::to($this->modulePath.'/'.$id);
    }

    /**
     * Update User Information
     *
     * @return Response
     */
    public function updateSettings( $id = 0 )
    {
        // ben: will convert to eloquent and move these DB codes to model
        DB::table('users')
            ->where('user_id', $id)
            ->update(array(
                'prescription_header' => Input::get('prescription_header'),
                'qrcode' => Input::get('qrcode'),
                'updated_at' => date('Y-m-d H:i:s')
            ) );

        // redirect
        Session::flash('message', 'Successfully updated User Settings!');
        return Redirect::to($this->modulePath.'/'.$id);
    }

    /**
     * Update User Background
     *
     * @return Response
     */
    public function updateBackground($id)
    {
        //check if this exist
        $userMD = DB::table('user_md')
            ->where('user_id', $id)->first();

        if($userMD) {
            DB::table('user_md')
            ->where('user_id', $id)
            ->update(array(
                'professional_titles' => Input::get('professional_titles'),
                'profession' => Input::get('profession'),
                'professional_license_number' => Input::get('professional_license_number'),
                'tin' => Input::get('tin'),
                's2' => Input::get('s2'),
                'ptr' => Input::get('ptr'),
                'med_school' => Input::get('med_school'),
                'residency_trn_inst' => Input::get('residency_trn_inst'),
                'residency_grad_yr' => Input::get('residency_grad_yr'),
                'updated_at' => date('Y-m-d H:i:s')
            ) );
        } else {
            $md = new MDUsers();

                $md->usermd_id = IdGenerator::generateId();
                $md->user_id= $id;
                $md->professional_titles= Input::get('professional_titles');
                $md->profession= Input::get('profession');
                $md->professional_license_number= Input::get('professional_license_number');
                $md->tin= Input::get('tin');
                $md->s2= Input::get('s2');
                $md->ptr= Input::get('ptr');
                $md->med_school= Input::get('med_school');
                $md->residency_trn_inst= Input::get('residency_trn_inst');
                $md->residency_grad_yr= Input::get('residency_grad_yr');

            $md->save();

        }

        // redirect
        Session::flash('message', 'Successfully updated User Background!');
        return Redirect::to($this->modulePath.'/'.$id);
    }

    /**
     * Change Profile Pic Form
     *
     * @return Response
     */
    public function changeprofilepic ( $id = 0 )
    {
        $data = array();

        // get data
        $userInfo = Users::getRecordById($id);
        $data['userInfo'] = $userInfo;
        $userContact = Contact::getRecordById($id);
        $data['userContact'] = $userContact;
        $userMd = MDUsers::getRecordById($id);
        $data['userMd'] = $userMd;
        $data['profile_completeness'] = Users::computeProfileCompleteness($id);

        return view($this->viewPath.'profile_picture')->with($data);
    }

    public function changeprofilepic_update ( $id = 0 )
    {
        $data = array();
        $file = array('profile_picture' => Input::file('profile_picture'));
        $rules = array('profile_picture' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
        $validator = Validator::make($file, $rules);

        if ($validator->fails()) {
            return Redirect::to("users/changeprofilepic/{$id}")->withInput()->withErrors($validator);
        }
        else {
            // checking file is valid.
            if (Input::file('profile_picture')->isValid()) {
                $destinationPath = 'public/uploads/profile_picture'; // upload path
                $extension = Input::file('profile_picture')->getClientOriginalExtension();
                $fileName = "profile_".rand(11111,99999).'_'.date('YmdHis').'.'.$extension;
                $originalName = Input::file('profile_picture')->getClientOriginalName();
                Input::file('profile_picture')->move($destinationPath, $fileName);


                // update profile picture
                Users::updateProfilePicture($id, $fileName);

                Session::flash('message', 'Your profile picture has been successfully added.');
                return Redirect::to("users/changeprofilepic/{$id}");
            }
            else {
                // sending back with error message.
                Session::flash('warning', 'uploaded file is not valid');
                return Redirect::to("users/changeprofilepic/{$id}");
            }
        }
    }

    /**
     * "Delete" User
     *
     * @return Response
     */
    public function deleteUser( $id = 0 )
    {
        //Users::deleteUser($id);
        Users::where('user_id', $id)->delete();

        // redirect
        Session::flash('message', 'Successfully Deleted User!');
        return Redirect::to($this->modulePath);
    }

    /**
     * Disable User
     *
     * @return Response
     */
    public function disableUser( $id = 0 )
    {
        $facilityID=Session::get('facility_details')->facility_id;
        $user = Users::with(array('facilityUser' => function($query) use($facilityID) {
                $query->where('facility_id', $facilityID);
        }))->where('user_id',$id)->first();

        if ( $user->facilityUser[0]->role_id <= 3) {
            Session::flash('warning', 'You cannot disable this account.');
            return Redirect::to($this->modulePath.'/'.$id);
        } else {
            Users::disableUser($id);

            Session::flash('message', 'Successfully disabled the selected user!');
            return Redirect::to($this->modulePath);
        }
    }

    /**
     * Enable User
     *
     * @return Response
     */
    public function enableUser( $id = 0 )
    {
        $user = Users::getRecordById($id); 
        Users::enableUser($id);

        Session::flash('message', 'Successfully enabled the selected user!');
        return Redirect::to($this->modulePath);
    }

    public function saveRole ($id)
    {
        $facility_id = Session::get('facility_details')->facility_id;

        $input_role = Input::get('role');
        $count = FacilityUser::where('facility_id', $facility_id)->where('role_id',$input_role)->count();
        // dd($count);
        if(($input_role==1 OR $input_role==3) AND $count>=1) {

            Session::flash('warning', 'You cannot add one more user account for this role.');
        } else {
            $role = FacilityUser::where('user_id', $id)->first();
            $role->role_id = $input_role;
            $role->save();

            $user = Users::with('facilities','facilityUser','mdUsers')
                    ->where('user_id', $id)
                    ->first();
            Session::put('user_details', $user);
            Session::flash('message', 'Successfully changed user role, please logout to take effect the changes.');
        }
        
        return Redirect::to($this->modulePath.'/'.$id);
    }

    /**
     * Update User's Catchment Area
     *
     * @return Response
     */
    public function updateCatchmentArea($user_id,$facility_id)
    {      
        $input = Input::all();
        $message = 'Failed to update data!';
        if(array_key_exists('ca_id', $input)) {
            $facility_user = FacilityUser::where('user_id',$user_id)->where('facility_id',$facility_id)->first();
            $facility_user->catchment_area_id = $input['ca_id'];
            $save = $facility_user->save();

            if($save) {
                $message = 'Successfully updated barangay area!';
            }
        }

        Session::flash('message', $message);
        return Redirect::to($this->modulePath.'/'.$user_id);
    }

}
