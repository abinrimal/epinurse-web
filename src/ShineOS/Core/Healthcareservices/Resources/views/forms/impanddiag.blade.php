<?php $ctr_FINDX = 0; ?>
<?php
if(empty($disposition_record->disposition)) { $read = NULL; }
else { $read = 'disabled'; }
$hide = 'hidden';

//special for Editing
if(isset($facility->ekey) AND $facility->ekey != NULL) {
    $read = NULL;
}
?>

<fieldset>
    <legend>Impressions and Diagnosis</legend>
        <div class="impressionDiagnosis col-sm-12">
            <table class="col-sm-12">
                <tbody class="appendHere">
                @if ($diagnosis_record != NULL)
                        <tr class="impanddiag">
                            <td class='col-sm-11'>
                                <table class="col-sm-12">
                                    <tr class="diagnosisInput" id="diagnosisInput">
                                       <td class='col-sm-3' valign="top"> <label class="control-label"> Diagnosis </label> </td>
                                       <td class='col-sm-8'>
                                           @foreach($diagnosis_record as $krecord => $vrecord)
                                           {!! Form::hidden('impanddiag[update][diagnosis_id][]', $vrecord->diagnosis_id) !!}
                                           @endforeach
                                           <select id="diagnosisEntry" name="impanddiag[update][diagnosislist_id][]" class="diagnosis_input select2 form-control required"  multiple="multiple" style="width: 100%" required="required" data-toggle="popover" data-trigger="manual" data-placement="left" title="Invalid Diagnosis Entry" data-content="You have too many diagnosis entries. When choosing FINAL Diagnosis, you only need one final diagnosis. Please delete the others and try again.">
                                               @foreach($diagnosis_record as $krecord => $vrecord)
                                            <option value="{{ $vrecord->diagnosislist_id }}" selected>{{ $vrecord->diagnosislist_id }}</option>
                                           @endforeach
                                            </select>
                                       </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    <tr>
                        <td colspan="2"> <legend></legend> </td>
                    </tr>
                    <tr class="trtype">
                        <td class='col-sm-11'>
                            <table class="col-sm-12">
                                <tbody>
                                    <tr>
                        <td class='col-sm-3' valign="top"> <label class="control-label"> Diagnosis Type </label> </td>
                        <td class='col-sm-8'>
                            <?php if($healthcareData->healthcareservicetype_id == 'FOLLO') { ?>
                            <div class="btn-group toggler" data-toggle="buttons">
    <label class="btn btn-default required @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'WODIA') active @endif {{$read}}">
    <i class="fa fa-check"></i> <input type="radio" name="impanddiag[update][type][]" required class="diagnosisType" autocomplete="off" value="WODIA" @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'WODIA') checked='checked' @endif> Working
    </label>
    <label class="btn btn-default required @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'FINDX') active @endif {{$read}}">
    <i class="fa fa-check"></i> <input type="radio" name="impanddiag[update][type][]" required class="diagnosisType" autocomplete="off" value="FINDX" @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'HOME') checked='checked' @endif> Final
    </label>
    </div>
                            
                            <?php } elseif($healthcareData->healthcareservicetype_id == 'ADMIN') { ?>
                            <div class="btn-group toggler" data-toggle="buttons">
    <label class="diagnosisType btn btn-default required @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'ADMDX') active @endif {{$read}}">
    <i class="fa fa-check"></i> <input type="radio" name="impanddiag[update][type][]" required class="" autocomplete="off" value="ADMDX" @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'ADMDX') checked='checked' @endif> Admitting
    </label>
    <label class="diagnosisType btn btn-default required @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'FINDX') active @endif {{$read}}">
    <i class="fa fa-check"></i> <input type="radio" name="impanddiag[update][type][]" required class="" autocomplete="off" value="FINDX" @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'HOME') checked='checked' @endif> Final
    </label>
    </div>
                            
                            <?php } else { ?>
                            <div class="btn-group toggler" data-toggle="buttons">
    <label class="diagnosisType btn btn-default required @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'CLIDI') active @endif {{$read}}">
    <i class="fa fa-check"></i> <input type="radio" name="impanddiag[update][type][]" required class="" autocomplete="off" value="CLIDI" @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'CLIDI') checked='checked' @endif> Clinical
    </label>
    <label class="diagnosisType btn btn-default required @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'WODIA') active @endif {{$read}}">
    <i class="fa fa-check"></i> <input type="radio" name="impanddiag[update][type][]" required class="" autocomplete="off" value="WODIA" @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'WODIA') checked='checked' @endif> Working
    </label>
    <label class="diagnosisType btn btn-default required @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'FINDX') active @endif {{$read}}">
    <i class="fa fa-check"></i> <input type="radio" name="impanddiag[update][type][]" required class="" autocomplete="off" value="FINDX" @if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type == 'HOME') checked='checked' @endif> Final
    </label>
    </div>
                            
                            <?php } ?>
                        </td>
                    </tr>
                    
                    <tr class="trnotes">
                        <td class='col-sm-3' valign="top"> <label class="control-label"> Diagnosis Notes </label> </td>
                        <td class='col-sm-8' style="padding-top:5px;">
                            {!! Form::textarea('impanddiag[update][notes][]', $vrecord->diagnosis_notes, ['class' => 'form-control noresize', 'placeholder' => 'Diagnosis notes', 'cols'=>'10', 'rows'=>'5', $read]) !!}
                        </td>
                    </tr>
                    </tbody>
                    </table>
                    </td>
                    </tr>
                @else
                    <?php $krecord = 0; ?>
                    <tr class="impanddiag" id="added{{ $krecord }}">
                            <td class='col-sm-10'>
                                {!! Form::hidden('impanddiag[insert][diagnosis_id][]', '') !!}
                                <table class="col-sm-12">
                                    <tr class="diagnosisInput" id="diagnosisInput{{ $krecord }}">
                                       <td class='col-sm-3' valign="top"> <label class="control-label"> Diagnosis </label> </td>
                                       <td class='col-sm-8'>
                                           <select id="diagnosisEntry" name="impanddiag[insert][diagnosislist_id][]" class="diagnosis_input select2 form-control required"  multiple="multiple" style="width: 100%" required="required" data-toggle="popover" data-trigger="manual" data-placement="left" title="Invalid Diagnosis Entry" data-content="You have too many diagnosis entries. When choosing FINAL Diagnosis, you only need one final diagnosis. Please delete the others and try again.">
                                            </select>
                                       </td>
                                    </tr>
                                    <tr class="trtype">
                                        <td class='col-sm-3' valign="top"> <label class="control-label"> Diagnosis Type </label> </td>
                                        <td class='col-sm-8'>
                                            <?php if($healthcareData->healthcareservicetype_id == 'FOLLO') { ?>
                                            <div class="btn-group toggler" data-toggle="buttons">
    <label class="diagnosisType btn btn-default required {{$read}}">
        <i class="fa fa-check"></i> <input type="radio" name="impanddiag[insert][type][]" required class="" autocomplete="off" value="WODIA"> Working
    </label>
    <label class="diagnosisType btn btn-default required {{$read}}">
        <i class="fa fa-check"></i> <input type="radio" name="impanddiag[insert][type][]" required class="" autocomplete="off" value="FINDX"> Final
    </label>
    </div>
                                            <?php } elseif($healthcareData->healthcareservicetype_id == 'ADMIN') { ?>
                                            <div class="btn-group toggler" data-toggle="buttons">
    <label class="diagnosisType btn btn-default required {{$read}}">
        <i class="fa fa-check"></i> <input type="radio" name="impanddiag[insert][type][]" required class="" autocomplete="off" value="ADMDX"> Admitting
    </label>
    <label class="diagnosisType btn btn-default required {{$read}}">
        <i class="fa fa-check"></i> <input type="radio" name="impanddiag[insert][type][]" required class="" autocomplete="off" value="FINDX"> Final
    </label>
    </div>

                                            <?php } else { ?>
                                            <div class="btn-group toggler" data-toggle="buttons">
    <label class="diagnosisType btn btn-default required {{$read}}">
        <i class="fa fa-check"></i> <input type="radio" name="impanddiag[insert][type][]" required class="" autocomplete="off" value="CLIDI"> Clinical
    </label>
    <label class="diagnosisType btn btn-default required {{$read}}">
        <i class="fa fa-check"></i> <input type="radio" name="impanddiag[insert][type][]" required class="" autocomplete="off" value="WODIA"> Working
    </label>
    <label class="diagnosisType btn btn-default required {{$read}}">
        <i class="fa fa-check"></i> <input type="radio" name="impanddiag[insert][type][]" required class="" autocomplete="off" value="FINDX"> Final
    </label>
    </div>
                                            
                                            <?php } ?>

                                        </td>
                                    </tr>
                                    
                                    <tr class="trnotes">
                                       <td class='col-sm-3' valign="top" style="padding-top:5px;"> <label class="control-label"> Diagnosis Notes </label> </td>
                                       <td class='col-sm-8' style="padding-top:5px;">
                                            {!! Form::textarea('impanddiag[insert][notes][]', '', ['class' => 'form-control noresize', 'placeholder' => 'Diagnosis notes', 'cols'=>'10', 'rows'=>'5', $read]) !!}
                                       </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"> <legend></legend> </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                @endif
                </tbody>
            </table>
        </div>
</fieldset>

@if(isset($vrecord->diagnosis_type) AND $vrecord->diagnosis_type=='FINDX')
    @foreach($diagnosis_record as $kdiag => $vdiag)
       @if(empty($vdiag->diagnosis_i_c_d10) == FALSE)
            {!! Form::hidden('impanddiag[ctr_diagnosis_i_c_d10]', '1') !!}
            <fieldset id="FinalDiagnosis">
                <legend>ICD10 for Final Diagnosis</legend>
                <div class="col-sm-12">
                    <label class="col-sm-3 control-label"> ICD10 CODE </label>
                    <div class="col-sm-8">
                        <select class="form-control required" id="diag_subsubcat" name="impanddiag[icd10][subsubcat]" <?php echo $read; ?>>
                            @if( isset($vdiag->diagnosis_i_c_d10[0]->icd10_code) AND $vdiag->diagnosis_i_c_d10[0]->icd10_code != NULL )
                                <option value="{{ $vdiag->diagnosis_i_c_d10[0]->icd10_code }}" selected> {{ $philicd10_codes[$vdiag->diagnosis_i_c_d10[0]->icd10_code] }}</option>
                            @else
                                <option value="" selected> </option>
                            @endif
                        </select>
                    </div>
                </div>
            </fieldset>
        @else
            {!! Form::hidden('impanddiag[ctr_diagnosis_i_c_d10]', '0') !!}
            <?php $hide = ''; ?>
        @endif
    @endforeach
@else
    {!! Form::hidden('impanddiag[ctr_diagnosis_i_c_d10]', '0') !!}
@endif

<fieldset id="FinalDiagnosis" class="{{ $hide }}">
    <legend>ICD10 for Final Diagnosis</legend>
    <div class="col-sm-12">
        <label class="col-sm-3 control-label"> ICD10 CODE </label>
        <div class="col-sm-8">
            <select class="form-control" id="diag_subsubcat" name="impanddiag[icd10][subsubcat]" <?php echo $read; ?>>
            </select>
        </div>
    </div>
</fieldset>

<br clear="all" />