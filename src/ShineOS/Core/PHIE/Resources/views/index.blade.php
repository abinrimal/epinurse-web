@extends('phie::layouts.master')
@section('heads')
    <link type="text/css" rel="stylesheet" media="all" href="<?php print shineos_includes_url(); ?>css/install/install.css"/>
    <style type="text/css">
        #infoText {
            border-radius: 3px;
            padding-right: 35px;
            padding: 15px;
            margin: 10px;
            border: 1px solid transparent;
            box-sizing: border-box;
            display: block;
        }
        .shineos-ui-progress-percent {
            color:rgba(0,0,0,0,.35);
            font-size:36px;
        }
        .inner {
            min-height: 150px;
        }
        h5 {
            line-height:1.4;
        }
    </style>
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
        <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">PHilippine Health Information Exchange</h3>
            </div>
            <hr />
            <div class="box-body">
                <div class="col-md-5 col-md-offset-1">
                    <div class="small-box bg-gray panels">
                        <div class="small-box-header">PHIE Lite Submission</div>
                        <div class="inner">
                            <h5 class="height80">Your EMR data is automatically submitted everyday to ensure your Philhealth claims are processed. You can see the status below by clicking on <kbd>View details</kbd>. <p class="hidden">If you need to manually submit your EMR data to PHIE, click <kbd>Submit Data</kbd> button. Make sure all fields required by PHIE is filled-up. Only completed data will be send to PHIE.</p></h5>
                        </div>
                        <a href="#" class="small-box-footer lead lead2 hidden" id="phiebox-link" style="padding: 15px 0;">
                            <i class="fa fa-cloud-upload fa-lg"></i> Submit Data
                        </a>

                        <div class="small-box-footer shineos_install_progress hidden" id="phiebox" style="display: none">
                            <div class="shineos-ui-progress">
                                <div class="shineos-ui-progress-info"><?php _e("Submitting data to PHIE Lite"); ?></div>
                                <div class="shineos-ui-progress-bar" style="width: 0%;"></div>
                                <span class="shineos-ui-progress-percent">0%</span>
                            </div>
                            <div id="installinfo"></div>
                            <div style="margin-top:5px;"><span id="timein"></span><span id="timeout"></span></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="small-box bg-gray">
                        <div class="small-box-header">Submission History</div>
                        <div class="inner">
                            <div class="table-responsive">
                                <table class="table dataTable">
                                  @if(isset($synclog))
                                      <thead>
                                          <tr>
                                            <th>Submission Date</th>
                                            <th>Submission Details</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                      <?php $thisdate = NULL; //dd($synclog); ?>
                                      @foreach($synclog as $fid=>$sync)
                                      <?php 
                                      if($sync['phiesync']) {
                                        $phiesync = $sync['phiesync']; //dd($sync); ?>
                                        
                                            <!-- Let us just show one row per submission date -->
                                            @if(date('Y-m-d', strtotime($phiesync->created_at)) == $thisdate AND $fid == $curfid)

                                            @else
                                                <?php
                                                    $username = getUserFullNameByUserID($phiesync->user_id);
                                                    $thisdate = date('Y-m-d', strtotime($phiesync->created_at));
                                                ?>
                                                <tr>
                                                    <td>
                                                        <h6> <b>{{ date('M. d, Y', strtotime($phiesync->created_at)) }}</b><br />{{ date('h:i:s A', strtotime($phiesync->created_at)) }}<br />
                                                        Facility: {{ getFacilityNameByFacID($phiesync->facility_id) }}
                                                        </h6>
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="{{ url('phie/showDetails/'.$phiesync->facility_id.'/'.$thisdate) }}" class="btn btn-block btn-default btn-sm" data-toggle="modal" data-target="#PHIE_json_response"> View details </a>
                                                        Submitted: {{ $phiesync->counter }} tables
                                                    </td>

                                                </tr>
                                                <?php 
                                                    $thisdate =  date('Y-m-d', strtotime($phiesync->created_at)); 
                                                    $curfid = $fid;
                                                    ?>
                                            @endif
                                        
                                      <?php } ?>
                                      @endforeach
                                      </tbody>
                                  @else
                                    <thead>
                                        <tr>
                                            <td colspan="2"> No data to display </td>
                                        </tr>
                                    </thead>
                                  @endif
                                </table>
                            </div>
                        </div>
                    </div>
                    <br clear="all" />
                </div>
                <div class="col-md-5">
                    <div class="small-box bg-orange panels">
                        <div class="small-box-header">Philhealth Assignment List</div>
                        <div class="inner">
                            <h5 class="height80">You can download your PHIC Masterlist for the current year. Choose a year and click on <kbd>Get Masterlist</kbd> to start.<br />&nbsp;</h5>
                            <select id="masteryear" class="select2-P form-text" style="width:100%;" />
                                <option></option>
                                <?php
                                    //let us generate the years
                                    $thisyear = date('Y');
                                    for($y = 2016; $y <= $thisyear; $y++) {
                                        echo "<option value=".$y.">".$y."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <a href="#" class="small-box-footer lead lead2 hidden" id="masterbox-link" style="padding: 15px 0;">
                            <i class="glyphicon glyphicon-save-file fa-lg"></i> Get Masterlist
                        </a>
                        <a href="#" class="small-box-footer lead lead2" id="masterbox-linkk" style="padding: 15px 0;">
                            <i class="glyphicon glyphicon-exclamation-sign fa-lg"></i> COMING SOON!
                        </a>

                        <div class="small-box-footer shineos_install_progress" id="masterbox" style="display: none">
                            <div class="shineos-ui-progress">
                                <div class="shineos-ui-progress-info"><?php _e("Accesing PHIE Lite"); ?></div>
                                <div class="shineos-ui-progress-bar" style="width: 0%;"></div>
                                <span class="shineos-ui-progress-percent">0%</span>
                            </div>
                            <div id="installinfo"></div>
                            <div style="margin-top:5px;"><span id="timein"></span><span id="timeout"></span></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="small-box bg-orange">
                        <div class="small-box-header">Downloaded Masterlists</div>
                        <div class="inner">
                            <div class="table-responsive">
                                <table class="table table-striped dataTable">
                                  <thead>
                                  <tr>
                                    <th>Date</th>
                                    <th>Masterlists</th>
                                  </tr>
                                  </thead>
                                  <tbody>

                                  </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 hidden">
                    <div class="small-box bg-red panels">
                        <div class="small-box-header">Philhealth SAP</div>
                        <div class="inner">
                            <h5 class="height80">Download a copy of your PHIC SAP for the quarter & year. Please ensure you have receive a notification before downloading. Choose quarter and year then click on Get to start.</h5>
                            <select id="sap-q-yr" class="select2-S form-text" style="width:100%;" />
                                <option></option>
                                <?php
                                    //let us generate the years
                                    $thisyear = date('Y');
                                    for($y = 2016; $y <= $thisyear; $y++) {
                                        //let us generate the quarters
                                        if($y == $thisyear) {
                                            $mon = date('n');
                                            if($mon >= 3) {
                                                echo "<option value=1:".$y.">Q1 ".$y."</option>";
                                            }
                                            if($mon >= 6) {
                                                echo "<option value=2:".$y.">Q2 ".$y."</option>";
                                            }
                                            if($mon >= 9) {
                                                echo "<option value=3:".$y.">Q3 ".$y."</option>";
                                            }
                                            if($mon == 12) {
                                                echo "<option value=4:".$y.">Q4 ".$y."</option>";
                                            }
                                        } else {
                                            echo "<option value=1:".$y.">Q1 ".$y."</option>";
                                            echo "<option value=2:".$y.">Q2 ".$y."</option>";
                                            echo "<option value=3:".$y.">Q3 ".$y."</option>";
                                            echo "<option value=4:".$y.">Q4 ".$y."</option>";
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <a href="#" class="small-box-footer lead lead2" id="sapbox-link" style="padding: 15px 0;">
                            <i class="fa fa-file-text fa-lg"></i> Get eSAP
                        </a>

                        <div class="small-box-footer shineos_install_progress" id="sapbox" style="display: none">
                            <div class="shineos-ui-progress">
                                <div class="shineos-ui-progress-info"><?php _e("Accessing PHIE Lite"); ?></div>
                                <div class="shineos-ui-progress-bar" style="width: 0%;"></div>
                                <span class="shineos-ui-progress-percent">0%</span>
                            </div>
                            <div id="installinfo"></div>
                            <div style="margin-top:5px;"><span id="timein"></span><span id="timeout"></span></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="small-box bg-red">
                        <div class="small-box-header">Downloaded SAPs</div>
                        <div class="inner">
                            <div class="table-responsive">
                                <table class="table table-striped dataTable">
                                  <thead>
                                  <tr>
                                    <th>Date</th>
                                    <th>SAP Details</th>
                                  </tr>
                                  </thead>
                                  <tbody>

                                  </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.box-body -->
            <div class="clearfix"></div>
            <div class="box-footer clearfix">
              <!-- <a class="btn btn-block btn-social btn-bitbucket" id="SyncPHIE">
                <i class="fa fa-cloud-upload"></i> Submit Data to PHIE
              </a> -->
                      <!-- <input type="submit" value="Submit Data to PHIE" name="SyncPHIE" id="SyncPHIE" class="btn btn-sm btn-info btn-flat pull-right"> -->
            </div>
            <!-- /.box-footer -->

          </div>

          <!-- /.box -->
        </div>
    </div>
@stop


@section('scripts')
<div class="modal fade" id="PHIE_json_response" tabindex="-1" role="dialog" aria-labelledby="myInfoModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>

<script>
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
function getTime() {
    time = new Date();

    var hh = addZero(time.getHours());
    var mm = addZero(time.getMinutes());
    var ss = addZero(time.getSeconds());

    thistime = hh + ":" + mm + ":" + ss;

    return thistime;
}

$(function(){
    $('#phiebox-link').on('click', function(e){
        e.preventDefault();
        //$('.box-body h5').fadeOut();
        $('#phiebox-link').fadeOut();

        xhr = $.ajax({
            url: '<?php site_url(); ?>phie/submitDatatoPHIE',
            type: "GET",
            beforeSend: function() {
              timein = getTime();
              $("#phiebox #timein").text("Start: "+timein);
              data = 'Submission is in progress. Please wait...';
              installprogress('phiebox');
            },
            error: function(data, response) {
                if(response == 'abort'){
                    xhr.abort();
                    xhr = null;
                }
                installprogressStop('phiebox');
            },
            success: function(data) {
              console.log(data);
              installprogress('phiebox');
            }
        });
    });

    $('#masterbox-link').on('click', function(e){
        e.preventDefault();
        var datee = $('#masteryear').val();
        $('#masterbox-link').fadeOut();

        xhr = $.ajax({
            url: '<?php site_url(); ?>phie/getMaster/'+datee,
            type: "GET",
            beforeSend: function() {
              timein = getTime();
              $('#masterbox #timein').text("Start: "+timein);
              data = 'Submission is in progress. Please wait...';
              installprogress('masterbox');
            },
            error: function(data, response) {
                if(response == 'abort'){
                    xhr.abort();
                    xhr = null;
                }
                installprogressStop('masterbox');
            },
            success: function(data) {
              console.log(data);
              installprogress('masterbox');
            }
        });
    });

    $('#sapbox-link').on('click', function(e){
        e.preventDefault();
        var qyear = $('#sap-q-yr').val();
        $('#sapbox-link').fadeOut();

        xhr = $.ajax({
            url: '<?php site_url(); ?>phie/getSAP/'+qyear,
            type: "GET",
            beforeSend: function() {
              timein = getTime();
              $('#sapbox #timein').text("Start: "+timein);
              data = 'Submission is in progress. Please wait...';
              installprogress('sapbox');
            },
            error: function(data, response) {
                if(response == 'abort'){
                    xhr.abort();
                    xhr = null;
                }
                installprogressStop('sapbox');
            },
            success: function(data) {
              console.log(data);
              installprogress('sapbox');
            }
        });
    });

    $(".select2-P").select2({
        placeholder: "-- Choose Year --",
        minimumResultsForSearch: Infinity,
        allowClear: true
    });
    $(".select2-S").select2({
        placeholder: "-- Choose Qtr & Year --",
        minimumResultsForSearch: Infinity,
        allowClear: true
    });
    $("#PHIE_json_response").on("show.bs.modal", function(e) {
        $(this).find(".modal-content").html("");
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
});

installprogressStopped = false;

function installprogressStop(thisbox) {
    var holder = $('#'+thisbox),
        bar = $(".shineos-ui-progress-bar", holder),
        percent = $(".shineos-ui-progress-percent", holder);
    bar.width('0%');
    percent.html('0%');
    percent.fadeOut();
    bar.fadeOut();
    installprogressStopped = true;
    //$('#'+thisbox+"-link").fadeIn();
}

function installprogress(thisbox, reset)
{
    var cnt = 5;
    if(thisbox == 'phiebox') {
        cnt = 26;
    }
    if (installprogressStopped) {
        installprogressStopped = false;
        return false;
    }

    var holder = $('#'+thisbox),
        bar = $(".shineos-ui-progress-bar", holder),
        percent = $(".shineos-ui-progress-percent", holder),
        reset = typeof reset === 'undefined' ? true : reset;

    if (reset === true) {
        bar.width('0%');
        percent.html('0%');
        holder.fadeIn();
    }

    <?php // $log_file_url = userfiles_url().'phie_logs/phie_'.$facilityid.'_log.txt'; ?>
    <?php $log_file_url = ''; ?>
    <?php // $logg_file_url = userfiles_url().'phie_logs/phiee_'.$facilityid.'_log.txt'; ?>
    <?php $logg_file_url = ''; ?>
    $.get('<?php print $log_file_url ?>', function (data) {
        var data = data.replace(/\r/g, '');
        var arr = data.split('\n'),
            l = arr.length,
            i = 0,
            lastline = arr[l-2],
            percentage = Math.round( ((l-2) / cnt) * 100);
        bar[0].style.width = percentage + '%';
        percent.html(percentage + '%');

        if(lastline == 'done') {
            percent.html('100%');
            $("#"+thisbox +" #installinfo").html("<h4>PHIE Lite process complete.</h4><p><a href='{{ url('/phie') }}' class='text-white'> Refresh the page </a></p>");
            timeout = getTime();
            $("#"+thisbox +" #timeout").text(" - Finish: "+timeout);
            installprogressStop(thisbox);
        } else if(lastline.indexOf('Error') == 0) {
            $("#"+thisbox +" #installinfo").html(lastline);
            timeout = getTime();
            $("#"+thisbox +" #timeout").text(" - Finish: "+timeout);
            installprogressStop(thisbox);
        } else {

            if( lastline.indexOf('Processing') == 0 ) {
                $.get('<?php print $logg_file_url ?>', function (dataa) {
                    var dataa = dataa.replace(/\r/g, '');
                    var arrr = dataa.split('\n'),
                        ll = arrr.length,
                        ii = 0,
                        lastt = arrr[ll-2],
                        ppercent = Math.round( ((ll-1) / 25) * 100);

                    if(lastt == 'ok') {
                    } else if( lastt.indexOf('Error') == 0 ) {
                        installprogressStop(thisbox);
                        $("#"+thisbox +" #installinfo").html("<h4 class='text-danger'>"+ lastt +"</h4><p>Please contact SHINEOS+ Support.</p>");
                        timeout = getTime();
                        $("#"+thisbox +" #timeout").text(" - Finish: "+timeout);
                    } else {
                        $("#"+thisbox +" #installinfo").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> "+lastt);
                        setTimeout(function(){
                            installprogress(thisbox, true);
                        }, 1500);
                    }
                });
            } else {

                if(lastline == 'done_w_err') {
                    $.get('<?php print $logg_file_url ?>', function (dataa) {
                    var dataa = dataa.replace(/\r/g, '');
                    var arrr = dataa.split('\n'),
                        ll = arrr.length,
                        ii = 0,
                        lastt = arrr[ll-2],
                        ppercent = Math.round( ((ll-1) / 25) * 100);

                        $("#"+thisbox +" #installinfo").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> " + lastt);
                        timeout = getTime();
                        $("#"+thisbox +" #timeout").text(" - Finish: "+timeout);
                        installprogressStop(thisbox);
                    });
                } else {
                    $("#"+thisbox +" #installinfo").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> "+lastline);
                    setTimeout(function(){
                        installprogress(thisbox, true);
                    }, 1500);
                }
            }
        }

    });
}
</script>
@stop
