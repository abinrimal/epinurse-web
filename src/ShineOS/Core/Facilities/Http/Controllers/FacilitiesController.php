<?php

namespace ShineOS\Core\Facilities\Http\Controllers;

use Shine\Plugin;
use Shine\Libraries\Utils;
use Shine\Libraries\Utils\Lovs;
use Illuminate\Routing\Controller;
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Facilities\Entities\FacilityContact;
use ShineOS\Core\Facilities\Entities\FacilityCatchmentArea;
use ShineOS\Core\Facilities\Entities\FacilityWorkforce;
use ShineOS\Core\Facilities\Entities\DOHFacilityCode;
use ShineOS\Core\Users\Entities\Users;
use Shine\Libraries\IdGenerator;
use View, DB, Response, Validator, Input, Mail, Cache, Session, Redirect, Hash, Auth, Schema;

class FacilitiesController extends Controller {

    protected $moduleName = 'Facilities';
    protected $modulePath = 'facilities';
    protected $viewPath = 'facilities::';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $modules =  Utils::getModules();

        # variables to share to all view
        View::share('modules', $modules);
        View::share('moduleName', $this->moduleName);
        View::share('modulePath', $this->modulePath);
    }

    //UNCOMMENT TO ENABLE

    /**
     * Display a user listing.
     *
     * @return Response
     */
    public function facilities()
    {
        //get this facility info from session
        //$thisfacility = json_decode(Cache::get('facility_details'));
        $thisfacility = json_decode(Session::get('facility_details'));
        $thisUser = Session::get('_global_user');
        $roles = Session::get('roles');
        $facilities = Facilities::getCurrentFacility($thisfacility->facility_id);

        //get all available plugins in the patients plugin folder
        //later on will use options DB to get only activated plugins
        $patientPluginDir = plugins_path()."/";
        $plugins = directoryFiles($patientPluginDir);
        asort($plugins);
        $plugs = array(); 

        $geodatabarangay = array();
        $file = plugins_path() . 'ShineLab_Geodata' . DS . 'GeodataModel.php';
        if(file_exists($file))
        {
            $location = 'Plugins\ShineLab_Geodata\GeodataModel';
            $geodatamodel = new $location;
            $geodatabarangay = $geodatamodel::where('facility_id',$thisfacility->facility_id)->get()->toArray();
        } 
        if($thisfacility->enabled_plugins){
            foreach($plugins as $k=>$plugin) {
                if(strpos($plugin, ".")===false) {
                    //check if config.php exists
                    if(file_exists(plugins_path().$plugin.'/config.php')){
                        include(plugins_path().$plugin.'/config.php');

                        //check if this folder is enabled
                        if(json_decode($thisfacility->enabled_plugins) != NULL) {
                            if(in_array($plugin_id, json_decode($thisfacility->enabled_plugins)) OR $roles['role_name'] == 'Developer'){
                                //get only plugins for this module
                                if($plugin_module == 'facilities'){
                                    if($plugin_table == 'plugintable') {
                                        $pdata = Plugin::where('primary_key_value',$thisfacility->facility_id)->first();
                                    } else {
                                        if (Schema::hasTable($plugin_table)) {
                                            if($plugin_title == 'Environmental Information') {
                                                $pdata = DB::table($plugin_table)->where($plugin_primaryKey, $thisfacility->facility_id) 
                                                ->first();
                                            }
                                            else {
                                                $pdata = DB::table($plugin_table)->where($plugin_primaryKey, $thisfacility->facility_id)->first();
                                            }
                                        }
                                    }
                                    $plugs[$k]['plugin_location'] = $plugin_location;
                                    $plugs[$k]['folder'] = $plugin_folder;
                                    $plugs[$k]['parent'] = $plugin_module;
                                    $plugs[$k]['title'] = $plugin_title;
                                    $plugs[$k]['plugin'] = $plugin_id;
                                    $plugs[$k]['pdata'] = $pdata;
                                }
                            }
                        }
                    }
                }
            }
        }
        $data = array();
        $data['brgys'] = $data['geodatabarangay'] = NULL;
        $brgys = getPatientBrgys(); //get all Brgys from facility city
        if($brgys){
            $data['brgys'] = $brgys;
            $data['geodatabarangay'] = $geodatabarangay;
        }

        // $getArrFacilityAllBrgyCode = Lovs::getArrFacilityAllBrgyCode($thisfacility->facility_id);
        // if(is_null($getArrFacilityAllBrgyCode)) {
        //     $data['facilitybrgycodes'] = json_decode($facilities->catchment_area);
        // } else {
        //     $data['facilitybrgycodes'] = $getArrFacilityAllBrgyCode;
        // }

        $data['plugs'] = $plugs;
        $data['currentFacility'] = $thisfacility;
        $facilityContact = FacilityContact::getContact($thisfacility->facility_id);
        $data['facilityContact'] = $facilityContact;
        $data['doh'] = $thisfacility->DOH_facility_code;
        $data['userInfo'] = $thisUser;
        $data['profile_completeness'] = Users::computeProfileCompleteness($thisUser->user_id);

        $data['equipments'] = Lovs::getEnumsByType('EQUIPMENT_TYPE');
        $data['specialties'] = Lovs::getEnumsByType('SPECIALTY_TYPE');
        $data['services'] = Lovs::getEnumsByType('SERVICES_TYPE');
        $data['faciltyType'] = Utils::faciltyType();
        $data['country'] = nations();

        // dd($data);
        return view($this->viewPath.'facilities')->with($data);
    }

    /**
     * Add facilities
     *
     * @return Response
     */
    public function add_facility () {
        $data = array();
        return view($this->viewPath.'index')->with($data);
    }

    /**
     * Update Facility Info
     *
     * @return Response
     */
    public function updatefacilityinfo ( $facility_id = 0 ) {
        $data = array();

        Facilities::updateFacilityById($facility_id);

        //let us update session values
        //$facility = json_encode(Facilities::getCurrentFacility($facility_id));
        //Session::put('_global_facility_info', $facility);
        Session::put('facility_details', Facilities::getCurrentFacility($facility_id));

        // redirect
        Session::flash('message', 'Successfully updated Facility Information!');
        return Redirect::to($this->modulePath);
    }

    /**
     * Update Facility Contact
     *
     * @return Response
     */
    public function updatefacilitycontact ( $facility_id = 0 ) {
        $data = array();

        FacilityContact::updateContactByFacilityId($facility_id);

        Session::put('facility_details', Facilities::getCurrentFacility($facility_id));

        // redirect
        Session::flash('message', 'Successfully updated Facility Contact!');
        return Redirect::to($this->modulePath.'#tab_2');
    }

    /**
     * Update Facility Specialization
     *
     * @return Response
     */
    public function updatespecialization ( $facility_id = 0 ) {
        $data = array();
        Facilities::updateFacilitySpecializationById($facility_id);

        //let us update session values
        Session::put('facility_details', Facilities::getCurrentFacility($facility_id));

        // redirect
        Session::flash('message', 'Successfully updated Facility Specialization!');
        return Redirect::to($this->modulePath.'#tab_3');
    }

    public function updateworkforce ( $facility_id = 0 ) {
        $data = array();
        FacilityWorkforce::updateFacilityWorkforceById($facility_id);

        //let us update session values
        Session::put('facility_details', Facilities::getCurrentFacility($facility_id));

        // redirect
        Session::flash('message', 'Successfully updated Facility Workforce!');
        return Redirect::to($this->modulePath).'#tab_4';
    }


    public function updatecatchmentarea ( $facility_id = 0 ) {
        $data = Input::all();
        $array = array();
        $user_id = Session::get('user_details')->user_id;
        if(array_key_exists('ca_barangayname', $data) AND array_key_exists('ca_barangay', $data)) {
            $facility_catchment = new FacilityCatchmentArea();
            $facility_catchment->ca_id = IdGenerator::generateId();
            $facility_catchment->facility_id = $facility_id;
            $facility_catchment->bhs_name = $data['ca_barangayname'];
            $facility_catchment->bhs_brgy_code = $data['ca_barangay'];
            $facility_catchment->createdby_userid =  $user_id; 
            $facility_catchment->save();

            $facility = Facilities::where('facility_id',$facility_id)->first();
            $facility->catchment_area = NULL;
            $facility->save();

            $facility_data = Facilities::getCurrentFacility($facility_id);

            Session::put('facility_details', $facility_data);
            $message = 'Successfully updated facility catchment area';
        } else {
            $message = 'Failed to add/update catchment area';
        }
        Session::flash('message', $message);
        return Redirect::to($this->modulePath.'#tab_5');
        
    }
    //if array is associative, array(array(key=>value))
    private function isAssoc(array $arr) {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }


    public function auditTrail()
    {
        return view($this->viewPath.'userlogs');
    }

    public function permissions()
    {
        return view($this->viewPath.'userpermissions');
    }


    private function print_this( $object = array(), $title = '' ) {
        echo "<hr><h2>{$title}</h2><pre>";
        print_r($object);
        echo "</pre>";
    }

    /**
     * Change Profile Pic Form
     *
     * @return Response
     */
    public function changelogo ( $id = 0 )
    {
        $data = array();

        $thisUser = Session::get('_global_user');
        $data['userInfo'] = $thisUser;
        // get data
        $facInfo = Facilities::getCurrentFacility($id);
        $data['facilityInfo'] = $facInfo;
        $data['profile_completeness'] = Users::computeProfileCompleteness($thisUser->user_id);

        return view($this->viewPath.'logo_picture')->with($data);
    }

    public function changelogo_update ( $id = 0 )
    {
        $data = array();
        $file = array('profile_picture' => Input::file('profile_picture'));
        $rules = array('profile_picture' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
        $validator = Validator::make($file, $rules);

        if ($validator->fails()) {
            return Redirect::to("facilities/changelogo/{$id}")->withInput()->withErrors($validator);
        }
        else {
            // checking file is valid.
            if (Input::file('profile_picture')->isValid()) {
                $destinationPath = 'public/uploads/profile_picture'; // upload path
                $extension = Input::file('profile_picture')->getClientOriginalExtension();
                $fileName = "facility_".rand(11111,99999).'_'.date('YmdHis').'.'.$extension;
                $originalName = Input::file('profile_picture')->getClientOriginalName();
                Input::file('profile_picture')->move($destinationPath, $fileName);


                // update profile picture
                Facilities::updateLogo($id, $fileName);

                Session::flash('message', 'Your logo has been successfully added.');
                return Redirect::to("facilities/changelogo/{$id}");
            }
            else {
                // sending back with error message.
                Session::flash('warning', 'uploaded file is not valid');
                return Redirect::to("facilities/changelogo/{$id}");
            }
        }
    }

    public function updateBrgyCatchmentArea($ca_id)
    { 
        $save=NULL;
        $type = 'error';
        $message = 'Failed to delete BHS name!'; 
        if($ca_id) {
            $check = FacilityUser::where('catchment_area_id',$ca_id)->count();
            if($check<=0) {
                $ca = FacilityCatchmentArea::find($ca_id); 
                if($ca) { $save = $ca->delete(); }
                if($save) {
                    $facility_data = Facilities::getCurrentFacility(Session::get('facility_details')->facility_id);
                    Session::put('facility_details', $facility_data);
                    $type = 'message';
                    $message = 'Successfully deleted barangay area!';
                }
            } else {
                $message = 'Failed to delete BHS, found '.$check.' user.'; 
            }
        }

        Session::flash($type, $message);
        return Redirect::to($this->modulePath);
    }

}
