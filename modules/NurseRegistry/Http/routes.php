<?php

Route::group(['prefix' => 'nurseregistry', 'namespace' => 'Modules\NurseRegistry\Http\Controllers', 'middleware' => 'auth.access:nurseregistry'], function()
{
	Route::get('/', 'NurseRegistryController@index');
	Route::get('/new', 'NurseRegistryController@new');
	Route::post('/save', 'NurseRegistryController@save');
	Route::get('/update/{id}', 'NurseRegistryController@update');
	Route::get('/delete/{id}', 'NurseRegistryController@delete');
});