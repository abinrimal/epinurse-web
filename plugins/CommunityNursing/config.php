<?php
$plugin_id = 'CommunityNursing';                       //plugin ID
$plugin_module = 'healthcareservices';          //module owner
$plugin_location = 'dropdown';                  //UI location where plugin will be accessible
$plugin_primaryKey = 'communitynursing_id';        //primary_key used to find data
$plugin_table = 'communitynursing_service';            //plugintable default; table_name custom table
$plugin_tabs_child = array('addservice','vitals','communitynursing_plugin'); //,
$plugin_type = 'consultation';
$plugin_age = '0-99';
$plugin_gender = "all";
//plugin maximum role to access this plugin (MANDATORY VALUE)
$plugin_role = 6;

$plugin_relationship = array();
$plugin_folder = 'CommunityNursing'; //module owner
$plugin_title = 'Community Nursing';            //plugin title
$plugin_description = 'Community Nursing';
$plugin_version = '1.0';
$plugin_developer = 'mediXserve';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2018";

$plugin_tabs = [
    'addservice' => 'Basic Information',
    'complaints' => 'Complaints',
    'impanddiag' => 'Impressions & Diagnosis',
    'disposition' => 'Disposition',
    'medicalorders' => 'Medical Orders',
    'vitals' => 'Vitals & Physical',
    'communitynursing_plugin' => 'Community Nursing'
];

$plugin_tabs_models = [
    'complaints' => 'GeneralConsultation',
    'disposition' => 'Disposition',
    'medicalorders' => 'MedicalOrder',
    'vitals' => 'VitalsPhysical',
    'communitynursing_plugin' => 'CommunityNursingModel'
];