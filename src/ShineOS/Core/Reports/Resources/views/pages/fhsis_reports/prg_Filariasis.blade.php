@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<table class="table table-striped table-bordered table-report table-responsive">
    <thead>
        <tr><th colspan="4">FILARIASIS</th></tr>
        <tr><th></th><th>Male</th><th>Female</th><th>Total</th></tr>
    </thead>
    <tbody>
        <tr>
            <td>Case Examined</td>
            <td> {{ $data['CASEEXAM_M'] }} </td>
            <td> {{ $data['CASEEXAM_F'] }} </td>
            <td> {{ $data['CASEEXAM_M'] + $data['CASEEXAM_F'] }} </td>
        </tr>
        <tr>
            <td>Cases Positive</td>
            <td> {{ $data['CASEPOS_M'] }} </td>
            <td> {{ $data['CASEPOS_F'] }} </td>
            <td> {{ $data['CASEPOS_M'] + $data['CASEPOS_F'] }} </td>
        </tr>
        <tr>
            <td>Microfilaria in the slides found positive</td>
            <td> {{ $data['MF_M'] }} </td>
            <td> {{ $data['MF_F'] }} </td>
            <td> {{ $data['MF_M'] + $data['MF_F'] }} </td>
        </tr>
        <tr>
            <td>Persons given Multi-Drug Administration</td>
            <td> {{ $data['MDA_M'] }} </td>
            <td> {{ $data['MDA_F'] }} </td>
            <td> {{ $data['MDA_M'] + $data['MDA_F'] }} </td>
        </tr>
        <tr>
            <td>Adenolymphangitis Cases</td>
            <td> {{ $data['ADENOCASE_M'] }} </td>
            <td> {{ $data['ADENOCASE_F'] }} </td>
            <td> {{ $data['ADENOCASE_M'] + $data['ADENOCASE_F'] }} </td>
        </tr>
        <tr>
            <td>No Case</td>
            <td> {{ $data['NOCASE_M'] }} </td>
            <td> {{ $data['NOCASE_F'] }} </td>
            <td> {{ $data['NOCASE_M'] + $data['NOCASE_F'] }} </td>
        </tr>
    </tbody>
</table>
@yield('modal-footer')