<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('diagnosis_view')==TRUE) {
            $sql = 'DROP VIEW `diagnosis_view`';
            DB::connection()->getPdo()->exec($sql);
        }
        if (Schema::hasTable('healthcare_view')==TRUE) {
            $sql = 'DROP VIEW `healthcare_view`';
            DB::connection()->getPdo()->exec($sql);
        }
        if (Schema::hasTable('patients_view')==TRUE) {
            $sql = 'DROP VIEW `patients_view`';
            DB::connection()->getPdo()->exec($sql);
        }
        if (Schema::hasTable('referrals_view')==TRUE) {
            $sql = 'DROP VIEW `referrals_view`';
            DB::connection()->getPdo()->exec($sql);
        }
        if (Schema::hasTable('referral_messages_view')==TRUE) {
            $sql = 'DROP VIEW `referral_messages_view`';
            DB::connection()->getPdo()->exec($sql);
        }
        if (Schema::hasTable('visits_view')==TRUE) {
            $sql = 'DROP VIEW `visits_view`';
            DB::connection()->getPdo()->exec($sql);
        }
        
        $path = database_path();
        DB::unprepared(file_get_contents($path.DIRECTORY_SEPARATOR.'shineosviews.sql')); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
