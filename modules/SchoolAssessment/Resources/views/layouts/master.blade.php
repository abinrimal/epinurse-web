@extends('layout.master')

@section('heads')
  {!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.css') !!}
  {!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.skinFlat.css') !!}
  {!! HTML::style('public/dist/css/flaticon.css') !!} 

  <style type="text/css">
    ul.schoolassessment li a {
      color: #555;
      font-size: 12px;
    }
    legend {
      padding-top: 10px;
    }

    .checkbox+.checkbox, .radio+.radio {
         margin-top: 0px; 
    }
    .school-subheader {
      margin: 10px 0 20px 0;
      font-size: 15px;
      padding-bottom: 9px;
      border-bottom: 1px solid #eee;
    }
    .eca-activities {
      text-align: center;
    }
    img.epinurse-icon {
      height: 50px;
    }
    .eca-oth {
      padding-top: 10px;
    }
  </style>
@stop

@section('page-header')
  <section class="content-header">
    <!-- edit by RJBS -->
    <h1>
      <i class="fa fa-university"></i>
      School Assessment
    </h1>
    <!-- end edit RJBS -->
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> <a href="{{ url('/schoolassessment') }}"> School Assessment </a></li>
    </ol>
  </section>
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
	    @if (Session::has('flash_message'))
	        <div class="alert {{Session::get('flash_type') }}">{{ Session::get('flash_message') }}</div>
	    @endif
   	</div>
	   	
		@yield('page-content')
	</div>
@stop

@section('plugin_jsscripts')
  {!! HTML::script('public/dist/plugins/ionslider/ion.rangeSlider.min.js') !!}
  <script type="text/javascript">
    $(function() {
      $('.nutrition_provided_food_radio').on('ifClicked', function(event){ 
        if(this.value == 1){
          $('#meal-spec').removeClass('hidden');
        }
        else{
          $('#meal-spec').addClass('hidden');
        };
      });
      $(".ionrange").ionRangeSlider({
        type: "single",
        grid: true,
        min: 1,
        max: 5
      });
    });
  </script>
@stop