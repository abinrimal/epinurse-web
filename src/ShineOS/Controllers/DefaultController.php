<?php

namespace ShineOS\Controllers;

use ShineOS\View;
use ShineOS\Install;
use Shine\Libraries\UserHelper;
use Shine\Libraries\EmailHelper;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use ShineOS\Core\Reports\Entities\M2;
use ShineOS\Core\PHIE\Http\Controllers\PHIEController;
use \Cache;
use \Event;
use \Session;
use \Redirect;
use \URL;
use \Hash;
use \File;

use Module;

class DefaultController extends Controller {

    public function __construct($app = null) {
        if (!is_object($this->app)){

            if (is_object($app)){
                $this->app = $app;
            } else {
                $this->app = shineos();
            }
        }

    }

    public function index() {
        $is_installed = shineos_is_installed();

        if (!$is_installed){
            $installer = new InstallController($this->app);
            return Redirect::to('install');
        }
        return Redirect::to('dashboard/');
    }

    public $return_data = false;
    public $page_url = false;
    public $create_new_page = false;
    public $render_this_url = false;
    public $isolate_by_html_id = false;
    public $functions = array();
    public $page = array();
    public $params = array();
    public $vars = array();
    public $app;


    public function track() {

        $facility = Session::get('facility_details');
        $curUser = Session::get('user_details');
        $sessID = Session::get('_token');

        $browser = getBrowser();

        if(isset($curUser)) {
            if(isset($curUser->user_id)) {
                $user = $curUser->user_id;
                $userType = 'provider';
            }
            if(isset($curUser->patient_id)) {
                $user = $curUser->patient_id;
                $userType = 'patient';
            }
        } else {
            $user = $userType = NULL;
        }
        $data['user_id'] = $user;
        if($facility) {
            $data['facility_id'] = $facid = $facility->facility_id;
        } else {
            $data['facility_id'] = $facid = "portal";
        }

        $json = array(
            'session_token' => $sessID,
            'browser' => $_POST['browser'],
            'browserversion' => $_POST['browserversion'],
            'device' => $_POST['device'],
            'devicemodel' => $_POST['devicemodel'],
            'devicevendor' => $_POST['devicevendor'],
            'os' => $_POST['os'],
            'osversion' => $_POST['osversion'],
            'osengine' => $_POST['osengine'],
            'osarchitecture' => $_POST['osarchitecture'],
            'date' => date('Y-m-d H:i:s'),
            'url' => stripslashes($_POST['url']),
            'facility_id' => $facid,
            'user_id' => $user,
            'user_type' => $userType,
            'element' => $_POST['element'],
            'element_type' => $_POST['type'],
            'element_name' => $_POST['name'],
            'element_id' => $_POST['ID'],
            'element_label' => $_POST['label'],
            'curvalue' => $_POST['curvalue'],
            'action' => $_POST['action'],
            'msg' => 'test'
        );
        $text = json_encode($json, JSON_UNESCAPED_SLASHES);

        $data['session_id'] = $sessID;
        $data['datetime'] = date('Y-m-d H:i:s');
        $data['json'] = $text;

        //DB::connection('mysql2')->table('tracker')->insert($data);

        $results['result'] = "OK";
        echo json_encode($json);

    }

    /*ON MAINTENANCE MODE*/
    public function maintenance() {
        $view = SHINEOS_PATH . 'Views/maintenance.php';

        $layout = new View($view);
        $layout = $layout->__toString();
        return $layout;
    }

    public function makeHash($text) {
        $salt = getenv('SECRET_KEY');
        $hash = Hash::make($text.$salt);

        echo $hash;
    }

    public function batchPHIE($facid=NULL) {
        $PHIEController = new PHIEController();
        if($facid) {
            $PHIEController->batchPHIE($facid);
        } else {
            $PHIEController->batchPHIE();
        }
    }

    public function update($type) {
        if(is_file(SHINEOS_USERFILES_FOLDER_NAME . '/updates/update.php')) {
            include_once(SHINEOS_USERFILES_FOLDER_NAME . '/updates/update.php');
            $done = doupdate($type);

            if($done) {
                File::delete(SHINEOS_USERFILES_FOLDER_NAME . '/updates/update.php');

                return Redirect::to('/login');
            }
        } else {
            echo "<h3>No updates available</h3>";
        }
    }

    public function testemail()
    {
        $data = array(
                'toUser_name' => "Romel John Santos",
                'toUser_email' => "rjsantos@ateneo.edu",
                'subj' => "Broadcast: Test Email Message on ".date("M d, Y"),
                'msg' => "<p>Quisque elit urna, rhoncus at nunc in, aliquam bibendum nunc. Proin ipsum ex, gravida hendrerit iaculis eget, malesuada at nunc! In pretium risus eu enim finibus auctor? Mauris eget nunc placerat, efficitur mi condimentum, fringilla enim? Aliquam erat volutpat. Cras tempus odio eu velit fermentum, et tempor justo vehicula. In non lectus et sem ultricies commodo? Etiam id egestas diam. Nam vel metus leo.</p>
                <p>Phasellus egestas elementum sollicitudin. Vivamus at rutrum leo, nec ornare urna. Curabitur viverra augue et eros gravida, non hendrerit quam vehicula. Quisque id malesuada lacus. Integer sed cursus tortor. Phasellus finibus efficitur mollis. Vestibulum accumsan tellus arcu, quis congue odio pretium ut. Proin molestie fermentum ex! Duis at ipsum id leo imperdiet scelerisque. Aenean mollis interdum eleifend. Nam id enim lacinia, feugiat urna vitae, luctus dui! Pellentesque vehicula diam sed placerat dapibus.</p>",
                'appointment_datetime' => date("M d, Y"),
                'fromUser_name' => "SHINE OS+ Administrator",
                'fromFacility' => "SHINE OS+ Test Clinic"
                );
        EmailHelper::SendReminderMessage($data);
        return view('reminders::emails.remind', $data);
    }

    public function testsms()
    {
        $mob = '639154472708';
         $ChikkaSMS = new ChikkaSMS;
         $sendText = $ChikkaSMS->sendText(123456, $mob, 'This is a test SMS from SHINEOS+. '.date("M d, Y"));
    }

    public function geojson()
    {
        $regions = DB::table('lov_region')->get();
        echo "[\n";
        foreach($regions as $n=>$r){
            echo "\t{\n";
            echo "\t\t\"region_code\": \"".$r->region_code."\",\n\t\t\"region_name\": \"".$r->region_short."\",\n";
            $provinces = DB::table('lov_province')->where('region_code',$r->region_code)->get();
            echo "\t\t\"provinces\": [\n";
            foreach($provinces as $m=>$p) {
                echo "\t\t\t{\n";
                echo "\t\t\t\t\"province_code\": \"".$p->province_code."\",\n\t\t\t\t\"province_name\": \"".$p->province_name."\",\n";
                
                $cities = DB::table('lov_citymunicipalities')->where('province_code',$p->province_code)->get();
                echo "\t\t\t\t\"cities\": [\n";
                foreach($cities as $l=>$c) {
                    echo "\t\t\t\t\t{\n";
                    echo "\t\t\t\t\t\t\"city_code\": \"".$c->city_code."\",\n\t\t\t\t\t\t\"city_name\": \"".$c->city_name."\",\n";

                    $brgys = DB::table('lov_barangays')->where('city_code', $c->city_code)->get();
                    echo "\t\t\t\t\t\t\"barangays\": [\n";
                    foreach($brgys as $k=>$b) {
                        echo "\t\t\t\t\t\t\t{\n";
                        echo "\t\t\t\t\t\t\t\t\"barangay_code\": \"".$b->barangay_code."\",\n\t\t\t\t\t\t\t\t\"barangay_name\": \"".$b->barangay_name."\"\n";
                        echo "\t\t\t\t\t\t\t}";
                        if($k < count($brgys)-1){
                            echo ",\n";
                        } else {
                            echo "\n";
                        }
                    }
                    echo "\t\t\t\t\t\t]\n";
                    echo "\t\t\t\t\t}";
                    if($l < count($cities)-1){
                        echo ",\n";
                    } else {
                        echo "\n";
                    }
                }
                echo "\t\t\t\t]\n";
                echo "\t\t\t}";
                if($m < count($provinces)-1){
                    echo ",\n";
                } else {
                    echo "\n";
                }
            }
            echo "\t\t]\n";
            echo "\t}";
            if($n < count($regions)-1){
                echo ",\n";
            } else {
                echo "\n";
            }
        }
        echo "]\n";
    }
}
