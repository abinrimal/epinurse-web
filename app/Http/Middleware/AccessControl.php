<?php
namespace Shine\Http\Middleware;

use Closure, Cache, Session, Auth;

class AccessControl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $module)
    {
        if (Auth::check()) {
            if (Session::has('roles')) {

                // Check if user wants to access his/her own profile
                $route = $request->route();

                $allowupdateusers = [
                    'users/{id}',
                    'users/changepassword/{id}',
                    'users/updateinfo/{id}',
                    'users/updatebackground/{id}',
                    'users/changeprofilepic_update/{id}',
                    'users/updatesettings/{id}'
                ];
                if(in_array($route->uri(), $allowupdateusers))
                {
                    $user = Session::get('user_details');
                    $user_id_logged = $user->user_id;

                    $param = $route->parameters();
                    if($param['id'] == $user_id_logged)
                    {
                        return $next($request);
                    }
                }

                // Proceed as usual if not
                $val = Session::get('roles');
                if ((isset($val['modules'][$module])) OR (isset($val['external_modules'][$module])) OR ($val['role_name'] == 'Developer')) 
                {
                    return $next($request);
                }
                else 
                {
                    Session::flash('warning', 'You are not authorized to access the page.');
                    return redirect('dashboard');
                }
            }
        }
        Auth::logout();
        $request->session()->flush();
        return redirect('logout/111');
    }
}
