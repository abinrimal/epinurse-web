<?php

return [
    'name' => 'School Assessment',
    'icon' => 'fa-university',
    'roles' => '["Doctor Admin","Doctor","Nurse","Midwife","Encoder"]',
    'version' => '1.0',
    'title' => 'School Assessment Module',
    'folder' => 'SchoolAssessment',
    'table' => 'school_assessment',
    'description' => 'EpiNurse School Assessment Module',
    'developer' => 'mediXserve',
    'copy' => '2018',
    'url' => 'epinurse.org'
];