<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMigrationPhilhealth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('patient_philhealthinfo')==TRUE) { 
            Schema::table('patient_philhealthinfo', function (Blueprint $table) {
                $table->string('patient_philhealthinfo_id', 60)->nullable()->change();
                $table->string('provider_account_id', 11)->nullable()->change();
                $table->string('philhealth_id', 60)->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
