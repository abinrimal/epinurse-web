<?php namespace ShineOS\Core\API\Http\Controllers\APIEpiNurse;

use Illuminate\Routing\Controller;
use ShineOS\Core\API\Http\Controllers\APIEpiNurseController;

use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\PatientContacts;
use ShineOS\Core\Patients\Entities\PatientDeathInfo;
use ShineOS\Core\Patients\Entities\PatientEmergencyInfo;

use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser;

use Plugins\MotherAndChild\MotherAndChildModel;

use ShineOS\Core\Users\Entities\Users;
use Illuminate\Http\JsonResponse;
use Shine\Libraries\IdGenerator;


use Request, Auth, Validator;

class MotherAndChildController extends APIEpiNurseController {

	public function insertMotherAndChild($request_data) {
		$data = $request_data['data'];
		$data['facility_id'] = $facility_id = $request_data['facility_id'];
		$data['user_id'] = $user_id = $request_data['user_id'];
		$data['patient_id'] = $patient_id = $request_data['patient_id'];

		$validator = Validator::make($data, [
			"facility_id" => "required|string",
			"user_id" => "required|string",
			"patient_id" => "required|string",
			"uuid" => "required",
			"ownerUuid" => "required",
			"age_of_menarche" => "required",
			"where_first_menarche" => "required",
			"sanitary_materials_used_pads" => "required"
			"sanitary_materials_used_tampon" => "required"
			"sanitary_materials_used_clothes" => "required"
			"sanitary_materials_used_hp" => "required"
			"sanitary_materials_used_mhp" => "required"
			"sanitary_materials_used_none" => "required"
			"how_often_used" => "required",
			"how_dispose_materials" => "required",
			"is_pregnant" => "required",
			"age_first_pregnancy" => "required",
			// "past_obstetric_history_gravida" => "required",
			// "past_obstetric_history_para" => "required",
			// "past_obstetric_history_abortion" => "required",
			// "past_obstetric_history_living" => "required",
			"obstetric_history_gravida" => "required",
			// "obstetric_history_para" => "required",
			"obstetric_history_abortion" => "required",
			// "obstetric_history_living" => "required",
			"period_gestation" => "required",
			"num_antenatal_visits" => "required",
			"antenal_service_anc_check_up" => "required",
			"antenal_service_albendazole" => "required",
			"antenal_service_td_immunization" => "required",
			"antenal_service_irontabs" => "required",
			"antenal_service_couseling" => "required",
			"antenal_service_pmtct" => "required",
			"antenal_service_others" => "required",
			"health_seeking_behaviour" => "required",
			// "medicines" => "required|array|size:0",
			"youngest_baby_months" => "required",
			"gap_between_kids" => "required",
			"place_delivery_youngest" => "required",
			"delivery_complications" => "required",
			"post_natal_pe" => "required",
			"post_natal_counsel_b_f" => "required",
			"post_natal_counsel_f_p" => "required",
			"post_natal_investigations" => "required",
			"post_natal_iron_tablets" => "required",
			"post_natal_others" => "required",
			"pnc_followup_none" => "required",
			"pnc_followup_within24hr" => "required",
			"pnc_followup_at3rd_day" => "required",
			"pnc_followup_at7th_day" => "required",
			"pnc_followup_at28th_day" => "required",
			"pnc_followup_at45th_day" => "required",
			"pnc_followup_others" => "required",
			"food_available_after_delivery" => "required",
			"assessment_food_quality" => "required",
			"assessment_food_frequency" => "required",
			"assessment_food_distribution" => "required",
			// "taking_nutritious_food" => "required",
			"prelactating_feeding" => "required",
			"dietary_restrictions" => "required",
			"had_counseling_family_planning" => "required",
			"used_family_planning_methods" => "required",
			"permanent_vasectomy" => "required",
			"permanent_minilap" => "required",
			"temporary_natural" => "required",
			"temporary_condom" => "required",
			"temporary_depo" => "required",
			"temporary_pills" => "required",
			"temporary_iucd" => "required",
			"temporary_implant" => "required",
			"source_maternity_info" => "required",
			"child_unique_id" => "required",
			"child_name" => "required",
			"child_sex" => "required",
			"date_of_birth_in_ad" => "required",
			"date_of_birth_in_bs" => "required",
			"weight_at_birth" => "required",
			"weight_current" => "required",
			"height_cm" => "required",
			"mid_arm_circumference" => "required",
			"has_congential_anomaly" => "required",
			"health_condition" => "required",
			"had_vit_k_injection" => "required",
			"injury_child_faced_falls" => "required",
			"injury_child_faced_drowning" => "required",
			"injury_child_faced_burnsscald" => "required",
			"injury_child_faced_poisoning" => "required",
			"injury_child_faced_suffocationchokingaspiration" => "required",
			"injury_child_faced_cut_injury" => "required",
			"injury_child_faced_others" => "required",
			"injury_cause_unsafe_home" => "required",
			"injury_cause_no_super_vision" => "required",
			"injury_cause_busy_mother" => "required",
			"injury_cause_slippery_floor" => "required",
			"injury_cause_others" => "required",
			"age_bcg_at_birth" => "required",
			// "remarks_bcg" => "required",
			"age_dpt_hepb_hib6_weeks" => "required",
			"age_dpt_hepb_hib10_weeks" => "required",
			"age_dpt_hepb_hib14_weeks" => "required",
			// "remarks_dpt_hepb_hib" => "required",
			"age_opv6_weeks" => "required",
			"age_opv10_weeks" => "required",
			"age_opv14_weeks" => "required",
			// "remarks_opv" => "required",
			"age_pcv6_weeks" => "required",
			"age_pcv10_weeks" => "required",
			"age_pcv9_months" => "required",
			// "remarks_pcv" => "required",
			"age_ipv14_weeks" => "required",
			// "remarks_ipv" => "required",
			"age_measles_rubella9_months" => "required",
			"age_measles_rubella15_months" => "required",
			// "remarks_measles_rubella" => "required",
			"age_japanese_encephalitis12_months" => "required",
			// "remarks_japanese_encephalitis" => "required",
			"advice_breast_feeding" => "required",
			"advice_dental_hygiene" => "required",
			"advice_toilet_training" => "required",
			"advice_complementary_feeding" => "required",
			"advice_accident_prevention" => "required",
			"md_physical" => "required",
			"md_verbal" => "required",
			"md_social" => "required",
			"md_spiritual_religious" => "required",
			"md_motor" => "required",
			"md_intellectual" => "required",
			"md_emotional" => "required"
        ]);

	    if($validator->fails() == false)
	    {
	    	$facilityUser = $this->getFacilityUser($data['user_id'],$data['facility_id']);
	    	if(!$facilityUser) {
	    		return new JsonResponse(array('status'=>'error', 'message'=>'Facility or User not found!'), 400);
	    	}

	    	$facilityPatientUser = FacilityPatientUser::where('facilityuser_id',$facilityUser->facilityuser_id)->where('patient_id',$patient_id)->first();
	    	if(!$facilityPatientUser) {
	    		return new JsonResponse(array('status'=>'error', 'message'=>'Patient not found!'), 400);
	    	}

	    	$motherandchild = MotherAndChildModel::where('uuid',$data['uuid'])->first();
	    	$message = 'Mother and child assessment has been updated!';
	    	if(!$motherandchild)
	    	{
	    		$message = 'New mother and child assessment has been added!';
		    	// Proceed to saving
		    	$hs_id = IdGenerator::generateId();

		    	// Save new healthcare
		    	$healthcare = $this->insertNewHealthcareService($data,$hs_id,'MotherAndChild');

		    	// Save new mother and child	        
	            $motherandchild =  new MotherAndChildModel;
	            $motherandchild->motherandchild_id = IdGenerator::generateId();
		        $motherandchild->healthcareservice_id = $hs_id;

		        $motherandchild->uuid = isset($data['uuid']) ? $data['uuid'] : NULL;
		        $motherandchild->owner_uuid = isset($data['ownerUuid']) ? $data['ownerUuid'] : NULL;
	    	}


	        $motherandchild->age_menarche = isset($data['age_of_menarche']) && $data['age_of_menarche'] != '' ? $data['age_of_menarche'] : NULL;
	        $motherandchild->stay_menarche = isset($data['where_first_menarche']) && $data['where_first_menarche'] != '' ? $data['where_first_menarche'] : NULL;
	        $motherandchild->stay_menarche_oth = isset($data['where_first_menarche_specify']) ? $data['where_first_menarche_specify'] : NULL;

	        $motherandchild->sanitary_materials_used_pads = isset($data['sanitary_materials_used_pads']) ? $data['sanitary_materials_used_pads'] : NULL;
	        $motherandchild->sanitary_materials_used_tampon = isset($data['sanitary_materials_used_tampon']) ? $data['sanitary_materials_used_tampon'] : NULL;
	        $motherandchild->sanitary_materials_used_clothes = isset($data['sanitary_materials_used_clothes']) ? $data['sanitary_materials_used_clothes'] : NULL;
	        $motherandchild->sanitary_materials_used_hp = isset($data['sanitary_materials_used_hp']) ? $data['sanitary_materials_used_hp'] : NULL;
	        $motherandchild->sanitary_materials_used_mhp = isset($data['sanitary_materials_used_mhp']) ? $data['sanitary_materials_used_mhp'] : NULL;
	        $motherandchild->sanitary_materials_used_none = isset($data['sanitary_materials_used_none']) ? $data['sanitary_materials_used_none'] : NULL;

	        $motherandchild->material_change_frequency = isset($data['how_often_used']) && $data['how_often_used'] != '' ? $data['material_change_frequency'] : NULL;
	        $motherandchild->material_change_frequency_oth = isset($data['how_often_used_specify']) ? $data['how_often_used_specify'] : NULL;
	        $motherandchild->timeliness_reason = isset($data['reason_not_timely']) ? $data['reason_not_timely'] : NULL;
	        $motherandchild->sanitarydisposal = isset($data['how_dispose_materials']) && $data['how_dispose_materials'] != '' ? $data['how_dispose_materials'] : NULL;
	        $motherandchild->sanitarydisposal_oth = isset($data['how_dispose_materials_specify']) ? $data['how_dispose_materials_specify'] : NULL;

	        $motherandchild->is_pregnant = isset($data['is_pregnant']) ? $data['is_pregnant'] : NULL;
	        $motherandchild->age_first_pregnancy = isset($data['age_first_pregnancy']) && $data['age_first_pregnancy'] != '' ? $data['age_first_pregnancy'] : NULL;
	        $motherandchild->past_gravida = isset($data['past_obstetric_history_gravida']) && $data['past_obstetric_history_gravida'] != '' ? $data['past_obstetric_history_gravida'] : NULL;
	        $motherandchild->past_para = isset($data['past_obstetric_history_para']) && $data['past_obstetric_history_para'] != '' ? $data['past_obstetric_history_para'] : NULL;
	        $motherandchild->past_abortion = isset($data['past_obstetric_history_abortion']) && $data['past_obstetric_history_abortion'] != '' ? $data['past_obstetric_history_abortion'] : NULL;
	        $motherandchild->past_living = isset($data['past_obstetric_history_living']) && $data['past_obstetric_history_living'] != '' ? $data['past_obstetric_history_living'] : NULL;
	        $motherandchild->cur_gravida = isset($data['obstetric_history_gravida']) && $data['obstetric_history_gravida'] != '' ? $data['obstetric_history_gravida'] : NULL;
	        $motherandchild->cur_para = isset($data['obstetric_history_para']) && $data['obstetric_history_para'] != '' ? $data['obstetric_history_para'] : NULL;
	        $motherandchild->cur_abortion = isset($data['obstetric_history_abortion']) && $data['obstetric_history_abortion'] != '' ? $data['obstetric_history_abortion'] : NULL;
	        $motherandchild->cur_living = isset($data['obstetric_history_living']) && $data['obstetric_history_living'] != '' ? $data['obstetric_history_living'] : NULL;
	        $motherandchild->period_gestation = isset($data['period_gestation']) && $data['period_gestation'] != '' ? $data['period_gestation'] : NULL;
	        $motherandchild->times_antenatal = isset($data['num_antenatal_visits']) && $data['num_antenatal_visits'] != '' ? $data['num_antenatal_visits'] : NULL;

	        $motherandchild->antenatal_services_anc_check_up = isset($data['antenatal_services_anc_check_up']) ? $data['antenatal_services_anc_check_up'] : NULL;
	        $motherandchild->antenatal_service_albendazole = isset($data['antenatal_service_albendazole']) ? $data['antenatal_service_albendazole'] : NULL;
	        $motherandchild->antenatal_service_td_immunization = isset($data['antenatal_service_td_immunization']) ? $data['antenatal_service_td_immunization'] : NULL;
	        $motherandchild->antenatal_service_irontabs = isset($data['antenatal_service_irontabs']) ? $data['antenatal_service_irontabs'] : NULL;
	        $motherandchild->antenatal_service_pmtct = isset($data['antenatal_service_pmtct']) ? $data['antenatal_service_pmtct'] : NULL;
	        $motherandchild->antenatal_service_others = isset($data['antenatal_service_others']) ? $data['antenatal_service_others'] : NULL;
	        $motherandchild->antenatal_service_specify = isset($data['antenatal_service_specify']) ? $data['antenatal_service_specify'] : NULL;

	        $motherandchild->healthseeking_behavior = isset($data['health_seeking_behaviour']) && $data['health_seeking_behaviour'] != '' ? $data['health_seeking_behaviour'] : NULL;
	        $motherandchild->healthseeking_behavior_oth_spec = isset($data['health_seeking_behaviour_specify']) ? $data['health_seeking_behaviour_specify'] : NULL;
	        
	        $motherandchild->medicines = isset($data['medicines']) ? json_encode($data['medicines']) : NULL;

	        $motherandchild->age_young_months = isset($data['youngest_baby_months']) && $data['youngest_baby_months'] != '' ? $data['youngest_baby_months'] : NULL;
	        $motherandchild->type_of_delivery = isset($data['type_of_delivery']) && $data['type_of_delivery'] != '' ? $data['type_of_delivery'] : NULL;
	        $motherandchild->age_gap_kids = isset($data['gap_between_kids']) && $data['gap_between_kids'] != '' ? $data['gap_between_kids'] : NULL;
	        $motherandchild->delivery_place = isset($data['place_delivery_youngest']) && $data['place_delivery_youngest'] != '' ? $data['place_delivery_youngest'] : NULL;
	        $motherandchild->delivery_place_oth_spec = isset($data['delivery_place_oth_spec']) ? $data['delivery_place_oth_spec'] : NULL;
	        $motherandchild->delivery_complication = isset($data['delivery_complications']) ? $data['delivery_complications'] : NULL;
	        $motherandchild->delivery_complication_spec = isset($data['delivery_complications_specify']) ? $data['delivery_complications_specify'] : NULL;
	        
	        $motherandchild->post_natal_pe = isset($data['post_natal_pe']) ? $data['post_natal_pe'] : NULL;
	        $motherandchild->post_natal_counsel_b_f = isset($data['post_natal_counsel_b_f']) ? $data['post_natal_counsel_b_f'] : NULL;
	        $motherandchild->post_natal_counsel_f_p = isset($data['post_natal_counsel_f_p']) ? $data['post_natal_counsel_f_p'] : NULL;
	        $motherandchild->post_natal_investigations = isset($data['post_natal_investigations']) ? $data['post_natal_investigations'] : NULL;
	        $motherandchild->post_natal_iron_tablets = isset($data['post_natal_iron_tablets']) ? $data['post_natal_iron_tablets'] : NULL;
	        $motherandchild->post_natal_vitamin_a = isset($data['post_natal_vitamin_a']) ? $data['post_natal_vitamin_a'] : NULL;
	        $motherandchild->post_natal_oth = isset($data['post_natal_others']) ? $data['post_natal_others'] : NULL;
	        $motherandchild->post_natal_oth_spec = isset($data['post_natal_specify']) ? $data['post_natal_specify'] : NULL;

	        $motherandchild->pnc_followup_none = isset($data['pnc_followup_none']) ? $data['pnc_followup_none'] : NULL;
	        $motherandchild->pnc_followup_within24hr = isset($data['pnc_followup_within24hr']) ? $data['pnc_followup_within24hr'] : NULL;
	        $motherandchild->pnc_followup_at3rd_day = isset($data['pnc_followup_at3rd_day']) ? $data['pnc_followup_at3rd_day'] : NULL;
	        $motherandchild->pnc_followup_at7th_day = isset($data['pnc_followup_at7th_day']) ? $data['pnc_followup_at7th_day'] : NULL;
	        $motherandchild->pnc_followup_at28th_day = isset($data['pnc_followup_at28th_day']) ? $data['pnc_followup_at28th_day'] : NULL;
			$motherandchild->pnc_followup_at45th_day = isset($data['pnc_followup_at45th_day']) ? $data['pnc_followup_at45th_day'] : NULL;
			$motherandchild->pnc_followup_others = isset($data['pnc_followup_others']) ? $data['pnc_followup_others'] : NULL;
			$motherandchild->pnc_followup_specify = isset($data['pnc_followup_specify']) ? $data['pnc_followup_specify'] : NULL;

	        $motherandchild->food_after_delivery = isset($data['food_available_after_delivery']) ? $data['food_available_after_delivery'] : NULL;
	        $motherandchild->food_after_delivery_quality = isset($data['assessment_food_quality']) ? $data['assessment_food_quality'] : NULL;
	        $motherandchild->food_after_delivery_frequency = isset($data['assessment_food_frequency']) ? $data['assessment_food_frequency'] : NULL;
	        $motherandchild->food_after_delivery_distribution = isset($data['assessment_food_distribution']) ? $data['assessment_food_distribution'] : NULL;
	        // $motherandchild->nutritious_food = isset($data['taking_nutritious_food']) ? $data['taking_nutritious_food'] : NULL;
	        // $motherandchild->nutritious_food_spec = isset($data['taking_nutritious_food_specify']) ? $data['taking_nutritious_food_specify'] : NULL;
	        $motherandchild->pre_lactating = isset($data['prelactating_feeding']) ? $data['prelactating_feeding'] : NULL;
	        $motherandchild->dietary_restriction = isset($data['dietary_restrictions']) ? $data['dietary_restrictions'] : NULL;
	        $motherandchild->dietary_restriction_spec = isset($data['dietary_restriction_spec']) ? $data['dietary_restriction_spec'] : NULL;

	        $motherandchild->fp_counseling = isset($data['had_counseling_family_planning']) ? $data['had_counseling_family_planning'] : NULL;
	        $motherandchild->fp_use = isset($data['used_family_planning_methods']) ? $data['used_family_planning_methods'] : NULL;
	        $motherandchild->permanent_vasectomy = isset($data['permanent_vasectomy']) ? $data['permanent_vasectomy'] : NULL;
	        $motherandchild->permanent_minilap = isset($data['permanent_minilap']) ? $data['permanent_minilap'] : NULL;
	        $motherandchild->temporary_natural = isset($data['temporary_natural']) ? $data['temporary_natural'] : NULL;
	        $motherandchild->temporary_condom = isset($data['temporary_condom']) ? $data['temporary_condom'] : NULL;
	        $motherandchild->temporary_depo = isset($data['temporary_depo']) ? $data['temporary_depo'] : NULL;
	        $motherandchild->temporary_pills = isset($data['temporary_pills']) ? $data['temporary_pills'] : NULL;
	        $motherandchild->temporary_iucd = isset($data['temporary_iucd']) ? $data['temporary_iucd'] : NULL;
	        $motherandchild->temporary_implant = isset($data['temporary_implant']) ? $data['temporary_implant'] : NULL;
	        $motherandchild->fp_info_src = isset($data['source_maternity_info']) && $data['source_maternity_info'] != '' ? $data['source_maternity_info'] : NULL;

	        $motherandchild->child_id = isset($data['child_unique_id']) ? $data['child_unique_id'] : NULL;
	        $motherandchild->child_name = isset($data['child_name']) ? $data['child_name'] : NULL;
	        $motherandchild->child_sex = isset($data['child_sex']) ? $data['child_sex'] : NULL;
	        $motherandchild->child_birthdate_ad = isset($data['date_of_birth_in_ad']) ? $data['date_of_birth_in_ad'] : NULL;
	        $motherandchild->child_birthdate_bs = isset($data['date_of_birth_in_bs']) ? $data['date_of_birth_in_bs'] : NULL;
	        $motherandchild->child_weight_birth = isset($data['weight_at_birth']) && $data['weight_at_birth'] != '' ? $data['weight_at_birth'] : NULL;
	        $motherandchild->child_weight_current = isset($data['child_weight_current']) && $data['child_weight_current'] != '' ? $data['child_weight_current'] : NULL;
	        $motherandchild->child_height = isset($data['weight_current']) && $data['weight_current'] != '' ? $data['weight_current'] : NULL;
	        $motherandchild->mid_arm_circumference = isset($data['mid_arm_circumference']) && $data['mid_arm_circumference'] != '' ? $data['mid_arm_circumference'] : NULL;
	        $motherandchild->congenital_anomaly = isset($data['has_congential_anomaly']) ? $data['has_congential_anomaly'] : NULL;
	        $motherandchild->congenital_anomaly_spec = isset($data['has_congential_anomaly_specify']) ? $data['has_congential_anomaly_specify'] : NULL;
	        $motherandchild->health_condition = isset($data['health_condition']) ? $data['health_condition'] : NULL;
	        $motherandchild->vitamin_k = isset($data['had_vit_k_injection']) ? $data['had_vit_k_injection'] : NULL;

	        $motherandchild->breastfeed_start = isset($data['when_start_breast_feed']) && $data['when_start_breast_feed'] != '' ? $data['when_start_breast_feed'] : NULL;
	        $motherandchild->bf_times = isset($data['how_often_breast_feed']) && $data['how_often_breast_feed'] != '' ? $data['how_often_breast_feed'] : NULL;
	        $motherandchild->exclusive_breastfeeding = isset($data['did_exclusive_breast_feed']) ? $data['did_exclusive_breast_feed'] : NULL;
	        $motherandchild->exclusive_breastfeeding_spec = isset($data['did_exclusive_breast_feed_specify']) ? $data['did_exclusive_breast_feed_specify'] : NULL;
	        $motherandchild->age_stop_bf_months = isset($data['stopped_breast_feeding_months']) ? $data['stopped_breast_feeding_months'] : NULL;
	        $motherandchild->reason_stopbf = isset($data['reason_discontinue_breast_feed']) ? $data['reason_discontinue_breast_feed'] : NULL;
	        $motherandchild->reason_stopbf_spec = isset($data['reason_discontinue_breast_feed_specify']) ? $data['reason_discontinue_breast_feed_specify'] : NULL;
	        $motherandchild->aware_bf = isset($data['aware_breast_feed_prevents_illness']) ? $data['aware_breast_feed_prevents_illness'] : NULL;
	        $motherandchild->solid_feeding = isset($data['started_semi_solid_food']) ? $data['started_semi_solid_food'] : NULL;
	        $motherandchild->food_supplement_rice_pudding = isset($data['food_supplement_rice_pudding']) ? $data['food_supplement_rice_pudding'] : NULL;
	        $motherandchild->food_supplement_jaulo = isset($data['food_supplement_jaulo']) ? $data['food_supplement_jaulo'] : NULL;
	        $motherandchild->food_supplement_lito = isset($data['food_supplement_lito']) ? $data['food_supplement_lito'] : NULL;
	        $motherandchild->food_supplement_cerelac = isset($data['food_supplement_cerelac']) ? $data['food_supplement_cerelac'] : NULL;
	        $motherandchild->food_supplement_oth = isset($data['food_supplement_others']) ? $data['food_supplement_others'] : NULL;
	        $motherandchild->food_supplement_oth_spec = isset($data['food_supplement_specify']) ? $data['food_supplement_specify'] : NULL;
	        $motherandchild->no_food = isset($data['why_no_supplement']) && $data['why_no_supplement'] != '' ? $data['why_no_supplement'] : NULL;
	        $motherandchild->no_food_oth = isset($data['no_food_oth']) ? $data['no_food_oth'] : NULL;
	        $motherandchild->clean_hands_breastfeed = isset($data['wash_hands_breastfeed']) ? $data['wash_hands_breastfeed'] : NULL;
	        $motherandchild->handwashing_facility_breastfeed = isset($data['has_handwash_facility']) ? $data['has_handwash_facility'] : NULL;
	        $motherandchild->child_food = isset($data['detail_food_to_baby']) ? $data['detail_food_to_baby'] : NULL;
	        $motherandchild->child_food_quality = isset($data['child_food_quality']) ? $data['child_food_quality'] : NULL;
	        $motherandchild->child_food_frequency = isset($data['child_food_frequency']) ? $data['child_food_frequency'] : NULL;
	        $motherandchild->child_food_distribution = isset($data['child_food_distribution']) ? $data['child_food_distribution'] : NULL;

	        $motherandchild->injury_type_falls = isset($data['injury_child_faced_falls']) ? $data['injury_child_faced_falls'] : NULL;
	        $motherandchild->injury_type_drowning = isset($data['injury_child_faced_drowning']) ? $data['injury_child_faced_drowning'] : NULL;
	        $motherandchild->injury_type_burnsscald = isset($data['injury_child_faced_burnsscald']) ? $data['injury_child_faced_burnsscald'] : NULL;
	        $motherandchild->injury_type_poisoning = isset($data['injury_child_faced_poisoning']) ? $data['injury_child_faced_poisoning'] : NULL;
	        $motherandchild->injury_type_suffocatingchokingaspiration = isset($data['injury_child_faced_suffocationchokingaspiration']) ? $data['injury_child_faced_suffocationchokingaspiration'] : NULL;
	        $motherandchild->injury_type_cut = isset($data['injury_child_faced_cut_injury']) ? $data['injury_child_faced_cut_injury'] : NULL;
	        $motherandchild->injury_type_oth = isset($data['injury_child_faced_others']) ? $data['injury_child_faced_others'] : NULL;
	        $motherandchild->injury_types_oth_spec = isset($data['injury_child_faced_specify']) ? $data['injury_child_faced_specify'] : NULL;

	        $motherandchild->injury_cause_unsafe_home = isset($data['injury_cause_unsafe_home']) ? $data['injury_cause_unsafe_home'] : NULL;
	        $motherandchild->injury_cause_no_supervision = isset($data['injury_cause_no_super_vision']) ? $data['injury_cause_no_super_vision'] : NULL;
	        $motherandchild->injury_cause_busy_mother = isset($data['injury_cause_busy_mother']) ? $data['injury_cause_busy_mother'] : NULL;
	        $motherandchild->injury_cause_slippery_floor = isset($data['injury_cause_slippery_floor']) ? $data['injury_cause_slippery_floor'] : NULL;
	        $motherandchild->injury_cause_oth = isset($data['injury_cause_others']) ? $data['injury_cause_others'] : NULL;
	        $motherandchild->injury_cause_oth_spec = isset($data['injury_cause_specify']) ? $data['injury_cause_specify'] : NULL;

	        $motherandchild->age_bcg_at_birth = isset($data['age_bcg_at_birth']) ? $data['age_bcg_at_birth'] : NULL;
	        $motherandchild->age_dpt_hepb_hib6_weeks = isset($data['age_dpt_hepb_hib6_weeks']) ? $data['age_dpt_hepb_hib6_weeks'] : NULL;
	        $motherandchild->age_dpt_hepb_hib10_weeks = isset($data['age_dpt_hepb_hib10_weeks']) ? $data['age_dpt_hepb_hib10_weeks'] : NULL;
	        $motherandchild->age_dpt_hepb_hib14_weeks = isset($data['age_dpt_hepb_hib14_weeks']) ? $data['age_dpt_hepb_hib14_weeks'] : NULL;
	        $motherandchild->age_opv6_weeks = isset($data['age_opv6_weeks']) ? $data['age_opv6_weeks'] : NULL;
	        $motherandchild->age_opv10_weeks = isset($data['age_opv10_weeks']) ? $data['age_opv10_weeks'] : NULL;
	        $motherandchild->age_opv14_weeks = isset($data['age_opv14_weeks']) ? $data['age_opv14_weeks'] : NULL;
	        $motherandchild->age_pcv6_weeks = isset($data['age_pcv6_weeks']) ? $data['age_pcv6_weeks'] : NULL;
	        $motherandchild->age_pcv10_weeks = isset($data['age_pcv10_weeks']) ? $data['age_pcv10_weeks'] : NULL;
	        $motherandchild->age_pcv9_months = isset($data['age_pcv9_months']) ? $data['age_pcv9_months'] : NULL;
	        $motherandchild->age_ipv14_weeks = isset($data['age_ipv14_weeks']) ? $data['age_ipv14_weeks'] : NULL;
	        $motherandchild->age_measles_rubella9_months = isset($data['age_measles_rubella9_months']) ? $data['age_measles_rubella9_months'] : NULL;
	        $motherandchild->age_measles_rubella15_months = isset($data['age_measles_rubella15_months']) ? $data['age_measles_rubella15_months'] : NULL;
	        $motherandchild->age_japanese_encephalitis12_months = isset($data['age_japanese_encephalitis12_months']) ? $data['age_japanese_encephalitis12_months'] : NULL;
	        
	        $motherandchild->immu_remarks_bcg = isset($data['remarks_bcg']) ? $data['remarks_bcg'] : NULL;
	        $motherandchild->immu_remarks_dpt_hepb_hib = isset($data['remarks_dpt_hepb_hib']) ? $data['remarks_dpt_hepb_hib'] : NULL;
	        $motherandchild->immu_remarks_opv = isset($data['remarks_opv']) ? $data['remarks_opv'] : NULL;
	        $motherandchild->immu_remarks_pcv = isset($data['remarks_pcv']) ? $data['remarks_pcv'] : NULL;
	        $motherandchild->immu_remarks_ipv = isset($data['remarks_ipv']) ? $data['remarks_ipv'] : NULL;
	        $motherandchild->immu_remarks_measles_rubella = isset($data['remarks_measles_rubella']) ? $data['remarks_measles_rubella'] : NULL;
	        $motherandchild->immu_remarks_japanese_encephalitis = isset($data['remarks_japanese_encephalitis']) ? $data['remarks_japanese_encephalitis'] : NULL;

	        $motherandchild->advise_bf = isset($data['advice_breast_feeding']) ? $data['advice_breast_feeding'] : NULL;
	        $motherandchild->advise_dh = isset($data['advice_dental_hygiene']) ? $data['advice_dental_hygiene'] : NULL;
	        $motherandchild->advise_tt = isset($data['advice_toilet_training']) ? $data['advice_toilet_training'] : NULL;
	        $motherandchild->advise_cf = isset($data['advice_complementary_feeding']) ? $data['advice_complementary_feeding'] : NULL;
	        $motherandchild->advise_ap = isset($data['advice_accident_prevention']) ? $data['advice_accident_prevention'] : NULL;
	        $motherandchild->advise_physical = isset($data['md_physical']) ? $data['md_physical'] : NULL;
	        $motherandchild->advise_verbal = isset($data['md_verbal']) ? $data['md_verbal'] : NULL;
	        $motherandchild->advise_social = isset($data['md_social']) ? $data['md_social'] : NULL;
	        $motherandchild->advise_spiritual = isset($data['md_spiritual_religious']) ? $data['md_spiritual_religious'] : NULL;
	        $motherandchild->advise_motor = isset($data['md_motor']) ? $data['md_motor'] : NULL;
	        $motherandchild->advise_intellectual = isset($data['md_intellectual']) ? $data['md_intellectual'] : NULL;
	        $motherandchild->advise_emotional = isset($data['md_emotional']) ? $data['md_emotional'] : NULL;

	        if($motherandchild->save())
	        {
	    		return new JsonResponse(array('status'=>'success', 'message'=>$message), 200);
	        }
	        else
	        {
	    		return new JsonResponse(array('status'=>'error', 'message'=>'Error in saving mother and child form.'), 400);
	        }
	        

	    }
	    else
	    {
	    	// return $this->json_response('error',$validator->errors()->all(),400);
	    	return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
	    }
	}

}