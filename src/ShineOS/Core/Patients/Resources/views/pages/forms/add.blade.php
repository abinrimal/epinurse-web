@extends('patients::layouts.inner')
@section('header-content') Create New Patient's Profile @stop

@section('patient-content')
    <style type="text/css" media="screen">
      .modal-header .close {
          margin-top: 0;
          padding: 4px 6px;
      }
    </style>
    <div class="nav-tabs-custom">
        @include('patients::partials.tab_nav')

        {!! Form::open(array('url' => 'patients/save', 'name'=>'crudForm', 'class'=>'form-horizontal', 'id'=>'wizardForm')) !!}
        <div class="tab-content">
            @include('patients::pages.forms.add_basic_info')
            @include('patients::pages.forms.add_personal')
            @include('patients::pages.forms.location')
            @include('patients::pages.forms.allergies_alerts')
            @include('patients::pages.forms.history')
            @if($plugs)
              @foreach($plugs as $plug)
                @if($plug['plugin_location'] == 'tab')
                    <?php View::addNamespace('pluginform', plugins_path().$plug['plugin']); ?>
                    @include('pluginform::'.strtolower($plug['plugin']))
                @endif
              @endforeach
            @endif
            @include('patients::pages.forms.notification_settings')
            @include('patients::pages.forms.photos')
        </div>

        <div class="wizard-buttons mainbuttons col-md-12 textcenter">
          <input class="btn btn-success" id="back" value="Back" type="reset" />
          <input type="submit" class="btn btn-success" id="next" value="Next" />
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('patient-footer')
@stop

@section('scripts')
  <div class="modal fade" id="consentBox" tabindex="-1" role="dialog" aria-labelledby="consentBox" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <button type="button" class="close" id="printConsent"><span aria-hidden="true"><i class="fa fa-print fa-fw"></i></span><span class="sr-only">Print</span></button>
            <h4 class="modal-title" id="consentBox">Patient Consent</h4>
          </div>
          <div class="modal-body" id="printableBox">
                <h2><img src=""><?php
                            $logo = $facility->facility_logo;
                        ?>
                        @if ( $logo != '' )
                            <img src="{{ url( 'public/uploads/profile_picture/'.$logo ) }}" class="img-circle" />
                        @else
                            <img src="{{ asset( 'public/dist/img/no_logo.png' ) }}" class="img-circle" />
                        @endif
                        SHINEOS+</h2>
                <h3 class="text-center">Patient Informed Consent for Participation to PHIE</h2>
                <p>&nbsp;</p>
                <p>I, <strong id='patname'></strong> hereby give my <strong>consent</strong> to <b>{{ $facility->facility_name }}</b> and/or <b>{{ $user->first_name." ".$user->last_name }}</b> in collecting, storing and sending or transmitting my personal and health data to the Philippine Health Information Exchange – Lite (PHIE Lite) for the purpose of PhilHealth’s Primary Care Benefit (PCB) Package and/or the Department of Health’s (DOH’s) National Health Data Reporting Requirements.</p>
                <p>I was made to understand that:</p>
                <ol>
                  <li>All information collected will be kept confidential and it will not be published or shared.</li>
                  <li>All information gathered, stored and transmitted to PhilHealth or DOH will be done thru SHINEOS+, an authorized and recognized EMR provider.</li>
                  <li>Appropriate safety measures have been put in place to protect the privacy and security of my well­being, health information, and other rights under laws governing data privacy and security, and related issuances of the Republic of the Philippines.</li>
                  <li>This consent is valid in PHIE Lite and other DOH National Health Data Reporting Requirements until it is revoked by myself or my duly authorized representative.</li>
                  <li>My participation is voluntary and I can cancel my consent at any time without giving reasons and without any disadvantage effects on my medical treatment and/or services.</li>
                </ol>
                <p>I certify that I have been made to understand my rights in a language and manner understandable to me by a representative of the facility/health care provider and that the health data or information is true and complete to the best of my knowledge.</p>
                <p>Signed this date of {{ date('F') }}, {{ date('Y') }} at {{ date('h:m A') }}.</p><p>&nbsp;</p>
                <p>___________________________________</p>
                <p>Name of Patient/Representative <small>(Signature over printed name)</small></p><p>&nbsp;</p>
                <p>___________________________________</p>
                <p>Representative of Health Facility <small>(Signature over printed name)</small></p><p>&nbsp;</p>
                <p>Contact Number: ______________</p>
          </div>
        </div>
      </div>
    </div>

  <script>
    var patientId = "";

    $(document).ready(function () {
        show_maiden();
    });

    $('#printConsent').on('click', function() {
        $('#printableBox').printThis({
            importStyle: true
        });
    });

    $("#consentBox").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        var name = $('input[name=inputPatientFirstName]').val()+" "+$('input[name=inputPatientLastName]').val();
        $(this).find(".modal-content #patname").text(name);
    });

  </script>
{!! HTML::script('public/dist/js/jquery.form.js') !!}
{!! HTML::script('public/dist/js/jquery.validate.js') !!}
{!! HTML::script('public/dist/js/jquery.form.wizard.js') !!}
{!! HTML::script('public/dist/js/pages/patients/patients.js') !!}

@stop
