<?php namespace ShineOS\Core\API\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use ShineOS\Core\Referrals\Entities\Referrals;
use ShineOS\Core\Referrals\Http\Controllers\ReferralsController as ReferralsController; 
use ShineOS\Core\Referrals\Entities\ReferralReasons as referralReasons;
use Shine\Libraries\IdGenerator;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser as facilityPatientUser;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\LOV\Entities\LovReferralReasons;
use ShineOS\Core\Facilities\Entities\Facilities;
use Request, Validator;

class APIReferralsController extends Controller {
	
	private $healthcareRepository;
	public function __construct(HealthcareRepository $healthcareRepository) {
		$this->HealthcareRepository = $healthcareRepository;
	}

	public function index() {
		return response(json_encode(array('message'=>'No data')), 200);
	}
	
	public function referralsInbound_get() {
		$data = Request::all();  
		if(array_key_exists('facility_id',$data)) {
			$ReferralsData = $this->referralsInbound($data['facility_id']);
			return new JsonResponse((array('status'=>'success','data'=>json_decode($ReferralsData['referrals']->toJson()))), 200);
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>'Facility id required'), 400);
		}
	}

	public function referralsInbound($facility_id) {
		$ReferralsData = new ReferralsController($this->HealthcareRepository);
		$ReferralsData = $ReferralsData->getReferral(array(1, 2, 3, 4), $facility_id, 'inbound');
		return $ReferralsData;
	}

	public function referralsOutbound_get() {
		$data = Request::all();  
		if(array_key_exists('facility_id',$data)) {
			$ReferralsData = $this->referralsDraftsOutbound($data['facility_id'], 'outbound');
			return new JsonResponse((array('status'=>'success','data'=>json_decode($ReferralsData['referrals']->toJson()))), 200);
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>'Facility id required'), 400);
		}
	}

	public function referralsDrafts_get() {
		$data = Request::all();  
		if(array_key_exists('facility_id',$data)) {
			$ReferralsData = $this->referralsDraftsOutbound($data['facility_id'], 'drafts');
			return new JsonResponse((array('status'=>'success','data'=>json_decode($ReferralsData['referrals']->toJson()))), 200);
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>'Facility id required'), 400);
		}
	}

	public function referralMessages_get() {
		$data = Request::all();  
		if(array_key_exists('facility_id',$data)) {
			$ReferralsData = new ReferralsController($this->HealthcareRepository);
			$ReferralsData = $ReferralsData->referralM(NULL, $data['facility_id']);
			
			return new JsonResponse((array('status'=>'success','data'=>json_decode($ReferralsData['referrals']->toJson()))), 200);
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>'Facility id required'), 400);
		}
	}

	public function referralsDraftsOutbound($facility_id, $type) {
		$ReferralsData = new ReferralsController($this->HealthcareRepository);
		$getFacilityUser = $ReferralsData->getFacilityUser($facility_id);
		if($type=='outbound'){
			$ReferralsData = $ReferralsData->getReferral(array(1, 2, 3, 4), $getFacilityUser, $type);
		} else {
			$ReferralsData = $ReferralsData->getReferral(array(6), $getFacilityUser, $type);
		}
		
		return $ReferralsData;
	}

	public function viewReferralByReferralId_get() {
		$data = Request::all(); 
		$validator = Validator::make($data, [ 
			'referral_id'	=> 'required'
        ]);
        
		if ($validator->fails()==false) {
			$ReferralsData = new ReferralsController($this->HealthcareRepository);
			$referrals = Referrals::where('referral_id',$data['referral_id'])->get();
			$dataReferral = $ReferralsData->dataReferral(null, $referrals, $data['referral_id']);

			// $ReferralsData = $this->referralsDraftsOutbound($data['facility_id'], 'drafts');
			return new JsonResponse((array('status'=>'success','data'=>json_decode($dataReferral))), 200);
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
	}

	public function createReferral_get()  {
		$data = Request::all(); 
		
		$validator = Validator::make($data, [ 
			'facility_id'			=> 'required',
	        'healthcareservice_id'  => 'required',
        ]);

		if ($validator->fails()==false) {
			$facility_id = $data['facility_id'];
			$healthcareId = $data['healthcareservice_id'];

	        $facilityPatientUserId = Healthcareservices::join('facility_patient_user','healthcare_services.facilitypatientuser_id','=','facility_patient_user.facilitypatientuser_id')->where('healthcare_services.healthcareservice_id', $healthcareId)->where('facility_patient_user.deleted_at', NULL)->select('healthcare_services.facilitypatientuser_id','healthcare_services.healthcareservicetype_id','healthcare_services.encounter_datetime')->get();
	        
	        if($facilityPatientUserId->isEmpty() == FALSE) {
	        	$Patients = new Patients;
	        	foreach ($facilityPatientUserId as $key => $value) {
		            $patientid = facilityPatientUser::where('facilitypatientuser_id', $value->facilitypatientuser_id)->pluck('patient_id');

		            $Patients = Patients::where('patient_id', $patientid)->where('deleted_at', NULL)->first();
		            $Patients->healthcare_servicetype_name = (isset($value->healthcareservicetype_id)) ? $value->healthcareservicetype_id : "";
		            $Patients->encounter_datetime = $value->encounter_datetime;
		            $Patients->healthcareId = $healthcareId; 
		        }

		        $Patients->lovReferralReasons = LovReferralReasons::all()->pluck('referral_reason','lovreferralreason_id');
		        $Patients->facilities = Facilities::whereNotIn('facility_id', [$facility_id])->orderBy('facility_name')->get();
		        
		        return new JsonResponse((array('status'=>'success','data'=>$Patients)), 200);
	        } else {
	        	return new JsonResponse(array('status'=>'error', 'message'=>'Error: Healthcare ID or Facility ID not found.'), 400);
	        }
	        
		} else {
			 return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
	}

	public function insertReferral_post() {
		$data = Request::all();

		$validator = Validator::make($data, [
			'referrer_facility_id'	=> 'required',
			'selected_facility_id'	=> 'required', 
			'healthcareservice_id'	=> 'required',

			'referral_reasons'		=> 'required',
			'urgency'				=> 'required|max:10',
			'method_transport'		=> 'required',
			// 'management_done'		=> 'required',
   //          'medical_given'         => 'required',
   //          'referral_remarks' 		=> 'required',
            'send' 					=> 'required|boolean', 
        ]);

		if ($validator->fails()==false) {
			$newId = IdGenerator::generateId();
	        $referrer_facility = $data['referrer_facility_id']; /*** referrer **/
	        $facility_details = json_decode($data['selected_facility_id']); /*** selected facilities - referred to **/
	        $ReferralReasons = json_decode($data['referral_reasons']);

            foreach ($facility_details as $key => $value) {
                $referral_id = $newId.$value.$value;

                $referrals = new Referrals();
                $referrals->referral_id = $referral_id;
                $referrals->facility_id = $value;
                $referrals->urgency = $data['urgency'];

                $referrals->method_transport = (array_key_exists('method_transport',$data)) ? $data['method_transport'] : '';
                $referrals->management_done = (array_key_exists('management_done',$data)) ? $data['management_done'] : '';
                $referrals->medical_given = (array_key_exists('medical_given',$data)) ? $data['medical_given'] : '';
                $referrals->referral_remarks = (array_key_exists('referral_remarks',$data)) ? $data['referral_remarks'] : '';

                if($data['send']) {
                    $referrals->referral_status = '1'; //SENT
                } else {
                    $referrals->referral_status = '6'; //save as DRAFT
                }

                $referrals->healthcareservice_id = $data['healthcareservice_id'];
                $referralssave = $referrals->save();

                if($ReferralReasons) {
                    foreach ($ReferralReasons as $k => $v) {
                        $referralReasons = new referralReasons(); 
                        $referralReasons->referralreason_id = $newId.$v.$value;
                        $referralReasons->referral_id = $referral_id;
                        $referralReasons->lovreferralreason_id = $v; 
                        $referralReasonssave = $referralReasons->save();
                    }
                }

                if($referralssave && $referralReasonssave) {
                    $subject = 'SHINE OS+: Referred Patient';
                    $message = 'Referral';
                    // $emailSending = new ReferralsController($this->HealthcareRepository);
                    // $emailSending = $emailSending->sendToEmail($referral_id, 'add', $subject, $message, 1);

                    $status1 = 'success';
		            $flash_type = 200;
		            $flash_message = 'Well done! You have successfully added a referral.';
                } else {
                    $status1 = 'error';
	                $flash_type = 400;
	                $flash_message = 'Failed to add';
                }
            }
            return new JsonResponse(array('status'=>$status1, 'message'=> $flash_message), $flash_type); 
	    } else {
			 return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
	}

	public function updateReferralStatus_post() {
		$data = Request::all();
		$validator = Validator::make($data, [
			'status'		=> 'required|in:Accept,Decline,Send,Discard',
			'referral_id'	=> 'required',
        ]);

		if ($validator->fails()==false) {
			$status = $data['status'];
			$referral_id = $data['referral_id'];

	        $message = new ReferralsController($this->HealthcareRepository);
	        $message = $message->updateReferralStatus($referral_id,$status);

	        if(strstr($message, "Successfully")) {
	        	$err_status = 200;
	        	$status1 = "success";
	        } else {
	        	$err_status = 400;
	        	$status1 = "error";
	        }
	        return new JsonResponse(array('status'=>$status1, 'message'=> $message), $err_status); 
		} else {
		 return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
    }

     public function addfollowup_post() {
     	$data = Request::all();
		$validator = Validator::make($data, [
			'referral_id'	=> 'required',
        ]);

		if ($validator->fails()==false) {
			$referral_id = $data['referral_id'];
	        $type = 'follow';
	        $referral_subject = 'SHINE OS+ referral follow-up';
	        $referral_message = 'A follow-up is being ask for the consultation referral forward to you.';
	        $referral_message_status = 1;
	        $referrer = 1;

	        $message = new ReferralsController($this->HealthcareRepository);
	        $message = $message->refMessageToDB($type, $referral_id, $referral_subject, $referral_message, $referral_message_status, $referrer);	        

	        if(strstr($message, "Successfully")) {
	        	$err_status = 200;
	        	$status1 = "success";
	        } else {
	        	$err_status = 400;
	        	$status1 = "error";
	        }
	        return new JsonResponse(array('status'=>$status1, 'message'=> $message), $err_status);
	    } else {
		 return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
    }
	
	public function replyToFollowUp_post() {
		$data = Request::all();
		$validator = Validator::make($data, [
			'referral_id'			=> 'required',
			'referral_message'		=> 'required',
			'current_facility_id' 	=> 'required',
        ]);

		if ($validator->fails()==false) {
	        $referral_message = $data['referral_message'];
	        $referral_id = $data['referral_id'];
	        $current_facilityid = $data['current_facility_id'];
	        $referralDetails = Referrals::where('referral_id', $referral_id)->where('facility_id', $current_facilityid)->first();

	        if($referralDetails){ 
	            $type = 'reply';
	            $referral_subject = 'SHINE OS+ reply to your referral';

	            $referral_message_status = 1;
	            $referralDetails = Referrals::where('referral_id', $referral_id)->first();
	            if($current_facilityid == $referralDetails['facility_id']) {
	                $referrer = 0;
	            } else {
	                $referrer = 1;
	            }
	            $message = new ReferralsController($this->HealthcareRepository);
	        	$message = $message->refMessageToDB($type, $referral_id, $referral_subject, $referral_message, $referral_message_status, $referrer);
	        } else {
	        	$err_status = 400;
	        	$status1 = "error";
	            $message = 'Facility ID and Referral ID did not matched.';
	        }

        	if(strstr($message, "Successfully")) {
	        	$err_status = 200;
	        	$status1 = "success";
	        } else {
	        	$err_status = 400;
	        	$status1 = "error";
	        }
	        return new JsonResponse(array('status'=>$status1, 'message'=> $message), $err_status);
        } else {
		 return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
    }
} 