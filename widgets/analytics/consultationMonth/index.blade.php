

<div class="box box-primary"><!--Consultations-->
    <div class="box-header">
        <div class="pull-right box-tools">
            <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button> -->
            <button id="consultationMonthDatePicker" class="btn btn-primary btn-sm daterange consultationMonthDatePicker" data-toggle="tooltip" title="Date range"><i class="fa fa-calendar"></i></button>
        </div>
        <i class="fa fa-stethoscope"></i><h3 class="box-title text-shine-blue consultationCountBoxTitle">Consultations Monthly Count</h3>
    </div><!-- /.box-header -->

    <div class="box-body no-padding">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#consultationCountTableCount" data-toggle="tab">Table</a></li>
                <li class=""><a href="#consultationCountBarCount" data-toggle="tab">Bar Chart</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="consultationCountTableCount">
                    <table id="consultationCount" class="table table-condensed">
                        <thead>
                            <tr><td colspan="4"><h4 class="consultationCountBoxTitle">Consultations This Month</h4></td></tr>
                            <tr>
                                <th>Month</th>
                                <th style="text-align:right">Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sum = 0; ?>
                            @foreach($visits as $visit)
                                <?php $sum += $visit->counter; ?>
                                <tr>
                                    <td>{{ $visit->monther }} {{ $visit->yearer }}</td>
                                    <td style="text-align:right">{{ $visit->counter }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td><strong> Total </strong></td>
                                <td style="text-align:right"><strong> {{ $sum }} </strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- /.tab-pane -->
                <div class="tab-pane" id="consultationCountBarCount">
                    <table id="consultationCount" class="table table-condensed">
                        <thead>
                            <tr><td colspan="4"><h4 class="consultationCountBoxTitle">Consultations This Month</h4></td></tr>
                        </thead>
                    </table>
                    <div class="chart">
                        <canvas id="consultationCountBarChart"></canvas>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
                </div><!-- /.tab-pane -->
            </div>
        </div>
    </div><!-- /.box-body -->

</div><!--/. end consultations-->

<script>
$(document).ready(function() {
    $('#consultationMonthDatePicker').daterangepicker(
        {
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1,'days'), moment().subtract(1,'days')],
                'Last 7 Days': [moment().subtract(6,'days'), moment()],
                'Last 30 Days': [moment().subtract(29,'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last 3 Months': [moment().subtract(2,'month').startOf('month'), moment().endOf('month')],
                'Last 6 Months': [moment().subtract(5,'month').startOf('month'), moment().endOf('month')],
                'Last 12 Months': [moment().subtract(11,'month').startOf('month'), moment().endOf('month')]
            },
            startDate: moment(),
            endDate: moment()
        },
        function(start, end, label) {
            $.ajax({
                type: "GET",
                url: "<?php site_url(); ?>enhancedanalytics/getconsultationmonth/"+start.format('YYYY-MM-DD')+"/"+end.format('YYYY-MM-DD'),
                beforeSend: function( xhr ) {
                    $('h4.consultationCountBoxTitle').text("Retrieving records starting from "+start.format('MMM DD, YYYY')+ " to "+end.format('MMM DD, YYYY'));
                    $('#consultationCount tbody').html("<tr><td colspan='4'><i class='fa fa-spinner fa-pulse fa-fw'></i> Loading. Please wait...</td></tr>");
                }
            })
            .done(function( msg ) {
                $('#consultationCount tbody').removeClass('provider_lister_box_loading');
                $('h4.consultationCountBoxTitle').text("Consultations starting from "+start.format('MMM DD, YYYY')+ " to "+end.format('MMM DD, YYYY'));
                $('#consultationCount tbody').html(msg);

                updateconsultationCountBarChart();
            });
    });
    function generateLabelsForBar() {
        var labels = [];
        var table = document.getElementById('consultationCount');
        for (var r = 2, n = table.rows.length-1; r < n; r++) {
          if(table.rows[r].cells[0] != null)
          {
            labels.push(table.rows[r].cells[0].innerHTML);
          }
        }      
        return labels;
    }
    function generateDataForBar() {
        var data = [];
        var table = document.getElementById('consultationCount');
        for (var r = 2, n = table.rows.length-1; r < n; r++) {
          if(table.rows[r].cells[1] != null)
          {
            data.push(table.rows[r].cells[1].innerHTML);
          }
        }      
        return data;
    }

    function updateconsultationCountBarChart()
    {
        //-------------
        //- BAR CHART -
        //-------------
        var consultationCountBarChartData = {
          labels: generateLabelsForBar(),
          datasets: [
            {
              label: "Consultations",
              fillColor: "rgba(60,141,188,0.9)",
              strokeColor: "rgba(60,141,188,0.8)",
              pointColor: "#3b8bba",
              pointStrokeColor: "rgba(60,141,188,1)",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(60,141,188,1)",
              data: generateDataForBar()
            }
          ]
        };
        $('#consultationCountBarChart').remove(); // this is my <canvas> element
        $('#consultationCountBarCount').append('<canvas id="consultationCountBarChart"></canvas>'); //append canvas
        var consultationCountBarChartCanvas = $("#consultationCountBarChart").get(0).getContext("2d");
        var consultationCountBarChart = new Chart(consultationCountBarChartCanvas);
        var consultationCountBarChartOptions = {
          //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
          scaleBeginAtZero: true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines: true,
          //String - Colour of the grid lines
          scaleGridLineColor: "rgba(0,0,0,.05)",
          //Number - Width of the grid lines
          scaleGridLineWidth: 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines: true,
          //Boolean - If there is a stroke on each bar
          barShowStroke: true,
          //Number - Pixel width of the bar stroke
          barStrokeWidth: 2,
          //Number - Spacing between each of the X value sets
          barValueSpacing: 5,
          //Number - Spacing between data sets within X values
          barDatasetSpacing: 1,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          //Boolean - whether to make the chart responsive
          responsive: true,
          maintainAspectRatio: true
        };

        consultationCountBarChartOptions.datasetFill = false;
        consultationCountBarChart.Bar(consultationCountBarChartData, consultationCountBarChartOptions);
    }

    $('[href=#consultationCountBarCount]').on('shown.bs.tab', function (e) {
        updateconsultationCountBarChart()
    });
});
</script>
