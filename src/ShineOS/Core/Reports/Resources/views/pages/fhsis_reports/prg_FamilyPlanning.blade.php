@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<table class="table table-striped table-bordered table-report table-responsive">
    <thead>
    <tr>
    <th rowspan="3">FAMILY PLANNING METHOD</th>
    <th rowspan="3" width="10%">Current User<br>(Beginning Month)</th>
    <th colspan="2">Acceptors</th>
    <th rowspan="3" width="10%">Dropout<br>(Present Month)</th>
    <th rowspan="3" width="10%">Current User<br>(End of Month)</th>
    <th rowspan="3" width="10%">New Acceptors of<br>the present Month</th>
    </tr>
    <tr><th width="10%">New Acceptors</th><th width="10%">Other Acceptors</th></tr>
    <tr><th>Previous Month</th><th>Present Month</th></tr>
    </thead>

    <tbody>
    <tr>
        <td>a. Female Sterilization/BTL</td>
        <td>{{ $data['PREV_FS'] }}</td>
        <td>{{ $data['TNA_FS'] }}</td>
        <td>{{ $data['TOA_FS'] }}</td>
        <td>{{ $data['TDO_FS'] }}</td>
        <td>{{ $data['TCU_FS'] }}</td>
        <td>{{ $data['TNAP_FS'] }}</td>
    </tr>
    <tr>
        <td>b. Male Sterilization/Vasectomy</td>
        <td>{{ $data['PREV_MS'] }}</td>
        <td>{{ $data['TNA_MS'] }}</td>
        <td>{{ $data['TOA_MS'] }}</td>
        <td>{{ $data['TDO_MS'] }}</td>
        <td>{{ $data['TCU_MS'] }}</td>
        <td>{{ $data['TNAP_MS'] }}</td>
    </tr>
    <tr>
        <td>c. Pills</td>
        <td>{{ $data['PREV_PILLS'] }}</td>
        <td>{{ $data['TNA_PILLS'] }}</td>
        <td>{{ $data['TOA_PILLS'] }}</td>
        <td>{{ $data['TDO_PILLS'] }}</td>
        <td>{{ $data['TCU_PILLS'] }}</td>
        <td>{{ $data['TNAP_PILLS'] }}</td>
    </tr>
    <tr>
        <td>d. IUD (Intrauterine Device)</td>
        <td>{{ $data['PREV_IUD'] }}</td>
        <td>{{ $data['TNA_IUD'] }}</td>
        <td>{{ $data['TOA_IUD'] }}</td>
        <td>{{ $data['TDO_IUD'] }}</td>
        <td>{{ $data['TCU_IUD'] }}</td>
        <td>{{ $data['TNAP_IUD'] }}</td>
    </tr>
    <tr>
        <td>e. Injectables (DMPA/CIC)</td>
        <td>{{ $data['PREV_DMPA'] }}</td>
        <td>{{ $data['TNA_DMPA'] }}</td>
        <td>{{ $data['TOA_DMPA'] }}</td>
        <td>{{ $data['TDO_DMPA'] }}</td>
        <td>{{ $data['TCU_DMPA'] }}</td>
        <td>{{ $data['TNAP_DMPA'] }}</td>
    </tr>
    <tr>
        <td>f. NFP-CM (Cervical Mucus)</td>
        <td>{{ $data['PREV_NFPCM'] }}</td>
        <td>{{ $data['TNA_NFPCM'] }}</td>
        <td>{{ $data['TOA_NFPCM'] }}</td>
        <td>{{ $data['TDO_NFPCM'] }}</td>
        <td>{{ $data['TCU_NFPCM'] }}</td>
        <td>{{ $data['TNAP_NFPCM'] }}</td>
    </tr>
    <tr>
        <td>g. NFP-BBT (Basal Body Temperature)</td>
        <td>{{ $data['PREV_NFPBBT'] }}</td>
        <td>{{ $data['TNA_NFPBBT'] }}</td>
        <td>{{ $data['TOA_NFPBBT'] }}</td>
        <td>{{ $data['TDO_NFPBBT'] }}</td>
        <td>{{ $data['TCU_NFPBBT'] }}</td>
        <td>{{ $data['TNAP_NFPBBT'] }}</td>
    </tr>
    <tr>
        <td>h. NFP-STM (Symptothermal Method)</td>
        <td>{{ $data['PREV_NFPSTM'] }}</td>
        <td>{{ $data['TNA_NFPSTM'] }}</td>
        <td>{{ $data['TOA_NFPSTM'] }}</td>
        <td>{{ $data['TDO_NFPSTM'] }}</td>
        <td>{{ $data['TCU_NFPSTM'] }}</td>
        <td>{{ $data['TNAP_NFPSTM'] }}</td>
    </tr>
    <tr>
        <td>i. NFP-SDM (Standard Days Method)</td>
        <td>{{ $data['PREV_NFPSDM'] }}</td>
        <td>{{ $data['TNA_NFPSDM'] }}</td>
        <td>{{ $data['TOA_NFPSDM'] }}</td>
        <td>{{ $data['TDO_NFPSDM'] }}</td>
        <td>{{ $data['TCU_NFPSDM'] }}</td>
        <td>{{ $data['TNAP_NFPSDM'] }}</td>
    </tr>
    <tr>
        <td>j. NFP-LAM (Lactational Amenorrhea Method)</td>
        <td>{{ $data['PREV_NFPLAM'] }}</td>
        <td>{{ $data['TNA_NFPLAM'] }}</td>
        <td>{{ $data['TOA_NFPLAM'] }}</td>
        <td>{{ $data['TDO_NFPLAM'] }}</td>
        <td>{{ $data['TCU_NFPLAM'] }}</td>
        <td>{{ $data['TNAP_NFPLAM'] }}</td>
    </tr>
    <tr>
        <td>k. Condom</td>
        <td>{{ $data['PREV_CONDOM'] }}</td>
        <td>{{ $data['TNA_CONDOM'] }}</td>
        <td>{{ $data['TOA_CONDOM'] }}</td>
        <td>{{ $data['TDO_CONDOM'] }}</td>
        <td>{{ $data['TCU_CONDOM'] }}</td>
        <td>{{ $data['TNAP_CONDOM'] }}</td>
    </tr>
    <tr><!-- 
        <td>l. Implant</td>
        <td>{{-- $data['PREV_IMPLANT'] --}}</td>
        <td>{{-- $data['TNA_IMPLANT'] --}}</td>
        <td>{{-- $data['TOA_IMPLANT'] --}}</td>
        <td>{{-- $data['TDO_IMPLANT'] --}}</td>
        <td>{{-- $data['TCU_IMPLANT'] --}}</td>
        <td>{{-- $data['TNAP_IMPLANT'] --}}</td>
    </tr> -->
    <tr>
        <td>TOTAL</td>
        <td>{{ $data['PREV_FS'] + $data['PREV_MS'] + $data['PREV_PILLS'] + $data['PREV_IUD'] + $data['PREV_DMPA'] + $data['PREV_NFPCM'] + $data['PREV_NFPBBT'] + $data['PREV_NFPSTM'] + $data['PREV_NFPSDM'] + $data['PREV_NFPLAM'] + $data['PREV_CONDOM'] }}</td>
        <td>{{ $data['TNA_FS'] + $data['TNA_MS'] + $data['TNA_PILLS'] + $data['TNA_IUD'] + $data['TNA_DMPA'] + $data['TNA_NFPCM'] + $data['TNA_NFPBBT'] + $data['TNA_NFPSTM'] + $data['TNA_NFPSDM'] + $data['TNA_NFPLAM'] + $data['TNA_CONDOM'] }}</td>
        <td>{{ $data['TOA_FS'] + $data['TOA_MS'] + $data['TOA_PILLS'] + $data['TOA_IUD'] + $data['TOA_DMPA'] + $data['TOA_NFPCM'] + $data['TOA_NFPBBT'] + $data['TOA_NFPSTM'] + $data['TOA_NFPSDM'] + $data['TOA_NFPLAM'] + $data['TOA_CONDOM'] }}</td>
        <td>{{ $data['TDO_FS'] + $data['TDO_MS'] + $data['TDO_PILLS'] + $data['TDO_IUD'] + $data['TDO_DMPA'] + $data['TDO_NFPCM'] + $data['TDO_NFPBBT'] + $data['TDO_NFPSTM'] + $data['TDO_NFPSDM'] + $data['TDO_NFPLAM'] + $data['TDO_CONDOM'] }}</td>
        <td>{{ $data['TCU_FS'] + $data['TCU_MS'] + $data['TCU_PILLS'] + $data['TCU_IUD'] + $data['TCU_DMPA'] + $data['TCU_NFPCM'] + $data['TCU_NFPBBT'] + $data['TCU_NFPSTM'] + $data['TCU_NFPSDM'] + $data['TCU_NFPLAM'] + $data['TCU_CONDOM'] }}</td>
        <td>{{ $data['TNAP_FS'] + $data['TNAP_MS'] + $data['TNAP_PILLS'] + $data['TNAP_IUD'] + $data['TNAP_DMPA'] + $data['TNAP_NFPCM'] + $data['TNAP_NFPBBT'] + $data['TNAP_NFPSTM'] + $data['TNAP_NFPSDM'] + $data['TNAP_NFPLAM'] + $data['TNAP_CONDOM'] }}</td>
    </tr>
    </tbody>
</table>
@yield('modal-footer')