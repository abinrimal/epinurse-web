<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhilhealthinfoPmrf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('patient_philhealthinfo')!=TRUE) {
            Schema::create('patient_philhealthinfo', function (Blueprint $table) {
                $table->increments('id');
                $table->string('patient_id', 60);
                $table->string('patient_philhealthinfo_id', 60)->nullable();
                $table->string('provider_account_id', 11)->nullable();
                $table->string('philhealth_id', 60)->nullable();
                $table->string('philhealth_category', 32)->nullable();
                $table->string('member_type', 32)->nullable();
                $table->string('ccash_transfer', 32)->nullable();
                $table->string('benefit_type', 32)->nullable();
                $table->string('pamilya_pantawid_id', 32)->nullable();
                $table->string('indigenous_group', 32)->nullable();
                $table->string('benefactor_member_id', 32)->nullable();
                $table->string('benefactor_relation', 5)->nullable();
                $table->string('benefactor_first_name', 32)->nullable();
                $table->string('benefactor_last_name', 32)->nullable();
                $table->string('benefactor_middle_name', 32)->nullable();
                $table->string('benefactor_name_suffix', 32)->nullable();
                $table->boolean('For_Enlistment')->nullable();
                $table->text('application_form')->nullable();

                $table->softDeletes();
                $table->timestamps();
                $table->unique('philhealth_id');
            });
        }

        if (Schema::hasTable('philhealth_pmrf')!=TRUE) {
            Schema::create('philhealth_pmrf', function (Blueprint $table) {
                $table->increments('id');
                $table->string('philhealthpmrf_id', 60);
                $table->string('Pat_Facility_No', 60)->nullable();
                $table->string('Encounter_ID', 60)->nullable();
                $table->date('app_date')->nullable();
                $table->string('patient_id', 60)->nullable();
                $table->string('MEMID_NO', 60)->nullable();
                $table->string('Purpose', 60)->nullable();
                $table->string('LASTNAME', 60)->nullable();
                $table->string('FIRSTNAME', 60)->nullable();
                $table->string('MIDDLENAME', 60)->nullable();
                $table->string('SUFFIX', 10)->nullable();
                $table->string('Maiden_Last_Name', 60)->nullable();
                $table->string('Maiden_First_Name', 60)->nullable();
                $table->string('Maiden_Midde_Name', 60)->nullable();
                $table->date('BIRTH_DATE')->nullable();
                $table->string('BIRTH_COUNTRY', 10)->nullable();
                $table->string('POB_PROV', 10)->nullable();
                $table->string('POB_MUNI', 10)->nullable();
                $table->string('POB_ZIPCODE', 10)->nullable();
                $table->string('SEX', 1)->nullable();
                $table->string('MARITAL_STATUS', 1)->nullable();
                $table->string('NATIONALITY', 5)->nullable();
                $table->string('religion', 60)->nullable();
                $table->string('bloodtype', 60)->nullable();
                $table->string('gsis_no', 60)->nullable();
                $table->string('sss_no', 60)->nullable();
                $table->string('mec_no', 60)->nullable();
                $table->string('pnp_no', 60)->nullable();
                $table->string('TIN_NO', 60)->nullable();
                $table->string('PRES_UNIT', 20)->nullable();
                $table->string('PRES_BLDG', 40)->nullable();
                $table->string('PRES_HOUSE_NO', 35)->nullable();
                $table->string('PRES_STREET', 200)->nullable();
                $table->string('PRES_SUBD', 50)->nullable();
                $table->string('PRES_REG_PSGC', 2)->nullable();
                $table->string('PRES_PROV_PSGC', 4)->nullable();
                $table->string('PRES_MUN_PSGC', 6)->nullable();
                $table->string('PRES_BGY', 9)->nullable();
                $table->string('PRES_COUNTRY', 20)->nullable();
                $table->string('PRES_ZIPCODE', 4)->nullable();
                $table->string('UNIT', 20)->nullable();
                $table->string('BLDG', 40)->nullable();
                $table->string('address', 100)->nullable();
                $table->string('HOUSE_NO', 35)->nullable();
                $table->string('STREET', 200)->nullable();
                $table->string('SUBD', 50)->nullable();
                $table->string('REGION_PSGC', 2)->nullable();
                $table->string('PROVINCE_PSGC', 4)->nullable();
                $table->string('MUNICIPALITY_PSGC', 6)->nullable();
                $table->string('BARANGAY', 9)->nullable();
                $table->string('RESI_COUNTRY', 20)->nullable();
                $table->string('ZIP_CODE', 10)->nullable();
                $table->string('Pat_Contact_Landline', 60)->nullable();
                $table->string('Pat_Contact_Mobile', 60)->nullable();
                $table->string('Pat_Contact_EmailAddress', 60)->nullable();
                $table->string('Spouse_MemID_No', 60)->nullable();

                $table->string('MEM_CATEGORY', 2)->nullable();
                $table->string('PREMIS_MEM_CATEGORY', 1)->nullable();
                $table->string('PREMIS_mem_type', 1)->nullable();
                $table->text('InfEco_InfSector_Specify')->nullable();
                $table->decimal('InfEco_InfSector_Specify_MoIncome', 8,2)->nullable();
                $table->string('InfEco_InfSector_Specify_NoIncome', 1)->nullable();
                $table->text('InfEco_SelfEarn_Specify')->nullable();
                $table->decimal('InfEco_SelfEarn_MoIncome', 8,2)->nullable();
                $table->text('InfEco_OrgGroup_Specify')->nullable();
                $table->text('Sponsored_LGU')->nullable();
                $table->text('Sponsored_NGA')->nullable();
                $table->text('Sponsored_Others_Specify')->nullable();
                $table->date('Lifetime_Member_RetirementDate')->nullable();
                $table->string('foreign_address', 100)->nullable();
                $table->string('foreign_tel_no', 15)->nullable();
                $table->string('country_code', 5)->nullable();
                $table->date('cov_start')->nullable();
                $table->date('cov_end')->nullable();
                $table->integer('memdep_no')->nullable();
                $table->string('status', 1)->nullable();
                $table->string('pmrf_status', 1)->nullable();

                $table->softDeletes();
                $table->timestamps();
                $table->unique('philhealthpmrf_id');
            });
        }

        if (Schema::hasTable('philhealth_pmrf_dependents')!=TRUE) { 
            Schema::create('philhealth_pmrf_dependents', function (Blueprint $table) {
                $table->increments('id');
                $table->string('philhealthpmrf_dep_id', 60);
                $table->string('Pat_Facility_No', 25)->nullable();
                $table->string('Encounter_ID', 100)->nullable();
                $table->string('MEMID_NO', 14)->nullable();
                $table->string('Dep_MemID_No', 14)->nullable();
                $table->string('Purpose', 1)->nullable();
                $table->string('Dep_Lastname', 100)->nullable();
                $table->string('Dep_Firstname', 100)->nullable();
                $table->string('Dep_Middlename', 100)->nullable();
                $table->string('Dep_Suffix', 7)->nullable();
                $table->string('maidenname', 60)->nullable();
                $table->string('Dep_Disability', 1)->nullable();
                $table->string('disability', 60)->nullable();
                $table->date('Dep_birthdate')->nullable();
                $table->string('Dep_Sex', 1)->nullable();
                $table->string('prov_code', 4)->nullable();
                $table->string('citymun_code', 6)->nullable();
                $table->string('relation', 1)->nullable();
                $table->string('MARITAL_STATUS', 1)->nullable();
                $table->string('status', 1)->nullable();
                $table->string('emp_stat', 1)->nullable();
                $table->string('occupation', 30)->nullable();
                $table->decimal('income', 8,2)->nullable();
                $table->softDeletes();
                $table->timestamps();
                $table->unique('philhealthpmrf_dep_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_philhealthinfo');
        Schema::dropIfExists('philhealth_pmrf');
        Schema::dropIfExists('philhealth_pmrf_dependents');
    }
}
