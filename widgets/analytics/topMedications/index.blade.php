<div class="box box-primary"><!--Consultations-->
    <div class="box-header">
        <div class="pull-right box-tools">
            <input type="number" value="10" id="topMedicationsNumberInput" class="" title="Number"/>
            <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button> -->
            <button id="medicationCountDatePicker" class="btn btn-primary btn-sm daterange medicationCountDatePicker" title="Date range"><i class="fa fa-calendar"></i></button>
        </div>
        <i class="fa fa-stethoscope"></i><h3 class="box-title text-shine-blue topMedicationsBoxTitle">Top Medications Count</h3>
    </div><!-- /.box-header -->

    <div class="box-body no-padding">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#topMedicationsTableCount" data-toggle="tab">Table</a></li>
                <li class=""><a href="#topMedicationsBarCount" data-toggle="tab">Bar Chart</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="topMedicationsTableCount">
                    <table id="medicationCount" class="table table-condensed">
                        <thead>
                            <tr><td colspan="4"><h4 class="topMedicationsBoxTitle">Top Medications This Month</h4></td></tr>
                            <tr>
                                <th>#</th>
                                <th>Medications</th>
                                <th style="text-align:right">Sum</th>
                                <th style="text-align:right">Percentage</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($medicationCount as $key => $diagnoses)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $diagnoses->brand_name }}</td>
                                    <td style="text-align:right">{{ $diagnoses->counter }}</td>
                                    <td style="text-align:right">{{ number_format(($diagnoses->counter/$totalCount)*100, 2, '.', '')  }} %</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.tab-pane -->
                <div class="tab-pane" id="topMedicationsBarCount">
                    <table id="medicationCount" class="table table-condensed">
                        <thead>
                            <tr><td colspan="4"><h4 class="topMedicationsBoxTitle">Top Medications This Month</h4></td></tr>
                        </thead>
                    </table>
                    <div class="chart">
                        <canvas id="topMedicationsBarChart"></canvas>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
                </div><!-- /.tab-pane -->
            </div>
        </div>
    </div><!-- /.box-body -->

</div><!--/. end consultations-->

<script>
$(document).ready(function() {
    var max = document.getElementById('topMedicationsNumberInput');
    $('#medicationCountDatePicker').daterangepicker(
        {
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1,'days'), moment().subtract(1,'days')],
                'Last 7 Days': [moment().subtract(6,'days'), moment()],
                'Last 30 Days': [moment().subtract(29,'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last 3 Months': [moment().subtract(2,'month').startOf('month'), moment().endOf('month')],
                'Last 6 Months': [moment().subtract(5,'month').startOf('month'), moment().endOf('month')],
                'Last 12 Months': [moment().subtract(11,'month').startOf('month'), moment().endOf('month')]
            },
            startDate: moment(),
            endDate: moment()
        },
        function(start, end, label) {
            $.ajax({
                type: "GET",
                url: "<?php site_url(); ?>enhancedanalytics/gettopmedications/"+start.format('YYYY-MM-DD')+"/"+end.format('YYYY-MM-DD')+"/"+max.value,
                beforeSend: function( xhr ) {
                    max = document.getElementById('topMedicationsNumberInput');
                    $('h4.topMedicationsBoxTitle').text("Retrieving records starting from "+start.format('MMM DD, YYYY')+ " to "+end.format('MMM DD, YYYY') );
                    $('#medicationCount tbody').html("<tr><td colspan='4'><i class='fa fa-spinner fa-pulse fa-fw'></i> Loading. Please wait...</td></tr>");
                }
            })
            .done(function( msg ) {
                $('#medicationCount tbody').removeClass('provider_lister_box_loading');
                $('h3.topMedicationsBoxTitle').text("Top " +max.value+ " Medications Count");
                $('h4.topMedicationsBoxTitle').text("Medications starting from "+start.format('MMM DD, YYYY')+ " to "+end.format('MMM DD, YYYY') );
                $('#medicationCount tbody').html(msg);

                updatetopMedicationsBarChart();
            });
    });

    function generateLabelsForBar() {
        var labels = [];
        var table = document.getElementById('medicationCount');
        for (var r = 2, n = table.rows.length; r < n; r++) {
          if(table.rows[r].cells[1] != null)
          {
            labels.push(table.rows[r].cells[1].innerHTML);
          }
        }        
        return labels;
    }
    function generateDataForBar() {
        var data = [];
        var table = document.getElementById('medicationCount');
        for (var r = 2, n = table.rows.length; r < n; r++) {
          if(table.rows[r].cells[2] != null)
          {
            data.push(table.rows[r].cells[2].innerHTML);
          }
        }       
        return data;
    }

    function updatetopMedicationsBarChart()
    {
        //-------------
        //- BAR CHART -
        //-------------
        var topMedicationsBarChartData = {
          labels: generateLabelsForBar(),
          datasets: [
            {
              label: "Top Medications",
              fillColor: "rgba(60,141,188,0.9)",
              strokeColor: "rgba(60,141,188,0.8)",
              pointColor: "#3b8bba",
              pointStrokeColor: "rgba(60,141,188,1)",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(60,141,188,1)",
              data: generateDataForBar()
            }
          ]
        };
        $('#topMedicationsBarChart').remove(); // this is my <canvas> element
        $('#topMedicationsBarCount').append('<canvas id="topMedicationsBarChart"></canvas>'); //append canvas
        var topMedicationsBarChartCanvas = $("#topMedicationsBarChart").get(0).getContext("2d");
        var topMedicationsBarChart = new Chart(topMedicationsBarChartCanvas);
        var topMedicationsBarChartOptions = {
          //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
          scaleBeginAtZero: true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines: true,
          //String - Colour of the grid lines
          scaleGridLineColor: "rgba(0,0,0,.05)",
          //Number - Width of the grid lines
          scaleGridLineWidth: 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines: true,
          //Boolean - If there is a stroke on each bar
          barShowStroke: true,
          //Number - Pixel width of the bar stroke
          barStrokeWidth: 2,
          //Number - Spacing between each of the X value sets
          barValueSpacing: 5,
          //Number - Spacing between data sets within X values
          barDatasetSpacing: 1,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          //Boolean - whether to make the chart responsive
          responsive: true,
          maintainAspectRatio: true
        };

        topMedicationsBarChartOptions.datasetFill = false;
        topMedicationsBarChart.Bar(topMedicationsBarChartData, topMedicationsBarChartOptions);
    }

    $('[href=#topMedicationsBarCount]').on('shown.bs.tab', function (e) {
        updatetopMedicationsBarChart()
    });
});
</script>
