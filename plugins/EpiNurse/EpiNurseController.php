<?php

namespace Plugins\EpiNurse;

use ShineOS\Core\Patients\Entities\Patients;

use Plugins\EpiNurse\EpiNurseModel;
use Shine\Repositories\Eloquent\UserRepository as UserRepository;
use Shine\Repositories\Contracts\UserRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\Libraries\UserHelper;
use Shine\Libraries\FacilityHelper;
use Shine\User;
use Shine\Plugin;
use Shine\Libraries\Utils;

class EpiNurseController extends Controller
{

    protected $moduleName = 'Patients';
    protected $modulePath = 'patients';

    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
        $this->middleware('auth');

        $this->userInfo = UserHelper::getUserInfo();
        $this->facility = FacilityHelper::facilityInfo();
        $this->facilityInfo = $this->facility->facility_id;
    }

    public function view($id)
    {
        $data['epinurse'] = EpiNurseModel::where('patient_id','=',$id)->first();

        View::addNamespace('pluginform', plugins_path().'EpiNurse');
        echo View::make('pluginform::master')->with($data);
    }


}
