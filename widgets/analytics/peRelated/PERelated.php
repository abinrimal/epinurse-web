<?php
namespace Widgets\analytics\PERelated;

use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use Plugins\SchoolAccident\SchoolAccidentModel;
use Shine\Repositories\Eloquent\UserRepository as UserRepository;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\FacilityRepositoryInterface;
use Shine\Libraries\HealthcareservicesHelper;
use Shine\Libraries\FacilityHelper;
use Arrilot\Widgets\AbstractWidget;
use View, Form, Response, Validator, Input, Mail, Session, Redirect, Hash, Auth, DB, Datetime, Request, Storage, Schema;

//DO NOT FORGET TO UNCOMMENT LOOP WHEN DONE WITH REPOSITORIES
class PERelated extends AbstractWidget
{
    private $UserRepository;
    private $healthcareRepository;

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function placeholder()
    {
        $loading = '<div class="box box-primary"><!--Consultations-->
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-cog fa-spin fa-fw"></i> Loading widget...</h3>
                </div>
            </div>';


        return $loading;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(UserRepository $UserRepository, HealthcareRepository $healthcareRepository)
    {

        $start = date('Y-m-01');
        $end = date('Y-m-d', strtotime('last day of this month'));
        $values = SchoolAccidentModel::getPERelatedCases($start, $end);
        $visits = $values['visits'];
        $total = $values['total'];
        $kind_of_injury = SchoolAccidentModel::getKindOfInjury();

        View::addNamespace('pe_related', 'widgets/analytics/PERelated/');
        return view("pe_related::index", [
            'config' => $this->config,
            'visits' => $visits,
            'total' => $total,
            'injuryKind' => $kind_of_injury
        ]);
    }
}
