@extends('users::layouts.master')
@section('title') SHINE OS+ | Registration @stop
@section('content')

<?php
    //RJBS solution for form data
    //for clean-up
    $enteredData = Session::get('enteredData');
    if($enteredData) {
        $DOH_facility_code = $enteredData['DOH_facility_code'];
        $facility_name = $enteredData['facility_name'];
        $provider_type = $enteredData['provider_type'];
        $ownership_type = $enteredData['ownership_type'];
        $facility_type = $enteredData['facility_type'];
        // $phic_accr_id = $enteredData['phic_accr_id'];
        $first_name = $enteredData['first_name'];
        $last_name = $enteredData['last_name'];
        $password = $enteredData['password'];
        $email = $enteredData['email'];
        $phone = $enteredData['phone'];
        $mobile = $enteredData['mobile'];
        // $region = $enteredData['region'];
        $province = $enteredData['province'];
        $city = $enteredData['city'];
        $barangay = $enteredData['barangay'];
        $ward_no = $enteredData['ward_no'];
        $district = $enteredData['district'];
    } else {
        $DOH_facility_code = '';
        $facility_name = '';
        $provider_type = '';
        $ownership_type = '';
        $facility_type = '';
        $phic_accr_id = '';
        $first_name = '';
        $last_name = '';
        $email = '';
        $phone = '';
        $mobile = '';
        $password = '';
        // $region = NULL;
        $province = NULL;
        $city = NULL;
        $barangay = NULL;
        $ward_no = NULL;
        $district = NULL;
    }
?>
<style>
    body {
        color: #333;
        background: #FFF;
        /*background: -moz-linear-gradient(top, #d2d6de 1%, #A9ABB6 100%);
        background: -webkit-linear-gradient(top, #d2d6de 1%,#A9ABB6 100%);
        background: linear-gradient(to bottom, #d2d6de 1%,#A9ABB6 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d2d6de', endColorstr='#A9ABB6',GradientType=0 );*/
    }
    img.shinelogo {
        height: 50px !important;
        margin: auto;
    }
    .form-control {
        background-color: #FFF; !important;
        border-color: #AAA !important;
        color: #333;
    }
    .input-group .input-group-addon {
        background-color: #DDD !important;
        border-color: #AAA !important;
        color: #222;
        cursor: pointer;
    }
    i.fa {
        color:#fff !important;
        font-size: 18px;
        width: 14px;
    }
    #captcha-image {
        opacity: 0.8 !important;
        height: 34px;
    }

    .popover {
        color: #000 !important;
    }
    .fa-refresh {
        margin-left: 10px;
    }
    select option {
        color: #000;
    }
    .help-block {
        position: relative !important;
        text-align: right !important;
        margin-top:0px;
        display: inline-block;
        margin-left: 25px;
    }
    .has-error .form-control {
        margin-bottom:0px;
    }

    .regbtn {
        padding: 8px 10px;
        border-color: #AAA !important;
        height: 36px !important;
    }
    .toggler {
        margin-bottom: 0;
        display: block;
    }
    .has-error .toggler {
        margin-bottom: 13px;
    }
    .btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
        /*border-top-left-radius: 0;
        border-bottom-left-radius: 0;*/
    }
    .input-group div .form-control:first-child {
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
    }
    .jumbotron {
        background-color:#FFF;
        color:#333;
        padding:20px !important;
        text-align: center;
        margin:10px;
        box-shadow: 0px 0px 4px rgba(0,0,0,.2);
    }
    .jumbotron h3 {
        font-size: 20px;
    }
    .jumbotron p {
        font-size: 13px !important;
    }
    .reg-jumbotron {
        padding:20px !important;
    }
    .top20 {
        margin-top:20px;
    }
    .bottom20 {
        margin-bottom:20px;
    }
    .for_government {
        display: none;
    }
    hr {
        margin-top:3px;
        margin-bottom:3px;
    }
    .input-group .form-control {
        width:50%;
    }
    .filebrowse {
        width: 100%;
        height: 34px;
        padding: 0px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        border: none;
        border-radius: 4px;
        margin:14px 0;
    }
</style>
<div class="container">
    {!! Form::open(array( 'url'=>$modulePath.'/register', 'id'=>'crudForm', 'name'=>'crudForm', 'class'=>'form-horizontal', 'files'=>true )) !!}
        <div class="row top20">
            <div class="reg-jumbotron text-center">
                <img src="{{ asset('public/uploads/customizations/epinurse/epinurse.png') }}" class="shinelogo img-responsive" />
                <h3>Provider Registration</h3>
                <p>Registration is subject to validation, review and approval. User accounts will be sent by email once it has been approved within <b>48 hours</b> of registration.<br />
                All <b>fields</b> are <b>required</b></p>
            </div>
        </div>
        <div class="row bottom20">

            <div class="col-md-4  bottom20">
                <div class="jumbotron">
                    @if (Session::has('message'))
                        <div class="alert alert-dismissible alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif
                    @if (Session::has('warning'))
                        <div class="alert alert-dismissible alert-warning">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <p>{{ Session::get('warning') }}</p>
                        </div>
                    @endif

                    <h4>REGISTRATION INFORMATION</h4>

                    <div class="form-group">
                        <div class="btn-group btn-group-justified toggler" data-toggle="buttons">
                          <label class="btn btn-default regbtn required @if($ownership_type == 'government') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" class="required ownership_type" name="ownership_type" id="" autocomplete="off" value="government" required="required" @if($ownership_type == 'government') checked="checked" @endif> Government
                          </label>
                          <label class="btn btn-default regbtn required @if($ownership_type == 'private') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" class="required ownership_type" name="ownership_type" id="" autocomplete="off" value="private" required="required"  @if($ownership_type == 'private') checked="checked" @endif> Private
                          </label>
                        </div>
                    </div>

                    <div class="for_government">
<!--                         <div class="form-group">
                            <input type="text" data-mask="" data-inputmask="'mask': 'DOH000000000099999'" placeholder="DOH Facility Code. Last 5 digits only." class="form-control" id="DOH_facility_code" name="DOH_facility_code" value="{{ $DOH_facility_code }}" />
                        </div>
                        <small>If you do not know your DOH Facility Code, you can check it from this site: <a href="http://nhfr.doh.gov.ph/" target="_blank">NHFR</a></small>
                        <hr />
                        <p class="text-left">You are required to engage us thru a Memorandum of Agreement (MOA), Engagement Form and Service Level Agreement. After registration, you will receive by email all necesssary documents and forms you need to have them sign by your Mayor or Health Officer.</p>
                        <p class="text-left"> You will also need to receive instructions how register to PHIE so you can submit to Philhealth and DOH.</p> -->
                    </div>

                    <div class="for_private">
<!--                         <p class="text-left">As a private facility or practitioner, we will require you to attached a scanned image of your PTR License during this registration.</p>
                        {!! Form::file('private_provider_ptr', array('id'=>'ptrImage', 'class'=>'filebrowse ptrImage')) !!}
                        <p class="text-left">If you are part of an educational institution or student who want to use SHINE OS+ for your courses, please contact us at inquiry@shine.ph.<br />You may also check our demo at http://www.shine.ph/demo before registering.</p> -->
                    </div>

                    <div class="instruct">
<!--                         <p class="text-left">Please choose the type of facility you want to register. There will be specific requirements depending on your chosen type.</p>
                        <p class="text-left">If you are part of an educational institution or student who want to use SHINE OS+ for your courses, please contact us at inquiry@shine.ph.<br />You may also check our demo at http://www.shine.ph/demo before registering.</p> -->
                    </div>

                </div>
            </div>

            <div class="col-md-4 bottom20">
                <div class="jumbotron">
                    <h4>PROVIDER INFO</h4>
                    <div class="form-group">
                        <input type="text" placeholder="Facility Name" class="form-control required" id="facility_name" value="{{ $facility_name }}" name="facility_name" required />
                    </div>

                    <div class="form-group">
                        {!! Form::select('provider_type', array(NULL => 'Select Provider Type', 'facility' => 'Facility', 'individual' => 'Individual'), $provider_type, ['class' => 'required form-control', 'required'=>'required']) !!}
                    </div>

                    <div class="form-group">
                        <?php $arr_facility_type = array_merge(array("NULL"=>"Select Facility Type"), $arr_facility_type->toarray()); ?>

                        {!! Form::select('facility_type', $arr_facility_type, $facility_type, ['class' => 'populate placeholder required form-control']) !!}
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6" style="float:left; padding-right:7px !important;">
                            <input type="text" placeholder="Ward No." class="form-control required" id="ward_no" value="{{ $ward_no }}" name="ward_no" required />
                        </div>
                        <div class="form-group col-md-6" style="float:left; padding-left:7px !important;">
                            <input type="text" placeholder="Municipality" class="form-control required" id="city" value="{{ $city }}" name="city" required />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6" style="float:left; padding-right:7px !important;">
                            <input type="text" placeholder="District" class="form-control required" id="district" value="{{ $district }}" name="district" required />
                        </div>

                        <div class="form-group col-md-6" style="float:left; padding-left:7px !important;">
                            <input type="text" placeholder="Province" class="form-control required" id="province" value="{{ $province }}" name="province" required />
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-4 bottom20">
                <div class="jumbotron">
                    <h4>ADMINISTRATOR ACCOUNT</h4>
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" placeholder="First Name" class="form-control required" id="first_name" value="{{ $first_name }}" name="first_name" />
                            <input type="text" placeholder="Last Name" class="form-control required" id="last_name" value="{{ $last_name }}" name="last_name" />
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="text" placeholder="Administrator Email Address" class="form-control required" id="email" value="{{ $email }}" name="email" />
                    </div>

                    <div class="form-group">
                        <input type="text" name="phone" data-mask="" class="form-control" placeholder="Telephone" value="{{ $phone }}" />
                    </div>

                    <div class="form-group">
                        <input type="text" name="mobile" data-mask="" class="form-control required" placeholder="Mobile" value="{{ $mobile }}" required />
                    </div>

                    <div class="form-group">
                        <input type="password" placeholder="Administrator Password" class="form-control required password" id="password" value="" name="password" />
                    </div>

                    <div class="form-group">
                        <input type="password" placeholder="Confirm Administrator Password" class="form-control required confirmPassword" id="password_confirm" value="" name="password_confirm" />
                    </div>
                    <div class="form-group">
                        <p> {!! Form::checkbox('user_doctor', 1, false, ['class' => 'field']) !!} I am a Doctor</p>
                    </div>
                    <div class="form-group">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 form-group text-center">
            <div class="col-md-12" style="padding-bottom: 10px">
                By clicking <strong class="label label-success">Register</strong>, you agree with the <a href="http://www.shine.ph/shineph/terms-of-use" target="_blank"> Terms and Conditions of Use </a> set out by this site.
            </div>
            <div style="display:inline-block;">
                <div class="has-succes col-md-6">
                    <img src="{{ url('registration/captcha') }}" alt="Please verify if you are a human" id="captcha-image" />
                    <a href="#" class="fa fa-refresh captcha-refresh">&nbsp;</a>
                </div>
                <div class="col-md-6">
                    <input id="test_captcha" placeholder="Copy the code." name="test_captcha" value="" type="text" class="form-control required captcha" />
                </div>
            </div>
            <div style="display:inline-block;top: -20px;position: relative;">
                <input type="submit" name="Register" id="btnRegister" class="btn btn-success" value="Register" />
                <a href="{{ url('/') }}" class="btn btn-warning">Cancel</a>
            </div>
        </div>
    {!! Form::close() !!}
</div>
@stop


@section('footer')
    <script src="{{ asset('public/dist/plugins/input-mask/inputmask.js') }}"></script>
    <script src="{{ asset('public/dist/plugins/input-mask/inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('public/dist/plugins/input-mask/inputmask.extensions.js') }}"></script>
    <script src="{{ asset('public/dist/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('public/dist/js/pages/registration.js') }}"></script>
@stop

