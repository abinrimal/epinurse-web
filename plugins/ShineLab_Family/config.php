<?php

$plugin_module = 'patients';
$plugin_title = 'Patient Family Record';
$plugin_id = 'Family';
$plugin_location = 'newdata';
$plugin_primaryKey = 'patient_id';
$plugin_table = 'patient_familyinfo';  //let us use the standard plugin table

$plugin_description = 'The Family Info Plugin collects additional data about the family of a patient. It includes parental names and emergency information. The plugin uses its own table for storing data.';
$plugin_version = '1.0';
$plugin_developer = 'MedixHealth';
$plugin_url = 'http://www.medixserve.com';
$plugin_copy = "2016";
$plugin_folder = 'ShineLab_Family';
