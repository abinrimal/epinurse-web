@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<table class="overflow-y table-striped table-bordered table-report" id="diseaseTable" width='100%'>
    <thead>
        <TR>
            <TH ROWSPAN="2" class="text-center">DISEASE</TH>
            <TH ROWSPAN="2" class="text-center">ICD 10 Code</TH>
            <TH COLSPAN="2" class="text-center">Under 1</TH>
            <TH COLSPAN="2" class="text-center">1-4</TH>
            <TH COLSPAN="2" class="text-center">5-9</TH>
            <TH COLSPAN="2" class="text-center">10-14</TH>
            <TH COLSPAN="2" class="text-center">15-19</TH>
            <TH COLSPAN="2" class="text-center">20-24</TH>
            <TH COLSPAN="2" class="text-center">25-29</TH>
            <TH COLSPAN="2" class="text-center">30-34</TH>
            <TH COLSPAN="2" class="text-center">35-39</TH>
            <TH COLSPAN="2" class="text-center">40-44</TH>
            <TH COLSPAN="2" class="text-center">45-49</TH>
            <TH COLSPAN="2" class="text-center">50-54</TH>
            <TH COLSPAN="2" class="text-center">55-59</TH>
            <TH COLSPAN="2" class="text-center">60-64</TH>
            <TH COLSPAN="2" class="text-center">65+</TH>
            <TH COLSPAN="3" class="text-center">TOTAL</TH>
        </TR>
        <TR>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH>
            <TH>F</TH><TH>M</TH><TH>Total</TH>
        </TR>
    </thead>
    <tbody>
    @if(array_key_exists('diseases',$data))
        @foreach($data['diseases'] as $disease_key => $disease_value)
        <tr>
            <td>
                {{ $disease_key }}
            </td>
            <td>
                {{ $disease_value['ICD10_CODE'] }}
            </td>
            <td>
                {{ $disease_value['UNDER1_F'] }}
            </td>
            <td>
                {{ $disease_value['UNDER1_M'] }}
            </td>
            <td>
                {{ $disease_value['1_4_F'] }}
            </td>
            <td>
                {{ $disease_value['1_4_M'] }}
            </td>
            <td>
                {{ $disease_value['5_9_F'] }}
            </td>
            <td>
                {{ $disease_value['5_9_M'] }}
            </td>
            <td>
                {{ $disease_value['10_14_F'] }}
            </td>
            <td>
                {{ $disease_value['10_14_M'] }}
            </td>
            <td>
                {{ $disease_value['15_19_F'] }}
            </td>
            <td>
                {{ $disease_value['15_19_M'] }}
            </td>
            <td>
                {{ $disease_value['20_24_F'] }}
            </td>
            <td>
                {{ $disease_value['20_24_M'] }}
            </td>
            <td>
                {{ $disease_value['25_29_F'] }}
            </td>
            <td>
                {{ $disease_value['25_29_M'] }}
            </td>
            <td>
                {{ $disease_value['30_34_F'] }}
            </td>
            <td>
                {{ $disease_value['30_34_M'] }}
            </td>
            <td>
                {{ $disease_value['35_39_F'] }}
            </td>
            <td>
                {{ $disease_value['35_39_M'] }}
            </td>
            <td>
                {{ $disease_value['40_44_F'] }}
            </td>
            <td>
                {{ $disease_value['40_44_M'] }}
            </td>
            <td>
                {{ $disease_value['45_49_F'] }}
            </td>
            <td>
                {{ $disease_value['45_49_M'] }}
            </td>
            <td>
                {{ $disease_value['50_54_F'] }}
            </td>
            <td>
                {{ $disease_value['50_54_M'] }}
            </td>
            <td>
                {{ $disease_value['55_59_F'] }}
            </td>
            <td>
                {{ $disease_value['55_59_M'] }}
            </td>
            <td>
                {{ $disease_value['60_64_F'] }}
            </td>
            <td>
                {{ $disease_value['60_64_M'] }}
            </td>
            <td>
                {{ $disease_value['65ABOVE_F'] }}
            </td>
            <td>
                {{ $disease_value['65ABOVE_M'] }}
            </td>
            <td>
                {{ $total_F = $disease_value['UNDER1_F'] + $disease_value['1_4_F'] + $disease_value['5_9_F'] + $disease_value['10_14_F'] + $disease_value['15_19_F'] + $disease_value['20_24_F'] + $disease_value['25_29_F'] + $disease_value['30_34_F'] + $disease_value['35_39_F'] + $disease_value['40_44_F'] + $disease_value['45_49_F'] + $disease_value['50_54_F'] + $disease_value['55_59_F'] + $disease_value['60_64_F'] + $disease_value['65ABOVE_F'] }}
            </td>
            <td>
                {{ $total_M = $disease_value['UNDER1_M'] + $disease_value['1_4_M'] + $disease_value['5_9_M'] + $disease_value['10_14_M'] + $disease_value['15_19_M'] + $disease_value['20_24_M'] + $disease_value['25_29_M'] + $disease_value['30_34_M'] + $disease_value['35_39_M'] + $disease_value['40_44_M'] + $disease_value['45_49_M'] + $disease_value['50_54_M'] + $disease_value['55_59_M'] + $disease_value['60_64_M'] + $disease_value['65ABOVE_M'] }}
            </td>
            <td>
                {{ $total_F + $total_M }}
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="34">
            NO DATA FOUND
            </td>
        </tr>
            
        @endif
    </tbody>
</table> @yield('modal-footer')