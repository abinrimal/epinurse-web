<?php

namespace Plugins\EpiNurse;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB, Input;

use Shine\Libraries\IdGenerator;
use Plugins\EpiNurse\EpiNurse;

use Webpatser\Uuid\Uuid;

class EpiNurseModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'epinurse_personal';
    protected static $table_name = 'epinurse_personal';
    protected $primaryKey = 'patient_id';
    protected $touches = array('patients');
    
    public function patients()
    {
        return $this->belongsTo('ShineOS\Core\Patients\Entities\Patients','patient_id','patient_id');
    }

    public static function saveplugin( $data, $id=NULL )
    {
        $ph = $data;

        $epinurse = self::where('patient_id',$ph['patient_id'])->first();
        if(!$epinurse)
        {
            $epinurse = new self;
            $epinurse->epinursepersonal_id = IdGenerator::generateId();
            $epinurse->patient_id = $ph['patient_id'];
            $epinurse->uuid = Uuid::generate(4)->string;
        }

        $epinurse->marital_status = isset($ph['marital_status']) && $ph['marital_status'] != '' ? $ph['marital_status'] : NULL;
        $epinurse->ethnicity = isset($ph['ethnicity']) ? $ph['ethnicity'] : NULL;
        $epinurse->religion = isset($ph['religion']) ? $ph['religion'] : NULL;
        $epinurse->educational_status = isset($ph['educational_status'])  && $ph['educational_status'] != '' ? $ph['educational_status'] : NULL;
        $epinurse->occupation = isset($ph['occupation']) && $ph['occupation'] != '' ? $ph['occupation'] : NULL;
        
        $epinurse->income_agriculture = isset($ph['income_agriculture']) ? $ph['income_agriculture'] : NULL;
        $epinurse->income_business = isset($ph['income_business']) ? $ph['income_business'] : NULL;
        $epinurse->income_service = isset($ph['income_service']) ? $ph['income_service'] : NULL;
        $epinurse->income_labor = isset($ph['income_labor']) ? $ph['income_labor'] : NULL;
        $epinurse->income_remittance = isset($ph['income_remittance']) ? $ph['income_remittance'] : NULL;
        $epinurse->income_others = isset($ph['income_others']) ? $ph['income_others'] : NULL;

        $epinurse->income_specify = isset($ph['income_specify']) ? $ph['income_specify'] : NULL;
        $epinurse->food_sufficiency = isset($ph['food_sufficiency']) && $ph['food_sufficiency'] != '' ? $ph['food_sufficiency'] : NULL;
        $epinurse->family_id = isset($ph['family_id']) ? $ph['family_id'] : NULL;
        $epinurse->type_of_family = isset($ph['type_of_family']) && $ph['type_of_family'] != '' ? $ph['type_of_family'] : NULL;
        $epinurse->fathers_name = isset($ph['fathers_name']) ? $ph['fathers_name'] : NULL;
        $epinurse->mothers_name = isset($ph['mothers_name']) ? $ph['mothers_name'] : NULL;
        $epinurse->local_guardians_name = isset($ph['local_guardians_name']) ? $ph['local_guardians_name'] : NULL;
        $epinurse->contact_of_parent_guardian = isset($ph['contact_of_parent_guardian']) ? $ph['contact_of_parent_guardian'] : NULL;
        
        $epinurse->number_male = isset($ph['number_male']) ? $ph['number_male'] : NULL;
        $epinurse->number_female = isset($ph['number_female']) ? $ph['number_female'] : NULL;
        $epinurse->number_others = isset($ph['number_others']) ? $ph['number_others'] : NULL;
        $epinurse->number_male_under5 = isset($ph['number_male_under5']) ? $ph['number_male_under5'] : NULL;
        $epinurse->number_female_under5 = isset($ph['number_male_under5']) ? $ph['number_male_under5'] : NULL;
        $epinurse->number_male_above60 = isset($ph['number_male_above60']) ? $ph['number_male_above60'] : NULL;
        $epinurse->number_female_above60 = isset($ph['number_female_above60']) ? $ph['number_female_above60'] : NULL;
        $epinurse->number_disabled = isset($ph['number_disabled']) ? $ph['number_disabled'] : NULL;
        $epinurse->number_pregnant = isset($ph['number_pregnant']) ? $ph['number_pregnant'] : NULL;
        $epinurse->number_lactating_mothers = isset($ph['number_lactating_mothers']) ? $ph['number_lactating_mothers'] : NULL;

        $epinurse->save();
    }


}
