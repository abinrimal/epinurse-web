<!DOCTYPE html>
<html>
  <head>
      @include('partials.head')
  </head>
  <body class="skin-blue printer">
    <div class="wrapper">
      <!-- Content Wrapper. Contains page content -->
      <div class="container">
        <section class="content">
          @yield('page-header')
          @yield('content')
        </section>
      </div>
    </div>

    @include('partials.footer')
  </body>
</html>
