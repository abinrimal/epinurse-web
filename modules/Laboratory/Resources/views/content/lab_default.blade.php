<?php

  $lab_filename = NULL;
  $hasResult = 0;
  $lab_id = NULL;
  $medlab_id = NULL;

  if(isset($testType) AND isset($lab[$testType]) AND $lab[$testType]!=NULL) {
    foreach ($lab as $b) {
        $lab_filename = $b->filename;
        $json_lab_data = json_decode($b->lab_data);
        $lab_id = $b->laboratoryresult_id;
        $hasResult = 1;
        foreach ($json_lab_data as $key => $value) {
          $data[$key] = $value;
        }
    }
  }

  if(isset($testType)) {
    $medlab_id = $medicalorderlaboratoryexam_id;
  }
?>

<input type="hidden" name="medicalorderlaboratoryexam_id" id="medicalorderlaboratoryexam_id" value="{{ $medlab_id }}"/>
@if(isset($testType) AND isset($lab[$testType]->LaboratoryResult->laboratoryresult_id))
<input type="hidden" name="laboratoryresult_id" id="laboratoryresult_id" value="{{ $lab[$testType]->LaboratoryResult->laboratoryresult_id }}"/>
@endif
<div class="row">
  <div class="box no-border">

    <div class="col-sm-12">
      <div class="col-sm-12">
          <h4>Add Attachments</h4>
          <p>Upload an attachment or fill out the form below.</p>
          <input type="file" name="lab_file_upload" id="lab_file_upload">
      </div>
      <div class="clearfix"></div>
    <hr />
    </div>
    <div class="col-sm-12">
    @if($hasResult == 0)
      <label class="col-sm-12">Results</label>
      <div class="col-sm-12">
        {!!Form::textarea('lab_results', null, ['class' => 'form-control']); !!}
      </div>
    @endif
    </div>
  </div>
</div>

@if($hasResult == 1)
<div class="row">
  <div class="box no-border">
    @if($lab_filename)
    <div class="col-sm-12">
      <div class="box-header with-border">
        <h3 class="box-title control-label"> Attachments</h3>
      </div>
    </div>
    <div class="col-sm-12" style="overflow:hidden;margin-top:20px;">
      <div id="image-gallery" class="col-md-12 cf">
          <?php
              $filenames = explode(';', $lab_filename);
              foreach($filenames as $filename) {
              $ext = strtolower(substr($filename, -3));
                if($ext == 'png' OR $ext == 'jpg' OR $ext == 'gif') {
            ?>
                    <p><a href="#" data-target="{{ url('laboratory/delete_attachment/'.$lab_id.'/'.$filename) }}" data-mod="{{ url('laboratory/modal/'.$testType.'/'.$lab[$testType]->medicalorderlaboratoryexam_id) }}" class="kbd kbd-sm bg-red deletebutton"><i class="fa fa-times"></i></a> {{ $filename }}</p>
                    <p><img src="{{ url(uploads_url().'/lab_results/'.$filename) }}"  data-high-res-src="{{ url(uploads_url().'/lab_results/'.$filename) }}" alt="" class="pannable-image col-sm-12"></p>
                <?php } else { ?>
                    <p><a href="#" data-target="{{ url('laboratory/delete_attachment/'.$lab_id.'/'.$filename) }}" data-mod="{{ url('laboratory/modal/'.$testType.'/'.$lab[$testType]->medicalorderlaboratoryexam_id) }}" class="kbd kbd-sm bg-red deletebutton"><i class="fa fa-times"></i></a> <a href="{{ url(uploads_url().'/lab_results/'.$filename) }}" target="_blank">{{ $filename }}</a></p>
                <?php } ?>
            <?php } ?>
      </div>
    @endif

    @if(isset($data['lab_results']) AND $data['lab_results'])
    <label class="col-sm-12">Results</label>
      <div class="col-sm-12">
        {!!Form::textarea('lab_results', $data['lab_results'], ['class' => 'form-control']); !!}
      </div>
    @endif
  </div>
</div>
@endif
