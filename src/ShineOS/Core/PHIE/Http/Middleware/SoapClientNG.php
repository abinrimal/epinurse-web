<?php namespace ShineOS\Core\PHIE\Http\Middleware;

use SoapClient;

class SoapClientNG extends SoapClient {

    public function __doRequest($req, $location, $action, $version = SOAP_1_1, $one_way=NULL) {

        $xml = explode("\r\n",
            parent::__doRequest($req, $location, $action, $version, $one_way=NULL)
        );

        $response = preg_replace( '/^(\x00\x00\xFE\xFF|\xFF\xFE\x00\x00|\xFE\xFF|\xFF\xFE|\xEF\xBB\xBF)/', "", $xml[0] );

        return $response;

    }
}
