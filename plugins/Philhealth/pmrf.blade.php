<?php

if(isset($patient)) {
    $patient_age = getAge($patient->birthdate);
}

$region = NULL;
$province = NULL;
$city = NULL;
$barangay = NULL;

$intent = "hidden";
if(isset($philhealth)) { $intent = "update"; }
else { $intent = "save"; $philhealth = NULL; }

$hidden = "hidden";
if(isset($philhealth->member_type)) {
    if($philhealth->member_type != 'DD') {
        $hidden = "hidden";
    }
    else{
        $hidden = "";
    }
}
else {
    $hidden = "hidden";
}

$nat = 'PHL';
if(isset($patient) AND $patient->nationality != NULL) {
    $nat = $patient->nationality;
}

$civilStat = 'U';
if(isset($patient) AND $patient->civil_status != NULL) {
    $civilStat = $patient->civil_status;
}

?>

<!-- APPLICATION FORM -->
<div class="modal-header bg-blue-gradient">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title" id="myModalLabel">PHILHEALTH APPLICATION FORM</h4>
</div>
<div id="applicationform" class="modal-body">
    {!! Form::model($patient, array('url' => 'plugin/call/Philhealth/Philhealth/apply/'.$patient->patient_id,'class'=>'form-horizontal')) !!}
    {!! Form::hidden('patient_id',$patient['patient_id']) !!}
    <div class="row">
        <div class="col-md-12">
            <legend>Enrollment Details</legend>
            <div class="col-md-6">
                <label class="col-md-4 control-label">Purpose</label>
                <div class="col-md-8 icheck">
                    <div class="radio-inline">
                        <label>
                            <input name="Purpose" type="radio" value="E" > For Enrollment
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input name="Purpose" type="radio" value="U" > For Updating
                        </label>
                    </div>
                    <br clear="all" />
                    <small>Purpose of Enrollment</small>
                </div>
            </div>
            <div class="col-md-6">
                <label class="col-md-4 control-label">PHIC PIN</label>
                <div class="col-md-8">
                  <input id="MEMID_NO" type="number" class="form-control" name="MEMID_NO" placeholder="PHIC PIN" value="{{ isset($philhealth) ? $philhealth->philhealth_id : NULL }}" maxlength="12">
                  <small>PhilHealth Identification Number (MEMBER'S PIN)</small>
                </div>
            </div>

            <br clear="all" />
            <legend>Personal Information</legend>
            <div class="col-md-6">
                <label class="col-md-4 control-label">Last Name</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="Last_Name" placeholder="Last Name" value="{{$patient->last_name}}" />
                </div>
            </div>
            <div class="col-md-6">
                <label class="col-md-4 control-label">First Name</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="First_Name" placeholder="First Name" value="{{$patient->first_name}}" />
                </div>
            </div>

            <div class="col-md-6">
                <label class="col-md-4 control-label">Middle Name</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="Middle_Name" placeholder="Middle Name" value="{{$patient->middle_name}}" />
                </div>
            </div>

            <div class="col-md-6">
                <label class="col-md-4 control-label">Name Suffix</label>
                <div class="col-md-8">
                    <div class="btn-group toggler" data-toggle="buttons">
                      <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'SR') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="SR"> Sr.
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'JR') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="JR"> Jr.
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'II') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="II"> II
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'III') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="III"> III
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'IV') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="IV"> IV
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'V') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="V"> V
                      </label>
                      <label class="btn btn-default">
                        <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="NA"> None
                      </label>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <label class="col-md-4 control-label">Maiden Mid Name</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="Maiden_Middle_Name" placeholder="Maiden Middle Name">
                </div>
            </div>

            <div class="col-md-6">
                <label class="col-md-4 control-label">Maiden Last Name</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="Maiden_Last_Name" placeholder="Maiden Last Name" value="{{ $patient->middle_name }}">
                </div>
            </div>

            <div class="col-md-6">
                <label class="col-md-4 control-label">Sex</label>
                <div class="col-md-8 icheck">
                    <div class="radio-inline">
                        <label>
                            <input name="Sex" type="radio" class="gender" value="M" <?php if ($patient->gender == "M") {echo  "checked";} ?> > Male
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input name="Sex" type="radio" class="gender" value="F" <?php if ($patient->gender == "F") {echo  "checked";} ?> > Female
                        </label>
                    </div>
                </div>
            </div>

            <div class="col-md-6 clear-fix">
                <label class="col-md-4 control-label">Civil Status</label>
                <div class="col-md-8">
                    {!! Form::select('Civil_Status_Dep', $civilStatus, $civilStat, ['class' => 'required form-control', 'id'=>'marital_status_field']) !!}
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <label class="col-md-4 control-label">Nationality</label>
                <div class="col-md-8">
                    {!! Form::select('Nationality', $nationality, $nat, ['class' => 'form-control alpha']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <label class="col-md-4 control-label">Religion</label>
                <div class="col-md-8">
                    <?php
                        $rel = NULL;
                        if(isset($patient) AND $patient->religion != NULL) {
                            $rel = $patient->religion;
                        }
                    ?>
                    {!! Form::select('Religion', $religion, $rel, ['class' => 'form-control alpha']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <label class="col-md-4 control-label">Blood Type</label>
                <div class="col-md-8">
                    {!! Form::select('blood_type', $bloodType, $patient->blood_type, ['class' => 'form-control alpha']) !!}
                </div>
            </div>
        </div>
    </div>

    <!-- CONTACT DETAILS -->
    <div class="row">
        <div class="col-md-12">
            <legend>Contact Details</legend>
            <div class="col-md-6">
                <label class="col-md-4 control-label">Phone number</label>
                <div class="col-md-8">
                    <?php
                    $phone = NULL;
                    if(isset($patient->patientContact->phone)):
                        $mobile = $patient->patientContact->phone;
                    endif;
                    ?>
                    {!! Form::text('phone', $phone, array('class' => 'form-control phonePH masked', 'name'=>'Patient_Phone', 'data-mask'=>'', 'data-inputmask'=>'&quot;mask&quot;: &quot;99-9999999&quot;', 'placeholder'=>'02-5772886')) !!}
                </div>
            </div>
            <div class="col-md-6">
                <label class="col-md-4 control-label">Cellphone No. </label>
                <div class="col-md-8">
                    <?php
                    $mobile = NULL;
                    if(isset($patient->patientContact->mobile)):
                        $mobile = $patient->patientContact->mobile;
                    endif;
                    ?>
                    {!! Form::text('mobile', $mobile, array('class' => 'form-control mobilePH masked', 'name'=>'Patient_Mobile', 'data-mask'=>'', 'data-inputmask'=>'&quot;mask&quot;: &quot;0999-9999999&quot;', 'placeholder'=>'0917-1234567')) !!}
                </div>
            </div>
            <div class="col-md-6">
                <label class="col-md-4 control-label">Email address</label>
                <div class="col-md-8">
                    <?php
                    $email = NULL;
                    if(isset($patient->patientContact->email)):
                        $email = $patient->patientContact->email;
                    endif;
                    ?>
                    {!! Form::text('email', $email, array('class' => 'form-control email', 'name'=>'Patient_Email','placeholder'=>'sample@sample.com')) !!}
                </div>
            </div>
            <!--<div class="col-md-6">
                <label class="col-md-4 control-label">Retype Email address</label>
                <div class="col-md-8">
                    {!! Form::text('email', '', array('class' => 'form-control email', 'name'=>'retypePatientEmail','placeholder'=>'Kailangan pa ba to')) !!}
                </div>
            </div> -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <!-- ADDRESS -->
        <legend>Address</legend>
        <div class="col-md-6">
            <label class="col-md-4 control-label">Unit/Room No., Floor</label>
            <div class="col-md-8">
              <input type="text" class="form-control" name="Unit_Room_Number" placeholder="Unit/Room No., Floor">
          </div>
        </div>
        <div class="col-md-6">
            <label class="col-md-4 control-label">Building Name</label>
            <div class="col-md-8">
              <input type="text" class="form-control" name="Building_Name" placeholder="Building Name">
          </div>
        </div>
        <div class="col-md-6">
            <label class="col-md-4 control-label">House/Bldg No.</label>
            <div class="col-md-8">
              <input type="text" class="form-control" name="House_Bldg_No" placeholder="House/Bldg No.">
          </div>
        </div>
        <div class="col-md-6">
            <label class="col-md-4 control-label">Street</label>
            <div class="col-md-8">
              <?php
                $street_address = NULL;
                if(isset($patient->patientContact->street_address)):
                    $street_address = $patient->patientContact->street_address;
                endif;
                ?>
              <input type="text" class="form-control" name="Street" placeholder="Street" value="{{ $patient->patientContact->street_address }}">
          </div>
        </div>
        <div class="col-md-6">
            <label class="col-md-4 control-label">Subdivision/Village</label>
            <div class="col-md-8">
              <input type="text" class="form-control" name="Subdivision_Village" placeholder="Subdivision/Village" value="{{ $patient->patientContact->village }}">
          </div>
        </div>

        <div class="form-group col-md-6">
                <label class="col-sm-4 control-label">Region</label>
                <div class="col-sm-8">
                    <select class="placeholder form-control required" name="region" id="region" <?php if($patient != NULL) echo "required"; ?> >
                        {{ Shine\Libraries\Utils::get_regions($patient->patientContact->region) }}
                    </select>
                </div>
        </div>
        <div class="form-group col-md-6">
                <label class="col-sm-4 control-label">Province</label>
                <div class="col-sm-8">
                    <select class="populate placeholder form-control required" name="province" id="province" <?php if($patient != NULL) echo "required"; ?> >
                        {{ Shine\Libraries\Utils::get_provinces($patient->patientContact->region, $patient->patientContact->province) }}
                    </select>
                </div>
        </div>
        <div class="form-group col-md-6">
                <label class="col-sm-4 control-label">City / Municipality</label>
                <div class="col-sm-8">
                    <select class="populate placeholder form-control required" name="city" id="city" <?php if($patient != NULL) echo "required"; ?> >
                        {{ Shine\Libraries\Utils::get_cities($patient->patientContact->region, $patient->patientContact->province, $patient->patientContact->city) }}
                    </select>
                </div>
        </div>
        <div class="form-group col-md-6">
                <label class="col-sm-4 control-label">Barangay</label>
                <div class="col-sm-8">
                    <select class="populate placeholder form-control required" name="brgy" id="brgy" <?php if($patient != NULL) echo "required"; ?> >
                        {{ Shine\Libraries\Utils::get_brgys($patient->patientContact->region, $patient->patientContact->province, $patient->patientContact->city, $patient->patientContact->barangay) }}
                    </select>
                </div>
        </div>
        <div class="col-md-6">
            <label class="col-md-4 control-label">Country</label>
            <div class="col-md-8">
            {!! Form::select('country', $nationality, 'PHL', ['class' => 'form-control alpha']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <label class="col-md-4 control-label">Zip Code</label>
            <div class="col-md-8">
              <input type="number" class="form-control" name="Zip_Code" placeholder="Zip Code" value="{{ $patient->patientContact->zip }}">
          </div>
        </div>
    </div>
    </div>

    <!-- BIRTH DETAILS -->
    <div class="row">
        <div class="col-md-12">
            <legend>Birth Details</legend>
            <div class= "col-md-6">
                <label class="col-md-4 control-label">Date of Birth</label>
                <div class="col-md-8">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      {!! Form::text('Patient_Birthdate', $patient->birthdate, array('class' => 'form-control datepicker required')) !!}
                    </div>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label class="col-sm-4 control-label">Region</label>
                <div class="col-sm-8">
                    <select class="placeholder form-control required" name="POB_Region" id="pmrf_birth_region" >
                        {{ Shine\Libraries\Utils::get_regions($patient->patientContact->region) }}
                    </select>
                </div>
            </div>
            <div class="form-group col-md-6">
                    <label class="col-sm-4 control-label">Province</label>
                    <div class="col-sm-8">
                        <select class="populate placeholder form-control required" name="POB_PROV" id="pmrf_birth_province">
                            {{ Shine\Libraries\Utils::get_provinces($patient->patientContact->region, $patient->patientContact->province) }}
                        </select>
                    </div>
            </div>
            <div class="form-group col-md-6">
                    <label class="col-sm-4 control-label">City / Municipality</label>
                    <div class="col-sm-8">
                        <select class="populate placeholder form-control required" name="POB_MUNI" id="pmrf_birth_city">
                            {{ Shine\Libraries\Utils::get_cities($patient->patientContact->region, $patient->patientContact->province, $patient->patientContact->city) }}
                        </select>
                    </div>
            </div>
            <div class="col-md-6">
                <label class="col-md-4 control-label">Birth Country</label>
                <div class="col-md-8">
                    {!! Form::select('BIRTH_COUNTRY', $nationality, 'PHL', ['class' => 'form-control alpha']) !!}
              </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <legend>Other Details</legend>
            <div class="col-md-6">
                <label class="col-md-4 control-label">TIN</label>
                <div class="col-md-8">
                  <input type="number" class="form-control" name="TIN" placeholder="Tax Identification Number">
                </div>
            </div>

            <div class="col-md-6">
                <label class="col-md-4 control-label">GSIS</label>
                <div class="col-md-8">
                  <input type="number" class="form-control" name="gsis_no" placeholder="GSIS Number">
                </div>
            </div>

            <div class="col-md-6">
                <label class="col-md-4 control-label">SSS No</label>
                <div class="col-md-8">
                  <input type="number" class="form-control" name="sss_no" placeholder="SSS Number">
                </div>
            </div>

            <div class="col-md-6">
                <label class="col-md-4 control-label">MEC No (OWWA)</label>
                <div class="col-md-8">
                  <input type="number" class="form-control" name="mec_no" placeholder="MEC Number">
                </div>
            </div>

            <div class="col-md-6">
                <label class="col-md-4 control-label">PNP No</label>
                <div class="col-md-8">
                  <input type="number" class="form-control" name="pnp_no" placeholder="PNP Number">
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    @if(!isset($philhealth) OR (isset($philhealth) AND $philhealth->member_type != 'DD'))
    <!-- DEPENDENT'S INFORMATION -->
    <div class = "row">
        <div class="col-md-12 parentDivDep">
            <legend>Dependent's Information</legend>
            <!-- <h4><mdall>Add Dependent</mdall></h4> -->
            <div class = "cloneDep">
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Relationship</label>
                    <div class="col-md-8">
                        {!! Form::select('phicDependent[Relationship][]', $getArrRelationship, NULL, ['class' => 'required form-control', 'id'=>'relationship']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="col-md-4 control-label">PIN (If Applicable)</label>
                    <div class="col-md-7">
                      <input type="number" class="form-control" name="phicDependent[PIN_Dep][]" placeholder="PIN (If Applicable)">
                    </div>
                    <div class="col-md-1">
                        <button id="removeBeneficiary-button" type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Last Name</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control required" name="phicDependent[Last_Name][]" placeholder="Last Name">
                  </div>
                </div>
                <div class="col-md-6">
                    <label class="col-md-4 control-label">First Name</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control required" name="phicDependent[First_Name][]" placeholder="First Name">
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Middle Name</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" name="phicDependent[Middle_Name][]" placeholder="Middle Name">
                  </div>
                </div>
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Name Suffix</label>
                    <div class="col-md-8">
                        <div class="btn-group toggler" data-toggle="buttons">
                          <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'SR') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="phicDependent[Suffix][]" id="" autocomplete="off" value="SR"> Sr.
                          </label>
                          <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'JR') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="phicDependent[Suffix][]" id="" autocomplete="off" value="JR"> Jr.
                          </label>
                          <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'II') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="phicDependent[Suffix][]" id="" autocomplete="off" value="II"> II
                          </label>
                          <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'III') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="phicDependent[Suffix][]" id="" autocomplete="off" value="III"> III
                          </label>
                          <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'IV') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="phicDependent[Suffix][]" id="" autocomplete="off" value="IV"> IV
                          </label>
                          <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'V') active @endif">
                            <i class="fa fa-check"></i> <input type="radio" name="phicDependent[Suffix][]" id="" autocomplete="off" value="V"> V
                          </label>
                          <label class="btn btn-default active">
                            <i class="fa fa-check"></i> <input type="radio" name="phicDependent[Suffix][]" id="" autocomplete="off" value="NA"> None
                          </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <label class="col-md-4 control-label">Maiden Middle Name</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" name="phicDependent[maidenmiddlename][]" placeholder="Maiden Middle Name">
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Maiden Last Name</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" name="phicDependent[maidenlastname][]" placeholder="Maiden Last Name">
                    </div>
                </div>

                <div class="col-md-6">
                    <label class="col-md-4 control-label">Sex</label>
                    <div class="col-md-8 icheck">
                        <div class="radio-inline">
                            <label>
                                <input name="phicDependent[Dep_Sex][]" type="radio" class="gender" value="M"> Male
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input name="phicDependent[Dep_Sex][]" type="radio" class="gender" value="F"> Female
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input name="phicDependent[Dep_Sex][]" type="radio" class="gender" value="U" checked="checked"> Unknown
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <label class="col-md-4 control-label">Civil Status</label>
                    <div class="col-md-8">
                    {!! Form::select('phicDependent[Civil_Status][]', $civilStatus, 'U', ['class' => 'required form-control dep_marital_status_field']) !!}
                    </div>
                </div>
                <div class="clearfix"></div>
                <div  class="col-md-6">
                    <label class="col-md-4 control-label">Date of Birth</label>
                    <div class="col-md-8">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                           {!! Form::text('birthdate', null, array('class' => 'form-control required datepicker', 'name'=>'phicDependent[Birth_Date][]')) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Disability</label>
                    <div class="col-md-8">
                        <select class="required form-control" name="phicDependent[Disability_Dep][]" id="relationship">
                            <option value="">- Select Disability -</option>
                            <option value="Y">Yes</option>
                            <option value="N">No</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-sm-4 control-label">Province</label>
                    <div class="col-sm-8">
                        <select class="populate placeholder form-control required" name="phicDependent[province][]" id="province">
                            {{ Shine\Libraries\Utils::get_provinces($patient->patientContact->region, $patient->patientContact->province) }}
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-sm-4 control-label">City / Municipality</label>
                    <div class="col-sm-8">
                        <select class="populate placeholder form-control required" name="phicDependent[city][]" id="city">
                            {{ Shine\Libraries\Utils::get_cities($patient->patientContact->region, $patient->patientContact->province, $patient->patientContact->city) }}
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <label class="col-md-4 control-label">Occupation</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="phicDependent[occupation][]" placeholder="Occupation">
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Monthly Amount of Income</label>
                    <div class="col-md-8">
                        <input type="number" class="form-control" name="phicDependent[income][]" placeholder="Monthly Amount of Income">
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Status Indicator</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="phicDependent[status][]" placeholder="Status Indicator">
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Employment Status</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="phicDependent[emp_stat][]" placeholder="Employment Status">
                    </div>
                </div>
                <div class="col-md-12">
                    <hr />
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="btn-group pull-right">
                <button class="btn btn-success" id="addBeneficiary-button" type="button"><i class="fa fa-plus"></i> Add Dependent</button>
            </div>
        </div>
    </div>
    @endif

    @if($patient_age < 18 OR (isset($philhealth) AND $philhealth->member_type == 'DD'))
        <!-- GUARDIAN'S INFORMATION (ONLY ENABLED WHEN MINOR) -->
        <div class= "row hidden">
            <div class="parentDivGuard col-md-12">
                <legend>Benefactor Information</legend>
                <p>If applications is a dependent, please verify and complete the Benefactor Information:</p>
                <div class = "cloneGuard">
                    <div class="col-md-6">
                        <label class="col-md-4 control-label">Relationship</label>
                        <div class="col-md-8">
                            <?php
                                $philhealth_relation = NULL;
                                if(isset($philhealth)) {
                                    $philhealth_relation = $philhealth->relation;
                                }
                            ?>
                            {!! Form::select('Relationship_Guar', $getArrRelationship, $philhealth_relation, ['class' => 'required form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-md-4 control-label">PIN (If Applicable)</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" name="Pin_Guar" placeholder="PIN (If Applicable)" value="{{ isset($philhealth) ? $philhealth->benefactor_member_id : NULL }}">
                      </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-md-4 control-label">Last Name</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" name="Last_Name_Guar" placeholder="Last Name" value="{{ isset($philhealth) ? $philhealth->benefactor_last_name : NULL }}">
                      </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-md-4 control-label">First Name</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" name="First_Name_Guar" placeholder="First Name" value="{{ isset($philhealth) ? $philhealth->benefactor_first_name : NULL }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-md-4 control-label">Middle Name</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" name="Middle_Name_Guar" placeholder="Middle Name" value="{{ isset($philhealth) ? $philhealth->benefactor_middle_name : NULL }}">
                      </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-md-4 control-label">Name Suffix</label>
                        <div class="col-md-8">
                            <div class="btn-group toggler" data-toggle="buttons">
                              <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'SR') active @endif">
                                <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="SR"> Sr.
                              </label>
                              <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'JR') active @endif">
                                <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="JR"> Jr.
                              </label>
                              <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'II') active @endif">
                                <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="II"> II
                              </label>
                              <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'III') active @endif">
                                <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="III"> III
                              </label>
                              <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'IV') active @endif">
                                <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="IV"> IV
                              </label>
                              <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'V') active @endif">
                                <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="V"> V
                              </label>
                              <label class="btn btn-default">
                                <i class="fa fa-check"></i> <input type="radio" name="Suffix" id="" autocomplete="off" value="NA"> None
                              </label>
                            </div>
                        </div>
                    </div>
                    <div  class="col-md-6">
                        <label class="col-md-4 control-label">Birth Date</label>
                            <div class="col-md-8">
                                {!! Form::text('birthdate', null, array('class' => 'form-control required datepicker', 'name'=>'Birth_Date_Dep')) !!}
                            </div>
                    </div>
                    <div class="row hidden">
                        <div class="col-md-12">
                            <div class="btn-group pull-right">
                                <button id="removeGuardian-button" type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Remove</button>
                                <button class="btn btn-success" id="addGuardian-button" type="button">Add Guardian</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- MEMBERSHIP CATEGORY INFORMATION -->
    <div class="row">
        <div class="col-md-12">
            <legend>Membership Category Information</legend>
            <div class="col-md-6">
                <label class="col-md-4 control-label">Member Category</label>
                <div class="col-md-8">
                    {!! Form::select('Member_Category', $ref_mem_cat, NULL, ['class' => 'form-control alpha', 'id'=>'Member_Category']) !!}
                </div>
            </div>

            <!-- ONLY ENABLED WHEN IE - Informal Sector IS CHOSEN IN MEMBER CATEGORY -->
            <div class="hidden" id ="InfEco_InfSector">
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Please specify </label>
                    <div class="col-md-8">
                        <small>(e.g. Market vendor, Street Hawker, Pedicab / Tricycle Driver, etc.)</small>
                        {!! Form::text('InfEco_InfSector_Specify', null, array('class' => 'form-control required')) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Estimated Monthly Income (Php)</label>
                    <div class="col-md-8">
                        {!! Form::number('InfEco_InfSector_Specify_MoIncome', null, array('class' => 'form-control required')) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="col-md-4 control-label">No Income</label>
                    <div class="col-md-8 icheck">
                        <div class="radio-inline">
                            <label>
                                <input name="InfEco_InfSector_Specify_MoIncome" type="radio" class="InfEco_InfSector_Specify_MoIncome" value="Y"> Yes
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input name="InfEco_InfSector_Specify_MoIncome" type="radio" class="InfEco_InfSector_Specify_MoIncome" value="N"> No
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ONLY ENABLED WHEN LIFETIME IS CHOSEN IN MEMBER CATEGORY -->
            <div class="hidden" id ="lifetime_type_form">
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Date / Effectivity of Retirement</label>
                    <div class="col-md-8">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          {!! Form::text('Lifetime_Member_RetirementDate', null, ['class' => 'form-control datepicker', 'id' => 'datepicker']); !!}
                        </div>
                    </div>
                </div>
            </div>

             <!-- ONLY ENABLED IF IE - Self Earning Individual IS CHOSEN IN MEMBER TYPE -->
             <div class="hidden" id ="self_earning">
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Please specify </label>
                    <div class="col-md-8">
                        <small>(e.g. Doctors, Lawyers, Engineers, etc.)</small>
                        {!! Form::text('InfEco_SelfEarn_Specify', null, array('class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Estimated Monthly Income (Php)</label>
                    <div class="col-md-8">
                        {!! Form::number('InfEco_SelfEarn_MoIncome', null, array('class' => 'form-control')) !!}
                    </div>
                </div>
            </div>


            <!-- ONLY ENABLED IF IE - Organized Group IS CHOSEN IN MEMBER TYPE -->
             <div class="hidden" id ="InfEco_OrgGroup">
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Please specify </label>
                    <div class="col-md-8">
                       {!! Form::text('InfEco_OrgGroup_Specify', null, array('class' => 'form-control')) !!}
                    </div>
                </div>
            </div>

            <!-- ONLY ENABLED IF Sponsored LGU IS CHOSEN IN MEMBER TYPE -->
             <div class="hidden" id ="LGU">
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Please specify </label>
                    <div class="col-md-8">
                       {!! Form::text('Sponsored_LGU', null, array('class' => 'form-control')) !!}
                    </div>
                </div>
            </div>

            <!-- ONLY ENABLED IF Sponsored NGA IS CHOSEN IN MEMBER TYPE -->
             <div class="hidden" id ="NGA">
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Please specify </label>
                    <div class="col-md-8">
                       {!! Form::text('Sponsored_NGA', null, array('class' => 'form-control')) !!}
                    </div>
                </div>
            </div>

            <!-- ONLY ENABLED IF Sponsored Others Specify IS CHOSEN IN MEMBER TYPE -->
             <div class="hidden" id ="SponsoredOthers">
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Please specify </label>
                    <div class="col-md-8">
                       {!! Form::text('Sponsored_Others_Specify', null, array('class' => 'form-control')) !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
    <br clear="all" />
    <div class="col-md-12">
        <div class="pull-right ">
            <button class="btn btn-primary" data-dismiss="modal">Close</button>
            <button type="submit" value="submit" class="btn btn-success">Apply Now</button>
        </div>
    </div>
    {!! Form::close() !!}
    <br clear="all" />
</div>
<div class="modal-footer">
</div>
{!! HTML::script('/plugins/Philhealth/Assets/js/philhealth.js'); !!}
<script type="text/javascript">
    $(document).ready(function() {
        //apply icheck for radios and checkboxes
        $('.icheck input').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
        $("input.datepicker").daterangepicker({
            singleDatePicker: true,
            autoUpdateInput: false,
            showDropdowns: true,
            "minDate": "01/01/1900",
            "maxDate": "<?php echo date("m/d/Y"); ?>"
        });

    });
</script>
