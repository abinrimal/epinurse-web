<?php

/* Functions for Users */

use ShineOS\Core\Users\Entities\Users;

    /**
    * Find a User by ID
    * @param string $id User ID
    * @return arrayObject Array of User properties
    * @package Users
    */
    function findByUserID($id)
    {
        return Users::where('user_id','=',$id)->first();
    }

    /**
    * Find a complete User by ID
    * @param string $id User ID
    * @return arrayObject Array of User properties
    * @package Users
    */
    function findUserByUserID($id)
    {
        $user = DB::table('users')
            ->leftJoin('user_md', 'user_md.user_id', '=', 'users.user_id')
            ->join('user_contact', 'user_contact.user_id', '=', 'users.user_id')
            ->where('users.user_id', $id)
            ->first();
        if($user){
            return $user;
        } else {
            return NULL;
        }
    }

    /**
    * Find a User by FacilityID
    * @param string $facid Facility ID
    * @return arrayObject Array of User properties
    * @package Users
    */
    function findUserByUserFacID($facid)
    {
        $user = DB::table('users')
            ->join('facility_user', 'facility_user.user_id', '=', 'users.user_id')
            ->join('user_md', 'user_md.user_id', '=', 'users.user_id')
            ->join('user_contact', 'user_contact.user_id', '=', 'users.user_id')
            ->where('facility_user.facilityuser_id', $facid)
            ->first();
        if($user){
            return $user;
        } else {
            return NULL;
        }
    }

    /**
    * Find a User using a specific field
    * @param string $column Field name
    * @return arrayObject Array of User properties
    * @package Users
    */
    function findByColumn($column)
    {
        return Users::get($column);
    }

    /**
    * Find active User's Photo
    * @param null
    * @return string Filename of photo
    * @package Users
    */
    function getUserPhoto()
    {
        $user = Session::get('_global_user');
        $xuser = findByUserID($user->user_id);

        //refresh session
        Session::put('_global_user', $xuser);

        return $xuser->profile_picture;
    }

    /**
    * Get the Role of User using Facility User ID
    * @param string $id Facility User ID
    * @return arrayObject Array of FacilityUser properties
    * @package Users
    */
    function getRoleByFacilityUserID($id = NULL)
    {
        $role = DB::table('facility_user')
            ->join('roles', 'roles.role_id', '=', 'facility_user.role_id')
            ->select('roles.role_name')
            ->where('facility_user.facilityuser_id', $id)
            ->first();

        return $role;
    }

    /**
    * Get the Role information using Facility User ID
    * @param string $id Facility User ID
    * @return arrayObject Array of Role properties
    * @package Users
    */
    function getRoleInfoByFacilityUserID($id = NULL)
    {
        $role = DB::table('facility_user')
            ->join('roles', 'roles.role_id', '=', 'facility_user.role_id')
            ->where('facility_user.facilityuser_id', $id)
            ->first();

        return $role;
    }

    /**
    * Get First Name of a User by FacilityUser ID
    * @param string $id Facility User ID
    * @return string Firstname of User
    * @package Users
    */
    function getUserFirstNameByID($id)
    {
        $user = DB::table('facility_user')
            ->join('users', 'facility_user.user_id', '=', 'users.user_id')
            ->select('users.*')
            ->where('facility_user.facilityuser_id', $id)
            ->first();
        if($user) {
            return $user->first_name;
        } else {
            //try
            $userb = DB::table('facility_user')
            ->join('users', 'facility_user.user_id', '=', 'users.user_id')
            ->select('users.*')
            ->where('facility_user.user_id', $id)
            ->first();
            if($userb) {
                return $userb->first_name;
            } else {
                return NULL;
            }
        }

    }

    /**
    * Get Last Name of a User by FacilityUser ID
    * @param string $id Facility User ID
    * @return string Lastname of User
    * @package Users
    */
    function getUserLastNameByID($id)
    {
        $user = DB::table('facility_user')
            ->join('users', 'facility_user.user_id', '=', 'users.user_id')
            ->select('users.*')
            ->where('facility_user.facilityuser_id', $id)
            ->first();
        if($user) {
            return $user->last_name;
        } else {
            //try
            $userb = DB::table('facility_user')
            ->join('users', 'facility_user.user_id', '=', 'users.user_id')
            ->select('users.*')
            ->where('facility_user.user_id', $id)
            ->first();
            if($userb) {
                return $userb->last_name;
            } else {
                return NULL;
            }
        }

    }

    /**
    * Get Middle Name of a User by FacilityUser ID
    * @param string $id Facility User ID
    * @return string Middlename of User
    * @package Users
    */
    function getUserMidNameByID($id)
    {
        $user = DB::table('facility_user')
            ->join('users', 'facility_user.user_id', '=', 'users.user_id')
            ->select('users.*')
            ->where('facility_user.facilityuser_id', $id)
            ->first();
        if($user) {
            return $user->middle_name;
        } else {
            //try
            $userb = DB::table('facility_user')
            ->join('users', 'facility_user.user_id', '=', 'users.user_id')
            ->select('users.*')
            ->where('facility_user.user_id', $id)
            ->first();
            if($userb) {
                return $userb->middle_name;
            } else {
                return NULL;
            }
        }
    }

    /**
    * Get Suffix of a User by FacilityUser ID
    * @param string $id Facility User ID
    * @return string Suffix of User
    * @package Users
    */
    function getUserSuffixByID($id)
    {
        $user = DB::table('facility_user')
            ->join('users', 'facility_user.user_id', '=', 'users.user_id')
            ->select('users.*')
            ->where('facility_user.facilityuser_id', $id)
            ->first();
        if($user) {
            return $user->suffix;
        } else {
            //try
            $userb = DB::table('facility_user')
            ->join('users', 'facility_user.user_id', '=', 'users.user_id')
            ->select('users.*')
            ->where('facility_user.user_id', $id)
            ->first();
            if($userb) {
                return $userb->suffix;
            } else {
                return NULL;
            }
        }

    }

    /**
    * Get Barangay Name of a User by Catchment area ID
    * @param string $ca_id Catchment area ID
    * @return string Barangay Name
    * @package Users
    */
    function getBHSNameByFacilityIDUserID($ca_id = NULL)
    {
        $db = DB::table('facility_catchment_area')
            ->where('ca_id', $ca_id)
            ->pluck('bhs_name');

        return $db;
    }

    /**
    * Get Barangay Code of a User by Catchment area ID
    * @param string $ca_id Catchment area ID
    * @return string Barangay Code
    * @package Users
    */
    function getBHSBrgyByFacilityIDUserID($ca_id = NULL)
    {
        $db = DB::table('facility_catchment_area')
            ->where('ca_id', $ca_id)
            ->pluck('bhs_brgy_code');

        return $db;
    }

