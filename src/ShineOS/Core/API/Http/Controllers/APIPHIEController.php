<?php namespace ShineOS\Core\API\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Shine\Libraries\IdGenerator;
use Request, Input, DateTime;
use Shine\Repositories\Eloquent\PatientRepository as PatientRepository;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;

class APIPHIEController extends Controller {
	private $PatientRepository;
	private $healthcareRepository;

    public function __construct(PatientRepository $PatientRepository, HealthcareRepository $healthcareRepository) {
        $this->PatientRepository = $PatientRepository;
        $this->HealthcareRepository = $healthcareRepository;
    }

	public function index() {
		return response(json_encode(array('message'=>'No data')), 200);
	}

	public function check_updated_at($phie_updated_at, $data_updated_at) {

	}

	public function PHIE_patientsData() {
		$data = Request::all();
		if(array_key_exists('facility_id',$data)) {
			// findByPatientID
			$PHIE_patientsData = [];
			$patientData = json_decode($this->PatientRepository->findPatientsByFacilityId($data['facility_id'])); 
			foreach ($patientData as $pkey => $pvalue) {
				$PHIE_patientsData[$pkey]['Pat_Phil_Health_No'] = !empty($pvalue->phil_health_no) ?  $pvalue->phil_health_no : "NOT MEMBER";
				$PHIE_patientsData[$pkey]['Pat_Facility_No'] = Input::has('patient_id') ?  Input::get('patient_id') : NULL; 
				$PHIE_patientsData[$pkey]['Pat_Last_Name'] = !empty($pvalue->last_name) ?  $pvalue->last_name : 'NA';
				$PHIE_patientsData[$pkey]['Pat_First_Name'] = !empty($pvalue->first_name) ?  $pvalue->first_name : 'NA';
				$PHIE_patientsData[$pkey]['Pat_Middle_Name'] = !empty($pvalue->middle_name) ?  $pvalue->middle_name : 'NA';
				$PHIE_patientsData[$pkey]['Pat_Suffix_Name'] = !empty($pvalue->name_suffix) ?  $pvalue->name_suffix : 'NA';
				$PHIE_patientsData[$pkey]['Maiden_Last_Name'] = !empty($pvalue->maiden_last_name) ?  $pvalue->maiden_last_name : 'NA';
				$PHIE_patientsData[$pkey]['Maiden_First_Name'] = !empty($pvalue->maiden_first_name) ?  $pvalue->maiden_first_name : 'NA';
				$PHIE_patientsData[$pkey]['Maiden_Midde_Name'] = !empty($pvalue->maiden_middle_name) ?  $pvalue->maiden_middle_name : 'NA';
				$PHIE_patientsData[$pkey]['Pat_Date_of_Birth'] = !empty($pvalue->birthdate) ?  $pvalue->birthdate : 'NA';
				$PHIE_patientsData[$pkey]['Pat_Place_of_Birth'] = !empty($pvalue->birthplace) ?  $pvalue->birthplace : 'NA';
				$PHIE_patientsData[$pkey]['Pat_Sex'] = !empty($pvalue->gender) ?  $pvalue->gender : 'NA';
				$PHIE_patientsData[$pkey]['Pat_Civil_Status'] = !empty($pvalue->civil_status) ?  $pvalue->civil_status : NULL;
				$PHIE_patientsData[$pkey]['Pat_Nationality'] = !empty($pvalue->nationality) ?  $pvalue->nationality : NULL;
				$PHIE_patientsData[$pkey]['Pat_Religion'] = !empty($pvalue->religion) ?  $pvalue->religion : NULL;
				$PHIE_patientsData[$pkey]['Pat_Religion_Others_Specify'] = !empty($pvalue->religion_others) ?  $pvalue->religion_others : NULL;
				$PHIE_patientsData[$pkey]['Pat_Blood_Type'] = !empty($pvalue->blood_type) ?  $pvalue->blood_type : NULL;
				$PHIE_patientsData[$pkey]['Pat_Pantawid_Pamilya_Member'] = !empty($pvalue->pantawid_pamilya_member) ?  $pvalue->pantawid_pamilya_member : 'NOT MEMBER';
				$PHIE_patientsData[$pkey]['Pat_Current_Address_StreetName'] = !empty($pvalue->street_address) ?  $pvalue->street_address : NULL;
				$PHIE_patientsData[$pkey]['Pat_Current_Address_Region'] = !empty($pvalue->region) ?  $pvalue->region : NULL;
				$PHIE_patientsData[$pkey]['Pat_Current_Address_Province'] = !empty($pvalue->province) ?  $pvalue->province : NULL;
				$PHIE_patientsData[$pkey]['Pat_Current_Address_City'] = !empty($pvalue->city) ?  $pvalue->city : NULL;
				$PHIE_patientsData[$pkey]['Pat_Current_Address_Barangay'] = !empty($pvalue->barangay) ?  $pvalue->barangay : NULL;
				$PHIE_patientsData[$pkey]['Pat_Current_Address_SitioPurok'] = !empty($pvalue->sitioPurok) ?  $pvalue->sitioPurok : NULL;
				$PHIE_patientsData[$pkey]['Pat_Current_Address_ZipCode'] = !empty($pvalue->zip) ?  $pvalue->zip : NULL;
				$PHIE_patientsData[$pkey]['Pat_Current_Address_Country'] = !empty($pvalue->country) ?  $pvalue->country : NULL;
				$PHIE_patientsData[$pkey]['Pat_Contact_Landline'] = !empty($pvalue->phone) ?  $pvalue->phone : 'NONE';
				$PHIE_patientsData[$pkey]['Pat_Contact_Mobile'] = !empty($pvalue->mobile) ?  $pvalue->mobile : 'NONE';
				$PHIE_patientsData[$pkey]['Pat_Contact_EmailAddress'] = !empty($pvalue->email) ?  $pvalue->email : 'NONE';
				$PHIE_patientsData[$pkey]['Pat_Current_Occupation_Description'] = !empty($pvalue->occupation) ?  $pvalue->occupation : 'NA';
				$PHIE_patientsData[$pkey]['Highest_Education'] = !empty($pvalue->highest_education) ?  $pvalue->highest_education : NULL;
				$PHIE_patientsData[$pkey]['Education_Others_Specify'] = !empty($pvalue->highesteducation_others) ?  $pvalue->highesteducation_others : 'NA';
				$PHIE_patientsData[$pkey]['Father_Firstname'] = !empty($pvalue->father_firstname) ?  $pvalue->father_firstname : 'NA';
				$PHIE_patientsData[$pkey]['Father_Middlename'] = !empty($pvalue->father_middlename) ?  $pvalue->father_middlename : 'NA';
				$PHIE_patientsData[$pkey]['Father_Lastname'] = !empty($pvalue->father_lastname) ?  $pvalue->father_lastname : 'NA';
				$PHIE_patientsData[$pkey]['Father_Suffix'] = !empty($pvalue->suffix) ?  $pvalue->suffix : 'NA';
				$PHIE_patientsData[$pkey]['Mother_Firstname'] = !empty($pvalue->mother_firstname) ?  $pvalue->mother_firstname : 'NA';
				$PHIE_patientsData[$pkey]['Mother_Maiden_Lastname'] = !empty($pvalue->mother_middlename) ?  $pvalue->mother_middlename : 'NA';
				$PHIE_patientsData[$pkey]['Mother_Maiden_Middlename'] = !empty($pvalue->mother_lastname) ?  $pvalue->mother_lastname : 'NA';
			}
			return new JsonResponse((array('status'=>'success','PHIE_patientsData'=>$PHIE_patientsData)), 200);
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>'Facility id required'), 400);
		}		
	}

	/**
	 * Encounter Log
	 */
	

	/**
	 * Past Medical History
	 */
	
	/**
	 * Past Surgical History
	 */
	
	/**
	 * Family History
	 */
	
	/**
	 * Personal/Social History
	 */
	
	/**
	 * Immunization History - Children
	 */
	
	/**
	 * Immunization History - Young Women
	 */
	
	/**
	 * Immunization History - Pregnancy
	 */
	
	/**
	 * Immunization History - Elderly and Immunocompromised
	 */
	
	/**
	 * Menstrual History
	 */
	
	/**
	 * Pregnancy History
	 */
	
	/**
	 * Family Planning Access
	 */

/**
 * Physical Examination
 */

	/**
	 * Vital Signs
	 */
	
	/**
	 * Skin
	 */
	
	/**
	 * Heent
	 */
	
	/**
	 * Chest/Lungs
	 */
	
	/**
	 * Heart
	 */
	
	/**
	 * Abdomen
	 */
	
	/**
	 * Extremities
	 */
	
/**
 * Alert
 */

/**
 * Whopen
 */
	
/**
 * Immunizations
 */

/**
 * Examinations
 */

/**
 * Procedures
 */

/**
 * Drugs and Medicines Prescription
 */

/**
 * Pregnancy
 */

/**
 * Delivery - Mother
 */

/**
 * Neonatal (Babies)
 */

/**
 * Death
 */

/**
 * 
 */



}