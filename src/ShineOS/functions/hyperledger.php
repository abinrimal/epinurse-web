<?php

function sendBlock($p)
{
  $facility = Session::get('facility_details');
  $curUser = Session::get('user_details');
  $postToken = Session::get('_token');

  $block['post'] = json_encode($p);
  $block['patient_id'] = $curUser->patient_id;
  $block['postToken'] = $postToken;

  //insert API here
}