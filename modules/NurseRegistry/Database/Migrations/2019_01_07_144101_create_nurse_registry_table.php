<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNurseRegistryTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nurse_registry', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('nurse_id',60);

            $table->string('facility_id',60);
            $table->string('user_id',60);

            $table->string('uuid',60)->nullable();
            $table->string('nan_number',60)->nullable();
            $table->string('first_name',60)->nullable();
            $table->string('middle_name',60)->nullable();
            $table->string('last_name',60)->nullable();
            $table->integer('sex')->nullable();
            $table->string('citizenship_number',60)->nullable();
            $table->string('date_of_birth_in_ad',60)->nullable();
            $table->string('date_of_birth_in_bs',60)->nullable();
            $table->integer('age')->nullable();
            $table->string('temp_house_number',60)->nullable();
            $table->string('temp_ward_number',60)->nullable();
            $table->string('temp_municipality',60)->nullable();
            $table->string('temp_district',60)->nullable();
            $table->string('temp_country',60)->nullable();
            $table->string('temp_zipcode',60)->nullable();
            $table->string('permanent_house_number',60)->nullable();
            $table->string('permanent_ward_number',60)->nullable();
            $table->string('permanent_municipality',60)->nullable();
            $table->string('permanent_district',60)->nullable();
            $table->string('permanent_country',60)->nullable();
            $table->string('permanent_zipcode',60)->nullable();
            $table->string('professional_status',60)->nullable();
            $table->string('designation',60)->nullable();
            $table->string('office',60)->nullable();

            $table->text('qualifications')->nullable();
            $table->text('trainings')->nullable();

            $table->softDeletes();
            $table->timestamps();
            $table->unique('nurse_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nurse_registry');
    }

}
