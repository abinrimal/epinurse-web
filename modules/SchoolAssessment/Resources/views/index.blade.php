@extends('schoolassessment::layouts.master')

@section('page-content')
	
	<div class="col-sm-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
			  <li class="active"><a data-toggle="tab" href="#school">Schools</a></li>
			  <li class="pull-right"> <a href="schoolassessment/new" class="btn btn-sm btn-primary"> <i class="fa fa-university"></i> <span> Add New School </span></a></li>
			</ul>

			<div class="tab-content">
				<div id="school" class="tab-pane fade in active">
					<table class="table table-condensed datatable">
						<thead>
						  <tr>
						  	<th class="nosort"></th>
						    <th>Name</th>
						    <th>Contact Person</th>
						    <th>Position</th>
						    <th class="nosort">Actions</th>
						  </tr>
						</thead>
						<tbody>
							@foreach($schools as $key => $school)
								<tr>
									<td>{{ $school->id or NULL }}</td>
									<td>{{ $school->school_name or NULL }}</td>
									<td>{{ $school->contact_person or NULL }}</td>
									<td>{{ $school->contact_person_position or NULL }}</td>
									<td>
										<div class="btn-group">
											<a href="{{ url('/schoolassessment/update/'.$school->school_id) }}" class="btn btn-success"><i class="fa fa-pencil"></i> Edit </a>
											<a href="{{ url('/schoolassessment/delete/'.$school->school_id) }}" class="btn btn-danger" onclick="if( confirm('Are you sure you want to delete this school?') ) { return true; } return false;"><i class="fa fa-trash-o"></i> Delete </a>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@stop