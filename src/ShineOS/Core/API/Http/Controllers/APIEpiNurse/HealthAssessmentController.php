<?php namespace ShineOS\Core\API\Http\Controllers\APIEpiNurse;

use Illuminate\Routing\Controller;
use ShineOS\Core\API\Http\Controllers\APIEpiNurseController;

use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\PatientContacts;
use ShineOS\Core\Patients\Entities\PatientDeathInfo;
use ShineOS\Core\Patients\Entities\PatientEmergencyInfo;

use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser;

use Plugins\HealthAssessment\HealthAssessmentModel;

use ShineOS\Core\Users\Entities\Users;
use Illuminate\Http\JsonResponse;
use Shine\Libraries\IdGenerator;


use Request, Auth, Validator;

class HealthAssessmentController extends APIEpiNurseController {

	public function insertHealthAssessment($request_data,$studentemployee) {
		$data = $request_data['data'];
		$data['facility_id'] = $facility_id = $request_data['facility_id'];
		$data['user_id'] = $user_id = $request_data['user_id'];
		$data['patient_id'] = $patient_id = $request_data['patient_id'];

		if($studentemployee=='Student')
		{
			$validator = Validator::make($data, [
				"facility_id" => "required|string",
				"user_id" => "required|string",
				"patient_id" => "required|string",
				"uuid" => "required",
				"ownerUuid" => "required",
				"vaccines_bcg" => "required",
				"vaccines_dpt" => "required",
				"vaccines_opv" => "required",
				"vaccines_pcv" => "required",
				"vaccines_mr" => "required",
				"vaccines_je" => "required",
				"vaccines_td" => "required",
				"vaccines_others" => "required",
				"temperature" => "required",
				"systolic" => "required",
				"diastolic" => "required",
				"height_cm" => "required",
				"weight_kg" => "required",
				"bmi" => "required",
				"bmi_computed" => "required",
				"acute_respiratory_infection" => "required",
				"acute_watery_diarrhea" => "required",
				"acute_bloody_diarrhea" => "required",
				"acute_jaundice_infection" => "required",
				"suspected_meningitis" => "required",
				"suspected_tetanus" => "required",
				"acute_flaccid_paralysis" => "required",
				"acute_hemorraphic_fever" => "required",
				"fever" => "required",
				"trauma_major_head" => "required",
				"trauma_major_spinal" => "required",
				"trauma_major_torso" => "required",
				"trauma_left_leg" => "required",
				"trauma_right_leg" => "required",
				"trauma_left_arm" => "required",
				"trauma_right_arm" => "required",
				"skin_diseases" => "required",
				"allegies" => "required",
				"respiratory_disease" => "required",
				"cardiovascular_disease" => "required",
				"endocrine_disease" => "required",
				"urinary_system_disease" => "required",
				"reproductiv_system_disease" => "required",
				"communication_hearing_problem" => "required",
				"vision_problem" => "required",
				"oral_dental_health_problem" => "required",
				"psychological_problem" => "required",
				"physical_structural_problems" => "required",
				"cognative_patterns" => "required",
				"bowel_habit" => "required",
				"bladder_habit" => "required",
				"under_medication" => "required",
				"recent_treatment" => "required",
				"recent_procedure" => "required",
				"recent_counseling" => "required",
				"bh_suspension" => "required",
				"bh_anti_social_behavior" => "required",
				"bh_delinquency" => "required",
				"bh_violence" => "required",
				"bh_smoking" => "required",
				"bh_alcohol" => "required",
				"bh_substance_abuse" => "required",
				"bh_suicidal_thoughts" => "required",
				"bh_suicidal_attempts" => "required",
				"breakfast_place" => "required",
				"breakfast_time" => "required",
				"breakfast_type_of_food" => "required",
				"lunch_place" => "required",
				"lunch_time" => "required",
				"lunch_type_of_food" => "required",
				"snack_place" => "required",
				"snack_time" => "required",
				"snack_type_of_food" => "required",
				"dinner_place" => "required",
				"dinner_time" => "required",
				"dinner_type_of_food" => "required",
				"malnutrition" => "required",
				"idle_hours" => "required",
				"active_hours" => "required",
				"family_interaction" => "required",
				"community_involvement" => "required",
				"child_ddr_knowledge" => "required",
				"child_complaint" => "required"		
	        ]);

			$drr = isset($data['child_ddr_knowledge']) ? $data['child_ddr_knowledge'] : NULL;
		}

		if($studentemployee=='Employee')
		{
			$validator = Validator::make($data, [
				"facility_id" => "required|string",
				"user_id" => "required|string",
				"patient_id" => "required|string",
				"uuid" => "required",
				"ownerUuid" => "required",	
				"temperature" => "required",
				"systolic" => "required",
				"diastolic" => "required",
				"height_cm" => "required",
				"weight_kg" => "required",
				"bmi" => "required",
				"bmi_computed" => "required",
				"acute_respiratory_infection" => "required",
				"acute_watery_diarrhea" => "required",
				"acute_bloody_diarrhea" => "required",
				"acute_jaundice_infection" => "required",
				"suspected_meningitis" => "required",
				"suspected_tetanus" => "required",
				"fever" => "required",
				"skin_diseases" => "required",
				"allegies" => "required",
				"injuries_trauma" => "required",
				"respiratory_disease" => "required",
				"cardiovascular_disease" => "required",
				"endocrine_disease" => "required",
				"urinary_system_disease" => "required",
				"reproductive_system_disease" => "required",
				"communication_hearing_problem" => "required",
				"vision_problem" => "required",
				"oral_dental_health_problem" => "required",
				"others" => "required",
				"under_medication" => "required",
				"recent_treatment" => "required",
				"recent_procedure" => "required",
				"bh_smoking" => "required",
				"bh_alcohol" => "required",
				"bh_substance_abuse" => "required",
				"bh_suicidal_thoughts" => "required",
				"bh_suicidal_attempts" => "required",
				"teacher_ddr_knowledge" => "required"
	        ]);	

	        $drr = isset($data['teacher_ddr_knowledge']) ? $data['teacher_ddr_knowledge'] : NULL;		
		}

	    if($validator->fails() == false)
	    {
	    	$facilityUser = $this->getFacilityUser($user_id,$facility_id);
	    	if(!$facilityUser) {
	    		return new JsonResponse(array('status'=>'error', 'message'=>'Facility or User not found!'), 400);
	    	}

	    	$facilityPatientUser = FacilityPatientUser::where('facilityuser_id',$facilityUser->facilityuser_id)->where('patient_id',$patient_id)->first();
	    	if(!$facilityPatientUser) {
	    		return new JsonResponse(array('status'=>'error', 'message'=>'Patient not found!'), 400);
	    	}

	    	$health_assessment = HealthAssessmentModel::where('uuid',$data['uuid'])->first();
	    	$message = 'Health assessment has been successfully updated!';
	    	if(!$health_assessment)
	    	{
	    		$message = 'New health assessment has been successfully added!';
		    	$hs_id = IdGenerator::generateId();

		    	// Save new healthcare
		    	$healthcare = $this->insertNewHealthcareService($data,$hs_id,'HealthAssessment');

		    	// Save new healthassessment
		    	$health_assessment = new HealthAssessmentModel;

	            $health_assessment->healthassessment_id = IdGenerator::generateId();
	            $health_assessment->healthcareservice_id = $hs_id;
	    	}
	    	else
	    	{
	    		$hs_id = $health_assessment->healthcareservice_id;
	    	}

	    	// Create/update vitals physical
	    	$vitals = $this->insertNewVitals($data,$hs_id);

			$health_assessment->student_employee = isset($studentemployee) ? $studentemployee : NULL;

			$health_assessment->uuid = isset($data['uuid']) ? $data['uuid'] : NULL;
			$health_assessment->owner_uuid = isset($data['ownerUuid']) ? $data['ownerUuid'] : NULL;
			$health_assessment->school = isset($data['school']) ? $data['school'] : NULL;
			$health_assessment->class = isset($data['class']) ? $data['class'] : NULL;
			$health_assessment->vaccine_bcg = isset($data['vaccines_bcg']) ? $data['vaccines_bcg'] : NULL;
			$health_assessment->vaccine_dpt = isset($data['vaccines_dpt']) ? $data['vaccines_dpt'] : NULL;
			$health_assessment->vaccine_opv = isset($data['vaccines_opv']) ? $data['vaccines_opv'] : NULL;
			$health_assessment->vaccine_pcv = isset($data['vaccines_pcv']) ? $data['vaccines_pcv'] : NULL;
			$health_assessment->vaccine_mr = isset($data['vaccines_mr']) ? $data['vaccines_mr'] : NULL;
			$health_assessment->vaccine_je = isset($data['vaccines_je']) ? $data['vaccines_je'] : NULL;
			$health_assessment->vaccine_td = isset($data['vaccines_td']) ? $data['vaccines_td'] : NULL;
			$health_assessment->vaccine_oth = isset($data['vaccines_others']) ? $data['vaccines_others'] : NULL;
			$health_assessment->vaccine_oth_spec = isset($data['vaccines_specify']) ? $data['vaccines_specify'] : NULL;

			$health_assessment->id_watery_diarrhea = isset($data['acute_watery_diarrhea']) ? $data['acute_watery_diarrhea'] : NULL;
			$health_assessment->id_bloody_diarrhea = isset($data['acute_bloody_diarrhea']) ? $data['acute_bloody_diarrhea'] : NULL;
			$health_assessment->id_flaccidpar = isset($data['acute_flaccid_paralysis']) ? $data['acute_flaccid_paralysis'] : NULL;
			$health_assessment->id_ari = isset($data['acute_respiratory_infection']) ? $data['acute_respiratory_infection'] : NULL;
			$health_assessment->id_dysentery = isset($data['id_dysentery']) ? $data['id_dysentery'] : NULL;
			$health_assessment->id_ajs = isset($data['acute_jaundice_infection']) ? $data['acute_jaundice_infection'] : NULL;
			$health_assessment->id_susmeningitis = isset($data['suspected_meningitis']) ? $data['suspected_meningitis'] : NULL;
			$health_assessment->id_sustetanus = isset($data['suspected_tetanus']) ? $data['suspected_tetanus'] : NULL;
			$health_assessment->id_fever = isset($data['fever']) ? $data['fever'] : NULL;
			$health_assessment->id_hemfever = isset($data['acute_hemorraphic_fever']) ? $data['acute_hemorraphic_fever'] : NULL;

			$health_assessment->mc_injuretrauma_head = isset($data['trauma_major_head']) ? $data['trauma_major_head'] : NULL;
			$health_assessment->mc_injuretrauma_spinal = isset($data['trauma_major_spinal']) ? $data['trauma_major_spinal'] : NULL;
			$health_assessment->mc_injuretrauma_torso = isset($data['trauma_major_torso']) ? $data['trauma_major_torso'] : NULL;
			$health_assessment->mc_injuretrauma_leftleg = isset($data['trauma_left_leg']) ? $data['trauma_left_leg'] : NULL;
			$health_assessment->mc_injuretrauma_rightleg = isset($data['trauma_right_leg']) ? $data['trauma_right_leg'] : NULL;
			$health_assessment->mc_injuretrauma_leftarm = isset($data['trauma_left_arm']) ? $data['trauma_left_arm'] : NULL;
			$health_assessment->mc_injuretrauma_rightarm = isset($data['trauma_right_arm']) ? $data['trauma_right_arm'] : NULL;
			$health_assessment->mc_injuretrauma_head_spec = isset($data['trauma_major_head_specify']) ? $data['trauma_major_head_specify'] : NULL;
			$health_assessment->mc_injuretrauma_spinal_spec = isset($data['trauma_major_spinal_specify']) ? $data['trauma_major_spinal_specify'] : NULL;
			$health_assessment->mc_injuretrauma_torso_spec = isset($data['trauma_major_torso_specify']) ? $data['trauma_major_torso_specify'] : NULL;
			$health_assessment->mc_injuretrauma_leftleg_spec = isset($data['trauma_left_leg_specify']) ? $data['trauma_left_leg_specify'] : NULL;
			$health_assessment->mc_injuretrauma_rightleg_spec = isset($data['trauma_right_leg_specify']) ? $data['trauma_right_leg_specify'] : NULL;
			$health_assessment->mc_injuretrauma_leftarm_spec = isset($data['trauma_left_arm_specify']) ? $data['trauma_left_arm_specify'] : NULL;
			$health_assessment->mc_injuretrauma_rightarm_spec = isset($data['trauma_right_arm_specify']) ? $data['trauma_right_arm_specify'] : NULL;

			$health_assessment->mc_skindisease = isset($data['skin_diseases']) ? $data['skin_diseases'] : NULL;
			$health_assessment->mc_skindisease_spec = isset($data['skin_diseases_specify']) ? $data['skin_diseases_specify'] : NULL;
			$health_assessment->mc_allergies = isset($data['allegies']) ? $data['allegies'] : NULL;
			$health_assessment->mc_allergies_spec = isset($data['allegies_specify']) ? $data['allegies_specify'] : NULL;
			$health_assessment->mc_respiratory = isset($data['respiratory_disease']) ? $data['respiratory_disease'] : NULL;
			$health_assessment->mc_respiratory_spec = isset($data['respiratory_disease_specify']) ? $data['respiratory_disease_specify'] : NULL;
			$health_assessment->mc_cardio = isset($data['cardiovascular_disease']) ? $data['cardiovascular_disease'] : NULL;
			$health_assessment->mc_cardio_spec = isset($data['cardiovascular_disease_specify']) ? $data['cardiovascular_disease_specify'] : NULL;
			$health_assessment->mc_endocrine = isset($data['endocrine_disease']) ? $data['endocrine_disease'] : NULL;
			$health_assessment->mc_endocrine_spec = isset($data['endocrine_disease_specify']) ? $data['endocrine_disease_specify'] : NULL;
			$health_assessment->mc_urinary = isset($data['urinary_system_disease']) ? $data['urinary_system_disease'] : NULL;
			$health_assessment->mc_urinary_spec = isset($data['urinary_system_disease_specify']) ? $data['urinary_system_disease_specify'] : NULL;
			$health_assessment->mc_reproductive = isset($data['reproductiv_system_disease']) ? $data['reproductiv_system_disease'] : NULL;
			$health_assessment->mc_reproductive_spec = isset($data['reproductiv_system_disease_specify']) ? $data['reproductiv_system_disease_specify'] : NULL;
			$health_assessment->mc_commhearing = isset($data['communication_hearing_problem']) ? $data['communication_hearing_problem'] : NULL;
			$health_assessment->mc_commhearing_spec = isset($data['communication_hearing_problem_specify']) ? $data['communication_hearing_problem_specify'] : NULL;
			$health_assessment->mc_vision = isset($data['vision_problem']) ? $data['vision_problem'] : NULL;
			$health_assessment->mc_vision_spec = isset($data['vision_problem_specify']) ? $data['vision_problem_specify'] : NULL;
			$health_assessment->mc_oraldental = isset($data['oral_dental_health_problem']) ? $data['oral_dental_health_problem'] : NULL;
			$health_assessment->mc_oraldental_spec = isset($data['oral_dental_health_problem_specify']) ? $data['oral_dental_health_problem_specify'] : NULL;
			$health_assessment->mc_psych = isset($data['psychological_problem']) ? $data['psychological_problem'] : NULL;
			$health_assessment->mc_psych_spec = isset($data['psychological_problem_specify']) ? $data['psychological_problem_specify'] : NULL;
			$health_assessment->mc_physical = isset($data['physical_structural_problems']) ? $data['physical_structural_problems'] : NULL;
			$health_assessment->mc_physical_spec = isset($data['physical_structural_problems_specify']) ? $data['physical_structural_problems_specify'] : NULL;
			$health_assessment->mc_cognitive = isset($data['cognative_patterns']) ? $data['cognative_patterns'] : NULL;
			$health_assessment->mc_cognitive_spec = isset($data['cognative_patterns_specify']) ? $data['cognative_patterns_specify'] : NULL;
			$health_assessment->mc_bowel = isset($data['bowel_habit']) ? $data['bowel_habit'] : NULL;
			$health_assessment->mc_bowel_spec = isset($data['bowel_habit_specify']) ? $data['bowel_habit_specify'] : NULL;
			$health_assessment->mc_bladder = isset($data['bladder_habit']) ? $data['bladder_habit'] : NULL;
			$health_assessment->mc_bladder_spec = isset($data['bladder_habit_specify']) ? $data['bladder_habit_specify'] : NULL;
			$health_assessment->mc_oth_spec = isset($data['others']) ? $data['others'] : NULL;

			$health_assessment->medications = isset($data['under_medication']) ? $data['under_medication'] : NULL;
			$health_assessment->medications_spec = isset($data['under_medication_specify']) ? $data['under_medication_specify'] : NULL;

			$health_assessment->treatment = isset($data['recent_treatment']) ? $data['recent_treatment'] : NULL;
			$health_assessment->treatment_spec = isset($data['recent_treatment_specify']) ? $data['recent_treatment_specify'] : NULL;
			$health_assessment->procedure = isset($data['recent_procedure']) ? $data['recent_procedure'] : NULL;
			$health_assessment->procedure_spec = isset($data['recent_procedure_specify']) ? $data['recent_procedure_specify'] : NULL;
			$health_assessment->counselling = isset($data['recent_counseling']) ? $data['recent_counseling'] : NULL;
			$health_assessment->counselling_spec = isset($data['recent_counseling_specify']) ? $data['recent_counseling_specify'] : NULL;

			$health_assessment->suspensionschool = isset($data['bh_suspension']) ? $data['bh_suspension'] : NULL;
			$health_assessment->reasonsuspension = isset($data['bh_suspension_specify']) ? $data['bh_suspension_specify'] : NULL;
			$health_assessment->antisocial = isset($data['bh_anti_social_behavior']) ? $data['bh_anti_social_behavior'] : NULL;
			$health_assessment->delinquency = isset($data['bh_delinquency']) ? $data['bh_delinquency'] : NULL;
			$health_assessment->violence = isset($data['bh_violence']) ? $data['bh_violence'] : NULL;
			$health_assessment->smoking = isset($data['bh_smoking']) ? $data['bh_smoking'] : NULL;
			$health_assessment->alcohol = isset($data['bh_alcohol']) ? $data['bh_alcohol'] : NULL;
			$health_assessment->substanceabuse = isset($data['bh_substance_abuse']) ? $data['bh_substance_abuse'] : NULL;
			$health_assessment->suicidalthoughts = isset($data['bh_suicidal_thoughts']) ? $data['bh_suicidal_thoughts'] : NULL;
			$health_assessment->suicidalattempts = isset($data['bh_suicidal_attempts']) ? $data['bh_suicidal_attempts'] : NULL;
			$health_assessment->breakfast_place = isset($data['breakfast_place']) ? $data['breakfast_place'] : NULL;
			$health_assessment->breakfast_time = isset($data['breakfast_time']) ? $data['breakfast_time'] : NULL;
			$health_assessment->breakfast_type_food = isset($data['breakfast_type_of_food']) ? $data['breakfast_type_of_food'] : NULL;
			$health_assessment->lunch_place = isset($data['lunch_place']) ? $data['lunch_place'] : NULL;
			$health_assessment->lunch_time = isset($data['lunch_time']) ? $data['lunch_time'] : NULL;
			$health_assessment->lunch_type_food = isset($data['lunch_type_of_food']) ? $data['lunch_type_of_food'] : NULL;
			$health_assessment->snacks_place = isset($data['snack_place']) ? $data['snack_place'] : NULL;
			$health_assessment->snacks_time = isset($data['snack_time']) ? $data['snack_time'] : NULL;
			$health_assessment->snacks_type_food = isset($data['snack_type_of_food']) ? $data['snack_type_of_food'] : NULL;
			$health_assessment->dinner_place = isset($data['dinner_place']) ? $data['dinner_place'] : NULL;
			$health_assessment->dinner_time = isset($data['dinner_time']) ? $data['dinner_time'] : NULL;
			$health_assessment->dinner_type_food = isset($data['dinner_type_of_food']) ? $data['dinner_type_of_food'] : NULL;
			$health_assessment->malnutrition = isset($data['malnutrition']) ? $data['malnutrition'] : NULL;
			$health_assessment->malnutrition_spec = isset($data['malnutrition_specify']) ? $data['malnutrition_specify'] : NULL;
			$health_assessment->idle_hours = isset($data['idle_hours']) ? $data['idle_hours'] : NULL;
			$health_assessment->active_hours = isset($data['active_hours']) ? $data['active_hours'] : NULL;
			$health_assessment->age_menarche = isset($data['age_of_menarche']) ? $data['age_of_menarche'] : NULL;
			$health_assessment->material_menstruation = isset($data['menstruation_materials']) ? $data['menstruation_materials'] : NULL;
			$health_assessment->family_involvement = isset($data['family_interaction']) ? $data['family_interaction'] : NULL;
			$health_assessment->family_involvement_reason = isset($data['family_interaction_specify']) ? $data['family_interaction_specify'] : NULL;
			$health_assessment->community_involvement = isset($data['community_involvement']) ? $data['community_involvement'] : NULL;
			$health_assessment->community_involvement_reason = isset($data['community_involvement_specify']) ? $data['community_involvement_specify'] : NULL;
			$health_assessment->drr_knowledge = $drr;

			$health_assessment->child_complaint = isset($data['child_complaint']) ? $data['child_complaint'] : NULL;
	       
	        if($health_assessment->save())
	        {
	    		return new JsonResponse(array('status'=>'success', 'message'=>$message), 200);
	        }
	        else
	        {
	    		return new JsonResponse(array('status'=>'error', 'message'=>'Error in student/employee health assessment!'), 400);
	        }
	    }
	    else
	    {
	    	// return $this->json_response('error',$validator->errors()->all(),400);
	    	return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
	    }
	}
}