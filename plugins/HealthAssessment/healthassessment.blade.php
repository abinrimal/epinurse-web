<?php

    $hservice_id = $allData['healthcareserviceid'];
    $shaservice_id = $data['healthassessment_id'];

    $suspensionReasons = array(
    	'' => 'Select Reason',
    	0 => 'Disobedience',
    	1 => 'Misconduct',
    	2 => 'Possessing drugs',
    	3 => 'Under influence of drugs',
    	4 => 'Damage/Theft of property',
    	5 => 'Bullying/Harassment',
    	5 => 'Bodily injury to self or others',
    	6 => 'Illness',
    )

?>

{!! Form::hidden('healthassessment[shaservice_id]',$shaservice_id) !!}
{!! Form::hidden('healthassessment[hservice_id]',$hservice_id) !!}

<style type="text/css">
	img.epinurse-icon {
		height: 50px;
	}
</style>

<fieldset>
	<legend>Student/Employee Health Assessment</legend>

    <div class="form-group col-sm-12">
        <div class="row">
 			<div class="radio col-sm-offset-1 col-sm-10">
 				<label class="col-sm-3 control-label">Student/Employee</label>
		        <div class="col-sm-9">
		            <div class="btn-group toggler" data-toggle="buttons">
		              <label id="student-button" class="btn btn-default @if(isset($data['student_employee']) && $data['student_employee'] == 'Student') active @endif">
		                <i class="fa fa-check"></i> <input type="radio" name="healthassessment[student_employee]" id="student-radio" autocomplete="off" @if(isset($data['student_employee']) && $data['student_employee'] == 'Student') checked @endif value="Student"> Student
		              </label>
		              <label id="employee-button" class="btn btn-default @if(isset($data['student_employee']) && $data['student_employee'] == 'Employee') active @endif">
		                <i class="fa fa-check"></i> <input type="radio" name="healthassessment[student_employee]" id="employee-radio" autocomplete="off" @if(isset($data['student_employee']) && $data['student_employee'] == 'Employee') checked @endif value="Employee"> Employee
		              </label>
		              <label id="na-button" class="btn btn-default @if(isset($data['student_employee']) && $data['student_employee'] == 'NA') active @endif">
		                <i class="fa fa-check"></i> <input type="radio" name="healthassessment[student_employee]" id="na-radio" autocomplete="off" @if(isset($data['student_employee']) && $data['student_employee'] == 'NA') checked @endif value="NA"> NA
		              </label>
		            </div>
		        </div>
	    	</div>
        </div>
    </div>

    <ul class="nav nav-tabs hidden student employee">
        <li class="hidden student"><a href="#schoolinfo" data-toggle="tab">School Information</a></li>
        <li class="hidden student"><a href="#immunization" data-toggle="tab">Immunization</a></li>
        <li class="hidden student employee"><a href="#infectiousdisease" data-toggle="tab">Infectious Disease</a></li>
        <li class="hidden student employee"><a href="#medicalconditions" data-toggle="tab">Medical Conditions</a></li>
        <li class="hidden student employee"><a href="#medications" data-toggle="tab">Medications</a></li>
        <li class="hidden student employee"><a href="#treatment" data-toggle="tab">Treatment/Procedure/Counselling</a></li>
        <li class="hidden student employee"><a href="#behavioral" data-toggle="tab">Behavioral Health</a></li>
        <li class="hidden student"><a href="#foodintake" data-toggle="tab">Food Intake</a></li>
        <li class="hidden student"><a href="#malnutrition" data-toggle="tab">Malnutrition</a></li>
        <li class="hidden student"><a href="#activities" data-toggle="tab">Activities</a></li>
        <li class="hidden student employee"><a href="#menstrualhealth" data-toggle="tab">Menstrual Health</a></li>
        <li class="hidden student"><a href="#family" data-toggle="tab">Family Involvement</a></li>
        <li class="hidden student employee"><a href="#drrknowledge" data-toggle="tab">DRR Knowledge</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade" id="schoolinfo">
        	<div class="col-sm-12">
				<div class="page-header">
				  <h4>School Information</h4>
				</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="col-sm-offset-1 col-sm-10">
		 			<label class="col-sm-4 control-label"> School </label>
		 			<div class="col-sm-6">
		 				{!! Form::text('healthassessment[school]',(isset($data['school'])) ? $data['school'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="col-sm-offset-1 col-sm-10">
		 			<label class="col-sm-4 control-label"> Class </label>
		 			<div class="col-sm-6">
		 				{!! Form::text('healthassessment[class]',(isset($data['class'])) ? $data['class'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
	 		</div>
        </div>
        <div class="tab-pane fade" id="immunization">
	 		<div class="col-sm-12">
				<div class="page-header">
				  <h4>Vaccines Administered</h4>
				</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
	 				<div class="col-sm-offset-1 col-sm-10">
	 					<label class="control-label checkbox">
							{!! Form::checkbox('healthassessment[vaccine_bcg]', 1, (isset($data['vaccine_bcg']) && $data['vaccine_bcg'] == 1) ? true : false) !!} BCG
						</label>
	 					<label class="control-label checkbox">
							{!! Form::checkbox('healthassessment[vaccine_dpt]', 1, (isset($data['vaccine_dpt']) && $data['vaccine_dpt'] == 1) ? true : false) !!} DPT
						</label>
						<label class="control-label checkbox">
							{!! Form::checkbox('healthassessment[vaccine_opv]', 1, (isset($data['vaccine_opv']) && $data['vaccine_opv'] == 1) ? true : false) !!} OPV
						</label>
						<label class="control-label checkbox">
							{!! Form::checkbox('healthassessment[vaccine_pcv]', 1, (isset($data['vaccine_pcv']) && $data['vaccine_pcv'] == 1) ? true : false) !!} PCV
						</label>
						<label class="control-label checkbox">
							{!! Form::checkbox('healthassessment[vaccine_mr]', 1, (isset($data['vaccine_mr']) && $data['vaccine_mr'] == 1) ? true : false) !!} MR
						</label>
						<label class="control-label checkbox">
							{!! Form::checkbox('healthassessment[vaccine_je]', 1, (isset($data['vaccine_je']) && $data['vaccine_je'] == 1) ? true : false) !!} JE
						</label>
						<label class="control-label checkbox">
							{!! Form::checkbox('healthassessment[vaccine_td]', 1, (isset($data['vaccine_td']) && $data['vaccine_td'] == 1) ? true : false) !!} TD
						</label>
						<label class="control-label checkbox">
							<div class="form-inline">
								{!! Form::checkbox('healthassessment[vaccine_oth]', 1, (isset($data['vaccine_oth']) && $data['vaccine_oth'] == 1) ? true : false) !!} Others, specify: 
								{!! Form::text('healthassessment[vaccine_oth_spec]', (isset($data['vaccine_oth_spec'])) ? $data['vaccine_oth_spec'] : NULL, ['class'=>'form-control']) !!}
							</div>
						</label>
	 				</div>
	 			</div>
	 		</div>
	 	</div>
        <div class="tab-pane fade" id="infectiousdisease">
	 		<div class="col-sm-12">
				<div class="page-header">
				  <h4>Infectious Disease</h4>
				</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Acute Respiratory Infection</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_ari]',0,(isset($data['id_ari']) && $data['id_ari'] == 0) ? true : false) !!} No 
		 				</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_ari]',1,(isset($data['id_ari']) && $data['id_ari'] == 1) ? true : false) !!} Yes
		 				</label>
		 			</div>
		 		</div>
	 		</div>
			<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Acute Watery Diarrhea</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_watery_diarrhea]',0,(isset($data['id_watery_diarrhea']) && $data['id_watery_diarrhea'] == 0) ? true : false) !!} No 
		 				</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_watery_diarrhea]',1,(isset($data['id_watery_diarrhea']) && $data['id_watery_diarrhea'] == 1) ? true : false) !!} Yes
		 				</label>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Acute Bloody Diarrhea</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_bloody_diarrhea]',0,(isset($data['id_bloody_diarrhea']) && $data['id_bloody_diarrhea'] == 0) ? true : false) !!} No 
		 				</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_bloody_diarrhea]',1,(isset($data['id_bloody_diarrhea']) && $data['id_bloody_diarrhea'] == 1) ? true : false) !!} Yes
		 				</label>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Dysentery</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_dysentery]',0,(isset($data['id_dysentery']) && $data['id_dysentery'] == 0) ? true : false) !!} No 
		 				</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_dysentery]',1,(isset($data['id_dysentery']) && $data['id_dysentery'] == 1) ? true : false) !!} Yes
		 				</label>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Acute Jaundice Symptoms</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_ajs]',0,(isset($data['id_ajs']) && $data['id_ajs'] == 0) ? true : false) !!} No 
		 				</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_ajs]',1,(isset($data['id_ajs']) && $data['id_ajs'] == 1) ? true : false) !!} Yes
		 				</label>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Suspected Meningitis</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_susmeningitis]',0,(isset($data['id_susmeningitis']) && $data['id_susmeningitis'] == 0) ? true : false) !!} No 
		 				</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_susmeningitis]',1,(isset($data['id_susmeningitis']) && $data['id_susmeningitis'] == 1) ? true : false) !!} Yes
		 				</label>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Suspected Tetanus</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_sustetanus]',0,(isset($data['id_sustetanus']) && $data['id_sustetanus'] == 0) ? true : false) !!} No 
		 				</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_sustetanus]',1,(isset($data['id_sustetanus']) && $data['id_sustetanus'] == 1) ? true : false) !!} Yes
		 				</label>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Acute Flaccid Paralysis</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_flaccidpar]',0,(isset($data['id_flaccidpar']) && $data['id_flaccidpar'] == 0) ? true : false) !!} No 
		 				</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_flaccidpar]',1,(isset($data['id_flaccidpar']) && $data['id_flaccidpar'] == 1) ? true : false) !!} Yes
		 				</label>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Acute Hemorrhagic Fever</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_hemfever]',0,(isset($data['id_hemfever']) && $data['id_hemfever'] == 0) ? true : false) !!} No 
		 				</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_hemfever]',1,(isset($data['id_hemfever']) && $data['id_hemfever'] == 1) ? true : false) !!} Yes
		 				</label>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Fever</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_fever]',0,(isset($data['id_fever']) && $data['id_fever'] == 0) ? true : false) !!} No 
		 				</label>
		 				<label class="radio-inline">
		 					{!! Form::radio('healthassessment[id_fever]',1,(isset($data['id_fever']) && $data['id_fever'] == 1) ? true : false) !!} Yes
		 				</label>
		 			</div>
		 		</div>
	 		</div>
        </div><!-- /.tab-pane -->
        <div class="tab-pane fade" id="medicalconditions">
	 		<div class="col-sm-12">
				<div class="page-header">
				  <h4>Medical Conditions</h4>
				</div>
	 		</div>
			<div class="hidden student">
		 		<div class="col-sm-12">
		 			<div class="icheck">
			 			<div class="radio col-sm-offset-1 col-sm-10">
			 				<label class="col-sm-4 control-label">Major Head Injuries/Trauma</label>
			 				<div class="col-sm-4">
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_head]',0,(isset($data['mc_injuretrauma_head']) && $data['mc_injuretrauma_head'] == 0) ? true : false,['id'=>'mc_injuretrauma_head_no', 'class'=>'mc_injuretrauma_head_radio']) !!} No 
				 				</label>
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_head]',1,(isset($data['mc_injuretrauma_head']) && $data['mc_injuretrauma_head'] == 1) ? true : false,['id'=>'mc_injuretrauma_head_yes', 'class'=>'mc_injuretrauma_head_radio']) !!} Yes
				 				</label>
				 			</div>
					 		<div class="<?php if(!isset($data['mc_injuretrauma_head']) || (isset($data['mc_injuretrauma_head']) && $data['mc_injuretrauma_head'] != 1)) { echo 'hidden'; } ?>" id="mc_injuretrauma_head_spec">
					 			<label class="col-sm-1 control-label">Specify: </label>
					 			<div class="col-sm-3">
					 				{!! Form::text('healthassessment[mc_injuretrauma_head_spec]', (isset($data['mc_injuretrauma_head_spec'])) ? $data['mc_injuretrauma_head_spec'] : NULL, ['class'=>'form-control','id'=>'mc_injuretrauma_head_spec_input']) !!}
					 			</div>
				 			</div>
				 		</div> 
				 	</div>
		 		</div>
			</div>
			<div class="hidden student">
		 		<div class="col-sm-12">
		 			<div class="icheck">
			 			<div class="radio col-sm-offset-1 col-sm-10">
			 				<label class="col-sm-4 control-label">Major Spinal Injuries/Trauma</label>
			 				<div class="col-sm-4">
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_spinal]',0,(isset($data['mc_injuretrauma_spinal']) && $data['mc_injuretrauma_spinal'] == 0) ? true : false,['id'=>'mc_injuretrauma_spinal_no', 'class'=>'mc_injuretrauma_spinal_radio']) !!} No 
				 				</label>
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_spinal]',1,(isset($data['mc_injuretrauma_spinal']) && $data['mc_injuretrauma_spinal'] == 1) ? true : false,['id'=>'mc_injuretrauma_spinal_yes', 'class'=>'mc_injuretrauma_spinal_radio']) !!} Yes
				 				</label>
				 			</div>
					 		<div class="<?php if(!isset($data['mc_injuretrauma_spinal']) || (isset($data['mc_injuretrauma_spinal']) && $data['mc_injuretrauma_spinal'] != 1)) { echo 'hidden'; } ?>" id="mc_injuretrauma_spinal_spec">
					 			<label class="col-sm-1 control-label">Specify: </label>
					 			<div class="col-sm-3">
					 				{!! Form::text('healthassessment[mc_injuretrauma_spinal_spec]', (isset($data['mc_injuretrauma_spinal_spec'])) ? $data['mc_injuretrauma_spinal_spec'] : NULL, ['class'=>'form-control','id'=>'mc_injuretrauma_spinal_spec_input']) !!}
					 			</div>
				 			</div>
				 		</div> 
				 	</div>
		 		</div>
			</div>
			<div class="hidden student">
		 		<div class="col-sm-12">
		 			<div class="icheck">
			 			<div class="radio col-sm-offset-1 col-sm-10">
			 				<label class="col-sm-4 control-label">Major Torso Injuries/Trauma</label>
			 				<div class="col-sm-4">
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_torso]',0,(isset($data['mc_injuretrauma_torso']) && $data['mc_injuretrauma_torso'] == 0) ? true : false,['id'=>'mc_injuretrauma_torso_no', 'class'=>'mc_injuretrauma_torso_radio']) !!} No 
				 				</label>
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_torso]',1,(isset($data['mc_injuretrauma_torso']) && $data['mc_injuretrauma_torso'] == 1) ? true : false,['id'=>'mc_injuretrauma_torso_yes', 'class'=>'mc_injuretrauma_torso_radio']) !!} Yes
				 				</label>
				 			</div>
					 		<div class="<?php if(!isset($data['mc_injuretrauma_torso']) || (isset($data['mc_injuretrauma_torso']) && $data['mc_injuretrauma_torso'] != 1)) { echo 'hidden'; } ?>" id="mc_injuretrauma_torso_spec">
					 			<label class="col-sm-1 control-label">Specify: </label>
					 			<div class="col-sm-3">
					 				{!! Form::text('healthassessment[mc_injuretrauma_torso_spec]', (isset($data['mc_injuretrauma_torso_spec'])) ? $data['mc_injuretrauma_torso_spec'] : NULL, ['class'=>'form-control','id'=>'mc_injuretrauma_torso_spec_input']) !!}
					 			</div>
				 			</div>
				 		</div> 
				 	</div>
		 		</div>
			</div>
			<div class="hidden student">
		 		<div class="col-sm-12">
		 			<div class="icheck">
			 			<div class="radio col-sm-offset-1 col-sm-10">
			 				<label class="col-sm-4 control-label"> Left Leg Injuries/Trauma</label>
			 				<div class="col-sm-4">
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_leftleg]',0,(isset($data['mc_injuretrauma_leftleg']) && $data['mc_injuretrauma_leftleg'] == 0) ? true : false,['id'=>'mc_injuretrauma_leftleg_no', 'class'=>'mc_injuretrauma_leftleg_radio']) !!} No 
				 				</label>
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_leftleg]',1,(isset($data['mc_injuretrauma_leftleg']) && $data['mc_injuretrauma_leftleg'] == 1) ? true : false,['id'=>'mc_injuretrauma_leftleg_yes', 'class'=>'mc_injuretrauma_leftleg_radio']) !!} Yes
				 				</label>
				 			</div>
					 		<div class="<?php if(!isset($data['mc_injuretrauma_leftleg']) || (isset($data['mc_injuretrauma_leftleg']) && $data['mc_injuretrauma_leftleg'] != 1)) { echo 'hidden'; } ?>" id="mc_injuretrauma_leftleg_spec">
					 			<label class="col-sm-1 control-label">Specify: </label>
					 			<div class="col-sm-3">
					 				{!! Form::text('healthassessment[mc_injuretrauma_leftleg_spec]', (isset($data['mc_injuretrauma_leftleg_spec'])) ? $data['mc_injuretrauma_leftleg_spec'] : NULL, ['class'=>'form-control','id'=>'mc_injuretrauma_leftleg_spec_input']) !!}
					 			</div>
				 			</div>
				 		</div> 
				 	</div>
		 		</div>
			</div>
			<div class="hidden student">
		 		<div class="col-sm-12">
		 			<div class="icheck">
			 			<div class="radio col-sm-offset-1 col-sm-10">
			 				<label class="col-sm-4 control-label">Right Leg Injuries/Trauma</label>
			 				<div class="col-sm-4">
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_rightleg]',0,(isset($data['mc_injuretrauma_rightleg']) && $data['mc_injuretrauma_rightleg'] == 0) ? true : false,['id'=>'mc_injuretrauma_rightleg_no', 'class'=>'mc_injuretrauma_rightleg_radio']) !!} No 
				 				</label>
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_rightleg]',1,(isset($data['mc_injuretrauma_rightleg']) && $data['mc_injuretrauma_rightleg'] == 1) ? true : false,['id'=>'mc_injuretrauma_rightleg_yes', 'class'=>'mc_injuretrauma_rightleg_radio']) !!} Yes
				 				</label>
				 			</div>
					 		<div class="<?php if(!isset($data['mc_injuretrauma_rightleg']) || (isset($data['mc_injuretrauma_rightleg']) && $data['mc_injuretrauma_rightleg'] != 1)) { echo 'hidden'; } ?>" id="mc_injuretrauma_rightleg_spec">
					 			<label class="col-sm-1 control-label">Specify: </label>
					 			<div class="col-sm-3">
					 				{!! Form::text('healthassessment[mc_injuretrauma_rightleg_spec]', (isset($data['mc_injuretrauma_rightleg_spec'])) ? $data['mc_injuretrauma_rightleg_spec'] : NULL, ['class'=>'form-control','id'=>'mc_injuretrauma_rightleg_spec_input']) !!}
					 			</div>
				 			</div>
				 		</div> 
				 	</div>
		 		</div>
			</div>
			<div class="hidden student">
		 		<div class="col-sm-12">
		 			<div class="icheck">
			 			<div class="radio col-sm-offset-1 col-sm-10">
			 				<label class="col-sm-4 control-label">Left Arm Injuries/Trauma</label>
			 				<div class="col-sm-4">
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_leftarm]',0,(isset($data['mc_injuretrauma_leftarm']) && $data['mc_injuretrauma_leftarm'] == 0) ? true : false,['id'=>'mc_injuretrauma_leftarm_no', 'class'=>'mc_injuretrauma_leftarm_radio']) !!} No 
				 				</label>
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_leftarm]',1,(isset($data['mc_injuretrauma_leftarm']) && $data['mc_injuretrauma_leftarm'] == 1) ? true : false,['id'=>'mc_injuretrauma_leftarm_yes', 'class'=>'mc_injuretrauma_leftarm_radio']) !!} Yes
				 				</label>
				 			</div>
					 		<div class="<?php if(!isset($data['mc_injuretrauma_leftarm']) || (isset($data['mc_injuretrauma_leftarm']) && $data['mc_injuretrauma_leftarm'] != 1)) { echo 'hidden'; } ?>" id="mc_injuretrauma_leftarm_spec">
					 			<label class="col-sm-1 control-label">Specify: </label>
					 			<div class="col-sm-3">
					 				{!! Form::text('healthassessment[mc_injuretrauma_leftarm_spec]', (isset($data['mc_injuretrauma_leftarm_spec'])) ? $data['mc_injuretrauma_leftarm_spec'] : NULL, ['class'=>'form-control','id'=>'mc_injuretrauma_leftarm_spec_input']) !!}
					 			</div>
				 			</div>
				 		</div> 
				 	</div>
		 		</div>
			</div>
			<div class="hidden student">
		 		<div class="col-sm-12">
		 			<div class="icheck">
			 			<div class="radio col-sm-offset-1 col-sm-10">
			 				<label class="col-sm-4 control-label">Right Arm Injuries/Trauma</label>
			 				<div class="col-sm-4">
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_rightarm]',0,(isset($data['mc_injuretrauma_rightarm']) && $data['mc_injuretrauma_rightarm'] == 0) ? true : false,['id'=>'mc_injuretrauma_rightarm_no', 'class'=>'mc_injuretrauma_rightarm_radio']) !!} No 
				 				</label>
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma_rightarm]',1,(isset($data['mc_injuretrauma_rightarm']) && $data['mc_injuretrauma_rightarm'] == 1) ? true : false,['id'=>'mc_injuretrauma_rightarm_yes', 'class'=>'mc_injuretrauma_rightarm_radio']) !!} Yes
				 				</label>
				 			</div>
					 		<div class="<?php if(!isset($data['mc_injuretrauma_rightarm']) || (isset($data['mc_injuretrauma_rightarm']) && $data['mc_injuretrauma_rightarm'] != 1)) { echo 'hidden'; } ?>" id="mc_injuretrauma_rightarm_spec">
					 			<label class="col-sm-1 control-label">Specify: </label>
					 			<div class="col-sm-3">
					 				{!! Form::text('healthassessment[mc_injuretrauma_rightarm_spec]', (isset($data['mc_injuretrauma_rightarm_spec'])) ? $data['mc_injuretrauma_rightarm_spec'] : NULL, ['class'=>'form-control','id'=>'mc_injuretrauma_rightarm_spec_input']) !!}
					 			</div>
				 			</div>
				 		</div> 
				 	</div>
		 		</div>
			</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Skin Diseases</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_skindisease]',0,(isset($data['mc_skindisease']) && $data['mc_skindisease'] == 0) ? true : false,['id'=>'mc_skindisease_no', 'class'=>'mc_skindisease_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_skindisease]',1,(isset($data['mc_skindisease']) && $data['mc_skindisease'] == 1) ? true : false,['id'=>'mc_skindisease_yes', 'class'=>'mc_skindisease_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_skindisease']) || (isset($data['mc_skindisease']) && $data['mc_skindisease'] != 1)) { echo 'hidden'; } ?>" id="mc_skindisease_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_skindisease_spec]', (isset($data['mc_skindisease_spec'])) ? $data['mc_skindisease_spec'] : NULL, ['class'=>'form-control','id'=>'mc_skindisease_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="hidden employee">
		 		<div class="col-sm-12">
		 			<div class="icheck">
			 			<div class="radio col-sm-offset-1 col-sm-10">
			 				<label class="col-sm-4 control-label">Injuries/Trauma</label>
			 				<div class="col-sm-4">
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma]',0,(isset($data['mc_injuretrauma']) && $data['mc_injuretrauma'] == 0) ? true : false,['id'=>'mc_injuretrauma_no', 'class'=>'mc_injuretrauma_radio']) !!} No 
				 				</label>
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[mc_injuretrauma]',1,(isset($data['mc_injuretrauma']) && $data['mc_injuretrauma'] == 1) ? true : false,['id'=>'mc_injuretrauma_yes', 'class'=>'mc_injuretrauma_radio']) !!} Yes
				 				</label>
				 			</div>
					 		<div class="<?php if(!isset($data['mc_injuretrauma']) || (isset($data['mc_injuretrauma']) && $data['mc_injuretrauma'] != 1)) { echo 'hidden'; } ?>" id="mc_injuretrauma_spec">
					 			<label class="col-sm-1 control-label">Specify: </label>
					 			<div class="col-sm-3">
					 				{!! Form::text('healthassessment[mc_injuretrauma_spec]', (isset($data['mc_injuretrauma_spec'])) ? $data['mc_injuretrauma_spec'] : NULL, ['class'=>'form-control','id'=>'mc_injuretrauma_spec_input']) !!}
					 			</div>
				 			</div>
				 		</div> 
				 	</div>
		 		</div>
			</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Allergies</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_allergies]',0,(isset($data['mc_allergies']) && $data['mc_allergies'] == 0) ? true : false,['id'=>'mc_allergies_no', 'class'=>'mc_allergies_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_allergies]',1,(isset($data['mc_allergies']) && $data['mc_allergies'] == 1) ? true : false,['id'=>'mc_allergies_yes', 'class'=>'mc_allergies_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_allergies']) || (isset($data['mc_allergies']) && $data['mc_allergies'] != 1)) { echo 'hidden'; } ?>" id="mc_allergies_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_allergies_spec]', (isset($data['mc_allergies_spec'])) ? $data['mc_allergies_spec'] : NULL, ['class'=>'form-control','id'=>'mc_allergies_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Respiratory Diseases</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_respiratory]',0,(isset($data['mc_respiratory']) && $data['mc_respiratory'] == 0) ? true : false,['id'=>'mc_respiratory_no', 'class'=>'mc_respiratory_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_respiratory]',1,(isset($data['mc_respiratory']) && $data['mc_respiratory'] == 1) ? true : false,['id'=>'mc_respiratory_yes', 'class'=>'mc_respiratory_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_respiratory']) || (isset($data['mc_respiratory']) && $data['mc_respiratory'] != 1)) { echo 'hidden'; } ?>" id="mc_respiratory_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_respiratory_spec]', (isset($data['mc_respiratory_spec'])) ? $data['mc_respiratory_spec'] : NULL, ['class'=>'form-control','id'=>'mc_respiratory_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Cardiovascular Diseases</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_cardio]',0,(isset($data['mc_cardio']) && $data['mc_cardio'] == 0) ? true : false,['id'=>'mc_cardio_no', 'class'=>'mc_cardio_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_cardio]',1,(isset($data['mc_cardio']) && $data['mc_cardio'] == 1) ? true : false,['id'=>'mc_cardio_yes', 'class'=>'mc_cardio_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_cardio']) || (isset($data['mc_cardio']) && $data['mc_cardio'] != 1)) { echo 'hidden'; } ?>" id="mc_cardio_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_cardio_spec]', (isset($data['mc_cardio_spec'])) ? $data['mc_cardio_spec'] : NULL, ['class'=>'form-control','id'=>'mc_cardio_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Endocrine Diseases</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_endocrine]',0,(isset($data['mc_endocrine']) && $data['mc_endocrine'] == 0) ? true : false,['id'=>'mc_endocrine_no', 'class'=>'mc_endocrine_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_endocrine]',1,(isset($data['mc_endocrine']) && $data['mc_endocrine'] == 1) ? true : false,['id'=>'mc_endocrine_yes', 'class'=>'mc_endocrine_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_endocrine']) || (isset($data['mc_endocrine']) && $data['mc_endocrine'] != 1)) { echo 'hidden'; } ?>" id="mc_endocrine_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_endocrine_spec]', (isset($data['mc_endocrine_spec'])) ? $data['mc_endocrine_spec'] : NULL, ['class'=>'form-control','id'=>'mc_endocrine_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Urinary System Diseases</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_urinary]',0,(isset($data['mc_urinary']) && $data['mc_urinary'] == 0) ? true : false,['id'=>'mc_urinary_no', 'class'=>'mc_urinary_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_urinary]',1,(isset($data['mc_urinary']) && $data['mc_urinary'] == 1) ? true : false,['id'=>'mc_urinary_yes', 'class'=>'mc_urinary_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_urinary']) || (isset($data['mc_urinary']) && $data['mc_urinary'] != 1)) { echo 'hidden'; } ?>" id="mc_urinary_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_urinary_spec]', (isset($data['mc_urinary_spec'])) ? $data['mc_urinary_spec'] : NULL, ['class'=>'form-control','id'=>'mc_urinary_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Reproductive System Diseases</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_reproductive]',0,(isset($data['mc_reproductive']) && $data['mc_reproductive'] == 0) ? true : false,['id'=>'mc_reproductive_no', 'class'=>'mc_reproductive_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_reproductive]',1,(isset($data['mc_reproductive']) && $data['mc_reproductive'] == 1) ? true : false,['id'=>'mc_reproductive_yes', 'class'=>'mc_reproductive_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_reproductive']) || (isset($data['mc_reproductive']) && $data['mc_reproductive'] != 1)) { echo 'hidden'; } ?>" id="mc_reproductive_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_reproductive_spec]', (isset($data['mc_reproductive_spec'])) ? $data['mc_reproductive_spec'] : NULL, ['class'=>'form-control','id'=>'mc_reproductive_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Communication and Hearing Problem</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_commhearing]',0,(isset($data['mc_commhearing']) && $data['mc_commhearing'] == 0) ? true : false,['id'=>'mc_commhearing_no', 'class'=>'mc_commhearing_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_commhearing]',1,(isset($data['mc_commhearing']) && $data['mc_commhearing'] == 1) ? true : false,['id'=>'mc_commhearing_yes', 'class'=>'mc_commhearing_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_commhearing']) || (isset($data['mc_commhearing']) && $data['mc_commhearing'] != 1)) { echo 'hidden'; } ?>" id="mc_commhearing_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_commhearing_spec]', (isset($data['mc_commhearing_spec'])) ? $data['mc_commhearing_spec'] : NULL, ['class'=>'form-control','id'=>'mc_commhearing_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Vision Problem</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_vision]',0,(isset($data['mc_vision']) && $data['mc_vision'] == 0) ? true : false,['id'=>'mc_vision_no', 'class'=>'mc_vision_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_vision]',1,(isset($data['mc_vision']) && $data['mc_vision'] == 1) ? true : false,['id'=>'mc_vision_yes', 'class'=>'mc_vision_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_vision']) || (isset($data['mc_vision']) && $data['mc_vision'] != 1)) { echo 'hidden'; } ?>" id="mc_vision_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_vision_spec]', (isset($data['mc_vision_spec'])) ? $data['mc_vision_spec'] : NULL, ['class'=>'form-control','id'=>'mc_vision_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Oral and Dental Health Problem</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_oraldental]',0,(isset($data['mc_oraldental']) && $data['mc_oraldental'] == 0) ? true : false,['id'=>'mc_oraldental_no', 'class'=>'mc_oraldental_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_oraldental]',1,(isset($data['mc_oraldental']) && $data['mc_oraldental'] == 1) ? true : false,['id'=>'mc_oraldental_yes', 'class'=>'mc_oraldental_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_oraldental']) || (isset($data['mc_oraldental']) && $data['mc_oraldental'] != 1)) { echo 'hidden'; } ?>" id="mc_oraldental_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_oraldental_spec]', (isset($data['mc_oraldental_spec'])) ? $data['mc_oraldental_spec'] : NULL, ['class'=>'form-control','id'=>'mc_oraldental_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Psychological and Behavioral Problem</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_psych]',0,(isset($data['mc_psych']) && $data['mc_psych'] == 0) ? true : false,['id'=>'mc_psych_no', 'class'=>'mc_psych_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_psych]',1,(isset($data['mc_psych']) && $data['mc_psych'] == 1) ? true : false,['id'=>'mc_psych_yes', 'class'=>'mc_psych_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_psych']) || (isset($data['mc_psych']) && $data['mc_psych'] != 1)) { echo 'hidden'; } ?>" id="mc_psych_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_psych_spec]', (isset($data['mc_psych_spec'])) ? $data['mc_psych_spec'] : NULL, ['class'=>'form-control','id'=>'mc_psych_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Physical Functioning and Structural Problem</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_physical]',0,(isset($data['mc_physical']) && $data['mc_physical'] == 0) ? true : false,['id'=>'mc_physical_no', 'class'=>'mc_physical_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_physical]',1,(isset($data['mc_physical']) && $data['mc_physical'] == 1) ? true : false,['id'=>'mc_physical_yes', 'class'=>'mc_physical_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_physical']) || (isset($data['mc_physical']) && $data['mc_physical'] != 1)) { echo 'hidden'; } ?>" id="mc_physical_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_physical_spec]', (isset($data['mc_physical_spec'])) ? $data['mc_physical_spec'] : NULL, ['class'=>'form-control','id'=>'mc_physical_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Cognitive Patterns</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_cognitive]',0,(isset($data['mc_cognitive']) && $data['mc_cognitive'] == 0) ? true : false,['id'=>'mc_cognitive_no', 'class'=>'mc_cognitive_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_cognitive]',1,(isset($data['mc_cognitive']) && $data['mc_cognitive'] == 1) ? true : false,['id'=>'mc_cognitive_yes', 'class'=>'mc_cognitive_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_cognitive']) || (isset($data['mc_cognitive']) && $data['mc_cognitive'] != 1)) { echo 'hidden'; } ?>" id="mc_cognitive_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_cognitive_spec]', (isset($data['mc_cognitive_spec'])) ? $data['mc_cognitive_spec'] : NULL, ['class'=>'form-control','id'=>'mc_cognitive_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Bowel Habit</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_bowel]',0,(isset($data['mc_bowel']) && $data['mc_bowel'] == 0) ? true : false,['id'=>'mc_bowel_no', 'class'=>'mc_bowel_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_bowel]',1,(isset($data['mc_bowel']) && $data['mc_bowel'] == 1) ? true : false,['id'=>'mc_bowel_yes', 'class'=>'mc_bowel_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_bowel']) || (isset($data['mc_bowel']) && $data['mc_bowel'] != 1)) { echo 'hidden'; } ?>" id="mc_bowel_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_bowel_spec]', (isset($data['mc_bowel_spec'])) ? $data['mc_bowel_spec'] : NULL, ['class'=>'form-control','id'=>'mc_bowel_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Bladder Habit</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_bladder]',0,(isset($data['mc_bladder']) && $data['mc_bladder'] == 0) ? true : false,['id'=>'mc_bladder_no', 'class'=>'mc_bladder_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[mc_bladder]',1,(isset($data['mc_bladder']) && $data['mc_bladder'] == 1) ? true : false,['id'=>'mc_bladder_yes', 'class'=>'mc_bladder_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['mc_bladder']) || (isset($data['mc_bladder']) && $data['mc_bladder'] != 1)) { echo 'hidden'; } ?>" id="mc_bladder_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[mc_bladder_spec]', (isset($data['mc_bladder_spec'])) ? $data['mc_bladder_spec'] : NULL, ['class'=>'form-control','id'=>'mc_bladder_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden employee">
	 			<div class="col-sm-offset-1 col-sm-10">
	 				<label class="col-sm-4 control-label">Others specify</label>
		 			<div class="col-sm-8">
		 				{!! Form::text('healthassessment[mc_oth_spec]',(isset($data['mc_oth_spec'])) ? $data['mc_oth_spec'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
	 		</div>
	 	</div>
	 	<div class="tab-pane fade" id="medications">
	 		<div class="col-sm-12">
				<div class="page-header">
				  <h4>Medications</h4>
				</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Is the patient under any medications?</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[medications]',0,(isset($data['medications']) && $data['medications'] == 0) ? true : false,['id'=>'medications_no', 'class'=>'medications_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[medications]',1,(isset($data['medications']) && $data['medications'] == 1) ? true : false,['id'=>'medications_yes', 'class'=>'medications_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['medications']) || (isset($data['medications']) && $data['medications'] != 1)) { echo 'hidden'; } ?>" id="medications_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[medications_spec]', (isset($data['medications_spec'])) ? $data['medications_spec'] : NULL, ['class'=>'form-control','id'=>'medications_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 	</div>
	 	<div class="tab-pane fade" id="treatment">
	 		<div class="col-sm-12">
				<div class="page-header">
				  <h4>Treatments/Procedures/Counselling</h4>
				</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Received any recent treatment</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[treatment]',0,(isset($data['treatment']) && $data['treatment'] == 0) ? true : false,['id'=>'treatment_no', 'class'=>'treatment_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[treatment]',1,(isset($data['treatment']) && $data['treatment'] == 1) ? true : false,['id'=>'treatment_yes', 'class'=>'treatment_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['treatment']) || (isset($data['treatment']) && $data['treatment'] != 1)) { echo 'hidden'; } ?>" id="treatment_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[treatment_spec]', (isset($data['treatment_spec'])) ? $data['treatment_spec'] : NULL, ['class'=>'form-control','id'=>'treatment_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Received any recent procedure</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[procedure]',0,(isset($data['procedure']) && $data['procedure'] == 0) ? true : false,['id'=>'procedure_no', 'class'=>'procedure_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[procedure]',1,(isset($data['procedure']) && $data['procedure'] == 1) ? true : false,['id'=>'procedure_yes', 'class'=>'procedure_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['procedure']) || (isset($data['procedure']) && $data['procedure'] != 1)) { echo 'hidden'; } ?>" id="procedure_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[procedure_spec]', (isset($data['procedure_spec'])) ? $data['procedure_spec'] : NULL, ['class'=>'form-control','id'=>'procedure_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Received any recent counselling</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[counselling]',0,(isset($data['counselling']) && $data['counselling'] == 0) ? true : false,['id'=>'counselling_no', 'class'=>'counselling_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[counselling]',1,(isset($data['counselling']) && $data['counselling'] == 1) ? true : false,['id'=>'counselling_yes', 'class'=>'counselling_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['counselling']) || (isset($data['counselling']) && $data['counselling'] != 1)) { echo 'hidden'; } ?>" id="counselling_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[counselling_spec]', (isset($data['counselling_spec'])) ? $data['counselling_spec'] : NULL, ['class'=>'form-control','id'=>'counselling_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
        </div>
        <div class="tab-pane fade" id="behavioral">
        	<div class="col-sm-12">
				<div class="page-header">
				  <h4>Behavioral Health</h4>
				</div>
			</div>
			<div class="hidden student">
				<div class="col-sm-12">
		 			<div class="icheck">
			 			<div class="radio col-sm-offset-1 col-sm-10">
			 				<label class="col-sm-4 control-label">Suspension from school</label>
			 				<div class="col-sm-4">
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[suspensionschool]',0,(isset($data['suspensionschool']) && $data['suspensionschool'] == 0) ? true : false,['id'=>'suspensionschool_no', 'class'=>'suspensionschool_radio']) !!} No 
				 				</label>
				 				<label class="radio-inline">
				 					{!! Form::radio('healthassessment[suspensionschool]',1,(isset($data['suspensionschool']) && $data['suspensionschool'] == 1) ? true : false,['id'=>'suspensionschool_yes', 'class'=>'suspensionschool_radio']) !!} Yes
				 				</label>
				 			</div>
			 			</div>
			 		</div>
		 		</div>
		 		<div class="col-sm-12 <?php if(!isset($data['suspensionschool']) || (isset($data['suspensionschool']) && $data['suspensionschool'] != 1)) { echo 'hidden'; } ?>" id="suspensionschool_reason">
		 			<div class="col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Reason for suspension</label>
		 				<div class="col-sm-4">
		 					{!! Form::text('healthassessment[reasonsuspension]',(isset($data['reasonsuspension'])) ? $data['reasonsuspension'] : NULL,['class'=>'form-control']) !!}
		 				</div>
		 			</div>
		 		</div>
		 	</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Antisocial behavior</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[antisocial]',0,(isset($data['antisocial']) && $data['antisocial'] == 0) ? true : false) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[antisocial]',1,(isset($data['antisocial']) && $data['antisocial'] == 1) ? true : false) !!} Yes
			 				</label>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Delinquency</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[delinquency]',0,(isset($data['delinquency']) && $data['delinquency'] == 0) ? true : false) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[delinquency]',1,(isset($data['delinquency']) && $data['delinquency'] == 1) ? true : false) !!} Yes
			 				</label>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Violence</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[violence]',0,(isset($data['violence']) && $data['violence'] == 0) ? true : false) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[violence]',1,(isset($data['violence']) && $data['violence'] == 1) ? true : false) !!} Yes
			 				</label>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Smoking</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[smoking]',0,(isset($data['smoking']) && $data['smoking'] == 0) ? true : false) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[smoking]',1,(isset($data['smoking']) && $data['smoking'] == 1) ? true : false) !!} Yes
			 				</label>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Alcohol</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[alcohol]',0,(isset($data['alcohol']) && $data['alcohol'] == 0) ? true : false) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[alcohol]',1,(isset($data['alcohol']) && $data['alcohol'] == 1) ? true : false) !!} Yes
			 				</label>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Substance abuse</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[substanceabuse]',0,(isset($data['substanceabuse']) && $data['substanceabuse'] == 0) ? true : false) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[substanceabuse]',1,(isset($data['substanceabuse']) && $data['substanceabuse'] == 1) ? true : false) !!} Yes
			 				</label>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Suicidal thoughts</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[suicidalthoughts]',0,(isset($data['suicidalthoughts']) && $data['suicidalthoughts'] == 0) ? true : false) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[suicidalthoughts]',1,(isset($data['suicidalthoughts']) && $data['suicidalthoughts'] == 1) ? true : false) !!} Yes
			 				</label>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Suicidal attempts</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[suicidalattempts]',0,(isset($data['suicidalattempts']) && $data['suicidalattempts'] == 0) ? true : false) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[suicidalattempts]',1,(isset($data['suicidalattempts']) && $data['suicidalattempts'] == 1) ? true : false) !!} Yes
			 				</label>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
        </div>
        <div class="tab-pane fade" id="foodintake">
        	<div class="col-sm-12">
				<div class="page-header">
				  <h4>Food Intake</h4>
				</div>
	 		</div>
	 		<div class="hidden student">
	 			<div class="col-sm-12">
	 				<h4> Breakfast </h4>
	 			</div>
	 			<div class="col-sm-12">
	 				<label class="control-label col-sm-1"> Place </label>
	 				<div class="col-sm-3">
	 					{!! Form::text('healthassessment[breakfast_place]',(isset($data['breakfast_place'])) ? $data['breakfast_place'] : NULL, ['class'=>'form-control']) !!}
	 				</div>
	 				<label class="control-label col-sm-1"> Time </label>
	 				<div class="col-sm-3">
	 					{!! Form::text('healthassessment[breakfast_time]',(isset($data['breakfast_time'])) ? $data['breakfast_time'] : NULL, ['class'=>'form-control']) !!}
	 				</div>
	 				<label class="control-label col-sm-1"> Type of food </label>
	 				<div class="col-sm-3">
	 					{!! Form::text('healthassessment[breakfast_type_food]',(isset($data['breakfast_type_food'])) ? $data['breakfast_type_food'] : NULL, ['class'=>'form-control']) !!}
	 				</div>
	 			</div>
	 			<div class="col-sm-12">
	 				<h4> Lunch </h4>
	 			</div>
	 			<div class="col-sm-12">
	 				<label class="control-label col-sm-1"> Place </label>
	 				<div class="col-sm-3">
	 					{!! Form::text('healthassessment[lunch_place]',(isset($data['lunch_place'])) ? $data['lunch_place'] : NULL, ['class'=>'form-control']) !!}
	 				</div>
	 				<label class="control-label col-sm-1"> Time </label>
	 				<div class="col-sm-3">
	 					{!! Form::text('healthassessment[lunch_time]',(isset($data['lunch_time'])) ? $data['lunch_time'] : NULL, ['class'=>'form-control']) !!}
	 				</div>
	 				<label class="control-label col-sm-1"> Type of food </label>
	 				<div class="col-sm-3">
	 					{!! Form::text('healthassessment[lunch_type_food]',(isset($data['lunch_type_food'])) ? $data['lunch_type_food'] : NULL, ['class'=>'form-control']) !!}
	 				</div>
	 			</div>
	 			<div class="col-sm-12">
	 				<h4> Snacks </h4>
	 			</div>
	 			<div class="col-sm-12">
	 				<label class="control-label col-sm-1"> Place </label>
	 				<div class="col-sm-3">
	 					{!! Form::text('healthassessment[snacks_place]',(isset($data['snacks_place'])) ? $data['snacks_place'] : NULL, ['class'=>'form-control']) !!}
	 				</div>
	 				<label class="control-label col-sm-1"> Time </label>
	 				<div class="col-sm-3">
	 					{!! Form::text('healthassessment[snacks_time]',(isset($data['snacks_time'])) ? $data['snacks_time'] : NULL, ['class'=>'form-control']) !!}
	 				</div>
	 				<label class="control-label col-sm-1"> Type of food </label>
	 				<div class="col-sm-3">
	 					{!! Form::text('healthassessment[snacks_type_food]',(isset($data['snacks_type_food'])) ? $data['snacks_type_food'] : NULL, ['class'=>'form-control']) !!}
	 				</div>
	 			</div>
	 			<div class="col-sm-12">
	 				<h4> Dinner </h4>
	 			</div>
	 			<div class="col-sm-12">
	 				<label class="control-label col-sm-1"> Place </label>
	 				<div class="col-sm-3">
	 					{!! Form::text('healthassessment[dinner_place]',(isset($data['dinner_place'])) ? $data['dinner_place'] : NULL, ['class'=>'form-control']) !!}
	 				</div>
	 				<label class="control-label col-sm-1"> Time </label>
	 				<div class="col-sm-3">
	 					{!! Form::text('healthassessment[dinner_time]',(isset($data['dinner_time'])) ? $data['dinner_time'] : NULL, ['class'=>'form-control']) !!}
	 				</div>
	 				<label class="control-label col-sm-1"> Type of food </label>
	 				<div class="col-sm-3">
	 					{!! Form::text('healthassessment[dinner_type_food]',(isset($data['dinner_type_food'])) ? $data['dinner_type_food'] : NULL, ['class'=>'form-control']) !!}
	 				</div>
	 			</div>
	 		</div>
        </div>
        <div class="tab-pane fade" id="malnutrition">
        	<div class="col-sm-12">
				<div class="page-header">
				  <h4>Malnutrition</h4>
				</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Malnutrition</label>
		 				<div class="col-sm-4">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[malnutrition]',0,(isset($data['malnutrition']) && $data['malnutrition'] == 0) ? true : false,['id'=>'malnutrition_no', 'class'=>'malnutrition_radio']) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[malnutrition]',1,(isset($data['malnutrition']) && $data['malnutrition'] == 1) ? true : false,['id'=>'malnutrition_yes', 'class'=>'malnutrition_radio']) !!} Yes
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['malnutrition']) || (isset($data['malnutrition']) && $data['malnutrition'] != 1)) { echo 'hidden'; } ?>" id="malnutrition_spec">
				 			<label class="col-sm-1 control-label">Specify: </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[malnutrition_spec]', (isset($data['malnutrition_spec'])) ? $data['malnutrition_spec'] : NULL, ['class'=>'form-control','id'=>'malnutrition_spec_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
        </div>
        <div class="tab-pane fade" id="activities">
        	<div class="col-sm-12">
				<div class="page-header">
				  <h4>Activities</h4>
				</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="col-sm-offset-1 col-sm-10">
		 			<label class="col-sm-4 control-label"> Idle Hours (Television, video games, etc.)</label>
		 			<div class="col-sm-6">
		 				{!! Form::text('healthassessment[idle_hours]',(isset($data['idle_hours'])) ? $data['idle_hours'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="col-sm-offset-1 col-sm-10">
		 			<label class="col-sm-4 control-label"> Active Hours (Indoor, outdoor games, etc.)</label>
		 			<div class="col-sm-6">
		 				{!! Form::text('healthassessment[active_hours]',(isset($data['active_hours'])) ? $data['active_hours'] : NULL, ['class'=>'form-control']) !!}
		 			</div>
	 			</div>
	 		</div>
        </div>
        <div class="tab-pane fade" id="menstrualhealth">
        	<div class="col-sm-12">
				<div class="page-header">
				  <h4>Menstrual Health</h4>
				</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<label class="col-sm-4 control-label">Specify age of menarche </label>
	 			<div class="col-sm-6">
	 				{!! Form::number('healthassessment[age_menarche]', (isset($data['age_menarche'])) ? $data['age_menarche'] : NULL, ['class'=>'form-control','id'=>'age_menarche_input']) !!}
	 			</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<label class="col-sm-4 control-label"> Type of material used during menstruation</label>
	 			<div class="col-sm-6">
	 				{!! Form::text('healthassessment[material_menstruation]',(isset($data['material_menstruation'])) ? $data['material_menstruation'] : NULL, ['class'=>'form-control']) !!}
	 			</div>
	 		</div>
        </div>
        <div class="tab-pane fade" id="family">
        	<div class="col-sm-12">
				<div class="page-header">
				  <h4>Family Involvement</h4>
				</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Family interaction/engagement</label>
		 				<div class="col-sm-3">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[family_involvement]',1,(isset($data['family_involvement']) && $data['family_involvement'] == 1) ? true : false,['id'=>'family_involvement_no', 'class'=>'family_involvement_radio']) !!} Yes
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[family_involvement]',0,(isset($data['family_involvement']) && $data['family_involvement'] == 0) ? true : false,['id'=>'family_involvement_yes', 'class'=>'family_involvement_radio']) !!} No
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['family_involvement']) || (isset($data['family_involvement']) && $data['family_involvement'] != 0)) { echo 'hidden'; } ?>" id="family_involvement_reason">
				 			<label class="col-sm-2 control-label">Reason </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[family_involvement_reason]', (isset($data['family_involvement_reason'])) ? $data['family_involvement_reason'] : NULL, ['class'=>'form-control','id'=>'family_involvement_reason_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
	 		<div class="col-sm-12 hidden student">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Community interaction</label>
		 				<div class="col-sm-3">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[community_involvement]',1,(isset($data['community_involvement']) && $data['community_involvement'] == 1) ? true : false,['id'=>'community_involvement_no', 'class'=>'community_involvement_radio']) !!} Yes
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[community_involvement]',0,(isset($data['community_involvement']) && $data['community_involvement'] == 0) ? true : false,['id'=>'community_involvement_yes', 'class'=>'community_involvement_radio']) !!} No
			 				</label>
			 			</div>
			 			<div class="<?php if(!isset($data['community_involvement']) || (isset($data['community_involvement']) && $data['community_involvement'] != 0)) { echo 'hidden'; } ?>" id="community_involvement_reason">
				 			<label class="col-sm-2 control-label">Reason </label>
				 			<div class="col-sm-3">
				 				{!! Form::text('healthassessment[community_involvement_reason]', (isset($data['community_involvement_reason'])) ? $data['community_involvement_reason'] : NULL, ['class'=>'form-control','id'=>'community_involvement_reason_input']) !!}
				 			</div>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
        </div>
        <div class="tab-pane fade" id="drrknowledge">
        	<div class="col-sm-12">
				<div class="page-header">
				  <h4>DRR Knowledge</h4>
				</div>
	 		</div>
	 		<div class="col-sm-12 hidden student employee">
	 			<div class="icheck">
		 			<div class="radio col-sm-offset-1 col-sm-10">
		 				<label class="col-sm-4 control-label">Does the student/teacher have DRR knowledge?</label>
		 				<div class="col-sm-3">
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[drr_knowledge]',0,(isset($data['drr_knowledge']) && $data['drr_knowledge'] == 0) ? true : false) !!} No 
			 				</label>
			 				<label class="radio-inline">
			 					{!! Form::radio('healthassessment[drr_knowledge]',1,(isset($data['drr_knowledge']) && $data['drr_knowledge'] == 1) ? true : false) !!} Yes
			 				</label>
			 			</div>
		 			</div>
		 		</div>
	 		</div>
        </div>
    </div>

</fieldset>

@section('plugin_jsscripts')
	<script type="text/javascript">
		$(function() {
			if($('#student-radio').is(':checked') == true) {
				$('.student').removeClass('hidden');
			}
			if($('#employee-radio').is(':checked') == true) {
				$('.employee').removeClass('hidden');
			}
			if($('#na-radio').is(':checked') == true) {
				$('.student').removeClass('hidden');
				$('.employee').removeClass('hidden');
			}

			$('.mc_skindisease_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_skindisease_spec').removeClass('hidden');
				}
				else{
					$('#mc_skindisease_spec').addClass('hidden');
				};
			});
			$('.mc_allergies_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_allergies_spec').removeClass('hidden');
				}
				else{
					$('#mc_allergies_spec').addClass('hidden');
				};
			});
			$('.mc_injuretrauma_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_injuretrauma_spec').removeClass('hidden');
				}
				else{
					$('#mc_injuretrauma_spec').addClass('hidden');
				};
			});
			$('.mc_injuretrauma_head_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_injuretrauma_head_spec').removeClass('hidden');
				}
				else{
					$('#mc_injuretrauma_head_spec').addClass('hidden');
				};
			});
			$('.mc_injuretrauma_spinal_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_injuretrauma_spinal_spec').removeClass('hidden');
				}
				else{
					$('#mc_injuretrauma_spinal_spec').addClass('hidden');
				};
			});
			$('.mc_injuretrauma_torso_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_injuretrauma_torso_spec').removeClass('hidden');
				}
				else{
					$('#mc_injuretrauma_torso_spec').addClass('hidden');
				};
			});
			$('.mc_injuretrauma_leftarm_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_injuretrauma_leftarm_spec').removeClass('hidden');
				}
				else{
					$('#mc_injuretrauma_leftarm_spec').addClass('hidden');
				};
			});
			$('.mc_injuretrauma_rightarm_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_injuretrauma_rightarm_spec').removeClass('hidden');
				}
				else{
					$('#mc_injuretrauma_rightarm_spec').addClass('hidden');
				};
			});
			$('.mc_injuretrauma_leftleg_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_injuretrauma_leftleg_spec').removeClass('hidden');
				}
				else{
					$('#mc_injuretrauma_leftleg_spec').addClass('hidden');
				};
			});
			$('.mc_injuretrauma_rightleg_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_injuretrauma_rightleg_spec').removeClass('hidden');
				}
				else{
					$('#mc_injuretrauma_rightleg_spec').addClass('hidden');
				};
			});
			$('.mc_respiratory_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_respiratory_spec').removeClass('hidden');
				}
				else{
					$('#mc_respiratory_spec').addClass('hidden');
				};
			});
			$('.mc_cardio_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_cardio_spec').removeClass('hidden');
				}
				else{
					$('#mc_cardio_spec').addClass('hidden');
				};
			});
			$('.mc_urinary_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_urinary_spec').removeClass('hidden');
				}
				else{
					$('#mc_urinary_spec').addClass('hidden');
				};
			});
			$('.mc_reproductive_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_reproductive_spec').removeClass('hidden');
				}
				else{
					$('#mc_reproductive_spec').addClass('hidden');
				};
			});
			$('.mc_commhearing_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_commhearing_spec').removeClass('hidden');
				}
				else{
					$('#mc_commhearing_spec').addClass('hidden');
				};
			});
			$('.mc_endocrine_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_endocrine_spec').removeClass('hidden');
				}
				else{
					$('#mc_endocrine_spec').addClass('hidden');
				};
			});
			$('.mc_vision_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_vision_spec').removeClass('hidden');
				}
				else{
					$('#mc_vision_spec').addClass('hidden');
				};
			});
			$('.mc_oraldental_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_oraldental_spec').removeClass('hidden');
				}
				else{
					$('#mc_oraldental_spec').addClass('hidden');
				};
			});
			$('.mc_psych_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_psych_spec').removeClass('hidden');
				}
				else{
					$('#mc_psych_spec').addClass('hidden');
				};
			});
			$('.mc_physical_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_physical_spec').removeClass('hidden');
				}
				else{
					$('#mc_physical_spec').addClass('hidden');
				};
			});
			$('.mc_cognitive_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_cognitive_spec').removeClass('hidden');
				}
				else{
					$('#mc_cognitive_spec').addClass('hidden');
				};
			});
			$('.mc_bowel_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_bowel_spec').removeClass('hidden');
				}
				else{
					$('#mc_bowel_spec').addClass('hidden');
				};
			});
			$('.mc_bladder_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#mc_bladder_spec').removeClass('hidden');
				}
				else{
					$('#mc_bladder_spec').addClass('hidden');
				};
			});
			$('.medications_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#medications_spec').removeClass('hidden');
				}
				else{
					$('#medications_spec').addClass('hidden');
				};
			});
			$('.treatment_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#treatment_spec').removeClass('hidden');
				}
				else{
					$('#treatment_spec').addClass('hidden');
				};
			});
			$('.procedure_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#procedure_spec').removeClass('hidden');
				}
				else{
					$('#procedure_spec').addClass('hidden');
				};
			});
			$('.counselling_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#counselling_spec').removeClass('hidden');
				}
				else{
					$('#counselling_spec').addClass('hidden');
				};
			});
			$('.malnutrition_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#malnutrition_spec').removeClass('hidden');
				}
				else{
					$('#malnutrition_spec').addClass('hidden');
				};
			});
			$('.menstruation_started_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#age_menarche').removeClass('hidden');
				}
				else{
					$('#age_menarche').addClass('hidden');
				};
			});
			$('.family_involvement_radio').on('ifClicked', function(event){ 
				if(this.value == 0){
					$('#family_involvement_reason').removeClass('hidden');
				}
				else{
					$('#family_involvement_reason').addClass('hidden');
				};
			});
			$('.community_involvement_radio').on('ifClicked', function(event){ 
				if(this.value == 0){
					$('#community_involvement_reason').removeClass('hidden');
				}
				else{
					$('#community_involvement_reason').addClass('hidden');
				};
			});
			$('.suspensionschool_radio').on('ifClicked', function(event){ 
				if(this.value == 1){
					$('#suspensionschool_reason').removeClass('hidden');
				}
				else{
					$('#suspensionschool_reason').addClass('hidden');
				};
			});

			$('#student-button').on('click', function(event){ 
				$('.employee').addClass('hidden');
				$('.na').addClass('hidden');
				$('.student').removeClass('hidden');
			});

			$('#employee-button').on('click', function(event){ 
				$('.student').addClass('hidden');
				$('.na').addClass('hidden');
				$('.employee').removeClass('hidden');
			});

			$('#na-button').on('click', function(event){ 
				$('.student').removeClass('hidden');
				$('.employee').removeClass('hidden');
			});
		});
	</script>
@stop