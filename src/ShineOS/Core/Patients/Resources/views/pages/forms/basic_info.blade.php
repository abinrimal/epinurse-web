@section('heads')
{!! HTML::style('public/dist/js/prettify.css') !!}
{!! HTML::style('plugins/datepicker/datepicker3.css') !!}
{!! HTML::style('plugins/timepicker/bootstrap-timepicker.min.css') !!}
@stop

<style>
.maiden { display: none;}
</style>

<div role="tabpanel" class="tab-pane active" id="basic">
    <fieldset {{ $hidden }}>
        <legend>Basic Record Information</legend>
        <!-- <div class="text-group">
            <p>Complete the following fields then click Next. If the information you entered here already existing, you will be notified.</p>
        </div> -->

        <fieldset>
            <div class="form-group col-md-6">
                <div class="row">
                <label class="col-sm-4 control-label">First name* </label>
                <div class="col-sm-8">
                    {!! Form::text('first_name', null, array('class' => 'form-control', 'name'=>'inputPatientFirstName')) !!}
                </div>
                </div>
            </div>
            <div class="form-group col-md-6">
                <div class="row">
                <label class="col-sm-4 control-label">Middle name* </label>
                <div class="col-sm-8">
                    {!! Form::text('middle_name', null, array('class' => 'form-control', 'name'=>'inputPatientMiddleName')) !!}
                </div>
                </div>
            </div>
            <div class="form-group col-md-6">
                <div class="row">
                <label class="col-sm-4 control-label">Last name* </label>
                <div class="col-sm-8">
                    {!! Form::text('last_name', null, array('class' => 'form-control', 'name'=>'inputPatientLastName')) !!}
                </div>
                </div>
            </div>
            <div class="form-group col-md-6">
                <div class="row">
                <label class="col-sm-4 control-label">Suffix</label>
                <div class="col-sm-8">
                    <div class="btn-group toggler" data-toggle="buttons">
                      <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'SR') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="SR"> Sr.
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'JR') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="JR"> Jr.
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'II') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="II"> II
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'III') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="III"> III
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'IV') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="IV"> IV
                      </label>
                      <label class="btn btn-default @if(isset($patient) AND $patient->name_suffix == 'V') active @endif">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="V"> V
                      </label>
                      <label class="btn btn-default">
                        <i class="fa fa-check"></i> <input type="radio" name="inputPatientSuffix" id="" autocomplete="off" value="NA"> None
                      </label>
                    </div>
                </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="row">
                    <label class="col-sm-4 control-label">Gender*</label>
                    <div class="col-sm-8">
                        <div class="btn-group toggler" data-toggle="buttons">
                          <?php
                            $genderm = false; $genderf = false; $genderu = false;
                            if(isset($patient) AND $patient->gender == "M") $genderm = true;
                            if(isset($patient) AND $patient->gender == "F") $genderf = true;
                            if(isset($patient) AND $patient->gender == "U") $genderu = true;
                            if(isset($patient) AND $patient->gender == "") $genderu = true;
                          ?>
                          <label class="btn btn-default required @if(isset($patient) AND $patient->gender=='M') active @endif">
                            <i class="fa fa-check"></i> {!! Form::radio('inputPatientGender', 'M', $genderm, array('class' => 'form-control gender', 'required'=>'required')) !!} Male
                          </label>
                          <label class="btn btn-default required @if(isset($patient) AND $patient->gender=='F') active @endif">
                            <i class="fa fa-check"></i> {!! Form::radio('inputPatientGender', 'F', $genderf, array('class' => 'form-control gender', 'required'=>'required')) !!} Female
                          </label>
                          @if($genderu)
                          <label class="btn btn-default required @if(isset($patient) AND ($patient->gender=='U' OR $patient->gender=='')) active  @endif">
                            <i class="fa fa-check"></i> {!! Form::radio('inputPatientGender', 'U', $genderu, array('class' => 'form-control gender', 'required'=>'required')) !!} Unknown
                          </label>
                          @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-6 hidden">
                <div class="row">
                    <label class="col-sm-4 control-label" for="marital_status">Marital Status*</label>
                    <div class="col-sm-8">
                    <?php
                        $civil_status_value = NULL;
                        if(isset($patient)) { $civil_status_value = $patient->civil_status; }
                    ?>

                        {!! Form::select('inputPatientStatus', $civil_status, $civil_status_value, ['class' => 'form-control', 'onchange'=>'show_maiden()', 'id'=>'marital_status_field']) !!}
                    </div>
                </div>
            </div>
            <?php
            $hide = 'hidden';
            if(isset($patient) AND isset($patient) AND $genderf == true AND $patient->civil_status == 'M') {
                $hide = '';
            } ?>
            <div class="form-group col-md-6 {{ $hide }}">
                <div class="row">
                    <div class="maiden">
                        <label class="col-sm-4 control-label">Maiden Last Name </label>
                        <div class="col-sm-8">
                            {!! Form::text('maiden_lastname', null, array('class' => 'form-control', 'name'=>'inputMaidenLastName')) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-6 {{ $hide }}">
                <div class="row">
                    <div class="maiden">
                        <label class="col-sm-4 control-label">Maiden Middle Name </label>
                        <div class="col-sm-8">
                            {!! Form::text('maiden_middlename', null, array('class' => 'form-control', 'name'=>'inputMaidenMiddleName')) !!}
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend></legend>
            <div class="form-group">
                <label class="col-sm-2 control-label">Birth Date (BS)*</label>
                <div class="col-sm-4 iconed-input">
                    <i class="fa fa-calendar inner-icon"></i>
                    {!! Form::text('birthdate_bs', date("m/d/Y", strtotime($patient->birthdate_bs)), array('id' => 'birthdate_bs', 'class' => 'form-control birthdate_bs', 'placeholder'=>'MM/DD/YYYY')) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Birth Date (AD)*</label>
                <div class="col-sm-4 iconed-input">
                    <i class="fa fa-calendar inner-icon"></i>
                    {!! Form::text('birthdate', date("m/d/Y", strtotime($patient->birthdate)), array('id' => 'birthdate_ad', 'class' => 'form-control datepicker_null', 'name'=>'inputPatientBirthDate')) !!}
                </div>
                <label class="col-sm-2 control-label">Age</label>
                <div class="col-sm-4">
                    {!! Form::text('age', getAge($patient->birthdate), array('class' => 'form-control', 'readonly'=>'readonly')) !!}
                </div>
                <label class="col-sm-2 control-label">Time of Birth</label>
                <div class="col-sm-4 bootstrap-timepicker timepicker iconed-input">
                    <i class="fa fa-clock-o inner-icon"></i>
                    {!! Form::text('birthtime', date("h:i A", strtotime($patient->birthtime)), array('id' => '', 'class' => 'form-control input-small timepicker', 'name'=>'inputPatientBirthTime')) !!}
                </div>
                <label class="col-sm-2 control-label">Place of Birth</label>
                <div class="col-sm-4">
                    {!! Form::text('birthplace', null, array('class' => 'form-control', 'name'=>'inputPatientBirthPlace')) !!}
                </div>

            </div>
        </fieldset>
        <fieldset>
            <legend></legend>
            <div class="form-group">
                <div class="col-md-6 hidden">
                    <label class="col-sm-4 control-label">Religion</label>
                    <div class="col-sm-8">
                        <select class="populate placeholder form-control" name="inputPatientReligion" id="inputPatientReligion" onchange="if(this.value == 'OTHER') { $('#religionother').removeClass('hidden'); } else { $('#religionother').addClass('hidden'); }">
                            <option value="">- Select religion -</option>
                            @foreach ($religion as $key=>$val)
                                <option value='{{ $key }}' @if(isset($patient) AND $patient->religion == $key) selected='selected' @endif >{{ $val }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div id="religionother" class="hidden">
                        <label class="col-sm-4 control-label">Specify Religion</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="inputPatientOtherReligion" placeholder="Other Religion" value="{{{ isset($patient->religion_others) ? $patient->religion_others : '' }}}"  />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="col-sm-4 control-label" for="education">Highest Education</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="inputPatientEducation" id="inputPatientEducation" onchange="if(this.value == '99') { $('#educother').removeClass('hidden'); } else { $('#educother').addClass('hidden'); }">
                            <option value="" @if(!isset($patient)) selected='selected' @endif>- Select education -</option>
                            <option value='01' @if(isset($patient) AND $patient->highest_education == '01') selected='selected' @endif >Elementary Education</option>
                            <option value='02' @if(isset($patient) AND $patient->highest_education == '02') selected='selected' @endif >High School Education</option>
                            <option value='03' @if(isset($patient) AND $patient->highest_education == '03') selected='selected' @endif >College</option>
                            <option value='04' @if(isset($patient) AND $patient->highest_education == '04') selected='selected' @endif >Postgraduate Program</option>
                            <option value='05' @if(isset($patient) AND $patient->highest_education == '05') selected='selected' @endif >No Formal Education/No Schooling</option>
                            <option value='06' @if(isset($patient) AND $patient->highest_education == '06') selected='selected' @endif >Not Applicable</option>
                            <option value='07' @if(isset($patient) AND $patient->highest_education == '07') selected='selected' @endif >Vocational</option>
                            <option value='99' @if(isset($patient) AND $patient->highest_education == '99') selected='selected' @endif >Others</option>
                        </select>
                    </div>
                    <div id="educother" class="hidden">
                        <label class="col-sm-4 control-label">Specify Education</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="inputPatientEducationOther" placeholder="Other Education" value="{{{ isset($patient->highesteducation_others) ? $patient->highesteducation_others : '' }}}"  />
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nationality</label>
                <div class="col-sm-4">
                    <?php $patnation = $patient->nationality ? $patient->nationality : 'PHL' ?>
                    {!! Form::select('inputPatientNationality', $nationality, $patnation, ['class' => 'form-control alpha']) !!}
                </div>
            </div>

        </fieldset>
    </fieldset>

</div><!-- /.tab-pane -->
