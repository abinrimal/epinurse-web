<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityNursingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('communitynursing_service')!=TRUE) { 
            Schema::create('communitynursing_service', function (Blueprint $table) {
                $table->increments('id');
                $table->string('communitynursing_id', 32);
                $table->string('healthcareservice_id', 32);

                $table->string('uuid', 100)->nullable();
                $table->string('owner_uuid', 100)->nullable();

                $table->integer('disaster_recently')->nullable();
                $table->string('disaster_type', 100)->nullable();
                $table->string('disaster_duration', 100)->nullable();

                $table->integer('number_human_loss')->nullable();
                $table->integer('number_death')->nullable();
                $table->integer('number_missing_people')->nullable();
                $table->integer('number_injured_people')->nullable();
                $table->integer('number_livestock_loss')->nullable();

                $table->integer('handwash')->nullable();
                $table->string('handwash_oth',100)->nullable();

                $table->integer('when_wash_hands_after_toileting')->nullable();
                $table->integer('when_wash_hands_before_cooking')->nullable();
                $table->integer('when_wash_hands_before_eating')->nullable();
                $table->integer('when_wash_hands_before_feeding_a_child')->nullable();
                $table->integer('when_wash_hands_others')->nullable();
                $table->integer('when_wash_hands_specify')->nullable();
                
                $table->integer('bathing_habit')->nullable();
                $table->integer('src_water_supply')->nullable();
                $table->string('src_water_supply_spec',100)->nullable();
                $table->integer('ade_water_supply')->nullable();
                $table->integer('quality_water')->nullable();

                $table->string('waterpurif_boiling',100)->nullable();
                $table->string('waterpurif_filtration',100)->nullable();
                $table->string('waterpurif_chemical',100)->nullable();
                $table->string('waterpurif_sodis',100)->nullable();
                $table->string('waterpurif_none',100)->nullable();
                $table->string('waterpurif_oth',100)->nullable();
                $table->string('waterpurif_oth_spec',100)->nullable();

                $table->integer('distance_water_supply')->nullable();
                $table->integer('toilet_available')->nullable();
                $table->integer('num_toilet')->nullable();
                $table->integer('type_toilet')->nullable();
                $table->string('type_toilet_specify',100)->nullable();
                $table->integer('toilet_distance')->nullable();

                $table->double('toilet_fr_water')->nullable();
                $table->integer('toilet_fr_water_unit')->nullable();
                $table->double('toilet_fr_waste')->nullable();
                $table->integer('toilet_fr_waste_unit')->nullable();

                $table->integer('waste_disposal')->nullable();
                $table->string('waste_disposal_spec',100)->nullable();

                $table->integer('disposal_method_composting')->nullable();
                $table->integer('disposal_method_burning')->nullable();
                $table->integer('disposal_method_dumping')->nullable();
                $table->integer('disposal_method_others')->nullable();
                $table->string('disposal_method_specify',100)->nullable();

                $table->integer('waste_collectors')->nullable();
                $table->integer('timely_collection_waste')->nullable();
                $table->integer('dead_animal_disposal')->nullable();
                $table->integer('drainage_facility')->nullable();
                $table->integer('surr_area_clean')->nullable();

                $table->integer('prob_hygiene_mens')->nullable();
                $table->integer('prob_hygiene_reas_watersupply')->nullable();
                $table->integer('prob_hygiene_reas_privacy')->nullable();
                $table->integer('prob_hygiene_reas_pads')->nullable();
                $table->integer('prob_hygiene_reas_padsdisposal')->nullable();
                $table->integer('prob_hygiene_reas_oth')->nullable();
                $table->string('prob_hygiene_reas_oth_spec',100)->nullable();

                $table->integer('visible_hazards_floor')->nullable();
                $table->integer('visible_hazards_light')->nullable();
                $table->integer('visible_hazards_electricity')->nullable();
                $table->integer('visible_hazards_surface')->nullable();
                $table->integer('visible_hazards_land')->nullable();
                $table->integer('visible_hazards_dumparea')->nullable();
                $table->integer('visible_hazards_pollution')->nullable();
                $table->integer('visible_hazards_ventilation')->nullable();
                $table->integer('visible_hazards_noise')->nullable();
                $table->integer('visible_hazards_none')->nullable();
                $table->string('visible_hazards_oth_spec',100)->nullable();

                $table->integer('area_prone_to_none')->nullable();
                $table->integer('bite_prone_snake')->nullable();
                $table->integer('bite_prone_animal')->nullable();
                $table->string('bite_prone_animal_spec',100)->nullable();
                $table->integer('bite_prone_insect')->nullable();
                $table->string('bite_prone_insect_spec',100)->nullable();
                $table->integer('bite_prone_oth')->nullable();
                $table->string('bite_prone_oth_spec',100)->nullable();

                $table->integer('free_vectors')->nullable();

                $table->integer('mosqprev_measure_net')->nullable();
                $table->integer('mosqprev_measure_liquid')->nullable();
                $table->integer('mosqprev_measure_drug')->nullable();
                $table->integer('mosqprev_measure_oth')->nullable();
                $table->integer('mosqprev_measure_oth_spec')->nullable();

                $table->integer('periodic_pest_control')->nullable();

                $table->integer('housing')->nullable();
                $table->string('housing_oth',100)->nullable();
                $table->integer('space_adequacy')->nullable();
                $table->integer('house_damage')->nullable();

                $table->integer('source_of_light_electricity')->nullable();
                $table->integer('source_of_light_oil_lamp')->nullable();
                $table->integer('source_of_light_solar')->nullable();
                $table->integer('source_of_light_others')->nullable();
                $table->string('source_of_light_specify',100)->nullable();

                $table->string('ventilation_adequacy',100)->nullable();
                $table->integer('kitchen')->nullable();
                $table->integer('foodstock_avail')->nullable();

                $table->integer('cooking_fuel_used_gas')->nullable();
                $table->integer('cooking_fuel_used_firewood')->nullable();
                $table->integer('cooking_fuel_used_kerosene')->nullable();
                $table->integer('cooking_fuel_used_others')->nullable();
                $table->string('cooking_fuel_used_specify',100)->nullable();
                $table->integer('kind_of_food_available_cooked')->nullable();
                $table->integer('kind_of_food_available_junk')->nullable();
                $table->integer('kind_of_food_available_others')->nullable();
                $table->string('kind_of_food_available_specify',100)->nullable();

                $table->integer('food_supply_type')->nullable();
                $table->string('food_supply_type_spec',100)->nullable();
                $table->integer('safe_food')->nullable();
                $table->integer('food_storage')->nullable();
                $table->integer('disease_outbreak')->nullable();
                $table->string('disease_outbreak_spec',100)->nullable();
                $table->integer('hs_onsite')->nullable();
                $table->integer('pfa')->nullable();
                $table->integer('distance_faci')->nullable();

                $table->integer('type_of_nearest_facility_health_post')->nullable();
                $table->integer('type_of_nearest_facility_primary_health_center')->nullable();
                $table->integer('type_of_nearest_facility_district_hospital')->nullable();
                $table->integer('type_of_nearest_facility_zonal_hospital')->nullable();
                $table->integer('type_of_nearest_facility_sub_regional_hospital')->nullable();
                $table->integer('type_of_nearest_facility_tertiary_hospital')->nullable();
                $table->integer('type_of_nearest_facility_private_hospital')->nullable();
                $table->integer('type_of_nearest_facility_others')->nullable();
                $table->stirng('type_of_nearest_facility_specify',100)->nullable();

                $table->integer('family_illness_none')->nullable();
                $table->integer('family_illness_diabetes')->nullable();
                $table->integer('family_illness_hypertension')->nullable();
                $table->integer('family_illness_copd')->nullable();
                $table->integer('family_illness_cancer')->nullable();
                $table->integer('family_illness_mental')->nullable();
                $table->integer('family_illness_oth')->nullable();
                $table->string('family_illness_oth_spec',100)->nullable();

                $table->integer('fp_use')->nullable();
                $table->integer('fp_method_used')->nullable();
                $table->integer('fp_temp_awareness')->nullable();
                $table->integer('fp_awareness_security')->nullable();
                $table->integer('fp_awareness_ecp')->nullable();

                $table->integer('where_ecp_available_mobile_clinic')->nullable();
                $table->integer('where_ecp_available_health_facility')->nullable();
                $table->integer('where_ecp_available_others')->nullable();
                $table->string('where_ecp_available_specify',100)->nullable();

                $table->integer('family_mental_illness')->nullable();
                $table->integer('medication')->nullable();

                $table->integer('health_hazards_none')->nullable();
                $table->integer('mental_hazard_smoking')->nullable();
                $table->integer('mental_hazard_alcoholism')->nullable();
                $table->integer('mental_hazard_drug')->nullable();
                $table->integer('mental_hazard_oth')->nullable();
                $table->string('mental_hazard_oth_spec',100)->nullable();

                $table->integer('mental_abuse_none')->nullable();
                $table->integer('mental_abuse_sexual')->nullable();
                $table->integer('mental_abuse_physical')->nullable();
                $table->integer('mental_abuse_psych')->nullable();
                $table->integer('mental_abuse_oth')->nullable();
                $table->string('mental_abuse_oth_spec',100)->nullable();
                              
                $table->softDeletes();
                $table->timestamps();
                $table->unique('healthcareservice_id');
            });
        }          //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
