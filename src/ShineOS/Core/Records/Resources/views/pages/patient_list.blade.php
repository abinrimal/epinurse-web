<div class="box no-border">
    @if(Session::has('message'))
      <p class="alert {{ Session::get('alert-class', 'alert-warning alert-dismissible') }}">{{ Session::get('message') }}</p>
    @endif

    <div class="box-body table-responsive no-padding overflowx-hidden">
        <table class="table table-hover" id="dataTable_patients">
            <thead>
            <tr>
                <th class="nosort">&nbsp;</th>
                <th>Patient Name</th>
                <th>Gender</th>
                <th>Age</th>
                <th>Birthdate</th>
                <th class="nosort forhide">Municipality</th>
                <th class="nosort forhide">District</th>
                <th class="nosort">&nbsp;</th>
            </tr>
            </thead>

        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->

@section('scripts')

    <div class="modal fade" id="deathInfoModal" tabindex="-1" role="dialog" aria-labelledby="deathInfoModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myInfoModalLabel"> Healthcare Record Preview </h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
    
        var T1 = $('table#dataTable_patients').DataTable({
            "Paginate": true,
            "LengthChange": true,
            "Filter": true,
            "Sort": true,
            "Info": true,
            "AutoWidth": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{{ url('records/getpatients') }}",
                "type": "POST",
                "headers": {'X-CSRF-Token': $('meta[name="_token"]').attr('content')}
            },
            "fnDrawCallback": function( oSettings ) {
                //insert all functions for Patient datatable
                //from modules and plugins
                {{ dataTablePatJsFuncs() }}
            },
            "columns": [
                { "data": "pid" },
                { "data": "name" },
                { "data": "gender" },
                { "data": "age" },
                { "data": "birthdate" },
                { "data": "municipality" },
                { "data": "district" },
                { "data": "action" }
            ],
            "columnDefs": [
                { "orderable": false, "targets": 'nosort' }
            ],
            "order": [[ 0, "desc" ]],
            dom: "<'row'<'col-sm-5 col-xs-5 hidden-xs'l><'col-sm-5 col-xs-12'f><'col-sm-2 col-xs-2 hidden-xs'B>>" +
                "<'row'<'col-sm-12 col-xs-12'tr>>" +
                "<'row'<'col-sm-5 col-xs-12'i><'col-sm-7 col-xs-12'p>>",
            buttons: [
                {
                    text: '<i class="fa fa-search"></i><span> | Advanced Search</span>',
                    action: function ( e, dt, node, config ) {
                        location="{{ url('records/search') }}";
                    },
                    className: 'btn btn-sm btn-warning xsbtn'
                }
            ]
        });

        var T2 = $('table#dataTable_healthcare').DataTable({
            "Paginate": true,
            "LengthChange": true,
            "Filter": true,
            "Sort": true,
            "Info": true,
            "AutoWidth": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{{ url('records/gethealthcare') }}",
                "type": "POST",
                "headers": { 'X-CSRF-Token': $('meta[name="_token"]').attr('content') }
            },
            "fnDrawCallback": function( oSettings ) {
                //insert all functions for Healthcare datatable
                //from modules and plugins
                {{ dataTableHCJsFuncs() }}
            },
            "columns": [
                { "data": "pid" },
                { "data": "name" },
                { "data": "service_type" },
                { "data": "seen_by" },
                { "data": "encounter_datetime" },
                { "data": "action" }
            ],
            "columnDefs": [
                { "orderable": false, "targets": 'nosort' }
            ],
            "order": [[ 4, "desc" ]],
            dom: "<'row'<'col-sm-5 col-xs-5 hidden-xs'l><'col-sm-5 col-xs-12'f><'col-sm-2 col-xs-2 hidden-xs'B>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                {
                    text: '<i class="fa fa-search"></i><span> | Advanced Search</span>',
                    action: function ( e, dt, node, config ) {
                        location="{{ url('records/search') }}";
                    },
                    className: 'btn btn-sm btn-warning xsbtn'
                }
            ]
        });

        $("#deathInfoModal").on("show.bs.modal", function(e) {
            $(this).find(".modal-content").html("");
            var link = $(e.relatedTarget);
            $(this).find(".modal-content").load(link.attr("href"));
        });
        
    </script>
@stop
