@extends('nurseregistry::layouts.master')

@section('page-content')		
	<div class="col-sm-12">
		<div class="box box-primary">
		  <div class="box-header with-border">
		    <h3 class="box-title">Add New Nurse Record</h3>
		  </div>
		  @include('nurseregistry::pages.nurseregistry-form')
		</div>
	</div>
@stop