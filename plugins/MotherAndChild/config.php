<?php
$plugin_id = 'MotherAndChild';                       //plugin ID
$plugin_module = 'healthcareservices';          //module owner
$plugin_location = 'dropdown';                  //UI location where plugin will be accessible
$plugin_primaryKey = 'motherandchild_id';        //primary_key used to find data
$plugin_table = 'motherandchild_service';            //plugintable default; table_name custom table
$plugin_tabs_child = array('addservice','vitals','motherandchild_plugin'); //,
$plugin_type = 'consultation';
$plugin_age = '08-60';
$plugin_gender = "F";
//plugin maximum role to access this plugin (MANDATORY VALUE)
$plugin_role = 6;

$plugin_relationship = array();
$plugin_folder = 'MotherAndChild'; //module owner
$plugin_title = 'Mother and Child';            //plugin title
$plugin_description = 'Mother and Child';
$plugin_version = '1.0';
$plugin_developer = 'mediXserve';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2018";

$plugin_tabs = [
    'addservice' => 'Basic Information',
    'complaints' => 'Complaints',
    'impanddiag' => 'Impressions & Diagnosis',
    'disposition' => 'Disposition',
    'medicalorders' => 'Medical Orders',
    'vitals' => 'Vitals & Physical',
    'motherandchild_plugin' => 'Mother And Child'
];

$plugin_tabs_models = [
    'complaints' => 'GeneralConsultation',
    'disposition' => 'Disposition',
    'medicalorders' => 'MedicalOrder',
    'vitals' => 'VitalsPhysical',
    'motherandchild_plugin' => 'MotherAndChildModel'
];