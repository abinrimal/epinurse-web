<?php namespace Modules\Laboratory\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrder;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderLabExam;
use ShineOS\Core\LOV\Entities\LovLaboratories;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\PatientDeathInfo;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use Shine\Libraries\IdGenerator;
use Modules\Laboratory\Entities\Laboratory;
use Session, Input, Validator, Request, Redirect, DB;

class LaboratoryController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');

        $modules = Session::get('roles');
    }

    public function index() {
        $data = [];
        $arr_medicalorder_id = [];
        $data['lov_laboratories'] = LovLaboratories::orderBy('laboratorydescription')->lists('laboratorydescription','laboratorycode');
        $facility_id = Session::get('facility_details');

        $facilityuser = FacilityUser::where('facility_id', $facility_id->facility_id)->lists('facilityuser_id');
        $facilityPatientUser = FacilityPatientUser::whereIn('facilityuser_id', $facilityuser)->get();

        if(count($facilityPatientUser)) {
            foreach ($facilityPatientUser as $fps_k => $fps_v) {

                $MedicalOrder = DB::select("SELECT * FROM `healthcare_services` a
                    LEFT JOIN `medicalorder` b ON b.`healthcareservice_id` = a.`healthcareservice_id`
                    WHERE a.`facilitypatientuser_id` = '".$fps_v->facilitypatientuser_id."'
                    AND b.`medicalorder_type` = 'MO_LAB_TEST' AND a.`deleted_at` IS NULL");

                if($MedicalOrder) {

                    foreach ($MedicalOrder as $key => $v) {

                        $death = PatientDeathInfo::where('patient_id', $fps_v->patient_id)->first();

                        if(!$death){
                            $patient = Patients::where('patient_id', $fps_v->patient_id)->first();
                            if($patient) {
                            $data['MedicalOrderLabExam'][$fps_k][$key]['lab'] = DB::table('medicalorder_laboratoryexam')
                                ->select(DB::raw('*, medicalorder_laboratoryexam.medicalorderlaboratoryexam_id AS lr_mdle_id, laboratory_result.created_at AS labdate, medicalorder_laboratoryexam.created_at AS orderdate'))
                                ->where('medicalorder_id',$v->medicalorder_id)
                                ->leftJoin('laboratory_result','laboratory_result.medicalorderlaboratoryexam_id','=','medicalorder_laboratoryexam.medicalorderlaboratoryexam_id')
                                ->get();

                                $data['MedicalOrderLabExam'][$fps_k][$key]['patient'] = $patient->toArray();
                            

                            $data['MedicalOrderLabExam'][$fps_k][$key]['healthcareservice'] = Healthcareservices::where('healthcareservice_id', $v->healthcareservice_id)->first()->toArray();

                            $arr_medicalorder_id[$fps_k][$key] = $v->medicalorder_id;
                            $arr_medicalorder_idd[$fps_k."-".$key] = $v->medicalorder_id;
                            $data['MedicalOrderLabExam_count'] = DB::table('medicalorder_laboratoryexam')->select('laboratory_test_type', DB::raw('count(*) as total'))->whereIn('medicalorder_id',$arr_medicalorder_idd)->groupBy('laboratory_test_type')->get();
                            }
                        }

                    }
                }
            }
        }


        return view('laboratory::index')->with($data);
    }

    public function add() {

        $input = Input::all();
        $exists = Laboratory::where('medicalorderlaboratoryexam_id',Input::get('medicalorderlaboratoryexam_id'))->first();

        unset($input['_token']);
        unset($input['medicalorderlaboratoryexam_id']);
        unset($input['lab_file_upload']);
        $json_labdata = json_encode($input);

        if(count($exists) < 1) {
            $lab_file_upload = Input::file('lab_file_upload');
            if($lab_file_upload!=NULL || !empty($lab_file_upload)) {
                if ($lab_file_upload->isValid()) {
                    $destinationPath = upload_base_path().'lab_results'; // upload path
                    $extension = Input::file('lab_file_upload')->getClientOriginalExtension();
                    $fileName = "lab_".rand(11111,99999).'_'.date('YmdHis').'.'.$extension;
                    $originalName = Input::file('lab_file_upload')->getClientOriginalName();
                    Input::file('lab_file_upload')->move($destinationPath, $fileName);

                    $Lab = new Laboratory();
                    $Lab->laboratoryresult_id = IdGenerator::generateId();
                    $Lab->medicalorderlaboratoryexam_id = Input::get('medicalorderlaboratoryexam_id');
                    $Lab->lab_data = $json_labdata;
                    $Lab->filename = $fileName;
                    $Lab->save();

                    $flash_type = "alert-class alert-success alert-dismissible";
                    $flash_message = "Successfully added data.";
                } else {
                    $flash_type = "alert-class alert-danger alert-dismissible";
                    $flash_message = "Failed to add data.";
                }
            } else {

                $Lab = new Laboratory();
                $Lab->laboratoryresult_id = IdGenerator::generateId();
                $Lab->medicalorderlaboratoryexam_id = Input::get('medicalorderlaboratoryexam_id');
                $Lab->lab_data = $json_labdata;
                $Lab->save();

                $flash_type = "alert-class alert-success alert-dismissible";
                $flash_message = "Successfully added data.";
            }
        } else {
            $flash_type = "alert-class alert-danger alert-dismissible";
            $flash_message = "Laboratory already exists.";
        }

       return back()->with('flash_type', $flash_type)->with('flash_message',$flash_message);
    }

    public function update() {

        $input = Input::all();
        $exists = Laboratory::where('medicalorderlaboratoryexam_id',Input::get('medicalorderlaboratoryexam_id'))->first();

        unset($input['_token']);
        unset($input['medicalorderlaboratoryexam_id']);
        unset($input['lab_file_upload']);
        $json_labdata = json_encode($input);

        //lab result must be existing.
        if(count($exists) > 0) {
            $lab_file_upload = Input::file('lab_file_upload');
            if($lab_file_upload!=NULL || !empty($lab_file_upload)) {
                if ($lab_file_upload->isValid()) {
                    $destinationPath = upload_base_path().'lab_results'; // upload path
                    $extension = Input::file('lab_file_upload')->getClientOriginalExtension();
                    $fileName = "lab_".rand(11111,99999).'_'.date('YmdHis').'.'.$extension;
                    $originalName = Input::file('lab_file_upload')->getClientOriginalName();
                    Input::file('lab_file_upload')->move($destinationPath, $fileName);

                    if($exists->filename) {
                        $upfilename = $exists->filename.";".$fileName;
                        Laboratory::where('medicalorderlaboratoryexam_id',Input::get('medicalorderlaboratoryexam_id'))
                    ->update(['filename' => $upfilename, 'lab_data' => $json_labdata]);
                    } else {
                        Laboratory::where('medicalorderlaboratoryexam_id',Input::get('medicalorderlaboratoryexam_id'))
                    ->update(['filename' => $fileName, 'lab_data' => $json_labdata]);
                    }

                    $flash_type = "alert-class alert-success alert-dismissible";
                    $flash_message = "Successfully updated data.";
                } else {
                    $flash_type = "alert-class alert-danger alert-dismissible";
                    $flash_message = "Failed to add data.";
                }
            } else {
                Laboratory::where('laboratoryresult_id',Input::get('laboratoryresult_id'))
                    ->update(['lab_data' => $json_labdata]);

                $flash_type = "alert-class alert-success alert-dismissible";
                $flash_message = "Successfully updated Lab data.";
            }
        } else {
            $flash_type = "alert-class alert-danger alert-dismissible";
            $flash_message = "Laboratory data is invalid or missing.";
        }

       return back()->with('flash_type', $flash_type)->with('flash_message',$flash_message);
    }

    public function add_view($laboratory_test_type, $medicalorderlaboratoryexam_id) {
        $data['lab'][$laboratory_test_type] = MedicalOrderLabExam::where('medicalorderlaboratoryexam_id',$medicalorderlaboratoryexam_id)->with('LaboratoryResult')->first();

        return view('laboratory::view')->with($data);
    }

    public function modal_view($laboratory_test_type, $medicalorderlaboratoryexam_id) {
        $data['disposition'] = NULL;
        $data['lab'][$laboratory_test_type] = $lab = Laboratory::where('medicalorderlaboratoryexam_id', $medicalorderlaboratoryexam_id)->with('MedicalOrderLabExam')->first();
        if($lab){
            //get disposition of this encounter
            $MO = MedicalOrder::with('Healthcareservices')->where('medicalorder_id',$lab['MedicalOrderLabExam']->medicalorder_id)->first();
            $HC = Healthcareservices::with('Disposition')->where('healthcareservice_id', $MO['Healthcareservices']->healthcareservice_id)->first();
            if($HC['Disposition']) {
                $data['disposition'] = $HC['Disposition']->disposition;
            }
        }
        $data['medicalorderlaboratoryexam_id'] = $medicalorderlaboratoryexam_id;
        $data['testType'] = $laboratory_test_type;
        $data['medicalorder_laboratoryexam'] = MedicalOrderLabExam::where('medicalorderlaboratoryexam_id',$medicalorderlaboratoryexam_id)->first();

        return view('laboratory::mod')->with($data);
    }

    public function delete_attachment($labid, $filename) {
        $exists = Laboratory::where('laboratoryresult_id',$labid)->first();

        $upfilenames = "";
        $filenames = explode(';', $exists->filename);
        foreach($filenames as $file) {
            if($filename != $file) {
                $upfilenames .= $file.";";
            }
        }

        Laboratory::where('laboratoryresult_id',$labid)
                    ->update(['filename' => trim($upfilenames, ';')]);

    }
}
