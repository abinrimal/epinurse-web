<?php

namespace Plugins\Philhealth;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class PhilhealthPMRFModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'philhealth_pmrf';
    protected static $table_name = 'philhealth_pmrf';
    protected $primaryKey = 'id';
    
}
