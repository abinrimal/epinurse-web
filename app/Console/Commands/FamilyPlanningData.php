<?php

namespace Shine\Console\Commands;

use Illuminate\Console\Command; 
use DB;

class FamilyPlanningData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronReports:FamilyPlanningData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron for collecting data reports for family planning';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fp_data = NULL;
        //$facility = Session::get('facility_details');
        $years = array(2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017);
        $months = array(1,2,3,4,5,6,7,8,9,10,11,12);

        foreach($years as $year) {
            foreach($months as $month) {
            //$month = date('n');
            //$year = date('Y');
                echo "Processing ".$month." ".$year."<br>";
                // ob_flush(); flush();
                // usleep(1125000);
                //process only current year
                $sql = "SELECT
                        ( SELECT h.facility_id FROM healthcare_services d
                            LEFT JOIN facility_patient_user b ON b.facilitypatientuser_id = d.facilitypatientuser_id
                            LEFT JOIN facility_user g ON g.facilityuser_id = b.facilityuser_id
                            LEFT JOIN facilities h ON h.facility_id = g.facility_id
                            WHERE d.`healthcareservice_id` = a.`healthcareservice_id` ) as 'facilityid',
                        ( SELECT i.barangay FROM healthcare_services d
                            LEFT JOIN facility_patient_user b ON b.facilitypatientuser_id = d.facilitypatientuser_id
                            LEFT JOIN patient_contact i ON b.patient_id = i.patient_id
                            WHERE d.`healthcareservice_id` = a.`healthcareservice_id` ) as 'barangaycode',
                        a.`current_method`,
                        a.`previous_method`,
                        SUM(a.`client_type`='CU') as 'CU',
                        SUM(a.`client_type`='NA') as 'NA_end',
                        SUM(a.`client_sub_type`='CC') as 'CC',
                        SUM(a.`client_sub_type`='CM') as 'CM',
                        SUM(a.`client_sub_type`='RS') as 'RS',
                        (SUM(a.`client_sub_type`='CC')+SUM(a.`client_sub_type`='CM')+SUM(a.`client_sub_type`='RS')) as 'OA',
                        SUM(a.`dropout_date` IS NOT NULL) as 'Dropout',
                        MONTH(a.`created_at`) as 'FPmonth',
                        YEAR(a.`created_at`) as 'FPyear', count(*) as 'FP_count',
                        a.created_at
                    FROM `familyplanning_service` a
                    WHERE YEAR(a.created_at) = $year
                    AND MONTH(a.created_at) = $month
                    GROUP BY a.current_method, a.previous_method, facilityid, barangaycode
                    ORDER BY FPyear ASC, FPmonth ASC";

                $cur_fp = DB::select( DB::raw( $sql ));
                if($cur_fp) {

                    foreach($cur_fp as $fp) {

                        if($fp->facilityid != NULL AND ($fp->current_method != NULL OR $fp->previous_method != NULL)) {

                            if($fp->FPmonth == 1) {
                                $fpmonth = 12;
                                $fpyear = $fp->FPyear - 1;
                            } else {
                                $fpmonth = $fp->FPmonth-1;
                                $fpyear = $fp->FPyear;
                            }

                            $brgy = $fp->barangaycode;
                            if($fp->barangaycode == NULL) {
                                $brgy = DB::table('facility_contact')->where('facility_id', $fp->facilityid)->pluck('barangay');
                            }

                            // Count CU and NA of previous months of current method
                            $CUpreviousMonth = self::getPrevCount('CU_end', $fp->facilityid, $brgy, $fp->current_method, False, $fpmonth, $fpyear);
                            $NApreviousMonth = self::getPrevCount('NA_end', $fp->facilityid, $brgy, $fp->current_method, False, $fpmonth, $fpyear);

                            // Count dropouts of previous method
                            $DropoutcurrentMonth = self::getPrevCount('Dropout', $fp->facilityid, $brgy, $fp->current_method, True, $fp->FPmonth, $fp->FPyear);

                            //compute ending users using FHSIS formula
                            $CUend = $CUpreviousMonth + $NApreviousMonth + $fp->OA - $DropoutcurrentMonth;
                            if($CUend < 0) {
                                $CUend = 0;
                            }

                            //delete previous record for this month, year, facility and method
                            $deletePrevM1FP = M1FP::where('facility_id',$fp->facilityid)
                                                    ->where('barangaycode',$brgy)
                                                    ->where('current_method',$fp->current_method)
                                                    ->where('previous_method',$fp->previous_method)
                                                    ->where('FPmonth',$fp->FPmonth)
                                                    ->where('FPyear',$fp->FPyear)
                                                    ->delete();

                            //store new computed data for this record
                            $fp_data = new M1FP();
                            $fp_data->facility_id = (is_null($fp->facilityid)) ? 0: $fp->facilityid;
                            $fp_data->barangaycode = (is_null($brgy)) ? 0: $brgy;
                            $fp_data->current_method = (is_null($fp->current_method)) ? 0: $fp->current_method;
                            $fp_data->previous_method = (is_null($fp->previous_method)) ? 0: $fp->previous_method;
                            $fp_data->CU_begin = (is_null($CUpreviousMonth)) ? 0: $CUpreviousMonth;
                            $fp_data->NA_begin = (is_null($NApreviousMonth)) ? 0: $NApreviousMonth;
                            $fp_data->CC = (is_null($fp->CC)) ? 0: $fp->CC;
                            $fp_data->CM = (is_null($fp->CM)) ? 0: $fp->CM;
                            $fp_data->RS = (is_null($fp->RS)) ? 0: $fp->RS;
                            $fp_data->OA = (is_null($fp->OA)) ? 0: $fp->OA;
                            $fp_data->Dropout = (is_null($fp->Dropout)) ? 0: $fp->Dropout;
                            $fp_data->CU_end = (is_null($CUend)) ? 0: $CUend;
                            $fp_data->NA_end = (is_null($fp->NA_end)) ? 0: $fp->NA_end;
                            $fp_data->FPmonth = (is_null($fp->FPmonth)) ? 0: $fp->FPmonth; 
                            $fp_data->FPyear = (is_null($fp->FPyear)) ? 0: $fp->FPyear;
                            $fp_data->save();
                        }
                    }
                }
            }
        }
        if ($fp_data) :
            $this->comment(PHP_EOL."Done".PHP_EOL);
        else:
            $this->comment(PHP_EOL."No Data Found".PHP_EOL);
        endif;

    }

    private function getPrevCount($type, $facilityid, $brgycode ,$method, $dropout=False,$month, $year) {
        if($dropout==False):
            $prevCU = M1FP::select($type)->where('facility_id', $facilityid)->where('barangaycode',$brgycode)->where('current_method', $method)->where('FPmonth', $month)->where('FPyear', $year)->first();
        else:
            $prevCU = M1FP::select($type)->where('facility_id', $facilityid)->where('barangaycode',$brgycode)->where('previous_method', $method)->where('FPmonth', $month)->where('FPyear', $year)->first();
        endif;

        if($prevCU) {
            if($prevCU->$type > 0) {
                return $prevCU->$type;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

}
