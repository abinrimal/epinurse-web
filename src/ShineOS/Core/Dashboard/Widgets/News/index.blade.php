<div class="box box-primary">
    <div class="box-header">
        <i class="fa fa-newspaper-o"></i><h3 class="box-title text-shine-blue boxTitle">News &amp; Updates</h3>
    </div>

    <div class="box-body">

        <!-- https://www.philhealth.gov.ph/advisories/2018/adv2018-0024.pdf -->
        <div class="col-md-12">
            <h4 class="smartblue"> Welcome! <small class="text-muted pull-right text-small"> January 16, 2019</small></h4>
            <p> Welcome to EpiNurse! </p>
            <hr />
        </div>
    </div><!-- /.box-body -->
    @if(!empty($visit_list))
    <div class="box-footer text-center">
        
    </div><!--/.box-footer-->
    @endif

</div><!--/. end consultations-->

<script>

</script>
