<?php

namespace Widgets\dashboard\Analytics;

use Arrilot\Widgets\AbstractWidget;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\Healthcareservices\Entities\Diagnosis;
use Shine\Repositories\Eloquent\FacilityRepository;
use ShineOS\Core\Facilities\Entities\FacilityUser;

use DB;
use Carbon\Carbon;
use View;
use Session;

class analytics extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function placeholder()
    {
        $loading = '<div class="box box-primary"><!--Consultations-->
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-cog fa-spin fa-fw"></i> Loading Analytics widget...</h3>
                </div>
            </div>';


        return $loading;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $facilityInfo = Session::get('facility_details');
        $user = Session::get('user_details');
        $bhs = NULL;
        if($user->facilityUser){
            foreach($user->facilityUser as $fuser) {
                if($fuser->facility_id == $facilityInfo->facility_id AND $fuser->catchment_area_id != NULL) {
                    //this is a BHS user
                    $bhs = $fuser->catchment_area_id;
                }
            }
        }

        $maxdate = date('Y-m-d 59:59:59');
        $rmaxdate = date('Y-m-d');
        $xdate = strtotime($rmaxdate .' -6 month');
        $mindate = date('Y-m-d 00:00:00', $xdate);

        $services = NULL;
        $diagnosis = NULL;
        $ranges = NULL;
        $cs_stats = NULL;
        $total = $scount = 0;

        if($bhs) { //this is a BHS account
            $fus = FacilityUser::where('catchment_area_id', $bhs)->lists('facilityuser_id')->toArray();
            if($fus) {
                //check if there are records to generate stats from
                $total = DB::table('healthcare_view')
                    ->whereIn('facilityuser_id', $fus)
                    ->where('hccreated', '<=', $maxdate)
                    ->where('hccreated', '>', $mindate)
                    ->count();
                if($total > 0) {
                    $services = DB::table('healthcare_view')
                        ->select('healthcareservicetype_id')
                        ->whereIn('facilityuser_id', $fus)
                        ->where('hccreated', '>=', $mindate)
                        ->where('hccreated', '<=', $maxdate)
                        ->groupBy('healthcareservicetype_id')
                        ->get();

                    $diagnosis = DB::table('diagnosis_view')
                        ->select('diagnosislist_id', DB::raw('count(*) as bilang'))
                        ->whereIn('facilityuser_id', $fus)
                        ->where('hccreated', '<=', $maxdate)
                        ->where('hccreated', '>', $mindate)
                        ->groupBy('diagnosislist_id')
                        ->orderBy('bilang', 'desc')
                        ->take(4)
                        ->get();

                    $cs_stats = $ranges = [];

                    for($d = 1; $d <= 6; $d++) {
                        $xr = strtotime($mindate .' +'.$d.' months');
                        $ranges[$d] = date('Y-m-d', $xr);
                    }

                    $scount = count($services);

                    foreach($services as $service) {
                        foreach($ranges as $range) {
                            $max = date('Y-m-30 59:59:59', strtotime($range));
                            $min = date('Y-m-01 00:00:00', strtotime($range));

                            $bils = DB::table('healthcare_view')
                                ->select(DB::raw('count(*) as bilang'))
                                ->whereIn('facilityuser_id', $fus)
                                ->where('hccreated', '>=', $min)
                                ->where('hccreated', '<=', $max)
                                ->where('healthcareservicetype_id', $service->healthcareservicetype_id)
                                ->get();

                            foreach($bils as $k=>$bil) {
                                if($bil->bilang > 1) {
                                    $cs_stats[$service->healthcareservicetype_id][$range] = $bil->bilang;
                                }
                            }
                        }
                    }
                }
            } else {
                $services = NULL;
                $diagnosis = NULL;
                $ranges = NULL;
                $cs_stats = NULL;
                $total = 0;
                $scount = 0;
            }

        } else {
            //check if there are records to generate stats from
            $total = DB::table('healthcare_view')
                ->where('facility_id', $facilityInfo->facility_id)
                ->where('hccreated', '<=', $maxdate)
                ->where('hccreated', '>', $mindate)
                ->count();
            if($total > 0) {
                $services = DB::table('healthcare_view')
                    ->select('healthcareservicetype_id')
                    ->where('facility_id', $facilityInfo->facility_id)
                    ->where('hccreated', '>=', $mindate)
                    ->where('hccreated', '<=', $maxdate)
                    ->groupBy('healthcareservicetype_id')
                    ->get();

                $diagnosis = DB::table('diagnosis_view')
                    ->select('diagnosislist_id', DB::raw('count(*) as bilang'))
                    ->where('facility_id', $facilityInfo->facility_id)
                    ->where('hccreated', '<=', $maxdate)
                    ->where('hccreated', '>', $mindate)
                    ->groupBy('diagnosislist_id')
                    ->orderBy('bilang', 'desc')
                    ->take(4)
                    ->get();

                $cs_stats = $ranges = [];

                for($d = 1; $d <= 6; $d++) {
                    $xr = strtotime($mindate .' +'.$d.' months');
                    $ranges[$d] = date('Y-m-d', $xr);
                }

                $scount = count($services);

                foreach($services as $service) {
                    foreach($ranges as $range) {
                        $max = date('Y-m-30 59:59:59', strtotime($range));
                        $min = date('Y-m-01 00:00:00', strtotime($range));

                        $bils = DB::table('healthcare_view')
                            ->select(DB::raw('count(*) as bilang'))
                            ->where('facility_id', $facilityInfo->facility_id)
                            ->where('hccreated', '>=', $min)
                            ->where('hccreated', '<=', $max)
                            ->where('healthcareservicetype_id', $service->healthcareservicetype_id)
                            ->get();

                        foreach($bils as $k=>$bil) {
                            if($bil->bilang > 1) {
                                $cs_stats[$service->healthcareservicetype_id][$range] = $bil->bilang;
                            }
                        }
                    }
                }
            }
        }

        View::addNamespace('analytics-widgets', 'widgets/dashboard/Analytics/');
        return view("analytics-widgets::index", [
            'config' => $this->config,
            'services' => $services,
            'mon' => $diagnosis,
            'ranges' => $ranges,
            'cs_stats' => $cs_stats,
            'total' => $total,
            'scount' => $scount
        ]);
    }
}
