<?php
namespace Modules\Laboratory\Entities;
use Illuminate\Database\Eloquent\Model;

class Laboratory extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'laboratory_result';
    protected static $static_table = 'laboratory_result';
    protected $primaryKey = 'laboratoryresult_id';
    protected $fillable = [];

    public function MedicalOrderLabExam() {
          return $this->belongsTo('ShineOS\Core\Healthcareservices\Entities\MedicalOrderLabExam', 'medicalorderlaboratoryexam_id', 'medicalorderlaboratoryexam_id');
      }

}
