<?php
namespace Widgets\analytics\HealthScreening;

use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Shine\Repositories\Eloquent\UserRepository as UserRepository;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\FacilityRepositoryInterface;
use Shine\Libraries\HealthcareservicesHelper;
use Shine\Libraries\FacilityHelper;
use Arrilot\Widgets\AbstractWidget;
use View;

use Plugins\JEEPPraxis\JEEPPraxisModel;

//DO NOT FORGET TO UNCOMMENT LOOP WHEN DONE WITH REPOSITORIES
class HealthScreening extends AbstractWidget
{
    private $UserRepository;
    private $healthcareRepository;

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function placeholder()
    {
        $loading = '<div class="box box-primary"><!--Consultations-->
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-cog fa-spin fa-fw"></i> Loading widget...</h3>
                </div>
            </div>';


        return $loading;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(UserRepository $UserRepository, HealthcareRepository $healthcareRepository)
    {
        $start = date('Y-m-01');
        $end = date('Y-m-d', strtotime('last day of this month'));
        // $visits = getHealthScreening($start,$end);

        $field_trips = JEEPPraxisModel::getRowsAndCount($start, $end);
        $field_trip = $field_trips['total'];
        
        $jta = '';

        $values['field_trip'] = $field_trip;
        $values['jta'] = $jta;

        View::addNamespace('health_screening', 'widgets/analytics/HealthScreening/');
        return view("health_screening::index", [
            'config' => $this->config,
            'values' => $values,
        ]);
    }
}
