<?php

namespace ShineOS\Core\Reports\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use ShineOS\Core\Reports\Http\Controllers\MaternalReportController as MaternalReportController;
use ShineOS\Core\Reports\Entities\M1;
use ShineOS\Core\Reports\Entities\M1FP; 
use ShineOS\Core\Facilities\Entities\FacilityWorkforce;
use ShineOS\Core\Reports\Http\Controllers\ReportsController;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Reports\Http\Controllers\TuberculosisReportController;
use Session,SoapClient,SoapVar,SoapHeader,DB,Input;

class FHSISReportsController extends Controller {

    protected $fhsisPath = 'reports::pages.fhsis_reports.';
    public function __construct() {
        $this->facility = NULL;
        if(Session::has('facility_details')) {
            $this->facility = Session::get('facility_details');
        }
        $this->month = date('m');
        $this->day = date('d');
        $this->year = date('Y');
        $this->wsdl = "http://uhmistrn.doh.gov.ph/efhsis/webservice/index.php?wsdl";  
        $this->fhsis = array('prg_ChildCare'=>'fhsis_childcare',
                        'prg_DemographicProfile'=>'fhsis_demographic_profile',
                        'prg_DentalHealth'=>'fhsis_dentalhealth',
                        'prg_EnvironmentalHealth'=>'fhsis_environmental_health',
                        'prg_FamilyPlanning'=>'fhsis_family_planning',
                        'prg_Filariasis'=>'fhsis_filariasis',
                        'prg_Leprosy'=>'fhsis_leprosy',
                        'prg_Malaria'=>'fhsis_malaria',
                        'prg_MaternalCare'=>'fhsis_maternalcare',
                        'prg_Morbidity'=>'fhsis_morbidity',
                        'prg_Mortality'=>'fhsis_mortality',
                        'prg_MortalityBHS'=>'fhsis_mortalitybhs',
                        'prg_Natality'=>'fhsis_natality',
                        'prg_Population'=>'fhsis_population',
                        'prg_Schistosomiasis'=>'fhsis_schistosomiasis',
                        'prg_Tuberculosis'=>'fhsis_tuberculosis',
                        'prg_Morbidity_Others'=>'fhsis_morbidity_others',
                        'prg_MortalityBHS_Others'=>'fhsis_mortality_others',
                        );
    }

    public function index() {
    }

    public function web_service($ws_program=NULL,$ws_indicator=NULL) {
        // dd($ws_program,$ws_indicator);
        // return array($ws_program,$ws_indicator);

    /////////////////////////////////////////////////////////////////////////////////
        // Initialize Soap Client
        $server_wsdl = $this->wsdl;
        $streamContext = stream_context_create(array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false
             )
        ));
        $option=array(
                'trace'=>1,
                'stream_context' => $streamContext
            );
        // Declare Soap Header to include in Soap Request
        // AuthenticationKey : Required Parameter for PHIE Soap Header.
        $headerBody = array(
                'ReturnFormat'=>'json' // json or xml
            );
        $headerBody = new SoapVar($headerBody,SOAP_ENC_OBJECT);
        $soap_header = new SoapHeader($server_wsdl,'Authentication',$headerBody);
        /* ---------------- Call Soap Method (Web Service Method) -------------------- */
        $wsParam = NULL;
        if($ws_program==NULL) {
            $function = 'WS_Check';
        } else {
            $function = 'ws_push_programdata';
            $wsParam = array(
                "ws_user"=>"SamboanRHU", // sample username
                "ws_pass"=>"SVxW9jbg65SGakD2", // sample password
                "ws_program"=>$ws_program, // type
                "ws_indicator"=>$ws_indicator // data
            );
        }
        $soap_server = new SoapClient($this->wsdl,$option);
        try{
            $soap_result = $soap_server->__soapCall($function, array($wsParam), NULL,$soap_header, $output_headers);
            /* -- Get Soap Response Header -- */
            $Response_Code = $output_headers['response_code'];
            $Response_Desc = $output_headers['response_desc'];
            $Response_DateTime = $output_headers['response_datetime'];
            $Response_Value = $output_headers['response_value'];

            $return = array('soap_result'=>$soap_result, 'response_code'=>$Response_Code, 'response_desc'=>$Response_Desc, 'response_datetime'=>$Response_DateTime, 'response_value'=>$Response_Value);
            // dd("a",$soap_server->__getLastResponse(), $return, $soap_result);
            return $return;
        } catch (\SoapFault $fault) {
            // dd("b", $fault, $soap_server->__getLastResponse());
            return $fault;
        }
    }

    public static function view_fhsis($type,$month,$year,$barangay,$quarter=NULL) {

        $_this = new self();
        $function = $_this->fhsis[$type];
        $data = $_this->$function($month,$year,$barangay);
        $data['type'] = $type;
        if(!is_null($quarter)) { $data['quarter'] = $quarter; }
        // dd($data);
        return $data;
    }

    public static function send_fhsis() { //Send fhsis program
        // dd(Input::all());
        $_this = new self();
        $input = Input::all();
        $send = $result = array();
        
        if(array_key_exists('fhsis_data', $input)) {
            $fhsis_data = (array)json_decode($input['fhsis_data']);
            // print_r($fhsis_data);
            $bgycode = $fhsis_data['bgycode'];
            $DMonth = $fhsis_data['DMonth'];
            $Year = array_key_exists('DYear', $fhsis_data) ? $fhsis_data['DYear'] : (array_key_exists('YEAR', $fhsis_data) ? $fhsis_data['YEAR'] : $fhsis_data['YEAR_ENV']);
            // dd($fhsis_data);
            if(is_array($bgycode) AND is_array($DMonth) AND $fhsis_data['type']!='prg_DemographicProfile' AND $fhsis_data['type']!='prg_EnvironmentalHealth' AND $fhsis_data['type']!='prg_Population') {
                /////////// ALL BARANGAY QUARTERLY OR ANNUALLY ////////////
                unset($fhsis_data['bgycode']);
                unset($fhsis_data['DMonth']);
                $ctr=0;
                foreach ($bgycode as $k_bgycode => $v_bgycode) {
                    foreach ($DMonth as $k_DMonth => $v_DMonth) {
                        $function = $_this->fhsis[$fhsis_data['type']];
                        $send[$ctr] = $_this->$function($v_DMonth,$Year,$v_bgycode);
                        $send[$ctr]['type'] = $fhsis_data['type'];
                        $ctr += 1;
                    }
                }
            } else if(is_array($bgycode)) {
                /////////// ALL BARANGAY MONTHLY /////////////////
                unset($fhsis_data['bgycode']);
                foreach ($bgycode as $k_bgycode => $v_bgycode) {
                    $function = $_this->fhsis[$fhsis_data['type']];
                    $send[$k_bgycode] = $_this->$function($fhsis_data['DMonth'],$Year,$v_bgycode);
                    $send[$k_bgycode]['type'] = $fhsis_data['type'];

                }
            } else if(is_array($DMonth) AND $fhsis_data['type']!='prg_DemographicProfile' AND $fhsis_data['type']!='prg_EnvironmentalHealth' AND $fhsis_data['type']!='prg_Population') {
                /////////// ONE BARANGAY QUARTERLY OR ANNUALLY //////////////
                unset($fhsis_data['DMonth']);
                foreach ($DMonth as $k_DMonth => $v_DMonth) {
                    $function = $_this->fhsis[$fhsis_data['type']];
                    $send[$k_DMonth] = $_this->$function($v_DMonth,$Year,$fhsis_data['bgycode']);
                    $send[$k_DMonth]['type'] = $fhsis_data['type'];
                }
            } else {
                // One Barangay per month-year
               $send[] = $fhsis_data;
            }
        }
        //SEND ---- insert function web service
        if(!is_null($send)) {
            foreach ($send as $k_send => $v_send) {
                $type = $v_send['type'];
                if($type=='prg_DemographicProfile' OR $type=='prg_EnvironmentalHealth' OR $type=='prg_Population') {
                    unset($v_send['DMonth']);
                }
                unset($v_send['type']);
                $result[$k_send] = $_this->web_service($type,$v_send);
            }
        }
        dd($send,$result);
    }

    public function fhsis_childcare($month=NULL, $year=NULL, $barangay=NULL) {

        $default_limit = ini_get('max_execution_time');
        set_time_limit(300);

        $data = array();
        if($this->facility!=NULL) {

            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;
            // dd('m',$month,$year,$barangay);
            $chilccare = ChildCareReportsController::recommendedImmunization('m',$month,$year,$barangay);
            $chilccare_diagnosis_based = ChildCareReportsController::diagnosisBased('m',$month,$year,$barangay);

            // Child given BCG
            $data['IMM_BCG_M'] = $chilccare['M']['IMM_BCG'];
            $data['IMM_BCG_F'] = $chilccare['F']['IMM_BCG'];

            // Child given DPT1
            $data['IMM_DPT1_M'] = $chilccare['M']['IMM_DPT1'];
            $data['IMM_DPT1_F'] = $chilccare['F']['IMM_DPT1'];

            // Child given DPT2
            $data['IMM_DPT2_M'] = $chilccare['M']['IMM_DPT2'];
            $data['IMM_DPT2_F'] = $chilccare['F']['IMM_DPT2'];

            // Child given DPT3
            $data['IMM_DPT3_M'] = $chilccare['M']['IMM_DPT3'];
            $data['IMM_DPT3_F'] = $chilccare['F']['IMM_DPT3'];
            
            // Child given OPV1
            $data['IMM_OPV1_M'] = $chilccare['M']['IMM_OPV1'];
            $data['IMM_OPV1_F'] = $chilccare['F']['IMM_OPV1'];

            // Child given OPV2
            $data['IMM_OPV2_M'] = $chilccare['M']['IMM_OPV2'];
            $data['IMM_OPV2_F'] = $chilccare['F']['IMM_OPV2'];

            // Child given OPV3
            $data['IMM_OPV3_M'] = $chilccare['M']['IMM_OPV3'];
            $data['IMM_OPV3_F'] = $chilccare['F']['IMM_OPV3'];

            // Child given HEPAB1 within 24 hrs
            $data['IMM_HEPAB1WIN24_M'] = $chilccare['M']['IMM_HEPAB1WIN24'];
            $data['IMM_HEPAB1WIN24_F'] = $chilccare['F']['IMM_HEPAB1WIN24'];

            // Child given HEPAB1 after 24 hrs
            $data['IMM_HEPAB124_M'] = $chilccare['M']['IMM_HEPAB1AFT24'];
            $data['IMM_HEPAB124_F'] = $chilccare['F']['IMM_HEPAB1AFT24'];

            // Child given HEPAB2
            $data['IMM_HEPAB2_M'] = $chilccare['M']['IMM_HEPAB2'];
            $data['IMM_HEPAB2_F'] = $chilccare['F']['IMM_HEPAB2'];

            // Child given HEPAB3
            $data['IMM_HEPAB3_M'] = $chilccare['M']['IMM_HEPAB3'];
            $data['IMM_HEPAB3_F'] = $chilccare['F']['IMM_HEPAB3'];

            // Child given MEASLES
            $data['IMM_MEAS_M'] = $chilccare['M']['IMM_MEAS'];
            $data['IMM_MEAS_F'] = $chilccare['F']['IMM_MEAS'];

            // Fully Immunized Child
            $data['FIC_M'] = $chilccare['M']['IMM_FULLY_IMM'];
            $data['FIC_F'] = $chilccare['F']['IMM_FULLY_IMM'];

            // Completely Immunized Child
            $data['CIC_M'] = $chilccare['M']['IMM_COMP_IMM'];
            $data['CIC_F'] = $chilccare['F']['IMM_COMP_IMM'];

            // Child Protected at Birth
            $data['CPB_M'] = $chilccare['M']['CPB'];
            $data['CPB_F'] = $chilccare['F']['CPB'];

            // Infant age 6 Months Seen
            $data['INF_AGE_M'] = $chilccare['M']['CHECKED_6MONTH'];
            $data['INF_AGE_F'] = $chilccare['F']['CHECKED_6MONTH'];

            // Infant exclusively breastfed until 6 months
            $data['INF_BREAST_M'] = $chilccare['M']['INF_BREAST'];
            $data['INF_BREAST_F'] = $chilccare['F']['INF_BREAST'];

            // Infant referred for Newborn Screening
            $data['INF_NEWBS_M'] = $chilccare['M']['INF_NEWBS'];
            $data['INF_NEWBS_F'] = $chilccare['F']['INF_NEWBS'];

            // Infant/Children given Vitamin A (6-11 months)
            $data['INF_VITA611_M'] = $chilccare['M']['INF_VITA611'];
            $data['INF_VITA611_F'] = $chilccare['F']['INF_VITA611'];

            // Infant/Children given Vitamin A (12-59 months)
            $data['INF_VITA1259_M'] = $chilccare['M']['INF_VITA1259'];
            $data['INF_VITA1259_F'] = $chilccare['F']['INF_VITA1259'];

            // Infant/Children given Vitamin A (60-71 months)
            $data['INF_VITA6071_M'] = $chilccare['M']['INF_VITA6071'];
            $data['INF_VITA6071_F'] = $chilccare['F']['INF_VITA6071'];

            // Sick Children seen (6-11 months) 
            $data['SICK_611_M'] = $chilccare_diagnosis_based['M']['SICK_611'];
            $data['SICK_611_F'] = $chilccare_diagnosis_based['F']['SICK_611'];

            // Sick Children seen (12-59 months)
            $data['SICK_1259_M'] = $chilccare_diagnosis_based['M']['SICK_1259'];
            $data['SICK_1259_F'] = $chilccare_diagnosis_based['F']['SICK_1259'];

            // Sick Children seen (60-61 months)
            $data['SICK_6071_M'] = $chilccare_diagnosis_based['M']['SICK_6071'];
            $data['SICK_6071_F'] = $chilccare_diagnosis_based['F']['SICK_6071'];

            // Sick Children given Vitamin A (6-11 months)
            $data['SICKVITA_611_M'] = $chilccare_diagnosis_based['M']['SICKVITA_611'];
            $data['SICKVITA_611_F'] = $chilccare_diagnosis_based['F']['SICKVITA_611'];

            // Sick Children given Vitamin A (12-59 months)
            $data['SICKVITA_1259_M'] = $chilccare_diagnosis_based['M']['SICKVITA_1259'];
            $data['SICKVITA_1259_F'] = $chilccare_diagnosis_based['F']['SICKVITA_1259'];

            // Sick Children given Vitaming A (60-71 months)
            $data['SICKVITA_6071_M'] = $chilccare_diagnosis_based['M']['SICKVITA_6071'];
            $data['SICKVITA_6071_F'] = $chilccare_diagnosis_based['F']['SICKVITA_6071'];

            // Infant 2-6 months with Low Birth Weight seen
            $data['INF26LBWNS_M'] = $chilccare['M']['INF26LBWNS'];
            $data['INF26LBWNS_F'] = $chilccare['F']['INF26LBWNS'];

            // Infant 2-6 months with Low Birth Weight given Iron
            $data['INF26FE_M'] = $chilccare['M']['INF26FE'];
            $data['INF26FE_F'] = $chilccare['F']['INF26FE'];

            // Anemic children seen (2-59 months)
            $data['ANECHILDNS_M'] = $chilccare_diagnosis_based['M']['ANECHILDNS'];
            $data['ANECHILDNS_F'] = $chilccare_diagnosis_based['F']['ANECHILDNS'];

            // Anemic children given Iron (2-59 months)
            $data['ANECHILDFE_M'] = $chilccare_diagnosis_based['M']['ANECHILDFE'];
            $data['ANECHILDFE_F'] = $chilccare_diagnosis_based['F']['ANECHILDFE'];

            // Children with Diarrhea (0-59 months old)
            $data['DIARRNC_M'] = $chilccare_diagnosis_based['M']['DIARRNC'];
            $data['DIARRNC_F'] = $chilccare_diagnosis_based['F']['DIARRNC'];

            // Children with Diarrhea given ORT (0-59 months old)
            $data['DIARRORT_M'] = $chilccare_diagnosis_based['M']['DIARRORS'];
            $data['DIARRORT_F'] = $chilccare_diagnosis_based['F']['DIARRORS'];

            // Children with Diarrhea given ORS (0-59 months old)
            $data['DIARRORS_M'] = $chilccare_diagnosis_based['M']['DIARRORS'];
            $data['DIARRORS_F'] = $chilccare_diagnosis_based['F']['DIARRORS'];

            // Children with Diarrhea given ORS with Zinc (0-59 months old)
            $data['DIARRORSZ_M'] = $chilccare_diagnosis_based['M']['DIARRORSZ'];
            $data['DIARRORSZ_F'] = $chilccare_diagnosis_based['F']['DIARRORSZ'];

            // Children with Pneumonia
            $data['PNEUNC_M'] = $chilccare_diagnosis_based['M']['PNEUNC'];
            $data['PNEUNC_F'] = $chilccare_diagnosis_based['F']['PNEUNC'];

            // Children with Pneumonia given treatment
            $data['PNEUGT_M'] = $chilccare_diagnosis_based['M']['PNEUGT'];
            $data['PNEUGT_F'] = $chilccare_diagnosis_based['F']['PNEUGT'];

            // Penta Antigen 1st dose 
            $data['IMM_PENTA1_M'] = $chilccare['M']['IMM_PENTA1'];
            $data['IMM_PENTA1_F'] = $chilccare['F']['IMM_PENTA1'];

            // Penta Antigen 2nd dose
            $data['IMM_PENTA2_M'] = $chilccare['M']['IMM_PENTA2'];
            $data['IMM_PENTA2_F'] = $chilccare['F']['IMM_PENTA2'];

            // Penta Antigen 3rd dose
            $data['IMM_PENTA3_M'] = $chilccare['M']['IMM_PENTA3'];
            $data['IMM_PENTA3_F'] = $chilccare['F']['IMM_PENTA3'];

            // MCV Antigen (AMV) 1st dose
            $data['IMM_MCV1_M'] = $chilccare['M']['IMM_MCV1'];
            $data['IMM_MCV1_F'] = $chilccare['F']['IMM_MCV1'];

            // MCV Antigen (AMV) 2nd dose
            $data['IMM_MCV2_M'] = $chilccare['M']['IMM_MCV2'];
            $data['IMM_MCV2_F'] = $chilccare['F']['IMM_MCV2'];

            // Rotavirus Antigen 1st dose
            $data['IMM_ROTA1_M'] = $chilccare['M']['IMM_ROTA1'];
            $data['IMM_ROTA1_F'] = $chilccare['F']['IMM_ROTA1'];

            // Rotavirus Antigen 2nd dose
            $data['IMM_ROTA2_M'] = $chilccare['M']['IMM_ROTA2'];
            $data['IMM_ROTA2_F'] = $chilccare['F']['IMM_ROTA2'];

            // Rotavirus Antigen 3rd dose
            $data['IMM_ROTA3_M'] = $chilccare['M']['IMM_ROTA3'];
            $data['IMM_ROTA3_F'] = $chilccare['F']['IMM_ROTA3'];

            // Pneumococal Conjugate Antigen 1st dose
            $data['IMM_PCV1_M'] = $chilccare['M']['IMM_PCV1'];
            $data['IMM_PCV1_F'] = $chilccare['F']['IMM_PCV1'];

            // Pneumococal Conjugate Antigen 2nd dose
            $data['IMM_PCV2_M'] = $chilccare['M']['IMM_PCV2'];
            $data['IMM_PCV2_F'] = $chilccare['F']['IMM_PCV2'];

            // Pneumococal Conjugate Antigen 3rd dose
            $data['IMM_PCV3_M'] = $chilccare['M']['IMM_PCV3'];
            $data['IMM_PCV3_F'] = $chilccare['F']['IMM_PCV3'];

            $natality = MaternalReportController::getData('natality',$month,$year,$barangay);
            $pregnancies = $natality['pregnancies'];
            // Total Livebirths
            $data['LBTOT_M'] = $pregnancies['M'];
            $data['LBTOT_F'] = $pregnancies['F'];

            // Infant given complimentary food from 6-8 months
            $data['INF_FOOD_M'] = 0;
            $data['INF_FOOD_F'] = 0;

            // Infant  12-23 months old received Vitamin A
            $data['INF_VITA1223_M'] = $chilccare['M']['INF_VITA1223'];
            $data['INF_VITA1223_F'] = $chilccare['F']['INF_VITA1223'];

            // Infant 24-35 months old received Vitamin A
            $data['INF_VITA2435_M'] = $chilccare['M']['INF_VITA2435'];
            $data['INF_VITA2435_F'] = $chilccare['F']['INF_VITA2435'];

            // Infant 36-47 months old received Vitamin A
            $data['INF_VITA3647_M'] = $chilccare['F']['INF_VITA3647'];
            $data['INF_VITA3647_F'] = $chilccare['F']['INF_VITA3647'];

            // Infant 48-59 months old received Vitamin A
            $data['INF_VITA4859_M'] = $chilccare['F']['INF_VITA4859'];
            $data['INF_VITA4859_F'] = $chilccare['F']['INF_VITA4859'];

            // Infant 2-5 months received Iron
            $data['INF25FE_M'] = $chilccare['M']['INF25FE'];
            $data['INF25FE_F'] = $chilccare['F']['INF25FE'];

            // Infant 6-11 months received Iron
            $data['INF611FE_M'] = $chilccare['M']['INF611FE'];
            $data['INF611FE_F'] = $chilccare['F']['INF611FE'];

            // Infant 12-23 months received Iron
            $data['INF1223FE_M'] = $chilccare['M']['INF1223FE'];
            $data['INF1223FE_F'] = $chilccare['F']['INF1223FE'];

            // Infant 24-35 months received Iron
            $data['INF2435FE_M'] = $chilccare['M']['INF2435FE'];
            $data['INF2435FE_F'] = $chilccare['F']['INF2435FE'];

            // Infant 36-47 months received Iron
            $data['INF3647FE_M'] = $chilccare['M']['INF3647FE'];
            $data['INF3647FE_F'] = $chilccare['F']['INF3647FE'];

            // Infant 48-59 months received Iron
            $data['INF4859FE_M'] = $chilccare['M']['INF4859FE'];
            $data['INF4859FE_F'] = $chilccare['F']['INF4859FE'];

            // Infant 6-11 months received MNP
            $data['INF611MNP_M'] = 0;
            $data['INF611MNP_F'] = 0;

            // Infant 12-23 months received MNP
            $data['INF1223MNP_M'] = 0;
            $data['INF1223MNP_F'] = 0;

            // Children 12-59 months old given de-worming tablet
            $data['CHILD_1259DW_M'] = 0;
            $data['CHILD_1259DW_F'] = 0;

            // Infant 2-6 months old w/ low birth weight given iron
            $data['INF26LBWFE_M'] = $chilccare['M']['INF26FE'];
            $data['INF26LBWFE_F'] = $chilccare['F']['INF26FE'];

            // Anemic Children 6-11 months old seen
            $data['ANECHILD611_M'] = $chilccare_diagnosis_based['M']['ANECHILD611'];
            $data['ANECHILD611_F'] = $chilccare_diagnosis_based['F']['ANECHILD611'];

            // Anemic Children 12-59 months old seen
            $data['ANECHILD1259_M'] = $chilccare_diagnosis_based['M']['ANECHILD1259'];
            $data['ANECHILD1259_F'] = $chilccare_diagnosis_based['F']['ANECHILD1259'];

            // Anemic Children 6-11 months old received full dose of Iron
            $data['ANECHILD611FE_M'] = $chilccare_diagnosis_based['M']['ANECHILD611FE'];
            $data['ANECHILD611FE_F'] = $chilccare_diagnosis_based['F']['ANECHILD611FE'];

            // Anemic Children 12-59 months old received full dose of Iron
            $data['ANECHILD1259FE_M'] = $chilccare_diagnosis_based['M']['ANECHILD611FE'];
            $data['ANECHILD1259FE_F'] = $chilccare_diagnosis_based['F']['ANECHILD611FE'];

            // FinalReports
        }

        set_time_limit($default_limit);

        return $data;
    }

    public function fhsis_demographic_profile($month=NULL,$year=NULL,$barangay=NULL) {
        
        $data = array('TOT_BGY'=>0, 'TOT_BHS'=>0, 'TOT_HC'=>0, 'TOT_HH'=>0);
        $arrWorkforce = NULL;
        if($this->facility!=NULL) {
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['YEAR'] = $year;
            $data['DMonth'] = $month;

            $demographic = ReportsController::getGeoData($year,$data['bgycode']);
            foreach ($demographic as $key => $value) { 
                $data['TOT_BGY'] += $value->barangay;
                $data['TOT_BHS'] += $value->bhs;
                $data['TOT_HC'] += $value->health_centers;
                $data['TOT_HH'] += $value->households; 
            } 
            $workforce = FacilityWorkforce::where('facility_id',$this->facility['facility_id'])->first();
            if($workforce!=NULL) { $workforce->toArray(); $arrWorkforce = (array)json_decode($workforce['workforce']); }
            $data['MD_M'] = is_null($arrWorkforce['doctors_male']) ? 0 : $arrWorkforce['doctors_male'];
            $data['MD_F'] = is_null($arrWorkforce['doctors_female']) ? 0 : $arrWorkforce['doctors_female'];
            $data['DENT_M'] = is_null($arrWorkforce['dentists_male']) ? 0 : $arrWorkforce['dentists_male'];
            $data['DENT_F'] = is_null($arrWorkforce['dentists_female']) ? 0 : $arrWorkforce['dentists_female'];
            $data['PHN_M'] = is_null($arrWorkforce['nurses_male']) ? 0 : $arrWorkforce['nurses_male'];
            $data['PHN_F'] = is_null($arrWorkforce['nurses_female']) ? 0 : $arrWorkforce['nurses_female']; 
            $data['MIDW_M'] = is_null($arrWorkforce['midwives_male']) ? 0 : $arrWorkforce['midwives_male']; 
            $data['MIDW_F'] = is_null($arrWorkforce['midwives_female']) ? 0 : $arrWorkforce['midwives_female']; 
            $data['NUTR_M'] = is_null($arrWorkforce['nutritionists_male']) ? 0 : $arrWorkforce['nutritionists_male']; 
            $data['NUTR_F'] = is_null($arrWorkforce['nutritionists_female']) ? 0 : $arrWorkforce['nutritionists_female']; 
            $data['MEDT_M'] = is_null($arrWorkforce['medical_technologists_male']) ? 0 : $arrWorkforce['medical_technologists_male']; 
            $data['MEDT_F'] = is_null($arrWorkforce['medical_technologists_female']) ? 0 : $arrWorkforce['medical_technologists_female']; 
            $data['SE_M'] = is_null($arrWorkforce['sanitary_engineers_male']) ? 0 : $arrWorkforce['sanitary_engineers_male']; 
            $data['SE_F'] = is_null($arrWorkforce['sanitary_engineers_female']) ? 0 : $arrWorkforce['sanitary_engineers_female']; 
            $data['SI_M'] = is_null($arrWorkforce['sanitary_inspectors_male']) ? 0 : $arrWorkforce['sanitary_inspectors_male']; 
            $data['SI_F'] = is_null($arrWorkforce['sanitary_inspectors_female']) ? 0 : $arrWorkforce['sanitary_inspectors_female']; 
            $data['BHW_M'] = is_null($arrWorkforce['active_barangay_health_workers_male']) ? 0 : $arrWorkforce['active_barangay_health_workers_male']; 
            $data['BHW_F'] = is_null($arrWorkforce['active_barangay_health_workers_female']) ? 0 : $arrWorkforce['active_barangay_health_workers_female']; 


        } 
        return $data; 
    }

    public function fhsis_environmental_health($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array('HH'=>0, 'HHWATER_LEVEL1'=>0, 'HHWATER_LEVEL2'=>0, 'HHWATER_LEVEL3'=>0, 'HHSANTOILET'=>0, 'HHWASTE'=>0, 'HHSANFAC'=>0, 'FOODEST'=>0, 'FOODEST_SAN'=>0, 'FOODHAND'=>0, 'FOODHAND_HC'=>0, 'SALT_TEST'=>0, 'SALT_TESTIOD'=>0);
        if($this->facility!=NULL) {
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['YEAR_ENV'] = $year;
            $data['DMonth'] = $month;

            $demographic = ReportsController::getGeoData($year,$data['bgycode']);
            foreach ($demographic as $key => $value) { 
                $data['HH'] += $value->households;
                $data['HHWATER_LEVEL1'] += $value->households_water_1;
                $data['HHWATER_LEVEL2'] += $value->households_water_2;
                $data['HHWATER_LEVEL3'] += $value->households_water_3;
                $data['HHSANTOILET'] += $value->households_toilet;
                $data['HHWASTE'] += $value->households_solid_waste;
                $data['HHSANFAC'] += $value->households_sanitary;
                $data['FOODEST'] += $value->food_establishments;
                $data['FOODEST_SAN'] += $value->food_establishments_permit;
                $data['FOODHAND'] += $value->food_handlers;
                $data['FOODHAND_HC'] += $value->food_handlers_permit;
                $data['SALT_TEST'] += $value->salt_samples;
                $data['SALT_TESTIOD'] += $value->salt_samples_iodine;
            }
        } 
        return $data;
    }

    public function fhsis_family_planning($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) {
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            $fp = M1FP::fpQuery($month,$year,$barangay);    

            $data['PREV_FS']=$fp['currentMethod']['FSTR/BTL']['CU_begin'];
            $data['PREV_MS']=$fp['currentMethod']['MSTR/VASECTOMY']['CU_begin'];
            $data['PREV_PILLS']=$fp['currentMethod']['PILLS']['CU_begin'];
            $data['PREV_IUD']=$fp['currentMethod']['IUD']['CU_begin'];
            $data['PREV_DMPA']=$fp['currentMethod']['INJ']['CU_begin'];
            $data['PREV_NFPCM']=$fp['currentMethod']['NFP-CM']['CU_begin'];
            $data['PREV_NFPBBT']=$fp['currentMethod']['NFP-BBT']['CU_begin'];
            $data['PREV_NFPLAM']=$fp['currentMethod']['NFP-LAM']['CU_begin'];
            $data['PREV_NFPSDM']=$fp['currentMethod']['NFP-SDM']['CU_begin'];
            $data['PREV_NFPSTM']=$fp['currentMethod']['NFP-STM']['CU_begin'];
            $data['PREV_CONDOM']=$fp['currentMethod']['CON']['CU_begin'];
            // $data['PREV_IMPLANT']=$fp['currentMethod']['IMPLANT']['CU_begin'];
            $data['TNAP_FS'] = 0;
            $data['TNAP_MS'] = 0;
            $data['TNAP_PILLS'] = 0;
            $data['TNAP_IUD'] = 0;
            $data['TNAP_DMPA'] = 0;
            $data['TNAP_NFPCM'] = 0;
            $data['TNAP_NFPBBT'] = 0;
            $data['TNAP_NFPLAM'] = 0;
            $data['TNAP_NFPSDM'] = 0;
            $data['TNAP_NFPSTM'] = 0;
            $data['TNAP_CONDOM'] = 0;
            // $data['TNAP_IMPLANT'] = 0;
            $data['TNA_FS']=$fp['currentMethod']['FSTR/BTL']['NA_begin'];
            $data['TNA_MS']=$fp['currentMethod']['MSTR/VASECTOMY']['NA_begin'];
            $data['TNA_PILLS']=$fp['currentMethod']['PILLS']['NA_begin'];
            $data['TNA_IUD']=$fp['currentMethod']['IUD']['NA_begin'];
            $data['TNA_DMPA']=$fp['currentMethod']['INJ']['NA_begin'];
            $data['TNA_NFPCM']=$fp['currentMethod']['NFP-CM']['NA_begin'];
            $data['TNA_NFPBBT']=$fp['currentMethod']['NFP-BBT']['NA_begin'];
            $data['TNA_NFPLAM']=$fp['currentMethod']['NFP-LAM']['NA_begin'];
            $data['TNA_NFPSDM']=$fp['currentMethod']['NFP-SDM']['NA_begin'];
            $data['TNA_NFPSTM']=$fp['currentMethod']['NFP-STM']['NA_begin'];
            $data['TNA_CONDOM']=$fp['currentMethod']['CON']['NA_begin'];
            // $data['TNA_IMPLANT']=$fp['currentMethod']['IMPLANT']['NA_begin'];
            $data['TOA_FS']=$fp['currentMethod']['FSTR/BTL']['OA'];
            $data['TOA_MS']=$fp['currentMethod']['MSTR/VASECTOMY']['OA'];
            $data['TOA_PILLS']=$fp['currentMethod']['PILLS']['OA'];
            $data['TOA_IUD']=$fp['currentMethod']['IUD']['OA'];
            $data['TOA_DMPA']=$fp['currentMethod']['INJ']['OA'];
            $data['TOA_NFPCM']=$fp['currentMethod']['NFP-CM']['OA'];
            $data['TOA_NFPBBT']=$fp['currentMethod']['NFP-BBT']['OA'];
            $data['TOA_NFPLAM']=$fp['currentMethod']['NFP-LAM']['OA'];
            $data['TOA_NFPSDM']=$fp['currentMethod']['NFP-SDM']['OA'];
            $data['TOA_NFPSTM']=$fp['currentMethod']['NFP-STM']['OA'];
            $data['TOA_CONDOM']=$fp['currentMethod']['CON']['OA'];
            // $data['TOA_IMPLANT']=$fp['currentMethod']['IMPLANT']['OA'];
            $data['TDO_FS']=$fp['previousMethod']['FSTR/BTL']['Dropout'];
            $data['TDO_MS']=$fp['previousMethod']['MSTR/VASECTOMY']['Dropout'];
            $data['TDO_PILLS']=$fp['previousMethod']['PILLS']['Dropout'];
            $data['TDO_IUD']=$fp['previousMethod']['IUD']['Dropout'];
            $data['TDO_DMPA']=$fp['previousMethod']['INJ']['Dropout'];
            $data['TDO_NFPCM']=$fp['previousMethod']['NFP-CM']['Dropout'];
            $data['TDO_NFPBBT']=$fp['previousMethod']['NFP-BBT']['Dropout'];
            $data['TDO_NFPLAM']=$fp['previousMethod']['NFP-LAM']['Dropout'];
            $data['TDO_NFPSDM']=$fp['previousMethod']['NFP-SDM']['Dropout'];
            $data['TDO_NFPSTM']=$fp['previousMethod']['NFP-STM']['Dropout'];
            $data['TDO_CONDOM']=$fp['previousMethod']['CON']['Dropout'];
            // $data['TDO_IMPLANT']=$fp['previousMethod']['IMPLANT']['Dropout'];
            $data['TCU_FS']=$fp['currentMethod']['FSTR/BTL']['CU_end'];
            $data['TCU_MS']=$fp['currentMethod']['MSTR/VASECTOMY']['CU_end'];
            $data['TCU_PILLS']=$fp['currentMethod']['PILLS']['CU_end'];
            $data['TCU_IUD']=$fp['currentMethod']['IUD']['CU_end'];
            $data['TCU_DMPA']=$fp['currentMethod']['INJ']['CU_end'];
            $data['TCU_NFPCM']=$fp['currentMethod']['NFP-CM']['CU_end'];
            $data['TCU_NFPBBT']=$fp['currentMethod']['NFP-BBT']['CU_end'];
            $data['TCU_NFPLAM']=$fp['currentMethod']['NFP-LAM']['CU_end'];
            $data['TCU_NFPSDM']=$fp['currentMethod']['NFP-SDM']['CU_end'];
            $data['TCU_NFPSTM']=$fp['currentMethod']['NFP-STM']['CU_end'];
            $data['TCU_CONDOM']=$fp['currentMethod']['CON']['CU_end'];
            // $data['TCU_IMPLANT']=$fp['currentMethod']['IMPLANT']['CU_end'];

            $data['FinalReports'] = 0;
        }
        
        return $data;
    }

    public function fhsis_natality($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) {
            $natality = MaternalReportController::getData('natality',$month,$year,$barangay);

            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            $pregnancies = $natality['pregnancies'];
            $data['LBTOT_M'] = $pregnancies['M'];
            $data['LBTOT_F'] = $pregnancies['F'];

            $livebirth_weight = $natality['livebirth_weight'];
            $data['BW2500UP_M'] = $livebirth_weight['M']['greater than 2500'];
            $data['BW2500UP_F'] = $livebirth_weight['F']['greater than 2500'];
            $data['BW2500LESS_M'] = $livebirth_weight['M']['less than 2500'];
            $data['BW2500LESS_F'] = $livebirth_weight['F']['less than 2500'];
            $data['BWNK_M'] = $livebirth_weight['M']['unknown'];
            $data['BWNK_F'] = $livebirth_weight['F']['unknown'];
            
            $attendant = $natality['attendant'];
            $data['ATTDOC_M'] = $attendant['M']['001'];
            $data['ATTDOC_F'] = $attendant['F']['001'];
            $data['ATTNURSE_M'] = $attendant['M']['002'];
            $data['ATTNURSE_F'] = $attendant['F']['002'];
            $data['ATTMIDW_M'] = $attendant['M']['003'];
            $data['ATTMIDW_F'] = $attendant['F']['003'];
            $data['ATTTH_M'] = $attendant['M']['004'];
            $data['ATTTH_F'] = $attendant['F']['004'];
            $data['ATTOTHER_M'] = $attendant['M']['999'];
            $data['ATTOTHER_F'] = $attendant['F']['999'];
            $data['ATTUNK_M'] = $attendant['M']['unknown'];
            $data['ATTUNK_F'] = $attendant['F']['unknown'];

            $delivered_type = $natality['delivered_type'];
            $data['TYPEPREG_NORM_M'] = $delivered_type['M']['N'];
            $data['TYPEPREG_NORM_F'] = $delivered_type['F']['N'];
            $data['TYPEPREG_RISK_M'] = $delivered_type['M']['O'];
            $data['TYPEPREG_RISK_F'] = $delivered_type['F']['O'];
            $data['TYPEPREG_NK_M'] = $delivered_type['M']['unknown'];
            $data['TYPEPREG_NK_F'] = $delivered_type['F']['unknown'];
            
            $delivery_place = $natality['delivery_place'];
            $data['NORMDEL_HOME_M'] = $delivery_place['M']['N']['hme'];
            $data['NORMDEL_HOME_F'] = $delivery_place['F']['N']['hme'];
            $data['NORMDEL_HOSP_M'] = $delivery_place['M']['N']['hp'];
            $data['NORMDEL_HOSP_F'] = $delivery_place['F']['N']['hp'];
            $data['NORMDEL_OTH_M'] = $delivery_place['M']['N']['oth'];
            $data['NORMDEL_OTH_F'] = $delivery_place['F']['N']['oth'];
            $data['OTD_HOME_M'] = $delivery_place['M']['O']['hme'];
            $data['OTD_HOME_F'] = $delivery_place['F']['O']['hme'];
            $data['OTD_HOSP_M'] = $delivery_place['M']['O']['hp'];
            $data['OTD_HOSP_F'] = $delivery_place['F']['O']['hp'];
            $data['OTD_OTHER_M'] = $delivery_place['M']['O']['oth'];
            $data['OTD_OTHER_F'] = $delivery_place['F']['O']['oth'];
            
            $data['NOPREG_M'] = $pregnancies['M'];
            $data['NOPREG_F'] = $pregnancies['F'];

            $preg_outcome = $natality['preg_outcome'];
            $data['FDTOT_M'] = $preg_outcome['M']['004'];
            $data['FDTOT_F'] = $preg_outcome['F']['004'];
            $data['ABRTOT_M'] = $preg_outcome['M']['007'];
            $data['ABRTOT_F'] = $preg_outcome['F']['007'];

            $data['NSD_M'] = $delivered_type['M']['N'];
            $data['NSD_F'] = $delivered_type['F']['N'];
            $data['OTD_M'] = $delivered_type['M']['O'];
            $data['OTD_F'] = $delivered_type['F']['O'];

            $data['PREG_TOT'] = $natality['maternal_count'];

            $data['OUT_LB'] = $preg_outcome['M']['001'] + $preg_outcome['F']['001'];
            $data['OUT_FD'] = $preg_outcome['M']['004'] + $preg_outcome['F']['004'];
            $data['OUT_ABR'] = $preg_outcome['M']['007'] + $preg_outcome['F']['007'];

            // FinalReports
        }
        return $data;
    }

    public function fhsis_maternalcare($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) {
            $maternal = MaternalReportController::getData('maternal',$month,$year,$barangay);
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            $data['PC1'] = $maternal['PC1']; //Pregnant women with 4 or more Prenatal visits
            $data['PC2'] = $maternal['PC2']; //Pregnant women givin 2 doses of Tetanus Toxiod
            $data['PC3'] = $maternal['PC3']; //Pregnant women given TT2 plus
            $data['PC4'] = $maternal['PC4']; //Pregnant women given complete iron with folic acid supplementation
            $data['PC5'] = $maternal['PC5'];//Pregnant women given Vitamin A supplementation
            $data['PP1'] = $maternal['PP1']; //Postpartum women given at least 2 postpartum visits
            $data['PP2'] = $maternal['PP2']; //Postpartum women given complete iron supplementation
            $data['PP3'] = $maternal['PP3']; //Postpartum women given Vitamin A supplementation
            $data['PP4'] = $maternal['PP4']; //Postpartum women initiated breastfeeding w/in 1 hour after delivery
            $data['W1049_VITA'] = $maternal['W1049_VITA']; //10-49 years old women given Iron supplementation
            $data['DELIV'] = $maternal['DELIV']; //Number of deliveries
        }
        return $data;
        // FinalReports
    }

    public function fhsis_morbidity($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) {
            $m2=ReportsController::generateReport('count',$month,$year,$barangay);
        
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            foreach ($m2 as $key => $value) {
                $data['diseases'][$key]['ICD10_CODE']=$value['code'];
                $data['diseases'][$key]['UNDER1_M']=$value['details']['0-1']['M'];
                $data['diseases'][$key]['UNDER1_F']=$value['details']['0-1']['F'];
                $data['diseases'][$key]['1_4_M']=$value['details']['1-4']['M'];
                $data['diseases'][$key]['1_4_F']=$value['details']['1-4']['F'];
                $data['diseases'][$key]['5_9_M']=$value['details']['5-9']['M'];
                $data['diseases'][$key]['5_9_F']=$value['details']['5-9']['F'];
                $data['diseases'][$key]['10_14_M']=$value['details']['10-14']['M'];
                $data['diseases'][$key]['10_14_F']=$value['details']['10-14']['F'];
                $data['diseases'][$key]['15_19_M']=$value['details']['15-19']['M'];
                $data['diseases'][$key]['15_19_F']=$value['details']['15-19']['F'];
                $data['diseases'][$key]['20_24_M']=$value['details']['20-24']['M'];;
                $data['diseases'][$key]['20_24_F']=$value['details']['20-24']['F'];
                $data['diseases'][$key]['25_29_M']=$value['details']['25-29']['M'];
                $data['diseases'][$key]['25_29_F']=$value['details']['25-29']['F'];
                $data['diseases'][$key]['30_34_M']=$value['details']['30-34']['M'];
                $data['diseases'][$key]['30_34_F']=$value['details']['30-34']['F'];
                $data['diseases'][$key]['35_39_M']=$value['details']['35-39']['M'];
                $data['diseases'][$key]['35_39_F']=$value['details']['35-39']['F'];
                $data['diseases'][$key]['40_44_M']=$value['details']['40-44']['M'];
                $data['diseases'][$key]['40_44_F']=$value['details']['40-44']['F'];
                $data['diseases'][$key]['45_49_M']=$value['details']['45-49']['M'];
                $data['diseases'][$key]['45_49_F']=$value['details']['45-49']['F'];
                $data['diseases'][$key]['50_54_M']=$value['details']['50-54']['M'];
                $data['diseases'][$key]['50_54_F']=$value['details']['50-54']['F'];
                $data['diseases'][$key]['55_59_M']=$value['details']['55-59']['M'];
                $data['diseases'][$key]['55_59_F']=$value['details']['55-59']['F'];
                $data['diseases'][$key]['60_64_M']=$value['details']['60-64']['M'];
                $data['diseases'][$key]['60_64_F']=$value['details']['60-64']['F'];
                $data['diseases'][$key]['65ABOVE_M']=$value['details']['65-69']['M']+$value['details']['70-100']['M'];
                $data['diseases'][$key]['65ABOVE_F']=$value['details']['65-69']['F']+$value['details']['70-100']['F'];
                $data['diseases'][$key]['FinalReports'] = 0;
            }
        }
        return $data;
    }

    public function fhsis_mortalitybhs($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) {
            $a3=ReportsController::generateReport('mortality', $month, $year, $barangay);            
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            foreach ($a3 as $key => $value) {
                $data['diseases'][$key]['ICD10_CODE']=$value['code'];
                $data['diseases'][$key]['22wks_7days_M'] = 0;
                $data['diseases'][$key]['22wks_7days_F'] = 0;
                $data['diseases'][$key]['8_28days_M'] = 0;
                $data['diseases'][$key]['8_28days_F'] = 0;

                $data['diseases'][$key]['UNDER1_M']=$value['details']['0-1']['M'];
                $data['diseases'][$key]['UNDER1_F']=$value['details']['0-1']['F'];

                $data['diseases'][$key]['1_4_M']=$value['details']['1-4']['M'];
                $data['diseases'][$key]['1_4_F']=$value['details']['1-4']['F'];
                $data['diseases'][$key]['5_9_M']=$value['details']['5-9']['M'];
                $data['diseases'][$key]['5_9_F']=$value['details']['5-9']['F'];
                $data['diseases'][$key]['10_14_M']=$value['details']['10-14']['M'];
                $data['diseases'][$key]['10_14_F']=$value['details']['10-14']['F'];
                $data['diseases'][$key]['15_19_M']=$value['details']['15-19']['M'];
                $data['diseases'][$key]['15_19_F']=$value['details']['15-19']['F'];
                $data['diseases'][$key]['20_24_M']=$value['details']['20-24']['M'];;
                $data['diseases'][$key]['20_24_F']=$value['details']['20-24']['F'];
                $data['diseases'][$key]['25_29_M']=$value['details']['25-29']['M'];
                $data['diseases'][$key]['25_29_F']=$value['details']['25-29']['F'];
                $data['diseases'][$key]['30_34_M']=$value['details']['30-34']['M'];
                $data['diseases'][$key]['30_34_F']=$value['details']['30-34']['F'];
                $data['diseases'][$key]['35_39_M']=$value['details']['35-39']['M'];
                $data['diseases'][$key]['35_39_F']=$value['details']['35-39']['F'];
                $data['diseases'][$key]['40_44_M']=$value['details']['40-44']['M'];
                $data['diseases'][$key]['40_44_F']=$value['details']['40-44']['F'];
                $data['diseases'][$key]['45_49_M']=$value['details']['45-49']['M'];
                $data['diseases'][$key]['45_49_F']=$value['details']['45-49']['F'];
                $data['diseases'][$key]['50_54_M']=$value['details']['50-54']['M'];
                $data['diseases'][$key]['50_54_F']=$value['details']['50-54']['F'];
                $data['diseases'][$key]['55_59_M']=$value['details']['55-59']['M'];
                $data['diseases'][$key]['55_59_F']=$value['details']['55-59']['F'];
                $data['diseases'][$key]['60_64_M']=$value['details']['60-64']['M'];
                $data['diseases'][$key]['60_64_F']=$value['details']['60-64']['F'];
                $data['diseases'][$key]['65ABOVE_M']=$value['details']['65-69']['M']+$value['details']['70-100']['M'];
                $data['diseases'][$key]['65ABOVE_F']=$value['details']['65-69']['F']+$value['details']['70-100']['F'];
                $data['diseases'][$key]['FinalReports'] = 0;

                $data['diseases'][$key]['disease_name'] = $key;
            }
        }
        return $data;
    }


    public function fhsis_mortality($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) {
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            $getMortality = ReportsController::getmortalityCount($month,$year,$barangay);
            $data['TOTDEATH_M'] = $getMortality['TOTDEATH_M'];
            $data['TOTDEATH_F'] = $getMortality['TOTDEATH_F'];
            $data['INFDEATH_M'] = $getMortality['INFDEATH_M'];
            $data['INFDEATH_F'] = $getMortality['INFDEATH_F'];
            $data['MATDEATH_F'] = $getMortality['MATDEATH_F'];
            $data['NEOTET_M'] = $getMortality['NEOTET_M'];
            $data['NEOTET_F'] = $getMortality['NEOTET_F'];
            $data['PRENATDEATH_M'] = $getMortality['PRENATDEATH_M'];
            $data['PRENATDEATH_F'] = $getMortality['PRENATDEATH_F'];;
            $data['DEATHUND5_M'] = $getMortality['DEATHUND5_M'];
            $data['DEATHUND5_F'] = $getMortality['DEATHUND5_F'];
            $data['FD_M'] = $getMortality['FD_M'];
            $data['FD_F'] = $getMortality['FD_F'];
            $data['NEON_M'] = $getMortality['NEON_M'];
            $data['NEON_F'] = $getMortality['NEON_F'];
            $data['FinalReports'] = $getMortality['FinalReports'];
        }
        return $data;
    }


    public function fhsis_population($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array('POP_BGY'=>0, 'NO_HH'=>0);
        if($this->facility!=NULL) {
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['POP_YEAR'] = $year;

            $demographic = ReportsController::getGeoData($year,$data['bgycode']);
            foreach ($demographic as $key => $value) { 
                $data['POP_BGY'] += $value->population;
                $data['NO_HH'] += $value->households;
            }
            $data['END_POP_FIL'] = 0;
            $data['END_POP_MAL'] = 0;
            $data['END_POP_SCH'] = 0;
            $data['FinalReports'] = 0; 
        } 
        return $data; 
    }

    public function fhsis_tuberculosis($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) {
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            $tbdata = TuberculosisReportController::TB_query($month,$year,$barangay);
            $data['TBSYMP_M'] = $tbdata['TBSYMP']['M'];
            $data['TBSYMP_F'] = $tbdata['TBSYMP']['F'];
            $data['SMEARPOS_M'] = $tbdata['SMEARPOS']['M'];
            $data['SMEARPOS_F'] = $tbdata['SMEARPOS']['F'];
            $data['NEWSMEAR_M'] = $tbdata['NEWSMEAR']['M'];
            $data['NEWSMEAR_F'] = $tbdata['NEWSMEAR']['F'];
            $data['NWSMEARPOS_M'] = $tbdata['NWSMEARPOS']['M'];
            $data['NWSMEARPOS_F'] = $tbdata['NWSMEARPOS']['F'];
            $data['SMEARET_M'] = $tbdata['SMEARET']['M'];
            $data['SMEARET_F'] = $tbdata['SMEARET']['F'];
            $data['SMEARET_CURED_M'] = $tbdata['SMEARET_CURED']['M'];
            $data['SMEARET_CURED_F'] = $tbdata['SMEARET_CURED']['F'];
            $data['SMEARET_REL_M'] = $tbdata['SMEARET_REL']['M'];
            $data['SMEARET_REL_F'] = $tbdata['SMEARET_REL']['F'];
            $data['SMEARET_TXF_M'] = $tbdata['SMEARET_TXF']['M'];
            $data['SMEARET_TXF_F'] = $tbdata['SMEARET_TXF']['F'];
            $data['SMEARET_RAD_M'] = $tbdata['SMEARET_RAD']['M'];
            $data['SMEARET_RAD_F'] = $tbdata['SMEARET_RAD']['F'];
            $data['SMEARET_OTB_M'] = $tbdata['SMEARET_OTB']['M'];
            $data['SMEARET_OTB_F'] = $tbdata['SMEARET_OTB']['F'];
            $data['SMEARET_CURED_REL_M'] = $tbdata['SMEARET_CURED_REL']['M'];
            $data['SMEARET_CURED_REL_F'] = $tbdata['SMEARET_CURED_REL']['F'];
            $data['SMEARET_CURED_TXF_M'] = $tbdata['SMEARET_CURED_TXF']['M'];
            $data['SMEARET_CURED_TXF_F'] = $tbdata['SMEARET_CURED_TXF']['F'];
            $data['SMEARET_CURED_RAD_M'] = $tbdata['SMEARET_CURED_RAD']['M'];
            $data['SMEARET_CURED_RAD_F'] = $tbdata['SMEARET_CURED_RAD']['F'];
            $data['TOT_TB_M'] = $tbdata['TOT_TB']['M'];
            $data['TOT_TB_F'] = $tbdata['TOT_TB']['F'];
            $data['TB_ALL_M'] = $tbdata['TB_ALL']['M'];
            $data['TB_ALL_F'] = $tbdata['TB_ALL']['F'];
            $data['FinalReports'] = 0;
        }
        return $data;
    }

    public function fhsis_dentalhealth($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) {
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            $data['OFC_M'] = 0;
            $data['OFC_F'] = 0;
            $data['CHILD_BOHC_M'] = 0;
            $data['CHILD_BOHC_F'] = 0;
            $data['AY_BOHC_M'] = 0;
            $data['AY_BOHC_F'] = 0;
            $data['PREG_BOHC'] = 0;
            $data['OLDPER_BOHC_M'] = 0;
            $data['OLDPER_BOHC_F'] = 0;
            $data['FinalReports'] = 0;
        }
        return $data;
    }

    public function fhsis_filariasis($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) {
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            $data['CASEEXAM_M'] = 0;
            $data['CASEEXAM_F'] = 0;
            $data['CASEPOS_M'] = 0;
            $data['CASEPOS_F'] = 0;
            $data['MF_M'] = 0;
            $data['MF_F'] = 0;
            $data['MDA_M'] = 0;
            $data['MDA_F'] = 0;
            $data['ADENOCASE_M'] = 0;
            $data['ADENOCASE_F'] = 0;
            $data['NOCASE_M'] = 0;
            $data['NOCASE_F'] = 0;
            $data['FinalReports'] = 0;
        }
        return $data;
    }

    public function fhsis_leprosy($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) {
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            $data['LEPCASE_M'] = 0;
            $data['LEPCASE_F'] = 0;
            $data['LEPCASE15_M'] = 0;
            $data['LEPCASE15_F'] = 0;
            $data['NEWDET_M'] = 0;
            $data['NEWDET_F'] = 0;
            $data['NEWDET2_M'] = 0;
            $data['NEWDET2_F'] = 0;
            $data['CASECURED_M'] = 0;
            $data['CASECURED_F'] = 0;
            $data['FinalReports'] = 0;
        }
        return $data;
    }

    public function fhsis_malaria($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) {
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            $data['MALCASELESS5_M'] = 0;
            $data['MALCASELESS5_F'] = 0;
            $data['MALCASEMORE5_M'] = 0;
            $data['MALCASEMORE5_F'] = 0;
            $data['CONFMAL_PFAC_M'] = 0;
            $data['CONFMAL_PFAC_F'] = 0;
            $data['CONFMAL_PFAC_P'] = 0;
            $data['CONFMAL_PVIV_M'] = 0;
            $data['CONFMAL_PVIV_F'] = 0;
            $data['CONFMAL_PVIV_P'] = 0;
            $data['CONFMAL_PMAL_M'] = 0;
            $data['CONFMAL_PMAL_F'] = 0;
            $data['CONFMAL_PMAL_P'] = 0;
            $data['CONFMAL_POVA_M'] = 0;
            $data['CONFMAL_POVA_F'] = 0;
            $data['CONFMAL_POVA_P'] = 0;
            $data['METHSLIDE_M'] = 0;
            $data['METHSLIDE_F'] = 0;
            $data['METHRDT_M'] = 0;
            $data['METHRDT_F'] = 0;
            $data['HH_ATRISK'] = 0;
            $data['HH_ITN'] = 0;
            $data['MALDEATH_M'] = 0;
            $data['MALDEATH_F'] = 0;
            $data['MALCASE_P'] = 0;
            $data['POP_LLIN'] = 0;
            $data['FinalReports'] = 0;
        }
        return $data;
    }

    public function fhsis_schistosomiasis($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) {
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            $data['SYMPCASE_M'] = 0;
            $data['SYMPCASE_F'] = 0;
            $data['POSCASE_M'] = 0;
            $data['POSCASE_F'] = 0;
            $data['CASEX_LOW_M'] = 0;
            $data['CASEX_LOW_F'] = 0;
            $data['CASEX_MED_M'] = 0;
            $data['CASEX_MED_F'] = 0;
            $data['CASEX_HIGH_M'] = 0;
            $data['CASEX_HIGH_F'] = 0;
            $data['CASETX_M'] = 0;
            $data['CASETX_F'] = 0;
            $data['CASEREF_M'] = 0;
            $data['CASEREF_F'] = 0;
            $data['COMPCASE_M'] = 0;
            $data['COMPCASE_F'] = 0;
            $data['FinalReports'] = 0;
        }
        return $data;
    }

    public function fhsis_morbidity_others($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) { 
            $others=ReportsController::generateReport('morbidity_others',$month,$year,$barangay);
            
            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            foreach ($others as $key => $value) {
                $data['diseases'][$key]['ICD10_CODE']=$value['code'];
                $data['diseases'][$key]['UNDER1_M']=$value['details']['0-1']['M'];
                $data['diseases'][$key]['UNDER1_F']=$value['details']['0-1']['F'];
                $data['diseases'][$key]['1_4_M']=$value['details']['1-4']['M'];
                $data['diseases'][$key]['1_4_F']=$value['details']['1-4']['F'];
                $data['diseases'][$key]['5_9_M']=$value['details']['5-9']['M'];
                $data['diseases'][$key]['5_9_F']=$value['details']['5-9']['F'];
                $data['diseases'][$key]['10_14_M']=$value['details']['10-14']['M'];
                $data['diseases'][$key]['10_14_F']=$value['details']['10-14']['F'];
                $data['diseases'][$key]['15_19_M']=$value['details']['15-19']['M'];
                $data['diseases'][$key]['15_19_F']=$value['details']['15-19']['F'];
                $data['diseases'][$key]['20_24_M']=$value['details']['20-24']['M'];;
                $data['diseases'][$key]['20_24_F']=$value['details']['20-24']['F'];
                $data['diseases'][$key]['25_29_M']=$value['details']['25-29']['M'];
                $data['diseases'][$key]['25_29_F']=$value['details']['25-29']['F'];
                $data['diseases'][$key]['30_34_M']=$value['details']['30-34']['M'];
                $data['diseases'][$key]['30_34_F']=$value['details']['30-34']['F'];
                $data['diseases'][$key]['35_39_M']=$value['details']['35-39']['M'];
                $data['diseases'][$key]['35_39_F']=$value['details']['35-39']['F'];
                $data['diseases'][$key]['40_44_M']=$value['details']['40-44']['M'];
                $data['diseases'][$key]['40_44_F']=$value['details']['40-44']['F'];
                $data['diseases'][$key]['45_49_M']=$value['details']['45-49']['M'];
                $data['diseases'][$key]['45_49_F']=$value['details']['45-49']['F'];
                $data['diseases'][$key]['50_54_M']=$value['details']['50-54']['M'];
                $data['diseases'][$key]['50_54_F']=$value['details']['50-54']['F'];
                $data['diseases'][$key]['55_59_M']=$value['details']['55-59']['M'];
                $data['diseases'][$key]['55_59_F']=$value['details']['55-59']['F'];
                $data['diseases'][$key]['60_64_M']=$value['details']['60-64']['M'];
                $data['diseases'][$key]['60_64_F']=$value['details']['60-64']['F'];
                $data['diseases'][$key]['65ABOVE_M']=$value['details']['65-69']['M']+$value['details']['70-100']['M'];
                $data['diseases'][$key]['65ABOVE_F']=$value['details']['65-69']['F']+$value['details']['70-100']['F'];
                $data['diseases'][$key]['FinalReports'] = 0;
            }
        }
        return $data;
    }

    public function fhsis_mortality_others($month=NULL,$year=NULL,$barangay=NULL) {
        $data = array();
        if($this->facility!=NULL) { 
            $others=ReportsController::generateReport('mortality_others',$month,$year,$barangay);

            $data['hfhudcode'] = $this->facility['facility_id'];
            $data['regcode'] = $this->facility->facilityContact['region'];
            $data['provcode'] = $this->facility->facilityContact['province'];
            $data['citycode'] = $this->facility->facilityContact['city'];
            $data['bgycode'] = $barangay;
            $data['DMonth'] = $month;
            $data['DYear'] = $year;

            foreach ($others as $key => $value) {
                $data['diseases'][$key]['ICD10_CODE']=$value['code'];
                $data['diseases'][$key]['22wks_7days_M'] = 0;
                $data['diseases'][$key]['22wks_7days_F'] = 0;
                $data['diseases'][$key]['8_28days_M'] = 0;
                $data['diseases'][$key]['8_28days_F'] = 0;

                $data['diseases'][$key]['UNDER1_M']=$value['details']['0-1']['M'];
                $data['diseases'][$key]['UNDER1_F']=$value['details']['0-1']['F'];

                $data['diseases'][$key]['1_4_M']=$value['details']['1-4']['M'];
                $data['diseases'][$key]['1_4_F']=$value['details']['1-4']['F'];
                $data['diseases'][$key]['5_9_M']=$value['details']['5-9']['M'];
                $data['diseases'][$key]['5_9_F']=$value['details']['5-9']['F'];
                $data['diseases'][$key]['10_14_M']=$value['details']['10-14']['M'];
                $data['diseases'][$key]['10_14_F']=$value['details']['10-14']['F'];
                $data['diseases'][$key]['15_19_M']=$value['details']['15-19']['M'];
                $data['diseases'][$key]['15_19_F']=$value['details']['15-19']['F'];
                $data['diseases'][$key]['20_24_M']=$value['details']['20-24']['M'];;
                $data['diseases'][$key]['20_24_F']=$value['details']['20-24']['F'];
                $data['diseases'][$key]['25_29_M']=$value['details']['25-29']['M'];
                $data['diseases'][$key]['25_29_F']=$value['details']['25-29']['F'];
                $data['diseases'][$key]['30_34_M']=$value['details']['30-34']['M'];
                $data['diseases'][$key]['30_34_F']=$value['details']['30-34']['F'];
                $data['diseases'][$key]['35_39_M']=$value['details']['35-39']['M'];
                $data['diseases'][$key]['35_39_F']=$value['details']['35-39']['F'];
                $data['diseases'][$key]['40_44_M']=$value['details']['40-44']['M'];
                $data['diseases'][$key]['40_44_F']=$value['details']['40-44']['F'];
                $data['diseases'][$key]['45_49_M']=$value['details']['45-49']['M'];
                $data['diseases'][$key]['45_49_F']=$value['details']['45-49']['F'];
                $data['diseases'][$key]['50_54_M']=$value['details']['50-54']['M'];
                $data['diseases'][$key]['50_54_F']=$value['details']['50-54']['F'];
                $data['diseases'][$key]['55_59_M']=$value['details']['55-59']['M'];
                $data['diseases'][$key]['55_59_F']=$value['details']['55-59']['F'];
                $data['diseases'][$key]['60_64_M']=$value['details']['60-64']['M'];
                $data['diseases'][$key]['60_64_F']=$value['details']['60-64']['F'];
                $data['diseases'][$key]['65ABOVE_M']=$value['details']['65-69']['M']+$value['details']['70-100']['M'];
                $data['diseases'][$key]['65ABOVE_F']=$value['details']['65-69']['F']+$value['details']['70-100']['F'];
                $data['diseases'][$key]['FinalReports'] = 0;

                $data['diseases'][$key]['disease_name'] = $key;
            }
        }
        return $data;
    }
}
