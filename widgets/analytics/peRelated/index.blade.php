<div class="box box-primary"><!--Consultations-->
    <div class="box-header">
        <div class="pull-right box-tools">
            <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button> -->
            <button id="peRelatedDatePicker" class="btn btn-primary btn-sm daterange peRelatedDatePicker" data-toggle="tooltip" title="Date range"><i class="fa fa-calendar"></i></button>
        </div>
        <i class="fa fa-stethoscope"></i><h3 class="box-title text-shine-blue peRelatedBoxTitle">PE Related Accident Count</h3>
    </div><!-- /.box-header -->

    <div class="box-body no-padding">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#peRelatedTableCount" data-toggle="tab">Table</a></li>
                <li class=""><a href="#peRelatedBarCount" data-toggle="tab">Bar Chart</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="peRelatedTableCount">
                    <table id="peRelated" class="table table-condensed">
                        <thead>
                            <tr><td colspan="4"><h4 class="peRelatedBoxTitle">PE Related Accident This Month</h4></td></tr>
                            <tr>
                                <th>Injury</th>
                                <th style="text-align:right">Count</th>
                                <th style="text-align:right">Percentage</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sum = 0; ?>
                            @foreach($visits as $visit)
                                <?php $sum += $visit->counter; ?>
                                <tr>
                                    <td>{{ $injuryKind[$visit->kind_of_injury] }}</td>
                                    <td style="text-align:right">{{ $visit->counter }}</td>
                                    <td style="text-align:right">{{ number_format(($visit->counter/$total)*100, 2, '.', '') }} % </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td><strong> Total </strong></td>
                                <td style="text-align:right"><strong> {{ $sum }} </strong></td>
                                <td style="text-align:right"><strong> 100.00 % </strong> </td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- /.tab-pane -->
                <div class="tab-pane" id="peRelatedBarCount">
                    <table id="peRelated" class="table table-condensed">
                        <thead>
                            <tr><td colspan="4"><h4 class="peRelatedBoxTitle">PE Related Injuries This Month</h4></td></tr>
                        </thead>
                    </table>
                    <div class="chart">
                        <canvas id="peRelatedBarChart"></canvas>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
                </div><!-- /.tab-pane -->
            </div>
        </div>
    </div><!-- /.box-body -->

</div><!--/. end consultations-->

<script>
$(document).ready(function() {
    $('#peRelatedDatePicker').daterangepicker(
        {
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1,'days'), moment().subtract(1,'days')],
                'Last 7 Days': [moment().subtract(6,'days'), moment()],
                'Last 30 Days': [moment().subtract(29,'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last 3 Months': [moment().subtract(2,'month').startOf('month'), moment().endOf('month')],
                'Last 6 Months': [moment().subtract(5,'month').startOf('month'), moment().endOf('month')],
                'Last 12 Months': [moment().subtract(11,'month').startOf('month'), moment().endOf('month')]
            },
            startDate: moment(),
            endDate: moment()
        },
        function(start, end, label) {
            $.ajax({
                type: "GET",
                url: "<?php site_url(); ?>enhancedanalytics/getperelatedcount/"+start.format('YYYY-MM-DD')+"/"+end.format('YYYY-MM-DD'),
                beforeSend: function( xhr ) {
                    $('h4.peRelatedBoxTitle').text("Retrieving records starting from "+start.format('MMM DD, YYYY')+ " to "+end.format('MMM DD, YYYY'));
                    $('#peRelated tbody').html("<tr><td colspan='4'><i class='fa fa-spinner fa-pulse fa-fw'></i> Loading. Please wait...</td></tr>");
                }
            })
            .done(function( msg ) {
                $('#peRelated tbody').removeClass('provider_lister_box_loading');
                $('h4.peRelatedBoxTitle').text("PE Related Injuries starting from "+start.format('MMM DD, YYYY')+ " to "+end.format('MMM DD, YYYY'));
                $('#peRelated tbody').html(msg);

                updatepeRelatedBarChart();
            });
    });
    function generateLabelsForBar() {
        var labels = [];
        var table = document.getElementById('peRelated');
        for (var r = 2, n = table.rows.length-1; r < n; r++) {
          if(table.rows[r].cells[0] != null)
          {
            labels.push(table.rows[r].cells[0].innerHTML);
          }
        }       
        return labels;
    }
    function generateDataForBar() {
        var data = [];
        var table = document.getElementById('peRelated');
        for (var r = 2, n = table.rows.length-1; r < n; r++) {
          if(table.rows[r].cells[1] != null)
          {
            data.push(table.rows[r].cells[1].innerHTML);
          }
        }       
        return data;
    }

    function updatepeRelatedBarChart()
    {
        //-------------
        //- BAR CHART -
        //-------------
        var peRelatedBarChartData = {
          labels: generateLabelsForBar(),
          datasets: [
            {
              label: "Consultations",
              fillColor: "rgba(60,141,188,0.9)",
              strokeColor: "rgba(60,141,188,0.8)",
              pointColor: "#3b8bba",
              pointStrokeColor: "rgba(60,141,188,1)",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(60,141,188,1)",
              data: generateDataForBar()
            }
          ]
        };
        $('#peRelatedBarChart').remove(); // this is my <canvas> element
        $('#peRelatedBarCount').append('<canvas id="peRelatedBarChart"></canvas>'); //append canvas
        var peRelatedBarChartCanvas = $("#peRelatedBarChart").get(0).getContext("2d");
        var peRelatedBarChart = new Chart(peRelatedBarChartCanvas);
        var peRelatedBarChartOptions = {
          //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
          scaleBeginAtZero: true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines: true,
          //String - Colour of the grid lines
          scaleGridLineColor: "rgba(0,0,0,.05)",
          //Number - Width of the grid lines
          scaleGridLineWidth: 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines: true,
          //Boolean - If there is a stroke on each bar
          barShowStroke: true,
          //Number - Pixel width of the bar stroke
          barStrokeWidth: 2,
          //Number - Spacing between each of the X value sets
          barValueSpacing: 5,
          //Number - Spacing between data sets within X values
          barDatasetSpacing: 1,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          //Boolean - whether to make the chart responsive
          responsive: true,
          maintainAspectRatio: true
        };

        peRelatedBarChartOptions.datasetFill = false;
        peRelatedBarChart.Bar(peRelatedBarChartData, peRelatedBarChartOptions);
    }

    $('[href=#peRelatedBarCount]').on('shown.bs.tab', function (e) {
        updatepeRelatedBarChart()
    });
});
</script>
