<?php namespace ShineOS\Core\API\Http\Controllers\APIEpiNurse;

use ShineOS\Core\API\Http\Controllers\APIEpiNurseController;

use Illuminate\Routing\Controller;

use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\PatientContacts;
use ShineOS\Core\Patients\Entities\PatientDeathInfo;
use ShineOS\Core\Patients\Entities\PatientEmergencyInfo;

use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser;

use ShineOS\Core\Users\Entities\Users;

use Plugins\EpiNurse\EpiNurseModel;

use Illuminate\Http\JsonResponse;
use Shine\Libraries\IdGenerator;


use Request, Auth, Validator;

class BasicInformationController extends APIEpiNurseController {
	
	public function insertNewPatient($request_data) {
		$data = $request_data['data'];
		$data['facility_id'] = $facility_id = $request_data['facility_id'];
		$data['user_id'] = $user_id = $request_data['user_id'];

		$validator = Validator::make($data, [
			"facility_id" => "required|string",
			"user_id" => "required|string",
			"uuid" => "required|string",
			"first_name" => "required|string",
			"last_name" => "required|string",
			"date_of_birth_in_bs" => "required|string",
			"date_of_birth_in_ad" => "required|string",
			"sex" => "required|integer",
			"house_number" => "required|string",
			"ward_number" => "required|string",
			"municipality" => "required|string",
			"district" => "required|string",
			"zipcode" => "required|string",
			"fathers_name" => "required|string",
			"father_late" => "required|integer",
			"mothers_name" => "required|string",
			"mother_late" => "required|integer",
			"local_guardians_name" => "required|string",
			"contact_of_parent_guardian" => "required|string",
			"family_i_d" => "required|string",
			"type_of_family" => "required|integer",
			"number_male" => "required|integer",
			"number_female" => "required|integer",
			"number_others" => "required|integer",
			"number_male_under5" => "required|integer",
			"number_female_under5" => "required|integer",
			"number_male_above60" => "required|integer",
			"number_female_above60" => "required|integer",
			"number_disabled" => "required|integer",
			"number_pregnant" => "required|integer",
			"number_lactating_mothers" => "required|integer",
			"ethnicity" => "required|string",
			"religion" => "required|string",
			"educational_status" => "required|integer",
			"occupation" => "required|integer",
			"income_agriculture" => "required|integer",
			"income_business" => "required|integer",
			"income_service" => "required|integer",
			"income_labor" => "required|integer",
			"income_remittance" => "required|integer",
			"income_others" => "required|integer",
			"food_sufficiency" => "required|integer"
        ]);

	    if($validator->fails() == false)
	    {
	    	$facilityUser = $this->getFacilityUser($user_id,$facility_id);
	    	if(!$facilityUser) {
	    		return new JsonResponse(array('status'=>'error', 'message'=>'Facility or User not found!'), 400);
	    	}

	    	// Proceed to saving
	    	$patient = array_key_exists('patient_id', $data) ? Patients::where('patient_id',$data['patient_id'])->first() : NULL;

	    	if($patient)
	    	{
	  			$message = "Basic information is successfully updated!";

	    		$patient_id = $data['patient_id'];
	    		// Patient is existing
	    		// $patient_id = $patient->patient_id;
	  			$patient_contact = PatientContacts::where('patient_id',$patient_id)->first();
	  			$epinurse = EpiNurseModel::where('patient_id',$patient_id)->first();
	  			// $fpu = FacilityPatientUser::where('patient_id',$patient_id)->where('facilityuser_id',$facilityUser->facilityuser_id)->first();
	    	}
	    	else
	    	{
	    		$message = "Basic information is successfully added!";
	    		// Patient is new

	    		// Create new Patient object
	    		$patient = new Patients;
	    		$patient->patient_id = $patient_id = IdGenerator::generateId();

	    		// Create new PatientContacts
	    		$patient_contact = new PatientContacts;
	    		$patient_contact->patient_contact_id = IdGenerator::generateId(); 
	    		$patient_contact->patient_id = $patient_id; 

	    		// Create new FacilityPatientUser object
	    		$fpu = new FacilityPatientUser;
	    		$fpu->facilitypatientuser_id = $fpu_id = IdGenerator::generateId();
	    		$fpu->patient_id = $patient_id;
	    		$fpu->facilityuser_id = $facilityUser->facilityuser_id;
	    		$fpu->owner = '1';
	    		$fpu->save();

	    		// Create new EpinurseInfo
	    		$epinurse = new EpiNurseModel;
	    		$epinurse->epinursepersonal_id = IdGenerator::generateId();
	    		$epinurse->patient_id = $patient_id;
	    		$epinurse->uuid = isset($data['uuid']) ? $data['uuid'] : NULL;

		    }

	    	$patient->first_name = array_key_exists('first_name', $data) ? $data['first_name'] : NULL;
	    	$patient->middle_name = array_key_exists('middle_name', $data) ? $data['middle_name'] : "N/A";
	    	$patient->last_name = array_key_exists('last_name', $data) ? $data['last_name'] : NULL;
	    	$patient->birthdate = array_key_exists('date_of_birth_in_ad', $data) ? date("Y-m-d", strtotime($data['date_of_birth_in_ad'])) : NULL;
	    	$patient->birthdate_bs = array_key_exists('date_of_birth_in_bs', $data) ? date("Y-m-d", strtotime($data['date_of_birth_in_bs'])) : NULL;
			$patient->age = array_key_exists('age', $data) ? $data['age'] : NULL;
			if(array_key_exists('sex', $data))
			{	    	
		    	switch ($data['sex']) {
		    		case 0:
		    			$sex = 'M';
		    			break;
	    			case 1:
	    				$sex = 'F';
	    				break;

	    			default:
	    				$sex = NULL;
	    				break;
		    	}
		    }
	    	$patient->gender = $sex;

	    	// Save new attributes for PatientContacts
	    	$patient_contact->house_no = array_key_exists('house_number', $data) ? $data['house_number'] : NULL;
	    	$patient_contact->ward_no = array_key_exists('ward_number', $data) ? $data['ward_number'] : NULL;
	    	$patient_contact->street_address = array_key_exists('street_name', $data) ? $data['street_name'] : NULL;
	    	$patient_contact->district = array_key_exists('district', $data) ? $data['district'] : NULL;
	    	$patient_contact->city = array_key_exists('municipality', $data) ? $data['municipality'] : NULL;
	    	$patient_contact->province = array_key_exists('province', $data) ? $data['province'] : NULL;
	    	$patient_contact->country = array_key_exists('country', $data) ? $data['country'] : NULL;
	    	$patient_contact->zip = array_key_exists('zipcode', $data) ? $data['zipcode'] : NULL;

	    	// Save new attributes for EpiNurseModel
	        $epinurse->marital_status = isset($data['marital_status']) ? $data['marital_status'] : NULL;
	        $epinurse->ethnicity = isset($data['ethnicity']) ? $data['ethnicity'] : NULL;
	        $epinurse->religion = isset($data['religion']) ? $data['religion'] : NULL;
	        $epinurse->educational_status = isset($data['educational_status']) ? $data['educational_status'] : NULL;
	        $epinurse->occupation = isset($data['occupation']) ? $data['occupation'] : NULL;
	        
	        $epinurse->income_agriculture = isset($data['income_agriculture']) ? $data['income_agriculture'] : NULL;
	        $epinurse->income_business = isset($data['income_business']) ? $data['income_business'] : NULL;
	        $epinurse->income_service = isset($data['income_service']) ? $data['income_service'] : NULL;
	        $epinurse->income_labor = isset($data['income_labor']) ? $data['income_labor'] : NULL;
	        $epinurse->income_remittance = isset($data['income_remittance']) ? $data['income_remittance'] : NULL;
	        $epinurse->income_others = isset($data['income_others']) ? $data['income_others'] : NULL;

	        $epinurse->income_specify = isset($data['income_specify']) ? $data['income_specify'] : NULL;
	        $epinurse->food_sufficiency = isset($data['food_sufficiency']) ? $data['food_sufficiency'] : NULL;
	        $epinurse->family_id = isset($data['family_i_d']) ? $data['family_i_d'] : NULL;
	        $epinurse->type_of_family = isset($data['type_of_family']) ? $data['type_of_family'] : NULL;
	        $epinurse->fathers_name = isset($data['fathers_name']) ? $data['fathers_name'] : NULL;
	        $epinurse->father_late = isset($data['father_late']) ? $data['father_late'] : NULL;
	        $epinurse->mothers_name = isset($data['mothers_name']) ? $data['mothers_name'] : NULL;
	        $epinurse->mother_late = isset($data['mother_late']) ? $data['mother_late'] : NULL;
	        $epinurse->local_guardians_name = isset($data['local_guardians_name']) ? $data['local_guardians_name'] : NULL;
	        $epinurse->contact_of_parent_guardian = isset($data['contact_of_parent_guardian']) ? $data['contact_of_parent_guardian'] : NULL;
	        
	        $epinurse->number_male = isset($data['number_male']) ? $data['number_male'] : NULL;
	        $epinurse->number_female = isset($data['number_female']) ? $data['number_female'] : NULL;
	        $epinurse->number_others = isset($data['number_others']) ? $data['number_others'] : NULL;
	        $epinurse->number_male_under5 = isset($data['number_male_under5']) ? $data['number_male_under5'] : NULL;
	        $epinurse->number_female_under5 = isset($data['number_female_under5']) ? $data['number_female_under5'] : NULL;
	        $epinurse->number_male_above60 = isset($data['number_male_above60']) ? $data['number_male_above60'] : NULL;
	        $epinurse->number_female_above60= isset($data['number_female_above60']) ? $data['number_female_above60'] : NULL;
	        $epinurse->number_disabled = isset($data['number_disabled']) ? $data['number_disabled'] : NULL;
	        $epinurse->number_pregnant = isset($data['number_pregnant']) ? $data['number_pregnant'] : NULL;
	        $epinurse->number_lactating_mothers = isset($data['number_lactating_mothers']) ? $data['number_lactating_mothers'] : NULL;

	    	// Perform save
	    	$patient->save();
	    	$patient_contact->save();
	    	$epinurse->save();

	    	// $saved_patient = Patients::with('patientContact','epinurseInfo')->where('patient_id',$patient_id)->first();

	    	return new JsonResponse(array('status'=>'success', 'patient_id'=>$patient_id,'message'=>$message,'data'=>$request_data), 200);
	    }
	    else
	    {
	    	// return $this->json_response('error',$validator->errors()->all(),400);
	    	return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
	    }
	}
}