<?php namespace ShineOS\Core\API\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Users\Entities\Users;
use ShineOS\Core\Patients\Entities\PatientMonitoring;
use ShineOS\Core\LOV\Http\Controllers\LOVController as LOVController;
use ShineOS\Core\Patients\Entities\PatientContacts;
use ShineOS\Core\Patients\Entities\PatientDeathInfo;
use ShineOS\Core\Patients\Entities\PatientEmergencyInfo;
use ShineOS\Core\Reminders\Entities\Reminders;
use ShineOS\Core\Reminders\Entities\ReminderMessage;
use ShineOS\Core\Referrals\Entities\Referrals;
use ShineOS\Core\LOV\Entities\LovReferralReasons;
use Shine\Libraries\FacilityHelper;
use Illuminate\Http\JsonResponse;
use Shine\Libraries\IdGenerator;

use Shine\Repositories\Eloquent\PatientRepository as PatientRepository;
use Shine\Repositories\Eloquent\FacilityRepository as FacilityRepository;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Eloquent\UserRepository as UserRepository;

use ShineOS\Core\Facilities\Entities\FacilityPatientUser;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices; 
use ShineOS\Core\Healthcareservices\Entities\GeneralConsultation; 
use ShineOS\Core\Healthcareservices\Entities\VitalsPhysical; 
use ShineOS\Core\Healthcareservices\Entities\Diagnosis; 
use ShineOS\Core\Healthcareservices\Entities\Disposition;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrder;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderLabExam;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderPrescription;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderProcedure;
use ShineOS\Core\Healthcareservices\Entities\Addendum;
use ShineOS\Core\LOV\Entities\LovLaboratories;
use Plugins\EpiNurse\EpiNurseModel;
use Modules\SchoolAssessment\Entities\SchoolAssessment;
use Plugins\HealthAssessment\HealthAssessmentModel;
use Plugins\CommunityNursing\CommunityNursingModel;
use Plugins\MotherAndChild\MotherAndChildModel;
use ShineOS\Core\API\Entities\EpiNursePhotos;

use ShineOS\Core\API\Http\Controllers\APIEpiNurse\BasicInformationController;
use ShineOS\Core\API\Http\Controllers\APIEpiNurse\HealthAssessmentController;
use ShineOS\Core\API\Http\Controllers\APIEpiNurse\CommunityNursingController;
use ShineOS\Core\API\Http\Controllers\APIEpiNurse\MotherAndChildController;
use ShineOS\Core\API\Http\Controllers\APIEpiNurse\SchoolAssessmentController;
use ShineOS\Core\API\Http\Controllers\APIEpiNurse\NurseRegistryController;

use Request, Auth, Validator, DateTime;

class APIEpiNurseController extends Controller {
	
	public function index() {
		return response(json_encode(array('message'=>'Running!')), 200);
	}

	public function uploadphoto() {
		$request_data = Request::all();

		$validator = Validator::make($request_data ,[
			"json" => "required",
			"photo" => "required"
        ]);

	    if($validator->fails() == false)
	    {
			$filename = $request_data['photo']->getClientOriginalName();
	    	$implodefile = explode('_', $filename);
	    	if(count($implodefile) < 3)
	    	{
	    		return new JsonResponse(array('status'=>'error','message'=>'Incorrect format for specified filename'), 400);
	    	}
	    	$image_uuid = explode('.', $implodefile[2])[0];

	    	$directory = public_path().'/public/uploads/epinurse/photos';
	    	$request_data['photo']->move($directory,$filename);
	    	// file_put_contents($uploaded, $request_data['photo']);

	    	if(file_exists($directory.'/'.$filename))
	    	{
				$photo = new EpiNursePhotos;
				$photo->epinursephotos_id = IdGenerator::generateId();
				$photo->image_uuid = $image_uuid;
				$photo->report_type = $implodefile[0];
				$photo->report_uuid = $implodefile[1];
				$photo->json = $request_data['json'];
				$photo->filename = $filename;

				if($photo->save())
				{
					return new JsonResponse(array('status'=>'success','message'=>'Photo has been successfully uploaded.'), 200);
				}
				else
				{
					return new JsonResponse(array('status'=>'error','message'=>'Photo upload failed'), 400);
				}
			}
			else
			{
				return new JsonResponse(array('status'=>'error','message'=>'Photo upload failed'), 400);
			}
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}	
	}

	public function submitdata() {
		$request_data = Request::all();
		$type = $request_data['type'];
		switch ($type) {
			case 'BasicInformation':
				$basicinfo = new BasicInformationController;
				return $basicinfo->insertNewPatient($request_data);
				break;
			case 'StudentHealthAssessment':
				$healthassessment = new HealthAssessmentController;
				return $healthassessment->insertHealthAssessment($request_data,'Student');
			break;
			case 'EmployeeHealthAssessment':
				$healthassessment = new HealthAssessmentController;
				return $healthassessment->insertHealthAssessment($request_data,'Employee');
			break;
			case 'CommunityNursing':
				$communitynursing = new CommunityNursingController;
				return $communitynursing->insertCommunityNursing($request_data);
			break;
			case 'MotherAndChild':
				$motherandchild = new MotherAndChildController;
				return $motherandchild->insertMotherAndChild($request_data);
			break;
			case 'SchoolHealthAssessment':
				$schoolassessment = new SchoolAssessmentController;
				return $schoolassessment->insertSchoolHealth($request_data);
			break;
			case 'NurseRegistration':
				$nurseregistry = new NurseRegistryController;
				return $nurseregistry->insertNurse($request_data);
			break;
			

			default:
				return new JsonResponse(array('status'=>'error', 'message'=>'Form type not found'), 400);
				break;
		}
	}

	public function getschools() {
		$request = Request::all();

		$validator = Validator::make($request, [
			"facility_id" => "required|string"
        ]);

	    if($validator->fails() == false)
	    {
			$facility_id = $request['facility_id'];
			$schools = SchoolAssessment::where('facility_id',$facility_id)->orderBy('school_name',"ASC")->get()->pluck('school_name');

			if($schools)
			{
				return new JsonResponse(array('status'=>'success','data'=>$schools), 200);
			}
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}		
	}

	public function getpatients(FacilityRepository $FacilityRepository) {
		$data = Request::all();
		if(array_key_exists('facility_id',$data)) { 
			$id = $data['facility_id'];
			$last_sync = isset($data['last_sync']) ? $data['last_sync'] : NULL;
			$facilities = json_decode($FacilityRepository->findByFacilityID($id));

			$now = new DateTime;
			if($facilities) {
				$result = $this->patientsResult($id,$last_sync);
				return new JsonResponse(array('status'=>'success','server_datetime'=> $now->format('Y-m-d H:i:s'),'data'=>$result), 200);
			} else {
				return new JsonResponse(array('status'=>'error', 'message'=>'Incorrect facility id'), 400);
			} 
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>'Facility id required'), 400);
		}
	}

	public function getpatientCount(FacilityRepository $FacilityRepository) {
		$data = Request::all();
		if(array_key_exists('facility_id',$data)) { 
			$id = $data['facility_id'];
			$last_sync = isset($data['last_sync']) ? $data['last_sync'] : NULL;
			$facilities = json_decode($FacilityRepository->findByFacilityID($id));

			$now = new DateTime;
			if($facilities) {
				$result = $this->patientsResult($id,$last_sync);
				$countPatient = count($result);
				return new JsonResponse(array('status'=>'success','server_datetime'=> $now->format('Y-m-d H:i:s'),'data'=>['count'=>$countPatient]), 200);
			} else {
				return new JsonResponse(array('status'=>'error', 'message'=>'Incorrect facility id'), 400);
			} 
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>'Facility id required'), 400);
		}
	}

	public function patientsResult($facility_id,$last_sync = NULL) {
		$last_update = $last_sync ? new DateTime($last_sync) : NULL;
		$result = Patients::with('patientContact','epinurseInfo')
		                	->whereHas('facilityUser',
		                		function($query) use ($facility_id) {
		                    		$query->where('facility_id', '=', $facility_id);
		                	})
		                	->where( function($q) use ($last_update) {
		                		if($last_update)
		                		{
		                			$q->where('updated_at', '>=', $last_update);
		                		}
		                	})
		                	->orderBy('updated_at', 'DESC')
		             		->get();

		$toReturn = array();
		if($result) {
			foreach ($result as $value) {
				if($value->epinurseInfo)
				{
					$basicinfo = $value->epinurseInfo->toArray();
					$basicinfo['first_name'] = $value->first_name;
					$basicinfo['middle_name'] = $value->middle_name;
					$basicinfo['last_name'] = $value->last_name;
					$basicinfo['date_of_birth_in_ad'] = date("m/d/Y", strtotime($value->birthdate));
					$basicinfo['date_of_birth_in_bs'] = date("m/d/Y", strtotime($value->birthdate_bs));
					$basicinfo['age'] = $value->age;
					$basicinfo['house_number'] = $value->patientContact->house_no;
					$basicinfo['ward_number'] = $value->patientContact->ward_no;
					$basicinfo['municipality'] = $value->patientContact->city;
					$basicinfo['district'] = $value->patientContact->district;
					$basicinfo['country'] = $value->patientContact->country;
					$basicinfo['zipcode'] = $value->patientContact->zip;
					$basicinfo['family_i_d'] = $basicinfo['family_id'];
					unset($basicinfo['family_id']);
			    	switch ($value->gender) {
			    		case 'M':
			    			$basicinfo['sex'] = 0;
			    			break;
		    			case 'F':
		    				$basicinfo['sex'] = 1;
		    				break;

		    			default:
		    				$sex = NULL;
		    				break;
			    	}

					$toReturn[] = $basicinfo;
				}
			}
		}
				
        return $toReturn;		
	}

	public function getFacilityUser($user_id,$facility_id)
	{
		$facilityUser = FacilityUser::where('user_id',$user_id)
							->where('facility_id',$facility_id)
							->first();
		if($facilityUser)
		{
			return $facilityUser;
		}
		
		return false;
	}

	public function insertNewHealthcareService($data,$hs_id,$type){

		$facilityUser = $this->getFacilityUser($data['user_id'],$data['facility_id']);
		$facilityPatientUser = FacilityPatientUser::where('facilityuser_id',$facilityUser->facilityuser_id)->where('patient_id',$data['patient_id'])->first();

		$healthcare = new Healthcareservices;
		$healthcare->healthcareservice_id = $hs_id;
		$healthcare->facilitypatientuser_id = $facilityPatientUser->facilitypatientuser_id;
		$healthcare->healthcareservicetype_id = $type;
		$healthcare->consultation_type = 'CONSU';
		$healthcare->encounter_type = 'O';
		$healthcare->encounter_datetime = date("Y-m-d H:i:s");
		$healthcare->seen_by = $facilityUser->facilityuser_id;

		if($healthcare->save())
		{
			return $healthcare;
		}
		return false;
	}

	public function insertNewVitals($data,$hs_id) {

		$vitals = VitalsPhysical::where('healthcareservice_id',$hs_id)->first();

		if(!$vitals)
		{
			$vitals = new VitalsPhysical;
			$vitals->vitalphysical_id = IdGenerator::generateId();
			$vitals->healthcareservice_id = $hs_id;
		}
		$vitals->temperature = isset($data['temperature']) ? $data['temperature'] : NULL;
		$vitals->bloodpressure_systolic = isset($data['systolic']) ? $data['systolic'] : NULL;
		$vitals->bloodpressure_diastolic = isset($data['diastolic']) ? $data['diastolic'] : NULL;
		$vitals->height = isset($data['height_cm']) ? $data['height_cm'] : NULL;
		$vitals->weight = isset($data['weight_kg']) ? $data['weight_kg'] : NULL; 

		if($vitals->save())
		{
			return $vitals;
		}
		return false;
	}
}