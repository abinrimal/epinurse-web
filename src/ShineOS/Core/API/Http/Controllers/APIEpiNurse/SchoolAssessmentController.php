<?php namespace ShineOS\Core\API\Http\Controllers\APIEpiNurse;

use Illuminate\Routing\Controller;
use ShineOS\Core\API\Http\Controllers\APIEpiNurseController;

use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\PatientContacts;
use ShineOS\Core\Patients\Entities\PatientDeathInfo;
use ShineOS\Core\Patients\Entities\PatientEmergencyInfo;

use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser;

use Modules\SchoolAssessment\Entities\SchoolAssessment;

use ShineOS\Core\Users\Entities\Users;
use Illuminate\Http\JsonResponse;
use Shine\Libraries\IdGenerator;


use Request, Auth, Validator;

class SchoolAssessmentController extends APIEpiNurseController {

	public function insertSchoolHealth($request_data) {
		$data = $request_data['data'];
		$data['facility_id'] = $facility_id = $request_data['facility_id'];
		$data['user_id'] = $user_id = $request_data['user_id'];

		$validator = Validator::make($data, [
			"facility_id" => "required|string",
			"user_id" => "required|string",
			"uuid"	 => "required",
			"school" => "required",
			"contact_person" => "required",
			"contact_position" => "required",
			"activities_student_health_assessment" => "required",
			"activities_staff_members_health_assessment" => "required",
			"activities_sanitation_class" => "required",
			"activities_diet_class" => "required",
			"distance_of_school_km" => "required",
			"ecd_present" => "required",
			"ecd_teacher_present" => "required",
			"classroom_facilities" => "required",
			"seating_arrangements" => "required",
			"appr_play_materials" => "required",
			"colors_illustrations" => "required",
			"toilet_handwashing" => "required",
			"collision_avoidance" => "required",
			"nonslip_floors" => "required",
			"lighting_facilities" => "required",
			"has_playground" => "required",
			"space_at_eca_facility" => "required",
			"eca_dancing" => "required",
			"eca_singing" => "required",
			"eca_running" => "required",
			"eca_football" => "required",
			"eca_volleyball" => "required",
			"eca_basketball" => "required",
			"eca_pingpong" => "required",
			"eca_badminton" => "required",
			"eca_martialarts" => "required",
			"eca_others" => "required",
			"first_aid_box_present" => "required",
			"first_aid_box_stocked" => "required",
			"num_students_covered_by_first_aid" => "required",
			"type_water_supplied" => "required",
			"water_outlet_availability" => "required",
			"school_provides_food_opt" => "required",
			"school_provides_food_breakfast" => "required",
			"school_provides_food_lunch" => "required",
			"school_provides_food_snacks" => "required",
			"school_provides_food_dinner" => "required",
			// "food_hygienic_safe" => "required",
			// "meal_balanced" => "required",
			"food_can_be_brought_from_home" => "required",
			"junk_food_allowed" => "required",
			"toil_has_running_water" => "required",
			"toil_has_cleaning_water" => "required",
			"toil_has_soap" => "required",
			"toil_has_sep_gender" => "required",
			"toil_has_sep_student_staff" => "required",
			"toil_has_trash" => "required",
			"toil_has_adequate_toilets"	 => "required",
			"toil_has_toilet_rating" => "required",
			"school_at" => "required",
			"building_type" => "required",
			"age_of_building" => "required",
			"building_height" => "required",
			"water_damage" => "required",
			"firefighting_instrument" => "required",
			"earthquake_hazards" => "required",
			"earthquake_vulnerabilities" => "required",
			// "earthquake_risk_score" => "required",
			"landslide_hazards" => "required",
			"landslide_vulnerabilities" => "required",
			// "landslide_risk_score" => "required",
			"wildlife_attack_hazards" => "required",
			"wildlife_attack_vulnerabilities" => "required",
			// "wildlife_attack_risk_score" => "required",
			"fire_hazards" => "required",
			"fire_vulnerabilities" => "required",
			// "fire_risk_score" => "required",
			"pest_diseases_hazards" => "required",
			"pest_diseases_vulnerabilities" => "required",
			// "pest_diseases_risk_score" => "required",
			"flood_hazards" => "required",
			"flood_vulnerabilities" => "required",
			// "flood_risk_score" => "required",
			"windstorm_hazards" => "required",
			"windstorm_vulnerabilities" => "required",
			// "windstorm_risk_score" => "required",
			"drought_hazards" => "required",
			"drought_vulnerabilities" => "required",
			// "drought_risk_score" => "required",
			"hailstorm_hazards" => "required",
			"hailstorm_vulnerabilities" => "required",
			// "hailstorm_risk_score" => "required",
			"soil_erosion_hazards" => "required",
			"soil_erosion_vulnerabilities" => "required",
			// "soil_erosion_risk_score" => "required",
			"epidemics_hazards" => "required",
			"epidemics_vulnerabilities" => "required",
			// "epidemics_risk_score" => "required",
			"disaster_drill" => "required",
			"disaster_preparedness_measures" => "required",
			"earthquake_resistant" => "required"
        ]);

	    if($validator->fails() == false)
	    {
	    	$facilityUser = $this->getFacilityUser($data['user_id'],$data['facility_id']);
	    	if(!$facilityUser) {
	    		return new JsonResponse(array('status'=>'error', 'message'=>'Facility not found!'), 400);
	    	}

	    	// Check if existing
	    	$school = SchoolAssessment::where('uuid',$data['uuid'])->first();

	    	$message = "School has been successfully updated!";
	    	if(!$school)
	    	{
	    		$message = "New school has been successfully added!";

		    	$school = new SchoolAssessment;
	    		$school->school_id = IdGenerator::generateId();
				$school->facility_id = $data['facility_id'];
				$school->uuid = isset($data['uuid']) ? $data['uuid'] : NULL;
	    	}

	    	// Proceed to saving
			$school->school_name = isset($data['school']) ? $data['school'] : NULL;
			$school->contact_person = isset($data['contact_person']) ? $data['contact_person'] : NULL;
			$school->contact_person_position = isset($data['contact_position']) ? $data['contact_position'] : NULL;
			$school->activities_student_health_assessment = isset($data['activities_student_health_assessment']) ? $data['activities_student_health_assessment'] : NULL;
			$school->activities_staff_members_health_assessment = isset($data['activities_staff_members_health_assessment']) ? $data['activities_staff_members_health_assessment'] : NULL;
			$school->activities_sanitation_class = isset($data['activities_sanitation_class']) ? $data['activities_sanitation_class'] : NULL;
			$school->activities_diet_class = isset($data['activities_diet_class']) ? $data['activities_diet_class'] : NULL;
			$school->school_distance = isset($data['distance_of_school_km']) && $data['distance_of_school_km'] != '' ? $data['distance_of_school_km'] : NULL;
			$school->ecd_program = isset($data['ecd_present']) ? $data['ecd_present'] : NULL;
			$school->ecd_teacher = isset($data['ecd_teacher_present']) ? $data['ecd_teacher_present'] : NULL;

			$school->ecd_classroom = isset($data['classroom_facilities']) ? $data['classroom_facilities'] : NULL;
			$school->ecd_toilet = isset($data['toilet_handwashing']) ? $data['toilet_handwashing'] : NULL;
			$school->ecd_seat_arrangement = isset($data['seating_arrangements']) ? $data['seating_arrangements'] : NULL;
			$school->ecd_collision_avoidance = isset($data['collision_avoidance']) ? $data['collision_avoidance'] : NULL;
			$school->ecd_appr_play_material = isset($data['appr_play_materials']) ? $data['appr_play_materials'] : NULL;
			$school->ecd_non_slip_floors = isset($data['nonslip_floors']) ? $data['nonslip_floors'] : NULL;
			$school->ecd_colors = isset($data['colors_illustrations']) ? $data['colors_illustrations'] : NULL;
			$school->ecd_lighting = isset($data['lighting_facilities']) ? $data['lighting_facilities'] : NULL;

			$school->eca_playground = isset($data['has_playground']) ? $data['has_playground'] : NULL;
			$school->eca_spacing = isset($data['space_at_eca_facility']) ? $data['space_at_eca_facility'] : NULL;

			$school->eca_dancing = isset($data['eca_dancing']) ? $data['eca_dancing'] : NULL;
			$school->eca_singing = isset($data['eca_singing']) ? $data['eca_singing'] : NULL;
			$school->eca_running = isset($data['eca_running']) ? $data['eca_running'] : NULL;
			$school->eca_football = isset($data['eca_football']) ? $data['eca_football'] : NULL;
			$school->eca_volleyball = isset($data['eca_volleyball']) ? $data['eca_volleyball'] : NULL;
			$school->eca_basketball = isset($data['eca_basketball']) ? $data['eca_basketball'] : NULL;
			$school->eca_pingpong = isset($data['eca_pingpong']) ? $data['eca_pingpong'] : NULL;
			$school->eca_badminton = isset($data['eca_badminton']) ? $data['eca_badminton'] : NULL;
			$school->eca_martialarts = isset($data['eca_martialarts']) ? $data['eca_martialarts'] : NULL;
			$school->eca_oth = isset($data['eca_others']) ? $data['eca_others'] : NULL;
			$school->eca_are_available_oth_spec = isset($data['eca_specify']) ? $data['eca_specify'] : NULL;

			$school->first_aid_box = isset($data['first_aid_box_present']) ? $data['first_aid_box_present'] : NULL;
			$school->first_aid_adequacy = isset($data['first_aid_box_stocked']) ? $data['first_aid_box_stocked'] : NULL;
			$school->first_aid_students = isset($data['num_students_covered_by_first_aid']) && $data['num_students_covered_by_first_aid'] != '' ? $data['num_students_covered_by_first_aid'] : NULL;

			$school->water_type = isset($data['type_water_supplied']) ? $data['type_water_supplied'] : NULL;
			$school->water_availability = isset($data['water_outlet_availability']) ? $data['water_outlet_availability'] : NULL;
			$school->nutrition_provided_food = isset($data['school_provides_food_opt']) ? $data['school_provides_food_opt'] : NULL;

			$school->nutrition_type_meal_breakfast = isset($data['school_proviced_food_breakfast']) ? $data['school_proviced_food_breakfast'] : NULL;
			$school->nutrition_type_meal_lunch = isset($data['school_proviced_food_lunch']) ? $data['school_proviced_food_lunch'] : NULL;
			$school->nutrition_type_meal_snacks = isset($data['school_proviced_food_snacks']) ? $data['school_proviced_food_snacks'] : NULL;
			$school->nutrition_type_meal_dinner = isset($data['school_proviced_food_dinner']) ? $data['school_proviced_food_dinner'] : NULL;

			$school->food_hygiene = isset($data['food_hygienic_safe']) ? $data['food_hygienic_safe'] : NULL;
			$school->food_balanced = isset($data['meal_balanced']) ? $data['meal_balanced'] : NULL;
			$school->food_from_home = isset($data['food_can_be_brought_from_home']) ? $data['food_can_be_brought_from_home'] : NULL;
			$school->food_junk = isset($data['junk_food_allowed']) ? $data['junk_food_allowed'] : NULL;

			$school->toil_has_running_water = isset($data['toil_has_running_water']) ? $data['toil_has_running_water'] : NULL;
			$school->toil_has_cleaning_water = isset($data['toil_has_cleaning_water']) ? $data['toil_has_cleaning_water'] : NULL;
			$school->toil_has_soap = isset($data['toil_has_soap']) ? $data['toil_has_soap'] : NULL;
			$school->toil_has_sep_gender = isset($data['toil_has_sep_gender']) ? $data['toil_has_sep_gender'] : NULL;
			$school->toil_has_sep_student_staff = isset($data['toil_has_sep_student_staff']) ? $data['toil_has_sep_student_staff'] : NULL;
			$school->toil_has_trash = isset($data['toil_has_trash']) ? $data['toil_has_trash'] : NULL;
			$school->toil_has_adequate_toilets = isset($data['toil_has_adequate_toilets']) ? $data['toil_has_adequate_toilets'] : NULL;

			$school->toilet_clean_range = isset($data['toil_has_toilet_rating']) ? $data['toil_has_toilet_rating'] : NULL;

			$school->school_location = isset($data['school_at']) ? $data['school_at'] : NULL;
			$school->school_location_oth_spec = isset($data['school_at_specify']) ? $data['school_at_specify'] : NULL;
			$school->building_type = isset($data['building_type']) ? $data['building_type'] : NULL;
			$school->age_building = isset($data['age_of_building']) && $data['age_of_building'] != '' ? $data['age_of_building'] : NULL;
			$school->height_building = isset($data['building_height']) ? $data['building_height'] : NULL;
			$school->water_damage = isset($data['water_damage']) ? $data['water_damage'] : NULL;
			$school->fire_fighting_instrument = isset($data['firefighting_instrument']) ? $data['firefighting_instrument'] : NULL;

			$school->disaster_drill = isset($data['disaster_drill']) ? $data['disaster_drill'] : NULL;
			$school->dd_how_often = isset($data['disaster_drill_specify']) ? $data['disaster_drill_specify'] : NULL;
			$school->disaster_preparedness = isset($data['disaster_preparedness_measures']) ? $data['disaster_preparedness_measures'] : NULL;
			$school->dp_specify = isset($data['disaster_preparedness_measures_specify']) ? $data['disaster_preparedness_measures_specify'] : NULL;
			$school->earthquake_resistant = isset($data['earthquake_resistant']) ? $data['earthquake_resistant'] : NULL;

			$school->earthquake_hazards = isset($data['earthquake_hazards']) ? $data['earthquake_hazards'] : NULL;
			$school->earthquake_vulnerabilities = isset($data['earthquake_vulnerabilities']) ? $data['earthquake_vulnerabilities'] : NULL;
			$school->earthquake_risk_score_one = isset($data['earthquake_risk_score']) ? $data['earthquake_risk_score'] : NULL;
			$school->earthquake_risk_score_two = isset($data['earthquake_risk_score2']) ? $data['earthquake_risk_score2'] : NULL;
			$school->landslide_hazards = isset($data['landslide_hazards']) ? $data['landslide_hazards'] : NULL;
			$school->landslide_vulnerabilities = isset($data['landslide_vulnerabilities']) ? $data['landslide_vulnerabilities'] : NULL;
			$school->landslide_risk_score_one = isset($data['landslide_risk_score']) ? $data['landslide_risk_score'] : NULL;
			$school->landslide_risk_score_two = isset($data['landslide_risk_score2']) ? $data['landslide_risk_score2'] : NULL;
			$school->wildlife_attack_hazards = isset($data['wildlife_attack_hazards']) ? $data['wildlife_attack_hazards'] : NULL;
			$school->wildlife_attack_vulnerabilities = isset($data['wildlife_attack_vulnerabilities']) ? $data['wildlife_attack_vulnerabilities'] : NULL;
			$school->wildlife_attack_risk_score_one = isset($data['wildlife_attack_risk_score']) ? $data['wildlife_attack_risk_score'] : NULL;
			$school->wildlife_attack_risk_score_two = isset($data['wildlife_attack_risk_score2']) ? $data['wildlife_attack_risk_score2'] : NULL;
			$school->fire_hazards = isset($data['fire_hazards']) ? $data['fire_hazards'] : NULL;
			$school->fire_vulnerabilities = isset($data['fire_vulnerabilities']) ? $data['fire_vulnerabilities'] : NULL;
			$school->fire_risk_score_one = isset($data['fire_risk_score']) ? $data['fire_risk_score'] : NULL;
			$school->fire_risk_score_two = isset($data['fire_risk_score2']) ? $data['fire_risk_score2'] : NULL;
			$school->pest_diseases_hazards = isset($data['pest_diseases_hazards']) ? $data['pest_diseases_hazards'] : NULL;
			$school->pest_diseases_vulnerabilities = isset($data['pest_diseases_vulnerabilities']) ? $data['pest_diseases_vulnerabilities'] : NULL;
			$school->pest_diseases_risk_score_one = isset($data['pest_diseases_risk_score']) ? $data['pest_diseases_risk_score'] : NULL;
			$school->pest_diseases_risk_score_two = isset($data['pest_diseases_risk_score2']) ? $data['pest_diseases_risk_score2'] : NULL;
			$school->flood_hazards = isset($data['flood_hazards']) ? $data['flood_hazards'] : NULL;
			$school->flood_vulnerabilities = isset($data['flood_vulnerabilities']) ? $data['flood_vulnerabilities'] : NULL;
			$school->flood_risk_score_one = isset($data['flood_risk_score']) ? $data['flood_risk_score'] : NULL;
			$school->flood_risk_score_two = isset($data['flood_risk_score2']) ? $data['flood_risk_score2'] : NULL;
			$school->windstorm_hazards = isset($data['windstorm_hazards']) ? $data['windstorm_hazards'] : NULL;
			$school->windstorm_vulnerabilities = isset($data['windstorm_vulnerabilities']) ? $data['windstorm_vulnerabilities'] : NULL;
			$school->windstorm_risk_score_one = isset($data['windstorm_risk_score']) ? $data['windstorm_risk_score'] : NULL;
			$school->windstorm_risk_score_two = isset($data['windstorm_risk_score2']) ? $data['windstorm_risk_score2'] : NULL;
			$school->drought_hazards = isset($data['drought_hazards']) ? $data['drought_hazards'] : NULL;
			$school->drought_vulnerabilities = isset($data['drought_vulnerabilities']) ? $data['drought_vulnerabilities'] : NULL;
			$school->drought_risk_score_one = isset($data['drought_risk_score']) ? $data['drought_risk_score'] : NULL;
			$school->drought_risk_score_two = isset($data['drought_risk_score2']) ? $data['drought_risk_score2'] : NULL;
			$school->hailstorm_hazards = isset($data['hailstorm_hazards']) ? $data['hailstorm_hazards'] : NULL;
			$school->hailstorm_vulnerabilities = isset($data['hailstorm_vulnerabilities']) ? $data['hailstorm_vulnerabilities'] : NULL;
			$school->hailstorm_risk_score_one = isset($data['hailstorm_risk_score']) ? $data['hailstorm_risk_score'] : NULL;
			$school->hailstorm_risk_score_two = isset($data['hailstorm_risk_score2']) ? $data['hailstorm_risk_score2'] : NULL;
			$school->soil_erosion_hazards = isset($data['soil_erosion_hazards']) ? $data['soil_erosion_hazards'] : NULL;
			$school->soil_erosion_vulnerabilities = isset($data['soil_erosion_vulnerabilities']) ? $data['soil_erosion_vulnerabilities'] : NULL;
			$school->soil_erosion_risk_score_one = isset($data['soil_erosion_risk_score']) ? $data['soil_erosion_risk_score'] : NULL;
			$school->soil_erosion_risk_score_two = isset($data['soil_erosion_risk_score2']) ? $data['soil_erosion_risk_score2'] : NULL;
			$school->epidemics_hazards = isset($data['epidemics_hazards']) ? $data['epidemics_hazards'] : NULL;
			$school->epidemics_vulnerabilities = isset($data['epidemics_vulnerabilities']) ? $data['epidemics_vulnerabilities'] : NULL;
			$school->epidemics_risk_score_one = isset($data['epidemics_risk_score']) ? $data['epidemics_risk_score'] : NULL;
			$school->epidemics_risk_score_two = isset($data['epidemics_risk_score2']) ? $data['epidemics_risk_score2'] : NULL;

			if($school->save())
			{
				return new JsonResponse(array('status'=>'success', 'message'=>$message), 200);
			}
			else
			{
				return new JsonResponse(array('status'=>'error', 'message'=>'Erron in saving school assessment!'), 400);
			}

	    }
	    else
	    {
	    	// return $this->json_response('error',$validator->errors()->all(),400);
	    	return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
	    }
	}

}