<?php

namespace ShineOS\Core\PHIE\Http\Controllers;

use Illuminate\Routing\Controller;
use Shine\Libraries\FacilityHelper;
use Shine\Libraries\UserHelper;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Request, Input, DateTime, DB;
use ShineOS\Core\PHIE\Http\Controllers\PHIEController;
use ShineOS\Core\LOV\Http\Controllers\LOVController;
use Session;

class PHIEPatientsController extends Controller {

    public function __construct() {
        $this->patientsRequired = array(
                                    'Pat_Facility_No',
                                    'Pat_Last_Name',
                                    'Pat_First_Name',
                                    'Pat_Middle_Name',
                                    'Pat_Date_of_Birth',
                                    'Pat_Sex');

        $this->Patients_DeathInfoRequired = array('Pat_Facility_No',
                                    'Encounter_ID',
                                    'Date_of_Death',
                                    'Time_of_Death',
                                    'PlaceDeath',
                                    'Stage_of_Maternal_Death',
                                    'Immediate_Cause_of_Death',
                                    'Antecedent_Cause_of_Death',
                                    'Underlying_Cause_of_Death',
                                    'Type_of_Death');

        $this->arrNationality = nations();
        // dd($this->arrNationality);

        $this->arrRefSuffix = getArrRefSuffix();
        // dd($this->arrRefSuffix);

        $this->arrBloodType = getArrBloodType();
        // dd($this->arrBloodType);

        $this->arrArrCivilStatus = getArrCivilStatus();
        // dd($this->arrArrCivilStatus);

        $this->userInfo = UserHelper::getUserInfo();
        $this->facilityInfo = FacilityHelper::facilityInfo()->facility_id;

    }

    public function findPatientsByFacilityId($id, $compareDate) {
        $PHIEController = new PHIEController;
        //collect all patients with healthcare for this period even with older recorded info
        $hc = DB::table('healthcare_view')
                ->where('facility_id', $id)
                ->where('deleted_at', NULL)
                ->where('hcupdated','>',$compareDate)
                ->whereYear('encounter_datetime', '=', 2017)
                ->whereMonth('encounter_datetime', '<=', 3)
                ->distinct()
                ->lists('patient_id');
        //$PHIEController->install_logg("Patient with Care records:".count($hc));

        //collect all patients without healthcare for this period
        $pats = DB::table('patients_view')
                ->where('facility_id', $id)
                ->where('deleted_at', NULL)
                ->where('updated_at','>',$compareDate)
                ->whereYear('created_at', '=', 2017)
                ->whereMonth('created_at', '<=', 3)
                ->whereNotIn('patient_id', $hc)
                ->lists('patient_id');
        //$PHIEController->install_logg("Patient Data only records:".count($pats));

        $patlist = array_merge($hc, $pats);
        //collect all patients record from the list generated about
        $result = Patients::with('facilityUser','patientDeathInfo','patientContact','patientFamilyInfo','patientPhilhealthInfo', 'patientEmploymentInfo', 'facilityPatientUser','healthcareservices')
                ->whereIn('patient_id', $patlist)
                ->get();

        return $result;
    }

    public function PHIE_patientsData($facility_id, $compareToDate=NULL) {
        $param_type = 'PatientData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $PHIEController->install_logg("Please wait...");
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_patientsData = [];
        $PHIE_Excluded_Patients = [];
        $this_errors = [];
        $PHIEController->install_log("Collecting ".$param_type.".");
        $patientData = $this->findPatientsByFacilityId($facility_id,$compareToDate);

        $facilityInfo = findByFacilityID($facility_id);

        /*$patientData content cheatsheet
        DATAFIELDS
        "id"
        "patient_id"
        "first_name"
        "last_name"
        "middle_name"
        "email"
        "password"
        "salt"
        "maiden_lastname"
        "maiden_middlename"
        "name_suffix"
        "gender"
        "civil_status"
        "birthdate"
        "birthtime"
        "birthplace"
        "highest_education"
        "highesteducation_others"
        "religion"
        "religion_others"
        "nationality"
        "blood_type"
        "birth_order"
        "referral_notif"
        "broadcast_notif"
        "nonreferral_notif"
        "patient_consent"
        "myshine_acct"
        "age"
        "photo_url"
        "deleted_at"
        "created_at"
        "updated_at"

        RELATIONS
        "facilityUser"
        "patientDeathInfo"
        "patientContact"
        "patientFamilyInfo"
        "patientPhilhealthInfo"
        "patientEmploymentInfo"
        "facilityPatientUser"
        "healthcareservices"

        dd($patientData[0]->patientDeathInfo, $patientData[0]->patientContact, $patientData[0]->patientFamilyInfo, $patientData[0]->patientPhilhealthInfo, $patientData[0]->patientEmploymentInfo, $patientData[0]->healthcareservices);
        */

        // 			];
        
        if($patientData) {
            $c = count($patientData);
            $ct = 0;
            $PHIEController->install_log("Filtering ".$param_type.".");
            foreach ($patientData as $pkey => $pvalue) {
                if($pvalue->patientPhilhealthInfo == NULL) {
                    $PHIE_patientsData[$pkey]['Pat_Phil_Health_No'] = "NON-MEMBER";
                } else {
                    $PHIE_patientsData[$pkey]['Pat_Phil_Health_No'] = !empty($pvalue->patientPhilhealthInfo->philhealth_id) ?  $pvalue->patientPhilhealthInfo->philhealth_id : "NON-MEMBER";
                }

                $PHIE_patientsData[$pkey]['Pat_Facility_No'] = !empty($pvalue->patient_id) ?  $pvalue->patient_id : 0;

                $PHIE_patientsData[$pkey]['Pat_Last_Name'] = !empty($pvalue->last_name) ?  $pvalue->last_name : 'NA';
                $PHIE_patientsData[$pkey]['Pat_First_Name'] = !empty($pvalue->first_name) ?  $pvalue->first_name : 'NA';
                $PHIE_patientsData[$pkey]['Pat_Middle_Name'] = !empty($pvalue->middle_name) ?  $pvalue->middle_name : 'NA';

                $name_suffix = 'NA';
                if(!empty($pvalue->name_suffix)) {
                    if(array_key_exists($pvalue->name_suffix, $this->arrRefSuffix)) {
                        $name_suffix = $pvalue->name_suffix;
                    }
                }
                $PHIE_patientsData[$pkey]['Pat_Suffix_Name'] = $name_suffix;
                $PHIE_patientsData[$pkey]['Maiden_Last_Name'] = !empty($pvalue->maiden_last_name) ?  $pvalue->maiden_last_name : 'NA';
                $PHIE_patientsData[$pkey]['Maiden_First_Name'] = !empty($pvalue->maiden_first_name) ?  $pvalue->maiden_first_name : 'NA';
                $PHIE_patientsData[$pkey]['Maiden_Middle_Name'] = !empty($pvalue->maiden_middle_name) ?  $pvalue->maiden_middle_name : 'NA';
                $PHIE_patientsData[$pkey]['Pat_Date_of_Birth'] = !empty($pvalue->birthdate) ?  $pvalue->birthdate : '1900-01-01';
                $PHIE_patientsData[$pkey]['Pat_Place_of_Birth'] = !empty($pvalue->birthplace) ?  $pvalue->birthplace : 'Not provided';
                $PHIE_patientsData[$pkey]['Pat_Sex'] = !empty($pvalue->gender) ?  $pvalue->gender : 'U';


                $civil_status = 'U';
                if(!empty($pvalue->civil_status)) {
                    if(array_key_exists($pvalue->civil_status, $this->arrArrCivilStatus)) {
                        $civil_status = $pvalue->civil_status;
                    }
                }
                $PHIE_patientsData[$pkey]['Pat_Civil_Status'] = $civil_status;

                $nationality = 'PHL';
                if(!empty($pvalue->nationality)) {
                    if(array_key_exists($pvalue->nationality, $this->arrNationality)) {
                        $nationality = $pvalue->nationality;
                    }
                }

                $PHIE_patientsData[$pkey]['Pat_Nationality'] = $nationality;
                $PHIE_patientsData[$pkey]['Pat_Religion'] = !empty($pvalue->religion) ?  $pvalue->religion : NULL;
                if($pvalue->religion == 'OTHER') {
                    $PHIE_patientsData[$pkey]['Pat_Religion_Other_Specify'] = !empty($pvalue->religion_others) ?  $pvalue->religion_others : 'Not given';
                } else {
                    $PHIE_patientsData[$pkey]['Pat_Religion_Other_Specify'] = NULL;
                }


                $blood_type = NULL;
                if(!empty($pvalue->blood_type)) {
                    if(array_key_exists($pvalue->blood_type, $this->arrBloodType)) {
                        if($pvalue->blood_type != 'U') {
                            $blood_type = $pvalue->blood_type;
                        }
                    }
                }
                $PHIE_patientsData[$pkey]['Pat_Blood_Type'] = $blood_type;
                $PHIE_patientsData[$pkey]['Pat_Pantawid_Pamilya_Member'] = !empty($pvalue->pantawid_pamilya_member) ?  $pvalue->pantawid_pamilya_member : 'NOT MEMBER';
                $PHIE_patientsData[$pkey]['Pat_Current_Address_StreetName'] = !empty($pvalue->street_address) ?  $pvalue->street_address : NULL;
                $PHIE_patientsData[$pkey]['Pat_Current_Address_Region'] = !empty($pvalue->region) ?  $pvalue->region : NULL;
                $PHIE_patientsData[$pkey]['Pat_Current_Address_Province'] = !empty($pvalue->province) ?  $pvalue->province : NULL;
                $PHIE_patientsData[$pkey]['Pat_Current_Address_City'] = !empty($pvalue->city) ?  $pvalue->city : NULL;
                $PHIE_patientsData[$pkey]['Pat_Current_Address_Barangay'] = !empty($pvalue->barangay) ?  $pvalue->barangay : NULL;
                $PHIE_patientsData[$pkey]['Pat_Current_Address_SitioPurok'] = !empty($pvalue->sitioPurok) ?  $pvalue->sitioPurok : NULL;
                $PHIE_patientsData[$pkey]['Pat_Current_Address_ZipCode'] = !empty($pvalue->zip) ?  $pvalue->zip : NULL;
                $PHIE_patientsData[$pkey]['Pat_Current_Address_Country'] = !empty($pvalue->country) ?  $pvalue->country : NULL;
                $PHIE_patientsData[$pkey]['Pat_Contact_Landline'] = !empty($pvalue->phone) ?  $pvalue->phone : 'NONE';
                $PHIE_patientsData[$pkey]['Pat_Contact_Mobile'] = !empty($pvalue->mobile) ?  $pvalue->mobile : 'NONE';
                $PHIE_patientsData[$pkey]['Pat_Contact_EmailAddress'] = !empty($pvalue->email) ?  $pvalue->email : 'NONE';
                $PHIE_patientsData[$pkey]['Pat_Current_Occupation_Description'] = !empty($pvalue->occupation) ?  $pvalue->occupation : 'NA';
                $PHIE_patientsData[$pkey]['Pat_Highest_Education'] = !empty($pvalue->highest_education) ?  $pvalue->highest_education : NULL;
                $PHIE_patientsData[$pkey]['Pat_Education_Other_Specify'] = !empty($pvalue->highesteducation_others) ?  $pvalue->highesteducation_others : 'NA';


                $PHIE_patientsData[$pkey]['Pat_Father_Firstname'] = !empty($pvalue->patientFamilyInfo->father_firstname) ?  $pvalue->patientFamilyInfo->father_firstname : 'NA';
                $PHIE_patientsData[$pkey]['Pat_Father_Middlename'] = !empty($pvalue->patientFamilyInfo->father_middlename) ?  $pvalue->patientFamilyInfo->father_middlename : 'NA';
                $PHIE_patientsData[$pkey]['Pat_Father_Lastname'] = !empty($pvalue->patientFamilyInfo->father_lastname) ?  $pvalue->patientFamilyInfo->father_lastname : 'NA';
                $PHIE_patientsData[$pkey]['Pat_Father_Suffixname'] = (!empty($pvalue->patientFamilyInfo->suffix) AND  $pvalue->patientFamilyInfo->suffix != 'on') ?  $pvalue->patientFamilyInfo->suffix : '';

                $PHIE_patientsData[$pkey]['Pat_Mother_Firstname'] = !empty($pvalue->patientFamilyInfo->mother_firstname) ?  $pvalue->patientFamilyInfo->mother_firstname : 'NA';
                $PHIE_patientsData[$pkey]['Pat_Mother_Lastname'] = !empty($pvalue->patientFamilyInfo->mother_middlename) ?  $pvalue->patientFamilyInfo->mother_middlename : 'NA';
                $PHIE_patientsData[$pkey]['Pat_Mother_Middlename'] = !empty($pvalue->patientFamilyInfo->mother_lastname) ?  $pvalue->patientFamilyInfo->mother_lastname : 'NA';

                $PHIE_patientsData[$pkey]['For_Enlistment'] = !empty($pvalue->patientPhilhealthInfo->For_Enlistment) ?  'Y' : 'Y';
                $PHIE_patientsData[$pkey]['For_EnlistmentDateTime'] = !empty($pvalue->patientPhilhealthInfo->created_at) ?  date('Y-m-d H:i:s', strtotime($pvalue->patientPhilhealthInfo->created_at)) : date('Y-m-d H:i:s', strtotime($pvalue->created_at));
            }

            //unset the array if incomplete data
            $out = false;
            if(count($PHIE_patientsData)) {
                foreach ($PHIE_patientsData as $result_key => $result_value) {
                    foreach ($result_value as $val_key => $val_value) {
                        $exists = in_array($val_key, $this->patientsRequired);
                        if($exists) {
                            if($val_value == NULL OR $val_value == "") {
                                $this_errors[] = $val_key;
                                $out = true;
                            }
                        }
                    }
                    if($out == true) {
                        $PHIE_Excluded_Patients[] = [ 'id'=>$result_key, 'Patient ID' => $PHIE_patientsData[$result_key]->Pat_Facility_No, 'Patient Name'=>$PHIE_patientsData[$result_key]->Pat_First_Name." ".$PHIE_patientsData[$result_key]->Pat_Last_Name, 'Incomplete Data'=>$this_errors ];
                        $out = false;
                        $this_errors = [];
                        unset($PHIE_patientsData[$result_key]);
                    }
                }
            }

            $data[$param_type] = $PHIE_patientsData;
            $data_Patient_ID = array_values(array_column($PHIE_patientsData, 'Pat_Facility_No'));
            $filer = $PHIE_Excluded_Patients;

            if(Session::get('_global_phie_type') == 'check') {
                $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_patientsData) ." records.");
                $PHIEController->install_logg("Excluded ".$param_type." ".count($filer).".<br />");
                $error_file = userfiles_path() .  'logs/'.date('Y-m-d').'_'.studly_case($facilityInfo->facility_name)."_".$param_type.'_error_log.txt';
                @file_put_contents($error_file, json_encode($filer) . "\n");
                //return result of send
                return array('result'=> NULL, 'PHIE_patientsData' => $data_Patient_ID);
            } elseif(Session::get('_global_phie_type') == 'singlex' OR Session::get('_global_phie_type') == 'single') {
                return array('result'=> NULL, 'PHIE_patientsData' => $data_Patient_ID);
            } elseif(Session::get('_global_phie_type') == 'singles') {
                $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_patientsData) ." records");
                $result = $PHIEController->sendArray($data, $param_type, $data_Patient_ID, $filer);
                return array('result'=> $result['result'],'PHIE_patientsData' => $result['newData']);
            } else {
                $nd = array();
                //send to PHIE
                /*$batch = 1;
                if(count($PHIE_patientsData) > 100) {
                    $batch = count($PHIE_patientsData)/100;
                }*/
                
                $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_patientsData) ." records");
                
                //for($x = 1; $x <= ceil($batch); $x++) {
                    $PHIEController->install_logg("<br>Sending data<br />");
                    //$slicearray = $data['PatientData'];
                    //$sendthis = array_slice($slicearray, ((100*$x)-99), (100*$x), true);
                    //$send['PatientData'] = $sendthis;
                    
                    $result = $PHIEController->sendArray($data, $param_type, $data_Patient_ID, $filer);
                    //$send['PatientData'] = NULL;
                    //$nd = $nd + $result['newData'];
                //}
                //return result of send
                return array('result'=> $result['result'],'PHIE_patientsData' => $result['newData']);
            }

        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

/**
 * Death
 */
    public function PHIE_Patients_DeathInfo($facility_id) {
        $param_type = 'DeathData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_Patients_DeathInfo = [];
        $DeathInfoData = $this->findPatientsByFacilityId($facility_id,$compareToDate);
        $PHIEController->install_logg("Please wait...");

        $healthcare_id = NULL;
        if($DeathInfoData) {
            $c = count($DeathInfoData);
            $PHIEController->install_log("Processing ".$param_type.".");
            $ct = 0;
            foreach ($DeathInfoData as $key => $value) {
                if(!is_null($value->healthcareservices) AND !is_null($value->patientDeathInfo)) {
                    foreach ($value->healthcareservices as $k_healthcare => $v_healthcare) {
                        $healthcare_datetime = new DateTime($v_healthcare->updated_at);
                        $healthcare_datetime = $healthcare_datetime->format('Y-m-d');

                        $patientDeathInfo_datetime = new DateTime($value->patientDeathInfo->updated_at);
                        $patientDeathInfo_datetime = $patientDeathInfo_datetime->format('Y-m-d');

                        if($healthcare_datetime == $patientDeathInfo_datetime AND $value->patient_id == $v_healthcare->patient_id) {
                            $healthcare_id = $v_healthcare->healthcareservice_id;
                        }
                    }

                    if($healthcare_id!=NULL) {
                        $PHIE_Patients_DeathInfo[$key]['Pat_Facility_No'] = !is_null($value->patient_id) ?  $value->patient_id : "";
                        $PHIE_Patients_DeathInfo[$key]['Encounter_ID'] = $healthcare_id;
                        $PHIE_Patients_DeathInfo[$key]['DeathCertificateNo'] = !is_null($value->patientDeathInfo->DeathCertificateNo) ?  $value->patientDeathInfo->DeathCertificateNo : "NONE";

                        $datetime_death = new Datetime($value->patientDeathInfo->datetime_death);
                        $date_death = $datetime_death->format('Y-m-d');
                        $time_death = $datetime_death->format('H:i:s');

                        $PHIE_Patients_DeathInfo[$key]['Date_of_Death'] = !is_null($date_death) ?  $date_death : "";
                        $PHIE_Patients_DeathInfo[$key]['Time_of_Death'] = !is_null($time_death) ?  $time_death : "";
                        $PHIE_Patients_DeathInfo[$key]['PlaceDeath'] = !is_null($value->patientDeathInfo->PlaceDeath) ?  $value->patientDeathInfo->PlaceDeath : "";
                        $PHIE_Patients_DeathInfo[$key]['PlaceDeath_FacilityBased'] = !is_null($value->patientDeathInfo->PlaceDeath_FacilityBased) ?  $value->patientDeathInfo->PlaceDeath_FacilityBased : NULL;
                        $PHIE_Patients_DeathInfo[$key]['PlaceDeath_NID'] = !is_null($value->patientDeathInfo->PlaceDeath_NID) ?  $value->patientDeathInfo->PlaceDeath_NID : NULL;
                        $PHIE_Patients_DeathInfo[$key]['PlaceDeath_NID_Others_Specify'] = !is_null($value->patientDeathInfo->PlaceDeath_NID_Others_Specify) ?  $value->patientDeathInfo->PlaceDeath_NID_Others_Specify : NULL;
                        $PHIE_Patients_DeathInfo[$key]['Stage_of_Maternal_Death'] = !is_null($value->patientDeathInfo->mStageDeath) ?  $value->patientDeathInfo->mStageDeath : "";
                        $PHIE_Patients_DeathInfo[$key]['Immediate_Cause_of_Death'] = !is_null($value->patientDeathInfo->Immediate_Cause_of_Death) ?  $value->patientDeathInfo->Immediate_Cause_of_Death : "";
                        $PHIE_Patients_DeathInfo[$key]['Antecedent_Cause_of_Death'] = !is_null($value->patientDeathInfo->Antecedent_Cause_of_Death) ?  $value->patientDeathInfo->Antecedent_Cause_of_Death : "";
                        $PHIE_Patients_DeathInfo[$key]['Underlying_Cause_of_Death'] = !is_null($value->patientDeathInfo->Underlying_Cause_of_Death) ?  $value->patientDeathInfo->Underlying_Cause_of_Death : "";
                        $PHIE_Patients_DeathInfo[$key]['Type_of_Death'] = !is_null($value->patientDeathInfo->Type_of_Death) ?  $value->patientDeathInfo->Type_of_Death : "";
                        $PHIE_Patients_DeathInfo[$key]['Remarks'] = !is_null($value->patientDeathInfo->Remarks) ?  $value->patientDeathInfo->Remarks : "";
                        $healthcare_id = NULL;
                    }
                }
            }

            //unset the array if incomplete data
            if(count($PHIE_Patients_DeathInfo)) {
                foreach ($PHIE_Patients_DeathInfo as $result_key => $result_value) {
                    foreach ($result_value as $val_key => $val_value) {
                        $exists = in_array($val_key, $this->Patients_DeathInfoRequired);
                        if($exists) {
                            if($val_value == NULL) {
                                unset($PHIE_Patients_DeathInfo[$result_key]);
                            }
                        }
                    }
                }
            }

            // dd($PHIE_Patients_DeathInfo);
            // return $PHIE_Patients_DeathInfo;
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_Patients_DeathInfo) ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $PHIE_Patients_DeathInfo;
            $result = $PHIEController->sendArray($data,$param_type, NULL);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }


    }

/**
 * Alert
 */



}
