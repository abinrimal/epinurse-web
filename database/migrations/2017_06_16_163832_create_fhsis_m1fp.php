<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFhsisM1fp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('fhsis_m1fp')!=TRUE) {
            Schema::create('fhsis_m1fp', function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('facility_id',60);
                $table->integer('CU_begin');
                $table->integer('NA_begin')->nullable();
                $table->integer('NA_end')->nullable();
                $table->string('current_method',60)->nullable();
                $table->string('previous_method',60)->nullable();
                $table->integer('CU');
                $table->integer('NA');
                $table->integer('CC')->nullable();
                $table->integer('CM')->nullable();
                $table->integer('RS')->nullable();
                $table->integer('OA');
                $table->integer('Dropout');
                $table->integer('FPmonth');
                $table->integer('FPyear');
                $table->integer('FP_count');
                $table->integer('CU_end');

                
                $table->softDeletes();
                $table->timestamps();

            }); 
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
